/**
* master view model
* it will hold a reference for the other view models
* that is done dinamically on the router
*/

var sbm = sbm || {};

sbm.VmMain = function (sb) {

    // view model properties

    this.welcomeMessage = sb.ko.observable(""); 

    // module initialization
    this.init = function () {
        
        // subscribe event when player has logged in
        sb.listen("done-login", this.onDoneLogin);

        console.log("VmMain init");
    };

    // module destruction
    // stop listening for events
    this.destroy = function () {

        sb.ignore("logged-in");

        console.log("VmMain destroy");

    };

    // callback when player logs in
    this.onDoneLogin = function (e, data) {

        if (data.status > 0) {
            this.welcomeMessage("Welcome " + data.username);
        }

        console.log("VmMain: user has logged in");

    }.bind(this);

};