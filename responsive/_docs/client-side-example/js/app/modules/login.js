/**
 * login view model
 */

var sbm = sbm || {};

sbm.Login = function (sb) {

    // view model properties

    // username textbox has ko value binding pointing to this property
    this.username = sb.ko.observable("");
    // password textbox has ko value binding pointing to this property
    this.password = sb.ko.observable("");
    // welcome message form has ko visible binding pointing to this property
    this.isLoggedIn = sb.ko.observable(false);
    // login form form has ko visible binding pointing to this property
    this.isNotLoggedIn = sb.ko.observable(true);

    // functions

    // view model initialization
    this.init = function () {

        // subscribe event when player has logged in
        sb.listen("done-login", this.onDoneLogin);

        console.log("Login init");
    };

    // view model destruction
    this.destroy = function () {
        console.log("Login destroy");
    };

    // player clicked on log in button
    // the button has a knockout click binding pointing to this function
    this.doLogin = function () {
        if (this.username() && this.password()) {

            // notify that user wants to log in
            sb.notify("do-login", [this.username(), this.password()]);

        }
    }.bind(this);

    // callback when player logs in
    this.onDoneLogin = function (e, data) {

        if (data.status > 0) {

            // change properties
            // this will hide the login form and show the welcome message

            this.isLoggedIn(true);
            this.isNotLoggedIn(false);

            console.log("Login: player has successfuly logged in");

        }

    }.bind(this);

};