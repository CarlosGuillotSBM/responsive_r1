/**
* The sandbox is the our facade
* The only thing modules know about
* It exposes a simpler API fromo the core library 
*/

var sbm = sbm || {};

sbm.Sandbox = function (core) {
	
	// expose 3rd party libraries to modules
	
	this.ko = core.ko;
	this.$ = core.$;

	// Pub / Sub

	this.notify = function (e, data) {
        core.publish(e, data);
    };

	this.listen = function (e, data) {
        core.subscribe(e, data);
    };

    this.ignore = function (e) {
        core.unsubscribe(e);
    };
};