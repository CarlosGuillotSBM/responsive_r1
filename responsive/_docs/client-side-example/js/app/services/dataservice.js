/**
* Responsible for all the AJAX requests to the server
*/

var sbm = sbm || {};

sbm.Dataservice = function (core) {

	// service initialization
	this.init = function () {

		// listens for login request
		core.subscribe("do-login", this.doLogin);

		console.log("Dataservice init");
	};

	// login request
	this.doLogin = function (e, username, password) {

		// AJAX call should be added here

		if (username && password) {
			
			var data = {
				status: 1,
				username: username
			};

			// notify view models that player has logged in
			core.publish("done-login", data);
		}

	};

};