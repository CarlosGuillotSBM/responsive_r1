/**
* The router is responsible for the module management
*/

var sbm = sbm || {};

sbm.Router = function (core, modules) {

	// properties

	this.vmMain = null;
	this.registeredModules = [];

	// service initialization
	this.init = function () {

		// create master view model
		this.vmMain = new sbm.VmMain(core.sb);
		this.vmMain.init();

		// start modules
		this.startModules();

		// destroy modules
		// this.destroyModues();
	};

	// create and start all the modules
	this.startModules = function () {
		
		// start modules
		var i = 0, vmName;
		for (i; i < modules.length; i++) {
			
			// instantiate module
			vmName = modules[i];
			var vm = new sbm[vmName](core.sb);
			
			// add reference to master view model
			this.vmMain[vmName] = vm;

			// start view model
			vm.init();

			// add to registered modules
			this.registeredModules.push(vmName);

			// apply ko bindings
			core.ko.applyBindings(this.vmMain, window.document.body);
		}
	};

	// destroy the created modules
	this.destroyModues = function () {
		
		var i=0, vmName;

		// loop through all registered modules
		// call destroy function of all of them
		for (i; i < this.registeredModules.length; i++) {
			vmName = this.registeredModules[i];
			this.vmMain[vmName].destroy();
		}	
	};

};