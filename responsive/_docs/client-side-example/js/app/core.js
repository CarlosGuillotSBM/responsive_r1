/**
* The library mediator
* Starts services
*/
var sbm = sbm || {};

sbm.Core = function ($, ko, modules) {

	// properties

	this.router = null;
	this.dataservice = null;
	this.sb = null;
	this.o = $({});
	
	// expose 3rd party libraries

	this.ko = ko;
	this.$ = $;

	// functions

	this.start = function () {

		// start sandbox

		this.sb = new sbm.Sandbox(this);

		// start services
		
		this.dataservice = new sbm.Dataservice(this);
		this.dataservice.init();

		this.router = new sbm.Router(this, modules);
		this.router.init();

	};

	// Custom Events

	// subscribe event
    this.subscribe = function () {
    	this.o.on.apply(this.o, arguments);
    }.bind(this);

    // unsubscribe event
    this.unsubscribe = function () {
        this.o.off.apply(this.o, arguments);
    }.bind(this);

    // publish event
    this.publish = function () {
        this.o.trigger.apply(this.o, arguments);
    }.bind(this);
};