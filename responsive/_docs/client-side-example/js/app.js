/**
* Initial script
* Loads the necessary scripts with yepnope
* Adds all modules to an array
* Instantiate the app and start it
*/

(function (yepnope, skinModules) {

    "use strict";
    
    // Let's load all the required scripts

    yepnope(
        [
            {
                load: [
                    "js/vendor/jquery.js",
                    "js/vendor/ko.js",
                    "js/app/core.js",
                    "js/app/sandbox.js",
                    "js/app/services/router.js",
                    "js/app/services/dataservice.js",
                    "js/app/modules/login.js",
                    "js/app/modules/vm-main.js"
                ],
                complete: initApp
            }
        ]
    );

    // callback after all the scripts have been loaded
    function initApp() {

        // add all the modules that have been added on the page

        var modules = [];
        
        var i = 0;
        for (i; i < skinModules.length; i++) {
            modules.push(skinModules[i]);
        }

        // start app

        var app = new sbm.Core($, ko, modules);
        app.start();
    }

}(yepnope, skinModules));