<?php

/**
 * Class Game Frame
 *
 */
class GameFrame extends Controller
{
    public function index()
    {
        $this->getFullPage('_global-library/partials/common/head.php');
        $this->getView('_global-library/partials/common/game-frame.php');
        $this->getFullPage('_global-library/partials/common/footer.php');
    }
    public function action($action, $params = null){
        /* 
            If we need to open a game and redirect to that URL directly from 
            our own site
        */
        if($action=="thirdparty"){
            
            $this->getFullPage('_global-library/partials/common/head.php');
            $this->getView('_global-library/partials/common/game-frame-redirect.php');
            $this->getFullPage('_global-library/partials/common/footer.php');
        }
        else if($action=="prize-wheel"){
            
            $this->getFullPage('_global-library/partials/common/head.php');
            $this->getView('_global-library/partials/common/game-frame__prize-wheel.php');
            $this->getFullPage('_global-library/partials/common/footer.php');
        }
        /* 
            Third party games to be displayed on an Iframe
        */
        else if($action=="mobile"){
            
            $this->getFullPage('_global-library/partials/common/head.php');
            $this->getView('_global-library/partials/common/game-frame-mobile.php');
            $this->getFullPage('_global-library/partials/common/footer.php');
        }
        else if($action === "three-radical"){
            $playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : -0;
            $sessionID = (isset($_COOKIE["SessionID"])) ? $_COOKIE["SessionID"] : 'NULL';
            $service_url = config("ThreeRadicalAPI");
            $threeParams = [
                'playerId' => $playerID,
                'sessionId' => $sessionID,
                'skinId' => config("SkinID")
            ];
            if($playerID != 0 && $sessionID != "NULL"){
                $this->token = $this->callMicroService($service_url, 'internal/generate-hmac', $threeParams, 1,'GET');
            }
            else{
                $this->token = -1;
            }
            $this->getFullPage('_global-library/partials/common/head.php');
            $this->getView('_global-library/partials/common/game-frame-t.php');
            $this->getFullPage('_global-library/partials/common/footer.php');
        }
        else{
            $this->error404();
        }
    }
}
