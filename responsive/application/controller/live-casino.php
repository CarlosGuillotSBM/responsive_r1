<?php

/**
 * Class Roulette

 */
class livecasino extends Controller
{
    public $row;
    public $games;
    public $rs;
    public $category;

    // called if the url is clean (no game specified) ie:	/games
    public function index()
    {
        $useCache = true;
        if(!$this->cached())
        {


             if ($this->controller == 'livecasino' and config('SkinID') == 10){
                    header('HTTP/1.1 301 Moved Permanently');
                    header("Location: /" . 'live-dealer');
                    exit;
            }


            $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
            $gamesModel = $this->loadModel("GamesModel");
            $this->category = $gamesModel->getGames('live-casino');
            $this->rs = $gamesModel->rs;
            $this->games = queryDBNextRS($this->rs);
            $useCache = !empty($this->games);
        }

        // load views.
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');
        if (file_exists('_views/live-casino/live-casino.php')) {
            $this->getViewAndCache('_views/live-casino/live-casino.php');
        } else {
            $this->getViewAndCache('_views/games/games.php', '', $useCache);
        }
        $this->getFullPage('_partials/common/footer.php');

    }

    // called if an action supplied ie:	/{category}/progressive-slots
    public function action($action, $params = null)
    {
        if ($params) {

            if( count($params) > 1 ){
                return $this->error404();
            } 


            $correctController = true;

            if (!$this->cached()) {
                $this->row = $this->loadModel("GamesModel")->getGameDetail($params[0]);
                $this->content = $this->loadModel("ContentModel")->getContent('gamesdetail', $params[0]);

                // make sure the controller = the gameCategory.URLKey
                if (!isset($this->row['URLKey']) || str_replace('-', '', $this->row['URLKey']) != $this->controller) {
                    $correctController = false;
                }
            } else {
                // make sure the controller = the start of the cache key
                if (substr(str_replace('_', '', $this->cache_key), 0, strlen($this->controller)) != $this->controller) $correctController = false;
            }

            if (($this->row != null || $this->cached()) && $correctController) {
                // load views. within the views we can echo out $songs and $amount_of_songs easily
                $this->getFullPage('_partials/common/head.php');
                $this->getFullPage('_partials/common/header.php');
                $this->getViewAndCache('_views/games/game-detail.php', $action);
                $this->getFullPage('_partials/common/footer.php');
            } else {
                $this->notFoundPage();
            }
        } else {



            $useCache = true;
            if (!$this->cached()) {

                $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
                $gamesModel = $this->loadModel("GamesModel");
                $this->category = $gamesModel->getGamesByCategoryAndProvider('live-casino', $action);
                $this->rs = $gamesModel->rs;
                $this->games = queryDBNextRS($this->rs);
                if (empty($this->category)) { // it must be category
                    //return $this->error404();
                    $this->category = $gamesModel->getGames($action);
                    $this->rs = $gamesModel->rs;
                    $this->games = queryDBNextRS($this->rs);
                }
                $useCache = !empty($this->games);

            }

            // load views.
            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            if (file_exists('_views/live-casino/live-casino.php')) {
                $this->getViewAndCache('_views/live-casino/live-casino.php');
            } else {
                $this->getViewAndCache('_views/games/games.php', '', $useCache);
            }
            $this->getFullPage('_partials/common/footer.php');
        }

    }
}


