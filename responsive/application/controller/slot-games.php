<?php

/**
 * Class Slot Games

 */
class Slotgames extends Controller
{
    public $row;
	public $games;
	public $rs;
	public $category;

	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {

		// luckypants transfer SEO redirects
		if(config('SkinID') <> Skin::GiveBackBingo && config('SkinID') <> Skin::LuckyVIP && config('SkinID') <> Skin::RegalWins) {
            $this->notFoundPage();
		} else {
	    	// weird hack to know if Player opted to go to account history from a NETENT game
	    	if (isset($_REQUEST["reason"]) && isset($_REQUEST["gameId"]) && isset($_REQUEST["sessId"])) {
	    		if ($_REQUEST["reason"] == 10) {
	    			header("Location: " . config("accountActivityURL"));
	    		}
            }
            
            $useCache = true;
			if(!$this->cached()) {
				$this->openDB();
				$this->content = $this->loadModel("ContentModel")->getContent($this->controller);
				$this->action = 'slots';

				$gamesModel = $this->loadModel("GamesModel");
				$this->category = $gamesModel->getGames('slot-games');
				$this->rs = $gamesModel->rs;
                $this->games = queryDBNextRS($this->rs);
                $useCache = !empty($this->games);
			}

	        // load views.
			$this->getFullPage('_partials/common/head.php');
	        $this->getFullPage('_partials/common/header.php');
	        $this->getViewAndCache('_views/games/games.php', '', $useCache);
			$this->getFullPage('_partials/common/footer.php');
		}
    }

	// called if an action supplied ie:	/{category}/progressive-slots
    public function action($action, $params = null)
    {
        $fault = true;
        if (!$params) {
            switch( config('SkinID')) {
                case Skin::GiveBackBingo:
                case Skin::LuckyVIP:
                case Skin::RegalWins:

                    $useCache = true;
                    if (!$this->cached()) {
                        $this->openDB();

                        $data = $this->controller;
                        if ($action == 'exclusive') $data .= $action; // a hack so the exclusive slots get their own hub

                        $this->content = $this->loadModel("ContentModel")->getContent($data);

                        $gamesModel = $this->loadModel("GamesModel");
                        $this->category = $gamesModel->getGames($action);
                        $this->rs = $gamesModel->rs;
                        $this->games = queryDBNextRS($this->rs);
                        $useCache = !empty($this->games);
                    }

                    if ($this->category != null || $this->cached()) {
                        // load views.
                        $this->getFullPage('_partials/common/head.php');
                        $this->getFullPage('_partials/common/header.php');
                        $this->getViewAndCache('_views/games/games.php', '', $useCache);
                        $this->getFullPage('_partials/common/footer.php');
                        $fault = false;
                    }
                    break;
            }
        }

        if (@$fault) {
            $this->notFoundPage();
        }
    }

}
