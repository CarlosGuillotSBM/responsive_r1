<?php

/**
* Class Games

*/
class Homepage extends Controller
{


	// called if the url is clean (no game specified) ie:	/games
	public function index()
	{
		if(config('SkinID') == 1) {
			if(!$this->cached()) {
				$this->openDB();
				$this->content = $this->loadModel("ContentModel")->getContent($this->controller);
			}
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getViewAndCache('_views/ab/ab.php');
			$this->getFullPage('_partials/common/footer.php');
		} else {
			$this->error();
		}
	}


}
