<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Bingo extends Controller
{
	//public $bingo;
    public $promotions;

    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
        $this->bingoNotSupportedForIE11GiveBackBingo();
        if(!$this->cached()) {
            $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
			$this->action = 'bingo';			
		}					

		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        			
		$this->getViewAndCache('_views/bingo/bingo.php');
		$this->getFullPage('_partials/common/footer.php');
    }
    
    public function action($action, $params = null)
    {

 

        if (  count($params) > 1 ){
            return    $this->notFoundPage();
        } 

        if ($params) {
            $this->bingoNotSupportedForIE11GiveBackBingo();
            if(!$this->cached())
            {
                $model = $this->loadModel("ContentModel");
                $this->bingo = $model->getContentDetail('bingo',$params[0]);
                $this->content = $model->getContent('bingodetail',$params[0]);
            }

            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            $this->getViewAndCache('_views/bingo/bingo-detail.php');
            $this->getFullPage('_partials/common/footer.php');
        } else {
            $this->bingoNotSupportedForIE11GiveBackBingo();
            if(config("SkinID") == 3 || config("SkinID") == 2 )
            {
                $model = $this->loadModel("ContentModel");


                
                $this->category = $model->getContent('bingo',$action);

                $games = array_keys($this->category);
                 
                $check = false;
                foreach ($games as $key => $value){
                   if (strpos( $value, $action)){
                    $check = true;
                   }                   
                }

                if(!$check){
                    return    $this->notFoundPage();
                } 
 

                $this->promotions = $this->category;
                $this->content =  isset($this->category['bingo-rooms-slider'][0]) ?
                    $this->category['bingo-rooms-slider'][0] : [];

                $this->getFullPage('_partials/common/head.php');
                $this->getFullPage('_partials/common/header.php');
                $this->getViewAndCache('_views/bingo/bingo-detail.php');
                $this->getFullPage('_partials/common/footer.php');
            }
            else
            {


                if(!$this->cached())
                {
                    $this->content = $this->loadModel("ContentModel")->getContent($this->controller);


                }

                $this->getFullPage('_partials/common/head.php');
                $this->getFullPage('_partials/common/header.php');
                $this->getViewAndCache('_views/bingo/bingo.php');
                $this->getFullPage('_partials/common/footer.php');
            }
        }

    }


    private function bingoNotSupportedForIE11GiveBackBingo()
    {
        $mobileDetect = new Mobile_Detect();
        if (config("SkinID") == Skin::GiveBackBingo && $mobileDetect->version('IE') && $mobileDetect->version('IE', Mobile_Detect::VERSION_TYPE_FLOAT) <= 11) {
            self::redirect("/browser-not-supported-chrome/");
        }
    }
}