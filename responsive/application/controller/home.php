<?php


/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Home extends Controller
{

	
	public $games = array();
	public $category;
	public $rs;
	public $promotions;
	public $community;

	
	
	public function index()
	{
		if($_SERVER["REQUEST_URI"] == '/home' || $_SERVER["REQUEST_URI"] == '/home/')
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			exit;
		}

		if(!$this->cached()) {
			$homeModel = $this->loadModel("ContentModel");									
			$this->content = $homeModel->getContent('home');
			$this->community = $homeModel->getContentForCommunity();
			$promos = $homeModel->getContent('promotions','',1); // the final 1 means, expired Content being received
			$this->promotions = @array_filter($promos["promotions"],function($promotion){  
				return empty($promotion["dateEnd"]);
            }); 

			$gamesModel = $this->loadModel("GamesModel");			

			$this->category = $gamesModel->getGames('LOBBY');	
			$this->rs = $gamesModel->rs;
			$this->games = queryDBNextRS($this->rs);				
		} else {
			
			if (config("SkinID") == 1) {
				$homeModel = $this->loadModel("ContentModel");									
				$this->content = $homeModel->getContent('home');
			}
		}
		
		
		
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/home/home.php');
		$this->getFullPage('_partials/common/footer.php');		
	}
	
	public function action($action, $params = null)
	{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			exit;		
	}
}
