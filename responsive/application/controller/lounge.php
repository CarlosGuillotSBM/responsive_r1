<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Lounge extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
		//echo "action=".$action;

    
		if(!$this->cached())
		{				
			$this->content = $this->loadModel("ContentModel")->getContent('lounge');		
		}	
		// load views. within the views we can echo out $songs and $amount_of_songs easily
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getView('_views/lounge/lounge.php');
		$this->getFullPage('_partials/common/footer.php');	
    }

	
	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {
        if ($params) 	{
            if(!$this->cached())
            {
                $this->content = $this->loadModel("ContentModel")->getContentDetail('lounge',$params[0]);
            }

            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            $this->getView('_views/lounge/lounge-article.php');
            $this->getFullPage('_partials/common/footer.php');
        } else {
            if(!$this->cached())
            {
                $this->content = $this->loadModel("ContentModel")->getContent('lounge');
            }

            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');

            switch ($action) {

                case 'vip':  	$this->getView('_views/lounge/lounge-vip.php'); break;
                case 'tips': 	$this->getView('_views/lounge/lounge-tips.php'); break;
                case 'tv': 	 	$this->getView('_views/lounge/lounge-tv.php'); break;
                case 'winners': $this->getView('_views/lounge/lounge-winners.php'); break;
                case 'blog': 	$this->getView('_views/lounge/lounge-blog.php'); break;
                case 'moolahs': $this->getView('_views/lounge/lounge-moolahs.php'); break;


            }
            $this->getFullPage('_partials/common/footer.php');
        }

    }
}
