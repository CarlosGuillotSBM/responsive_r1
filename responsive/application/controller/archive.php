<?php

class Archive extends Controller
{
    public $dates;
	
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
		if(!$this->cached())
		{							
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);	
		}				
		
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/archive/archive.php');
		$this->getFullPage('_partials/common/footer.php');	
	
    }

	
    public function action($action, $params = null)
    {
        if ($params)  {
            if($action == 'games') $action = 'gamesdetail';

            if(!$this->cached())
            {
                // archive/promotions/201501/
                $contentModel =  $this->loadModel("ContentModel");
                $this->dates = 	 $contentModel->getArchiveContentDates($action,$params[0]);
                $this->content = $contentModel->getArchiveContentList($action,$params[0]);
            }

            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            $this->getViewAndCache('_views/archive/archive-list.php');
            $this->getFullPage('_partials/common/footer.php');
        } else {
            switch($action)
            {
                case 'blog':
                case 'winners':
                case 'promotions':
                case 'games':
                    if($action == 'games') $action = 'gamesdetail';

                    if(!$this->cached())
                    {
                        // archive/promotions/201501/
                        $contentModel =  $this->loadModel("ContentModel");
                        $this->dates = 	 $contentModel->getArchiveContentDates($action,'');
                        $this->content = $contentModel->getArchiveContentList($action,'');
                    }
                    $this->getFullPage('_partials/common/head.php');
                    $this->getFullPage('_partials/common/header.php');
                    $this->getViewAndCache('_views/archive/archive-list.php');
                    $this->getFullPage('_partials/common/footer.php');
                    break;

                default:
                    $this->error404();
                    break;
            }
        }

    }
}
