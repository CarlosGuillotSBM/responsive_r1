<?php

class Ab extends Controller
{
    
	
    // called if the url is clean (no promotion specified) ie:	/ab
    public function index()
    {
		if(!$this->cached())
		{
			$this->openDB();							
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);	
		}				
		
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/ab/ab.php');
		$this->getFullPage('_partials/common/footer.php');	
	
    }


}
