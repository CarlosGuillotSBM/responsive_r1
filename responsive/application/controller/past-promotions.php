<?php

/**
 * Class Games

 */
class PastPromotions extends Controller
{
	
	
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {				
		header('HTTP/1.1 301 Moved Permanently');
		header("Location: /");
		exit;									
    }

	public function action($action, $params = null)
	{
		header('HTTP/1.1 301 Moved Permanently');
		header("Location: /promotions/" . $action);
		exit;										
	}	
	
	
	// called if a sub-action supplied ie:	/{category}/game/mega-moola/
    public function subAction($action,$subAction)
    {
		header('HTTP/1.1 301 Moved Permanently');
		header("Location: /");
		exit;									
			
    }
	
}
