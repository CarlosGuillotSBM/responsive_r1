<?php
/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Preview extends Controller
{

	
	public $games=array();
	public $category;
	public $rs;

	
	
	public function index()
	{
		
		if(!isset($_COOKIE["SessionID"]) ||  $_COOKIE["SessionID"] == '')
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /login/');
			//exit;
		}		
	
	
		if(!$this->cached())
		{						
			$modelContent = $this->loadModel("ContentModel");

			$this->content = $modelContent->getContent('start');

			$this->promotions = $modelContent->getContent('promotions');
			$gamesModel = $this->loadModel("GamesModel");			
/*			
			if(config("SkinID") == 6)
			{	
				// bingo extra games on start page	
				$this->category = $gamesModel->getGames('SLOTS');	
				$this->rs = $gamesModel->rs;
				$this->games = queryDBNextRS($this->rs);
			} else {
*/
				$this->category = $gamesModel->getGames('LOBBY');	
				$this->rs = $gamesModel->rs;
				$this->games = queryDBNextRS($this->rs);	
/*			}
*/
			
		}	
		

		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/start/start.php');
		$this->getFullPage('_partials/common/footer.php');		
	}
	
	public function action($action, $params = null)
	{
	    $fault = true;

	    if (!$params) {

            if ($this->checkInternalIP()) {
                if (isset($_REQUEST["PlayerID"]) && isset($_REQUEST["SessionID"])) {
                    setacookie("showOldSite", false, 0);
                    $_REQUEST["redirectURL"] = $action;
                    $_REQUEST["loginWithSession"] = true;
                    $_REQUEST["username"] = $_REQUEST["PlayerID"];
                    $_REQUEST["password"] = $_REQUEST["SessionID"];
                    parse_str($_SERVER["QUERY_STRING"], $query_array);
                    $_REQUEST["extraActions"] = json_encode($query_array);
                    $this->getFullPageWithParams('_partials/common/head.php', $_REQUEST);
                    $this->getFullPage('_partials/common/header.php');
                    $this->getFullPage('_partials/common/footer.php');
                    $this->getFullPageWithParams('_global-library/partials/preview/main.php', $_REQUEST);
                    $fault = false;

                } else {
                    $_REQUEST["redirectURL"] = $action;
                    $_REQUEST["username"] = config("previewUsername");
                    $_REQUEST["password"] = config("previewPassword");
                    parse_str($_SERVER["QUERY_STRING"], $query_array);
                    $_REQUEST["extraActions"] = json_encode($query_array);
                    $this->getFullPageWithParams('_partials/common/head.php', $_REQUEST);
                    $this->getFullPage('_partials/common/header.php');
                    $this->getFullPage('_partials/common/footer.php');
                    $this->getFullPageWithParams('_global-library/partials/preview/main.php', $_REQUEST);
                    $fault = false;
                }
            }
        }

		if (@$fault) {
		    $this->error404();
		}

	}

	
	
}