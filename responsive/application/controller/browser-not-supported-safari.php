<?php

/**
 * Class BrowserNotSupportedSafari
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class BrowserNotSupportedSafari extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:  /promotions
    public function index()
    {
        $this->getFullPage('_partials/common/head.php');
        if (config("SkinID") === 3){
            $this->getFullPage('_partials/common/browser-not-supported-header.php');
        } else {
            $this->getFullPage('_partials/common/header.php');
        }
        $this->getViewAndCache('_global-library/views/browser-not-supported/browser-not-supported-safari.php');
        $this->getFullPage('_partials/common/footer.php');
    }

    public function action($action, $params = null)
    {
        $this->notFoundPage();
    }
}
