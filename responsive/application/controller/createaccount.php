<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class CreateAccount extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
    	if(config("SkinID") == 5){
			if(!$this->cached())
	        {                                                
	            $this->content = $this->loadModel("ContentModel")->getContent('registration','registration');                                      
	        }	
			$this->action = 'registration';
			// load views. within the views we can echo out $songs and $amount_of_songs easily
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getViewAndCache('_views/register-3step/createaccount.php');
			$this->getFullPage('_partials/common/footer.php');
    	} else {
			goto_error_page();
    	}

	
		
    }

	
	public function action($action, $params = null)
	{
	    if ($params || (config("SkinID") != 5)) {
            goto_error_page();
        } else {
            if(!$this->cached())
			{						
				$this->content = $this->loadModel("ContentModel")->getContent('registration',$action);								
			}
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getViewAndCache('_views/register-3step/register-3step.php');
			$this->getFullPage('_partials/common/footer.php');	    		
    	}
	
	}
}
