<?php


/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Luckyclub extends Controller
{

	
	public $games = array();
	public $category;
	public $rs;
	public $promotions;
	public $community;

	
	
	public function index()
	{
		
		if($_SERVER["REQUEST_URI"] == '/home' || $_SERVER["REQUEST_URI"] == '/home/')
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			exit;
		}
	
	
		if(!$this->cached())
		{
			
			$pageModel = $this->loadModel("ContentModel");									
			$this->content = $pageModel->getContent('Luckyclub');		
			
		}	
				


		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/lucky-club/lucky-club.php');
		$this->getFullPage('_partials/common/footer.php');		
	}
	
	public function action($action, $params = null)
	{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			exit;		
	}
}
