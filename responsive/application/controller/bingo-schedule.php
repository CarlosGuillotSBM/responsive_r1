<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Bingoschedule extends Controller
{
    public function index()
    {
        $this->bingoNotSupportedForIE11GiveBackBingo();
		if(!$this->cached()) {
			$this->content = $this->loadModel("ContentModel")->getContent('bingoschedule');
		}
		
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/bingo-schedule/bingo-schedule.php');		
		$this->getFullPage('_partials/common/footer.php');
    }
	
    public function action($action, $params = null)
    {
        $this->notFoundPage();
    }

    private function bingoNotSupportedForIE11GiveBackBingo()
    {
        $mobileDetect = new Mobile_Detect();
        if (config("SkinID") == Skin::GiveBackBingo && $mobileDetect->version('IE') && $mobileDetect->version('IE', Mobile_Detect::VERSION_TYPE_FLOAT) <= 11) {
            self::redirect("/browser-not-supported-chrome/");
        }
    }
}
