<?php

/**
 * Class Roulette

 */
class livedealer extends Controller
{
    public $row;
	public $games;
	public $rs;
	public $category;

	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {
        // if (config("SkinID") !== Skin::LuckyVIP && config("SkinID") !== Skin::RegalWins) {
        //     $this->notFoundPage();
        // }
        
        $useCache = true;
        if(!$this->cached())
		{
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);
			$gamesModel = $this->loadModel("GamesModel");
			$this->category = $gamesModel->getGames('live-dealer');
			$this->rs = $gamesModel->rs;
			$this->games = queryDBNextRS($this->rs);
            $useCache = !empty($this->games);
		}

        // load views.
		$this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');
        if (file_exists('_views/live-casino/live-casino.php')) {
        	$this->getViewAndCache('_views/live-casino/live-casino.php');
        } else {
        	$this->getViewAndCache('_views/games/games.php', '', $useCache);
        }
		$this->getFullPage('_partials/common/footer.php');

    }

	// called if an action supplied ie:	/{category}/progressive-slots
    public function action($action, $params = null)
    {
        $fault = true;
        if (!$params) {

            $useCache = true;
            if(!$this->cached())
            {
                $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
                $gamesModel = $this->loadModel("GamesModel");
                $this->category = $gamesModel->getGamesByCategoryAndProvider('live-dealer', $action);
                $this->rs = $gamesModel->rs;
                $this->games = queryDBNextRS($this->rs);
                if (empty($this->category)) { // it must be category
                    $this->category = $gamesModel->getGames($action);
                    $this->rs = $gamesModel->rs;
                    $this->games = queryDBNextRS($this->rs);
                }
                $useCache = !empty($this->games);
            }

            // load views.
            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            if (file_exists('_views/live-casino/live-casino.php')) {
                $this->getViewAndCache('_views/live-casino/live-casino.php');
            } else {
                $this->getViewAndCache('_views/games/games.php', '', $useCache);
            }
            $this->getFullPage('_partials/common/footer.php');
            $fault = false;
        }

        if (@$fault) {
            $this->notFoundPage();

        }

    }


}