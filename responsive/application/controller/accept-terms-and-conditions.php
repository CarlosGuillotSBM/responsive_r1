<?php

/**
 * Accept Terms and conditions
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class AcceptTermsandconditions extends Controller
{

    public function index()
    {
		if(!$this->cached()){
			$this->content = $this->loadModel("ContentModel")->getContent("accepttermsandconditions");
		}
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/terms-and-conditions/accept-terms-and-conditions.php');
		$this->getFullPage('_partials/common/footer.php');
    }

    public function action($action, $params = null)
    {
        $this->notFoundPage();
    }

}
