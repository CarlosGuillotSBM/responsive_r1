<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Cashier extends Controller
{
	
	
    // called if the url is clean (no promotion specified) ie:	/promotions
	public function index()
	{
        // check source
        $source = isset($_REQUEST["Source"]) ? $_REQUEST["Source"] : "Cas";
        // check if there is a logged player, if not redirects to home page and ends this script
        if(!isset($_COOKIE["PlayerID"])){
            header("Location: " . "/");
            return;
        }

        // Successful deposit - post to thank you pages
        if(@$_REQUEST["Status"] == 1 && @$_REQUEST["Function"] == "Deposit")
        {
            if (@$_REQUEST["FirstDeposit"] == 1) {
                // TODO: DEV-3541
                if (isset($_COOKIE["AffTrail"]) && $_COOKIE["AffTrail"] != ''){

                    $playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;

                    $params =   array(  "PlayerID"         => array($playerID, "int", 0),     
                                        "AffiliateTrail" => array(@$_COOKIE["AffTrail"], "str", 1000)
                                    );

                    $ok = queryDB($this->db,"DataAffiliateTrail",$params);   
                    if($ok["Code"]==0) {
                        setacookie('AffTrail', null, -1, '/');
                    }          
                }

                setacookie('DepositAmount', @$_REQUEST["Amount"], -1, '/');
                setacookie('DepositCount', 1, -1, '/');

                // redirect page depends on the source
                if ($source == "PRD") {
                    $url = "/cashier/ftd-thank-you/";
                } else {
                    $url = "/cashier/ftd-thank-you-cashier/";
                }

            } else if (@$_REQUEST["DepositSuccessCount"] == 2) {
                $url = "/cashier/deposit-2-thank-you/";
            } else if (@$_REQUEST["DepositSuccessCount"] == 3) {
                $url = "/cashier/deposit-3-thank-you/";
            } else {
                $url = "/cashier/thank-you/";
            }

            echo "<form method='POST' action='" .$url. "' id='cashier_ret_form'>";
            foreach($_REQUEST as $key => $value)
            {
                echo "<input type='hidden' name='".$key."' value='".$value."' />";
            }
            echo '</form><script>document.getElementById("cashier_ret_form").submit();</script>';
            exit;
        }

        // Tokenization failed
        if(@$_REQUEST["Status"] == 95 && @$_REQUEST["Function"] == "Deposit")
        {
            // redirect page to the right location - use GET so the cashier client handles the error
            if ($source == "PRD") {

                // redirect to /welcome/
                header("Location: " . "/welcome/?Status=95");

            } else {

                // redirect to /cashier/
                header("Location: " . "/cashier/?Status=95");

            }

        }

        $this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        			
		$this->getView('_views/cashier/cashier.php');
		$this->getFullPage('_partials/common/footer.php');
	}

	
	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
	public function action($action, $params = null)
	{
	    if ($params) {
            echo "here".$action.'-'.$params[0];
            $this->error404();
        } else {
            switch ($action) {
                case 'ftd-thank-you': // FTD from welcome page
                    $this->getFullPage('_partials/common/head.php');
                    $this->getFullPage('_partials/common/header.php');
                    include	'_trackers/deposit_1.php';
                    $this->getView('_global-library/partials/cashier/thank-you.php');
                    $this->getFullPage('_partials/common/footer.php');
                    break;
                case 'ftd-thank-you-cashier': // FTD from cashier page
                    $this->getFullPage('_partials/common/head.php');
                    $this->getFullPage('_partials/common/header.php');
                    include	'_trackers/deposit_1.php';
                    $this->getView('_global-library/partials/cashier/thank-you.php');
                    $this->getFullPage('_partials/common/footer.php');
                    break;
                case 'deposit-2-thank-you':
                    $this->getFullPage('_partials/common/head.php');
                    $this->getFullPage('_partials/common/header.php');
                    include	'_trackers/deposit_2.php';
                    $this->getView('_global-library/partials/cashier/thank-you.php');
                    $this->getFullPage('_partials/common/footer.php');
                    break;
                case 'deposit-3-thank-you':
                    $this->getFullPage('_partials/common/head.php');
                    $this->getFullPage('_partials/common/header.php');
                    include	'_trackers/deposit_3.php';
                    $this->getView('_global-library/partials/cashier/thank-you.php');
                    $this->getFullPage('_partials/common/footer.php');
                    break;
                case 'thank-you':
                    $this->getFullPage('_partials/common/head.php');
                    $this->getFullPage('_partials/common/header.php');
                    $this->getView('_global-library/partials/cashier/thank-you.php');
                    $this->getFullPage('_partials/common/footer.php');
                    break;

                default:

                    $this->getFullPage('_partials/common/head.php');
                    $this->getFullPage('_partials/common/header.php');
                    $this->getView('_views/cashier/cashier.php');
                    $this->getFullPage('_partials/common/footer.php');

                    break;
            }
        }
	}

}
