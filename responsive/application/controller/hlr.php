<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Hlr extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:  /promotions
    public function index()
    {

        $cogsIPs = array('54.236.208.235','54.236.230.195','184.73.205.69');
        // we just allow Mobivate to access this area
        if (in_array(getPlayerIP(),$cogsIPs))
        {
            $xml = $_REQUEST['xml'];

            require_once("libs/MobivateManager.php");

            $mobivate = new MobivateManager($this->db);
            $mobivate->updateSMSStatus($xml);
        }
    }



}
