<?php
class Extraclub extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
    	if(!$this->cached())
		{
												
			$this->content = $this->loadModel("ContentModel")->getContent('extra-club');							
			
		}
			
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/extra-club/extra-club.php');		
		$this->getFullPage('_partials/common/footer.php');
	
    }


}
