<?php
class BingoRules extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
    	if(!$this->cached())
		{
												
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);							
			
		}
			
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/bingo-rules/bingo-rules.php');		
		$this->getFullPage('_partials/common/footer.php');
	
    }

}
