<?php

/**
 * Class Games

 */
class Uncategorized extends Controller
{
	
	
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {				
		if(config('SkinID') == 3)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header("Location: /");
			exit;									
		}
		else
		{
            $this->error404();
			exit;

		}								
    }

	public function action($action, $params = null)
    {

        if ((config('SkinID') == 3) && (!$params)) {
            header('HTTP/1.1 301 Moved Permanently');
            header("Location: /");
            exit;
        } else {
            $this->error404();
            exit;

        }

    }
	
}
