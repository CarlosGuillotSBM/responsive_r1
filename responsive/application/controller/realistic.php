<?php

/**
 * Class Slots

 */
class Realistic extends Controller
{
    public $row;
	public $games;
	public $rs;
	public $category;
	
	
	
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {
		$useCache = true;
		if(!$this->cached())
		{
			
			$this->openDB();		
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);		
			$this->action = 'slots';

			
			$gamesModel = $this->loadModel("GamesModel");			
			$this->category = $gamesModel->getGames($this->controller);	
			$this->rs = $gamesModel->rs;
			$this->games = queryDBNextRS($this->rs);
			$useCache = !empty($this->games);		
		}
		
		$this->isGameProvider = true;	
		
        // load views. 
		$this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');		
        $this->getViewAndCache('_views/games/games.php', '', $useCache);
		$this->getFullPage('_partials/common/footer.php');
						
    }

	// called if an action supplied ie:	/{category}/progressive-slots
    public function action($action, $params = null)
    {	
		// special case for /{category}/game/
		if($action == 'game')
		{		
			if(!$this->cached())
			{
				$this->openDB();
				$this->content = $this->loadModel("ContentModel")->getContent($this->controller);
			}
			
			// load views. 
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');		
			$this->getViewAndCache('_views/games/game-hub.php');
			$this->getFullPage('_partials/common/footer.php');
			return;
		}
		
		$useCache = true;
		if(!$this->cached())
		{
			$this->openDB();

			$data = $this->controller;
			if($action == 'exclusive') $data .= $action; // a hack so the exclusive slots get their own hub
			
			$this->content = $this->loadModel("ContentModel")->getContent($data);			
			
			$gamesModel = $this->loadModel("GamesModel");			
			$this->category = $gamesModel->getGames($action);									
			$this->rs = $gamesModel->rs;
			$this->games = queryDBNextRS($this->rs);
			$useCache = !empty($this->games);
		}
		
		if($this->category != null || $this->cached())
		{
			// load views. 
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');		
			$this->getViewAndCache('_views/games/games.php', '', $useCache);
			$this->getFullPage('_partials/common/footer.php');
		}
		else
		{
            $this->notFoundPage();
		}		
    }
	
	
	// called if a sub-action supplied ie:	/{category}/game/mega-moola/
    public function subAction($action,$subAction)
    {	
		// redirects for removed games
		switch($subAction)
		{
			case 'monopoly-dream-life':
			case 'monopoly-plus':
			case 'monopoly-here-and-now':
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: /slots/');
				exit;
				break;			
		}


		
		$correctController = true;
		
		if(!$this->cached())
		{			
			$this->row = 	 $this->loadModel("GamesModel")->getGameDetail($subAction);	
			$this->content = $this->loadModel("ContentModel")->getContent('gamesdetail',$subAction);				
			
			// make sure the controller = the gameCategory.URLKey
			if(str_replace('-','',@$this->row['URLKey']) != $this->controller)
			{	
				$correctController = false;
			}
		}
		else
		{
			// make sure the controller = the start of the cache key
			if(substr(str_replace('_','',$this->cache_key),0,strlen($this->controller)) != $this->controller) $correctController = false;
		}
		
		if(($this->row != null || $this->cached()) && $correctController)
		{
			// load views. within the views we can echo out $songs and $amount_of_songs easily
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');        
			$this->getViewAndCache('_views/games/game-detail.php',$action);
			$this->getFullPage('_partials/common/footer.php');		
		}
		else
		{		
            $this->notFoundPage();
		}				
    }
	
}
