<?php

/**
 * Class Games

 */
class PastPromotionsoffline extends Controller
{
	
	
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {				
		header('HTTP/1.1 301 Moved Permanently');
		header("Location: /");
		exit;									
    }

	public function action($action, $params = null)
	{
	    if (!$params) {
	        header('HTTP/1.1 301 Moved Permanently');

            if (config("SkinID") == Skin::KittyBingo) {
                header("Location: /promotions/");
            } else {
                header("Location: /");
            }
            exit;
        } else {
            header('HTTP/1.1 301 Moved Permanently');
            header("Location: /");
            exit;
        }
	}	


	
}
