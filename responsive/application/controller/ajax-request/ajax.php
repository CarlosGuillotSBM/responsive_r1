<?php

/**
 * Class Ajax
 *
 */
abstract class Ajax extends Controller
{

    /**
     * @var
     */
    protected $function = null;

    /**
     * @var
     */
    protected $playerID;

    /**
     * @var
     */
    protected $sessionID;

    /**
     * @var
     */
    protected $errorCode;

    /**
     * @var
     */
    protected $parameters = array();

    /**
     * @var
     */
    protected $rawContent;

    /**
     * @var
     */
    protected $response = array();

    /**
     * @var
     */
    protected $debug;

    public function __construct()
    {
        $this->_initFromHeaders();
    }

    public function initServices()
    {
        $this->openDB();
        if(config("Memcache"))
        {
            $this->initMemcache();
        }
    }

    public function flushCache()
    {
        if (Env::isStaging()) {
            $this->initMemcache();
            return $this->memcache->flush();
        }
        return false;
    }

    private function initMemcache() {
        if (!isset($this->memcache)){
            $this->memcache = new Memcache;
            $this->useMemcache = $this->memcache->connect('127.0.0.1',11210);
        }

    }

    /**
     * Factory method for loading the classes
     *
     * @return Ajax
     */
    public static function createFromFunction()
    {
        $function = @$_REQUEST['f'];
        if (isset($function) && (is_string($function)) && (count_chars($function)) > 0) {
            if(!@include("controller/ajax-request/{$function}.php")){
                throw new InvalidArgumentException("Failed to include '$function.php'; redirect to the old way");
            }
            $class = ucfirst($function);

            return new $class();
        } else {
            throw new InvalidArgumentException("The function is incorrect. Please check you ajax request");
        }
    }

    /**
     * Write the headers of the response
     */
    public function writeHeaders()
    {
        header("Content-Type: application/javascript");
    }

    /**
     * Return to the FrontEnd the processed information, relying on which environment you are
     */
    public function getResponse()
    {
        /*--------------------------------------------------------------------------------------------
            DEBUGGING ON DEV OR RETURN THE RESPONSE
        --------------------------------------------------------------------------------------------*/

        if($this->debug && Env::inState([ Env::dev, Env::local])) {
            $this->_debugResponse();
        } else {
            $this->_liveResponse();
        }
    }

    /**
     * Execute the endpoint
     */
    public function execute() {
        if ($this->validate()) {
            $this->buildParameters();
            $this->getContents();
            $this->prepareResponse();
        }
    }

    /**
     * Validate if the input headers, requests, cookies are valid
     *
     * @return boolean
     */
    protected function validate()
    {
        return true;
    }

    /**
     * Build the array of parameters that has been obtained from the request
     *
     * @return array
     */
    abstract protected function buildParameters();

    /**
     * Get some contents from different source data (ddbb, ...)
     *
     * @return mixed
     */
    abstract protected function getContents();

    /**
     * Prepare an array with the values that are going to be used in the response
     *
     * @return mixed
     */
    abstract protected function prepareResponse();

    /**
     * Init some properties of the class from the headers
     *
     * @return mixed
     */
    protected function _initFromHeaders()
    {
        $this->writeHeaders();
        $this->function =  @$_REQUEST['f'];
        $this->playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;
        $this->sessionID = (isset($_COOKIE["SessionID"])) ? $_COOKIE["SessionID"] : 'NULL';
        $this->debug = (isset($_REQUEST["debug"])) ? $_REQUEST["debug"] : false;
    }

    /**
     * @param $sessionID
     * @return null
     */
    public function checkIfSessionIdNull($sessionID)
    {
        if ($sessionID === 'NULL') {
            $response["Code"] = -1;
            $response["Msg"] = "No session ID sent";
            return $response;
        }
        return null;
    }

    /**
     * Return Debug Information if you are in dev or local environment
     */
    private function _debugResponse() {
        echo "\n";
        echo "Debug Ajax calls";
        echo "\n\n";

        echo "Function: " . $this->function;
        echo "\n\nRequest\n";
        print_r($this->parameters);
        echo "\nSQL\n\n";
        echo 'exec data' . $this->function . ' ' . SdbParams($this->parameters);

        echo "\n\nResponse\n";
        print_r($this->response);
    }

    /**
     * Return the main information because you are in live environment
     */
    private function _liveResponse() {
        //	callback handler added 23/12/14 - MC

        # If a "callback" GET parameter was passed, use its value, otherwise false. Note
        # that "callback" is a defacto standard for the JSONP function name, but it may
        # be called something else, or not implemented at all. For example, Flickr uses
        # "jsonpcallback" for their API.
        $jsonp_callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
        if ($jsonp_callback) {
            echo $jsonp_callback . '(' . json_encode($this->response) . ')';
        } else {
            echo json_encode($this->response);
        }
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param mixed $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return mixed
     */
    public function getRawContent()
    {
        return $this->rawContent;
    }

    /**
     * @param mixed $rawContent
     */
    public function setRawContent($rawContent)
    {
        $this->rawContent = $rawContent;
    }
}
