<?php

/**
* Class Balance
*
*/
class PrizeWheel extends Ajax
{
    
    /**
    * {@inheritDoc}
    */
    protected function buildParameters()
    {
        $this->data = [
            'playerId' => $this->playerID,
            'sessionId' => $this->sessionID,
            
        ];
        $this->payload = json_encode( $this->data );
    }
    
    /**
    * Do a request from the Prize Wheel API
    */
    protected function getContents()
    {   
        $response = $this->doCurl(config("PrizeWheelAPIURL").'game/count/', $this->payload ); 
        $this->rawContent =$response;
    }
    /* This does a CURL call, which will handle exceptions
    can possibly be refactored for multiple uses across calls 
    */
    protected function doCurl($url, $payload)
    {   
        try {
            $ch = curl_init( $url ); 
            /* Catch exceptions on initialization */
            if ($ch === false) {
                throw new Exception('failed to initialize');
            }
            /* Setup request to send json via POST. */
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            /*
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            $certFolder = __DIR__."\\..\\..\\libs\\";
            curl_setopt($ch, CURLOPT_CAINFO, $certFolder . "cacert.pem");
            curl_setopt($ch, CURLOPT_CAPATH, $certFolder);
            /* Return response instead of printing. */
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            /* Send request. */
            $result = curl_exec($ch);
            /* Catch exceptions on request */
            if ($result === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }
            curl_close($ch);   
            /* If we get information from the API, else return 0*/
            if( isset(json_decode($result)->FreeGamesCount)){
                $response = array("Code" => 0 , "Spins" => json_decode($result)->FreeGamesCount);
            }
            else{
                $response = array("Code" => 500 , "Spins" => 0, "Debug" =>  $result);
            }
        }
        catch(Exception $e) {
            /* If we don't get information catch errors */
            $response = array("Code" => 500 , "Spins" => 0, "Message" => 'Curl failed with error '.$e->getCode()." : ".$e->getMessage());
            }
            return $response;
        }
        
        protected function prepareResponse()
        {
            $this->response = $this->rawContent;
        }
    }