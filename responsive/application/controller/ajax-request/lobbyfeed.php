<?php

/**
 * Class Lobbyfeed
 *
 */
class Lobbyfeed extends Ajax
{
    /**
     * Build the array of parameters that has been obtained from the request
     *
     * @return array
     */
    protected function buildParameters()
    {
        $this->parameters = array();
    }

    /**
     * {@inheritDoc}
     */
    protected function getContents()
    {
        $this->rawContent = array();
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareResponse()
    {
        include('libs/lobby_feed.php');
    }

    public function getResponse()
    {
        $this->response = array();
        exit;
    }
}
