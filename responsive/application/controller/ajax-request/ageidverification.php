<?php

/**
* Class Balance
*
*/
class AgeIdVerification extends Ajax
{
    
    /**
    * {@inheritDoc}
    */
    protected function buildParameters()
    {
        $this->data = [
            'playerId' => (float)$this->playerID
        ];
        $this->payload = json_encode( $this->data );
    }
    
    /**
    * Do a request from the Prize Wheel API
    */
    protected function getContents()
    {   
        // Determine what to do based on the request type
        switch($_REQUEST["t"]){
            case "runCheck":
                $response = $this->runAgeIdCheck(); 
                break;

        }
        $this->rawContent = $response;
    }
    /* This does a CURL call, which will handle exceptions
    can possibly be refactored for multiple uses across calls 
    */
    protected function runAgeIdCheck()
    {
    	$params = ["playerId" => @$this->playerID];
        $response = $this->callMicroservice(config("PlayerManagementAPI"), 'verification/rad', $params, 1, 'POST', []);
        var_dump($response);
        // Mock method for DEV
        if(config("env") == "dev"){
            if($this->playerID == 21575){
                $Msg = "<p>Sorry, we have been unable to verify your identity.</p>
                <p>Select 'Continue' to be taken to a secure area where you can quickly and safely upload the following documents:</p>
                <ul>
                <li>Passport and/or Identity card</li>
                <li>AND a utility bill</li>
                </ul>";
                $Title = "Check failed";
                $response = array("Code" => 0, "Msg" => $Msg, "Title"=> $Title, "Status"=>"fail");
            }
            else{
                $Title = "Success";
                $Msg = "<p>Your account has been successfully verified.</p>
                <p> Enjoy your gameplay!</p>";
                $response = array("Code" => 0, "Msg" => $Msg, "Title"=> $Title, "Status"=>"success");
            }
        }
        else{

        }
        return $response;
    }
    /* This does a CURL call, which will handle exceptions
    can possibly be refactored for multiple uses across calls 
    */
    protected function doCurl($url, $payload)
    {   
        try {
            var_dump($payload);
            $ch = curl_init( $url ); 
            /* Catch exceptions on initialization */
            if ($ch === false) {
                throw new Exception('failed to initialize');
            }
            /* Setup request to send json via POST. */
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            /*
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            $certFolder = __DIR__."\\..\\..\\libs\\";
            curl_setopt($ch, CURLOPT_CAINFO, $certFolder . "cacert.pem");
            curl_setopt($ch, CURLOPT_CAPATH, $certFolder);
            /* Return response instead of printing. */
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            /* Send request. */
            $result = curl_exec($ch);
            /* Catch exceptions on request */
            if ($result === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }
            curl_close($ch);   
            /* If we get information from the API, else return 0*/
            var_dump($result);
            if( isset(json_decode($result)->FreeGamesCount)){
                $response = array("Code" => 0 , "Spins" => json_decode($result)->FreeGamesCount);
            }
            else{
                $response = array("Code" => 500 , "Spins" => 0, "Debug" =>  $result);
            }
        }
        catch(Exception $e) {
            /* If we don't get information catch errors */
            $response = array("Code" => 500 , "Spins" => 0, "Message" => 'Curl failed with error '.$e->getCode()." : ".$e->getMessage());
            }
            return $response;
        }

    protected function prepareResponse()
    {
        $this->response = $this->rawContent;
    }
}