<?php

/**
 * Class Logout
 *
 */
class Logout extends Ajax
{

    protected function buildParameters()
    {
        $this->parameters = array("PlayerID"  => array($this->playerID, "int", 0),
                                  "SessionID" => array($this->sessionID, "UI", 0),
                                  "SkinID" 	  => array(config("SkinID"), "int", 0),
                                  "Lang" 	  => array(isset($_COOKIE["LanguageCode"]) ? $_COOKIE["LanguageCode"] : 'en', "str", 2),
                                  "IP"  	  => array(getPlayerIP(), "str", 50)
        );
    }

    protected function getContents()
    {
        $this->openDB();
        $this->rawContent = queryDB($this->db, "DataLogout", $this->parameters);
    }

    protected function prepareResponse()
    {
        setacookie("PlayerID",null,-1,'/');
        setacookie("SessionID",null,-1,'/');
        $this->response = $this->rawContent;
    }
}
