<?php

/**
 * Class Login
 ******* RECAPTCHA CODES
 * R07 - Player marked as a bot
 * R08 - No response from recaptcha API
 * R09 - No recaptcha token received
 *******
 */
class Login extends Ajax
{

    protected $userAgent;

    /**
     * {@inheritDoc}
     */
    protected function _initFromHeaders()
    {
        parent::_initFromHeaders();
        $this->userAgent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "Unknown";
        $this->errorCode = isset($_REQUEST["l"]) ? $_REQUEST["l"] : false;
        $this->afterRegistration = @$_REQUEST["afterRegistration"];
    }

    /**
     * {@inheritDoc}
     */
    protected function validate()
    {
        if (isset($this->errorCode) && ($this->errorCode == 0)) {
            if (!validate("username", @$_REQUEST["u"])) {
                $this->response["Code"] = 1;
                $this->response["Msg"] = "Sorry, your login details (username) are not valid";
                return false;
            }
            @$_REQUEST["p"] = sanitizeValue(@$_REQUEST["p"]);
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    protected function buildParameters()
    {
        $this->recaptchaToken = @$_REQUEST["t"] ?? "N/A";
        $this->parameters =  array(
            "Username"              => array(@$_REQUEST["u"], "str", 15),
            "Password"              => array(@$_REQUEST["p"], "str", 15),
            "SkinID"                => array(config("SkinID"), "int", 0),
            "DeviceCategoryID"      => array(config("RealDevice"), "int", 0),
            "UserAgent"             => array($this->userAgent, "str", 500),
            "Lang"                  => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                    => array(getPlayerIP(), "str", 50),
            "ScreenSize"            => array(@$_REQUEST["s"], "str", 15),
            "RedirectURL"           => array(@$_REQUEST["r"], "str", 15)
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function getContents()
    {
        $ignoreIPs = ["197.224.132.172"];
        if (strpos($this->parameters["Username"][0], '@') !== false) {
            $j_st = json_encode($this->parameters);
            $j_st = str_replace('"Password":["' . $this->parameters["Password"][0] . '","str",15]', '"Password":["xxx","str",15]', $j_st);
            $theFileData = date("H:i:s") . " " . $j_st;
            $fp = fopen("L:/agsLogs/DataLogin/" . date("Ymd") . ".log", "a");
            fwrite($fp, $theFileData . "\r\n");
            fclose($fp);
        }
        // If we are logging in from register
        if ($this->afterRegistration) {
            $this->tryDBForLogin();
        }
        // If we are logging in from login or modal
        else {
            if (in_array(getPlayerIP(), $ignoreIPs) || $this->checkIfInternalIP()) {
                $this->tryDBForLogin();
            } else {
                if ($this->doRecaptchaCheck()) {
                    $this->tryDBForLogin();
                }
            }
        }
    }

    protected function tryDBForLogin()
    {
        if (strpos($this->parameters["Username"][0], '@') !== false) {
            $j_st = json_encode($this->parameters);
            $j_st = str_replace('"Password":["' . $this->parameters["Password"][0] . '","str",15]', '"Password":["xxx","str",15]', $j_st);
            $theFileData = date("H:i:s") . " " . $j_st;
            $fp = fopen("L:/agsLogs/DataLogin/" . date("Ymd") . ".log", "a");
            fwrite($fp, $theFileData . "\r\n");
            fclose($fp);
        }

        $this->openDB();
        if (isset($this->errorCode) && ($this->errorCode == 1)) {
            $this->rawContent = queryDB($this->db, "DataLoginWithSession", $this->parameters, false, false, $rs);
        } else {
            $this->rawContent = queryDB($this->db, "DataLogin", $this->parameters, false, false, $rs);
        }
    }

    protected function doRecaptchaCheck()
    {
        if ($this->recaptchaToken != "N/A") {
            $recaptcha_url = config("RecaptchaURL");
            $recaptcha_secret = config("recaptcha3SecretKey");
            // Make and decode POST request:
            $recaptcha_res = $this->simpleRESTRequest("POST", $recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $this->recaptchaToken);
            $recaptcha = $recaptcha_res["body"];

            // Take action based on the score returned:
            if ($recaptcha->success == true) {
                // The player is most likely to be a human
                if ($recaptcha->score >= 0.5) {
                    return true;
                }
                // The player is most likely to be a filthy bot';
                else {
                    $this->logToFile("recaptcha", @$_REQUEST["u"] . "\t" . $recaptcha->score . "\t");
                    if (config("SkinID") == "12") {
                        $this->rawContent = [
                            'Code' => 1,
                            'Msg' => 'Something went wrong. If the issue persists, please contact support and quote code "R07"'
                        ];
                        return false;
                    } else {
                        return true;
                    }
                }
            }
            // Error calling recaptcha API
            else {
                return true;
                /*
                $this->rawContent = [
                    'Code' => 1,
                    'Msg' => 'Something went wrong. If the issue persists, please contact support and quote code "R08"'
                ];
                */
            }
        }
        return true;
    }

    protected function checkIfInternalIP()
    {
        $playerIP = getPlayerIP();
        // Local development
        if ($playerIP === "127.0.0.1") {
            return true;
        }
        // Internal Mauritius IPs
        if (strpos($playerIP, '10.10.12.') !== false) {
            return true;
        }
        // Internal UK IPs
        if (strpos($playerIP, '192.168.200.') !== false) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareResponse()
    {
        // default API failure message  
        $response = [
            'Code' => -1,
            'Msg' => 'Please try again'
        ];

        if (isset($this->rawContent)) {
            $code = isset($this->rawContent["Code"]) ? $this->rawContent["Code"] : -1;

            $eventLog = array();

            if ($code == 0 || $code == 9 || $code == 10001) {
                // send response
                $response = [
                    "Code" => $code,
                    "Msg" => $this->rawContent["Msg"],
                    "PlayerID" => $this->rawContent["PlayerID"],
                    "SessionID" => $this->rawContent["SessionID"],
                    "Username" => $this->rawContent["Username"],
                    "LastLoginDate" => $this->rawContent["LastLoginDate"],
                    "Firstname" => $this->rawContent["Firstname"],
                    "Class" => $this->rawContent["Class"],
                    "Balance" => $this->rawContent["Balance"],
                    "Real" => $this->rawContent["Real"],
                    "Bonus" => $this->rawContent["Bonus"],
                    "BonusWins" => $this->rawContent["BonusWins"],
                    "Points" => $this->rawContent["Points"],
                    "Funded" => $this->rawContent["Funded"],
                    "DepositCount" => $this->rawContent["DepositCount"],
                    "CurrencyCode" => $this->rawContent["CurrencyCode"],
                    "MobileVerified" => $this->rawContent["MobileVerified"],
                    "LimitConfirmation" => $this->rawContent["LimitConfirmation"],
                    "LimitConfirmationMessage" => $this->rawContent["LimitConfirmationMessage"],
                    "RedirectURL" => $this->rawContent["RedirectURL"],
                    "AffID" => $this->rawContent["AffID"],
                    "AffiliateID" => $this->rawContent["AffiliateID"],
                    "RegistrationDate" => $this->rawContent["RegistrationDate"],
                    "Avatar" => $this->rawContent["Avatar"],
                    "Email" => $this->rawContent["Email"],
                    "LastName" => $this->rawContent["LastName"],
                    "DOB" => $this->rawContent["DOB"],
                    "Postcode" => $this->rawContent["Postcode"],
                    "Verified" => $this->rawContent["VerificationStatus"],
                    "AspersCustomerNumber" => $this->rawContent["AspersCustomerNumber"],
                    "IPAddress" => $this->rawContent["IPAddress"],
                    "CountryCode" => $this->rawContent["CountryCode"],
                    "URUVerificationStatus" => $this->rawContent["URUVerificationStatus"]
                ];
                if (config("LCCPEnabled") == 0) {
                    $response["URUVerificationStatus"] = 1;
                }

                if ($code == 0) {
                    // START DISABLE MICROSERVICE CALLS (PLAYER MANAGEMENT API)
                    if ($this->hasPlayerSrPopUp($this->rawContent["PlayerID"])) {
                        $response["SessionID"] = "";
                        $response["Code"] = 10000;
                    }
                    // player needs to accept terms and conditions?
                    if ($this->hasPlayerTCsPopUp($this->rawContent["PlayerID"])) {
                        $response["SessionID"] = "";
                        $response["Code"] = 10001;
                        $response["ts"] = $this->rawContent["SessionID"];
                    }
                    // END DISABLE MICROSERVICE CALLS (PLAYER MANAGEMENT API)
                    // Is this an Aspers Player?
                    if ($response["IPAddress"] == "AspersOffline") {
                        $response["SessionID"] = "";
                        $response["Code"] = 10002;
                        $response["ts"] = $this->rawContent["SessionID"];
                        $response["tpID"] = $this->rawContent["PlayerID"];
                    }
                    if ($response["URUVerificationStatus"] != 1) {
                        $response["SessionID"] = $this->rawContent["SessionID"];
                        $response["Code"] = 10003;
                    }
                }

                setacookie("PlayerID", $response["PlayerID"], 0, '/');
                setacookie("SessionID", $response["SessionID"], 0, '/');
                setacookie("Class", $response["Class"], 0, '/');
                setacookie("Verified", $response["Verified"], 0, '/');

                if (intval(@$response["AffID"]) > 0) setacookie("AffID", $response["AffID"], 0, '/');
                if (intval(@$response["AffiliateID"]) > 0) setacookie("AffiliateID", $response["AffiliateID"], 0, '/');

                $response["Favourites"] = queryDBNextRS($rs);
                $response["GamingData"] = queryDBNextRS($rs);
            } else {
                if ($code == 3) {
                    sendLockedUserMail($this->rawContent);
                }
                $response["Code"] = $code;
                $response["Msg"] = $this->rawContent["Msg"];
                $response["SelfExclude"] = @$this->rawContent["SelfExclude"];
            }


            // START DISABLE MICROSERVICE CALLS
            if (!in_array($code, [1, 7, 8, 10])) { //different from bad login

                $gamstopResp = $this->callGamstopLogin(
                    $this->rawContent["PlayerID"],
                    $this->rawContent["Firstname"],
                    $this->rawContent["LastName"],
                    $this->rawContent["Email"],
                    $this->rawContent["DOB"],
                    $this->rawContent["Postcode"]
                );


                if ($gamstopResp['httpStatus'] == 200) {
                    if (!$gamstopResp['body']['allowOperation']) {
                        if ($code == 0 || $code == 9) {
                            $response = [
                                "Code" => 1001
                            ];
                        }
                        $response["Msg"] = str_replace("%%supportUrl%%", config("supportPage"), $gamstopResp['body']['messageToDisplay']);
                    } //setup request to send json via POST


                    //                    $eventLog= array(
                    //                        'X-Exclusion'  => $gamstopResp['body']['gamStopStatus'],
                    //                        'X-Match-Type' => $gamstopResp['body']['matchType'],
                    //                        'X-Unique-Id'  => $gamstopResp['body']['referenceId']
                    //                    );
                }



                //We'll send the event log to CS with the response from
                //$payload = json_encode($eventLog, JSON_FORCE_OBJECT);

                $body = [
                    'thirdPartyRequestTypeId' => 1, // this is hardcoded to 1, it means
                    'response' => $eventLog,
                    'request' => $gamstopResp["request"]
                ];

                // if the registration didn't go well we won't have playerId
                if (isset($response["PlayerID"])) {
                    $body["playerId"] = $response["PlayerID"];
                }
                if (isset($this->rawContent["PlayerID"])) {
                    $body["response"]["playerId"] = $this->rawContent["PlayerID"];
                    $body["playerId"] = $this->rawContent["PlayerID"];
                }

                // if for some reason we may not have regSession in the request, we add it otherwise
                if (isset($this->rawContent["SessionID"])) {
                    $body["response"]["loginSession"] = $this->rawContent["SessionID"];
                    $body["loginSession"] = $this->rawContent["SessionID"];
                }
            }
            // END DISABLE MICROSERVICE CALLS (GAMSTOP)

            $this->response = $response;
        }
        // END DISABLE MICROSERVICE CALLS (GAMSTOP)

        $this->response = $response;
    }


    private function callGamstopLogin($playerID, $firstName, $lastName, $email, $dateOfBirth, $postcode)
    {

        $body = [
            'firstName' => $firstName,
            'lastName' => $lastName,
            'dateOfBirth' => $dateOfBirth,
            'postcode' => $postcode,
            'email' => $email,
            'playerId' => $playerID,
        ];

        $res = $this->callMicroservice(config("GamStopAPI"), 'login', [], 0, 'POST', $body);
        $res["request"] = $body;

        return $res;
    }


    private function hasPlayerSrPopUp($playerID)
    {

        try {

            $res = $this->callMicroservice(config("PlayerManagementAPI"), 'SocialGamingResponsibility', ['playerId' => $playerID], 1, 'GET');
            return $res["httpStatus"] == 200 && @$res["body"] !== null;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    private function hasPlayerTCsPopUp($playerID)
    {

        try {

            $params = [
                'playerId' => $playerID,
                'skinId' => config("SkinID")
            ];
            $res = $this->callMicroservice(config("PlayerManagementAPI"), 'contents/non-accepted', $params, 1, 'GET', []);

            return $res["httpStatus"] == 200 && count($res["body"]) > 0;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
