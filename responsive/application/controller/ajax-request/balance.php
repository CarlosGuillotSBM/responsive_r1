<?php

/**
 * Class Balance
 *
 */
class Balance extends Ajax
{

    /**
     * {@inheritDoc}
     */
    protected function buildParameters()
    {
        $this->parameters = array( "PlayerID"  => array($this->playerID, "int", 0),
                                   "SessionID" => array($this->sessionID, "UI", 0),
                                   "Lang" 	   => array(@$_COOKIE["LanguageCode"], "str", 2),
                                   "IP"        => array(getPlayerIP(), "str", 50)
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function getContents()
    {
        $this->openDB();
        $this->rawContent = queryDB($this->db,"DataGetPlayerBalance",$this->parameters);
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareResponse()
    {
        if(@$this->rawContent['Code'] != 0)
        {
            if (isset($_COOKIE['SessionID']))
            {
                unset($_COOKIE['SessionID']);
                setacookie('SessionID', null, -1, '/');
            }
            if (isset($_COOKIE['PlayerID']))
            {
                unset($_COOKIE['PlayerID']);
                setacookie('PlayerID', null, -1, '/');
            }
        }
        $this->response = $this->rawContent;
    }
}
