<?php

include_once "repository/ParameterValidator.php";

/**
 * Class Balance
 *
 */
class Leaderboardbycompetition extends Ajax
{

    /**
     * {@inheritDoc}
     */
    protected function buildParameters()
    {
        ParameterValidator::isNumeric($_REQUEST["cId"]);
        $this->parameters =  (isset($_REQUEST["cId"])) ? (int) $_REQUEST["cId"] : 0;
    }

    /**
     * {@inheritDoc}
     */
    protected function getContents()
    {
        $this->openDB();
        $this->rawContent = $this->loadRepository("Competition")->fetchAllWithPoints(config("SkinID"), $this->parameters);
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareResponse()
    {
        $this->response["Leaderboard"] = $this->prepareLeaderBoard($this->rawContent, $this->playerID);
        $this->response["NumOfPlayers"] = count($this->rawContent);
    }

    /**
     * Function to prepare the data in order to send them to the view
     * for the leaderboardbycompetitionId function
     *
     * @param $rows
     * @param null $playerID
     * @return array
     */
    private function prepareLeaderBoard($rows, $playerID = null)
    {
        $playerRow = array();
        $playerRow["Position"] = 0;
        foreach($rows as $row) {
            if ($row["PlayerID"] == $playerID) {
                $playerRow = $row;
            }
        }
        if (count($rows) > 5 && $playerRow["Position"] >= 5) {
            $rows[4] = $playerRow;
        }

        return array_slice($rows, 0, 5);
    }
}
