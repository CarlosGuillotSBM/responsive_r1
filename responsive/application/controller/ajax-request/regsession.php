<?php

/**
 * Class Balance
 *
 */
class Regsession extends Ajax
{
    private $user_agent2;
    private $ip2;
    private $ipID2;
    public $rs;

    /**
     * {@inheritDoc}
     */
    protected function _initFromHeaders()
    {
        parent::_initFromHeaders();
        $this->ip2 = getPlayerIP();
        $this->ipID2 =
            substr($this->ip2, 0, 3) > 127 ? (ip2long($this->ip2) & 0x7fffffff) + 0x80000000 : ip2long($this->ip2);
        $this->user_agent2 = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Unknown';
    }

    /**
     * {@inheritDoc}
     */
    protected function buildParameters()
    {
        $this->parameters = array(
            'SkinID' => array(config('SkinID'), 'int', 0),
            'IP' => array($this->ip2, 'str', 15),
            'Lang' => array(@$_COOKIE['LanguageCode'], 'str', 2),
            'CountryIPID' => array($this->ipID2, 'int', 0),
            'DeviceCategoryID' => array(config('Device'), 'int', 0),
            'UserAgent' => array($this->user_agent2, 'str', 500)
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function getContents()
    {
        // db data

        $this->openDB();
        $this->row = queryDB($this->db, 'DataRegSession', $this->parameters, false, false, $this->rs);

        // tc's and privacy policy
        $this->row['tcs'] = $this->getTcsContents();

        // add all data to response
        $this->rawContent = addRowToResponse($this->row);
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareResponse()
    {
        if ($this->row['Code'] != 0) {
            $this->rawContent['Msg'] = $this->row['Msg'];
        } else {
            $this->rawContent['Currencies'] = queryDBNextRS($this->rs);
            $this->rawContent['Countries'] = queryDBNextRS($this->rs);
        }
        $this->response = $this->rawContent;
    }

    /**
     * @return mixed
     */
    public function getUserAgent2()
    {
        return $this->user_agent2;
    }

    /**
     * @param mixed $user_agent2
     */
    public function setUserAgent2($user_agent2)
    {
        $this->user_agent2 = $user_agent2;
    }

    /**
     * @return mixed
     */
    public function getIp2()
    {
        return $this->ip2;
    }

    /**
     * @param mixed $ip2
     */
    public function setIp2($ip2)
    {
        $this->ip2 = $ip2;
    }

    /**
     * @return mixed
     */
    public function getIpID2()
    {
        return $this->ipID2;
    }

    /**
     * @param mixed $ipID2
     */
    public function setIpID2($ipID2)
    {
        $this->ipID2 = $ipID2;
    }

    /**
     * @return mixed
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * @param mixed $row
     */
    public function setRow($row)
    {
        $this->row = $row;
    }

    /**
     * @return mixed
     */
    public function getRs()
    {
        return $this->rs;
    }

    /**
     * @param mixed $rs
     */
    public function setRs($rs)
    {
        $this->rs = $rs;
    }

    private function getTcsContents()
    {
        try {
            $params = [
                'skinId' => config('SkinID')
            ];

            $terms = $this->callMicroservice(config('PlayerManagementAPI'), 'contents/latest', $params, 1, 'GET', []);

            // loop through the array
            $tcs = [];
            foreach ($terms['body'] as $term) {
                if (isset($term['id'])) {
                    $tcs[] = $term['id'];
                }
            }

            return join(',', $tcs);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }

        return '';
    }
}
