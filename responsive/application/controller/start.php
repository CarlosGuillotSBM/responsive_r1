<?php
/**
 * Class Start
 */
class Start extends Controller
{
 
    public $games=array();
    public $category;
    public $rs;
    public $community;
 
    public function index()
    {
        
        if(!isset($_COOKIE["SessionID"]) ||  $_COOKIE["SessionID"] == '')
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /login/');
            //exit;
        }        
 
        //if(!$this->cached())
        //{                        
            $modelContent = $this->loadModel("ContentModel");
            $this->content = $modelContent->getContent('start');
            //$this->promotions = $modelContent->filterOutPromotionsByPlayerId($this->promotions, $this->getPlayerId());
            $this->promotions = $modelContent->getContent('promotions','',0); // 0 don't get expired promos
            $this->promotions['promotions'] = $modelContent->filterOutPromotionsByPlayerId($this->promotions['promotions'], $this->getPlayerId());
            $gamesModel = $this->loadModel("GamesModel");
            $this->community = $modelContent->getContentForCommunity();
            //$this->community = $modelContent->getContent('community');
/*            
            if(config("SkinID") == 6)
            {    
                // bingo extra games on start page    
                $this->category = $gameasModel->getGames('SLOTS');    
                $this->rs = $gamesModel->rs;
                $this->games = queryDBNextRS($this->rs);
            } else {
*/
            $this->category = $gamesModel->getGames('LOBBY');
            $this->rs = $gamesModel->rs;
            $this->games = queryDBNextRS($this->rs);
/*            }
*/
            
        //}
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');
        $this->getViewAndCache('_views/start/start.php');
        $this->getFullPage('_partials/common/footer.php');        
    }
}