<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Blog extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
	
		header('Location: /community/blog/');
		exit;
	
	
	
	
		// if(!$this->cached())
		// {        
			// $blogModel = $this->loadModel("BlogModel");        
			// $this->data = unserialize($blogModel->getPosts());
		// }
		
		// load views. within the views we can echo out $songs and $amount_of_songs easily
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/blog/blog.php');
		$this->getFullPage('_partials/common/footer.php');
    }

	
	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {
		header('Location: /community/blog/');
		exit;

    	$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/blog/post.php');
		$this->getFullPage('_partials/common/footer.php');
		
    }


}
