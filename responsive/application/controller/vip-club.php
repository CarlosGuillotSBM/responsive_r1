<?php


/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class VipClub extends Controller
{
	
	public function index()
	{
		if(!$this->cached())
		{
			$pageModel = $this->loadModel("ContentModel");									
			$this->content = $pageModel->getContent('Vipclub');
		}

		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/vip-club/vip-club.php');
		$this->getFullPage('_partials/common/footer.php');		
	}
	
	public function action($action, $params = null)
	{
        $this->redirect("/");
	}

}
