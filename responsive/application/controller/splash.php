<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class splash extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
		if(!$this->cached())
		{
												
			$this->content = $this->loadModel("ContentModel")->getContent('splash');							
			
		}
		
		$this->getFullPage('_partials/common/head.php');
		// $this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/splash/splash.php');		
		$this->getFullPage('_partials/common/footer.php');
	
    }

	
    public function action($action, $params = null) {
		header("HTTP/1.0 404 Not Found");
		$this->getFullPage('_partials/common/head.php');
		// $this->getFullPage('_partials/common/header.php');
		$this->getView('_views/error/error.php');
		$this->getFullPage('_partials/common/footer.php');		
    }

}
