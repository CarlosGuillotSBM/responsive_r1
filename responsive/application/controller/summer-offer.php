<?php

/**
 * Class Games

 */
class Summeroffer extends Controller
{
	
	
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {				
		if(config('SkinID') == 3)
		{
			header('HTTP/1.1 301 Moved Permanently');
			header("Location: /media/exclusive/summer-offer/");
			exit;									
		}
		else
		{
            $this->error404();
			exit;

		}								
    }
	
}
