<?php

/**
 * Class Slots

 */
class Slots extends Controller
{
    public $row;
    public $games;
    public $rs;
    public $category;
    public $gamesCategory;

    // called if the url is clean (no game specified) ie:	/games
    public function index()
    {

        // weird hack to know if Player opted to go to account history from a NETENT game
        if (isset($_REQUEST["reason"]) && isset($_REQUEST["gameId"]) && isset($_REQUEST["sessId"])) {
            if ($_REQUEST["reason"] == 10){
                header("Location: " . config("accountActivityURL"));
            }
        }

        $useCache = true;

        if(!$this->cached())
        {

            $this->openDB();
            $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
            $this->action = 'slots';

            $gamesModel = $this->loadModel("GamesModel");
            $this->category = $gamesModel->getGames('slots');
            $this->rs = $gamesModel->rs;
            $this->games = queryDBNextRS($this->rs);

            $useCache = !empty($this->games);

            // log when array games is empty

            if (empty($this->games)){
                // check if memcache flag exist, so we have to delete
                if(config("Memcache") && $this->memcache->get('games_started_failing')== null){
                    $date = new DateTime();
                    $date = $date->format("Y/m/d h:i:s");
                    $this->memcache->set('games_started_failing', $date);
                    $this->recordsetGamesHandler("Games started failing at: " . $date);
                 }
            } else{
                if(config("Memcache") && $this->memcache->get('games_started_failing')){
                    $date = new DateTime();
                    $date = $date->format("Y/m/d h:i:s");
                    $this->recordsetGamesHandler("Games recovered at: " . $date);

                    //deleting memcache flag
                    $this->memcache->delete('games_started_failing');
                }
            }

        } else{

            $this->gamesCategory = $this->getGamesFromCache('slots');
            $this->games =  $this->gamesCategory;
        }



        // load views.
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');

        // if there is a specific slots view include it otherwise include games view
        if (file_exists('_views/slots/slots.php')) {
            $this->getViewAndCache('_views/slots/slots.php', '', $useCache);
        } else {
            $this->getViewAndCache('_views/games/games.php', '', $useCache);
        }
        $this->getFullPage('_partials/common/footer.php');

    }

    // called if an action supplied ie:	/{category}/progressive-slots
    public function action($action, $params = null)
    {
        if( count($params) > 1 ){
            return $this->error404();
        } 
 

        if ($params) {
             // redirects for removed games
            switch($params[0])
            {
                case 'monopoly-dream-life':
                case 'monopoly-plus':
                case 'monopoly-here-and-now':
                    header('HTTP/1.1 301 Moved Permanently');
                    header('Location: /slots/');
                    exit;
                    break;
            }


            $correctController = true;

            if(!$this->cached())
            {
                $this->row = 	 $this->loadModel("GamesModel")->getGameDetail($params[0]);
                $this->content = $this->loadModel("ContentModel")->getContent('gamesdetail',$params[0]);


                // make sure the controller = the gameCategory.URLKey
                if(str_replace('-','',@$this->row['URLKey']) != $this->controller)
                {
                    $correctController = false;
                }
            }
            else
            {
                // make sure the controller = the start of the cache key
                if(substr(str_replace('_','',$this->cache_key),0,strlen($this->controller)) != $this->controller) $correctController = false;
            }

            if(($this->row != null || $this->cached()) && $correctController)
            {
                // load views. within the views we can echo out $songs and $amount_of_songs easily
                $this->getFullPage('_partials/common/head.php');
                $this->getFullPage('_partials/common/header.php');
                $this->getViewAndCache('_views/games/game-detail.php',$action);
                $this->getFullPage('_partials/common/footer.php');
            }
        } else {

            // luckypants transfer SEO redirects
            if(config('SkinID') == 3)
            {
                $loc = '';
                switch($action)
                {
                    case 'cash-spin': 			$loc = 'slots/'; break;
                    case 'cleopatra-slots': 	$loc = 'slots/game/cleopatra/'; break;
                    case 'davinci-diamonds':	$loc = 'slots/game/da-vinci-diamonds/'; break;
                    case 'enchanted-prince':	$loc = 'slots/game/enchanted-prince/'; break;
                    case 'fluffy-favs-slots':	$loc = 'slots/game/fluffy-favourites/'; break;
                    case 'golden-goddess':		$loc = 'slots/game/golden-goddess/'; break;
                    case 'grease-1-slots':		$loc = 'slots/game/grease-1/'; break;
                    case 'grease-danny-sandy':	$loc = 'slots/game/grease-danny-and-sandy/'; break;
                    case 'grease-ladies-birds':	$loc = 'slots/game/grease-pink-ladies-and-t-birds/'; break;
                    case 'irish-luck-slots':	$loc = 'slots/game/irish-luck/'; break;
                    case 'jazz-cat':			$loc = 'slots/game/jazz-cat/'; break;
                    case 'lady-luck-deluxe':	$loc = 'slots/game/lady-luck-deluxe/'; break;
                    case 'photo-safari':		$loc = 'slots/game/photo-safari/'; break;
                    case 'piggy-payout':		$loc = 'slots/game/piggy-payout/'; break;
                    case 'pirate-princess-slots':		$loc = 'slots/game/pirate-princess/'; break;
                    case 'pixies-of-forest':	$loc = 'slots/game/pixies-of-the-forest/'; break;
                    case 'secret-garden-slots':	$loc = 'slots/game/secret-garden/'; break;
                    case 'shaman-dream-slots':	$loc = 'slots/game/shamans-dream/'; break;
                    case 'shopping-spree-slots':		$loc = 'slots/game/shopping-spree/'; break;
                    case 'siberian-storm':		$loc = 'slots/game/siberian-storm/'; break;
                    case 'temple-of-isis':		$loc = 'slots/game/temple-of-isis/'; break;
                    case 'thunderstruck-2-slots':		$loc = 'slots/game/thunderstruck-2/'; break;
                    case 'white-wizard':		$loc = 'slots/game/white-wizard/'; break;


                }

                if($loc != '')
                {
                    header('HTTP/1.1 301 Moved Permanently');
                    header("Location: /" . $loc);
                    exit;
                }

            }


            /*
              R10-1135 : regal wins - doubled category pages
              regal wins - doubled category pages
            */

            if (config('SkinID') == 10){
                $loc = '';
                switch($action) {
                    case 'scratch-and-arcade'  :   $loc = 'scratch-and-arcade'; break;
                    case 'sports'              :   $loc = 'sports'; break;
                    case 'table-card'          :   $loc = 'table-card'; break;
                    case 'roulette'            :   $loc = 'roulette'; break;
                    case 'roulette-games'      :   $loc = 'roulette-games'; break;
                    case 'table-games'         :   $loc = 'table-games'; break;
                    case 'live-dealer'         :   $loc = 'live-dealer'; break;
                    case 'live-casino'         :   $loc = 'live-casino'; break;
                }

                if($loc != '')
                {
                    header('HTTP/1.1 301 Moved Permanently');
                    header("Location: /" . $loc);
                    exit;
                }
            }


            $useCache = true;
            if(!$this->cached()) {

                $this->openDB();

                $data = $this->controller;
                // Removed due to fact we need to add banner separately from the rest of slots subpages
                // Also, hub has been removed too
                // 01/06/2018
                if($action == 'exclusive' || $action == 'exclusive-games') $data .= $action; // a hack so the exclusive slots get their own hub


                $this->content = $this->loadModel("ContentModel")->getContent($data);

                $gamesModel = $this->loadModel("GamesModel");
                $this->category = $gamesModel->getGames($action);
                $this->rs = $gamesModel->rs;


                $this->games = queryDBNextRS($this->rs);

                if( is_array($this->games) and count($this->games ) > 0){
                    $useCache = !empty($this->games);
                }else{

                    return $this->error404();
                }



            }

            if($this->category != null || $this->cached()) {
                // load views.
                $this->getFullPage('_partials/common/head.php');
                $this->getFullPage('_partials/common/header.php');
                if (file_exists('_views/slots/slots.php')) {
                    $this->getViewAndCache('_views/slots/slots.php', '', $useCache);
                } else {
                    $this->getViewAndCache('_views/games/games.php', '', $useCache);
                }
                $this->getFullPage('_partials/common/footer.php');
            }
        }
    }

}