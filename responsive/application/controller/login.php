<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Login extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
		if(!$this->cached())
		{						
			$this->content = $this->loadModel("ContentModel")->getContent('login','login');								
		}	
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getView('_views/login/login.php');
		$this->getFullPage('_partials/common/footer.php');	
    }

       // fix the login sub pages problem
    public function action($action, $params = null)
    {
		if(!$this->cached())
		{						
			$this->content = $this->loadModel("ContentModel")->getContent('login',$action);								
		}	
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getView('_views/login/login.php');
		$this->getFullPage('_partials/common/footer.php');	
    }
}
