<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Register extends Controller
{
    

    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
		if(!$this->cached())
        {                                                
            $this->content = $this->loadModel("ContentModel")->getContent('registration','registration');                                      
        }	
		$this->action = 'registration';
		
		//R10-414 when aspers go to the url 'register?terms=1' this will display the tcs and privacy policy in a json file
		if (isset($_REQUEST["terms"])) {
			
			if (!isset($this->content)) {
				$this->content = $this->loadModel("ContentModel")->getContent('registration','registration');
			}
			
			header('Content-Type: application/json');
			$termsContent= new stdClass();
			$termsContent->tcContent = ($this->content['4step-tcs-popup'][0]['Body']);
			$termsContent->tcID = ($this->content['4step-tcs-popup'][0]['ID']);
			$termsContent->ppContent = ($this->content['4step-privacy-popup'][0]['Body']);
			$termsContent->ppID = ($this->content['4step-privacy-popup'][0]['ID']);
			
			$ppJSON = json_encode($termsContent);
			echo($ppJSON);
			exit();
		}


		// load views. within the views we can echo out $songs and $amount_of_songs easily
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_global-library/partials/common/register-header.php');
		$this->getViewAndCache('_views/join-now/join-now.php');
		$this->getFullPage('_partials/common/footer.php');	
    }

	
	public function action($action, $params = null)
	{
	    $fault = true;
	    if (!$params) {
            if (!$this->cached()) {
                $this->content = $this->loadModel("ContentModel")->getContent('registration', $action);
            }
            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_global-library/partials/common/register-header.php');
            $this->getViewAndCache('_views/join-now/join-now.php');
            $this->getFullPage('_partials/common/footer.php');
            $fault = false;
        }
        if (@$fault) {
            $this->error404();
        }
	}

	


}
