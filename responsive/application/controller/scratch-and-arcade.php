<?php

/**
 * Class Blackjack

 */
class Scratchandarcade extends Controller
{
    public $row;
	public $games;
	public $rs;
	public $category;
	
	
	
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {
        if(config('SkinID') == Skin::GiveBackBingo && config('SkinID') == Skin::LuckyVIP && config('SkinID') == Skin::RegalWins) {
            $this->notFoundPage();
        } else {

            $useCache = true;
			if(!$this->cached())
			{						
				$this->content = $this->loadModel("ContentModel")->getContent($this->controller);		
				$gamesModel = $this->loadModel("GamesModel");			
				$this->category = $gamesModel->getGames('scratch-and-arcade');	
				$this->rs = $gamesModel->rs;
				$this->games = queryDBNextRS($this->rs);	
				$useCache = !empty($this->games);		
			}
					
	        // load views. 
			$this->getFullPage('_partials/common/head.php');
	        $this->getFullPage('_partials/common/header.php');		
	        $this->getViewAndCache('_views/games/games.php', '', $useCache);
			$this->getFullPage('_partials/common/footer.php');
		}

    }

	// called if an action supplied ie:	/{category}/progressive-slots
    public function action($action, $params = null)
    {
        $fault = true;
        if ($params) {
            $correctController = true;

            if(!$this->cached())
            {
                $this->row = 	 $this->loadModel("GamesModel")->getGameDetail($params[0]);
                $this->content = $this->loadModel("ContentModel")->getContent('gamesdetail',$params[0]);

                // make sure the controller = the gameCategory.URLKey
                if(str_replace('-','',@$this->row['URLKey']) != $this->controller)
                {
                    $correctController = false;
                }
            }
            else
            {
                // make sure the controller = the start of the cache key
                if(substr(str_replace('_','',$this->cache_key),0,strlen($this->controller)) != $this->controller) $correctController = false;
            }

            if(($this->row != null || $this->cached()) && $correctController)
            {
                // load views. within the views we can echo out $songs and $amount_of_songs easily
                $this->getFullPage('_partials/common/head.php');
                $this->getFullPage('_partials/common/header.php');
                $this->getViewAndCache('_views/games/game-detail.php',$action);
                $this->getFullPage('_partials/common/footer.php');
                $fault = false;
            }

        }
        if (@$fault) {
            $this->notFoundPage();
        }
    }

}
