<?php

/**
 * Class Landing Page
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Jackpotoffers extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:  /promotions
    public function index()
    {
        if(!$this->cached())
        {                                               
            $this->content = $this->loadModel("ContentModel")->getContent('jackpotoffers');
        }
        
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');        
        $this->getViewAndCache('_views/jackpot-offers/jackpot-offers.php');       
        $this->getFullPage('_partials/common/footer.php');
    
    }

    
    // called if the url is not clean (a promotion specified) ie:   /promotions/todays-specials
    // todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {   
        header("HTTP/1.0 404 Not Found");
        $this->getFullPage('_partials/common/head.php');
        // $this->getFullPage('_partials/common/header.php');
        $this->getView('_views/error/error.php');
        // $this->getFullPage('_partials/common/footer.php');      
    }

}
