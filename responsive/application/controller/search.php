<?php

/**
 * Class Search
 */
class Search extends Controller
{
    public function index()
    {
		if(!$this->cached())
		{						
			$this->content = $this->loadModel("ContentModel")->getContent('search','search');								
		}	
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getView('_views/search/search.php');
		$this->getFullPage('_partials/common/footer.php');	
    }

    public function action($action, $params = null)
    {
        $this->error404();
    }
}
