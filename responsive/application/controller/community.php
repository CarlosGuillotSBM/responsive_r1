<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Community extends Controller
{
    public $commoncontent;
    public $sidebanners = array();

    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
        if(!$this->cached())
        {
            $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
        }
        // load views. within the views we can echo out $songs and $amount_of_songs easily
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');
        $this->getViewAndCache('_views/community/community.php');
        $this->getFullPage('_partials/common/footer.php');
    }


    // called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
    // todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {

  

        if ( count($params) > 1 ){
           return    $this->notFoundPage();
         } 

        if ( $action != 'blog' && $action != 'winners' && $action != 'comunnity' &&   $action != 'chat-news' &&   $action != 'charity'  ){
            return    $this->notFoundPage();            
        } 
 
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');
        /* If we have a specific URL then access that page */
        if ($params) {
            $correctController = true;
            if(!$this->cached())
            {

                $cm = 	$this->loadModel("ContentModel");

                 // Common content for display
                $this->commoncontent = $cm->getContent($this->controller);
                // Content should be specific to the route
                $this->content = $cm->getContent($this->controller,$params[0]);


                if ($action == 'charity' && !isset($this->content["charity"]) ){
                    return    @$this->notFoundPage();
                }

                /* Right now the only view that uses this is KB, will look into refractoring*/
                if(config("SkinID")==2){
                    @$this->sidebanners = $cm->filterContentByKey($this->commoncontent['side-content'],'DetailsURL',$params[0],true);
                }
                @$this->content = $this->content[$action][0];
            }
            /* Views should be shown depending on what are we looking for */
            switch ($action) {
                /* If we need details from a game winner we have to show a specific view */
                case 'winners': $this->getViewAndCache('_views/community/community-winners-detail.php'); break;
                /* Most cases send us directly to the article page */
                default: 	$this->getViewAndCache('_views/community/community-article.php'); break;
            }
        }
        else{
            if(!$this->cached())
            {
                $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
            }
            switch ($action) {
                case 'bolton-wanderers': 	$this->getViewAndCache('_views/community/community-bolton-wanderers.php'); break;
                case 'rafc': 	$this->getViewAndCache('_views/community/community-rafc.php'); break;
                case 'tv': 	 	$this->getViewAndCache('_views/community/community-tv.php'); break;
                case 'winners': $this->getViewAndCache('_views/community/community-winners.php'); break;
                case 'blog': 	$this->getViewAndCache('_views/community/community-blog.php'); break;
                case 'chat-news': 	$this->getViewAndCache('_views/community/community-chat-news.php'); break;
                case 'charity': 	$this->getViewAndCache('_views/community/community-charity.php'); break;
                case 'featured-story': 	$this->getViewAndCache('_views/community/community.php'); break;
            }
        }
        $this->getFullPage('_partials/common/footer.php');

    }

    function subAction($action,$subAction)
    {
        /*
        This isn't getting triggered.
        If I can find the root cause it would be better if we placed subURL detection here
        */
        if(!$this->cached())
        {
            $cm = 	$this->loadModel("ContentModel");
            $this->content = $cm->getContentDetail('community',$subAction);
            $this->commoncontent = $cm->getContent('community');
            if (isset($this->commoncontent['side-content'])) {
                $this->sidebanners = $cm->filterContentByKey($this->commoncontent['side-content'],'DetailsURL',$subAction,true);
            }
        }

        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');
        switch ($action) {
            case 'winners': $this->getViewAndCache('_views/community/community-winners-detail.php'); break;
            default: $this->getViewAndCache('_views/community/community-article.php'); break;
        }

        //$this->getView('_views/community/community-article.php');
        $this->getFullPage('_partials/common/footer.php');

    }


}