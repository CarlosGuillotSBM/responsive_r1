<?php
class Vip extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
	
		if(config("SkinID") == 1 || config("SkinID") == 5)
		{
			header('Location: /loyalty/');
			exit;
		}
	
		if(config("SkinID") == 3)
		{
			header('Location: /lucky-club/');
			exit;
		}	
	
		if(config("SkinID") == 2)
		{
			header('Location: /kitty-club/');
			exit;
		}	
	
		if(config("SkinID") == 6)
		{
			header('Location: /extra-club/');
			exit;
		}	
	
	
	
    	if(!$this->cached())
		{
												
			$this->content = $this->loadModel("ContentModel")->getContent('vip');							
			
		}
			
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/vip/vip.php');		
		$this->getFullPage('_partials/common/footer.php');
	
    }




}
