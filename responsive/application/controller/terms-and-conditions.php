<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Termsandconditions extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
	// PLEASE NOTE: cache won't be used for Terms and Conditions as for some reason it was failing too often (DEV-10187)
    public function index()
    {
		$this->content = $this->loadModel("ContentModel")->getContent($this->controller);										
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getView('_views/terms-and-conditions/terms-and-conditions.php');		
		$this->getFullPage('_partials/common/footer.php');
	
    }

	
	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {
        if (!$params) {
            if(!$this->cached())
            {
                $this->content = $this->loadModel("ContentModel")->getContent($this->controller,$action);
            }

            if(file_exists('_views/'.$this->controller_url.'/'.$action.'.php') || $this->cached())
            {
                // load views.
                $this->getFullPage('_partials/common/head.php');
                $this->getFullPage('_partials/common/header.php');
                $this->getViewAndCache('_views/'.$this->controller_url.'/'.$action.'.php');
                $this->getFullPage('_partials/common/footer.php');
                $fault = false;
            }
        }

        if (@$fault) {
            $this->error404();
        }
		
    }
}
