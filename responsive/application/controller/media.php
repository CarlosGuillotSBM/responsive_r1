<?php

/**
 * Class Games

 */
class Media extends Controller
{
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {
        $this->error404();
						
    }

    // media folder change
	public function action($action, $params = null)
	{
	    if ($params) {
            $last = array_pop($params);
            if ($last === 'index.php') {
                $route = join( '/', $params);
                //echo "New route:". $route."\n";
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: ' . '/media/' . $action . "/" . $route . '/');
                exit;
            }
            $params[] = $last; // put it back
            $route = join( '/', $params);
            $view = config("Skin") . '/media/' . $action . "/" . $route . "/index.php";
        } else {
            $view = config("Skin") . '/media/' . $action . "/index.php";
        }
        $this->getView($view);
	}
	
}
