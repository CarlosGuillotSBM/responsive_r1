<?php

/**
 * Class BrowserNotSupported
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class BrowserNotSupported extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:  /promotions
    public function index()
    {
        $this->getViewAndCache('_views/browser-not-supported/browser-not-supported.php');
    }

}
