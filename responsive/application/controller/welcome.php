<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Welcome extends Controller
{
	public $registered = false;

    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
        if (!isset($_COOKIE["SessionID"])) {
            header("Location: /");
        }

		if (isset($_COOKIE["Registered"])) {
			$this->registered = true;
			setacookie("Registered",null,-1,'/');
		}
		$action = 'welcome';
		if(!$this->cached())
		{
			$this->openDB();
			$this->content = $this->loadModel("ContentModel")->getContent('welcome',$action);

			// $$$$$$$$$$$$$$$
			// FOR PAGES THAT NEEDS GAMES IN THE WELCOME PAGE
			// $$$$$$$$$$$$$$$
			// $gamesModel = $this->loadModel("GamesModel");
			// $this->category = $gamesModel->getGames('welcome');
			// $this->rs = $gamesModel->rs;
			// $this->games = queryDBNextRS($this->rs);
			// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		}

		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		include	'_trackers/register.php';
		$this->getViewAndCache('_views/welcome/welcome.php');
		$this->getFullPage('_partials/common/footer.php');
    }


	public function action($action, $params = null)
    {

        if ($params) {
            $this->error404();
        } else {
            if (!isset($_COOKIE["SessionID"])) {
                header("Location: /");
            }

            if (isset($_COOKIE["Registered"])) {
                $this->registered = true;
                setacookie("Registered",null,-1,'/');
            }

            if(!$this->cached())
            {
                $this->openDB();
                $contentModel = $this->loadModel("ContentModel");
                $ownData = $contentModel->getContentByFilter('welcome','DetailsURL', $action, $action);
                if (empty($ownData)) {
                    $this->content = $contentModel->getContent('welcome',$action);
                } else {
                    $this->content['welcome-offers'] = $contentModel->filterContentByKey($ownData, "Key", 'welcome-offers');
                    $this->content['welcome-bonus-offers-title'] =  $contentModel->filterContentByKey($ownData, "Key", 'welcome-bonus-offers-title');
                    $this->content['welcome-deposit-offers-title'] =  $contentModel->filterContentByKey($ownData, "Key", 'welcome-deposit-offers-title');
                    $this->content['welcome-content'] =  $contentModel->filterContentByKey($ownData, "Key", 'welcome-content');
                }
                // $$$$$$$$$$$$$$$
                // FOR PAGES THAT NEEDS GAMES IN THE WELCOME PAGE
                // $$$$$$$$$$$$$$$
                // $gamesModel = $this->loadModel("GamesModel");
                // $this->category = $gamesModel->getGames('welcome');
                // $this->rs = $gamesModel->rs;
                // $this->games = queryDBNextRS($this->rs);
                // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            }

            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            include	'_trackers/register.php';
            $this->getViewAndCache('_views/welcome/welcome.php', $action);
            $this->getFullPage('_partials/common/footer.php');
        }

	}
}
