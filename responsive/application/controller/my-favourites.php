<?php


/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class myfavourites extends Controller
{

	public function index()
	{
		
	
		if(!$this->cached())
		{						
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);

		}	

		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/my-favourites/my-favourites.php');
		$this->getFullPage('_partials/common/footer.php');		
	}

}
