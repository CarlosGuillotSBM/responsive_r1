<?php

/**
 * Class More
 */
class More extends Controller
{
    public function index()
    {
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getView('_views/more/more.php');
		$this->getFullPage('_partials/common/footer.php');	
    }

    public function action($action, $params = null)
    {
        $this->error404();
    }
}
