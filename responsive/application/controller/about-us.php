<?php

class Aboutus extends Controller
{
    
	
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
		if(!$this->cached())
		{
			$this->openDB();							
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);	
		}				
		
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/about-us/about-us.php');
		$this->getFullPage('_partials/common/footer.php');	
	
    }
}
