<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Faq extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:  /promotions
    public function index()
    {
        if(!$this->cached())
        {                                                
            $this->content = $this->loadModel("ContentModel")->getContent('faq');                                      
        }
        
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');        
        $this->getViewAndCache('_views/faq/faq.php');        
        $this->getFullPage('_partials/common/footer.php');
    
    }


}
