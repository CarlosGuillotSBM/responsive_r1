<?php

/**
 * Progressive jackpots
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class ProgressiveJackpots extends Controller
{

    public function index()
    {		
		header("Content-type: text/xml;charset=UTF-8");
		$xml = new SimpleXMLElement('<jackpot/>');
		$xml->addChild('totalAmount', $this->totalProgresives);
		ob_clean();
		print($xml->asXML());
    }

}
