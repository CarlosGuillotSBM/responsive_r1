<?php
/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Lobby extends Controller
{

	
	public $games=array();
	public $category;
	public $rs;

	
	
	public function index()
	{
		
		if(!isset($_COOKIE["SessionID"]) ||  $_COOKIE["SessionID"] == '')
		{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /login/');
			exit;
		}		
		
		// always redirect to the start page
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: /start/');
		exit;


		
	
		if(!$this->cached())
		{						
			$modelContent = $this->loadModel("ContentModel");

			$this->content = $modelContent->getContent('lobby');

			$this->promotions = $modelContent->getContent('promotions');
			$gamesModel = $this->loadModel("GamesModel");			
			$this->category = $gamesModel->getGames('LOBBY');	
			$this->rs = $gamesModel->rs;
			$this->games = queryDBNextRS($this->rs);	
			//$this->promotions = $modelContent->getContent('promotions');
			
			if(config("SkinID") == 6 || config("SkinID") == 7)
			{	
				// bingo extra games on lobby page	
				$community = $modelContent->getContent('community');
				$this->promotions["promotions"] = array();
				$this->promotions["promotions"][] = $community["blog"][1];
				$this->promotions["promotions"][] = $community["blog"][2];
				$this->promotions["promotions"][] = $community["blog"][3];
				$gamesModel = $this->loadModel("GamesModel");			
				$this->category = $gamesModel->getGames('SLOTS');	
				$this->rs = $gamesModel->rs;
				$this->games = queryDBNextRS($this->rs);
			}
			
			if(config("SkinID") == 5)
			{	
				// magicalvegas games on lobby page		


			}
			
		}	
		


		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/lobby/lobby.php');
		$this->getFullPage('_partials/common/footer.php');		
	}

}
