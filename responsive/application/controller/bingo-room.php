<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class BingoRoom extends Controller
{

    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {		
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_global-library/views/bingo/bingo-redirect.php');
        $this->getFullPage('_global-library/partials/common/footer.php');
    }
}
