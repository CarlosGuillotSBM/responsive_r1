<?php

/**
* Class Games
*
* Please note:
* Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
* This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
*
*/
class Myaccount extends Controller
{
	// called if the url is clean (no promotion specified) ie:	/promotions
	public function index()
	{
		// Check URI Query
		$this->validateURI();

		if (config("SkinID") === 300 && config("RealDevice") == 1) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /start/');
			exit;
		}

		if(!$this->cached())
		{
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);
		}

		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');
		$this->getViewAndCache('_views/my-account/my-account.php');
		$this->getFullPage('_partials/common/footer.php');
	}

	public function validateURI( )
	{
		$isAGuestUser = $this->isAGuestUser();
		$URI = $_SERVER["QUERY_STRING"];
		if ( $URI === 'tab=ui-redeem-tab' && $isAGuestUser ) {
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /login/');
		}
		return;
	}

	public function isAGuestUser()
	{
		if( !isset( $_COOKIE["SessionID"] ) ||  $_COOKIE["SessionID"] == '' )
		{
			return true;
		}
		return false;
	}

	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
	public function action($action, $params = null)
	{


		switch ($action) {

			case 'update-details':
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getView('_global-library/my-account/my-account__update-details.php');
			$this->getFullPage('_partials/common/footer.php');
			break;

			case 'convert-points':
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getView('_global-library/my-account/my-account__convert-points.php');
			$this->getFullPage('_partials/common/footer.php');
			break;

			case 'account-activity':
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getView('_global-library/my-account/my-account__account-activity.php');
			$this->getFullPage('_partials/common/footer.php');
			break;

			case 'your-offers':
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getView('_global-library/my-account/my-account__your-offers.php');
			$this->getFullPage('_partials/common/footer.php');
			break;

			default:
			# code...
			break;
		}

	}



}
