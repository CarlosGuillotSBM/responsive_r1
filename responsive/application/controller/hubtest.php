<?php

/**
 * Class Roulette

 */ 
class Hubtest extends Controller
{
    public $row;
	public $games;
	public $rs;
	public $category;
	
	
	
	// called if the url is clean (no game specified) ie:	/games
    public function index()
    {
		$this->content = $this->loadModel("ContentModel")->getContent($this->controller);							
		
		if(Env::isLive() && config('Spacebar') === false && config('ExternalIp') === false)
		{
            $this->error404();
		}
		else
		{	
			// load views. 
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');		
			$this->getViewAndCache('_views/games/hubtest.php');
			$this->getFullPage('_partials/common/footer.php');
		}					
    }

}
