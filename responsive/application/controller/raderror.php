<?php
class RadError extends Controller
{

	public $games;
	public $rs;

    public function index()
    {

    	$this->getFullPage('_partials/common/head.php');
    	$this->getFullPage('_partials/common/header.php');
    	$this->getView('_views/error/error.php');
    	$this->getFullPage('_partials/common/footer.php');		
    }
    public function action($action, $params = null)
    {

    	$this->getFullPage('_partials/common/head.php');
    	$this->getFullPage('_partials/common/header.php');
    	$this->getView('_views/error/error.php');
    	$this->getFullPage('_partials/common/footer.php');		
    }
}
