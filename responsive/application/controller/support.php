<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Support extends Controller
{
    
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
	
		if(in_array(config("SkinID"), array(Skin::LuckyPants, Skin::GiveBackBingo, Skin::KittyBingo))) {
			header('HTTP/1.1 301 Moved Permanently');
			header("Location: /help/");
			exit;			
		}
	
	
		if(!$this->cached())
		{												
			$this->content = $this->loadModel("ContentModel")->getContent('support');										
		}
		
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        
		$this->getViewAndCache('_views/support/support.php');		
		$this->getFullPage('_partials/common/footer.php');
	
    }

	
	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {
        $this->notFoundPage();
    }
}
