<?php

/**
 * Class Games
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Promotions extends Controller
{

	public $promotions;
    public $action;
    public $sidebanners = array();

	private $nextPromoIndex = 0;

    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
        $model = $this->loadModel("ContentModel");
        $this->content = $model->getContent('promotions');
        $this->action = "";

		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php'); 
        if (!$this->getSessionId()) {
        	if (isset($this->content['promotions'])) {
				// Text5 is the name of the field that is used to store the promotion restrictions
				// we need to filter out the ones containing the Keyword IN but just in logged out views
				$this->content['promotions'] = $model->filterOutContentByKeyContains($this->content['promotions'],'Text5','IN');
			}	
			$this->getViewAndCache('_views/promotions/promotions.php');
        } else {
            $this->content['promotions'] = $model->filterOutPromotionsByPlayerId($this->content['promotions'],$this->getPlayerId());
			$this->getView('_views/promotions/promotions.php');
        }	
		$this->getFullPage('_partials/common/footer.php');
    }
	
	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {
        if ($params) {
            $this->error404();

        } else {
            $toBeCached = !$this->cached() && !$this->getSessionId(); // note: we can't cache if player is logged in!!

            $promotionsModel = $this->loadModel("ContentModel");
            $this->content = $promotionsModel->getContentDetail('promotions',$action);
            $this->promotions = $promotionsModel->getContent('promotions');

            // Applying filters

            if (!$this->getSessionId()) {

                // filter by session
                if (isset($this->promotions["promotions"])) {
                    // Text5 is the name of the field that is used to store the promotion restrictions
                    // we need to filter out the ones containing the Keyword IN but just in logged out views
                    $this->promotions["promotions"] = $promotionsModel->filterOutContentByKeyContains($this->promotions["promotions"],'Text5','IN');
                }
            } else {

                // filter by player ID
                $this->promotions["promotions"] = $promotionsModel->filterOutPromotionsByPlayerId($this->promotions["promotions"],$this->getPlayerId());
            }

            // get array of promos in order to display the link Next Promo on the page

            if (isset($this->promotions['promotion-details__side-content'])) {
                $this->sideBanners = $promotionsModel->filterContentByKey($this->promotions['promotion-details__side-content'], 'DetailsURL', $action, true);
            }

            if($toBeCached)
            {


                $this->action = $action;

                // total promos
                $totalPromos = 0;
                if (is_array(@$this->promotions['promotions']))
                    $totalPromos = count($this->promotions['promotions']);

                // putt out the item from the promotions array if it's the one in the content array
                for($i=0;$i<$totalPromos;$i++)
                {
                    if(@$this->promotions['promotions'][$i]['DetailsURL'] == @$this->content['DetailsURL'])
                    {
                        // get next promo Index
                        $this->nextPromoIndex = $i + 1 == $totalPromos ? 0 : $i + 1;

                        unset($this->promotions['promotions'][$i]);

                        break;
                    }
                }
            }

            if(is_array(@$this->content) && count(@$this->content) == 0){
                $this->notFoundPage();
                return;
            }

            if ($this->getSessionId()) {
                if ( !$promotionsModel->isPromotionAccessibleByPlayerId($this->content, $this->getPlayerId())) {
                    $this->notFoundPage();
                    return;
                }
            }

            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            if ($toBeCached) {
                $this->getViewAndCache('_views/promotions/promotion-detail.php');
            } else {
                $this->getView('_views/promotions/promotion-detail.php');
            }
            $this->getFullPage('_partials/common/footer.php');
        }


    }


	/**
	 * Get the next promotion in the array
	 */
	public function getNextPromoURL()
	{
		return @$this->promotions['promotions'][$this->nextPromoIndex]['DetailsURL'];
	}

}
