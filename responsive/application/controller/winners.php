<?php

/**
 * Class Winners
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Winners extends Controller
{
    public $commoncontent;
    
    // called if the url is clean (no promotion specified) ie:	/promotions
    public function index()
    {
    	if(config("SkinID") == 2){
			if(!$this->cached())
			{				
				$this->content = $this->loadModel("ContentModel")->getContent($this->controller);		
			}	
			// load views. within the views we can echo out $songs and $amount_of_songs easily
			$this->getFullPage('_partials/common/head.php');
			$this->getFullPage('_partials/common/header.php');
			$this->getViewAndCache('_views/winners/winners.php');
			$this->getFullPage('_partials/common/footer.php');	    	
		} else {
			goto_error_page();
    	}

    }

	
	// called if the url is not clean (a promotion specified) ie:	/promotions/todays-specials
	// todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {
		if(!$this->cached())
		{			
			$this->content = $this->loadModel("ContentModel")->getContent($this->controller);		
		} 
		$this->getFullPage('_partials/common/head.php');
		$this->getFullPage('_partials/common/header.php');        			
		$this->getViewAndCache('_views/community/community-winners.php');
		$this->getFullPage('_partials/common/footer.php');
		
    }



}
