<?php

class Loyalty extends Controller
{
    
    // called if the url is clean (no promotion specified) ie:  /promotions
    public function index()
    {
		$this->checkViewExists('_views/loyalty/loyalty.php');				
	
        if(!$this->cached())
        {                                                
            $this->content = $this->loadModel("ContentModel")->getContent('loyalty');                                      
        }
		
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');        
        $this->getViewAndCache('_views/loyalty/loyalty.php');        
        $this->getFullPage('_partials/common/footer.php');
    
    }

    
    // called if the url is not clean (a promotion specified) ie:   /promotions/todays-specials
    // todays-specials will be passed in as the action.
    public function action($action, $params = null)
    {
        if ($params) {
            $this->error404();
        } else {
            if(!$this->cached())
            {
                $this->content = $this->loadModel("ContentModel")->getContent('loyalty');
            }

            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');

            switch ($action)
            {
                case 'vip-points':
                case 'magical-moolahs':
                    $this->getViewAndCache('_views/loyalty/loyalty-vip-points.php'); break;
                case 'vip-club':
                    $this->getViewAndCache('_views/loyalty/loyalty-vip-club.php'); break;
                default:
                    $this->getView('_views/error/error.php'); break;
            }
            $this->getFullPage('_partials/common/footer.php');
        }

    }
}
