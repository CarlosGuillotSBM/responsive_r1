<?php


/**
 * Class Amp
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Amp extends Controller
{

	
	public $games = array();
	public $category;
	public $rs;
	public $promotions;
	public $community;

	
	
	public function index()
	{		

		$this->getViewAndCache('../../../landing-pages/'.config('Skin').'/media/ppc/ab-home-amp/index.php');
	}
	
	public function action($action, $params = null)
	{
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: /');
			exit;		
	}
}
