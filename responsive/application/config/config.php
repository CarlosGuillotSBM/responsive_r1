
<?php
/**
 * Configuration **
 */
error_reporting(E_ALL);
if (!defined('COMPUTER')) {
    define('COMPUTER', $_SERVER['COMPUTERNAME']);
}
header('Vary: User-Agent');

// you may not want to load anything else
if (empty($route)) {
    $route = '';
}

include $route . 'libs/utils.php';

class Config
{
    static $validServers = [
        // LOCAL
        'responsive.spinandwin.webdev' => 'spinandwin-local', // SPINANDWIN
        'responsive.kittybingo.webdev' => 'kittybingo-local', // KITTYBINGO
        'responsive.magicalvegas.webdev' => 'magicalvegas-local', // MAGICAL VEGAS
        'responsive.kingjack.webdev' => 'kingjack-local', // KING JACK
        'responsive.luckypantsbingo.webdev' => 'luckypantsbingo-local', // LUCKY PANTS
        'responsive.bingoextra.webdev' => 'bingoextra-local', // BINGO EXTRA
        'responsive.luckyvip.webdev' => 'luckyvip-local', // LUCKY VIP
        'responsive.regalwins.webdev' => 'regalwins-local', // REGAL WINS
        'responsive.givebackbingo.webdev' => 'givebackbingo-local', // GIVE BACK BINGO
        'responsive.aspers.webdev' => 'aspers-local', // ASPERS

        // DEV SERVERS
        'responsivekitty.dagacubedev.net' => 'kittybingo-dev',
        'kittybingo.dagacubedev.net' => 'kittybingo-dev',
        'kittybingo.dagacubeuat.net' => 'kittybingo-dev',
        'kittybingo2.dagacubedev.net' => 'kittybingo-dev',

        'luckypantsbingo2.dagacubedev.net' => 'luckypantsbingo-dev',
        'luckypantsbingo3.dagacubedev.net' => 'luckypantsbingo-dev',
        'responsivepants.dagacubedev.net' => 'luckypantsbingo-dev',
        'luckypantsbingo.dagacubeuat.net' => 'luckypantsbingo-dev',
        'luckypantsbingo.dagacubedev.net' => 'luckypantsbingo-dev',

        'directslot.dagacubedev.net' => 'directslot-dev',
        'responsivemagical.dagacubedev.net' => 'magicalvegas-dev',
        'magicalvegas.dagacubeuat.net' => 'magicalvegas-dev',

        'responsiveking.dagacubedev.net' => 'kingjack-dev',
        'responsivespinandwin.dagacubedev.net' => 'spinandwin-dev',
        'spinandwin.dagacubedev.net' => 'spinandwin-dev',
        'spinandwin.dagacubeuat.net' => 'spinandwin-dev',

        'bingoextra.dagacubedev.net' => 'bingoextra-dev',
        'bingoextra.dagacubeuat.net' => 'bingoextra-dev',

        'luckyvip.dagacubedev.net' => 'luckyvip-dev',
        'luckyvip.dagacubeuat.net' => 'luckyvip-dev',

        'regalwins.dagacubedev.net' => 'regalwins-dev',
        'regalwins.dagacubeuat.net' => 'regalwins-dev',

        'givebackbingo.dagacubedev.net' => 'givebackbingo-dev',
        'givebackbingo.dagacubeuat.net' => 'givebackbingo-dev',

        'aspers.dagacubedev.net' => 'aspers-dev',
        'aspers.dagacubeuat.net' => 'aspers-dev',

        'kingjackcasino.dagacubedev.net' => 'kingjack-dev',
        'kingjackcasino.dagacubeuat.net' => 'kingjack-dev',

        // LIVE SERVERS
        'www.magicalvegas.com' => 'magicalvegas',
        'www.kingjackcasino.com' => 'kingjack',
        'www.spinandwin.com' => 'spinandwin',
        'www.bingoextra.com' => 'bingoextra',
        'www.kittybingo.com' => 'kittybingo',
        'www.luckypantsbingo.com' => 'luckypantsbingo',
        'www.directslot.gr' => 'directslot',
        'www.luckyvip.com' => 'luckyvip',
        'www.regalwins.com' => 'regalwins',
        'www.givebackbingo.com' => 'givebackbingo',
        'www.aspers.com' => 'aspers',

        // STAGING
        'r1staging_magicalvegas.dagacube.net' => 'magicalvegas',
        'r1staging_kingjackcasino.dagacube.net' => 'kingjack',
        'r1staging_spinandwin.dagacube.net' => 'spinandwin',
        'r1staging_bingoextra.dagacube.net' => 'bingoextra',
        'r1staging_kittybingo.dagacube.net' => 'kittybingo',
        'r1staging_luckypantsbingo.dagacube.net' => 'luckypantsbingo',
        'r1staging_luckyvip.dagacube.net' => 'luckyvip',
        'r1staging_regalwins.dagacube.net' => 'regalwins',
        'r1staging_givebackbingo.dagacube.net' => 'givebackbingo',
        'r1staging_aspers.dagacube.net' => 'aspers'
    ];

    // set of office IP's
    static $ips = array(
        '62.232.79.254',
        '176.35.211.24',
        '46.65.33.113',
        '120.151.3.227',
        '127.0.0.1',
        '192.168.200.182',
        '196.192.15.123',
        '62.252.145.60',
        '58.175.242.246',
        '192.168.150.128'
    );

    /*
	GT Server Backend:
	95.131.236.21 (Malta proxy server)
	185.16.78.3 (Guernsey proxy Server)
	185.16.77.14 (Austria proxy server)
	Nagarro Office / Backend in Gurgaon, India:
	Range: 103.56.173.1
    */
    static $externalIps = array('95.131.236.21', '185.16.78.3', '185.16.77.14', '103.56.173.1');

    static function loadConfig($configName)
    {
        $server = strtolower($_SERVER['SERVER_NAME']);

        // fall back to looking up server name
        if (empty($configName)) {
            if (array_key_exists($server, self::$validServers)) {
                $configName = empty(self::$validServers[$server]) ? $server : self::$validServers[$server];
            }
        }

        $env = parse_ini_file('sites/' . $configName . '.ini', false, INI_SCANNER_TYPED);
        if (!$env) {
            return false;
        }

        if (isset($env['display_errors'])) {
            ini_set('display_errors', $env['display_errors']);
        }

        $env['URL'] = @$_SERVER['HTTPS'] == 'on' ? 'https://'.$server : 'http://' . $server;
        $env['ips'] = self::$ips;

        // port 8433 or new staging access on subdomain
        if (@$_SERVER['SERVER_PORT'] == 8433 || strpos($server, 'r1staging') === 0) {
            $env['URL'] .= ':8433';
            $env['Env'] = Env::staging;

            // This bit will help us to determine what's the db source

            if (@$_REQUEST['dbResource'] === 'staging') {
                unset($_COOKIE['dbResource']);
                setacookie('dbResource', null, -1, '/');
                setacookie('dbResource', 'stagingDb', 0, '/');
            } elseif (@$_REQUEST['dbResource'] === 'live') {
                unset($_COOKIE['dbResource']);
                setacookie('dbResource', null, -1, '/');
            }

            if (@$_COOKIE['dbResource'] === 'stagingDb') {
                $env['competitionEngineURL'] = 'https://reelrunner.dagacube.net/competitions_staging';
            }

            $env['Edit'] = 1;
            $env['Debug'] = true;
            $env['jsDebug'] = true;
            $env['Memcache'] = 0;
            $env['GameURL'] = 'https://game.dagacube.net/radgame_staging.php';
            $env['adyen'] = 'https://live.adyen.com/hpp/cse/js/1114860830473905.shtml';
            $env['IGapi'] =
                'https://bravo.aspzone.co.uk:51330/UAT/OnlineGateway/CustomerManagementService.svc?singleWsdl';
            $env['IGComputerName'] = 'STRIDECONNECTION';
            $env['ig-self-exclude'] = 'https://www.aspers.com:8433/ig/self-exclude.php';
            // $env["IGComputerName"] = "IGCRMDMZ";
        }

        switch ($env['Env']) {
            case Env::local:
            case Env::dev:
                $env['ApiGatewayURL'] = 'https://apigateway.sbmdev.net/';
                $env['NewApiGatewayURL'] = 'https://apigateway.sbmdev.net/';
                $env['ThreeRadicalAPI'] = $env['ApiGatewayURL'] . 'three-radical';
                $env['PublicAPIGatewayURL'] = 'https://apigateway.sbmdev.net/';
                $env['PrizeWheelURL'] = 'https://gameclient.dagacubedev.net/html/freegames/prize_wheel/index.php';
                $env['LocateApiKey'] = 'EC97-GR75-HC71-MA99';
                $env['Logs'] = 'D:\\agsLogs\\';
                $env['SocketURL'] = 'wss://api.dagacube.net/notifications-service/api/v1/notifications';

                $env['Memcache'] = $env['Env'] === Env::local ? false : true;
                break;

            case Env::live:
            case Env::staging:
                $env['PrizeWheelURL'] = 'https://gameclient.dagacube.net:8433/html5/freegames/prize_wheel/index.php';
                $env['ApiGatewayURL'] = 'https://apigateway.dagacube.net/';
                $env['PublicAPIGatewayURL'] = 'https://api.dagacube.net/';
                $env['NewApiGatewayURL'] = 'http://api.dagacube.net:7100/';
                $env['Logs'] = 'L:\\agsLogs\\';
                $env['PrizeWheelAPIURL'] = 'https://api.dagacube.net/free-games-engine/v1/';
                $env['LocateApiKey'] = 'CH41-UR26-CN88-FY79';
                $env['SocketURL'] = 'wss://api.dagacube.net/notifications-service/api/v1/notifications';
                break;
        }
        $env['SocketsEnabled'] = $env['Env'] != Env::live ? 1 : 0;
        $env['LCCPEnabled'] = $env['Env'] == Env::live ? 1 : 0;

        // The different Microservices
        $env['PlayerManagementAPI'] = $env['ApiGatewayURL'] . 'player';
        $env['GamStopAPI'] = $env['NewApiGatewayURL'] . 'gamstop';
        $env['EventLogAPI'] = $env['NewApiGatewayURL'] . 'eventlog';
        $env['ThreeRadicalAPI'] = $env['NewApiGatewayURL'] . 'three-radical';
        $env['PrizeWheelAPIURL'] = $env['PublicAPIGatewayURL'] . 'free-games-engine/v1/';

        $env['Device'] = getDevice();
        $env['RealDevice'] = getRealDevice();
        $ip = getPlayerIP();
        $env['playerIP'] = $ip;
        $env['Spacebar'] = in_array($ip, self::$ips);
        $env['ExternalIp'] = in_array($ip, self::$externalIps);
        $env['Version'] = '@@Replace';

        // verified player gets better access level
        $env['AccessLevel'] = 1; // 1 means Child Friendly won't be used

        // $env['AccessLevel'] = empty($_COOKIE['PlayerID']) || empty($_COOKIE['Verified']) ? 0 : 1;
        if ($env['Env'] == 'live') {
            $env['PrizeWheelURL'] = 'https://gameclient.dagacube.net/html5/freegames/prize_wheel/index.php';
            $env['SocketsEnabled'] = 0;
        }
        $env['RecaptchaURL'] = 'https://www.google.com/recaptcha/api/siteverify';

        ksort($env, SORT_STRING | SORT_FLAG_CASE);
        return $env;
    }
}

$configName = $_SERVER['SPACEBAR_CONFIG'] ?? '';

$config = Config::loadConfig($configName);
if (!$config) {
    echo <<<EOF
<br>The site URL was not found in the application/config/config.php file.<br>
There needs to be an entry for <b>{$_SERVER['SERVER_NAME']}</b> there or nothing will work!
EOF;
    exit();
}

switch (config('maintenance')) {
    case 1:
        include '_views/maintenance/under-maintenance.php';
        header('HTTP/1.1 503 Service Unavailable');
        header('Retry-After: 1800');
        exit();
        break;
    case 2:
        if (!in_array($config['playerIP'], $config['ips'])) {
            include '_views/maintenance/under-maintenance.php';
            header('HTTP/1.1 503 Service Unavailable');
            header('Retry-After: 1800');
            exit();
        }
        break;
}
include 'db.php';
function jsConfig()
{
    ?>
	var sbm = sbm || {};
	sbm.serverconfig = {
		skinId: <?php echo config('SkinID'); ?>,
		editMode: !!"<?php echo config('Edit'); ?>",
		device: <?php echo config('RealDevice'); ?>,
		ip: "<?php echo getPlayerIP(); ?>",
		gameUrl: "<?php echo config('GameURL'); ?>",
		cashierUrl: "<?php echo config('CashierURL'); ?>",
		debug: !!"<?php echo config('jsDebug'); ?>",
		version: "<?php echo config('Version'); ?>",
		bingoUrl: "<?php echo config('BingoURL'); ?>",
		slotPage: "<?php echo config('slotPage'); ?>",
		showSplash: !!"<?php echo config('ShowSplash'); ?>",
		contentSwap: true,
		gameLaunchMode: "<?php echo config('gameLaunchMode'); ?>",
		accountActivityURL: "<?php echo config('accountActivityURL'); ?>",
		realityCheckOn: !!"<?php echo config('reality-check-on'); ?>",
		messageInboxOn: !!"<?php echo config('inbox-msg-on'); ?>",
		switchStartDate: new Date('2017', '05', '12', '12', '34', '09'),// yyyy,mm (month-1!!!), dd,hh,mm,ss
		switchOldSiteURL: "<?php echo config('switchOldSiteURL'); ?>",
		end3WeeksDate: new Date('2017', '05', '28', '10', '00', '00'),// yyyy,mm (month-1!!!), dd,hh,mm,ss
		start3WeeksDate: new Date('2017', '05', '21', '10', '00', '01'),// yyyy,mm (month-1!!!), dd,hh,mm,ss
		bingoHtml5Only: !!"<?php echo config('bingoHtml5Only'); ?>",
		adyen: "<?php echo config('adyen'); ?>",
		gtmId: "<?php echo config('gtmID'); ?>",
		separatedNames: !!"<?php echo config('separatedNames'); ?>",
		playerManagementAPI: "<?php echo config('PlayerManagementAPI'); ?>",
		liveChatURL: "<?php echo config('LiveChatURL'); ?>",
		threeRadicalURLBoard: "<?php echo @config('threeRadicalBoardURL'); ?>",
		prizeWheelURL: "<?php echo @config('PrizeWheelURL'); ?>",
		socketURL: "<?php echo @config('SocketURL'); ?>",
		socketsEnabled: "<?php echo @config('SocketsEnabled'); ?>",
		baseUrl: "<?php echo @config('URL'); ?>",
        recaptcha3SiteKey: "<?php echo @config('recaptcha3SiteKey'); ?>"
	};
<?php
}

