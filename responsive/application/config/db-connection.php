<?php
/*
SINGLE CENTRAL DATABASE FUNCTIONS FILE
Using:
- PHP SqlServer Driver and SQL Native Client
  php/ext/php_sqlsrv_54_nts.dll ver 3.0.3421.0

This file is included before a php script accesses a database.
The calling script can select any Server, Database and Connection Number in the dbInit() function,
or use the default $dbServer defined in this file if not specified.

Notes:
SQL Native Client error:
 - Executing SQL directly; no cursor
   This may mean there is a login/security problem on the db

UPDATES

*/

// check we are being included from the file we expect

$callingFile = substr(GetIncludingFile(), -25);

if ((($callingFile !== 'application\config\db.php') && ($callingFile !== 'application/config/db.php'))) {
    switch (config('Env')) {
        case 'local':
        case 'dev':
            break;
        default:
            sendEmergencyMail([
                'hamishi@spacebarmedia.com',
                'ChristoO@tncconsultingltd.com',
                'Mohtadib@spacebarmedia.com',
                'rafaels@spacebarmedia.com'
            ]);
            exit;

    }
}

$dbConnection = array();

switch (config('Env')) {
    case 'live':
        setConfig("env", "live");
        $dbConnection["GAME"] = "192.168.40.51";
        break;
    case 'staging':
        setConfig("env", "staging");
        if (@$_COOKIE['dbResource'] === 'stagingDb') {
            $dbConnection["GAME"] = "192.168.40.70";
        } else {
            $dbConnection["GAME"] = "192.168.40.51";
        }
        break;
    default:
        switch (strtolower(substr(COMPUTER, 0, 5))) {

            /* ---------------------------------------------------------------------------------------
                DEV SERVERS
                --------------------------------------------------------------------------------------- */

            case 'devs':
                setConfig("env", "dev");
                $dbConnection["GAME"] = "DEEBEE\DEEBEE"; //the default SQL server database name to use. Can be overridden in the dbInit call from the calling script
                //$dbConnection["UID"] = "game";
                //$dbConnection["PWD"] = "th3.n3w$0m0v";
                break;
            case 'ldnse': // ldnserver08
                setConfig("env", "dev");
                $dbConnection["GAME"] = "BINGOX1\GAME";
                //$dbConnection["UID"] = "DaGaCube";
                //$dbConnection["PWD"] = "DaGaCube";
                break;
            case 'serve': //server10
                setConfig("env", "dev");
                $dbConnection["GAME"] = "SERVER10";
                //$dbConnection["UID"] = "DaGaCube";
                //$dbConnection["PWD"] = "DaGaCube";
                break;
            case 'ldnvm': //dev
                setConfig("env", "dev");
                $dbConnection["GAME"] = "LDNVM1VL206DEV";
                //$dbConnection["UID"] = "DaGaCube"; //dont need because we use the machine login
                //$dbConnection["PWD"] = "DaGaCube"; //dont need because we use the machine login
                break;
            case 'ldnst': //local development
            case 'macde':
                setConfig("env", "dev");
                $dbConnection["GAME"] = "192.168.206.21";
                //$dbConnection["UID"] = "DaGaCube"; //dont need because we use the machine login
                //$dbConnection["PWD"] = "DaGaCube"; //dont need because we use the machine login
                break;
            default:

                echo "ERROR<br>";
                echo COMPUTER . " not found in /config/db.php";
                exit;
        }
        break;
}

// FIND THE CALLING FILE PATH AND MAKE SURE IT'S db.php
function GetIncludingFile()
{
    $file = false;
    $backtrace = debug_backtrace(false);
    $include_functions = array('include', 'include_once', 'require', 'require_once');
    for ($index = 0; $index < count($backtrace); $index++) {
        $function = $backtrace[$index]['function'];
        if (in_array($function, $include_functions)) {
            $file = $backtrace[$index]['file'];
            break;
        }
    }
    return $file;
}

function sendEmergencyMail($to_in)
{
    $to = is_array($to_in) ? join(',', $to_in) : $to_in;
    $from = "support@spinandwin.com";
    $subject = "IMPORTANT! - someone is trying to steal our DB credentials";
    $ip = getEmergencyPlayerIP();
    $trace = print_r(debug_backtrace(true), true);
    $message = <<<EOF
Someone tried to read the db-connection file from not db.php\n\r
IP {$ip}\r\n
Stack trace\r\n
{$trace}
EOF;

    $headers = "Content-type: text/plain; charset=iso-8859-1\r\n";
    $headers .= "From: $from\r\n";
    mail($to, $subject, $message, $headers);
}

function getEmergencyPlayerIP()
{
    if (isset($_SERVER)) {
        if (isset($_SERVER["REMOTE_ADDR"]))
            if ($_SERVER["REMOTE_ADDR"] != "")
                return $_SERVER["REMOTE_ADDR"];

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
                return $_SERVER["HTTP_X_FORWARDED_FOR"];

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
            if ($_SERVER["HTTP_CLIENT_IP"] != "")
                return $_SERVER["HTTP_CLIENT_IP"];
    }

    if (getenv('REMOTE_ADDR'))
        if (getenv('REMOTE_ADDR') != "")
            return getenv('REMOTE_ADDR');

    if (getenv('HTTP_X_FORWARDED_FOR'))
        if (getenv('HTTP_X_FORWARDED_FOR') != "")
            return getenv('HTTP_X_FORWARDED_FOR');

    if (getenv('HTTP_CLIENT_IP'))
        if (getenv('HTTP_CLIENT_IP'))
            return getenv('HTTP_CLIENT_IP');
}

?>