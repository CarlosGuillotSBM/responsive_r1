
<?php

if(empty($route)){
	$route = '';
} 

include 'Mobile_Detect.php';
include 'Silverpop.php';
include_once $route.'models/UserGroup.php';

$debug_procs = '';


/*---------------------------------------------------------------------------
get some config
---------------------------------------------------------------------------*/

function config($what)
{
	global $config;
	return $config[$what];
}

/*---------------------------------------------------------------------------
set some config
--------------------------------------------------------------------------*/
function setConfig($what,$value)
{
	global $config;
	$config[$what] = $value;
}


class Env {
    const local = 'local';
    const live = 'live';
    const dev = 'dev';
    const staging  = 'staging';

    const states = [
    	self::live, self::staging, self::dev, self::local
	];

    // test if in a set of states
    static function inState($states = self::states) {
    	return in_array(strtolower(config('Env')), $states);
	}

    static function isLocal() {
        return (strtolower( config('Env')) === self::local);
    }

	static function isDev() {
        return (strtolower( config('Env')) === self::dev);
    }

	static function isStaging() {
        return (strtolower( config('Env')) === self::staging);
    }

    static function isLive() {
        return (strtolower( config('Env')) === self::live);
	}
}



/*---------------------------------------------------------------------------
SKIN EDITOR PERMISSIONS
--------------------------------------------------------------------------*/
function isLoggedInSkinEditor()
{
	return checkCookie("EditUser") && checkCookie("AdminSessionID");
}

function isAdminInSkinEditor()
{
	return checkCookie("IsAdmin") && $_COOKIE["IsAdmin"] == 1;
}

function isDaubKGroupSkinEditor()
{
	return checkCookie("UserGroup") && $_COOKIE["UserGroup"] == UserGroup::DaubK ;
}

function is8BallGroupSkinEditor()
{
	return checkCookie("UserGroup") && $_COOKIE["UserGroup"] == UserGroup::EightBall ;
}

function checkCookie($cookie)
{
	return isset($_COOKIE[$cookie]) && $_COOKIE[$cookie] != '';
}

/*---------------------------------------------------------------------------
END SKIN EDITOR PERMISSIONS
--------------------------------------------------------------------------*/





/*====================================================================
Called from WriteLog parameter function calls for db parameters to convert the params to string.
Returns the dbparams as a string that can be inserted into a sql exec eg 1,'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',123456 (see db.php)
======================================================================*/
function SdbParams ( $dbParams ) {
	$sParams="";
	foreach ($dbParams as $dbParam) {
		switch($dbParam[1])
		{
			case 'int':
			if(strval($dbParam[0]) == '')
			{
				$sParams .= "null, ";
			}
			else
			{
				$sParams .= "'".str_replace("'","''",strval($dbParam[0]))."', ";
			}
			break;

			case 'UI':
			if(strval($dbParam[0]) == 'NULL')
			{
				$sParams .= strval($dbParam[0]).", ";
			}
			else
			{
				$sParams .= "'".str_replace("'","''",$dbParam[0])."', ";
			}
			break;
			case 'date':
			if(strval($dbParam[0]) == '')
			{
				$sParams .= "null, ";
			}
			else
			{
				$sParams .= "'".str_replace("'","''",$dbParam[0])."', ";
			}
			break;

			default:
			$sParams .= "'".str_replace("'","''",$dbParam[0])."', ";
			break;



		}
	}
	$sParams = substr($sParams,0,strlen($sParams)-2);
	return $sParams;
}


/*====================================================================
//look for the player ip in several places.
======================================================================*/
function getPlayerIP() {
	if (isset($_SERVER)) {
		if (isset($_SERVER["REMOTE_ADDR"]))
		if ($_SERVER["REMOTE_ADDR"] != "")
		return $_SERVER["REMOTE_ADDR"];

		if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
		if ($_SERVER["HTTP_X_FORWARDED_FOR"] != "")
		return $_SERVER["HTTP_X_FORWARDED_FOR"];

		if (isset($_SERVER["HTTP_CLIENT_IP"]))
		if ($_SERVER["HTTP_CLIENT_IP"] != "")
		return $_SERVER["HTTP_CLIENT_IP"];
	}

	if (getenv('REMOTE_ADDR'))
	if (getenv('REMOTE_ADDR') != "")
	return getenv('REMOTE_ADDR');

	if (getenv('HTTP_X_FORWARDED_FOR'))
	if (getenv('HTTP_X_FORWARDED_FOR') != "")
	return getenv('HTTP_X_FORWARDED_FOR');

	if (getenv('HTTP_CLIENT_IP'))
	if (getenv('HTTP_CLIENT_IP'))
	return getenv('HTTP_CLIENT_IP');
}



// Find out the device type
function getDevice()
{
	$detect = new Mobile_Detect;
	if($detect->isMobile() || $detect->isTablet()) {return 2;}
	return 1;
}

// Find out the REAL device type
function getRealDevice()
{
	$detect = new Mobile_Detect;

	if($detect->isTablet()) return 2;
	if($detect->isMobile()) return 3;

	return 1;
}


function getBrowser()
{

	$browser = new Browser();
	$device = $browser->getBrowser();

}


function queryDB($db,$proc,$params,$multirow = false,$MakeRowsCategories = false,&$rs=null)
{

	global $debug_procs;
	$rows = array();
	$paramString = SdbParams($params);
	$sql = "exec dbo.$proc $paramString";

	$debug_procs .= $sql."<br>";

	if($proc == "DataRealityCheckSet")
	{
		WriteLog('DB: '.$sql);
	}



	if ($db) {
        // windows and live
        $rs = sqlsrv_query(@$db, $sql);

        if($rs)
        {
            while($row = sqlsrv_fetch_object($rs))
            {
                if (isset($row) || $row!=null)
                {

                    $fields = array();
                    foreach($row as $key => $value)
                    {
                        $fields[$key]=$value;
                    }
                    if($multirow)
                    {
                        if($MakeRowsCategories && isset($fields['Category']))
                            $rows[$fields['Category']] = $fields;
                        else
                            $rows[] = $fields;
                    }
                    else
                    {
                        $rows = $fields;
                    }
                }
            }
        }
        else
        {
            $dbErr="<br><br>queryDB Sproc: $proc ****** Error:<br><br>";
            foreach( sqlsrv_errors() as $error )
            {
                $dbErr.=$sql."<br>";

                $dbErr.="SQLSTATE: ".$error['SQLSTATE']."<br><br>";
                $dbErr.="code: ".$error['code']."<br>";
                $dbErr.="message: ".$error['message'];
            }
            raise_error($dbErr);
        }
	} else {
	    trigger_error( "No connection to database ", E_USER_ERROR);
    }


    return $rows;
}

function queryDatabaseWithSQL($db, $sql, $multirow = false, $MakeRowsCategories = false, &$rs=null)
{
	$rows = array();
	if($rs = sqlsrv_query($db, $sql)) {
		return transformResultFromDDBB("sqlsrv_fetch_object", $rs);
	} else {
		$dbErr="<br><br>queryDB Error:<br><br>";
		foreach( sqlsrv_errors() as $error ) {
			$dbErr.=$sql."<br>";

			$dbErr.="SQLSTATE: ".$error['SQLSTATE']."<br><br>";
			$dbErr.="code: ".$error['code']."<br>";
			$dbErr.="message: ".$error['message'];
		}
		raise_error($dbErr);

		$errors = sqlsrv_errors();
		$message = "";
		$code = -1;
		if (isset($errors[0])) {
			$message = $errors[0]['message'];
			$code = $errors[0]['code'];
		}
		throw new ErrorException($message, $code);
	}
}

function transformResultFromDDBB($function, $resourse)
{
	$rows = array();
	while($row = call_user_func($function, $resourse)) {
		if (isset($row) || $row!=null) {
			$fields = array();
			foreach($row as $key => $value) {
				$fields[$key]=$value;
			}
			$rows[] = $fields;
		}
	}
	return $rows;
}


/**
* Function to handle the insertion of rows
*
* @param $tableName
* @param array $argumentList
* @return string
*/
function prepareRowForInsertion($tableName, array $argumentList)
{
	$query = "INSERT INTO ".$tableName." ";
	$columns = "(";
	$values = "(";
	foreach ($argumentList as $column => $value)
	{
		$columns .= "[$column], ";
		$values .= "$value, ";
	}

	$columns = rtrim($columns, ", ") . ") ";
	$values  = rtrim($values, ", ") . ") ";

	return $query . $columns . "VALUES ". $values.";";
}

/**
* Function to handle the update of the rows
*
* @param $tableName
* @param array $setList
* @param array $whereList
* @return string
*/
function prepareRowForUpdate($tableName, array $setList, array $whereList)
{
	$query = "UPDATE ".$tableName." SET ";
	$set = "";
	$condition = "";
	foreach ($setList as $column => $value) {
		$set .= $column." = ".$value.",";
	}
	foreach ($whereList as $firstPart => $secondPart) {
		$condition .= $firstPart." = ".$secondPart.",";
	}

	$set = rtrim($set, ",");
	$condition  = rtrim($condition, ",");

	return $query . $set . " WHERE " . $condition.";";
}

/**
* Function to handle the update of the rows
*
* @param $tableName
* @param array $whereList
* @return string
*/
function prepareRowForDeletion($tableName, array $whereList)
{
	$query = "DELETE FROM ".$tableName." WHERE ";
	$condition = "";
	foreach ($whereList as $firstPart => $secondPart) {
		$condition .= "[$firstPart] = '$secondPart',";
	}
	$condition  = rtrim($condition, ",");

	return $query . $condition.";";
}

/*---------------------------------------------------------------------------
RETURN DATA FROM THE NEXT RECORDSET

---------------------------------------------------------------------------*/
function queryDBNextRS(&$rs,$multirow = true,$oneColumn = false)
{
	$rows = array();
	if($rs == null) return $rows;
	if(sqlsrv_next_result($rs))
	{

		while($row = sqlsrv_fetch_object($rs))
		{
			if (isset($row) || $row!=null)
			{
				$fields = array();
				foreach($row as $key => $value)
				{
					$fields[$key]=$value;
				}
			}
			if($multirow)
			{
				if($oneColumn)
				{
					$rows[] = $value;
				}
				else
				{
					$rows[] = $fields;
				}
			}
			else
			{
				$rows = $fields;
			}
		}
		return $rows;
	}
	else
	{
		return $rows;
	}
}

/*---------------------------------------------------------------------------
KEEPS THE AFFILIATE TRAIL IN THE DATABASE
We need to keep it here since it needs to be used in two places
---------------------------------------------------------------------------*/
function keepAffiliateTrail ($db) {
	$ok = array();
	$ok["Code"] = 1;
	$ok["Msg"] = "Ftd went wrong";
	if (isset($_COOKIE["AffTrail"]) && $_COOKIE["AffTrail"] != ''){

		$playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;

		$params =   array(  "PlayerID"         => array($playerID, "int", 0),
		"AffiliateTrail" => array(@$_COOKIE["AffTrail"], "str", 1000)
	);

	$ok = queryDB($db,"DataAffiliateTrail",$params);
	if($ok["Code"]==0) {
		setacookie('AffTrail', null, -1, '/');
	}
	$ok["Msg"] = "Ftd done";
}
return $ok;
}


/*---------------------------------------------------------------------------
CREATE THE BREADCRUMBS ARRAY
RETURNS A KEY VALUE PAIR ARRAY OF BREADCRUMBS
key = the part of the url
value = the pretty name for display
---------------------------------------------------------------------------*/
function createBreadcrumbs()
{
	if(strtok(@$_SERVER["REQUEST_URI"],'?') == '/content.php')
	$split = @$_REQUEST["cid"];
	else
	$split = strtok(@$_SERVER["REQUEST_URI"],'?');

	$b = array();
	$trail = '/';


	$b['Home'] = '/';



	// split URL
	$url = ltrim(rtrim($split, '/'),'/');
	$url = filter_var($url, FILTER_SANITIZE_URL);
	$aUrl = explode('/', $url);

	if($url == '') return $b;

	foreach($aUrl as $value)
	{

		$trail.= $value.'/';
		$name = ucwords(str_replace('-',' ',$value));
		if($name != 'Game' && strpos($name,'Thank You') === false)
		$b[$name] = $trail;
	}

	return $b;
}

/*---------------------------------------------------------------------------
CREATE CACHE KEY
---------------------------------------------------------------------------*/
function createCacheKey()
{
	if(strtok(@$_SERVER["REQUEST_URI"],'?') == '/content.php')
	$url = @$_REQUEST["cid"];
	else
	$url = filter_var(strtok($_SERVER["REQUEST_URI"],'?'), FILTER_SANITIZE_URL);

	if(substr($url, -1) == '/') {$url = substr($url, 0, -1);}

	if($url == '')
	{
		$ret = 'skin_'.config('SkinID');
	}
	else
	{
		$ret=strtolower(substr(str_replace('-','_',str_replace('/','_',$url)),1).'_skin_'.config('SkinID'));
	}

	return $ret;
}



/*---------------------------------------------------------------------------
SEND HANDLED ERRORS HERE FOR DISPLAY OR LOGGING
---------------------------------------------------------------------------*/
function raise_error($errorString,$exit = false,$email = false)
{
	if(Env::isDev())
	{
		echo $errorString;

		if($exit)
		{
			exit;
		}

	}

	WriteLog('ERROR: '.$errorString);

}


function WriteLog($data)
{
	if(@config("Log")=='') return;


	$theFileName=config("Log")."/".date("Ymd").".log";
	$theFileData = date("H:i:s")." ".$data;
	$fp = fopen($theFileName,"a+");
	if ($fp) {
        fwrite($fp,$theFileData."\r\n");
        fclose($fp);
    }

}

/*
	Writes $data to the file specified by the $path
*/
function WriteNewLog($path, $data)
{
	$theFileName = $path."/".date("Ymd").".txt";
	$theFileData = date("H:i:s")." ".$data;
	$fp = fopen($theFileName,"a+");
	if ($fp) {
        fwrite($fp,$theFileData."\r\n");
        fclose($fp);
    }
}

/*---------------------------------------------------------------------------
TURN A DB ROW INTO A RESPONSE FOR ALL RETURNED FIELDS - used by data.php
---------------------------------------------------------------------------*/
function addRowToResponse($row)
{
	if (isset($row["Code"])) {
        $response["Code"] = $row["Code"];

        if($row["Code"] != 0)
        {
            $response["Msg"] = $row["Msg"];
        }
        else
        {
            foreach($row as $key => $value)
            {
                $response[$key] = $value;
            }
        }
	} else {
		$response = [
			'Code' => -1,
			'Msg' => 'Request failed'
		];
	}

	return $response;

}


//------------------------------------------------------------------------------------------------------------------------------------------
// SET AN OPTION SELECTED
//------------------------------------------------------------------------------------------------------------------------------------------
function setSelected($test,$value)
{
	if($test == $value) echo " selected ";
}

/*------------------------------------------------------------------------------------------------------------------------------------------
Date: 22/02/2019
Feature: R10-734
Description : Calling the new API for our address look-up, problem to solve, get all detaills from an specific address.
Step 1) With parameter $Postcode we fetch a CONTAINER ID (Loqate class),
Step 2) With the CONTAINER ID we make a second api call to the same end point (Loqate class).We fetch a collection of
addrese. We traversing all addreses in order to store all addreses  (id $response["AddressesId"])
Step 3) From the user interface the client select an specific address. We compare the element position selected with
the array addresses id passed and then we make a thrird API call to LoqateCaptureDetaills.

------------------------------------------------------------------------------------------------------------------------------------------*/
function CheckPostCodeWith3rdParty($Postcode) {


	$configName = $_SERVER['SPACEBAR_CONFIG'] ?? '';
	$config = Config::loadConfig( $configName);
	$locatApiKeyCurrentEnviroment = $config["LocateApiKey"];

	require_once('loqate.php');
	require_once('LoqateCaptureDetaills.php');

	$client = new Loqate($locatApiKeyCurrentEnviroment, $Postcode, false, '', '', 'GB', 10, '');
	$client->MakeRequest();
	$data = $client->HasData();
	$container = null;



	try{
		//if the first api call was ok
		if(isset($data)){

			foreach ($data as $k1 =>$v1){
				if (array_key_exists('Id', $v1)) {
					$container = $v1["Id"];
				}
			}
		} else {
			throw new Exception("No data");
		}
	}catch (Exception $e) {

		$response["Code"] = -2;
		$response["Msg"] = "Postcode lookup not working. Please add your address manually";

		return $response;
	}


	if ($container !== null){

		$second_api_call = new Loqate($locatApiKeyCurrentEnviroment, $Postcode, false, $container, '', 'GB', 10, '');
		$second_api_call->MakeRequest();
		$data_second_api_call = $second_api_call->HasData();
		$resultado = json_decode(json_encode((array)$data_second_api_call), TRUE);
		$container = null;
		$items = 0;

		foreach ($resultado as $key => $value )  {
			foreach ( $value as $key_level2 => $value_level2 ) {


				foreach ($value_level2 as  $key_level3 => $value_level3 ) {

					if($key_level2 === 'Id') {
						$address_id =   $value_level3;
						$response["AddressesId"] [] = $value_level3;
					}
					if($key_level2 === 'Text') {
						$response["Addresses"][$items] = $value_level3;
					}
				}
			}

			$response["Code"] = 0;
			$response["Postcode"] = $resultado[$items]["Text"];
			$items++;
		}
		return $response;
	}//end check-container


}//endif


/*------------------------------------------------------------------------------------------------------------------------------------------
 Date: 22/02/2019
 Description : get the address details from the address Id
 Feature: R10-734
------------------------------------------------------------------------------------------------------------------------------------------*/
function CheckAddressDetailsWith3rdParty($Id) {

	$configName = $_SERVER['SPACEBAR_CONFIG'] ?? '';
	$config = Config::loadConfig( $configName);
	$locatApiKeyCurrentEnviroment = $config["LocateApiKey"];

	require_once('loqate.php');
	require_once('LoqateCaptureDetaills.php');

	$client = new LoqateCaptureDetaills($locatApiKeyCurrentEnviroment, $Id);
	$client->MakeRequest();
	$data = $client->HasData();
	$container = null;

	$data = json_decode(json_encode((array)$data), TRUE);

	try{
		if(isset($data)){

			$response["Code"] = 0;
			$response["Line1"] = $data[0]["Line1"];
			$response["Line2"] = $data[0]["Line2"];
			$response["City"] = $data[0]["City"];
			$response["County"] = empty($data[0]["Province"]) ? $data[0]["Province"] : "";
			$response["Data"] = $data;

			return $response;
		}

		throw new Exception("No data");

	}catch (Exception $e) {

		$response["Code"] = -2;
		$response["Msg"] = "Postcode lookup not working. Please add your address manually";

		return $response;
	}
}


/*---------------------------------------------------------------------------
CHECK AN ITEM IN THE ROW EXISTS
---------------------------------------------------------------------------*/
function exists($r)
{
	if(isset($r) && $r != '')
	{
		return true;
	}
	return false;
}



/*---------------------------------------------------------------------------
GO TO THE ERROR PAGE
it also sets the headers correctly
this must be called before any content has been sent.
---------------------------------------------------------------------------*/
function goto_error_page()
{
	header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /error/");
    exit;
}


/*---------------------------------------------------------------------------
READ CONTENT FROM DISK
---------------------------------------------------------------------------*/
function readFileContent($filename)
{
	$path = './mailers/';//substr($_SERVER["SCRIPT_FILENAME"],0,strripos($_SERVER["SCRIPT_FILENAME"],"\\")+1);
	$file = $path.$filename;
	if (!file_exists($file)){
		// we try in our own folder, it may be mailer.php
		$path = './';//substr($_SERVER["SCRIPT_FILENAME"],0,strripos($_SERVER["SCRIPT_FILENAME"],"\\")+1);
		$file = $path.$filename;
	}
	$fh = fopen($file, 'r');
	$text = fread($fh, filesize ($file));
	fclose ($fh);
	return $text;
}

/*---------------------------------------------------------------------------
SEND AN EMAIL
---------------------------------------------------------------------------*/
function sendMail ($from, $to, $subject, $message, $html = false) {
	if(Env::isLocal()) return;
	$headers = "";
	if($html)
	{
		$headers ="MIME-Version: 1.0\r\n";
		$headers.="Content-type: text/html; charset=utf-8\r\n";
	}
	else
	{
		$headers.="Content-type: text/plain; charset=iso-8859-1\r\n";
	}
	$headers .="From: $from\r\n";
	mail($to,$subject,$message,$headers);

}

/*---------------------------------------------------------------------------
	Replace tokens and content in template
---------------------------------------------------------------------------*/
function loadTemplate( $template, $content, $tokens) {
    $result = '';
    if (!empty($template)) {
        // replace the content first...
        if (isset($content)) {
            $result = str_ireplace("%%CONTENT%%",$content,$template);
        }

        $result = str_ireplace( array_keys($tokens), array_values($tokens),$result);
    }
    return $result;
}

/*---------------------------------------------------------------------------
SEND AN EMAIL
---------------------------------------------------------------------------*/
function getPreviewMessage($data){

    $template = $data["MessageContent"];
    // if is html we'll use the template

    $tokens = [
        '%%TemplateTitle%%' => $data["TemplateTitle"],
        '%%MessageDesignerTemplateImageName%%' => $data['TemplateImageName'],
        '%%MessageDesignerTemplateImageName2%%' => $data['TemplateImageName2'],
        '%%MessageDesignerTemplateCTALabel%%' => $data['TemplateCTALabel'],
        '%%MessageDesignerTemplateCTALink%%' => $data['TemplateCTALink'],
        '%%MessageContent%%' => $data['TemplateText'],
        '%%Alias%%' => $data['Username'],
        '%%Firstname%%' => $data['Firstname'],
        '%%Lastname%%' => isset($data['Lastname']) ? $data['Lastname'] : '',
        '%%Currency%%' => $data['CurrencyCode'],
        '%%CurrencySymbol%%' => $data['Symbol'],
        '%%CentsSymbol%%' => $data['CentsSymbol'],
        '%%Amount%%' => amountToString($data["Amount"]),
        '%%AwardDate%%' => $data['AwardDate'],
        '%%ExpireDate%%' => $data['ExpireDate'],
        '%%SupportEmail%%' => $data['SupportEmail'],
        '%%Custom01%%' => $data['Custom01'],
        '%%Custom02%%' => $data['Custom02'],
        '%%Custom03%%' => $data['Custom03'],
        '%%Custom04%%' => $data['Custom04'],
        '%%Custom05%%' => $data['Custom05'],
        '%%Custom06%%' => $data['Custom06'],
        '%%Custom07%%' => $data['Custom07'],
        '%%Custom08%%' => $data['Custom08'],
        '%%Name%%' => $data['Name'],
        '%%URL%%' => $data['SkinURL'],
        '<a ' => "<a target='_parent'"

    ];
    return loadTemplate($template, '', $tokens);
}

/*---------------------------------------------------------------------------
SEND FORGOT PASSWORD EMAIL
---------------------------------------------------------------------------*/
function sendForgotPasswordMail($data)
{

    $template = readFileContent("template.html");
    $content = readFileContent("forgot_password_content.html");

    $tokens = [
        '%%Alias%%' => 	@$data["Username"],
        '%%GUID%%' => @$data["GUID"],
        '%%PlayerID%%' => @$data["PlayerID"],
        '%%Firstname%%' => @$data["Firstname"],
        '%%Lastname%%' => @$data["Lastname"],
        '%%Name%%' => config("Name"),
        '%%SupportEmail%%' => config("Email"),
        '%%URL%%' => config("URL")
    ];

    $message = loadTemplate($template, $content, $tokens);

    sendMail (config("Email"), $data["Email"] , config("Name")." Password update",$message, true);

    return $message;
}

/*---------------------------------------------------------------------------
SEND LOCKED USER MAIL
---------------------------------------------------------------------------*/
function sendLockedUserMail( $data )
{
    $data['GUID'] = $data['ReactivationGUID'];

    $template = readFileContent("template.html");
    $content = readFileContent("security_content.html");

    $tokens = [
        '%%Alias%%' => 	$data["Username"],
        '%%BrandNameURL%%' => strtolower( $_SERVER["SERVER_NAME"] ),
        '%%Email%%' => $data["Email"],
        '%%GUID%%' => $data["GUID"],
        '%%ReactivationGUID%%' => $data['ReactivationGUID'],
        '%%PlayerID%%' => $data["PlayerID"],
        '%%Firstname%%' => $data["Firstname"],
        '%%Lastname%%' => $data["Lastname"],
        '%%Name%%' => config("Name"),
        '%%SupportEmail%%' => config("Email"),
        '%%SkinID%%' => config("SkinID"),
        '%%supportPage%%' => config("supportPage"),
        '%%URL%%' => config("URL")
    ];

    $message = loadTemplate($template, $content, $tokens);

    sendMail (config("Email"), $data["Email"] , config("Name")."Your account has been locked",$message, true);
}

/*---------------------------------------------------------------------------
SEND SECURITY MAIL
---------------------------------------------------------------------------*/

function sendSecurityMail($data)
{
    $data['GUID'] = $data['ReactivationGUID'];

    $template = readFileContent("template.html");
    $content = readFileContent("security_content.html");

    $tokens = [
        '%%Alias%%' => 	$data["Username"],
        '%%BrandNameURL%%' => strtolower( $_SERVER["SERVER_NAME"] ),
        '%%Email%%' => $data["Email"],
        '%%GUID%%' => $data["GUID"],
        '%%ReactivationGUID%%' => $data['ReactivationGUID'],
        '%%PlayerID%%' => $data["PlayerID"],
        '%%Firstname%%' => $data["Firstname"],
        '%%Lastname%%' => $data["Lastname"],
        '%%Name%%' => config("Name"),
        '%%SupportEmail%%' => config("Email"),
        '%%SkinID%%' => config("SkinID"),
        '%%supportPage%%' => config("supportPage"),
        '%%URL%%' => config("URL")
    ];

    $message = loadTemplate($template, $content, $tokens);

    sendMail (config("Email"), $data["Email"] , config("Name")." Security Mail",$message, true);
}

/*---------------------------------------------------------------------------
SEND REGISTRATION MAIL
---------------------------------------------------------------------------*/
function sendRegistrationMail($data)
{
    $ret = '';

    // if(config("SkinID") != 6)
    // {

    $template = readFileContent("template.html");
    $content = readFileContent("register_content.html");

    $tokens = [
        '%%SupportEmail%%' => config("Email"),
        '%%URL%%' => config("URL"),
        '%%SkinID%%' => config("SkinID"),
        '%%Alias%%' => 	$data["Username"],
        '%%Firstname%%' => $data["Firstname"],
        '%%Lastname%%' => $data["Lastname"],
        '%%PlayerID%%' => $data["PlayerID"],
        '%%Password%%' => $data['Password'],
        '%%RegEmailValidation%%' => $data['RegEmailValidation'],
        '%%PromoEmailValidation%%' => $data['PromoEmailValidation'],
        '%%ValidationGUID%%' => $data['ValidationGUID'],
        '%%CurrencySymbol%%' => $data['CurrencySymbol']
    ];

    $message = loadTemplate($template, $content, $tokens);

    sendMail (config("Email"), $data["Email"] , "Welcome to ".config("Name"),$message, true);

    // add player to the silverpop engage database
    //$mail = new Silverpop;
    //$ret .= $mail->addPlayer($data);



    // }
    // else
    // {
    // $ret .= $mail->sendRegistrationMail($data);
    // }
    return $ret;
}


/*---------------------------------------------------------------------------
SEND COGS ForgotPasswordMail
---------------------------------------------------------------------------*/
function COGSForgotPasswordMail($db,$email)
{

	sendForgotPasswordMail($db,$email);

}

/*---------------------------------------------------------------------------
SEND RAF EMAIL
---------------------------------------------------------------------------*/
function sendReferAFriendMail($data)
{

    $emails = explode(';',@$data["ValidEmails"]);
    $emailsType = explode(';',@$data["ValidEmailsTypes"]);

    $referLink = config("URL").'/register/'.@$data["UserName"];
    $realLink = $referLink;

    $tokens = [
        '%%SupportEmail%%' => config("Email"),
        '%%URL%%' => config("URL"),
        '%%Alias%%' => 	$data["Username"],
        '%%Firstname%%' => $data["Firstname"],
        '%%Lastname%%' => $data["Lastname"],
        '%%ReferLink%%' => $referLink,
        '%%RealLink%%' => $realLink,
        '%%Message%%' => $data["Message"]
    ];

    $template = readFileContent("refer_template.html");

    // load content outside loop
    $reminderContent = readFileContent("refer_reminder_content.html");
    $inviteContent = readFileContent("refer_invite_content.html");

    $count=sizeof($emails);
    for ($i=0; $i<$count; $i++)
    {
        if ($emails[$i]!=''){
            if ($emailsType[$i]>1){
                $content = $reminderContent;
                $subject = "Join ". @$data["FirstName"]." at " . @$data["SkinName"] . "!";
            } else {
                $content = $inviteContent;
                $subject = @$data["FirstName"]." invites you to join " . @$data["SkinName"] . "!";
            }

            // TODO: no personal data of receiver so could generate just two messages instead of lots
            $message = loadTemplate($template, $content, $tokens);

            sendMail (config("Email"), $emails[$i], $subject , $message, true);

        }
    }

}

/*---------------------------------------------------------------------------
FUNCTION TO GET THE HOME PAGE URL, LOGGED OFF == "/", LOGGED IN == "LOBBY"
---------------------------------------------------------------------------*/
function getHomeURL(){

	if (isset($_COOKIE["SessionID"])) {
		$homeUrl = "/start/";
	}else{
		$homeUrl = "/";
	}

	return $homeUrl;
}
function logged(){

	if (isset($_COOKIE["SessionID"])) {
		$logged = "in";
	}else{
		$logged = "out";
	}

	return $logged;
}

/*---------------------------------------------------------------------------
FUNCTION INSERT AN EDIT POINT
---------------------------------------------------------------------------*/
function edit($controller,$key='',$subkey='', $styles="")
{
	if(config("Edit"))
	{
		echo '<a class="edit-point" data-bind="edit: {page: \''.$controller.'\', key: \''.$key.'\', subkey: \''.$subkey.'\' }" style="display: none;'.$styles.'" data-nohijack="true" href="/_global-library/editor/editor.php">edit</a>';
	}
}

function sanitizeValue($value)
{
	return htmlentities(strip_tags($value));
}

/*---------------------------------------------------------------------------
VALIDATE FIELDS
---------------------------------------------------------------------------*/
function validate($item,$value)
{
	switch($item)
	{
		case "username":
		$valid = false;
		$value = strtolower(trim($value));
		if (strlen($value) <= 100 &&
		strlen($value) >= 3   &&
		((preg_match("/^[.\w-]+@([\w-]+\.)+[a-zA-Z]{2,6}$/", $value)) || (preg_match("/^([a-z]{1}([a-z0-9_]){2,14})$/i", $value)))
	) {
		$valid = true;
	}
	break;

	case "password":
	$rules = array("/.{5}/",
	"/^.{0,15}$/",
	"/^\S+$/",
	"/[_a-zA-Z0-9!@\^#\$%\&\*]/",
	"/[0-9]/");

	foreach ($rules as $pregMatch) {
		$valid=preg_match($pregMatch, $value);
		if (!$valid) break;
	}
	if ($valid) $valid=preg_match("/^[_a-zA-Z0-9!@^#\$%\&\*]+$/i", $value);
	break;

	case "email":
	$valid = false;
	$value = strtolower(trim($value));
	if (strlen($value) <= 100 && preg_match("/^[.\w-]+@([\w-]+\.)+[a-zA-Z]{2,6}$/", $value)) {
		$valid = true;
		$hostName = substr(strstr($value, "@"),1);
		if(!empty($hostName)) {
			exec("nslookup -type=MX $hostName", $result);
			// check each line to find the one that starts with the host
			// name. If it exists then the function succeeded.
			foreach ($result as $line) {
				if(preg_match("/^$hostName/i",$line)) {
					$valid = true;
				}
			}
		}
	}
	break;


}
return $valid;

}

// add elipsps to strings that are toooooo long
function ellipsis($str,$maxlength=20,$additional="...")
{
	return (strlen($str)>$maxlength)? substr($str,0,$maxlength).$additional : $str;
}


// set a cookie
function setacookie($name,$value,$expire = -1,$domain = '/')
{
	$_COOKIE[$name] = $value;
	if(Env::inState( [ Env::live, Env::staging]))
	{
		setcookie($name,$value,$expire,$domain,null,true);
	}
	else
	{
		setcookie($name,$value,$expire,$domain);
	}
}

function amountToString ($amount) {
	//This needs to come from the db, emailType or so
	if (is_numeric($amount)){
		return number_format($amount,2);
	} else {
		return utf8_decode($amount);
	}

}

// returns tomorrow's day in our internal format MON,TUE,WED,THU,FRI,SAT,SUN
function getTomorrow () {
	return strtoupper(date('D', strtotime('+1 day')));
}

// returns today's day in our internal format MON,TUE,WED,THU,FRI,SAT,SUN
function getToday () {
	return strtoupper(date('D'));
}

function getVersionedSource($versionedSource) {
	return $versionedSource.'?='.config('Version');
}

/*
returns the current url including query string
*/
function getRequestURL() {
	return 'http' . (isset($_SERVER['HTTPS']) ? 's' : ''). "://" . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

?>


