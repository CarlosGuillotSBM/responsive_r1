<?php

include_once 'controller/ajax-request/ajax.php';

try {
    $request = Ajax::createFromFunction();
    $request->writeHeaders();
    $request->execute();
    $request->getResponse();
} catch (\Exception $e) {
    require 'libs/oldData.php';
}

?>