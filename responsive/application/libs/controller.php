<?php

include_once "repository/ParameterValidator.php";

if (!Env::isLive()) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(-1);
}


/**
 * This is the "base controller class". All other "real" controllers extend this class.
 */
class Controller
{

    public $data = null;        // this will hold the data that was loaded by the model
    public $feature = null;        // this will hold the 1st feature row found in the data
    public $fullPage = true;    // will be true if we are loading server side, false if client side
    public $db = null;            // db connection
    public $row = null;            // the current row;
    public $cache_key = '';
    public $memcache = null;    // a reference to the memcache object
    public $useMemcache = false;
    public $controller = '';    // the name of the class
    public $controller_url = '';        // the name of the controller as specified in the url ( MINUS SIGN KEPT )
    public $action = '';
    public $subaction = '';
    public $title = '';
    public $breadcrumbs = array(); // a key/value array (key=title, value=url)
    public $seo_meta = '';
    public $content;
    public $winners;
    public $winners_total;
    public $progressives;
    public $loadedFromCache = 'nope';
    public $isGameProvider = false;
    public $totalProgresives = 0;
    public $category;
    public $games;
    public $rs;


    function __construct()
    {

        ini_set('session.cookie_lifetime', 0);
        session_set_cookie_params(0);

        session_start();


        $db = $this->openDB();
        if (config("Memcache")) {
            $this->memcache = new Memcache;
            $this->useMemcache = $this->memcache->connect('127.0.0.1', 11210);
        }

        setacookie("DeviceUsed", $this->getRealDevice());
        // see if we have basic cookies set, if not look them up and set them
        if (!isset($_COOKIE["LanguageCode"])) {
            $indexModel = $this->loadModel("IndexModel")->getIndex();

            if (isset($indexModel["Code"]) && $indexModel["Code"] == 0) {
                setacookie("CountryCode", $indexModel["CountryCode"], 0);
                setacookie("CurrencyCode", $indexModel["CurrencyCode"], 0);
                setacookie("CurrencySymbol", $indexModel["CurrencySymbol"], 0);
                setacookie("CentsSymbol", $indexModel["CentsSymbol"], 0);
                setacookie("LanguageCode", $indexModel["LanguageCode"], 0);
            }
        }


        // If there's no affID at this time and we don't have it in the session, it means it comes from natural search
        if (!isset($_SESSION["session"])) {
            //setacookie("session",0,0);
            $_SESSION["session"] = 0;
            $affID = 0;
            //echo "yeah";
        }

        // CAPTURE AFFILIATES - direct link
        if (isset($_GET['aid']) && $_GET['aid'] != '' && isset($_GET['tid']) && $_GET['tid'] != '' && isset($_GET['cid']) && $_GET['cid'] != '') {

            try {
                ParameterValidator::filterInputParameterInt(@$_GET['aid']);
                ParameterValidator::filterInputParameterInt(@$_GET['cid']);
                ParameterValidator::filterInputParameterInt(@$_GET['tid']);
                $params =     array(
                    "aid"               => array(@$_GET['aid'], "int", 0),
                    "cid"               => array(@$_GET['cid'], "int", 0),
                    "tid"               => array(@$_GET['tid'], "int", 0),
                    "DynamicVariable"   => array(@$_GET['dv'], "str", 100),
                    "KeywordLandingPage" => array(getRequestURL(), "str", 200)
                );

                $affID = queryDB($this->db, "WebAffiliateClick", $params);
                $affID = @$affID['SubCampaignID'];


                setacookie("dv", @$_GET['dv']);
            } catch (ParameterValidatorException $e) {
                // TODO Jose Handle this error propertly
            }
        }

        // CAPTURE AFFILIATES - VIA THE REDIRECTOR
        if (!isset($affID) && isset($_GET['a']) && $_GET['a'] != '') {
            $affID = $_GET['a'];
            $affCredit = @$_GET['ac'];  // this is a GUID that has come from external free play to win signup bonus.

            if (isset($affCredit) && $affCredit !== '') {
                if (!isset($_COOKIE["AffCredit"])) {
                    setacookie("AffCredit", $affCredit, time() + (60 * 30), "/");
                }
            }
        }

        if (isset($affID)) {

            $parameterAID = @$_GET['aid'];

            setacookie("AffID", $affID, null, "/");

            // We set this temporary cookie to send it to the funnel system
            setacookie("AffTrackID", $affID, time() + (60 * 60 * 24 * 30), "/");

            $ref = "Empty HTTP_REFERER";

            // if referrer is on the URL and not empty
            if (!empty($_GET["ref"])) {

                // get referrer from URL
                $ref = $_GET["ref"];
            } else {

                // Check for param HTTP_REFERER

                if (@$_SERVER['HTTP_REFERER'] != '') {

                    $refererDomain = parse_url(@$_SERVER['HTTP_REFERER']);

                    $ref = !empty($refererDomain) && !empty($refererDomain['host']) ? $refererDomain['host'] : "Empty HOST";
                }
            }


            $trimreferer = substr(date("Y-m-d H:i:s") . '|' . $affID . '|' . $ref . '~' . @$_COOKIE["AffTrail"], 0, 1000);
            setacookie("AffTrail", $trimreferer, time() + (60 * 60 * 24 * 30), "/");

            $affIDs = array(86, 111, 710, 713, 709, 714, 806, 805, 859, 858, 1000, 1008, 1009, 1010, 1011, 1689, 1690, 1691, 1754, 1755, 1756, 1865, 1866, 1863, 1864, 1861, 1862, 1914, 1922, 1923, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 2075, 2076);
            if ($affID != 0 && $parameterAID != 1000 && !in_array(@$_GET['aid'], $affIDs)) {
                header('HTTP/1.1 301 Moved Permanently');
                header("Location: " . strtok($_SERVER["REQUEST_URI"], '?'));
                exit;
            }
        }

        // REMEMBER THE REFERER IF COME FROM EXTERNAL LINK

        if (@$_SERVER['HTTP_REFERER'] != '') {

            $refererDomain = parse_url(@$_SERVER['HTTP_REFERER']);

            if (@$refererDomain['host'] != @$_SERVER['SERVER_NAME']) {
                setacookie("HTTPReferer", $_SERVER['HTTP_REFERER'], time() + (60 * 60 * 24 * 3), "/");
            }
        }

        // GAM-1398 - storing the live chat URL to be consumed by HTML5 Bingo Client
        setacookie("globalLiveHelp", config("LiveChatURL"), time() + (60 * 60 * 24 * 3), "/");


        // create the breadcrumbs array
        $this->breadcrumbs = createBreadcrumbs();
        $this->cache_key = createCacheKey();



        // get the meta information
        if ($this->useMemcache && $this->cached($this->cache_key . '_SEO_' . config('RealDevice'))) {
            $this->seo_meta = $this->memcache->get($this->cache_key . '_SEO_' . config('RealDevice'));
            $this->title = $this->memcache->get($this->cache_key . '_Title_' . config('RealDevice'));
        } else {
            $this->seo_meta = $this->loadModel("SEO_Model")->getContent($this->cache_key);

            if (preg_match('/<title>(.+)<\/title>/', $this->seo_meta, $matches) && isset($matches[1])) {
                $this->title = $matches[1];
            } else {
                $this->title = "";
            }

            if ($this->useMemcache) {
                $this->memcache->set($this->cache_key . '_SEO_' . config('RealDevice'), $this->seo_meta, false, config("Memcache_expires_seconds"));
                $this->memcache->set($this->cache_key . '_Title_' . config('RealDevice'), $this->title, false, config("Memcache_expires_seconds"));
            }
        }


        // get the winners information
        if ($this->useMemcache && $this->cached('WINNERS_' . config("SkinID"))) {
            $this->winners = $this->memcache->get('WINNERS_' . config("SkinID"));
            $this->winners_total = $this->memcache->get('WINNERS_TOTAL_' . config("SkinID"));
        } else {
            $winnersModel = $this->winners = $this->loadModel("WinnersModel");

            $this->winners = $winnersModel->getWinners('RECENT');
            $this->winners_total = $winnersModel->getWinners('TOTAL');



            if ($this->useMemcache) {
                $this->memcache->set('WINNERS_' . config("SkinID"), $this->winners, false, config("Memcache_expires_seconds_data"));
                $this->memcache->set('WINNERS_TOTAL_' . config("SkinID"), $this->winners_total, false, config("Memcache_expires_seconds_data"));
            }
        }



        // get the progressive information
        if ($this->useMemcache && $this->cached('PROGRESSIVE_' . config("SkinID"))) {
            $this->progressives = $this->memcache->get('PROGRESSIVE_' . config("SkinID"));
            $this->totalProgresives = $this->memcache->get('PROGRESSIVE_TOTAL_' . config("SkinID"));
        } else {
            $this->progressives = $this->loadModel("ProgressiveModel")->getProgressives();
            $this->totalProgresives = $this->getProgressiveByNameFromArray($this->array_column($this->progressives, 'Name'));

            if ($this->useMemcache) {
                $this->memcache->set('PROGRESSIVE_TOTAL_' . config("SkinID"), $this->totalProgresives, false, config("Memcache_expires_seconds_data"));
                $this->memcache->set('PROGRESSIVE_' . config("SkinID"), $this->progressives, false, config("Memcache_expires_seconds_data"));
            }
        }
    }
    /***
    Array column implementation for php < 5.5
     */
    function array_column(array $input, $columnKey, $indexKey = null)
    {
        $array = array();
        foreach ($input as $value) {
            if (!isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }

            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            } else {
                if (!isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if (!is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }

        return $array;
    }
    /* ---------------------------------------------------------------------------------
    GET PROGRESSIVE TOTAL OF ALL THE PROGRESSIVES INCLUDED IN THE ARRAY
    returns the progressive amount
    ----------------------------------------------------------------------------------*/
    public function getProgressiveByNameFromArray($names)
    {
        $total = 0;
        foreach ($this->progressives as $value) {
            if (in_array($value['Name'], $names)) {
                $total += str_replace(',', '', $value['Jackpot']);
            }
        }
        return $total;
    }

    /**
     * Get a cookie parameter
     *
     * @param $parameter
     * @param null $default
     * @return null
     */
    public function getCookie($parameter, $default = null)
    {
        if (isset($_COOKIE[$parameter])) {
            return $_COOKIE[$parameter];
        } else {
            return $default;
        }
    }

    public function getPlayerId()
    {
        return $this->getCookie("PlayerID", 0);
    }

    public function getSessionId()
    {
        return $this->getCookie("SessionID", null);
    }

    public static function redirect($url, $code = 301)
    {
        $stringCode = ($code === 301) ? "301 Moved Permanently" : "302 Moved Temporally";
        header("HTTP/1.1 301 $stringCode");
        header("Location: $url");
        exit;
    }

    public function notFoundPage()
    {
        header("HTTP/1.0 404 Not Found");

        // only return content if we're looking for a clean URL
        if (strrchr($_SERVER["REQUEST_URI"], ".") === false) {
            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            $this->getView('_views/error/error.php');
            $this->getFullPage('_partials/common/footer.php');
        }
    }

    protected function error404()
    {
        header("HTTP/1.0 404 Not Found");
        $this->getFullPage('_partials/common/head.php');
        $this->getFullPage('_partials/common/header.php');
        $this->getView('_views/error/error.php');
        $this->getFullPage('_partials/common/footer.php');
    }


    /* ---------------------------------------------------------------------------------
    * Default action to be overridden
    ----------------------------------------------------------------------------------*/
    public function action($action, $params = null)
    {
        $this->error404();
    }

    /* ---------------------------------------------------------------------------------
    GET PROGRESSIVE BY NAME
    returns the progressive amount
    ----------------------------------------------------------------------------------*/
    public function getProgressiveByName($name)
    {
        foreach ($this->progressives as $value) {
            if ($value['Name'] == $name) {
                return str_replace(',', '', $value['Jackpot']);
            }
        }
        return '';
    }


    /* ---------------------------------------------------------------------------------
    OPEN THE DATABASE CONNECTION WITH THE CREDENTIALS FROM APPLICATION/CONFIG/DB.PHP
    DEPENDING ON THE ENVIRONMENT WE ARE IN
    ----------------------------------------------------------------------------------*/
    public function openDB($Database = 'Responsive')
    {
        global $dbConnection;

        if (isset($this->db)) return;

        try {
            if (Env::isLocal()) {
                $connectionInfo = array("CharacterSet" => "UTF-8", "Database" => $Database,   "UID" => $dbConnection["UID"], "PWD" => $dbConnection["PWD"], 'ReturnDatesAsStrings' => true);
            } else {
                $connectionInfo = array("CharacterSet" => "UTF-8", "Database" => $Database, "LoginTimeout" => 10, "ReturnDatesAsStrings" => true, "MultipleActiveResultSets" => 1, "ConnectionPooling" => 1, "APP" => "WEB_" . config("SkinID"));
            }
            $this->db = sqlsrv_connect($dbConnection['GAME'], $connectionInfo);
        } catch (Exception $e) {
            $dbErr = '<br><br>Could not connect to the database<br>';
            $dbErr .= 'Database:' . $Database . '<br>';
        }

        if ($this->db == false) {
            $dbErr = "<br><br>Could not connect to the database<br>";
            $dbErr .= 'Server:' . $dbConnection['GAME'] . '<br>';

            $dbErr .= 'Database:' . $Database . '<br>';

            foreach (sqlsrv_errors() as $error) {
                $dbErr .= "SQLSTATE: " . $error['SQLSTATE'] . "<br>";
                $dbErr .= "code: " . $error['code'] . "<br>";
                $dbErr .= "message: " . $error['message'] . "<br>";
            }
            raise_error($dbErr, true);
        }
    }

    /* ---------------------------------------------------------------------------------
    CLOSE DB CONNECTION
    ----------------------------------------------------------------------------------*/
    public function closeDB()
    {
        // close if DB open
        if ($this->db) {
            sqlsrv_close(@$this->db);
        }
    }


    /* ---------------------------------------------------------------------------------
    LOAD A MODEL FILE TO HANDLE DATA ACCESS AND PASS IT THE DB CONNECTION FROM THE CONTROLLER.
    ----------------------------------------------------------------------------------*/
    public function loadModel($model_name)
    {
        require 'models/' . strtolower($model_name) . '.php';
        return new $model_name($this->db, $this);
    }

    /**
     * Load a repository by ddbb
     *
     * @param $model_name
     * @param string $ddbb
     *
     * @return mixed
     */
    public function loadRepository($model_name, $ddbb = "Responsive")
    {
        $class = $model_name . "Repository";
        require 'repository/' . $class . '.php';
        return new $class($this->db, $ddbb);
    }

    /* ---------------------------------------------------------------------------------
    INCLUDE THIS VIEW (PARTIAL) FOR EVERY ROW OF DATA FOR TEMPLATING,
    pass to the feature param the number of which item to feature
    ----------------------------------------------------------------------------------*/
    public function repeatData($dataObject, $feature = 0, $partial = '', $variables = null)
    {

        if (isset($variables)) {
            extract($variables);
        }
        $count = 1;
        if (!isset($dataObject)) {
            $dataObject = array();
        }

        if (is_array($dataObject)) {

            foreach ($dataObject as $row) {
                $row = $this->getWidgetExtras($row);
                if ($count == $feature) {
                    $row["Feature"] = '--featured';
                } else {
                    $row["Feature"] = '';
                }

                if ($partial == '') {
                    $include_partial = $row["Partial"];  // use partial from ContentCategoryType
                } else {
                    $include_partial = $partial;
                }
                include $include_partial;
                $count += 1;
            }
        }
    }




    /* ---------------------------------------------------------------------------------
    INCLUDE THIS VIEW (PARTIAL) FOR EVERY ROW OF DATA FOR TEMPLATING,
    pass to the feature param the number of which item to feature
    pass the tab number for putting content on only some tabs
    ----------------------------------------------------------------------------------*/
    public function repeatDataWithTabs($dataObject, $tab_number_1_based = '', $feature = 0, $partial = '')
    {
        $count = 1;
        if (!isset($dataObject)) {
            $dataObject = array();
        }

        if (is_array($dataObject)) {
            foreach ($dataObject as $row) {
                $row = $this->getWidgetExtras($row);
                if ($row['Text1'] != '') {
                    $tabArray = explode(',', $row['Text1']);
                    if (!in_array($tab_number_1_based, $tabArray))
                        continue;
                }


                if ($count == $feature)
                    $row["Feature"] = '--featured';
                else
                    $row["Feature"] = '';

                if ($partial == '') {
                    $include_partial = $row["Partial"];  // use partial from ContentCategoryType
                } else {
                    $include_partial = $partial;
                }
                include $include_partial;
                $count += 1;
            }
        }
    }

    /* ---------------------------------------------------------------------------------
    INCLUDE THIS VIEW (PARTIAL) FOR EVERY ROW OF DATA FOR TEMPLATING,
    pass to the feature param the number of which item to feature
    pass the tab number for putting content on only some tabs
    you can use all to display them all
    valuesToFilterBy is a comma separeted list. The elements from dataObject which match the condition, will be displayed
    ----------------------------------------------------------------------------------*/
    public function repeatDataWithFilter($dataObject, $valuesToFilterBy = '', $feature = 0, $partial = '', $variables = [])
    {
        if (isset($variables)) {
            extract($variables);
        }
        $count = 1;
        if (!isset($dataObject)) {
            $dataObject = array();
        }

        $filterArray = explode(',', $valuesToFilterBy);

        if (is_array($dataObject)) {
            foreach ($dataObject as $row) {
                if ($row['Text1'] != '' && $valuesToFilterBy !== 'ALL') {
                    $tabArray = explode(',', trim($row['Text1'], ''));
                    if (count(array_intersect($filterArray, $tabArray)) === 0)
                        //if(!in_array($tab_number_1_based,$tabArray))
                        continue;
                }


                if ($count == $feature)
                    $row["Feature"] = '--featured';
                else
                    $row["Feature"] = '';

                if ($partial == '') {
                    $include_partial = $row["Partial"];  // use partial from ContentCategoryType
                } else {
                    $include_partial = $partial;
                }
                include $include_partial;
                $count += 1;
            }
        }
    }

    /** If the given widget has extras, then we retrieve them from the db
     **/
    public function getWidgetExtras($widget)
    {

        if (isset($widget['ContentCategoryTypeID']) && ($widget['ContentCategoryTypeID'] == 30 ||  $widget['ContentCategoryTypeID'] == 48)) {

            $params =     array(
                "SkinID"        => array(config("SkinID"), "int", 0),
                "Device"        => array(config("RealDevice"), "int", 0),
                "CategoryID"    => array($widget['Text1'], "int", 0),
                "limit"         => array($widget['Text2'], "int", 0),
                "Preview"       => array(config("Edit"), "int", 0)
            );



            $catGames = array();
            $catGames = queryDB($this->db, "WebGetGamesByCategory", $params, true, false, $rs);
            $widget['Games'] = $catGames;


            $category = queryDBNextRS($rs);
            $widget['Url'] = '/slots/' . $category[0]['CategoryKey'];
        }
        return $widget;
    }

    /* ---------------------------------------------------------------------------------
    INCLUDE THIS VIEW (PARTIAL) FOR EVERY ROW OF DATA FOR TEMPLATING FROM THE GAMES OBJECT
    ----------------------------------------------------------------------------------*/
    public function getPartial($dataObject, $index_1_based = 0, $partial = '')
    {
        if (is_int($index_1_based)) {
            $index = $index_1_based - 1;

            if (!isset($dataObject))
                $dataObject = array();

            if (is_array($dataObject)) {
                if (isset($dataObject[$index])) {
                    $row = $dataObject[$index];
                    if ($partial == '')
                        $partial = $row["Partial"];  // use partial from ContentCategoryType


                    include $partial;
                }
            }
        } else {
            if (!isset($dataObject))
                $dataObject = array();

            if (is_array($dataObject)) {
                foreach ($dataObject as $row) {
                    $row = $this->getWidgetExtras($row);
                    if ($row["DetailsURL"] == $index_1_based) {
                        if ($partial == '')
                            $partial = $row["Partial"];  // use partial from ContentCategoryType

                        include $partial;
                        continue;
                    }
                }
            }
        }
    }

    /*---------------------------------------------------------------------------
    GET THE DESCRIPTION FROM THE KEY (ACTION) FROM THE PASSED DATASET
    ---------------------------------------------------------------------------*/
    function getDescriptionFromKey($dataObject, $key)
    {
        foreach ($dataObject as $row) {
            if ($row["Key"] == $key) return $row["Description"];
        }
        return '';
    }



    /* ---------------------------------------------------------------------------------
    INCLUDE THIS VIEW (PARTIAL) FOR EVERY ROW OF DATA FOR TEMPLATING
    ----------------------------------------------------------------------------------*/
    /*
    public function repeatData($category='',$partial)
    {
        if($category=='')
        {
            foreach($this->data as $this->row)
            {
                include $partial;
            }
        }
        else
        {
            foreach($this->data as $this->row)
            {
                if(isset($this->row["Category"]) && stripos($this->row["Category"],$category) !== false)
                {
                    include $partial;
                }
            }
        }
        
    }
    */

    /* ---------------------------------------------------------------------------------
    FIND THE FIRST ROW WHERE THE FEATURE COLUMN = 1
    LOAD IT INTO $this->feature
    ----------------------------------------------------------------------------------*/
    public function getFeature()
    {
        foreach ($this->data as $row) {
            if (isset($row["Feature"]) && $row["Feature"] == 1) {
                $this->feature = $row;
            }
        }
    }



    /* ---------------------------------------------------------------------------------
    INCLUDE THIS VIEW IF WE ARE LOADING THE WHOLE PAGE FROM THE SERVER
    ----------------------------------------------------------------------------------*/
    public function getFullPage($viewName)
    {

        if ($this->fullPage) include $viewName;
    }

    /* ---------------------------------------------------------------------------------
    INCLUDE THIS VIEW IF WE ARE LOADING THE WHOLE PAGE FROM THE SERVER INCLUDING PARAMS
    ----------------------------------------------------------------------------------*/
    public function getFullPageWithParams($viewName, $params)
    {

        extract($params);
        if ($this->fullPage) include $viewName;
    }

    public function checkViewExists($viewName)
    {

        if (stream_resolve_include_path($viewName) === false) {
            if ($this->fullPage) {
                // server side
                header("HTTP/1.0 404 Not Found");
                require_once '_partials/common/head.php';
                require_once '_partials/common/header.php';
                require_once '_views/error/error.php';
                require_once '_partials/common/footer.php';
                exit;
            } else {
                // client side
                require_once '_views/error/error.php';
            }
        }
    }



    /* ---------------------------------------------------------------------------------
    GET VIEW BY FILENAME
    ----------------------------------------------------------------------------------*/
    public function getView($viewName)
    {
        $ipath = join("\n", explode(PATH_SEPARATOR, get_include_path()));

        if (stream_resolve_include_path($viewName) === false) {
            header("HTTP/1.0 404 Not Found");
            $this->getFullPage('_partials/common/head.php');
            $this->getFullPage('_partials/common/header.php');
            $this->getView('_views/error/error.php');
            $this->getFullPage('_partials/common/footer.php');
        } else {
            require_once $viewName;
        }
    }


    /* ---------------------------------------------------------------------------------
    RETURNS TRUE IF THE VIEW EXISTS IN THE CACHE
    ----------------------------------------------------------------------------------*/
    public function cached($key = '')
    {
        if (!$this->useMemcache) return false;

        if ($key == '') $key = $this->cache_key . '_' . config('RealDevice');

        if ($this->memcache->add($key, '') === false) // failed to add the key means the item already exists
        {
            return true;
        } else {
            $this->memcache->delete($key);
            return false;
        }
    }


    /* ---------------------------------------------------------------------------------
    GET CATEGORY DESCRIPTION LINK
    ----------------------------------------------------------------------------------*/
    public function getCategoryHref($linkText = '')
    {

        if (@$this->category["Link"] == 0) // nothing deeper
        {
            if ($linkText == '') {
                return @$this->category["Description"]; //so just return the description
            } else {
                return ''; //return nothing
            }
        }
        $key = str_replace('hub_', '', @$this->category["Key"]);

        if ($this->controller_url == 'home' || $this->controller_url == 'games') {
            $ret = strtolower('/' . $key . '/');
        } else {
            if ($this->controller_url == 'lobby') { // if lobby page we add the category as sub-page

                $ret = strtolower('/' . $this->category["Category"] . '/' . $key . '/');
            } else {
                $ret = strtolower('/' . $this->controller_url . '/' . $key . '/');
            }
        }

        if ($linkText != '') {
            return '<a data-hijack="true" href="' . $ret . '">' . $linkText . '</a>';
        } else {
            return '<a data-hijack="true" href="' . $ret . '">' . @$this->category["Description"] . '</a>';
        }
    }

    /* ---------------------------------------------------------------------------------
    GET GAME LINK
    ----------------------------------------------------------------------------------*/
    public function getGameHref($category)
    {
        if ($category == '') return '';


        $key = str_replace('hub_', '', @$this->category["Key"]);

        if ($this->controller_url == 'home' || $this->controller_url == 'games') {
            $ret = strtolower('/' . $category . '/game/');
        } else {
            $ret = strtolower('/' . $this->controller_url . '/game/');
        }
        return $ret;
    }

    /* ---------------------------------------------------------------------------------
    GET A ALREADY RENDERED VIEW FROM MEMCACHE
    ----------------------------------------------------------------------------------*/
    public function getViewAndCache($viewName, $action = '', $useCache = true)
    {
        if (!$this->useMemcache || !$useCache) {
            $this->loadedFromCache = 'cache off';
            require_once $viewName;
        } else {
            // add NV to key for non-verified users
            $mcKey = config('AccessLevel') ?
                join('_', [$this->cache_key, config('RealDevice')]) : join('_', [$this->cache_key, config('RealDevice'), 'NV']);

            $view = $this->memcache->get($mcKey);

            if ($view == null) {

                ob_start();
                require_once $viewName;
                $view = ob_get_contents();
                ob_end_clean();
                $this->memcache->set($mcKey, $view, false, config("Memcache_expires_seconds"));
                $this->loadedFromCache = 'cache empty - refreshed key:' . $this->cache_key . '_' . config('RealDevice');
                echo $view;
            } else {
                $this->loadedFromCache = 'loaded from cache :' . $mcKey;
                echo $view;
            }
        }
    }

    /* ---------------------------------------------------------------------------------
    GET THE NEXT CATEGORY AND SET OF GAMES
    ----------------------------------------------------------------------------------*/
    public function GetNextCategory()
    {
        $nextArray = queryDBNextRS($this->rs, false);
        if (is_array($nextArray) && count($nextArray) > 0) {
            $this->category = $nextArray;
            $this->games = queryDBNextRS($this->rs);
            return true;
        }
        return false;
    }



    public function checkInternalIP()
    {

        $ip = getPlayerIP();

        return !Env::isLive() || in_array($ip, config("ips"));
    }

    private function getRealDevice()
    {
        if (config("RealDevice") === 1) {
            return "Web";
        } elseif (config("RealDevice") === 2) {
            return "Tablet";
        } elseif (config("RealDevice") === 3) {
            return "Mobile";
        }
    }

    public function callMicroservice($apiUrl, $endPoint, $params, $version = '0',  $verb = 'GET', $body = [], $removeApiFromURL = false)
    {

        try {
            if ($removeApiFromURL) {
                $res = $this->requestMicroService($apiUrl, $endPoint, $params, $version, $verb, true, true);
            } else {
                switch ($verb) {
                    case 'PATCH':
                    case 'GET':
                        $res = $this->requestMicroService($apiUrl, $endPoint, $params, $version, $verb);
                        break;
                    case 'POST':
                    case 'PUT':
                        $res = $this->requestMicroService($apiUrl, $endPoint, $params, $version, $verb, $body);
                        break;
                    default:
                        $res = null;
                        break;
                }
            }

            if ($res["httpStatus"] == 500) {

                // send email
                $subject = "Micro service 500 error";
                $message = "{$apiUrl}/api/v{$version}/{$endPoint}/ \r\n" . json_encode($res['body']);

                $from = "donotreply@spacebarmedia.com";
                $to = "Mohtadib@spacebarmedia.com, celestinom@spacebarmedia.com, BobK@srgservicesltd.com, GabrielN@spacebarmedia.com, RafaelS@spacebarmedia.com, ShrideviR@spacebarmedia.com";
                $headers = "Content-type: text/plain; charset=iso-8859-1\r\n";
                $headers .= "From: $from\r\n";
                mail($to, $subject, $message, $headers);
            }

            return $res;
        } catch (Exception $e) {

            // send email
            $subject = "Micro service 500 error with exception";
            $message = "{$apiUrl}/api/v{$version}/{$endPoint}/ \r\n" . $e->getMessage();

            $from = "donotreply@spacebarmedia.com";
            $to = "Mohtadib@spacebarmedia.com, celestinom@spacebarmedia.com, BobK@srgservicesltd.com, GabrielN@spacebarmediacom, RafaelS@spacebarmediacom, ShrideviR@spacebarmedia.com";
            $headers = "Content-type: text/plain; charset=iso-8859-1\r\n";
            $headers .= "From: $from\r\n";
            //mail($to,$subject,$message,$headers);
        }
    }

    private function requestMicroService($apiUrl, $endPoint, $params, $version = '0',  $verb = 'GET', $json = null, $removeApiFromURL = false)
    {

        require_once("autoload.php");
        $client = new GuzzleHttp\Client(['verify' => false, 'exceptions' => false]);
        $options = ['query' => $params];
        if (isset($json)) {
            // this automagically makes Content-Type: application/json
            $options['json'] = $json;
            $options['headers'] = ['Cache-Control' =>  'no-cache'];
        }
        if ($removeApiFromURL) {
            $res = $client->request($verb, "{$apiUrl}/v{$version}/{$endPoint}/", $options);
        } else {
            $res = $client->request($verb, "{$apiUrl}/api/v{$version}/{$endPoint}/", $options);
        }

        $response = ['httpStatus' => $res->getStatusCode(), 'body' => json_decode($res->getBody()->getContents(), true)];

        return $response;
    }
    /******* REST Request with Guzzle for general usage
    $verb = Verb for request (POST, PUT, GET)
    $url = URL to request
    $options = Additional parameters for the request
    $decode = Decode received object from JSON
     ********/
    public function simpleRESTRequest($verb = "GET", $url = "data.php", $options = [], $decode = true)
    {
        require_once("autoload.php");
        $response = ['httpStatus' => 500, 'body' => "REST error"];
        $client = new GuzzleHttp\Client(['verify' => false, 'exceptions' => false]);
        $res = $client->request($verb, $url, $options);
        $body = $decode ? json_decode($res->getBody()->getContents(), true) : $res->getBody()->getContents();
        $body_object = new stdClass();
        foreach ($body as $key => $value) {
            $body_object->$key = $value;
        }
        $response = ['httpStatus' => $res->getStatusCode(), 'body' => $body_object];
        return $response;
    }
    /******* Log Handling for different use cases
    $folderName = Folder name
    $message = Message to be added to the logfile
    $addIp = Append user IP at the end of the logfile line
     ********/
    public function logToFile($folderName = "radlog", $message = "Log action", $appendIp = true)
    {
        $folderName = config("Logs") . $folderName;
        if (!is_dir($folderName)) {
            //Create our directory if it does not exist
            mkdir($folderName);
        }
        $fileName = $folderName . "\\" . date("Ymd") . ".log";
        $line = config("SkinID") . "\t" . date("H:i:s") . "\t" . $message;
        if ($appendIp) {
            $line .= "\t" . getPlayerIP();
        }
        $line .= "\r\n";
        if (!is_file($fileName)) {
            file_put_contents($fileName, $line);     // Save our content to the file.
        } else {
            file_put_contents($fileName, $line, FILE_APPEND);
        }
    }


function getGamesFromCache ($gamesCategoryName) {


    // I bring the games from cache
    // If I don't have those games I go to the DB -- first time the site loads or after restart
    // If I have them I just return them


    // Get games from the cache
    $gamesCategory = $this->memcache->get($gamesCategoryName.'_skin_'.config("SkinID"));

    // Si no hay juegos tenemos que acceder a la base de datos para obtenerlos y cachearlos
    if (empty($gamesCategory)) {


        //sleep(2);
        $this->openDB();

        $this->content = $this->loadModel("ContentModel")->getContent($this->controller);
        $this->action = 'slots';

        $gamesModel = $this->loadModel("GamesModel");
        $this->category = $gamesModel->getGames($gamesCategoryName);
        $this->rs = $gamesModel->rs;
        $this->games = queryDBNextRS($this->rs);

        $gamesCategory['info'] = $this->category;
        $gamesCategory['games'] = $this->games;
        $gamesCategory['children'] = [];


//            while ($this->GetNextCategoryFromDB()) {
//                if(count((array)$this->games) > 0) {
//                    $childrenGamesCategory['info'] = $this->category;
//                    $childrenGamesCategory['games'] = $this->games;
//
//                    array_push($gamesCategory['children'], $childrenGamesCategory);
//
//            }} ;
//
//            $this->GetNextCategoryFromDB();

    }

    $gamesCategory['iterator'] = 0;
    return $gamesCategory;

}

    function isLastChildrenCategory ($gamesCategory) {
        return $gamesCategory['iterator'] == count($gamesCategory['children']);
    }



    function recordsetGamesHandler( $description ){
            $configName = $_SERVER['SPACEBAR_CONFIG'] ?? '';
            $config = Config::loadConfig( $configName);
            $theFileData = 'logDbAcess';
            $fp = fopen("L:/DbLogs/" . date("Ymd") . ".log", "a");
            fwrite($fp,  "$description \r\n");
            fclose($fp);
    }

}