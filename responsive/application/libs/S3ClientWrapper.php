<?php

require_once("autoload.php");

/**
 * Created by PhpStorm.
 * User: josem
 * Date: 31/07/2017
 * Time: 23:02
 */
class S3ClientWrapper
{
    const BUCKET_NAME = 'rad-images-landing';
    const ACCESS_KEY = 'AKIAIHQDZE664LBSDMSQ';
    const ACCESS_SECRET = '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';
    const REGION = 'eu-west-2';
    const SIGNATURE = 'v4';

    private $client;
    private $prefix;

    public function __construct()
    {
        $this->client = \Aws\S3\S3Client::factory(array(
                'region' => self::REGION,
                'signature' => self::SIGNATURE,
                'credentials' => array(
                    'key'    => self::ACCESS_KEY,
                    'secret' => self::ACCESS_SECRET
                )
            )
        );
    }

    public function listObjects() {
        return $this->client->getIterator('ListObjects', array(
            'Bucket' => self::BUCKET_NAME,
            'Prefix' => $this->prefix
        ));
    }

    public function putObject($key, $object) {
        return $this->client->upload(self::BUCKET_NAME, $key, $object, 'public-read');
    }

    public function getNextLevel($string) {
        $removePrefix = str_replace($this->prefix,'', $string);
        $removePrefix = trim($removePrefix, '/');
        $spliced = explode('/',$removePrefix);
        return current($spliced);
    }

    public function getFilename($string) {
        $removePrefix = str_replace($this->prefix,'', $string);
        $removePrefix = trim($removePrefix, '/');
        $spliced = explode('/',$removePrefix);
        if (count($spliced) == 1) {
            return $spliced[0];
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }
}