<?php

$skin = @$_REQUEST["skin"];
$device = @$_REQUEST["device"];

/*
	Group names match the linkage in the lobby.swf, do not have spaces in them and are translated by the language file
	Room node order is matched to the order of "pos" naming in the group clip
	possible attributes for group nodes: name * , type * , userData, background
*/


switch($skin)
{
	case "Kitty":
		$response = '{
			"groups": [
					{
						"name": "Club", "linkage": "Club", "type": "server",
						"rooms":
						[
							{"description": "Free Bingo", "id": "133", "css": "room_club"},
							{"description": "Free 50", "id": "104", "css": "room_club"},
							{"description": "Silver £100 a Day", "id": "135", "css": "room_club"},
							{"description": "Gold £500 a week", "id": "136", "css": "room_club"},
							{"description": "Ruby £1000 a month", "id": "137", "css": "room_club"},
							{"description": "Bonus Booster", "id": "115", "css": "room_club"}
						]
					},
					{
						"name": "Bingo", "linkage": "Bingo", "type": "server",
						"rooms":
						[
							{"description": "Kitty Cat", "id": "106", "css": "room_bingo"},
							{"description": "After Dark", "id": "156", "css": "room_bingo"},
							{"description": "Splish Splash", "id": "101", "css": "room_bingo"},
							{"description": "Fat Cat", "id": "110", "css": "room_bingo"},
							{"description": "Penny Heaven", "id": "112", "css": "room_bingo"},
							{"description": "Turbo", "id": "218", "css": "room_bingo"},
							{"description": "Furry 5", "id": "122", "css": "room_bingo"},
							{"description": "Newbie Room", "id": "207", "css": "room_bingo"},
							{"description": "LobbyTest", "id": "0", "css": "room_bingo"}
						]
					},
					{
						"name": "Prebuy", "linkage": "Prebuy", "type": "server",
						"rooms":
						[
							{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
							{"description": "Fiver Frenzy", "id": "246", "css": "room_newbie"},
							{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
							{"description": "Win Back Wednesday", "id": "208", "css": "room_newbie"},
							{"description": "Pandora\'s Mystery Box", "id": "113", "css": "room_newbie"},
							{"description": "The Big Dabber", "id": "117", "css": "room_newbie"}
						]
					},
					{
						"name": "No Limits", "linkage": "NoLimits", "type": "server",
						"rooms":
						[
							{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
							{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
							{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
						]
					}';
				
				if ($device == 1)
				{
					$response .= '
					,{
						"name": "Slots", "linkage": "Slots", "type": "slots", "instants": "lobbyFeatured",
						"rooms":
						[
						]
					}
					,{
						"name": "Free Spins", "linkage": "FreeSpins", "type": "slots", "instants": "freeSpins", "userData": "BONUSSPINS",
						"rooms":
						[
						]
					}
					';
				}
		$response .= ']
		}';
	break;
	

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	case "Pants":
		$response = '{
			"groups": [
				{
					"name": "Lucky Club", "linkage": "LuckyClub", "type": "server",
					"rooms":
					[
						{"description": "Free Bingo", "id": "133", "css": "room_club"},
						{"description": "Free 50", "id": "104", "css": "room_club"},
						{"description": "Silver £100 a Day", "id": "135", "css": "room_club"},
						{"description": "Gold £500 a week", "id": "136", "css": "room_club"},
						{"description": "Ruby £500 a month", "id": "137", "css": "room_club"},
						{"description": "Welcome £40", "id": "115", "css": "room_club"}
					]
				},
				{
					"name": "Bingo", "linkage": "Bingo", "type": "server",
					"rooms":
					[
						{"description": "Lucky Stripes", "id": "201", "css": "room_bingo"},
						{"description": "After Dark", "id": "156", "css": "room_bingo"},
						{"description": "Red Hearts", "id": "101", "css": "room_bingo"},
						{"description": "Fat Cat", "id": "110", "css": "room_bingo"},
						{"description": "Penny Heaven", "id": "112", "css": "room_bingo"},
						{"description": "Droopy Drawers", "id": "122", "css": "room_bingo"},
						{"description": "Newbie", "id": "207", "css": "room_bingo"},
						{"description": "Turbo", "id": "218", "css": "room_bingo"}
					]
				},
				{
					"name": "Prebuys", "linkage": "Prebuys", "type": "server",
					"rooms":
					[
						{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
						{"description": "Fiver Frenzy", "id": "246", "css": "room_newbie"},
						{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
						{"description": "Win Back Wednesday", "id": "208", "css": "room_newbie"},
						{"description": "Pandora\'s Mystery Box", "id": "113", "css": "room_newbie"},
						{"description": "The Big Dabber", "id": "117", "css": "room_newbie"}
					]
				},
				{
					"name": "No Limits", "linkage": "NoLimit", "type": "server",
					"rooms":
					[
						{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
						{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
						{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
					]
				}';
				
				if ($device == 1)
				{
					$response .= '
					,{
						"name": "Slots", "linkage": "Slots", "type": "slots", "instants": "lobbyFeatured",
						"rooms":
						[
						]
					}
					,{
						"name": "Freebies", "linkage": "Freebies", "type": "slots", "instants": "freeSpins", "userData": "BONUSSPINS",
						"rooms":
						[
						]
					}
					';
				}
		$response .= ']
		}';
	break;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	case "Extra":
		$response = '{
			"groups": [
				{
					"name": "Bingo", "linkage": "Bingo", "type": "server",
					"rooms":
					[
						{"description": "The Extra Room", "id": "245", "css": "room_bingo"},
						{"description": "The Diner", "id": "110", "css": "room_bingo"},
						{"description": "The Candy Store", "id": "101", "css": "room_bingo"},
						{"description": "The Drive-In", "id": "156", "css": "room_bingo"},
						{"description": "Penny Arcade", "id": "112", "css": "room_bingo"},
						{"description": "Turbo", "id": "218", "css": "room_bingo"},
						{"description": "Lucky Lanes", "id": "122", "css": "room_bingo"},
						{"description": "Newbie Room", "id": "207", "css": "room_bingo"}
					]
				},
				{
					"name": "Prebuy", "linkage": "Prebuy", "type": "server",
					"rooms":
					[
						{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
						{"description": "Win Back Wednesday", "id": "208", "css": "room_newbie"},
						{"description": "Extra Surprise", "id": "113", "css": "room_newbie"},
						{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
				        {"description": "The Jukebox", "id": "117", "css": "room_newbie"},
						{"description": "The Roller Rink", "id": "246", "css": "room_newbie"}
					]
				},
				{
					"name": "No Limits", "linkage": "NoLimits", "type": "server",
					"rooms":
					[
						{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
						{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
						{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
					]
				}';
				
				if ($device == 1)
				{
					$response .= '
					,{
						"name": "Slots", "linkage": "Slots", "type": "slots", "instants": "lobbyFeatured",
						"rooms":
						[
						]
					}
					,{
						"name": "Free Spins", "linkage": "FreeSpins", "type": "slots", "instants": "freeSpins", "userData": "BONUSSPINS",
						"rooms":
						[
						]
					}
					';
				}
		$response .= ']
		}';
	break;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	case "Bubbly":
		$response = '{
			"groups": [
				{
					"name": "Bingo", "linkage": "Bingo", "type": "server",
					"rooms":
					[
						{"description": "Bubble Trouble", "id": "245", "css": "room_bingo"},
						{"description": "The Fizz Room", "id": "110", "css": "room_bingo"},
						{"description": "Frothy Fun", "id": "101", "css": "room_bingo"},
						{"description": "Watershed", "id": "156", "css": "room_bingo"},
						{"description": "Popping Pennies", "id": "112", "css": "room_bingo"},
						{"description": "Turbo", "id": "218", "css": "room_bingo"},
						{"description": "The Whirlpool", "id": "122", "css": "room_bingo"},
						{"description": "Newbie Room", "id": "207", "css": "room_bingo"}
					]
				},
				{
					"name": "Prebuy", "linkage": "Prebuy", "type": "server",
					"rooms":
					[
						{"description": "Fiver Frenzy", "id": "246", "css": "room_newbie"},
						{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
						{"description": "Win Back Wednesday", "id": "208", "css": "room_newbie"},
						{"description": "Treasure Chest", "id": "113", "css": "room_newbie"},
						{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
						{"description": "Soap \'n\' Suds", "id": "117", "css": "room_newbie"}
					]
				},
				{
					"name": "No Limits", "linkage": "NoLimits", "type": "server",
					"rooms":
					[
						{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
						{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
						{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
					]
				}
			]
		}
		';
	break;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	case "GiveBack":
		$response = '{
				"groups": [
							{
								"name": "VIP Club", "linkage": "VIPClub", "type": "server",
								"rooms":
								[
									{"description": "Free Bingo", "id": "133", "css": "room_club"},
									{"description": "Free 50", "id": "104", "css": "room_club"},
									{"description": "Silver £100 a Day", "id": "135", "css": "room_club"},
									{"description": "Gold £500 a week", "id": "136", "css": "room_club"},
									{"description": "Ruby £500 a month", "id": "137", "css": "room_club"},
									{"description": "Welcome £40", "id": "115", "css": "room_club"}
								]
							},
							{
								"name": "Bingo", "linkage": "Bingo", "type": "server",
								"rooms":
								[
									{"description": "Lucky Stripes", "id": "201", "css": "room_bingo"},
									{"description": "After Dark", "id": "156", "css": "room_bingo"},
									{"description": "Red Hearts", "id": "101", "css": "room_bingo"},
									{"description": "Fat Cat", "id": "110", "css": "room_bingo"},
									{"description": "Penny Heaven", "id": "112", "css": "room_bingo"},
									{"description": "Droopy Drawers", "id": "122", "css": "room_bingo"},
									{"description": "Newbie", "id": "207", "css": "room_bingo"},
									{"description": "Turbo", "id": "218", "css": "room_bingo"}
								]
							},
							{
								"name": "Prebuys", "linkage": "Prebuys", "type": "server",
								"rooms":
								[
									{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
									{"description": "Fiver Frenzy", "id": "246", "css": "room_newbie"},
									{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
									{"description": "Win Back Wednesday", "id": "208", "css": "room_newbie"},
									{"description": "Pandora\'s Mystery Box", "id": "113", "css": "room_newbie"},
									{"description": "The Big Dabber", "id": "117", "css": "room_newbie"},
									{"description": "Wild Thing", "id": "254", "css": "room_newbie"},
									{"description": "7 Day\'s FREE Bingo", "id": "252", "css": "room_newbie"},
									{"description": "Fundraiser Frenzy", "id": "253", "css": "room_newbie"}
								]
							},
							{
								"name": "No Limits", "linkage": "NoLimit", "type": "server",
								"rooms":
								[
									{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
									{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
									{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
								]
							}
					';
				
				if ($device == 1)
				{
					$response .= '
					,{
						"name": "Slots", "linkage": "Slots", "type": "slots", "instants": "lobbyFeatured",
						"rooms":
						[
						]
					}
					,{
						"name": "Freebies", "linkage": "Freebies", "type": "slots", "instants": "freeSpins", "userData": "BONUSSPINS",
						"rooms":
						[
						]
					}
					';
				}
		$response .= ']
		}';
	break;
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	default:
		$response = "default";
}


//	callback handler added 23/12/14 - MC
	
	# If a "callback" GET parameter was passed, use its value, otherwise false. Note
	# that "callback" is a defacto standard for the JSONP function name, but it may
	# be called something else, or not implemented at all. For example, Flickr uses
	# "jsonpcallback" for their API.
	$jsonp_callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
	
	if ($jsonp_callback)
	{
		echo $jsonp_callback . '(' . $response . ')';
	}
	else
	{
		echo $response;
	}
?>