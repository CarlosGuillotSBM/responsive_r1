<?php

class Application
{
    /** @var null The controller */
    public $controller = null;
    public $url_controller = null;

    /** @var null The method (of the above controller), often also named "action" */
    public $url_action = null;

    /** @var null Parameter one */
    public $url_params = [];

    public $model = '';

    protected $mobileDetect;


    /**
     * "Start" the application:
     * Analyze the URL elements and calls the according controller/method or the fallback
     */
    public function __construct($controller)
    {
        //echo 'controller/' . $this->url_controller . '.php';
        /*
                // for local previewing
                if($_SERVER["QUERY_STRING"] == "preview=1" || $_SERVER["QUERY_STRING"] == "preview=0")
                {
                    $trimurl = strtok($_SERVER["REQUEST_URI"],'?');
                    header('Location: '.$trimurl);
                    exit;
                }
        */

        if ($controller !== null) {

            // CLIENT SIDE RENDERING
            // THIS MEANS WE ARE JUST LOADING CONTENT NOT THE WHOLE PAGE
            $this->splitUrl($controller);

            if (stream_resolve_include_path('controller/' . $this->url_controller . '.php')) {

                $controller_url_name = $this->url_controller;

                // we have a controller
                require 'controller/' . $this->url_controller . '.php';
                $className = str_replace("-", "", $this->url_controller);


                $this->controller = new $className();

                $this->controller->controller = strtolower(get_class($this->controller));
                $this->controller->controller_url = strtolower($controller_url_name);
                $this->controller->action = strtolower($this->url_action);
                $this->controller->route = array_map( 'strtolower', $this->url_params);


                $this->controller->fullPage = false;

                if (isset($this->url_action)) {
                    $this->controller->action($this->url_action, $this->url_params);
                } else {
                    $this->controller->index();
                }

            } else {
                header("HTTP/1.0 404 Not Found");
                // only return content if we're looking for a clean URL
                if (strrchr($_SERVER["REQUEST_URI"], ".") === false) {
                    require_once 'controller/raderror.php';
                    $this->controller = new RadError();
                    $this->controller->index();
                }
            }

            return;
        } else {
            // create array with URL parts in $url
            $this->splitUrl();
        }

        // SERVER SIDE RENDERING


        // return error page if too many params
        if (count($this->url_params) > 3) {
            header("HTTP/1.0 404 Not Found");
            require_once 'controller/raderror.php';
            $this->controller = new RadError();
            $this->controller->index();
            exit;
        }


        // check for controller: does such a controller exist ?

        if ($this->url_controller === 'novomatic' && (config("SkinID") === 3 || config("SkinID") === 6)) {
          $this->url_controller = 'raderror';
        };

        if (stream_resolve_include_path('controller/' . $this->url_controller . '.php')) {

            // if so, then load this file and create this controller
            // example: if controller would be "car", then this line would translate into: $this->car = new car();

            $controller_url_name = $this->url_controller;
            require 'controller/' . $this->url_controller . '.php';

            $className = str_replace("-", "", $this->url_controller);

            $this->controller = new $className();
            $this->controller->controller = strtolower(get_class($this->controller));
            $this->controller->controller_url = strtolower($controller_url_name);
            $this->controller->action = strtolower($this->url_action);
            $this->controller->route = array_map('strtolower', $this->url_params);

            // default/fallback: call the index() method of a selected controller
            if ($this->url_action != '')
                $this->controller->action($this->url_action, $this->url_params);
            else
                $this->controller->index();
        } else {
            header("HTTP/1.0 404 Not Found");
            // only return content if we're looking for a clean URL
            if (strrchr($_SERVER["REQUEST_URI"], ".") === false) {
                require_once 'controller/raderror.php';
                $this->controller = new RadError();
                $this->controller->index();
            }
            exit;
        }

        //for debugging. uncomment this if you have problems with the URL

        //echo "suck" . $this->controller_url;

        if (config("Debug") && $this->controller->controller_url != 'mailers') {

            //print_r($this->controller);
            global $debug_procs;

            $device = config('RealDevice');

            $params = join('/', $this->url_params);

            echo <<<EOF
Controller: {$this->controller->controller_url}<br />
Action: {$this->url_action}<br />
Parameters: {$params}<br />
Model: {$this->model}<br />
Cache Key: {$this->controller->cache_key} <br />
Device $device
{$this->controller->loadedFromCache}<br />
$debug_procs
<br /><br />
EOF;

        } else {
            echo "<!--";
            echo $this->controller->loadedFromCache;
            echo "-->";
        }

        $this->controller->closeDB();
    }

    /**
     * Get and split the URL
     */
    private function splitUrl($split = '')
    {

        if ($split == '') $split = strtok($_SERVER["REQUEST_URI"], '?');

        // split URL
        $url = ltrim(rtrim($split, '/'), '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);

        // Put URL parts into according properties
        // By the way, the syntax here is just a short form of if/else, called "Ternary Operators"
        // @see http://davidwalsh.name/php-shorthand-if-else-ternary-operators
        $controller = (isset($url[0]) ? $url[0] : null);

        if (empty($controller)) {
            $controller = 'home';
        } else if ($controller === 'error') {
            $controller = 'raderror';
        }
        $this->url_controller = $controller;

        $this->url_action = (isset($url[1]) ? $url[1] : null);
        // limit number of elements
        $this->url_params = array_slice( $url,  2,5);
    }
}
