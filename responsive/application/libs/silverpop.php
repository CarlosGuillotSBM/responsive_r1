<?php

class Silverpop
{

	private $silverpop = array();
	private $transact = array();	
	private $transacttoken;

	
    public function __construct()
    {
	
		$this->silverpop['pod_number'] = 3;
		$this->silverpop['url'] = 'https://api'.$this->silverpop['pod_number'].'.silverpop.com';
		$this->silverpop['transact_url'] = 'http://transact'.$this->silverpop['pod_number'].'.silverpop.com';
		
		//original ones for engage  apiuser@spacebarmedia.com
		$this->silverpop['client_id'] = '5713998d-3082-4cf7-9cd9-8d49c0dac8a1';
		$this->silverpop['client_secret'] = '1778ba95-cec1-4018-8d00-4a2ac038bbd9';
		$this->silverpop['refresh_token'] = '9c7014b6-8cdb-4bce-84e7-fbc1bc64a47c';		

		// transact tokens for transact1@spacebarmedia.com
		$this->transact['url'] = 'https://api'.$this->silverpop['pod_number'].'.silverpop.com';
		$this->transact['client_id'] = '36adc4c8-99ed-4f83-907a-8b3f0bb94ca8';
		$this->transact['client_secret'] = 'ca8eefe9-27c1-47b1-a85c-09b1a6dcb0b2';
		$this->transact['refresh_token'] = 'bfa55dd7-fa24-4547-a64e-81a6fd71ee1a';			
	}
		

/*
	SEND A REGISTRATION MAIL USING TRANSACT
	https://kb.silverpop.com
	https://kb.silverpop.com/kb/Engage/Automation/Automated_Messages/0001_How_to/Assign_a_Mailing_to_the_Automated_Message
	


*/
	public function sendRegistrationMail($player)
	{		
	
		$transacttoken = $this->getSilverpopToken($this->transact);
	
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><XTMAILING/>');        	
		$xmlBody = $xml->addChild('CAMPAIGN_ID',config('Silverpop_register_campaign_id'));
		$xmlBody = $xml->addChild('SHOW_ALL_SEND_DETAIL','true');
		$xmlBody = $xml->addChild('SEND_AS_BATCH','false');
		$xmlBody = $xml->addChild('NO_RETRY_ON_FAILURE','false');

		// save some columns	
		$xmlSaveColumns = $xml->addChild('SAVE_COLUMNS');		
		$xmlSaveColumns->addChild('COLUMN_NAME','PlayerID');
		$xmlSaveColumns->addChild('COLUMN_NAME','Mail_Sent');


		// add recipient
		$xmlPlayer = $xml->addChild('RECIPIENT');						
		$xmlPlayer->addChild('EMAIL',$player['Email']);
		$xmlPlayer->addChild('BODY_TYPE','HTML');
		
		
		// add personalization fields
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','PlayerID');
		$xmlPersonalization->addChild('VALUE',$player['PlayerID']);

		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','FIRSTNAME');
		$xmlPersonalization->addChild('VALUE',$player['Firstname']);
		
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','ALIAS');
		$xmlPersonalization->addChild('VALUE',$player['Username']);

		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','URL');
		$xmlPersonalization->addChild('VALUE',config("URL"));
		
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','CURRENCYSYMBOL');				
		$xmlPersonalization->addChild('VALUE',@$player['CurrencySymbol']);				
		
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','Mail_Sent');
		$xmlPersonalization->addChild('VALUE','Register');
		
		$request = $xml->asXML();
		
		

		$ch = curl_init();
		if ($ch) 
		{  			
			curl_setopt($ch, CURLOPT_URL, $this->silverpop['transact_url'].'/XTMail');		
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$transacttoken));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'xml='.utf8_decode($request));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
			curl_setopt($ch, CURLOPT_TIMEOUT, 180);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_ENCODING ,"UTF-8");

			
			
			// don't wait
			//curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			//curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);			
			
			$response = curl_exec ($ch);		
			$xml_response = simplexml_load_string($response);
		}
		curl_close ($ch);			
		
		if(@$xml_response->Body->RESULT->SUCCESS != "TRUE") 
		{
			return ' Send Mail Success: ';		
		}
		else
		{
			return print_r($xml_response,true);
		}						
	}	


	public function sendForgotPasswordMail($player)
	{		
	
		$transacttoken = $this->getSilverpopToken($this->transact);
	
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><XTMAILING/>');        	
		$xmlBody = $xml->addChild('CAMPAIGN_ID',config('Silverpop_forgot_pwd_campaign_id'));
		$xmlBody = $xml->addChild('SHOW_ALL_SEND_DETAIL','true');
		$xmlBody = $xml->addChild('SEND_AS_BATCH','false');
		$xmlBody = $xml->addChild('NO_RETRY_ON_FAILURE','false');

		// save some columns	
		$xmlSaveColumns = $xml->addChild('SAVE_COLUMNS');		
		$xmlSaveColumns->addChild('COLUMN_NAME','PlayerID');
		$xmlSaveColumns->addChild('COLUMN_NAME','Mail_Sent');


		// add recipient
		$xmlPlayer = $xml->addChild('RECIPIENT');						
		$xmlPlayer->addChild('EMAIL',$player['Email']);
		$xmlPlayer->addChild('BODY_TYPE','HTML');
		
		
		// add personalization fields
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','PlayerID');
		$xmlPersonalization->addChild('VALUE',$player['PlayerID']);
		
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','GUID');				
		$xmlPersonalization->addChild('VALUE',@$player['GUID']);

		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','FIRSTNAME');
		$xmlPersonalization->addChild('VALUE',$player['Firstname']);
		
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','ALIAS');
		$xmlPersonalization->addChild('VALUE',$player['Username']);

		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','URL');
		$xmlPersonalization->addChild('VALUE',config("URL"));
		
		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','Mail_Sent');
		$xmlPersonalization->addChild('VALUE','ForgotPassword');

		$xmlPersonalization = $xmlPlayer->addChild('PERSONALIZATION');	
		$xmlPersonalization->addChild('TAG_NAME','CURRENCYSYMBOL');				
		$xmlPersonalization->addChild('VALUE',@$player['CurrencySymbol']);	

		
		$request = $xml->asXML();
		

		$ch = curl_init();
		if ($ch) 
		{  			
			curl_setopt($ch, CURLOPT_URL, $this->silverpop['transact_url'].'/XTMail');		
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$transacttoken));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'xml='.utf8_decode($request));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
			curl_setopt($ch, CURLOPT_TIMEOUT, 180);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_ENCODING ,"UTF-8");

			$response = curl_exec ($ch);			
		}
		curl_close ($ch);			
		
	}	
	
	
	
	/*
	
		ADD THE PLAYER TO THE SILVERPOP DATABASE

	$player['Email'] = 'hamishi@spacebarmedia.com';
	$player['Country'] = 'UNITED KINGDOM';
	$player['Excluded'] = 'False';
	$player['FirstName'] = 'Hamish';
	$player['Funded'] = '';
	$player['Gender'] = 'Male';
	$player['Language'] = 'en';	
	$player['PlayerID'] = 548355;	
	$player['Skin'] = 'Bingo Extra';		

Array
(
    [Code] => 0
    [RegEmailValidation] => 0
    [PromoEmailValidation] => 0
    [ValidationGUID] => 
    [PlayerID] => 11854
    [Username] => hamish125
    [Email] => hamish125@yopmail.com
    [Firstname] => Hamish
    [Password] => qqq111
    [RegStatus] => 1
    [EmailType] => normal
    [CurrencySymbol] => Â£
    [FullName] => Hamish Inglis
    [CountryName] => UNITED KINGDOM 
    [SkinName] => Magical Vegas
    [CurrencyCode] => GBP
)
	
	
	*/
    public function addPlayer($player)
	{

		$silverpoptoken = $this->getSilverpopToken($this->silverpop);


		
		$xml = new SimpleXMLElement('<Envelope/>');        	
		$xmlBody = $xml->addChild('Body');
		$xmlPlayer = $xmlBody->addChild('AddRecipient');
		$xmlPlayer->addChild('LIST_ID',config('Silverpop_list_id'));
		$xmlPlayer->addChild('CREATED_FROM',1);
		$xmlPlayer->addChild('UPDATE_IF_FOUND','true');
		$xmlPlayer->addChild('EMAIL_TYPE',0);
		$xmlPlayer->addChild('OPT_IN_DETAILS','Registration');
						
		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','Email');
		$xmlColumn->addChild('VALUE',$player['Email']);		

						
		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','Country');
		$xmlColumn->addChild('VALUE',$player['CountryName']);		

		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','CurrencyCode');
		$xmlColumn->addChild('VALUE',$player['CurrencyCode']);		

		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','Firstname');
		$xmlColumn->addChild('VALUE',$player['Firstname']);				

		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','FullName');
		$xmlColumn->addChild('VALUE',$player['FullName']);					
		
		
		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','PlayerID');
		$xmlColumn->addChild('VALUE',$player['PlayerID']);	
		
		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','Skin');
		$xmlColumn->addChild('VALUE',$player['SkinName']);	

		$xmlColumn = $xmlPlayer->addChild('COLUMN');
		$xmlColumn->addChild('NAME','Username');
		$xmlColumn->addChild('VALUE',$player['Username']);	
		
		
		$request = $xml->asXML();
					
		
		$ch = curl_init();
		if ($ch) 
		{  			
			curl_setopt($ch, CURLOPT_URL, $this->silverpop['url'].'/XMLAPI');		
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$silverpoptoken));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'xml='.$request);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
			curl_setopt($ch, CURLOPT_TIMEOUT, 180);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_ENCODING ,"UTF-8");
			
			$response = curl_exec ($ch);				
			$xml_response = simplexml_load_string($response);
			
			curl_close ($ch);	
			
			if(@$xml_response->Body->RESULT->SUCCESS == 'TRUE')
			{
				// player added successfully to silverpop DB
				return ' Add Player Success: ';
			}
			else
			{
				// returned from call error
				return print_r(@$xml_response,true);
			}					
		}
		else
		{
			// curl failed
			return ' Add Player Curl Fail';	
		}					
	
	}

    public function previewMail($player)
	{
		$silverpoptoken = $this->getSilverpopToken($this->silverpop);
		

		echo ("Silver Previeeewwww!!!");

		if(strlen($silverpoptoken) == 36)
		{

		
			$xml = new SimpleXMLElement('<Envelope/>');        	
			$xmlBody = $xml->addChild('Body');
			$xmlTemplate = $xmlBody->addChild('PreviewMailing');
			$xmlTemplate->addChild('MailingId','23086604');
							
			
			
			$request = $xml->asXML();
						
			
			$ch = curl_init();
			print_r($ch);
			if ($ch) 
			{  			
				curl_setopt($ch, CURLOPT_URL, $this->silverpop['url'].'/XMLAPI');		
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$silverpoptoken));
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, 'xml='.$request);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
				curl_setopt($ch, CURLOPT_TIMEOUT, 180);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

				$response = curl_exec ($ch);				
				$xml_response = simplexml_load_string($response);
				
				curl_close ($ch);	
				print_r($response);
				
				if(@$xml_response->Body->RESULT->SUCCESS == 'TRUE')
				{
					// player added successfully to silverpop DB
					return true;			
				}
				else
				{
					// returned from call error
					return false;
				}					
			}
			else
			{
				// curl failed
				return false;
			}					
		}
		else
		{
			// failed getting token
			return false;
		}	
	}

	
	/*
		GET A OAUTH SECURITY TOKEN
	*/	
	private function getSilverpopToken($silverpop)
	{
		$silverpop_access_token = '';
		
		$params = array(
		  "grant_type"    => 'refresh_token',
          "client_id"     => $silverpop['client_id'],
          "client_secret" => $silverpop['client_secret'],
          "refresh_token" => $silverpop['refresh_token']
           );

		   
		$ch = curl_init();
		if ($ch) 
		{  
			$request = http_build_query($params);
			curl_setopt($ch, CURLOPT_URL, $silverpop['url'].'/oauth/token');		
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
			curl_setopt($ch, CURLOPT_TIMEOUT, 180);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

			$response = @curl_exec ($ch);

			
			
			
			
			
			if($this->isJson($response))
			{			
				$silverpop_access_token = json_decode($response)->access_token;
			}
			
		}
		curl_close ($ch);
		
		return $silverpop_access_token;
	}

	
	private function isJson($string) 
	{
	  return ((is_string($string) && 
			 (is_object(json_decode($string)) || 
			 is_array(json_decode($string))))) ? true : false;
	}	
}	


/*
	Country
	CurrencyCode
	Firstname
	FullName
	PlayerID
	Skin
	Username


*/

/*	
	$player['Email'] = 'hamishi@spacebarmedia.com';
	$player['Country'] = 'UNITED KINGDOM';
	$player['Excluded'] = 'False';
	$player['FirstName'] = 'Hamish';
	$player['Username'] = 'HamishMagical';
	$player['Funded'] = '';
	$player['Gender'] = 'Male';
	$player['Language'] = 'en';	
	$player['PlayerID'] = 548355;
	$player['Promotional'] = '';	
	$player['Skin'] = 'Bingo Extra';
	
	$m = new Silverpop;

	$m->addPlayer($player);	
	$m->sendRegistrationMail($player,22319460);	

	echo 'done';




<Envelope>
<Body>
<AddRecipient>
	<LIST_ID>85628</LIST_ID>
<CREATED_FROM>1</CREATED_FROM>
<CONTACT_LISTS>
<CONTACT_LIST_ID>289032</CONTACT_LIST_ID>
<CONTACT_LIST_ID>12345</CONTACT_LIST_ID>

</CONTACT_LISTS>
<COLUMN>
<NAME>Customer Id</NAME>
<VALUE>123-45-6789</VALUE>
</COLUMN>
<COLUMN>
<NAME>EMAIL</NAME>
<VALUE>somebody@domain.com</VALUE>
</COLUMN>
<COLUMN>
<NAME>Fname</NAME>
<VALUE>John</VALUE>
</COLUMN>
</AddRecipient>
</Body>

*/



?>
