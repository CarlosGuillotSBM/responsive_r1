<?php
/*
Interface that provides you with the methods to do HLR checks, listens for SMS delivery receipts and send SMS

*/

interface MobileNetworkManager {
	public function checkHLR ($phoneNumber);
	public function updateSMSStatus ($deliveryReceipt);
	public function singleSms($phoneNumber,$message,$reference,$sender);
}

	
	
?>