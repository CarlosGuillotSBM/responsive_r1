<?php

header("Content-Type: application/javascript");
include_once 'controller/ajax-request/testajax.php';

$function =  @$_REQUEST['f'];

$playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;
$sessionID = (isset($_COOKIE["SessionID"])) ? $_COOKIE["SessionID"] : 'NULL';


//Just to make use of the ajax openDB, as we implemented it abstract
$ajax = new oldDataAjax();
$response = array();
$ajax->initServices();

switch($function)
{
    /*--------------------------------------------------------------------------------------------
    *    !!PLAYER REGISTER - CHECK USERNAME!!                                                   *
    --------------------------------------------------------------------------------------------*/
    case "regcheckusername":

        if(!validate("username",@$_REQUEST["u"]))
        {
            $response["Code"] = 2;
            $response["Msg"] = "Username contains invalid characters";
            break;
        }


        $params =   array("RegSessionID"     => array(@$_REQUEST["rs"], "str", 36),
            "SkinID"             => array(config("SkinID"), "int", 0),
            "GroupID"            => array(@$_REQUEST["g"], "int", 0),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "Username"       => array(@$_REQUEST["u"], "str", 15),
            "IP"                 => array(getPlayerIP(), "str", 50)
        );


        $row = queryDB($ajax->db,"DataRegcheckusername",$params,false,false,$rs);
        $response = addRowToResponse($row);

        if($row["Code"] != 0)
        {
            $response["Msg"] = $row["Msg"];
            $response["Suggest"] = queryDBNextRS($rs,true,true);
        }

        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER REGISTER - CHECK EMAIL
    --------------------------------------------------------------------------------------------*/
    case "regcheckemail":


        if(!validate("email",@$_REQUEST["e"]))
        {
            $response["Code"] = 1;
            $response["Msg"] = "Please enter a valid email address";
            break;
        }


        $params =   array("RegSessionID"     => array(@$_REQUEST["rs"], "str", 36),
            "SkinID"             => array(config("SkinID"), "int", 0),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "Email"          => array(@$_REQUEST["e"], "str", 15),
            "IP"                 => array(getPlayerIP(), "str", 50)
        );


        $row = queryDB($ajax->db,"DataRegcheckemail",$params,false,false,$rs);
        $response = addRowToResponse($row);

        if($row["Code"] != 0)
        {
            $response["Msg"] = $row["Msg"];
        }

        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER REGISTER - CHECK POSTCODE
    --------------------------------------------------------------------------------------------*/
    case "regcheckpostcode":
        $response = CheckPostCodeWith3rdParty(@$_REQUEST["p"]);
        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER REGISTER - GET ADDRESS DETAILS
    --------------------------------------------------------------------------------------------*/
    case "getAddressDetails":

        $response = CheckAddressDetailsWith3rdParty(@$_REQUEST["i"]);
        break;


    case "getnetdeposits":


        $params = [
            'playerID' => $playerID,
            'sessionID' => $sessionID
        ];

        $netDepositsResp = $ajax->callMicroservice(config("PlayerManagementAPI"), 'player/netDeposits', $params, 0, 'GET', []);

        if ($netDepositsResp['httpStatus'] == 200) {
            $response["Code"] = 0;
            $response["Amount"] = $netDepositsResp['body']['amount'];
        } else {
            $response["Code"] = 1;
            $response["Error"] = "Cannot retrieve net deposit";
        }

        break;


    /*--------------------------------------------------------------------------------------------
    PLAYER TRANSACTIONS SUMMARY
    --------------------------------------------------------------------------------------------*/
    case "getPlayerTransactionsSummary":


        $params = [
            'playerId' => $playerID,
            'sessionId' => $sessionID,
            'startDate' => $_REQUEST["startDate"]
        ];

        if (!empty($_REQUEST["endDate"])) {
            $params['endDate'] = $_REQUEST["endDate"];
        }

        $playerTransactionsSummaryResp = $ajax->callMicroservice(config("PlayerManagementAPI"), 'players/history/summary', $params, 0, 'GET', []);


        if ($playerTransactionsSummaryResp['httpStatus'] == 200) {
            $response["Code"] = 0;
            $response["Deposits"] = $playerTransactionsSummaryResp['body']['deposits'];
            $response["CompletedWithdrawals"] = $playerTransactionsSummaryResp['body']['completedWithdrawals'];
            $response["Wagers"] = $playerTransactionsSummaryResp['body']['wagers'];
            $response["Wins"] = $playerTransactionsSummaryResp['body']['wins'];
        } else {
            $response["Code"] = 1;
            $response["Error"] = "Cannot retrieve player transactions summary";
        }


        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER REGISTER
    --------------------------------------------------------------------------------------------*/
    case "register":

        $eventLog = array();

// if ASPERS
        if (config("SkinID") === 12) {

            $url = config("ig-self-exclude");

            if ($url) {

                // for IG we need to tweak the post code because on IG the postcode always have an empty space before the last 3 characters
                // and on our websites we do not allow for empty spaces

                // workaround to fix R10-519
                // store on temporary variable the original postcode without spaces to be assigned later to the $_request
                $originalPostCode = $_REQUEST['pst'];

                $postcocde = $_REQUEST['pst'];
                $firstPart = substr($postcocde, 0, strlen($postcocde) - 3);
                $secondPart = substr($postcocde, -3);
                $_REQUEST['pst'] = $firstPart . "%20" . $secondPart;

                foreach (['dob', 'fn', 'ln', 'pst'] as $field) {
                    if (isset($_REQUEST[$field])) {
                        $params[] = $field . '=' . $_REQUEST[$field];
                    }
                }

                $url = $url . '?' . join( '&', $params);

                // assign the orignal postcode back
                $_REQUEST['pst'] = $originalPostCode;

                try {

                    $curl = curl_init($url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
                    curl_setopt($curl, CURLOPT_CAINFO, __FILE__ . "\\..\\cacert.pem");
                    curl_setopt($curl, CURLOPT_CAPATH, __FILE__ . "\\..\\");
                    $curl_response = curl_exec($curl);

                    if($curl_response === "1") {

                        $response["Code"] = 1;
                        $response["Msg"] = "Oops! There was a problem registering your account. Please contact our Customer Support Team on 08002797632 (Toll Free) or 02038879842 (Landline) quoting the reference ASP-SE";

                        break;
                    }

                } catch (Exception $e) {
                    // do nothing - just ignore
                }

            }

        }

        $gamstopBody = [
            'firstName' => $_REQUEST["fn"],
            'lastName' => $_REQUEST["ln"],
            'email' => $_REQUEST["e"],
            'dateOfBirth' => $_REQUEST["dob"],
            'postcode' => $_REQUEST["pst"]
        ];

        $gamstopResp = $ajax->callMicroservice("http://api.dagacube.net:7100/gamstop", 'registration', [], 0, 'POST', $gamstopBody);
        $gamStopStatus = 'U';

        if ($gamstopResp['httpStatus'] == 200) {
            $gamStopStatus = $gamstopResp['body']['gamStopStatus'];
        }

        if(!validate("username",@$_REQUEST["u"]))
        {
            $response["Code"] = 1;
            $response["Msg"] = "Username contains invalid characters";
            break;
        }

        if(!validate("password",@$_REQUEST["p"]))
        {
            $response["Code"] = 1;
            $response["Msg"] = "Password must contain letters and numbers and be at least 6 characters.";
            break;
        }

        if($gamStopStatus == 'U')
        {
            $response["Code"] = 1029;
            $response["Msg"] = "We couldn't complete the registration process, please try again soon";
            break;
        }


// Get HTTP referer and utmccn and utmctr from _utmz cookie
        $referer = @$_COOKIE["HTTPReferer"];
        $utmz = @$_COOKIE["_utmz"];
        $detail = "";

        if ($referer) {

            $utmz = $utmz ? explode("|", $utmz) : "";

            if (is_array($utmz)) {
                for($i = 0; $i < sizeof($utmz); $i++) {
                    $temp = explode("=", @$utmz[$i]);
                    if (is_array($temp)) {
                        if (sizeof($temp) == 2) {
                            $detail[$temp[0]] = $temp[1];
                        }
                    }
                }
            }

            $utmccn = @$detail["utmccn"] ? @$detail["utmccn"] : "";
            $utmctr = @$detail["utmctr"] ? @$detail["utmctr"] : "";

            // build http referer
            $referer = $referer . " utmccn:" . $utmccn . " utmctr:" . $utmctr;

        } else {
            $referer = "";
        }
        $_REQUEST["a2"] = (!isset($_REQUEST["a2"]))? "" : $_REQUEST["a2"];





        $params =   array(
            "RegSessionID"          => array(@$_REQUEST["rs"], "str", 36),
            "SkinID"                => array(config("SkinID"), "int", 0),
            "GroupID"               => array(@$_REQUEST["g"], "int", 0),
            "TrackerCode"           => array(@$_COOKIE["AffID"], "str", 500),
            "IPAddress"             => array(getPlayerIP(), "str", 15),
            "RegSeconds"            => array(config("minRegTime"), "int", 0),
            "Username"              => array($_REQUEST["u"], "str", 15),
            "Password"              => array($_REQUEST["p"], "str", 10),
            "Email"                 => array($_REQUEST["e"], "str", 100),
            "CurrencyCode"          => array($_REQUEST["c"], "str", 3),
            "PromoCode"             => array($_REQUEST["pc"], "str", 20),
            "Title"                 => array("", "str", 50), // title is not part of the form
            "FirstName"             => array($_REQUEST["fn"], "str", 50),
            "LastName"              => array($_REQUEST["ln"], "str", 50),
            "DOB"                   => array($_REQUEST["dob"], "str", 10),
            "CountryCode"           => array($_REQUEST["cc"], "str", 3),
            "Address1"              => array($_REQUEST["a1"], "str", 50),
            "Address2"              => array($_REQUEST["a2"], "str", 50),
            "City"                  => array($_REQUEST["cty"], "str", 50),
            "State"                 => array($_REQUEST["st"], "str", 50),
            "Postcode"              => array($_REQUEST["pst"], "str", 15),
            "LandPhone"             => array(@$_REQUEST["lp"], "str", 50),
            "MobilePhone"           => array($_REQUEST["mp"], "str", 50),
            "WantsEmail"            => array(@$_REQUEST["we"], "int", 0),
            "SendSMS"               => array(@$_REQUEST["ws"], "int", 0),
            "Referer"               => array($referer, "str", 1000),
            "AffiliateTrail"        => array(@$_COOKIE["AffTrail"], "str", 1000),
            "Referrer"              => array($_REQUEST["ref"], "str", 50),
            "Source"                => array($_REQUEST["src"], "str", 100),
            "Landing"               => array($_REQUEST["l"],"str", 100),
            "Lang"                  => array(@$_COOKIE["LanguageCode"], "str", 2),
            "AspersCustomerNumber"  => array($_REQUEST["ac"], "int", 0),
            "CasinoLocation"        => array($_REQUEST["oc"], "str", 50),
            "GamstopSelfExcluded"   => array($gamStopStatus, "str", 1),
            "WantsPhone"            =>array(@$_REQUEST["wp"], "int", 0),
            "WantsPostMail"         => array((@$_REQUEST["wpm"] == "1" ? 1 : 0), "int", 0),
            "Gender"                => array($_REQUEST["gender"], "int", 0),
        );



        $row = queryDB($ajax->db,"DataRegister",$params);

        if(isset($row['Code']) && ($row["Code"] === 0))
        {
            $response["Code"] = 0;
            $response["PlayerID"] = $row["PlayerID"];
            $response["RegEmailValidation"] = $row["RegEmailValidation"];
            $response["PromoEmailValidation"] = $row["PromoEmailValidation"];
            $response["RegistrationDate"] = $row["RegistrationDate"];
            if (isset($_COOKIE["AffTrail"]) && $_COOKIE["AffTrail"] != ''){
                $affiliates = explode("~", $_COOKIE["AffTrail"]);
                setacookie("AffTrail",$affiliates[0]."~",time()+(60*60*24*3),'/');
            }
            if($row["RegStatus"] == 1 && @$_REQUEST["we"] == 1) {
                $response["Silverpop"] = sendRegistrationMail($row);
            }

            // we check whether the number is valid
            if(!empty($row["PlayerID"]) && Env::isLive()) {
                require_once("libs/MobivateManager.php");

                $mobivate = new MobivateManager($ajax->db);
                $mobivate->checkHLR($_REQUEST["mp"],$row["PlayerID"],getPlayerIP()," at registration.");
            }
            if (@$row != null && (array_key_exists("MatchedPlayerIds", $row) == true && !empty($row["MatchedPlayerIds"]))) {
                require_once("autoload.php");
                $client = new GuzzleHttp\Client(['verify' => false]);
                $url = config("PlayerManagementAPI");
                $res = $client->request('POST', "{$url}/api/v0/register/optimize", [
                    'query' => ['playerId' => $row["PlayerID"], 'relatedPlayerIds' => $row["MatchedPlayerIds"]]
                ]);

                if ($res->getStatusCode() == 200) {
                    $playerBody = $res->getBody()->getContents();
                    $playerStatus = json_decode($playerBody, true);
                    if ($playerStatus["status"] == "ACTIVE" && @$_REQUEST["we"] == 1) {
                        $response["Silverpop"] = sendRegistrationMail($row);
                    }
                }
            }

            // third party comms

            $commsParams = [
                'wantsThirdPartyCalls' => @$_REQUEST["tpc"],
                'wantsThirdPartySMS' => @$_REQUEST["tpsms"],
                'wantsThirdPartyEmails' => @$_REQUEST["tpemails"]
            ];


            $ajax->callMicroservice(config("PlayerManagementAPI"), "players/".$row["PlayerID"]."/communications", $commsParams, 1, 'PATCH', []);

            // gamstop  P previously registered, N no registered, Y registering

            if ($gamStopStatus == 'P') {

                $params = [
                    'eventLogTypeId' => $gamstopResp['body']['playerNote']['eventLogTypeId'],
                    'entityId' => $response["PlayerID"],
                    'details' => $gamstopResp['body']['playerNote']['description'],
                    'referenceId' => 0
                ];

                $ajax->callMicroservice(config("EventLogAPI"), 'event_log/main', $params, 0, 'POST', []);

                $body = [
                    'playerId' => $response["PlayerID"],
                    'note' => $gamstopResp['body']['playerNote']['description']
                ];

                $ajax->callMicroservice(config("PlayerManagementAPI"), 'notes', [], 0, 'POST', $body);
            } elseif ($gamStopStatus == 'Y') {

                $response["Msg"] = str_replace("%%supportUrl%%", config("supportPage"), $gamstopResp['body']['messageToDisplay']);
            }

            //setup request to send json via POST
            $eventLog = array(
                'X-Exclusion' => $gamstopResp['body']['gamStopStatus'],
                'X-Match-Type' => $gamstopResp['body']['matchType'],
                'X-Unique-Id' => $gamstopResp['body']['referenceId']
            );

        }
        else
        {
            $response = addRowToResponse($row);
            if ($gamStopStatus == 'Y') {

                $response["Msg"] = str_replace("%%supportUrl%%", config("supportPage"), $gamstopResp['body']['messageToDisplay']);
            }
        }

        //We'll send the event log to CS with the response from

        $body = [
            'thirdPartyRequestTypeId' => 1 // this is hardcoded to 1, it means gamstop
        ];

        // if the registration didn't go well we won't have playerId
        if (isset($response["PlayerID"])) {
            $body["response"]["playerId"] = $response["PlayerID"];
            $body["playerId"] = $response["PlayerID"];

        }

        // if for some reason we may not have regSession in the request, we add it otherwise
        if (isset($_REQUEST["rs"])) {
            $body["response"]["regSession"] = $_REQUEST["rs"];
            $body["regSession"] = $_REQUEST["rs"];
        }

        $body["response"]["response"] = $eventLog;
        $body["response"]["request"] = $gamstopBody;

        $res = $ajax->callMicroservice(config("EventLogAPI"), 'third-party-response-logs', [], 1, 'POST', $body);

        break;


    /*--------------------------------------------------------------------------------------------
    SET / UNSET PLAYER FAVOURITE GAMES
    --------------------------------------------------------------------------------------------*/
    case "setplayerfavouritegame":


        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "URLKey"             => array(@$_REQUEST["g"], "str", 100),
            "DeviceCategoryID" => array(config("Device"), "string", 1),
            "Add"                => array(@$_REQUEST["a"], "int", 0)
        );


        $row = queryDB($ajax->db,"DataPlayerFavouriteGame",$params,false,false,$rs);
        $favourites = queryDBNextRS($rs);

        $response["Code"] = $row["Code"];
        if($row["Code"] == 0)
        {
            $response["Game"] = $row["Game"];
            $response["Favourites"] = $favourites;
        }
        else
        {
            $response["Msg"] = $row["Msg"];
        }
        break;


    /*--------------------------------------------------------------------------------------------
    SET / UNSET PLAYER FAVOURITE GAMES
    --------------------------------------------------------------------------------------------*/
    case "getplayerfavouritegames":

        $params = array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"        => array($sessionID, "UI", 0),
            "IP"               => array(getPlayerIP(), "str", 50),
            "Lang"             => array(@$_COOKIE["LanguageCode"], "str", 2),
            "DeviceCategoryID" => array(config("Device"), "string", 1)
        );

        $row = queryDB($ajax->db,"DataPlayerListFavouriteGames",$params,true,false,$rs);
        if(isset($row["Code"])) {
            $response["Data"] = array();
        } else {
            $response["Data"] = $row;
        }
        break;


    /*--------------------------------------------------------------------------------------------
    GET CURRENT WINNERS
    --------------------------------------------------------------------------------------------*/
    case "getwinners":


        if($ajax->useMemcache && $ajax->cached('DATA_'.config("SkinID").'_WINNERS'))
        {
            // it's in the cache and valid so use it!
            $response = $ajax->memcache->get('DATA_'.config("SkinID").'_WINNERS');
        }
        else
        {
            // not in the cache or cache is out of date - get from the DB
            $params =   array("SkinID"               => array(config("SkinID"), "int", 0));
            $row = queryDB($ajax->db,"DataGetWinners",$params,false,false,$rs);
            $response["Code"] = $row["Code"];

            // good response
            if($row["Code"] == 0)
            {
                $response["Winners"] = queryDBNextRS($rs);
                if($ajax->useMemcache)
                {
                    // save the winners in the cache
                    $ajax->memcache->set('DATA_'.config("SkinID").'_WINNERS',$response,false,config("Memcache_expires_seconds_data"));
                }
            }
            else
            {
                // bad response
                $response["Msg"] = $row["Msg"];
            }
        }


        break;


    /*--------------------------------------------------------------------------------------------
    GET LAST TOP WINNERS
    --------------------------------------------------------------------------------------------*/
    case "getlasttopwinners":


        if($ajax->useMemcache && $ajax->cached('DATA_'.config("SkinID").'_WINNERS'))
        {
            // it's in the cache and valid so use it!
            $response = $ajax->memcache->get('DATA_'.config("SkinID").'_WINNERS');
        }
        else
        {
            // not in the cache or cache is out of date - get from the DB
            $params =   array("SkinID"               => array(config("SkinID"), "int", 0));
            $response = queryDB($ajax->db,"DataGetLastTopWinners",$params,false,false,$rs);

            // good response
            if($response["Code"] == 0)
            {
                if($ajax->useMemcache)
                {
                    // save the winners in the cache
                    $ajax->memcache->set('DATA_'.config("SkinID").'_WINNERS',$response,false, config("Memcache_expires_seconds_data"));
                }
            }
            else
            {
                // bad response
                $response["Msg"] = $response["Msg"];
            }
        }


        break;

    /*--------------------------------------------------------------------------------------------
    GET CURRENT PROGRESSIVES
    --------------------------------------------------------------------------------------------*/
    case "getprogressives":


        if($ajax->useMemcache && $ajax->cached('DATA_'.config("SkinID").'_PROGRESSIVES'))
        {
            // it's in the cache and valid so use it!
            $response = $ajax->memcache->get('DATA_'.config("SkinID").'_PROGRESSIVES');
        }
        else
        {
            // not in the cache or cache is out of date - get from the DB
            $params =   array("SkinID"  => array(config("SkinID"), "int", 0));
            $row = queryDB($ajax->db,"DataGetProgressives",$params,false,false,$rs);
            $response["Code"] = $row["Code"];

            // good response
            if($row["Code"] == 0)
            {
                $response["Progressives"] = queryDBNextRS($rs);
                $response["Total"] = queryDBNextRS($rs);
                if (!empty($response["Total"]) && !empty($response["Total"][0])){
                    $response["Total"] = $response["Total"][0];
                }
                if($ajax->useMemcache)
                {
                    // save the winners in the cache
                    $ajax->memcache->set('DATA_'.config("SkinID").'_PROGRESSIVES',$response,false, config("Memcache_expires_seconds_data"));
                }
            }
            else
            {
                // bad response
                $response["Msg"] = $row["Msg"];
            }
        }


        break;



    /*--------------------------------------------------------------------------------------------
    GET PLAYER ACCOUNT INFO
    --------------------------------------------------------------------------------------------*/
    case "getplayeraccount":


        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
        );

        $response = queryDB($ajax->db,"DataGetPlayerAccount",$params);
        $response["Msg"] = "Username contains invalid characters";
        break;



    /*--------------------------------------------------------------------------------------------
    GET PLAYERS TRANSACTIONS
    --------------------------------------------------------------------------------------------*/
    case "getplayertx":

        if ($response = $ajax->checkIfSessionIdNull($sessionID)){
            break;
        }

        if (strlen(@$_REQUEST["date"]) !== 8) {
            $response["Code"] = -20;
            $response["Msg"] = "Error sending the date";
            break;
        }

        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "Date"           => array(@$_REQUEST["date"], "str", 50),
            "DisplayStart"     => array(@$_REQUEST["start"], "int", 0),
            "DisplayLength"    => array(@$_REQUEST["length"], "int", 0),
            "SortCol"            => array(@$_REQUEST["sort"], "int", 0),
            "SortDir"            => array(@$_REQUEST["dir"], "str", 4),
            "Search"             => array(@$_REQUEST["search"], "str", 50),
            "TxTypeIDs"      => array(@$_REQUEST["types"], "str", 50),
            "endDate"       => (!empty($_REQUEST["endDate"])) ? array(@$_REQUEST["endDate"], "str", 50) : array(@$_REQUEST["date"], "str", 50)
//all, games, promo, cashier, deposits, withdrawals, wagers, wins

        );


        $row = queryDB($ajax->db,"DataGetPlayerTx",$params,false,false,$rs);
        if($row["Code"] <> 0)
        {
            $response["Code"] = $row["Code"];
            $response["Msg"] = $row["Msg"];
        }
        else
        {
            $response["Code"] = 0;
            $response["Total"] = $row["Total"];
            $response["Start"] = $row["Start"];
            $response["Returned"] = $row["Returned"];
            $response["Data"] = queryDBNextRS($rs);
        }

        break;


    /*--------------------------------------------------------------------------------------------
    GET PLAYERS TRANSACTIONS
    --------------------------------------------------------------------------------------------*/
    case "getplayerbonustx":


        $params =   array(
            "PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2)

        );

        $row = queryDB($ajax->db,"DataGetPlayerBonusTx",$params,false,false,$rs);


        if($row["Code"] <> 0)
        {
            $response["Code"] = $row["Code"];
            $response["Msg"] = $row["Msg"];
        }
        else
        {
            $response["Code"] = $row["Code"];
            // R10 - 1042 remove after fix.
            // $response["Data"] = queryDBNextRS($rs);
            /* Hotfix */
            $Data = queryDBNextRS($rs);
            foreach ($Data as $result){
                if($result["PromoDescription"] != "Play £10 GET 1 FREE WHEEL" && $result["Games"] != "Phoenix Reborn - Desktop H5, Phoenix Reborn - Mobile H5, Sparkling Fruit Match 3 - Desktop H5, Sparkling Fruit Match 3 - Mobile H5, Aztec Rising, Beez Knees, "){
                    $newData[] = $result;
                }
            }
            $response["Data"] = $newData;
            /* End Hotfix */
        }

        break;



    /*--------------------------------------------------------------------------------------------
    SEARCH GAMES
    Game Types on database
    1   Flash
    2   HTML5
    3   Minigame
    --------------------------------------------------------------------------------------------*/
    case "searchgames":

        $params =   array(
            "SkinID"     => array(config("SkinID"), "int", 0),
            "SearchTerm" => array(@$_REQUEST["t"], "string", 100),
            "Device"     => array(config("Device"), "string", 1),
            "AccessLevel" => array( config( 'AccessLevel'), "int", 0)
        );

        $response = queryDB($ajax->db,"DataSearchGames",$params,false,false,$rs);
        if($response["Code"] == 0)
        {
            $response['Games']  = queryDBNextRS($rs);
        }


        break;

    /*--------------------------------------------------------------------------------------------
    GET THE PLAYERS DETAILS
    --------------------------------------------------------------------------------------------*/
    case "getplayerdetails":

// get old player details

        $params =   array(
            "PlayerID"              => array($playerID, "int", 0),
            "SessionID"             => array($sessionID, "UI", 0),
            "SkinID"                => array(config("SkinID"), "int", 0),
            "IP"                    => array(getPlayerIP(), "str", 50),
            "Lang"                  => array(@$_COOKIE["LanguageCode"], "str", 2)
        );


        $response = queryDB($ajax->db,"DataGetPlayerDetails",$params,false,false,$rs);
        $response["LimitConfirmation"] = 1;
        $response["Security"] = queryDBNextRS($rs);


        break;


    /*--------------------------------------------------------------------------------------------
   UPDATE THE PLAYERS DETAILS
   --------------------------------------------------------------------------------------------*/
    case "setplayerdetails":

        $params =   array(
            "PlayerID"              => array($playerID, "int", 0),
            "SessionID"             => array($sessionID, "UI", 0),
            "SkinID"                => array(config("SkinID"), "int", 0),
            "IP"                    => array(getPlayerIP(), "str", 50),
            "OldPassword"           => array(@$_REQUEST["opwd"], "str", 15),
            "Password"              => array(@$_REQUEST["pwd"], "str", 15),
            "LandPhone"             => array(@$_REQUEST["lp"], "str", 50),
            "LandPhoneExt"          => array(@$_REQUEST["lpe"], "str", 10),
            "MobilePhone"           => array(@$_REQUEST["mp"], "str", 50),
            "SecurityQuestionID"    => array(@$_REQUEST["sqid"], "int", 0),
            "SecurityAnswer"        => array(@$_REQUEST["sa"], "str", 50),

            "WantsEmail"            => array(@$_REQUEST["email"], "int", 50),
            "SendSMS"               => array(@$_REQUEST["sms"], "int", 50),
            "WantsPostMail"        => array(@$_REQUEST["mailpost"], "int", 50),
            "Lang"                  => array(@$_COOKIE["LanguageCode"], "str", 2),
            "WantsPhoneCalls"       => array(@$_REQUEST["phone"], "int", 50)


        );


// request to database to update old details
        $response = queryDB($ajax->db,"DataSetPlayerDetails",$params);

        if ($response["Code"] === 4) {
            $response["Code"] = 0;
            $response["Msg"] = "";
        }

        $commsParams = [
            'wantsThirdPartyCalls' => @$_REQUEST["tpc"],
            'wantsThirdPartySMS' => @$_REQUEST["tpsms"],
            'wantsThirdPartyEmails' => @$_REQUEST["tpemails"]
        ];

        $ajax->callMicroservice(config("PlayerManagementAPI"), "players/".$playerID."/communications", $commsParams, 1, 'PATCH', []);

// mobivate
        if (isset($response["ModifiedPhone"]) && $response["ModifiedPhone"] == 1) {
            // we check whether the number is valid
            require_once("libs/MobivateManager.php");

            $mobivate = new MobivateManager($ajax->db);
            $mobivate->checkHLR($_REQUEST["mp"],$playerID,getPlayerIP()," at player details update.");

            unset($response["ModifiedPhone"]);
        }

// if player is registered on ASPERS - Should update the details on IG  
        if (config("SkinID") === 12 && ($_REQUEST["acn"] != null) ) {

            $url = config("ig-update-customer");

            if ($url) {

                // build url passing the player details

                $url = $url . "?email=" . $_REQUEST["email"] . "&sms=" . $_REQUEST["sms"]  . "&acn=" . $_REQUEST["acn"];

                try {

                    // just do the request any error will be logged on the endpoint

                    $curl = curl_init($url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
                    curl_setopt($curl, CURLOPT_CAINFO, __FILE__ . "\\..\\cacert.pem");
                    curl_setopt($curl, CURLOPT_CAPATH, __FILE__ . "\\..\\");
                    $curl_response = curl_exec($curl);

                } catch (Exception $e) {
                    // do nothing - just ignore
                }

            }

        }

        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER REDEEM POINTS
    --------------------------------------------------------------------------------------------*/
    case "redeempoints":


        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "Points"         => array(@$_REQUEST["p"], "int", 0),
            "Type"               => array(@$_REQUEST["t"], "int", 0),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2)
        );

        $response = queryDB($ajax->db,"DataRedeemPoints",$params);
        break;


    /*--------------------------------------------------------------------------------------------
    GET THE PLAYERS RAF INFO
    --------------------------------------------------------------------------------------------*/
    case "getplayerrafinfo":


        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2)
        );

        $response = queryDB($ajax->db,"DataPlayerGetRAFInfo",$params,false,false,$rs);

        $response["Friends"] = queryDBNextRS($rs);
        $response["Referrals"] = queryDBNextRS($rs);

        break;


//------------------------------------------------------------------------------------------------------------------------------------------
//    REFER A LIST OF FRIENDS sending them an email
//------------------------------------------------------------------------------------------------------------------------------------------
    case "rafrefer":

        $validEmails = '';
        $invalidEmails = '';
        $emails = explode(';',@$_REQUEST["el"]);
        foreach($emails as $key => $val) {
            if (filter_var( $val, FILTER_VALIDATE_EMAIL )){
                // If the email is validated we add it to the list of valid Emails
                $validEmails .= $val.';';
            } else {
                $invalidEmails .= $val.';';
            }
        }

        $params = array("PlayerID"  => array($playerID, "str", 250),
            "SessionID"   => array($sessionID, "str", 36),
            "SkinID" => array(config("SkinID"), "int", 0),
            "EmailList" => array($validEmails, "str", 8000),
            "IP"        => array(getPlayerIP(), "str", 50)
        );

        $response = queryDB($ajax->db,"DataRafRefer",$params);

        if(@$response["Code"] == 0)
        {

            $response["InvalidEmails"] .= $invalidEmails;
            $response["Message"] = htmlentities('', ENT_QUOTES);//@$_REQUEST["Message"], ENT_QUOTES);
            sendReferAFriendMail($response);
            unset($response["Message"]);
        }

        break;


    /*--------------------------------------------------------------------------------------------
    GET THE WINNERS
    --------------------------------------------------------------------------------------------*/
    case "winners":


        $params =   array("SkinID"           => array(config("SkinID"), "int", 0)
        );

        $response = queryDB($ajax->db,"DataGetWinners",$params,true,false);
        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER FORGOT PASSWORD
    --------------------------------------------------------------------------------------------*/
    case "playerforgotpassword":


        $params =   array("Email"           => array(@$_REQUEST["e"], "str", 50),
            "SkinID"            => array(config("SkinID"), "int", 0),
            "IP"                => array(getPlayerIP(), "str", 50),
            "Lang"          => array(@$_COOKIE["LanguageCode"], "str", 2)
        );

        $response = queryDB($ajax->db,"DataPlayerForgotPassword",$params);

// send the mail
        if(@$response["Code"] == 0)
        {
            sendForgotPasswordMail($response);
        }


        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER FORGOT PASSWORD
    --------------------------------------------------------------------------------------------*/
    case "getbingorooms":


        $params =   array("SecondsToGo"     => array(1, "int", 0),
            "SkinID"            => array(config("SkinID"), "int", 0)
        );

        $response = queryDB($ajax->db,"DataGetBingoRooms",$params,false,false,$rs);
        $response['Rooms'] = queryDBNextRS($rs);
        break;


    /*--------------------------------------------------------------------------------------------
    PLAYER FORGOT UPDATE
    --------------------------------------------------------------------------------------------*/
    case "playerforgotpasswordupdate":


        $params =   array("SkinID"          => array(config("SkinID"), "int", 0),
            "PlayerID"      => array($_REQUEST['id'], "int", 0),
            "GUID"          => array($_REQUEST['guid'], "UI", 0),
            "Password"      => array($_REQUEST['p'], "str", 15),
            "IP"                => array(getPlayerIP(), "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2)
        );
        $response = queryDB($ajax->db,"DataPlayerForgotPasswordUpdate",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    PLAYER ACCEPT TERMS AND CONDITIONS
    --------------------------------------------------------------------------------------------*/
    case "accepttandc":


        $params =   array("PlayerID"        => array($playerID, "int", 0),
            "IP"                => array(getPlayerIP(), "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2)
        );

        $response = queryDB($ajax->db,"DataAcceptTandC",$params);

        if($response["Code"] == 0)
        {
            setacookie("SessionID",$response["SessionID"],0,'/');
        }

        break;


    /*--------------------------------------------------------------------------------------------
    BINGO MINIGAME GAMES FEED
    --------------------------------------------------------------------------------------------*/
    case "minigamefeed":

        $live = (Env::inState([Env::live, Env::dev])) ? 1 : 0;

        if($ajax->useMemcache && $ajax->cached('BINGO_MINIGAME_FEED_'.config("SkinID").'_'.$live.'_'.config("Device")))
        {
            $response = $ajax->memcache->get('BINGO_MINIGAME_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"));
        }
        else
        {


            $params =   array("SkinID"      => array(config("SkinID"), "int", 0),
                "Device"        => array(config("Device"), "int", 0),
                "Live"      => array($live, "int", 0)
            );

            $response = queryDB($ajax->db,"DataMinigameFeed",$params,false,false,$rs);

            if($response["Code"] == 0)
            {
                $response["Data"] = queryDBNextRS($rs);

                if($ajax->useMemcache)
                {
                    $ajax->memcache->set('BINGO_MINIGAME_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"),$response,false,20000);
                }
            }
        }

        break;

    /*--------------------------------------------------------------------------------------------
    BINGO PROMOTIONS FEED
    --------------------------------------------------------------------------------------------*/
    case "promotionsfeed":

        $live = (Env::inState([Env::live, Env::dev])) ? 1 : 2;

        if($ajax->useMemcache && $ajax->cached('BINGO_PROMOTIONS_FEED_'.config("SkinID").'_'.$live.'_'.config("Device")))
        {
            $response = $ajax->memcache->get('BINGO_PROMOTIONS_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"));
        }
        else
        {


            $params =   array("SkinID"      => array(config("SkinID"), "int", 0),
                "Device"        => array(config("Device"), "int", 0),
                "Live"      => array($live, "int", 0)
            );

            $response = queryDB($ajax->db,"DataPromotionsFeed",$params,false,false,$rs);

            if($response["Code"] == 0)
            {
                $response["Data"] = queryDBNextRS($rs);

                if($ajax->useMemcache)
                {
                    $ajax->memcache->set('BINGO_PROMOTIONS_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"),$response,false,20000);
                }
            }
        }
        break;

    /*--------------------------------------------------------------------------------------------
    BINGO MAIN GAMES FEED
    --------------------------------------------------------------------------------------------*/
    case "gamesfeed":

        $live = (Env::inState([Env::live, Env::dev])) ? 1 : 0;

        if($ajax->useMemcache && $ajax->cached('BINGO_GAME_FEED_'.config("SkinID").'_'.$live.'_'.config("Device")))
        {
            $response = $ajax->memcache->get('BINGO_GAME_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"));
        }
        else
        {


            $params =   array("SkinID"      => array(config("SkinID"), "int", 0),
                "Device"        => array(config("Device"), "int", 0),
                "Live"      => array($live, "int", 0)
            );

            $response = queryDB($ajax->db,"DataGamesFeed",$params,false,false,$rs);

            if($response["Code"] == 0)
            {

                $nextCategoryDescription = queryDBNextRS($rs,false);
                while(is_array($nextCategoryDescription) && count($nextCategoryDescription) > 0)
                {
                    $response["Data"][$nextCategoryDescription["Description"]] = queryDBNextRS($rs);
                    $nextCategoryDescription = queryDBNextRS($rs,false);
                }

                if($ajax->useMemcache)
                {
                    $ajax->memcache->set('BINGO_GAME_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"),$response,false,20000);
                }
            }
        }
        break;

    /*--------------------------------------------------------------------------------------------
    BINGO GAMES FEED FOR EXTRA FLASH PANELS
    --------------------------------------------------------------------------------------------*/
    case "gamesfeedflash":

        $live = (Env::inState([Env::live, Env::dev])) ? 1 : 0;

        if($ajax->useMemcache && $ajax->cached('BINGO_FLASH_FEED_'.config("SkinID").'_'.$live.'_'.config("Device")))
        {
            $response = $ajax->memcache->get('BINGO_FLASH_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"));
        }
        else
        {
            $params =   array("SkinID"      => array(config("SkinID"), "int", 0),
                "Device"        => array(config("Device"), "int", 0),
                "Live"      => array($live, "int", 0)
            );
            $response = queryDB($ajax->db,"DataGamesFeedFlash",$params,false,false,$rs);

            if($response["Code"] == 0)
            {
                $nextCategoryDescription = queryDBNextRS($rs,false);
                while(is_array($nextCategoryDescription) && count($nextCategoryDescription) > 0)
                {
                    $response["Data"][$nextCategoryDescription["Key"]]["Description"] = $nextCategoryDescription["Description"];
                    $response["Data"][$nextCategoryDescription["Key"]]["Games"] = queryDBNextRS($rs);
                    $nextCategoryDescription = queryDBNextRS($rs,false);
                }

                if($ajax->useMemcache)
                {
                    $ajax->memcache->set('BINGO_FLASH_FEED_'.config("SkinID").'_'.$live.'_'.config("Device"),$response,false,20000);
                }
            }
        }
        break;

    /*--------------------------------------------------------------------------------------------
    FOR FLASH MINIGAMES CHECK GAME AVAILABLE
    --------------------------------------------------------------------------------------------*/
    case "gamesfeedflashcheck":


        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "GameID"             => array($_REQUEST['gameid'], "int", 0)
        );

        $response = queryDB($ajax->db,"datagamesfeedflashcheck",$params);
        break;



    /*--------------------------------------------------------------------------------------------
    GET GAMES RECOMMENDATIONS
    --------------------------------------------------------------------------------------------*/
    case "gamerecommendations":

        if($ajax->useMemcache && $ajax->cached('RECOMMENDATION_GAME_FEED_'.$playerID))
        {
            $response = $ajax->memcache->get('RECOMMENDATION_GAME_FEED_'.$playerID);
        }
        else
        {


            $params =   array("PlayerID"    => array($playerID, "int", 0),
                "SkinID"        => array(config("SkinID"), "int", 0),
                "IP"            => array(getPlayerIP(), "str", 50)
            );

            $response = queryDB($ajax->db,"DataGetRecommendedGames",$params,false,false,$rs);

            if($response["Code"] == 0)
            {

                $response["Games"] = queryDBNextRS($rs,true);

                if($ajax->useMemcache)
                {
                    $ajax->memcache->set('RECOMMENDATION_GAME_FEED_'.$playerID,$response,false,20000);
                }
            }
        }
        break;



    /*--------------------------------------------------------------------------------------------
    TRACKNAVIGATION the navigation of a player
    --------------------------------------------------------------------------------------------*/
    case "tracknavigation":

        if (COMPUTER == 'MacDev') {
            $response["Code"] = 0;
        }
        else {
            $user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "Unknown";

            $params =   array("NavigationPage"  => array($_REQUEST['np'], "str", 200),
                "UserAgent"      => array($user_agent, "str", 500),
                "DeviceID"      => array(config("RealDevice"), "int", 0),
                "IP"                => array(getPlayerIP(), "str", 50),
                "Action"            => array($_REQUEST['a'], "str", 50)
            );

            if (isset($_COOKIE["AffTrackID"])) {
                $params["AffiliateID"] = array(@$_COOKIE["AffTrackID"], "str", 500);
                unset($_COOKIE['AffTrackID']);
                setcookie('AffTrackID', null, -1, '/');
            } else {
                $params["AffiliateID"] = array(-1, "int", 0);
            }

            if (!empty($_REQUEST['l'])) {
                $params["Label"] = array($_REQUEST['l'], "str", 200);
            } else {
                $params["Label"] = array('', "int", 0);
            }

            if (!empty($_COOKIE["CookieTrackGUID"])){
                $params["CookieGUID"] = array(@$_COOKIE["CookieTrackGUID"], "str", 50);
            }

            else {
                //we should send an empty guid to the db so it can returns us the right one using playerID and SessionID if they're empty
                $params["CookieGUID"] = array('', "str", 50);
            }

            if (!empty($playerID) && !empty($sessionID)){
                $params["PlayerID"] = array($playerID, "int", 0);
                $params["SessionID"] = array($sessionID, "UI", 0);
            }
            $response = queryDB($ajax->db,"DataTrackNavigation",$params);
        }
        break;


    /*--------------------------------------------------------------------------------------------
    GET PLAYERS LIMITS
    --------------------------------------------------------------------------------------------*/
    case "getlimits":


        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                 => array(getPlayerIP(), "str", 50)
        );

        $response = queryDB($ajax->db,"DataPlayerGetLimits",$params);
        break;


    /*--------------------------------------------------------------------------------------------
    Back up opt in
    --------------------------------------------------------------------------------------------*/
    case "backupOptIn":

        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "TournamentID"   => array($_REQUEST['t'], "int", 0)
        );

        $response = queryDB($ajax->db,"DataBackupOptIn",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    SET PLAYERS LIMITS
    --------------------------------------------------------------------------------------------*/
    case "setlimits":


        $params =   array("PlayerID"          => array($playerID, "int", 0),
            "SessionID"           => array($sessionID, "UI", 0),
            "Lang"            => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                  => array(getPlayerIP(), "str", 50),
            "WagerLimitPeriod" => array($_REQUEST['wlp'], "int", 0),
            "WagerLimitAmount"    => array($_REQUEST['wla'], "int", 0),
            "LossLimitPeriod"  => array($_REQUEST['llp'], "int", 0),
            "LossLimitAmount"     => array($_REQUEST['lla'], "int", 0),
            "DepositLimitPeriod"      => array($_REQUEST['dlp'], "int", 0),
            "DepositLimitAmount"    => array($_REQUEST['dla'], "int", 0)

        );

        $response = queryDB($ajax->db,"DataPlayerSetLimits",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    GET LIMITS VALUES
    --------------------------------------------------------------------------------------------*/
    case "getlimitvalues":


        $params =   array("SkinID"        => array(config("SkinID"), "int", 0)

        );

        $row = queryDB($ajax->db,"DataPlayerGetLimitValues",$params,false,false,$rs);
        $response["Code"] = $row["Code"];
        $response["values"] = queryDBNextRS($rs);
        break;

    /*--------------------------------------------------------------------------------------------
    ACCEPT/CANCEL PLAYERS LIMITS
    --------------------------------------------------------------------------------------------*/
    case "acceptplayerlimits":


        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "SessionID"          => array($sessionID, "UI", 0),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                 => array(getPlayerIP(), "str", 50),
            "Accept"             => array(@$_REQUEST["a"], "int", 0)
        );

        $response = queryDB($ajax->db,"DataPlayerAcceptLimits",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    CHECK PROMO CODE
    --------------------------------------------------------------------------------------------*/
    case "checkpromocode":

        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "Promocode"          => array(@$_REQUEST['p'], "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                 => array(getPlayerIP(), "str", 50)
        );

        $response = queryDB($ajax->db,"DataCheckPromoCode",$params);
        break;




//------------------------------------------------------------------------------------------------------------------------------------------
//  RAF HISTORY
//------------------------------------------------------------------------------------------------------------------------------------------
    case "rafhistory":


        $params = array("PlayerID"  => array($playerID, "str", 250),
            "SessionID"   => array($sessionID, "str", 36),
            "SkinID" => array(config("SkinID"), "int", 0),
            "IP"        => array(getPlayerIP(), "str", 15),
            "Number" => array($_REQUEST["Number"], "int", 0)
        );

        $response = queryDB($ajax->db,"RafGetPlayerReferences",$params);
//process("","Main","RafGetPlayerReferences");
        $rafHistory["References"] = $response["Data"];
        $params = array("PlayerID"  => array($playerID, "str", 250),
            "SessionID"   => array($sessionID, "str", 36),
            "SkinID" => array(config("SkinID"), "int", 0),
            "IP"        => array(getPlayerIP(), "str", 15)
        );

        $response = queryDB($ajax->db,"RafGetPlayerFriends",$params);
//process("","Main","RafGetPlayerFriends");
        $rafHistory["Friends"] = $response["Data"];
        unset($response["Data"]);
        $response["Data"][0]["RafHistory"]=$rafHistory;

        break;

//------------------------------------------------------------------------------------------------------------------------------------------
//  RAF Pre publishing on a social network
//------------------------------------------------------------------------------------------------------------------------------------------
    case "rafprepublish":

        $fieldcheck += validate("ID", @$playerID);
        $fieldcheck += validate("SkinID", @config("SkinID"));
        $fieldcheck += validate("IP", @getPlayerIP());
        $fieldcheck += validate("SessionID", @$sessionID);
        $fieldcheck += validate("Action", @$_REQUEST["Type"]);


        $params = array("PlayerID"  => array($playerID, "str", 250),
            "SessionID"   => array($sessionID, "str", 36),
            "SkinID" => array(config("SkinID"), "int", 0),
            "IP"        => array(getPlayerIP(), "str", 15),
            "Type"  => array($_REQUEST["Type"], "str", 25)
        );

        $response = queryDB($ajax->db,"RafPrePublish",$params);
        break;
//------------------------------------------------------------------------------------------------------------------------------------------
//  RAF Success publishing on a social network
//------------------------------------------------------------------------------------------------------------------------------------------
    case "rafsuccesspublish":


        $params = array("PlayerID"  => array($playerID, "str", 250),
            "SessionID"   => array($sessionID, "str", 36),
            "SkinID" => array(config("SkinID"), "int", 0),
            "IP"        => array(getPlayerIP(), "str", 15),
            "Type"  => array($_REQUEST["Type"], "str", 25)
        );

        $response = queryDB($ajax->db,"RafSuccessPublish",$params);
        break;
//------------------------------------------------------------------------------------------------------------------------------------------
//  RAF REPLY
//------------------------------------------------------------------------------------------------------------------------------------------
    case "rafreply":


        if (!validate("DomainName", @$_REQUEST["Source"])){
            $sourceReply = 'unknown';
        }else {
            $sourceReply =  @$_REQUEST["Source"];
        }


        $params = array("SkinID" => array(config("SkinID"), "int", 0),
            "Alias"   => array($_REQUEST["Alias"], "str", 36),
            "Source"   => array($sourceReply, "str", 20),
            "IP"        => array(getPlayerIP(), "str", 15)
        );

        $response = queryDB($ajax->db,"RafReply",$params);
        break;

//------------------------------------------------------------------------------------------------------------------------------------------
//  RedeemPlayerPromoCode - Redeem a promo code
//------------------------------------------------------------------------------------------------------------------------------------------
    case "playerverifymobile":

        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "Promocode"          => array(@$_REQUEST['p'], "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                 => array(getPlayerIP(), "str", 50)
        );

        $response = queryDB($ajax->db,"DataPlayerVerifyMobile",$params);
        break;

//------------------------------------------------------------------------------------------------------------------------------------------
//  RedeemPlayerPromoCode - Redeem a promo code
//------------------------------------------------------------------------------------------------------------------------------------------
    case "playerredeempromocode":

        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "Promocode"          => array(@$_REQUEST['p'], "str", 50),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                 => array(getPlayerIP(), "str", 50)
        );

        $response = queryDB($ajax->db,"DataPlayerRedeemPromoCode",$params);

        break;
//------------------------------------------------------------------------------------------------------------------------------------------
//  Resend the phone verification code by sms
//------------------------------------------------------------------------------------------------------------------------------------------
    case "resendphoneverificationcode":

        $params =   array("PlayerID"         => array($playerID, "int", 0),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                 => array(getPlayerIP(), "str", 50)
        );

        $response = queryDB($ajax->db,"DataResendPhoneVerificationCode",$params);

        break;
//------------------------------------------------------------------------------------------------------------------------------------------
//  Returns a list with all the active competitions
//------------------------------------------------------------------------------------------------------------------------------------------
    case "competitionpromolist":

        if(!isset($sessionID) || $sessionID == '' || $sessionID == 'NULL')
        {
            $sessionID = '2d3bf6a6-ed62-4bed-82c3-04a48a2a276b';
            $playerID = 0;
        }

        $params = array("PlayerID"    => array($playerID, "int", 0),
            "SessionID"   => array($sessionID, "str", 36),
            "IP"          => array(getPlayerIP(), "str", 15),
            "Lang"           => array(@$_COOKIE["LanguageCode"], "str", 2),
            "SkinID"    => array(config("SkinID"), "int", 0)
        );
        $row = queryDB($ajax->db,"DataCompetitionPromoList",$params,false,false,$rs);
        $response = addRowToResponse($row);

        if($row["Code"] != 0)
        {
            $response["Msg"] = $row["Msg"];
        }
        else
        {
            $response["Competitions"] = queryDBNextRS($rs);
        }

        break;


//------------------------------------------------------------------------------------------------------------------------------------------
//  RAF REPLY
//------------------------------------------------------------------------------------------------------------------------------------------
    case "rafgetplayerexpectedprize":

        $params = array("PlayerID"  => array($playerID, "str", 250),
            "SessionID"   => array($sessionID, "str", 36),
            "SkinID" => array(config("SkinID"), "int", 0),
            "IP"        => array(getPlayerIP(), "str", 15)
        );


        $response = queryDB($ajax->db,"DataRafGetPlayerExpectedPrize",$params);
        break;

//------------------------------------------------------------------------------------------------------------------------------------------
//  COMPETITION OPT IN
//------------------------------------------------------------------------------------------------------------------------------------------
    case "optin":
        if(!isset($sessionID) || $sessionID == '' || $sessionID == 'NULL')
        {
            $sessionID = '2d3bf6a6-ed62-4bed-82c3-04a48a2a276b';
            $justFree = 1;
        } else {
            $justFree = 0;
        }
// two kinds of competitions, the ones before 675 work differently

        if (@$_REQUEST["c"] < 675) {
            $params = array("PlayerID"    => array(@$_REQUEST["p"], "int", 0),
                "SessionID"   => array($sessionID, "str", 36),
                "IP"          => array(getPlayerIP(), "str", 15),
                "lang"        => array(@$_COOKIE["LanguageCode"], "str", 2),
                "CompetitionID"   => array(@$_REQUEST["c"], "int", 0),
                "BuyInType"   => array(@$_REQUEST["t"], "int", 0),
                "JustFree"   => array($justFree, "int", 0)

            );

            $response = queryDB($ajax->db,"DataCompetitionOptIn",$params);

        } else {
            $numTickets = 0; // At the moment num Tickets is the default value = 0
            $languageCode =  (isset($_COOKIE["LanguageCode"])) ? $_COOKIE["LanguageCode"] : 'en';
            $service_url = config("competitionEngineURL").'/optin/'.@$_REQUEST["p"]."/".$sessionID;
            $service_url .= "/".getPlayerIP()."/".$languageCode."/".@$_REQUEST["c"]."/0";
            $service_url .= "/".$numTickets."/".$justFree;

            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_CAINFO, __FILE__ . "\\..\\cacert.pem");
            curl_setopt($curl, CURLOPT_CAPATH, __FILE__ . "\\..\\");
            $curl_response = curl_exec($curl);
            if(curl_error($curl) || json_decode($curl_response) === null)
            {
                //echo 'error:' . curl_error($c);
                $from = 'RAD_OptIn_Error';
                $subject = 'RAD OptIn Player Error';
                $message = "The playerID: ".$_REQUEST["p"]." could not opt in in the competition: ".$_REQUEST["c"];
                $message .= '<br>';
                $message .= 'Technical info';
                $message .= '<br>';
                $message .= $service_url;
                $message .= '<br>';
                $message .= 'Curl error: ' .curl_error($curl);
                $message .= '<br>';
                $message .= 'Curl Response: ' . $curl_response;
                $message .= '<br>';
                $to = 'sophiep@spacebarmedia.com,crmmru@srgservicesltd.com,bobk@srgservicesltd.com,CasinoBM@srgservicesltd.com,GamesBE@daub365.com,qa@spacebarmedia.com,Mohtadib@spacebarmedia.com,jennyi@spacebarmedia,laurend@spacebarmedia';
                $headers = 'From: webmaster@spacebarmedia.com' . "\r\n" .
                    'Reply-To: webmaster@spacebarmedia.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                mail($to,$subject,$message,$headers);
                $response = array();
                $response["Code"] = -12;
                $response["Msg"] = "Sorry we have been unable to opt you in at this time. Please try again later.";
            } else {
                $response = json_decode($curl_response);
            }
            curl_close($curl);
        }
        break;

//------------------------------------------------------------------------------------------------------------------------------------------
//  VOIDING BONUS
//------------------------------------------------------------------------------------------------------------------------------------------
    case "playertxpromocancel":

        $params = array("PlayerID"    => array($playerID, "int", 0),
            "SessionID"   => array($sessionID, "str", 36),
            "IP"          => array(getPlayerIP(), "str", 15),
            "TxPromoID"   => array(@$_REQUEST["p"], "int", 0),
            "lang"        => array(@$_COOKIE["LanguageCode"], "str", 2)

        );

        $response = queryDB($ajax->db,"DataPlayerTxPromoCancel",$params);
        break;

//------------------------------------------------------------------------------------------------------------------------------------------
//  UPDATE MESSAGE
//------------------------------------------------------------------------------------------------------------------------------------------
    case "messageUpdateStatus":
        if ($response = $ajax->checkIfSessionIdNull($sessionID)){
            break;
        }
        $params = array("PlayerID"    => array($playerID, "int", 0),
            "SessionID"         => array($sessionID, "str", 36),
            "action"            => array(@$_REQUEST["a"], "str", 10),
            "messageID"         => array(@$_REQUEST["m"], "int", 0),
            "lang"              => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"                => array(getPlayerIP(), "str", 15),
            "DeviceID"          => array(config("RealDevice"), "int", 0)

        );

        $response = queryDB($ajax->db,"DataMessageUpdateStatus",$params);
        break;


//------------------------------------------------------------------------------------------------------------------------------------------
//  GET MESSAGE PREVIEW
//------------------------------------------------------------------------------------------------------------------------------------------
    case "messageGetPreview":
        if ($response = $ajax->checkIfSessionIdNull($sessionID)){
            break;
        }
        $params = array("PlayerID"    => array($playerID, "int", 0),
            "SessionID"   => array($sessionID, "str", 36),
            "messageID"    => array(@$_REQUEST["m"], "int", 0),
            "lang"        => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"          => array(getPlayerIP(), "str", 15)

        );

        $response = queryDB($ajax->db,"DataMessageGetPreview",$params,false,false,$rs);
        if($response["Code"] != 0)
        {
            $response["Msg"] = $row["Msg"];
        }
        else
        {
            $message["Code"] = 0;
            $message["Html"] = getPreviewMessage($response);
            unset($response);
            $response = $message;
        }
        break;

    /*--------------------------------------------------------------------------------------------
    Gets the player messages
    --------------------------------------------------------------------------------------------*/
    case "getPlayerMessages":
        if ($response = $ajax->checkIfSessionIdNull($sessionID)){
            break;
        }
        if (strlen($sessionID) != 36)
            break;
        $params = array("PlayerID"    => array($playerID, "int", 0),
            "SessionID"   => array($sessionID, "str", 36),
            "IP"          => array(getPlayerIP(), "str", 15),
            "lang"        => array(@$_COOKIE["LanguageCode"], "str", 2)
        );

        if (!empty($_REQUEST["t"])){
            $params["TemplateID"] = array($_REQUEST["t"], "int",0 );
        }

        $response = queryDB($ajax->db,"DataPlayerGetMessagesInbox",$params,false,false,$rs);
        if ($response["Code"] == 0) {
            $response["Messages"] = queryDBNextRS($rs);
        }
        if (isset($response["Messages"])) {
            foreach ($response["Messages"] as $key => $message) {
                $response["Messages"][$key]["Subject"] = html_entity_decode(trim($message["Subject"]));
            }
        }
        break;

    /*--------------------------------------------------------------------------------------------
    Claims the player's cashback if any
    --------------------------------------------------------------------------------------------*/
    case "claimCashback":

        $params = array(    "PlayerID"      => array($playerID, "int", 0),
            "TxCashBackID"  => array($_REQUEST["c"], "int", 0),
            "SessionID"     => array($sessionID, "str", 36),
            "lang"          => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"            => array(getPlayerIP(), "str", 15)
        );

        $response = queryDB($ajax->db,"DataClaimCashback",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    Gets the player Cashback if any
    --------------------------------------------------------------------------------------------*/
    case "getPlayerCashback":

        if ($response = $ajax->checkIfSessionIdNull($sessionID)) {
            break;
        }

        $params = array(    "PlayerID"      => array($playerID, "int", 0),
            "SessionID"     => array($sessionID, "str", 36),
            "lang"          => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"            => array(getPlayerIP(), "str", 15)
        );

        $response = queryDB($ajax->db,"DataGetPlayerCashback",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    Sends the info of the Affiliate trail to the db after a first deposit (utils.php)
    --------------------------------------------------------------------------------------------*/
    case "firstDeposit":
        $response = keepAffiliateTrail($ajax->db);

        break;

    /*--------------------------------------------------------------------------------------------
    Gets the reality type for the given player
    --------------------------------------------------------------------------------------------*/
    case "realityGet":
        if ($response = $ajax->checkIfSessionIdNull($sessionID)){
            break;
        }
        $params = array(    "PlayerID"      => array($playerID, "int", 0),
            "SessionID"     => array($sessionID, "str", 36),
            "IP"            => array(getPlayerIP(), "str", 15),
            "lang"          => array(@$_COOKIE["LanguageCode"], "str", 2)
        );

        $response = queryDB($ajax->db,"DataRealityCheckGet",$params,false,false,$rs);
        $response['FrequencyOptions'] = queryDBNextRS($rs);


        break;

    /*--------------------------------------------------------------------------------------------
    Sets the reality type for the given player
    --------------------------------------------------------------------------------------------*/
    case "realitySet":

        $params = array(    "RealityCheckTypeID"      => array(@$_REQUEST["rc"], "int", 0),
            "PlayerID"      => array($playerID, "int", 0),
            "SessionID"     => array($sessionID, "str", 36),
            "IP"            => array(getPlayerIP(), "str", 15),
            "lang"          => array(@$_COOKIE["LanguageCode"], "str", 2)
        );

        $response = queryDB($ajax->db,"DataRealityCheckSet",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    Stops the reality Check for a given game for a given player
    --------------------------------------------------------------------------------------------*/
    case "realityStop":

        $params = array(    "GameID"      => array(@$_REQUEST["g"], "str", 50),
            "PlayerID"      => array($playerID, "int", 0),
            "SessionID"     => array($sessionID, "str", 36),
            "IP"            => array(getPlayerIP(), "str", 15),
            "lang"          => array(@$_COOKIE["LanguageCode"], "str", 2),
            "DeviceCategoryID" => array(config("RealDevice"), "int", 0),
            "GameLaunchSourceID" => array(1, "int", 0),
            "SkinID"        => array(config("SkinID"), "int", 0)
        );

        $response = queryDB($ajax->db,"DataRealityCheckStopGame",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    Starts the reality Check for a given game for a given player
    --------------------------------------------------------------------------------------------*/
    case "realityStart":

        $params = array(    "GameID"      => array(@$_REQUEST["g"], "str", 50),
            "PlayerID"      => array($playerID, "int", 0),
            "SessionID"     => array($sessionID, "str", 36),
            "IP"            => array(getPlayerIP(), "str", 15),
            "lang"          => array(@$_COOKIE["LanguageCode"], "str", 2),
            "DeviceCategoryID" => array(config("RealDevice"), "int", 0),
            "GameLaunchSourceID" => array(1, "int", 0),
            "SkinID"        => array(config("SkinID"), "int", 0)

        );

        $response = queryDB($ajax->db,"DataRealityCheckStartGame",$params);
        break;
    /*--------------------------------------------------------------------------------------------
    GET GAME INFO FOR CONTENT
    --------------------------------------------------------------------------------------------*/
    case "getgameinfo":


        $params =   array("GameID"        => array(@$_REQUEST["id"], "str", 100),
            "SkinID"        => array(config("SkinID"), "int", 0)
        );

        $response = queryDB($ajax->db,"DataGetGamesInfo",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    CLAIMS A PRIZE PLAYER
    --------------------------------------------------------------------------------------------*/
    case "claimPrize":
        $params =   array(  "PlayerID"      => array($playerID, "int", 0),
            "SessionID"     => array($sessionID, "str", 36),
            "IP"            => array(getPlayerIP(), "str", 15),
            "lang"          => array(@$_COOKIE["LanguageCode"], "str", 2),
            "PrizePlayerID" => array(@$_REQUEST["p"], "int", 0)
        );

        $response = queryDB($ajax->db,"DataClaimPrize",$params);

        break;

    /*--------------------------------------------------------------------------------------------
    SUBMITS A POTENTIAL PLAYER
    --------------------------------------------------------------------------------------------*/
    case "submitPotentialPlayer":

        $email = @$_REQUEST["e"];
        $wantsEmail = @$_REQUEST["we"];

        if(!validate("email", $email))
        {
            $response["Code"] = 1;
            $response["Msg"] = "Please enter a valid email address";
            break;
        }

        $params = array(
            "SkinID" => array(config("SkinID"), "int", 0),
            "Email"  => array($email, "int", 0),
            "WantsEmail"  => array($wantsEmail, "int", 0)
        );

        $response = queryDB($ajax->db,"DataSubmitPotentialPlayer",$params);

        break;

    /*--------------------------------------------------------------------------------------------
    INTELLIGENT GAMMING API REQUESTS
    --------------------------------------------------------------------------------------------*/
    case "getCustomer":

        require "tasks/ig/SoapClient.php";
        require "tasks/ig/GetCustomer.php";

        try {

            // create request
            $myRequest = new IgGetCustomerRequest();

            if ($myRequest->valid) {

                // create custom soap client
                $client = new radSoapClient(config("IGapi"));

                // do the request
                $igResponse = $client->doRequest($function, $myRequest->request);

                // get response
                $response = $myRequest->outputResponse($igResponse);

            } else {

                $response = $myRequest->error;
            }



        } catch( Exception $error ) {

            $response = array(
                "Code"  => 1,
                "Msg"   => "Something went wrong, please verify that the data entered is correct and try again.",
                "Error" => $error->getMessage()
            );
        }

        break;

    /*--------------------------------------------------------------------------------------------
    GET PLAYER ACCOUNT VERIFICATION
    --------------------------------------------------------------------------------------------*/
    case "getPlayerAccountVerification":
        require_once("autoload.php");
        $client = new GuzzleHttp\Client(['verify' => false]);
        $url = config("PlayerManagementAPI");
        $res = $client->request('GET', "{$url}/api/v1/verification/", [
            'query' => ['playerId' => $playerID, 'sessionId' => $sessionID]
        ]);

        $response["Code"] = 0;
        $response["StatusCode"] = $res->getStatusCode();
        $response["Body"] = $res->getBody()->getContents();

        break;

    /*--------------------------------------------------------------------------------------------
    GET DOCUMENTS
    --------------------------------------------------------------------------------------------*/
    case "getDocuments":
        require_once("autoload.php");
        $client = new GuzzleHttp\Client(['verify' => false]);
        $url = config("PlayerManagementAPI");
        $res = $client->request('GET', "{$url}/api/v1/verification/documents", []);

        $response["Code"] = 0;
        $response["StatusCode"] = $res->getStatusCode();
        $response["Body"] = $res->getBody()->getContents();

        break;


    /*--------------------------------------------------------------------------------------------
    UPLOAD FILES
    --------------------------------------------------------------------------------------------*/
    case "uploadFilesPlayerAccountVerification":
        require_once("autoload.php");
        $client = new GuzzleHttp\Client(['verify' => false]);
        if(is_array($_FILES) && count($_FILES) > 0) {
            $multipart = [];
            foreach ($_FILES as $key => $value) {
                $sourcePath = $value["tmp_name"];
                $drive = explode('\\', $_SERVER['DOCUMENT_ROOT'])[0];
                $targetPath = "$drive\WEB\documents\\".$value['name'];
                if (is_uploaded_file($sourcePath) && move_uploaded_file($sourcePath,$targetPath)) {
                    $content = file_get_contents($targetPath);
                    $file = [
                        'name' => 'documents',
                        'filename' => $value['name'],
                        'contents' => $content
                    ];
                    $multipart[] = $file;
                    unlink($targetPath);
                } else {
                    throw new Exception("Error with the uploaded file or moving it from {$sourcePath} to {$targetPath}");
                }
            }
            $multipart[] = [
                'name' => 'playerId',
                'contents' => $playerID
            ];
            $multipart[] = [
                'name' => 'sessionId',
                'contents' => $sessionID
            ];
            $url = config("PlayerManagementAPI");
            $res = $client->request('POST', "{$url}/api/v1/verification/documents", ['multipart' => $multipart]);

            $response["Code"] = 0;
            $response["StatusCode"] = $res->getStatusCode();
            $response["Body"] = $res->getBody()->getContents();
        }
        break;

    /*--------------------------------------------------------------------------------------------
    GET SOCIAL GAMING VERIFICATION
    --------------------------------------------------------------------------------------------*/

    case "getSocialResponsiblePopupAction":

        $action = @$_REQUEST["a"];
        require_once("autoload.php");
        $client = new GuzzleHttp\Client(['verify' => false]);
        $url = config("PlayerManagementAPI");
        $res = $client->request('POST', "{$url}/api/v1/SocialGamingResponsibility/action", [
            'query' => ['playerId' => $playerID, 'action' => $action]
        ]);

        $response["Code"] = 0;
        $response["StatusCode"] = $res->getStatusCode();
        $response["Body"] = $res->getBody()->getContents();

        break;
    /*--------------------------------------------------------------------------------------------
    HLR
    --------------------------------------------------------------------------------------------*/

    case "hlr":
        $cogsIPs = array('192.168.35.70', '192.168.205.70');
        // we just allow CogsIps to access this area
        if (in_array(getPlayerIP(),$cogsIPs))
        {


            require_once("libs/MobivateManager.php");

            $mobivate = new MobivateManager($ajax->db);

            $playerId = (isset($_REQUEST["playerId"]) && $_REQUEST["playerId"]) ? $_REQUEST["playerId"] : 0;
            $message = (isset($_REQUEST["message"]) && $_REQUEST["message"]) ? $_REQUEST["message"] : ' from COGS';
            $mobivate->checkHLR($_REQUEST["phoneNumber"], $playerId, getPlayerIP(), $message);
        }
        break;

    /*--------------------------------------------------------------------------------------------
    Claims the player's upgrade bonus if any
    --------------------------------------------------------------------------------------------*/
    case "claimUpgradeBonus":

        $params = array(   "PlayerID"      => array($playerID, "int", 0),
            "TxPromoClaimID"  => array($_REQUEST["c"], "int", 0),
            "SessionID"    => array($sessionID, "str", 36),
            "lang"         => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"           => array(getPlayerIP(), "str", 15)
        );

        $response = queryDB($ajax->db,"DataClaimUpgradeBonus",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    Gets the player upgrade bonus if any
    --------------------------------------------------------------------------------------------*/
    case "getPlayerUpgradeBonus":

        if ($response = $ajax->checkIfSessionIdNull($sessionID)) {
            break;
        }

        $params = array(   "PlayerID"      => array($playerID, "int", 0),
            "SessionID"    => array($sessionID, "str", 36),
            "lang"         => array(@$_COOKIE["LanguageCode"], "str", 2),
            "IP"           => array(getPlayerIP(), "str", 15)
        );

        $response = queryDB($ajax->db,"DataGetPlayerUpgradeBonus",$params);
        break;

    /*--------------------------------------------------------------------------------------------
    MAIL SEND MAIL
    --------------------------------------------------------------------------------------------*/
    case "mailSendMail":

        $params =   array();

        $response = queryDB($ajax->db,"mailSendMail",$params,true,false,$rs);

        break;
    /*
    case "flush":

    if(!$ajax->useMemcache)
    {
        $ajax->memcache = new Memcache;
        $ajax->memcache->connect("127.0.0.1",11210);
    }

    $ajax->memcache->flush();

    echo "memcache flushed";
    exit;
    break;
    */


    /*--------------------------------------------------------------------------------------------
    get all TC's and PP that a player should accept
    --------------------------------------------------------------------------------------------*/
    case "getAllTermsForPlayer":

        $api = config("PlayerManagementAPI");

        // session validation

        $params = [
            'playerId' => $playerID,
            'sessionId' => @$_REQUEST["s"]
        ];

        $session = $ajax->callMicroservice($api, 'session/check', $params, 0, 'GET', []);

        // is session valid?

        if ($session["body"]["status"] === "OK") {

            // yes - then we can do the get terms for this player
            $params = [
                'playerId' => $playerID,
                'skinId' => config("SkinID")
            ];

            $terms = $ajax->callMicroservice(config("PlayerManagementAPI"), 'contents/non-accepted', $params, 1, 'GET', []);

            if ($terms["httpStatus"] === 200) {

                $response["Code"] = 0;

                // loop through the array
                foreach ($terms["body"] as $term) {

                    if ($term["contentTypeId"] === 1) {

                        $response["tcsId"] = $term["id"];
                        $response["tcs"] = $term["content"];
                    }

                    if ($term["contentTypeId"] === 2) {

                        $response["ppId"] = $term["id"];
                        $response["pp"] = $term["content"];
                    }
                }

            } else {

                $response["Code"] = 1;
                $response["Msg"] = "Oops! Something unexpected happened. Please contact support and quote 'R02'";

            }

        } else {

            // no

            $response["Code"] = 1;
            $response["Msg"] = "Oops! Something unexpected happened. Please contact support and quote 'R01'";
        }

        break;

    /*--------------------------------------------------------------------------------------------
    Save that player accepted the new version of TC's and/or PP
    --------------------------------------------------------------------------------------------*/
    case "playerAcceptedTerms":

        // if sent as true the players record will be always updated
        $gdprCompliant = true;

        // get player detail
        $res =$ajax->callMicroservice(config("PlayerManagementAPI"), 'players/' .$playerID, [], 1, 'GET', []);

        if ($res['httpStatus'] >= 200 and $res['httpStatus'] < 300) {

            // send complaint flag to CS if they need to update the player's record 
            // there is no need to update if they were already compliant

            $gdprCompliant = $res["body"]["gdprCompliant"];
            if (!$gdprCompliant) $gdprCompliant = true;
        }

        // player accepted terms request

        $params = [
            'playerId' => $playerID,
            'acceptedContents' => @$_REQUEST["c"],
            'gdprCompliant' => $gdprCompliant
        ];

        $res =$ajax->callMicroservice(config("PlayerManagementAPI"), 'player-content-acceptance', $params, 1, 'POST', []);

        // success?
        if ($res['httpStatus'] >= 200 and $res['httpStatus'] < 300) {

            $response["Code"] = 0;

        } else {

            $response["Code"] = 1;
            $response["Msg"] = "Oops! Something unexpected happened. Please contact support and quote 'R03'";
        }

        break;


    /*--------------------------------------------------------------------------------------------
    Player registration accepts terms
    --------------------------------------------------------------------------------------------*/
    case "playerAcceptedTermsUponRegistration":

        $params = [
            'playerId' => @$_REQUEST["p"],
            'acceptedContents' => @$_REQUEST["c"],
            'gdprCompliant' => true
        ];

        $res =$ajax->callMicroservice(config("PlayerManagementAPI"), 'player-content-acceptance', $params, 1, 'POST', []);

        // success?
        if ($res['httpStatus'] >= 200 and $res['httpStatus'] < 300) {

            $response["Code"] = 0;

        } else {

            $response["Code"] = 1;
            $response["Msg"] = "Oops! Something unexpected happened. Please contact support and quote 'R04'";
        }

        break;

    /*--------------------------------------------------------------------------------------------
    Get player details
    --------------------------------------------------------------------------------------------*/
    case "getPlayerEntity":

        // get player detail
        $res =$ajax->callMicroservice(config("PlayerManagementAPI"), 'players/' .$playerID, [], 1, 'GET', []);

        if ($res['httpStatus'] >= 200 and $res['httpStatus'] < 300) {
            $response["Code"] = 0;
            $response["Body"] = $res["body"];
        }

        break;

    /*--------------------------------------------------------------------------------------------
    Update player 3rd party details
    --------------------------------------------------------------------------------------------*/
    case "updatePlayerCommsFromPopUp":

        $commsParams = [
            'wantsThirdPartyCalls' => @$_REQUEST["tpc"],
            'wantsThirdPartySMS' => @$_REQUEST["tpsms"],
            'wantsThirdPartyEmails' => @$_REQUEST["tpemails"],
            'source' => 'third-party-gdpr'
        ];

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), "players/".$playerID."/communications", $commsParams, 1, 'PATCH', []);

        if ($res['httpStatus'] >= 200 and $res['httpStatus'] < 300) {
            $response["Code"] = 0;
        } else {
            $response["Code"] = 1;
            $response["Msg"] = "Oops! Something unexpected happened. Please contact support and quote 'R05'";

        }

        break;
    //------------------------------------------------------------------------------------------------------------------------------------------
    //  Three Radical GET HMAC Token to append to URLs
    //------------------------------------------------------------------------------------------------------------------------------------------
    case "getThreeRadicalHMACToken":
        if(isset($sessionID))
        {
            $threeParams = [
                'playerId' => $playerID,
                'sessionId' => $sessionID,
                'skinId' => config("SkinID")
            ];
            $service_url = config("ThreeRadicalAPI");
            $res=$ajax->callMicroService($service_url, 'internal/generate-hmac', $threeParams, 1,'GET');
            if ($res['httpStatus'] >= 200 and $res['httpStatus'] < 300) {
                $response["Code"] = 0;
                $response["hmac"] = $res["body"];
                $response["playerID"] = $playerID;
            } else {
                $response["Code"] = 1;
                $response["Msg"] = "Oops! Something unexpected happened. Please contact support and quote 'R06'";
                $response["Debug"] = $res;
            }
        }
        break;
    case "getAspersTermsAndConditions":
        require_once("autoload.php");
        $client = new GuzzleHttp\Client(['verify' => false]);
        $url = config("URL")."/register?terms=1";
        $res = $client->request('GET', "{$url}");

        if ($res->getStatusCode() == 200) {
            $termsBody = $res->getBody()->getContents();
            $body = json_decode($termsBody, true);
            $response["tcContent"] = $body["tcContent"];
            $response["tcID"] = $body["tcID"];
            $response["ppContent"] = $body["ppContent"];
            $response["ppID"] = $body["ppID"];
        }
        break;

    case "doAspersFirstLogin":
        if ( @$_REQUEST["wp"] == "false" ){
            @$_REQUEST["wp"] = 1;
        }else{
            @$_REQUEST["wp"] = 0;
        }
        $params =   array(
            "PlayerIP"              => array(getPlayerIP(), "str", 50),
            "WantsEmail"            => array(@$_REQUEST["we"], "int", 0),
            "WantsSMS"              => array(@$_REQUEST["ws"], "int", 0),
            "WantsPhone"            => array(@$_REQUEST["wp"], "int", 0),
            "WantsTPEmail"          => array(@$_REQUEST["wte"], "int", 0),
            "WantsTPPhone"          => array(@$_REQUEST["wtp"], "int", 0),
            "WantsTPSMS"            => array(@$_REQUEST["wts"], "int", 0),
            "PlayerID"              => array(@$_REQUEST["pID"], "int", 0)
        );
        $response = queryDB($ajax->db,"AspersFirstLogin",$params);



        if(@$_REQUEST["wd"] == 1){
            update_ig_preferences(@$_REQUEST["wte"],@$_REQUEST["wte"],@$_REQUEST["acn"]);
        }
        break;


    case "test":
        /*
        $row["Code"] = 0;
        $row["RegEmailValidation"] = 0;
        $row["PromoEmailValidation"] = 0;
        $row["ValidationGUID"] = 0;
        $row["PlayerID"] = 11680;
        $row["Username"] = 'hamish86';
        $row["Email"] = 'hamishi@spacebarmedia.com';
        $row["Firstname"] = 'hamish';
        $row["Password"] = 'qqq111';
        
        sendRegistrationMail($row);
        echo "there";
        break;
        */
    default:

        $function = "Function Not Found";
        $params = array();
        break;
}
/*--------------------------------------------------------------------------------------------
DEBUGGING ON DEV OR RETURN THE RESPONSE
--------------------------------------------------------------------------------------------*/
if(@$_REQUEST["debug"] && (Env::inState([Env::local, Env::dev])))
{

    echo "\n";
    echo "Debug Ajax calls";
    echo "\n\n";

    echo "Function: ".$function;
    echo "\n\nRequest\n";
    print_r($params);
    echo "\nSQL\n\n";
    echo 'exec data'. $function .' '.SdbParams ($params);

    echo "\n\nResponse\n";
    print_r($response);

}

else
{
    //  callback handler added 23/12/14 - MC

    # If a "callback" GET parameter was passed, use its value, otherwise false. Note
    # that "callback" is a defacto standard for the JSONP function name, but it may
    # be called something else, or not implemented at all. For example, Flickr uses
    # "jsonpcallback" for their API.
    $jsonp_callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;

    if ($jsonp_callback)
    {
        echo $jsonp_callback . '(' . json_encode($response) . ')';
    }
    else
    {
        echo json_encode($response);
    }


    function update_ig_preferences($wantsTPEmail, $wantsTPSMS, $aspersCN ){
        $url = config("ig-update-customer");

        if ($url) {

            // build url passing the player details

            $url = $url . "?email=" . @$wantsTPEmail . "&sms=" . @$wantsTPSMS  . "&acn=" . $aspersCN;
            echo $url;

            try {

                // just do the request any error will be logged on the endpoint

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
                curl_setopt($curl, CURLOPT_CAINFO, __FILE__ . "\\..\\cacert.pem");
                curl_setopt($curl, CURLOPT_CAPATH, __FILE__ . "\\..\\");
                $curl_response = curl_exec($curl);

            } catch (Exception $e) {
                // do nothing - just ignore 
            }

        }

    }

}
?>