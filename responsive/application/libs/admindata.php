<?php
header("Content-Type: application/javascript");
require 'controller/ajax-request/testajax.php';
require 'libs/S3ClientWrapper.php';

if(Env::isLive()) exit; // admin functions can only be done on staging / local / dev

$function =  @$_REQUEST['f'];

//Just to make use of the ajax openDB, as we implemented it abstract
$ajax = new oldDataAjax();
$response = array();
$ajax->initServices();

if(!isset($_COOKIE["EditUser"]) && $function != 'login')
{
	$response["Code"] = -10;
	$response["Msg"] = 'Invalid User';
}



switch($function)
{

	/*--------------------------------------------------------------------------------------------
		LOGIN TO THE EDITOR
	--------------------------------------------------------------------------------------------*/
	case "login":

		$params = 	array("Username" 	=> array(@$_REQUEST["u"], "str", 50),
						  "Password" 	=> array(@$_REQUEST["p"], "str", 50),
						  "IP"     		=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminLogin",$params);
		if($response["Code"] == 0)
		{

            $allowedSkinsForEightBall = array(
                Skin::GiveBackBingo,
                Skin::LuckyVIP,
                Skin::RegalWins,
            );

            if (($response["UserGroup"] == UserGroup::EightBall) && !in_array(config("SkinID"), $allowedSkinsForEightBall)) {
                unset($response);
                $response["Code"] = -10;
                $response["Msg"] = 'The user is not allowed to access this site';
            } else {
                setacookie("EditUser", $response["UserID"], 0, '/');
                setacookie("AdminSessionID", $response["AdminSessionID"], 0, '/');
                setacookie("AdminUsername", $response["AdminUsername"], time() + 60 * 60 * 24 * 30, '/');
            }

			//print_r($response);

		}
		else
		{
			ClearSession();
		}
		break;

    /*--------------------------------------------------------------------------------------------
        LOGOUT TO THE EDITOR
    --------------------------------------------------------------------------------------------*/
    case "logout":
        ClearSession();
        $response["Code"] = 0;
        $response["Msg"] = 'Session cleared successfully';
        break;


	/*--------------------------------------------------------------------------------------------
		GET LIST OF CONTENT FOR A PAGE / KEY / SUBKEY
	--------------------------------------------------------------------------------------------*/
	case "categorylist":


		$params = 	array("SkinID" 		 	 => array(config("SkinID"), "int", 0),
						  "Page" 		 	 => array(@$_REQUEST["p"], "str", 100),
						  "Key" 		 	 => array(@$_REQUEST["k"], "str", 100),
						  "SubKey" 		 	 => array(@$_REQUEST["skey"], "str", 100),
						  "UserID"			 => array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 => array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 => array(getPlayerIP(), "str", 50),
						  "CreateCategory"   => array(@$_REQUEST["c"], "int", 0)
          );


		$response = queryDB($ajax->db,"AdminGetContentCategoryList",$params,false,false,$rs);

		if($response["Code"] == 0)
		{
			$content = queryDBNextRS($rs);
			$response["Data"] = $content;
		}
		break;

	/*--------------------------------------------------------------------------------------------
		GET LIST OF CONTENT FOR A PAGE / KEY / ALL SUBKEYS
	--------------------------------------------------------------------------------------------*/
	case "categorylistall":


		$params = 	array("SkinID" 		 	 => array(config("SkinID"), "int", 0),
						  "Page" 		 	 => array(@$_REQUEST["p"], "str", 100),
						  "Key" 		 	 => array(@$_REQUEST["k"], "str", 100),
						  "UserID"			 => array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 => array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 => array(getPlayerIP(), "str", 50)
          );


		$response = queryDB($ajax->db,"AdminGetContentCategoryListAll",$params,false,false,$rs);

		if($response["Code"] == 0)
		{
			$content = queryDBNextRS($rs);
			$response["Data"] = $content;
		}
		break;


	/*--------------------------------------------------------------------------------------------
		GET LIST OF EDITOR USERS
	--------------------------------------------------------------------------------------------*/
	case "editoruserslistall":
        if (isLoggedInSkinEditor() && isAdminInSkinEditor()){
            $response["Code"] = 0;
            $users = $ajax->loadRepository("EditUser")->fetchAll();
            $response["Data"] = $users;
        } else {
            $response["Code"] = -10;
            $response["Msg"] = 'Invalid user';
        }
        break;

    /*--------------------------------------------------------------------------------------------
        INSERT A NEW USER INTO THE DDBB
    --------------------------------------------------------------------------------------------*/
    case "newuser":

        if(!isset($_REQUEST["user"]))
        {
            $response["Code"] = -10;
            $response["Msg"] = 'No user sent';
        } else {
            $user = json_decode(base64_decode($_REQUEST["user"]), true);

            try{
                $ajax->loadRepository("EditUser")->insert($user);
                $response["Code"] = 0;
            } catch(ErrorException $e) {
                $response["Code"] = $e->getCode();
                $response["Msg"] = $e->getMessage();
            }
        }
        break;

    /*--------------------------------------------------------------------------------------------
        UPDATE THE MODIFICATION OF A USER
    --------------------------------------------------------------------------------------------*/
    case "updateuser":

        if(!isset($_REQUEST["user"]))
        {
            $response["Code"] = -10;
            $response["Msg"] = 'No user sent';
        } else {
            $user = json_decode(base64_decode($_REQUEST["user"]), true);
            try{
                $ajax->loadRepository("EditUser")->updateById($user["ID"], $user);
                $response["Code"] = 0;
            } catch(ErrorException $e) {
                $response["Code"] = $e->getCode();
                $response["Msg"] = $e->getMessage();
            }
        }
        break;

    /*--------------------------------------------------------------------------------------------
        GET A PIECE OF CONTENT FOR THE ADMIN EDITOR
    --------------------------------------------------------------------------------------------*/
	case "content":


		$params = 	array("ID" 		 		 		=> array(@$_REQUEST["id"], "int", 0),
						  "Device" 		 		 	=> array(@$_REQUEST["d"], "int", 0),
						  "DefaultCategoryTypeID"	=> array(@$_REQUEST["cct"], "int", 0),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );


		$response = queryDB($ajax->db,"AdminGetContent",$params,false,false,$rs);


		if($response["Code"] == 0)
		{
			$type = queryDBNextRS($rs);
			$details = queryDBNextRS($rs);
			$intro = queryDBNextRS($rs);
			$body = queryDBNextRS($rs);
			$terms = queryDBNextRS($rs);

			$response["Offer"][] = array('ID' => 0, 'Name' => 'No Offer Specified');

			$response["Type"] = $type;

			if(count($details) > 0) $response["Details_tab"] = $details;
			if(count($intro) > 0) $response["Intro_tab"] = $intro;
			if(count($body) > 0) $response["Body_tab"] = $body;
			if(count($terms) > 0) $response["Terms_tab"] = $terms;
		}


		break;






	/*--------------------------------------------------------------------------------------------
		SAVE A PIECE OF CONTENT FOR THE ADMIN EDITOR
	--------------------------------------------------------------------------------------------*/
	case "setcontent":


		$image = str_replace('/../_global-library','/_global-library',@$_REQUEST["img"]);
		$image1 = str_replace('/../_global-library','/_global-library',@$_REQUEST["img1"]);

		$params = 	array("ID" 						=> array(@$_REQUEST["id"], "int", 0),
						  "ContentCategoryID" 		=> array(@$_REQUEST["ccid"], "int", 0),
						  "ContentCategoryTypeID" 	=> array(@$_REQUEST["cctid"], "int", 0),
						  "name" 					=> array(@$_REQUEST["n"], "str", 100),
						  "title" 					=> array(@$_REQUEST["t"], "str", 100),
						  "image" 					=> array($image, "str", 200),
						  "imageAlt" 				=> array(@$_REQUEST["imga"], "str", 200),
						  "image1" 					=> array($image1, "str", 200),
						  "imageAlt1" 				=> array(@$_REQUEST["imga1"], "str", 200),
						  "detailsURL" 				=> array(@$_REQUEST["d"], "str", 200),
						  "intro" 					=> array(@$_REQUEST["i"], "str", 1000),
						  "body" 					=> array(@$_REQUEST["b"], "str", 80000),
						  "terms" 					=> array(@$_REQUEST["tm"], "str", 80000),
						  "Text1" 					=> array(@$_REQUEST["t1"], "str", 1000),
						  "Text2" 					=> array(@$_REQUEST["t2"], "str", 1000),
						  "Text3" 					=> array(@$_REQUEST["t3"], "str", 1000),
						  "Text4" 					=> array(@$_REQUEST["t4"], "str", 1000),
						  "Text5" 					=> array(@$_REQUEST["t5"], "str", 1000),
						  "Text6" 					=> array(@$_REQUEST["t6"], "str", 1000),
						  "Text7" 					=> array(@$_REQUEST["t7"], "str", 1000),
						  "Text8" 					=> array(@$_REQUEST["t8"], "str", 1000),
						  "Text9" 					=> array(@$_REQUEST["t9"], "str", 1000),
						  "Text10" 					=> array(@$_REQUEST["t10"], "str", 1000),
						  "Text11" 					=> array(@$_REQUEST["t11"], "str", 1000),
						  "Text12" 					=> array(@$_REQUEST["t12"], "str", 1000),
						  "Text13" 					=> array(@$_REQUEST["t13"], "str", 1000),
						  "Text14" 					=> array(@$_REQUEST["t14"], "str", 1000),
						  "Text15" 					=> array(@$_REQUEST["t15"], "str", 1000),
						  "Text16" 					=> array(@$_REQUEST["t16"], "str", 1000),
						  "Text17" 					=> array(@$_REQUEST["t17"], "str", 1000),
						  "Text18" 					=> array(@$_REQUEST["t18"], "str", 1000),
						  "Days"	 				=> array(@$_REQUEST["days"], "str", 100),
						  "OfferID"	 				=> array(@$_REQUEST["o"], "int", 100),
						  "DateFrom_DST" 			=> array(@$_REQUEST["df"], "date", 100),
						  "DateTo_DST"	 			=> array(@$_REQUEST["dt"], "date", 100),
						  "DeviceCategoryIDs"	 	=> array(@$_REQUEST["dc"], "str", 100),
						  "StatusID" 				=> array(@$_REQUEST["s"], "int", 0),
						  "Top" 					=> array(@$_REQUEST["top"], "int", 0),
						  "EditUserID"			 	=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );


		$response = queryDB($ajax->db,"AdminSetContent",$params,false,false,$rs);
		break;

	/*--------------------------------------------------------------------------------------------
		SET THE ORDER OF CONTENT FOR A CATEGORY IN THE ADMIN EDITOR
	--------------------------------------------------------------------------------------------*/
	case "setcontentorder":


		$params = 	array("ContentCategoryID" 		=> array(@$_REQUEST["ccid"], "int", 0),
						  "SkinID" 					=> array(config("SkinID"), "int", 0),
						  "Order" 					=> array(@$_REQUEST["o"], "str", 4000),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );


		$response = queryDB($ajax->db,"AdminSetContentOrder",$params);
		break;

    /*--------------------------------------------------------------------------------------------
    GET CONTENT IMAGES PATH
--------------------------------------------------------------------------------------------*/
    case "getcontentimages":

        $params = 	array("ContentCategoryTypeID" 	=> array(@$_REQUEST["cctid"], "int", 0),
            "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
            "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
            "IP"     			 		=> array(getPlayerIP(), "str", 50)
        );

        $r = queryDB($ajax->db,"AdminGetContentImages",$params);

        if($r["Code"] == 0)
        {
            $folder = $r["ImagePath"];
            if ($folder == '/../_global-Library/_upload-images/') {
                $prefix = 'images/_global-library/_upload-images/';
            } else {
                $prefix = 'images/' . config("Skin") . $folder;
            }
            $subpath = (isset($_REQUEST["d"])) ? $_REQUEST["d"] : '';

            $parentPath = '';
            if($subpath != '')
            {
                $parentPath = dirname($subpath);
                if($parentPath == '.') $parentPath = '';
                if($parentPath != '')  $parentPath.='/';
                $prefix .= $subpath;
                $folder .= $subpath;
            }

            $s3ClientWrapper = new S3ClientWrapper();
            $s3ClientWrapper->setPrefix($prefix);
            $iterator = $s3ClientWrapper->listObjects();
            $folders = array();
            $images = array();
            foreach ($iterator as $object) {
                if ($object["Size"] == 0) {
                    $directory = $s3ClientWrapper->getNextLevel($object['Key']);
                    if($directory == '') {
                        $folders[] = array('type' => 1, 'display' =>'Up Folder', 'path' => $parentPath);
                    } else {
                        $folderTemp = array('type' => 2, 'display' => $directory, 'path' => $directory);
                        if (!in_array($folderTemp, $folders)) {
                            $folders[] = $folderTemp;
                        }
                    }
                } else {
                    $image = $s3ClientWrapper->getFilename($object['Key']);
                    if ($image != null)
                        $images[] = array('display' => $image,'size' => $object['Size']);
                }
            }

            $response["Code"] = 0;
            $response["ImagePath"] = $folder;
            $response["Folders"] = $folders;
            $response["Images"] = $images;
        }

        break;

    /*--------------------------------------------------------------------------------------------
        GET CONTENT IMAGES PATH
    --------------------------------------------------------------------------------------------*/
	case "getcontentimagesbysearch":

		$params = 	array("ContentCategoryTypeID" 	=> array(@$_REQUEST["cctid"], "int", 0),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
        );
		$r = queryDB($ajax->db,"AdminGetContentImages",$params);

		if($r["Code"] == 0) {

            $folder = $r["ImagePath"];
            if ($folder == '/../_global-Library/_upload-images/') {
                $prefix = 'images/_global-library/_upload-images/';
            } else {
                $prefix = 'images/' . config("Skin") . $folder;
            }
            $extract = $prefix;
            $subpath = (isset($_REQUEST["d"])) ? $_REQUEST["d"] : '';
            $searchText = (isset($_REQUEST["s"])) ? $_REQUEST["s"] : '';
            if($subpath != '')
                $prefix .= $subpath;

            $s3ClientWrapper = new S3ClientWrapper();
            $s3ClientWrapper->setPrefix($prefix);
            $iterator = $s3ClientWrapper->listObjects();
            $images = array();
            foreach ($iterator as $object) {
                if ($object["Size"] != 0 && strpos($object['Key'], $searchText) !== false) {
                    $image = str_replace($extract, "", $object['Key']);
                    $images[] = array('display' => $image,'size' => $object['Size']);
                }
            }

			$response["Code"] = 0;
			$response["ImagePath"] = $folder;
			$response["Folders"] = false;
			$response["Images"] = $images;
		}

		break;

	/*--------------------------------------------------------------------------------------------
		UPLOAD AN IMAGE
	--------------------------------------------------------------------------------------------*/
	case "uploadimage":

		$return = 0;
		if(is_array($_FILES) && is_uploaded_file($_FILES['userImage']['tmp_name']))
		{
		    $realpath = realpath(".");
		    $path = explode("\\", $realpath);
            $skin = end($path);
            $prefix = "images/".$skin;
            $filename = $_FILES['userImage']['name'];
            $key = $prefix.$_REQUEST['path'].$filename;
            $targetPath = $realpath."\\..\\..\\..\\images\\".$skin."\\".$_REQUEST['path'].$filename;
            $sourcePath = $_FILES["userImage"]["tmp_name"];

            $response["TargetPath"] = $targetPath;
            $response["AwsS3Key"] = $key;
            if(!move_uploaded_file($sourcePath,$targetPath)) {
                throw new Exception("Error moving file from {$sourcePath} to {$targetPath}");
            } else {
                // store file content as a string in $str
                $object = file_get_contents($targetPath);

                $s3ClientWrapper = new S3ClientWrapper();
                $s3ClientWrapper->putObject($key, $object);

                $return = 1;
            }
		}
		$response["Code"] = $return;
		break;

	/*--------------------------------------------------------------------------------------------
		GET SEO CONTENT
	--------------------------------------------------------------------------------------------*/
	case "getseocontent":

		$params = 	array("SkinID" 					=> array(config("SkinID"), "int", 0),
						  "Page" 					=> array(@$_REQUEST["p"], "str", 200),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminGetSEOContent",$params);
		break;

	/*--------------------------------------------------------------------------------------------
		SET SEO CONTENT
	--------------------------------------------------------------------------------------------*/
	case "setseocontent":

		$params = 	array("SkinID" 					=> array(config("SkinID"), "int", 0),
						  "Page" 					=> array(@$_REQUEST["p"], "str", 200),
						  "Content" 				=> array(@$_REQUEST["c"], "str", 4000),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminSetSEOContent",$params);
		break;

	/*--------------------------------------------------------------------------------------------
		PUSH CONTENT LIVE
	--------------------------------------------------------------------------------------------*/
	case "setcontentlive":

		$params = 	array("ContentCategotyID" 		=> array(@$_REQUEST["ccid"], "int", 0),
						  "SubKey" 					=> array(@$_REQUEST["skey"], "str", 100),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50),
						  "ContentIDs" 				=> array(@$_REQUEST["cids"], "str", 100)
          );

		$response = queryDB($ajax->db,"AdminSetContentLive",$params);
        $ajax->flushCache();

		break;

	/*--------------------------------------------------------------------------------------------
		BRING ALL CONTENT FROM LIVE BACK TO STAGING
	--------------------------------------------------------------------------------------------*/
	case "revertallcontent":

		$params = 	array("ContentCategotyID" 		=> array(@$_REQUEST["ccid"], "int", 0),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminRevertAllContent",$params);
		break;

	/*--------------------------------------------------------------------------------------------
		BRING A SINGLE BIT OF CONTENT FROM LIVE BACK TO STAGING
	--------------------------------------------------------------------------------------------*/
	case "revertcontent":

		$params = 	array("ContentID" 	=> array(@$_REQUEST["cid"], "int", 0),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminRevertContent",$params);
		break;



	/*--------------------------------------------------------------------------------------------
		CLEAR THE EDITING FLAG
	--------------------------------------------------------------------------------------------*/
	case "canceledit":


		$params = 	array("ContentID" 	 => array(@$_REQUEST["id"], "int", 0),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
		);


		$response = queryDB($ajax->db,"AdminCancelContentEditing",$params);
		break;


	/*--------------------------------------------------------------------------------------------
		DELETE CONTENT
	--------------------------------------------------------------------------------------------*/
	case "deletecontent":


		$params = 	array("ID" 	 		=> array(@$_REQUEST["id"], "int", 0),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminDeleteContent",$params);
		break;

	/*--------------------------------------------------------------------------------------------
		GET GAME INFO FOR CONTENT
	--------------------------------------------------------------------------------------------*/
	case "getgameinfo":


		$params = 	array("GameID" 				=> array(@$_REQUEST["id"], "str", 100),
						  "SkinID" 				=> array(config("SkinID"), "int", 0),
						  "UserID"			 	=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 	=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 	=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminGetGamesInfo",$params);
		break;

	/*--------------------------------------------------------------------------------------------
		GET GAME LIST
	--------------------------------------------------------------------------------------------*/
	case "searchgames":


		$params = 	array("SkinID" 	 	 => array(config("SkinID"), "int", 0),
						  "GameID" 		 => array(($_REQUEST["id"] != '') ? $_REQUEST["id"] : 0, "int", 0),
						  "NameFilter"   => array(@$_REQUEST["name"], "string", 100),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
          );

		$response = queryDB($ajax->db,"AdminSearchGames",$params,false,false,$rs);
		if($response["Code"] == 0)
		{
			$response['Games'] 	= queryDBNextRS($rs);
		}


		break;

	/*--------------------------------------------------------------------------------------------
		GET GAME CATEGORY SKINS
	--------------------------------------------------------------------------------------------*/
	case "getgamecategoryskins":


		$params = 	array("GameID" 	 				=> array(@$_REQUEST["id"], "int", 0),
						  "SkinID" 	 	 			=> array(config("SkinID"), "int", 0),
						  "UserID"			 		=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	 		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			 		=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"Admingetgamecategoryskins",$params,false,false,$rs);

		if($response["Code"] == 0)
		{
			$response['Skins'] = 	  queryDBNextRS($rs);
			$response['Categories'] = queryDBNextRS($rs);
			$response['SkinCat'] 	= queryDBNextRS($rs);
		}


		break;

	/*--------------------------------------------------------------------------------------------
			ADD GAME TO CATEGORY DEVICES
	--------------------------------------------------------------------------------------------*/
	case "addgames":


		$params = 	array("GameID" 	 		=> array(@$_REQUEST["id"], "int", 0),
						  "Json" 	 		=> array(@$_REQUEST["json"], "string", 8000),
						  "UserID"			=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminAddGames",$params,false,false,$rs);
		break;


	/*--------------------------------------------------------------------------------------------
			GET A LIST OF ALL THE SKINS
	--------------------------------------------------------------------------------------------*/
	case "getskins":


		$params = 	array("UserID"			=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminGetSkins",$params,false,false,$rs);
		if($response["Code"] == 0)
		{
			$response['Skins'] = queryDBNextRS($rs);
			$response['Devices'] = queryDBNextRS($rs);
		}

		break;

	/*--------------------------------------------------------------------------------------------
			GET CATEGORIES FOR A SKIN
	--------------------------------------------------------------------------------------------*/
	case "getgamecategorylist":


		$params = 	array("SkinID" 	 	 	=> array($_REQUEST["sid"], "int", 0),
						  "UserID"			=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminGetGameCategoryList",$params,false,false,$rs);
		if($response["Code"] == 0)
		{
			$response['Categories'] = queryDBNextRS($rs);
		}

		break;

	/*--------------------------------------------------------------------------------------------
			GET ALL GAMES IN A CATEGORY
	--------------------------------------------------------------------------------------------*/
	case "getgamesincategory":


		$params = 	array("CategoryID" 	 		=> array(@$_REQUEST["id"], "int", 0),
						  "SkinID" 	 	 		=> array(@$_REQUEST["sid"], "int", 0),
						  "DeviceCategoryID" 	=> array(@$_REQUEST["did"], "int", 0),
						  "UserID"				=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     				=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminGetGamesInCategory",$params,false,false,$rs);
		if($response["Code"] == 0)
		{
			$response['Games'] = queryDBNextRS($rs);
		}

		break;


	/*--------------------------------------------------------------------------------------------
			SORT GAMES IN A CATEGORY
	--------------------------------------------------------------------------------------------*/
	case "setgameorder":


		$params = 	array("CategoryID" 	 		=> array(@$_REQUEST["id"], "int", 0),
						  "DeviceCategoryID" 	=> array(@$_REQUEST["did"], "int", 0),
						  "SkinID" 			=> array(@$_REQUEST["sid"], "int", 0),
						  "Json" 	 		=> array(@$_REQUEST["o"], "string", 8000),
						  "UserID"			=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"	=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     			=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminSetGameOrder",$params,false,false,$rs);
		break;

	/*--------------------------------------------------------------------------------------------
			SEND GAMES FROM STAGING TO LIVE
			either for 1 category / device or for the whole skin
	--------------------------------------------------------------------------------------------*/
	case "setgameslive":


		$params = 	array("GameCategoryDescID" 	=> array(@$_REQUEST["gc"], "int", 0),
						  "Device" 				=> array(@$_REQUEST["dc"], "int", 0),
						  "SkinID" 		 	 	=> array(@$_REQUEST["sid"], "int", 0),
						  "UserID"				=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     				=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminSetGamesLive",$params,false,false);
        $ajax->flushCache();


		break;

	/*--------------------------------------------------------------------------------------------
			REVERT GAMES FROM LIVE TO STAGING
			either for 1 category / device or for the whole skin
	--------------------------------------------------------------------------------------------*/
	case "revertgames":


		$params = 	array("GameCategoryDescID" 	=> array(@$_REQUEST["gc"], "int", 0),
						  "Device" 				=> array(@$_REQUEST["dc"], "int", 0),
						  "SkinID" 		 	 	=> array(@$_REQUEST["sid"], "int", 0),
						  "UserID"				=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     				=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminRevertGames",$params,false,false);

		break;

	/*--------------------------------------------------------------------------------------------
			REMOVE 1 OR MORE GAMES
	--------------------------------------------------------------------------------------------*/
	case "removegames":


		$params = 	array("GameCategoryDescID" 	=> array(@$_REQUEST["gc"], "int", 0),
						  "Device" 				=> array(@$_REQUEST["dc"], "int", 0),
						  "SkinID" 		 	 	=> array(@$_REQUEST["sid"], "int", 0),
						  "JSON" 		 	 	=> array(@$_REQUEST["json"], "string", 1000),
						  "UserID"				=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     				=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminRemoveGames",$params,false,false);

		break;


	/*--------------------------------------------------------------------------------------------
			RETURN ALL THE IMAGES FOR A WHOLE SKIN
	--------------------------------------------------------------------------------------------*/
	case "checkskinimages":

		$params = 	array("SkinID" 				=> array(config("SkinID"), "int", 0),
						  "Live"				=> array(@$_REQUEST["l"], "int", 0),
						  "Device" 				=> array(@$_REQUEST["d"], "int", 0),
						  "UserID"				=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     				=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"AdminCheckSkinImages",$params,false,false,$rs);

		if($response["Code"] == 0)
		{
			//$name = queryDBNextRS($rs);
			while($name = queryDBNextRS($rs))
			{

			//print_r($name[0]);
			//exit;

				$page['Name']['ID'] = $name[0]['ID'];
				$page['Name']['Page'] = $name[0]['Page'];
				$page['Name']['Key'] = $name[0]['Key'];
				$page['Images'] = queryDBNextRS($rs);

				$response["Data"][] = $page;

				//$name = queryDBNextRS($rs);
			}


		}


		break;


	/*--------------------------------------------------------------------------------------------
			RETURN ALL THE CONTENT THAT IS DIFFERENT BETWEEN STAGING AND LIVE
	--------------------------------------------------------------------------------------------*/
	case "comparestagingtolive":

		$params = 	array("SkinID" 				=> array(config("SkinID"), "int", 0),
						  "UserID"				=> array(@$_COOKIE["EditUser"], "int", 0),
						  "AdminSessionID"		=> array(@$_COOKIE["AdminSessionID"], "UI", 0),
						  "IP"     				=> array(getPlayerIP(), "str", 50)
						  );


		$response = queryDB($ajax->db,"Admincomparestagingtolive",$params,false,false,$rs);

		if($response["Code"] == 0)
		{
			$response["Data"] = queryDBNextRS($rs);
		}
		break;




	/*--------------------------------------------------------------------------------------------
		FLUSH THE CACHE
	--------------------------------------------------------------------------------------------*/
	case "flush":

        $flushed = $ajax->flushCache();
        $response = array("Code" => 0, "Flushed" => $flushed);
		break;

	/*--------------------------------------------------------------------------------------------
    SEND UNIT TEST REPORT
	--------------------------------------------------------------------------------------------*/
	case "sendtestreport":

		$subject = @$_REQUEST["s"];
		$content = @$_REQUEST["c"];

		sendMail ("no-reply@spacebarmedia.com", "Mohtadib@spacebarmedia.com,HamishI@spacebarmedia.com,AlvaroV@spacebarmedia.com" , $subject, $content, true);
		$response = array("Code" => 0);

		break;

    /*--------------------------------------------------------------------------------------------
    Create new version of TC's 
    --------------------------------------------------------------------------------------------*/
    case "createNewVersionTCs":

	    $body = ['contentTypeId' => 1, 'skinId' =>  config("SkinID"), "content"  => @$_REQUEST['c']];

	    $res = $ajax->callMicroservice(config("PlayerManagementAPI"), 'contents', [], 1, 'POST', $body);    

	    $response["Code"] = 0;
	    $response["Body"] = $res["body"];
	    
	    break;

    /*--------------------------------------------------------------------------------------------
        Save tc draft without creating a new version
    --------------------------------------------------------------------------------------------*/
    case "updateTPNoVersion":

    	$body = ["id" => @$_REQUEST['i'], "content"  => @$_REQUEST['c']];

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), "contents/update-content", [], 1, 'PUT', $body);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

    /*--------------------------------------------------------------------------------------------

    Show latest version of TC's / PP
    --------------------------------------------------------------------------------------------*/
    case "showLatestVersionsTP":


        $params = [
            'skinId' =>  config("SkinID")
        ];

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), 'contents/latest', $params, 1, 'GET', []);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

    /*--------------------------------------------------------------------------------------------
    Show Pending and Approved version of TC's / PP
    --------------------------------------------------------------------------------------------*/
    case "showAllVersionsTP":


        $params = [
            'skinId' =>  config("SkinID"),
            'status' =>   "APPROVED,PENDING"
        ];

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), 'contents', $params, 1, 'GET', []);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

    /*--------------------------------------------------------------------------------------------
    Show Pending and Approved version of TC's / PP
    --------------------------------------------------------------------------------------------*/
    case "getContentTC":


        $params = [
            'skinId' =>  config("SkinID"),
            'status' =>   "APPROVED,PENDING",
            'contentTypeId' => 1
        ];

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), 'contents', $params, 1, 'GET', []);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

    /*--------------------------------------------------------------------------------------------
        Save new TC's
    --------------------------------------------------------------------------------------------*/
    case "savetcs":

		$body = ['contentTypeId' => 1, 'skinId' =>  config("SkinID"), "content"  => @$_REQUEST['c']];

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), 'contents', [], 1, 'POST', $body);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

  	/*--------------------------------------------------------------------------------------------
        Publish TC's / PP
    --------------------------------------------------------------------------------------------*/
    case "publishtcs":

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), "contents/" . @$_REQUEST['i'], [], 1, 'PUT', []);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

    /*--------------------------------------------------------------------------------------------
        Save new TC's / PP
    --------------------------------------------------------------------------------------------*/
    case "savepp":

		$body = ['contentTypeId' => 2, 'skinId' =>  config("SkinID"), "content"  => @$_REQUEST['c']];

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), 'contents', [], 1, 'POST', $body);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

  	/*--------------------------------------------------------------------------------------------
        Publish TC's / PP
    --------------------------------------------------------------------------------------------*/
    case "publishpp":

        $res = $ajax->callMicroservice(config("PlayerManagementAPI"), "contents/" . @$_REQUEST['i'], [], 1, 'PUT', []);    

        $response["Code"] = 0;
        $response["Body"] = $res["body"];
        
        break;

	default:

		$function = "Function Not Found";
		$params = array();
		break;
}
	/*--------------------------------------------------------------------------------------------
		CLEAR SESSION COOKIES IF SESSION TIMEOUT
	--------------------------------------------------------------------------------------------*/
	if(@$response["Code"] == -1) ClearSession();


	/*--------------------------------------------------------------------------------------------
		RETURNING THE RESPONSE IN JSON FORMAT
	--------------------------------------------------------------------------------------------*/
	echo json_encode($response);


function ClearSession()
{
	$response['Msg'] = 'Session Expired';
	setacookie("EditUser",null,-1,'/');
	setacookie("AdminSessionID",null,-1,'/');
}


?>