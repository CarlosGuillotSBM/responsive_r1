<?php
require 'controller/ajax-request/testajax.php';

//Just to make use of the ajax openDB, as we implemented it abstract
$ajax = new oldDataAjax();
$ajax->initServices();


$item = @$_REQUEST["item"];
$page = @$_REQUEST["page"];
$id = @$_REQUEST["id"];

if(@$_REQUEST["action"] == 'save_seo')
{
	if($item == 'SEO')
	{	// SEO content headers
		$ajax->openDB();
		$params = 	array("SkinID" 		 => array(config("SkinID"), "int", 0),							  
						  "Page" 		 => array($page, "str", 100),
						  "Content"		 => array(@$_REQUEST["content"], "str", 100000)
						  
		);
		$row = queryDB($ajax->db,"AdminSetSEOContent",$params);		

		if($row["Code"] == 0) echo "<script>window.close()</script>";	
		exit;
	}
}


if(@$_REQUEST["action"] == 'save')
{	// normal content headers

	
	$ajax->openDB();
	$params = 	array("SkinID" 		 => array(config("SkinID"), "int", 0),							  
					  "Page" 		 => array($page, "str", 100),
					  "Item" 		 => array($item, "str", 100),
					  "Position" 	 => array($_REQUEST["position"], "int", 0),
					  "DeviceCategoryIDs" => array(implode(',',$_REQUEST["device"]), "str", 100),
					  "PlayerClassIDs"    => array($_REQUEST["pcIDs"], "str", 100),
					  "SubCampaignIDs"    => array($_REQUEST["scIDs"], "str", 100),
					  "Content"		 => array(@$_REQUEST["content"], "str", 100000)
					  
	);
	$row = queryDB($ajax->db,"AdminSetContent",$params);		

	
	//if($row["Code"] == 0) echo "<script>window.close()</script>";	
	exit;
	
}	


$ajax->openDB();



?>
<html>
<head></head>
<body>
<script>
function saveItem(action)
{	
	//document.getElementById("content").editable = false;	
	//document.getElementById("content").readOnly = true;
	
	document.getElementById("action").value = action;
	document.getElementById("frm").submit();
}
</script>

<style>
	BODY
	{
		font-family:arial;
	}
	
	.text
	{
		width:100%
	}

</style>
<form method="post" action="edit.php" id="frm">
	<input type='hidden' name='action' id='action'/>		
	<input type='hidden' name='page' id='page' value="<?php echo $page;?>"/>	
	<input type='hidden' name='item' id='item' value="<?php echo $item;?>"/>

<?php if($item == 'SEO'){

		
		$params = 	array("SkinID" 		 => array(config("SkinID"), "str", 15),							  
						  "Page" 		 => array($page, "str", 15)							 
						  
        );
		$row = queryDB($ajax->db,"AdminGetSEOContent",$params);				

?>
	<table width="100%">
		<tr>			
			<td>SEO: <?php echo $page;?></td>			
			<td align="right">				
				<input type="button" name="save" id="save" value="Save Changes" onclick="saveItem('save_seo');"/>
			</td>
		</tr>
	</table>		
	<textarea id="content" name="content" style="width:100%; height:635px" editable=true><?php echo @$row["Content"];?></textarea>
	<table width="100%">
		<tr>			
			<td>Last Updated: <?php echo @$row["DateUpdated"];?></td>			
		</tr>
	</table>	
	
	
	
<?php } else { 
/*
	
		$params = 	array("SkinID" 		 => array(config("SkinID"), "str", 15),							  
						  "Page" 		 => array($page."_".$item, "str", 15)							 
						  
        );
		$row = queryDB($ajax->db,"AdminGetContent",$params);	
*/
?> 
<script type="text/javascript" src="/_common/js/bower_components/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "link image"
    ]
});

/*
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen"
        
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	*/
</script>

	<table width="100%" >
		<tr>			
			<td>Page:</td>
			<td><b><?php echo $page;?></b></td>			
			<td>Item:</td>
			<td><input type="text" name="item" id="item" value="<?php echo $item;?>" class="text"/></td>			
					
			<td align="right">				
				<input type="button" name="save" id="save" value="Save" onclick="saveItem('save');"/>
				<input type="button" name="saveNew" id="saveNew" value="Save New" onclick="saveItem('new');"/>
			</td>
		</tr>
		<tr>
			<td>Device:</td>			
			<td colspan=2>
				<input type="checkbox" name="device[]" id="deviceWeb" value="1" checked /><label for="deviceWeb">Web</label>
				<input type="checkbox" name="device[]" id="deviceTab"  value="2" checked /><label for="deviceTab">Tablet</label>
				<input type="checkbox" name="device[]" id="devicePhone"  value="3" checked /><label for="devicePhone">Phone</label>
			</td>		
			<td align="right">Position:</td>
			<td>
				<select name="position" id="position">
					<?php 
						for($i=1;$i<51;$i++)
						{
							echo "<option value='".$i."'>".$i."</option>";
						}
					?>	
				</select>				
			</td>				
		</tr>		
		<tr>
			<td>Title:</td>			
			<td colspan=4><input type="text" name="title" id="title" value="" class="text"/></td>			
		</tr>
	</table>
	
    <textarea name="content" style="width:100%; height:480px"></textarea>
	
	<table width="100%" >
	<tr>
			<td>ClassIDs</td>
			<td align="right"><input type="text" name="pcIDs" id="pcIDs" value="" class="text"/></td>					
			<td>SubCampaignIDs</td>
			<td align="right"><input type="text" name="scIDs" id="scIDs" value="" class="text"/></td>			
	</tr>
	</table>
<?php } ?>	
</form>
</body>
</html>