<?php
/*
Interface that provides you with the methods to do HLR checks, listens for SMS delivery receipts and send SMS

*/
require_once("MobileNetworkManager.php");
require_once("mildlettuce/blender-php-api/bulksms/BulkSMS.php");
require_once("mildlettuce/blender-php-api/bulksms/DeliveryReceipt.php");

class MobivateManager implements MobileNetworkManager {
	private $mobivatePassword = "276DagaPoints";
	private $mobivateUsername = "daga";
	private $mobivateReference = "a42JKLm-M11";
	private $mobivateHLRRoute = "mhlrglobal";

	public $smsStatus = array('SENT'=> 3,
										'ACCEPTED'=> 2, 
										'DELIVERED'=> 1,
										'EXPIRED'=> -1,
										'DELETED'=> -2, 
										'UNDELIVERABLE'=> -3,
										'UNKNOWN'=> 0,
										'REJECTED'=> -4, 
										'FAILED_INSUFFICIENT_CREDIT'=> -5,
										'FAILED_ROUTE_INVALID'=> -6,
										'FAILED_LIMITS_EXCEEDED'=> -7
										);

	private $valid = array(	'SENT'=> 2,
										'ACCEPTED'=> 2, 
										'DELIVERED'=> 1,
										'EXPIRED'=> 2,
										'DELETED'=> 2, 
										'UNDELIVERABLE'=> 0,
										'UNKNOWN'=> 2,
										'REJECTED'=> 0, 
										'FAILED_INSUFFICIENT_CREDIT'=> 2,
										'FAILED_ROUTE_INVALID'=> 2,
										'FAILED_LIMITS_EXCEEDED'=> 2,
										'REJECTED_INVALID_RECIPIENT'=> 0
										);			

	private $db;

    function __construct($db)
    {
    	$this->db = $db;
    }



	public function checkHLR ($phoneNumber, $playerId = 0, $ip='0.0.0.0', $message=' from COGS') {
        # Create the DB params
        $params = 	array("PlayerID" 	=> array($playerId, "int", 0),
				            "IP" 		=> array($ip, "int", 0),
				            "MobilePhone" 	=> array($phoneNumber, "str", 15),
				            "Message" 	=> array($message, "str", 50)
				        );

        # Execute the DB proc
        $row = queryDB($this->db,"svrCheckHLR",$params);
        // we check whether we need to validate the number
        if ($row["Code"] == 1) {
        	// if the code is 1 we need to validate it against mobivate

	        # Create client instance
	        $bulksms = new Blender\Client\BulkSMS();
	        # Login to gateway
	        $bulksms->login($this->mobivateUsername, $this->mobivatePassword);
	        # We create the reference
	        $mobivateReference = $phoneNumber;
	        # Send to a single recipient
	        $mobivateResponse = $bulksms->sendSingle("RAD Validator", $phoneNumber, "HLR Validation", $this->mobivateHLRRoute, $mobivateReference);    

        }

	}

	public function updateSMSStatus ($deliveryReceipt) {

		# Create receiver instance 
		$receipt = simplexml_load_string($deliveryReceipt);
		# Get our own sms reference
		$myref = $receipt->clientReference;
		# Get the status of the message
		$status = (string) $receipt->status;
		# Get the mobile phone number		
		$hlr = $receipt->hlr;
		$phoneNumber = filter_var( $hlr->mobile, FILTER_SANITIZE_STRING);

		if (is_numeric($phoneNumber) && strlen($phoneNumber) > 9) {

			# Create the DB params
	        $params = 	array("MobilePhone" 	=> array($phoneNumber, "str", 15),
					            "Status" 		=> array($this->valid[$status], "int", 0)
					        );

	        # Execute the DB proc
	        queryDB($this->db,"svrUpdateHLR",$params);
    	} else {
			$myfile = @fopen(config('Logs')."MobivateManager\\log.txt", "a");
			if($myfile) {
			  $newLine = "\r\n";
			  fwrite($myfile, $newLine . date('M-d-Y h:i:s') . ' ' . $deliveryReceipt);
			  fclose($myfile);		
			}
		} 

	}

	public function singleSms($phoneNumber,$message,$reference,$sender){

	}
}

	
	
?>