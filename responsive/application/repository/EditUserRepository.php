<?php

require 'AbstractRepository.php';
require 'QueryBuilder.php';

class EditUserRepository extends AbstractRepository
{

    public function fetchAll()
    {
        $queryBuilder = QueryBuilder::createQueryBuilder();
        $queryBuilder
            ->select("u.ID, u.Username, u.CanUpload, u.IsAdmin, u.UserGroup")
            ->from("[Responsive].[dbo].[EditUser] u (NOLOCK)")
            ->orderBy("u.ID DESC");

        $this->createSQLFromQueryBuilder($queryBuilder);
        $result = $this->getResult();
        return $result;
    }
}
