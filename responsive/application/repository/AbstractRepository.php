<?php

class AbstractRepository {

    protected $query;
    protected $db;
    protected $ddbbName;

    /**
     * @param $db
     */
    public function __construct($db, $ddbbName)
    {
        $this->db = $db;
        $this->ddbbName = $ddbbName;
    }

    /**
     * Access to the ddbb and get the result
     */
    public function getResult()
    {
        return queryDatabaseWithSQL($this->db, $this->query);
    }

    /**
     * Access to the ddbb and get the result but through a procedure
     *
     * @param $procedureName
     * @param $parameters
     * @param $multirow
     * @param $makeRowCategories
     * @param $resultSet
     * @return array
     */
    public function getProcedureResult($procedureName, $parameters, $multirow, $makeRowCategories, &$resultSet)
    {
        return queryDB($this->db, $procedureName, $parameters, $multirow, $makeRowCategories, $resultSet);
    }

    public function createSQLFromQueryBuilder(QueryBuilder $queryBuilder)
    {
        $this->query = $queryBuilder->getQueryRawSQL();
    }

    /**
     * Set the query in raw for the call
     *
     * @param $sqlQuery
     */
    public function setRawSQL($sqlQuery)
    {
        $this->query = $sqlQuery;
    }

    /**
     * Get the query in raw
     */
    public function getRawSQL()
    {
        return $this->query;
    }

    public function insert($values)
    {
        $values = $this->sanitizeValues($values);
        $this->query = prepareRowForInsertion($this->adaptTableName(), $values);

        return $this->getResult();
    }

    public function updateById($id, $values)
    {
        $values = $this->sanitizeValues(
            $this->preprosValues($values)
        );
        $this->query = prepareRowForUpdate($this->adaptTableName(), $values, array("ID" => $id));
        return $this->getResult();
    }

    public function preprosValues($values)
    {
        unset($values["ID"]);
        unset($values["Id"]);
        unset($values["id"]);

        return $values;
    }

    public function sanitizeValues($values)
    {
        foreach($values as $key => $value){
            if (is_string($value)) {
                $values[$key] = "'".str_replace("'","''",$value)."'";
            }
            if (is_bool($value)) {
                $values[$key] = ($value === true) ? 1 : 0;
            }
        }
        return $values;
    }

    public function adaptTableName()
    {
        $class = str_replace ("Repository", "", get_class($this));
        return "[{$this->ddbbName}].[dbo].[$class]";
    }

}
