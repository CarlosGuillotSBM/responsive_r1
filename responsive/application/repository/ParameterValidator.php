<?php

include_once "exceptions/ParameterValidatorException.php";

class ParameterValidator {

    static public function isInteger($parameter)
    {
        if (is_int($parameter)){
            return true;
        } else {
            throw new ParameterValidatorException("Invalid Integer Parameter");
        }
    }

    static public function isNumeric($parameter)
    {
        if (is_numeric($parameter)){
            return true;
        } else {
            throw new ParameterValidatorException("Invalid Numeric Parameter");
        }
    }

    static public function filterInputParameterInt($parameter)
    {
        if (filter_var($parameter, FILTER_VALIDATE_INT) !== false) {
            return true;
        } else {
            throw new ParameterValidatorException("Invalid Numeric Input Parameter");
        }
    }
}
