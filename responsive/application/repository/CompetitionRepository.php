<?php

require 'AbstractRepository.php';
require 'QueryBuilder.php';
include_once "repository/ParameterValidator.php";

class CompetitionRepository extends AbstractRepository
{

public function fetchAllWithPoints($skinId, $competitionID)
{
ParameterValidator::isInteger($skinId);
ParameterValidator::isInteger($competitionID);
$queryBuilder = QueryBuilder::createQueryBuilder();
$queryBuilder
->select("cp.CompetitionID")
->addSelect("cp.PlayerID")
->addSelect("cp.Points")
->addSelect("cp.ScorePoints")
->addSelect("c.BoxStyle as PromoName")
->addSelect("p.Username")
->addSelect("RANK() OVER (PARTITION BY CompetitionID ORDER BY cp.Points DESC) as Position")
->from("[Competition].[dbo].[CompPlayer] cp (NOLOCK)")
->join("[Competition].[dbo].[Competition] c (NOLOCK)", "c.ID = cp.CompetitionID")
->join("[Main].[dbo].[Player] p (NOLOCK)", "cp.PlayerID = p.ID")
->where("c.CurrentRound = cp.Round")
->andWhere("c.StatusID = 1")
->andWhere("c.SkinID = {$skinId}")
->andWhere("c.ID = {$competitionID}")
->orderBy("CompetitionID DESC, Points DESC");

$this->createSQLFromQueryBuilder($queryBuilder);
$result = $this->getResult();
return $result;
}
}