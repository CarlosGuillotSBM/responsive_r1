<?php

class QueryBuilder {

    private $select;
    private $from;
    private $where;
    private $order;

    public function getQueryRawSQL()
    {
        return
            ltrim($this->select, ",").$this->from.$this->where.$this->order.";";
    }

    public function select($select)
    {
        if (!isset($this->select)) {
            $this->select .= "SELECT " . $select;
        }
        return $this;
    }

    public function addSelect($select)
    {
        $this->select .= ", " . $select;
        return $this;
    }

    public function from($from)
    {
        if (!isset($this->from)) {
            $this->from = " FROM " . $from;
        }
        return $this;
    }

    public function join($join, $on)
    {
        $this->from .= " JOIN " . $join . " ON " . $on;
        return $this;
    }

    public function where ($where)
    {
        if (!isset($this->where)) {
            $this->where = " WHERE " . $where;
        }
        return $this;
    }

    public function andWhere ($where)
    {
        $this->where .= " AND " . $where;
        return $this;
    }

    public function orderBY ($orderBy)
    {
        if (!isset($this->order)) {
            $this->order = " ORDER BY " . $orderBy;
        }
        return $this;
    }

    public static function createQueryBuilder()
    {
        return new QueryBuilder();
    }
}
