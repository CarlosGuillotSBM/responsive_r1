<?php

class ProgressiveModel
{

    function __construct($db) 
	{
		$this->db = $db;		
    }

	
	public function getProgressives()
    {
		  $params = 	array("SkinID" => array(config("SkinID"), "int", 0)							
          );
		  
		  return queryDB($this->db,"WebGetProgressives",$params,true);	
    }	

}
