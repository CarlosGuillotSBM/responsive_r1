<?php

class WinnersModel
{

    function __construct($db) 
	{
		$this->db = $db;		
    }

	
	public function getWinners($type)
    {
		  $params = 	array("SkinID"  => array(config("SkinID"), "int", 0),
							  "WinType" => array($type, "str", 50),
			  				  "Device" => array(config("Device"), "int", 0),
          );
		  
		  return queryDB($this->db,"WebGetWinners",$params,true);
    }

}
