<?php

class Skin {

    const SpinAndWin = 1;
    const KittyBingo = 2;
    const LuckyPants = 3;
    const DirectSlot = 4;
    const MagicalVegas = 5;
    const BingoExtra = 6;
    const LuckyVIP = 8;
    const GiveBackBingo = 9;
    const RegalWins = 10;
    const KingJack = 11;
    const Aspers = 12;
}
