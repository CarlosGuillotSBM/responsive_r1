<?php
/*

	THE INDEX MODEL GETS INFORMATION ABOUT COUNTRY, CURRENCY AND LANGUAGE BASED ON THE PLAYERS IP

*/
class IndexModel
{

    function __construct($db) 
	{   	        
        $this->db = $db;			
    }


	
    public function getIndex()
    {
		$ip = getPlayerIP(); 
		$ipID= (substr($ip, 0, 3) > 127) ? ((ip2long($ip) & 0x7FFFFFFF) + 0x80000000) : ip2long($ip);
		
		$params = 	array("SkinID" => array(config("SkinID"), "int", 0),							  							 
						  "CountryIPID"     => array($ipID, "int", 0)
		);

		return queryDB($this->db,"WebIndex",$params,false);	
    }
		
	
}
