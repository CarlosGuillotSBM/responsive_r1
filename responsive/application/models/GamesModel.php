<?php

class GamesModel
{
	public  $rs;

    function __construct($db)
	{
        $this->db = $db;
    }


    /**
     * Get all games from database
     */
    public function getGames($category)
    {
		  $preview = (Env::inState([Env::live, Env::dev])) ? 0 : 1;


		  $params = 	array("SkinID" => array(config("SkinID"), "int", 0),
							  "Category"  => array($category, "str", 50),
							  "Device" => array(config("Device"), "int", 0),
							  "IP"     => array(getPlayerIP(), "str", 50),
							  "Preview"   => array($preview, "int", 50),
                               "AccessLevel" => array( config( 'AccessLevel'), "int", 0)
          );

		  $ret = queryDB($this->db,"WebGetGamesWithCategories",$params,false,false,$rs);
		  $this->rs = $rs;
		  return $ret;
    }

    /**
     * Get all games from database within a category belonging to a given provider
     */
    public function getGamesByCategoryAndProvider($category,$provider)
    {
		  $preview = (Env::inState([Env::live, Env::dev])) ? 0 : 1;
	
	
		  $params = 	array("SkinID" => array(config("SkinID"), "int", 0),	
							  "Category"  => array($category, "str", 50),	
							  "Provider"  => array($provider, "str", 50),								  							  					 
							  "Device" => array(config("Device"), "int", 0),							  
							  "IP"     => array(getPlayerIP(), "str", 50),
							  "Preview"   => array($preview, "int", 50),
                              "AccessLevel" => array( config( 'AccessLevel') , "int", 0)
          );
		  		  		  
		  $ret = queryDB($this->db,"WebGetGamesWithCategoriesAndProviders",$params,false,false,$rs);			  
		  $this->rs = $rs;			    
		  return $ret; 
    }

    /**
     * Get all the details for a game
     */
    public function getGameDetail($detailURL)
    {
		$preview = (Env::inState([Env::live, Env::dev])) ? 0 : 1;

		  $params = 	array("SkinID" => array(config("SkinID"), "int", 0),
							  "DetailURL" => array($detailURL, "str", 100),
							  "IP"     => array(getPlayerIP(), "str", 50),
							  "Preview"   => array($preview, "int", 50)
          );

		  return queryDB($this->db,"WebGetGameDetails",$params);
    }


}
