<?php

class BingoModel
{
	public $rs;

    function __construct($db) 
	{
		$this->db = $db;		
    }

	/*
		NOTE: 
		Are we using this? if we remove we still see the bingo rooms on /bingo/		
	*/
	
	
    /**
     * Get all games from database
     */
    public function getGames($category)
    {
		  $live = (Env::inState([Env::live, Env::dev])) ? 1 : 0;
	
	
		  $params = 	array("SkinID" => array(config("SkinID"), "int", 0),	
							  "Category"  => array($category, "str", 50),							  							  					 
							  "Device" => array(config("Device"), "int", 0),							  
							  "IP"     => array(getPlayerIP(), "str", 50),
							  "Live"   => array($live, "int", 50),
                              "AccessLevel" => array( config( 'AccessLevel'), "int", 0)
          );
		  		  		  
		  $ret = queryDB($this->db,"WebGetGamesWithCategories",$params,false,false,$rs);			  
		  $this->rs = $rs;			    
		  return $ret; 
    }
	
    /**
     * Get all the bingo rooms from database
     */
    public function getBingoRooms($key)
    {
	
		  $params = 	array("SkinID" => array(config("SkinID"), "int", 0),								
							  "Key"    => array($key, "str", 50),							  
							  "Device" => array(config("Device"), "int", 0),							  
							  "IP"     => array(getPlayerIP(), "str", 50)
          );
		  
		  return queryDB($this->db,"WebGetBingoRooms",$params);	
	
    }

}
