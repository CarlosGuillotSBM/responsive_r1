<?php

class ContentModel
{

    /**
     * Every model needs a database connection, passed to the model
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Get the Web Content performing WebGetContent proc
     *
     * @param $page
     * @param string $subkey
     * @param int $includeExpiring
     *
     * @return array
     */
    public function getWebContent($page, $subkey = '',$includeExpiring = 0)
    {
        $pc = (isset($_COOKIE["PlayerClassID"])) ? $_COOKIE["PlayerClassID"] : 0;
        $offer = (isset($_COOKIE["OfferID"])) ? $_COOKIE["OfferID"] : 0;

        $params = 	array(
            "SkinID"            => 			array(config("SkinID"), "int", 0),
            "Page"              => 			array($page, "str", 100),	// this is a general page name like promotions
            "DetailsURL"        =>		    array($subkey, "str", 100),	// this is a parent key of a game or other content
            "Device"            => 			array(config("Device"), "int", 0),
            "PlayerClassID"     => 	        array($pc, "int", 0),
            "OfferID"           => 			array($offer, "int", 0),
            "IP"                => 			array(getPlayerIP(), "str", 50),
            "Preview"           => 			array(config("Edit"), "int", 0),
            "IncludeExpiring"   => 	        array($includeExpiring, "int", 0)
        );

        return queryDB($this->db,"WebGetContent",$params,true,true,$rs);
    }

    /**
     * Get the contents from the ddbb
     *
     * @param $page
     * @param string $subkey
     * @param int $includeExpiring
     *
     * @return array
     */
    public function getContent($page, $subkey = '', $includeExpiring = 0)
    {
        $allContent = $this->getWebContent($page, $subkey, $includeExpiring);
        if ( $allContent ) {
             foreach($allContent as $value) {
                  $rows[$value["Key"]][] = $value;
             }
             if(!isset($rows)) $rows = array();

             return $rows;
        }
    }

    /**
     * Get the contents filtered by some key => value
     *
     * @param $page
     * @param $key
     * @param $value
     * @param string $subkey
     * @param int $includeExpiring
     *
     * @return array
     */
    public function getContentByFilter($page, $key, $value, $subkey = '',$includeExpiring = 0)
    {
        return $this->filterContentByKey($this->getWebContent($page, $subkey, $includeExpiring),$key,$value);
    }

    /**
     * Filters content based on a key and value. Optionally it can allow accepting empty values as valid. False by default
     *
     * @param $allContent
     * @param $key
     * @param $value
     * @param bool $includeEmptyValues
     *
     * @return array
     */
    public function filterContentByKey (array $allContent, $key, $value, $includeEmptyValues = false) {
        $filteredContents = array();
        foreach ($allContent as $content) {
            if (isset($content[$key]) && ($content[$key] === $value || ($includeEmptyValues && empty($content[$key])))) {
                $filteredContents[] = $content;
            }
        }
        return $filteredContents;
    }


    /**
     * Filters content based on a key and value. Optionally it can allow accepting empty values as valid. False by default
     *
     * @param $allContent
     * @param $key
     * @param $value
     * @param bool $includeEmptyValues
     *
     * @return array
     */
    public function filterOutContentByKeyContains (array $allContent, $key, $value, $includeEmptyValues = false) {
        //var_dump($allContent);
        $filteredContents = array();
        foreach ($allContent as $content) {
            if (!(isset($content[$key]) && (strpos($content[$key], $value) !== false))) {
                $filteredContents[] = $content;
            }
        }
        return $filteredContents;
    }

    /**
     * Filter the promotions by a player ID
     *
     * @param array $promotions
     * @param $playerId
     * @return array
     */
    public function filterOutPromotionsByPlayerId($promotions, $playerId) {
        $filteredContents = array();
        if (!is_null($promotions)){
            foreach ($promotions as $promotion) {
                $playersIdShowOnly = $this->getPlayersIdsShowOnly($promotion["Text7"]);
                if (empty($playersIdShowOnly) || in_array($playerId, $playersIdShowOnly)) {
                    $filteredContents[] = $promotion;
                }
            }
        }
        return $filteredContents;
    }

    /**
     * Check if the promotion is accessible from outside
     *
     * @param array $promotion
     * @param $playerId
     * @return bool
     */
    public function isPromotionAccessibleByPlayerId(array $promotion, $playerId) {
        $playersIdShowOnly = $this->getPlayersIdsShowOnly($promotion["Text7"]);
        if (empty($playersIdShowOnly) || in_array($playerId, $playersIdShowOnly)) {
            return true;
        }
        return false;
    }

    private function getPlayersIdsShowOnly($playersIDs) {
        if (empty($playersIDs)) {
            return array();
        } else {
            return explode(" ", $playersIDs);
        }
    }

    /**
     * Filters content based on a key and value. Optionally it can allow accepting empty values as valid. False by default
     *
     * @param $allContent
     * @param $key
     * @param $value
     * @param bool $includeEmptyValues
     *
     * @return array
     */
    public function filterContentByKeyContains (array $allContent, $key, $value, $includeEmptyValues = false) {
        //var_dump($allContent);
        $filteredContents = array();
        foreach ($allContent as $content) {
            if (isset($content[$key]) && (strpos($content[$key], $value) !== false)) {
                //echo $content[$key];
                $filteredContents[] = $content;
            }
        }
        return $filteredContents;
    }

    /**
     * Get the contents for the community section
     *
     * @return array
     */
    public function getContentForCommunity()
    {
        $community = $this->getContentByFilter('community', 'Text14', 'Y');
        // Fill the array with fallback values
        if (count($community) < 4) {
            $allCommunityContents = $this->getWebContent('community');
            $fallbackElement = array_filter($allCommunityContents, function($a) use ($community) {
                return !in_array($a, $community);
            });
            $community = array_merge($community, $fallbackElement);
        }
        return $community;
    }

    public function getArchiveContentDates($page,$date)
    {
        $pc = (isset($_COOKIE["PlayerClassID"])) ? $_COOKIE["PlayerClassID"] : 0;
        $offer = (isset($_COOKIE["OfferID"])) ? $_COOKIE["OfferID"] : 0;

        $params = 	array(
            "SkinID"        =>          array(config("SkinID"), "int", 0),
            "Page"          =>          array($page, "str", 100),	// this is a general page name like promotions
            "Date"          =>          array($date, "str", 100),	// this is a parent key of a game or other content
            "Device"        =>          array(config("Device"), "int", 0),
            "PlayerClassID" =>          array($pc, "int", 0),
            "OfferID"       =>          array($offer, "int", 0),
            "IP"            =>          array(getPlayerIP(), "str", 50),
            "Preview"       =>          array(config("Edit"), "int", 0)
        );

        $allContent = queryDB($this->db,"WebGetArchiveContentDates",$params,true,true,$rs);

        $currentYear = 0;
        $year = array();
        foreach($allContent as $value)
        {
            $dates[$value['YearString']][] = $value;
        }

        if(!isset($dates)) $dates = array();

        return $dates;
    }

    public function getArchiveContentList($page,$date)
    {
        $pc = (isset($_COOKIE["PlayerClassID"])) ? $_COOKIE["PlayerClassID"] : 0;
        $offer = (isset($_COOKIE["OfferID"])) ? $_COOKIE["OfferID"] : 0;

        $params = 	array(
            "SkinID"        => 			array(config("SkinID"), "int", 0),
            "Page"          => 			array($page, "str", 100),	// this is a general page name like promotions
            "Date"          =>			array($date, "str", 100),	// this is a parent key of a game or other content
            "Device"        => 			array(config("Device"), "int", 0),
            "PlayerClassID" => 	        array($pc, "int", 0),
            "OfferID"       => 			array($offer, "int", 0),
            "IP"            => 			array(getPlayerIP(), "str", 50),
            "Preview"       => 			array(config("Edit"), "int", 0)
        );

        $allContent = queryDB($this->db,"WebGetArchiveContentList",$params,true,true,$rs);
        foreach($allContent as $value)
        {
            $rows[$value["Key"]][] = $value;
        }
        if(!isset($rows)) $rows = array();

        return $rows;
    }

    public function getContentDetail($page,$category)
    {
        $params = 	array(
            "SkinID" 	    =>          array(config("SkinID"), "int", 0),
            "Page" 	        =>          array($page, "str", 100),
            "DetailsURL"	=>          array($category, "str", 100),	// this is a specific promotion like summer-promo
            "Device"        =>          array(config("Device"), "int", 0),
            "IP"            =>          array(getPlayerIP(), "str", 50),
            "Preview"       => 	        array(config("Edit"), "int", 0)
        );

        return queryDB($this->db,"WebGetContentDetail",$params);
    }
}
