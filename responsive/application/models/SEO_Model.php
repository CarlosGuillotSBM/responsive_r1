<?php

class SEO_Model
{

    function __construct($db,$controller) 
	{
		$this->controller = $controller;
        $this->db = $db;	
    }

    /**
     * Get content from database
     */
    public function getContent($page)
    {
		$params = 	array("SkinID" => 	array(config("SkinID"), "int", 0),
						  "Page"   => 	array($page, "str", 500)
		);

		$rows = queryDB($this->db,"WebGetSEOContent",$params); 			
		
		return @$rows["Content"];
	 
    }
		
}
