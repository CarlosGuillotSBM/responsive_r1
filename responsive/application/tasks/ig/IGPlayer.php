<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once "SoapClient.php";
require_once "RegisterCustomer.php";
require_once "RegisterCustomerHelpers.php";

/*
  New player for IG
*/

class IGPlayer
{
    private $soapClient;
    private $player; 

    function __construct () {
        $this->soapClient = new radSoapClient(config("IGapi"));
    }

    /*
        Checks if player is in a land based casino area
    */
    private function isInCasinoArea () {

        $IsInCasinoArea = new IsInCasinoArea($this->player["Postcode"]);

        return $IsInCasinoArea->check();
    }

    /*
        Get customer from IG system
        Returns IG customer if found a match otherwise returns false 
    */
    private function getIGCustomer() {

        $isAnIGCustomer = new CheckIGCustomer($this->player);

        $igCustomerData = $isAnIGCustomer->check();

        if (!$igCustomerData) return false;

        return $igCustomerData;
    }

    /*
        Updates the Main..Player.AspersCustomerNumber
    */
    private function updateAspersCustomerNumber ($customerNumber) {
        global $db;
        $params = array( "ID"=> array($this->player["ID"], "int", 0),
                     "AspersCustomerNumber" => array($customerNumber, "int", 0)
                );

        $row = queryDB($db,"DataSetAspersCustomerNumber", $params);
        
        // write log
        if($row["Code"] == 0 && $customerNumber != 0) {
            WriteNewLog(config("IGLog"),"Player " . $this->player['ID'] . " updated with IG Number " . $customerNumber);
        }
    }

	/*
        Resets the Aspers Customer number setting it to 0
    */
	public function resetAspersCustomerNumber ($playerId) {
        global $db;
        $params = array( "ID"=> array($playerId, "int", 0),
                     "AspersCustomerNumber" => array(0, "int", 0)
                );

        $row = queryDB($db,"DataSetAspersCustomerNumber", $params);
        
        WriteNewLog(config("IGLog"),"Reset AspersCustomerNumber on player with ID " . $playerId);
	}
	
    private function isValidPlayer ($player) {

        return isset($player["ID"]) && 
                isset($player["Postcode"]) &&
                isset($player["DOB"]) &&
                isset($player["Firstname"]) &&
                isset($player["Lastname"]) &&
                isset($player["Title"]) &&
                isset($player["Gender"]);
    }

    /*
        Creates a player on IG system
        Needs to check if player is on a land based casino area
        Needs to check if player is already registered on IG
        If already registered we need to get the Customer Number and update our records
        If doesn't exist on IG we need to create it and then get Customer Number and update our records 
    */
    public function createPlayerOnIG ($player) {

		if (!isset($player) || !$this->isValidPlayer($player)) throw new Exception("Player is not valid", 1);

        $number = 0;        
        $this->player = $player;

        // is in casino area?
        if ($this->isInCasinoArea()) {

            // already exists on IG?

            $customer = $this->getIGCustomer();

            if (!$customer) {

                // create user on IG

                $myRequest = new IgRegisterCustomerRequest($this->player);
                $igResponse = $this->soapClient->doRequest("RegisterCustomer", $myRequest->request);
                $response = $myRequest->outputResponse($igResponse);

                $number = $response["CustomerNumber"];
				WriteNewLog(config("IGLog"),"Player " . $player["ID"] . " was created on IG with number " . $number);

            } else {

                $number = isset($customer->Number) ? $customer->Number : 0;
				WriteNewLog(config("IGLog"),"Player " . $player["ID"] . " was found on IG with number " . $number);
            }

            // update player record on our database
            $this->updateAspersCustomerNumber($number);

        } else {
			
			WriteNewLog(config("IGLog"),"Player " . $player["ID"] . " is not on a Casino location");
			$this->resetAspersCustomerNumber($player["ID"]);
		}

    }

    public function processPlayer ($player) {
        $this->createPlayerOnIG($player);
    }
}