<?php

class EmailLog
{
    public static function sendLog ($path, $filename) {
		
		$file = $path . "\\" . $filename;
		
		$mailto = 'nmoliveira@gmail.com';
		$subject = 'Aspers bulk registration log';
		$message = 'Please find attached to this email the log of all the players processed today.';

		$content = file_get_contents($file);
		$content = chunk_split(base64_encode($content));

		// a random hash will be necessary to send mixed content
		$separator = md5(time());

		// carriage return type (RFC)
		$eol = "\r\n";

		// main header (multipart mandatory)
		$headers = join( $eol,[
		    'From: noreply <noreply@aspers.com>',
		    'MIME-Version: 1.0' ,
		    'Content-Type: multipart/mixed; boundary="' . $separator . '"',
		    'Content-Transfer-Encoding: 7bit',
		    'This is a MIME encoded message.'
        ]). $eol;

		// message
		$body = join( $eol, [
		    '--' . $separator,
		    'Content-Type: text/plain; charset="iso-8859-1"',
		    'Content-Transfer-Encoding: 8bit',
		    $message,

		// attachment
            '--' . $separator,
		    'Content-Type: application/octet-stream; name="' . $filename . '"',
		    'Content-Transfer-Encoding: base64',
		    'Content-Disposition: attachment',
		    $content,
		    '--' . $separator . '--'
        ]);

		//SEND Mail
		mail($mailto, $subject, $body, $headers);

	}
}