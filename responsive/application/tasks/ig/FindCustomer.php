<?php

/*
Formats the request to be sent
Validates input parameters

*/
class IgFindCustomerRequest
{


     /*
     Properties
     */
     // private $_customerNumber;
     public $request = [];
     public $valid = true;
     public $error;

     /*
     Constructor
     */
     function __construct( $player ) {

        $computer = config("IGComputerName");

        if ($this->requestIsValid($player)) {
            
            // from DB this comes as a datetime and from the registration will come as a string
            // as a string, this code will fail
            $playerDOB = date_format($player['DOB'], 'Y-m-d');
            
            // work around if the DOB was a string and the date_format didn't work
			if (!$playerDOB) $playerDOB = $player['DOB'];
		
            //test using DOB, First name and Lastname and Postcode
            $this->request[] = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="http://www.IntelligentGaming.co.uk" xmlns:ig="IG.Cms.Gateway.DataContracts">
                                <soapenv:Header/>
                                    <soapenv:Body>
                                            <int:FindCustomer>
                                                <int:request>
                                                    <ig:ComputerName>'.$computer.'</ig:ComputerName>
                                                    <ig:DateOfBirth>'.$playerDOB.'T00:00:00</ig:DateOfBirth>
                                                    <ig:Forename>'.$player['Firstname'].'</ig:Forename>
                                                    <ig:MaxRecords>1</ig:MaxRecords>
                                                    <ig:Surname>'.$player['Lastname'].'</ig:Surname>
                                                    <ig:VisitedToday>false</ig:VisitedToday>
                                                </int:request>
                                            </int:FindCustomer>
                                    </soapenv:Body>
                                </soapenv:Envelope>';
                 
        }

         if ($this->requestIsValid($player, 1)) {
			 
			 $playerDOB = date_format($player['DOB'], 'Y-m-d');

             //test using DOB, Lastname and Postcode

             $this->request[] = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="http://www.IntelligentGaming.co.uk" xmlns:ig="IG.Cms.Gateway.DataContracts">
                                <soapenv:Header/>
                                    <soapenv:Body>
                                        <int:FindCustomer>
                                            <int:request>
                                                    <ig:ComputerName>'.$computer.'</ig:ComputerName>
                                                    <ig:DateOfBirth>'.$playerDOB.'T00:00:00</ig:DateOfBirth>
                                                    <ig:MaxRecords>1</ig:MaxRecords>
                                                    <ig:Postcode>'.$player['Postcode'].'</ig:Postcode>
                                                    <ig:Surname>'.$player['Lastname'].'</ig:Surname>
                                                    <ig:VisitedToday>false</ig:VisitedToday>
                                            </int:request>
                                        </int:FindCustomer>
                                    </soapenv:Body>
                                </soapenv:Envelope>';

         }
         
         if (!$this->request) {
               $this->valid = false;
               $this->error = array(
                    "Code" => 1,
                    "Msg"   => "Request Failed",
                    "Error" => "Invalid parameters"
               );

          }
     }

     /*
     Functions
     */
     /*
     Validates the request parameters
     */
     function requestIsValid($player, $mode = 0) {
        $result = false;
        if ( config("IGComputerName")) {
            $result = $mode ?
                isset($player['DOB']) && $player['Postcode'] && $player['Lastname']:
                isset($player['DOB']) && $player['Firstname'] && $player['Lastname'];

        }
        return $result;
     }
     /*
     Returns the response from IG
     */
     function outputResponse($response) {
         
          // request was successful
          if ($response->FindCustomerResult->Success) {
               // If response have Customer
               if ( isset($response->FindCustomerResult->Customers->Customer) ) {
                    return $response->FindCustomerResult->Customers->Customer;
               }
               return false;
          }
          return array(
               "Code" => 1,
               "Msg"   => "Request Failed",
               "Error" => "IG returned error"
          );
     }
}
