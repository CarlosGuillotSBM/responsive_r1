<?php

/*
Formats the request to be sent
Validates input parameters

*/
class IgUpdateCustomerRequest
{

     /*
     Properties
     */
    
     public $request;
     public $valid = true;
     public $error;

     /*
     Constructor
     */
     function __construct( $player ) {
		
          if ($this->requestIsValid($player)) {

               $this->request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="http://www.IntelligentGaming.co.uk" xmlns:ig="IG.Cms.Gateway.DataContracts">
                                     <soapenv:Header/>
                                        <soapenv:Body>
                                             <int:UpdateCustomer>
                                                  <int:request>
                                                       <ig:ComputerName>'.config("IGComputerName").'</ig:ComputerName>
                                                       <ig:Customer>
                                                            <ig:IsEmailContactAllowed>'.$player['WantsEmail'].'</ig:IsEmailContactAllowed>
                                                            <ig:IsSMSContactAllowed>'.$player['SendSMS'].'</ig:IsSMSContactAllowed>
                                                       </ig:Customer>
                                                       <ig:CustomerNumber>'.$player['AspersCustomerNumber'].'</ig:CustomerNumber>
                                                  </int:request>
                                        </int:UpdateCustomer>
                                        </soapenv:Body>
                                   </soapenv:Envelope>';

          } else {

               $this->valid = false;
               $this->error = array(
                    "Code" => 1,
                    "Msg"   => "Request Failed",
                    "Error" => "Invalid parameters"
               );

          }
     }

     /*
     Functions
     */
     /*
     Validates the request parameters
     */
     function requestIsValid($player) {
          return config("IGComputerName") !== null &&  isset($player['WantsEmail']) && $player['SendSMS'];
     }
     /*
     Returns the response from IG
     */
     function outputResponse($response) {
		 
		  // request was successful
          if ($response->UpdateCustomerResult->Success) {
            
			// IG returns an array with the properties that were saved but for now it's fine to send just true as it was successful
			return true;
          }
          return array(
               "Code" => 1,
               "Msg"   => "Request Failed",
               "Error" => "IG returned error"
          );
     }
}