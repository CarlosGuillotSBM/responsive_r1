<?php

    try {
        
        $config = array(
            "IGComputerName" => "STRIDECONNECTION",
            "IGapi" => "https://bravo.aspzone.co.uk:51331/Live/OnlineGateway/CustomerManagementService.svc?singleWsdl"
        );
        
        include_once 'RegisterCustomerHelpers.php';

        $player = array (
            'DOB' => isset($_REQUEST["dob"]) ? $_REQUEST["dob"] : '',
            'Firstname' => isset($_REQUEST["fn"]) ? $_REQUEST["fn"] : '',
            'Lastname' => isset($_REQUEST["ln"]) ? $_REQUEST["ln"] : '',
            'Postcode' => isset($_REQUEST["pst"]) ? $_REQUEST["pst"] : ''
        );

        $result = new CheckIGCustomer($player);

        $igResponse = $result->check();

        $excluded = (is_object($igResponse) && (isset($igResponse->Status)) && ($igResponse->Status === 'SelfExcluded')) ?
            1 : 0;

        echo  $excluded;

    
    } catch (Exception $e) {
        // do nothing - just ignore
    }