<?php

/*
  Formats the request to be sent
  Validates input parameters

*/

class IgGetCustomerRequest
{

    /*
      Properties
    */
    private $_customerNumber;
    public $request;
    public $valid = true;
    public $error;

    /*
      Constructor
    */
    function __construct()
    {

        $this->_customerNumber = @$_REQUEST["n"];

        if ($this->requestIsValid()) {
            $computer = config("IGComputerName");


            $this->request = <<<EOF
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="http://www.IntelligentGaming.co.uk" xmlns:ig="IG.Cms.Gateway.DataContracts">
    <soapenv:Header/>
    <soapenv:Body>
        <int:GetCustomer>
           <!--Optional:-->
           <int:request>
              <!--Optional:-->
              <ig:ComputerName>{$computer}</ig:ComputerName>
              <ig:CustomerNumber>{$this->_customerNumber}</ig:CustomerNumber>
           </int:request>
        </int:GetCustomer>
    </soapenv:Body>
    </soapenv:Envelope>
EOF;

      } else {

        $this->valid = false;
        $this->error = array(
          "Code" => 1,
          "Msg"   => "Request Failed",
          "Error" => "Invalid parameters"
        );

      }
    }

    /*
      Functions
    */


    /*
      Validates the request parameters
    */
    function requestIsValid() {

      return isset($this->_customerNumber);
    }

    /*
      Returns the response from IG
    */
    function outputResponse($response) {

      // request was successful
      if ($response->GetCustomerResult->Success) {

        return array(
            "Code" => 0,
            "Customer" => $response->GetCustomerResult->Customer
          );

      }

      // Request has failed
      return array(
          "Code" => 1,
          "Msg"   => "Request Failed",
          "Error" => "IG returned error"
        );
    }
  }
