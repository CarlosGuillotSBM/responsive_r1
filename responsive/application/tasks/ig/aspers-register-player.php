<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	$glo["Computer"] = strtolower($_SERVER["COMPUTERNAME"]);
	
    global $config;
	
	set_include_path("D:/WEB/agsSkins/Responsive/application/");
		
	require_once("D:/WEB/inc/db7_lite.php");
	require_once "D:/WEB/agsSkins/Responsive/application/libs/utils.php";
	require_once "D:/WEB/agsSkins/Responsive/application/tasks/ig/SoapClient.php";
	require_once "D:/WEB/agsSkins/Responsive/application/tasks/ig/RegisterCustomer.php";
	require_once "D:/WEB/agsSkins/Responsive/application/tasks/ig/RegisterCustomerHelpers.php";
	require_once "D:/WEB/agsSkins/Responsive/application/tasks/ig/IGPlayer.php";
	require_once "D:/WEB/agsSkins/Responsive/application/tasks/ig/EmailLog.php";
	
    $config = array(
        "SkinID" => 12,
        "Env" => Env::dev,
        "Memcache" => false,
        "RealDevice" => 1,
        "IGapi" => "https://bravo.aspzone.co.uk:51331/Live/OnlineGateway/CustomerManagementService.svc?singleWsdl",
        "IGComputerName" => "STRIDECONNECTION",
        "IGMembershipTypeId" => "22",
        "IGLog" => 'L:\agsLogs\agsSkins\aspers.com\aspers-register-customers'
    );

	$dbErr = $SdbParams = "";
	$dbSproc = "DataGetAspersPlayersNotRegisteredOnIG";
	$dbParams = array();

	$debug = false;

	global $db;
	global $rs;
	global $players;

	$db = "";

	if(dbInit("", "Responsive", __FILE__."[".__LINE__."]", 1) == false) {

		WriteNewLog(config("IGLog"),"*** Cannot init db ***");

		$subject = "Aspers bulk registration database connection failed";
		$message = "Could not connect to the database";
		
		$from = "donotreply@aspers.com";
		$to = "Mohtadib@spacebarmedia.com, OC@daubalderney.com, samuelc@spacebarmedia.com, QA@spacebarmedia.com";
		$headers ="Content-type: text/plain; charset=iso-8859-1\r\n";	
		$headers.="From: $from\r\n";	
		mail($to,$subject,$message,$headers);

		die("cannot init db");

	} else {
		$rs = dbExecSproc($dbSproc,$dbParams);

		if ($rs === false) {
			
			WriteNewLog(config("IGLog"),"*** Cannot exec sproc ***");

			$subject = "Aspers bulk registration SP execution failed";
			$message = "Could not exec the sproc";
			
			$from = "donotreply@aspers.com";
			$to = "Mohtadib@spacebarmedia.com, OC@daubalderney.com, samuelc@spacebarmedia.com, QA@spacebarmedia.com";
			$headers ="Content-type: text/plain; charset=iso-8859-1\r\n";	
			$headers.="From: $from\r\n";	
			mail($to,$subject,$message,$headers);

			die("cannot exec sproc");

		}
	}

    // create custom soap client
    $client = new radSoapClient(config("IGapi"));
	
	$igPlayer = null;

	while($player = sqlsrv_fetch_array($rs)) {
		
		if (!isset($player) || $player == null) {
		  WriteNewLog(config("IGLog"),"*** No new players ***");
		}
	
		try {

			if (!isset($igPlayer)) $igPlayer = new IGPlayer();
		
			// register only if allows 3rd party comms?
			$thirdPartyConsent = ($player["WantsThirdPartyEmail"] == 1) || ($player["WantsThirdPartyPhoneCalls"] == 1) || ($player["WantsThirdPartySMS"] == 1);
			
			if ($thirdPartyConsent) {
			
				// set title -  we dont have that info anymore only the gender
				if ($player["Gender"] === "Male") {
					$player["Title"] = "Mr";
				} else if ($player["Gender"] === "Female") {
					$player["Title"] = "Ms";
				} else {
					$player["Title"] = "Other";
				}

				$igPlayer->processPlayer($player);

			} else {
				// update aspers number as 0
				WriteNewLog(config("IGLog"),"Player " . $player["ID"] . " does not consent 3rd party comms");
				$igPlayer->resetAspersCustomerNumber($player["ID"]);
			}
		}
		catch (Exception $e)
		{
		  WriteNewLog(config("IGLog"), $e);
		}
	}
	
	if (!$debug) {
		
		EmailLog::sendLog(config("IGLog"), date("Ymd").".txt");
	}