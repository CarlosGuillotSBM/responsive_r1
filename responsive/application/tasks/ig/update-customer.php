<?php

    try {

		set_include_path("D:/WEB/agsSkins/Responsive/application/");        
		require_once "D:/WEB/agsSkins/Responsive/application/libs/utils.php";
		
        $config = array(
            "IGComputerName" => "STRIDECONNECTION",
            "IGapi" => "https://bravo.aspzone.co.uk:51331/Live/OnlineGateway/CustomerManagementService.svc?singleWsdl",
            "IGLog" => 'L:\agsLogs\agsSkins\aspers.com\aspers-update-customers'
        );
        
        include_once 'UpdateCustomerHelpers.php';

        $player = array (
            'WantsEmail' => $_REQUEST["email"],
            'SendSMS' => $_REQUEST["sms"],
            'AspersCustomerNumber' => $_REQUEST["acn"]
        );

        // update player request

        $result = new UpdateIGCustomer($player);
        $igResponse = $result->update();

        // if we updated successfuly the player the response will be "true"
        if ($igResponse) {

            echo 1;
			WriteNewLog(config("IGLog"),"*** Player preferences saved: " . $_REQUEST["acn"] . " ***");

        } else {

            WriteNewLog(config("IGLog"),"*** Could not save player preferences: " . $_REQUEST["acn"] . " ***");
        }
    
    } catch (Exception $e) {
        // do nothing - just ignore
    }