<?php

/*
Formats the request to be sent
Validates input parameters

*/

class IgRegisterCustomerRequest
{

    /*
    Properties
    */
    // private $_customerNumber;
    public $request;
    public $valid = true;
    public $error;
    private $player;

    /*
    Constructor
    */
    function __construct($player)
    {

        $this->player = $player;

        if ($this->requestIsValid()) {
            $computer = config("IGComputerName");
            $member = config("IGMembershipTypeId");

			$playerDOB = date_format($player['DOB'], 'Y-m-d');
			
            $this->request = <<<EOF
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="http://www.IntelligentGaming.co.uk" xmlns:ig="IG.Cms.Gateway.DataContracts">
    <soapenv:Header/>
    <soapenv:Body>
        <int:RegisterCustomer>
           <int:request>
              <ig:ComputerName>{$computer}</ig:ComputerName>
              <ig:DateOfBirth>{$playerDOB}T00:00:00</ig:DateOfBirth>
              <ig:Forename>{$player['Firstname']}</ig:Forename>
              <ig:Gender>{$player['Gender']}</ig:Gender>
              <ig:MembershipTypeId>{$member}</ig:MembershipTypeId>
              <ig:NationalityId>1</ig:NationalityId>
              <ig:Occupation>Not defined</ig:Occupation>
              <ig:Surname>{$player['Lastname']}</ig:Surname>
              <ig:SuppressLogVisit>1</ig:SuppressLogVisit>
              <ig:Title>{$player['Title']}</ig:Title>
           </int:request>
        </int:RegisterCustomer>
    </soapenv:Body>
</soapenv:Envelope>
EOF;

          } else {

               throw new Exception("Player is not valid", 1);

          }
     }

     /*
     Functions
     */
     /*
     Validates the request parameters
     */
     function requestIsValid() {

          return isset($this->player['DOB']) && isset($this->player['Firstname']) && isset($this->player['Gender'])
               && isset($this->player['Lastname']) && isset($this->player['Title']);
     }
     /*
     Returns the response from IG
     */
     function outputResponse($response) {  

        // request was successful
        if ($response->RegisterCustomerResult->Success) {
             // If response have Customer
             if ( isset($response->RegisterCustomerResult->CustomerNumber) ) {
                  return array(
                       "Code" => 0,
                       "CustomerNumber" => $response->RegisterCustomerResult->CustomerNumber,
                  );
             }
             return false;
        }
        return array(
             "Code" => 1,
             "Msg"   => "Request Failed",
             "Error" => $response->RegisterCustomerResult->FailureReason
        );
     }
}
