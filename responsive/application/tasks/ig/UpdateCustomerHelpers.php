<?php

if (!function_exists('config')) {
    function config($what)
    {
        global $config;
        return $config[$what];
    }
}

require_once "SoapClient.php";
require_once "UpdateCustomer.php";

class UpdateIGCustomer {

     function __construct( $player ) {
          $this->player = $player;
     }

     public function update( ) {
          try {

               // create request
               $myRequest = new IgUpdateCustomerRequest( $this->player );

               if ($myRequest->valid) {

                   // create custom soap client
                   $client = new radSoapClient(config("IGapi"));
                   
                   // do the request
                   $igResponse = $client->doRequest('UpdateCustomer', $myRequest->request);

                   // get response
                   $response = $myRequest->outputResponse($igResponse);

                   return $response;

               }

          } catch( Exception $error ) {
                $response = array(
                     "Code"  => 1,
                     "Msg"   => "Something went wrong, please verify that the data entered is correct and try again.",
                     "Error" => $error->getMessage()
               );
          }
     }
}