<?php

class CustomSoapClient extends SoapClient {

	public $request;

	public function __doRequest($request, $location, $action, $version, $one_way = 0) {

	    return parent::__doRequest($this->request, $location, $action, $version);
	}

}

class radSoapClient {

	private $_client;

	static function alert($msg) {
        $subject = "Aspers connection to IG failed";
        $message = "Could not connect to IG API: " . $msg;
        $from = "donotreply@aspers.com";
        $to = "Mohtadib@spacebarmedia.com, OC@daubalderney.com, samuelc@spacebarmedia.com, QA@spacebarmedia.com";
        $headers ="Content-type: text/plain; charset=iso-8859-1\r\n";
        $headers.="From: $from\r\n";
        mail($to,$subject,$message,$headers);
    }

	function __construct($wsdl) {
		
		$opts = array(
				'ssl' => array(
				'ciphers' => 'RC4-SHA',
				'verify_peer' => false,
				'verify_peer_name' => false
			)
		);

		$params = array(
			'encoding' => 'UTF-8',
			'verifypeer' => false,
			'verifyhost' => false,
			'soap_version' => SOAP_1_1,
			'trace' => 1,
			'exceptions' => 1,
			'connection_timeout' => 180,
			'stream_context' => stream_context_create($opts)
		);

		try {
			
			$this->_client = new CustomSoapClient($wsdl, $params);
		} catch (Exception $e) {
			self::alert($e->getMessage());
		}
	}

	public function doRequest($function, $request) {
		
		$this->_client->request = $request;
		return $this->_client->__SoapCall($function, array());
	}

}