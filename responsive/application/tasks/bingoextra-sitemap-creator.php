<?php
/*************************************************************

 BINGOEXTRA

 Simple site crawler to create a search engine XML sitemap
 Runs on a scheduled task every day to refresh the sitemap.xml
 
 8may15 - Hamish Inglis 
 16oct17 - HI - add images as well

 local launcher for job
 "C:\Program Files (x86)\PHP\v5.3\php-cgi.exe" -f "C:\WEB\responsive\trunk\responsive\application\tasks\bingoextra-sitemap-creator.php"
 
 live launcher for job
 "C:\PHP\v7.1\php-cgi.exe" -f "D:\WEB\agsSkins\Responsive\application\tasks\bingoextra-sitemap-creator.php"
 
 
*************************************************************/
set_time_limit(0);


    if(php_uname('n') == 'LDNSTATION80')
	{
		// hamish's computer	
		$base_url = "responsive.bingoextra.dev";												// url to scan
		$base_path = "C:\\WEB\\responsive\\trunk\\responsive\\public\\skins\\bingoextra";		// the real path to the root of the site
	}
	else
	{
		// live servers
		$base_url = "https://www.bingoextra.com";												// url to scan
		$base_path = "D:\\WEB\\agsSkins\\Responsive\\public\\skins\\bingoextra";				// the real path to the root of the site
	}

	
	
	$file = $base_path."\sitemap.xml";											// output file		
	$imgfile = $base_path."\image-sitemap.xml";													// image output file		
	$media_paths = array('/media/affiliates/','/media/exclusive/','/media/ppc/'); // array of all the media file folders to search for subfolders ( 1 per media landing page )


	$debug = false;
		
	$maxDepth = 0;    			// how dee to go down the tree 0 = unlimited
    $extension = ".php";		// scan files with extension
    $freq = "daily";			// scan frequency    
    $scanned = array();
	$imgscanned = array();
	$imgxml = "";
	$depth = 0; 				// current depth
	$imgcount = 0;		
	

	
	// Write sitemap.xml
    $pf = fopen ($file, "w");
    if (!$pf)
    {
		echo "cannot create $file\n";
		return;
    }
		
    fwrite ($pf,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<urlset
      xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
      xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n");
	
	// scan from homepage
	Scan('/');	
		
	foreach($media_paths as $value)
	{
		ScanMediaFiles($value);
	}
    fwrite ($pf, "</urlset>\n");
	fwrite ($pf, "<!-- Sitemap Created on ".date("F j, Y, g:i a")." -->"); 
    fclose ($pf);



	// Write image-sitemap.xml
    $sf = fopen ($imgfile, "w");
    if (!$sf)
    {
		echo "cannot create $imgfile\n";
		return;
    }

	
    fwrite ($sf,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<urlset
      xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"
	  xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" 
      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
      xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");	

		
	fwrite ($sf,$imgxml);

    fwrite ($sf, "
	</urlset>\n");
			
	fwrite ($sf, "<!-- Sitemap Created on ".date("F j, Y, g:i a")." -->"); 
    fclose ($sf);
	

	
	
	
	
	
	/*************************************************************		
	Split the Path
	*************************************************************/		
	function Path ($p)
	{
		$a = explode ("/", $p);
		$len = strlen ($a[count ($a) - 1]);
		return (substr ($p, 0, strlen ($p) - $len));
	}

	/*************************************************************		
	Get data from URL
	*************************************************************/		
	function GetUrl($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		
		$data = curl_exec($ch);
		echo curl_error ( $ch );
		curl_close($ch);
		return $data;
	}

	
	/*************************************************************		
	Recursevely scan the website looking for links starting from the homepage
	*************************************************************/	
	function Scan($url)
	{
		
		global $scanned, $pf, $extension, $freq,  $base_url, $depth, $maxDepth,$imgscanned, $debug;
		$depth++;
		

		$html = GetUrl ($base_url.$url);
		
		if($debug)
		{	
			ob_end_flush();
			echo $base_url.$url."\n"; 
			echo "Images Found: ".ScanImages($html,$base_url.$url) . "\n";
			echo "Images Array Length: ".count($imgscanned) . "\n\n";			
			ob_start();
		}
		else
		{
			ScanImages($html,$base_url.$url);	
		}			
		
	  $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
		
		if(preg_match_all("/$regexp/siU", $html, $matches, PREG_SET_ORDER)) 
		{
		
			foreach($matches as $match) 
			{
			$path = strtolower($match[2]);
			
			if(substr($path,0,1) != '#' 
				&& trim($path) != '' 
				&& trim($path) != '//' 
				&& substr($path,0,7) != 'http://'  
				&& substr($path,0,8) != 'https://'
				&& substr($path,0,7) != 'mailto:'
				&& substr($path,-4) != '.php'
				&& substr($path,0,1) == '/'
				&& substr($path,-1) == '/'
				)
			{			
				if(!in_array ($path, $scanned)) 
				{
					array_push ($scanned, $path);
					
					fwrite ($pf, "<url>\n  <loc>".$base_url.$path."</loc>\n" .
								 "  <changefreq>".$freq."</changefreq>\n" .
								 "</url>\n");
					
						if($maxDepth >= $depth || $maxDepth == 0) Scan($path);							
				}	
			}
		
		 }
	  }
		$depth--;
	}	

	

	/*************************************************************		
	Scan all the images on the page
	*************************************************************/	
	function ScanImages($html,$path)
	{
		global $imgxml, $pf, $extension, $freq,  $base_url, $imgscanned, $imgcount;
				
		$regexp = "<img .*?>";
		if(preg_match_all("/$regexp/si", $html, $matches)) 
		{
			$yesterday = date("Y-m-d", time() - 60 * 60 * 24);
		
			$imgxml = $imgxml . "
		<url>
			<loc>" . $path . "</loc>
			<lastmod>" . $yesterday ."</lastmod>
			<changefreq>daily</changefreq>
			<priority>0.9</priority>";

			$imgcount = 0;		
	  
			foreach($matches[0] as $match) 
			{		
				
				
				// get the src from data-original this is for lazy loaded games
				$src = "";
				$regexp = 'data-original\s*=\s*"(.*?)"';
				if(preg_match("/$regexp/si", $match, $imgmatch)) 
	{
					$src = 	rtrim($path,'/').$imgmatch[1];					
		}
			
				// get the src for normal images
				if($src == "")
		{
					$regexp = 'src\s*=\s*"(.*?)"';
					if(preg_match("/$regexp/si", $match, $imgmatch)) 
			{
						$src = 	rtrim($path,'/').$imgmatch[1];					
					}
			}					
			
				// get the alt for images
				$title = "";
					
				$regexp = 'alt\s*=\s*"(.*?)"';
				if(preg_match("/$regexp/si", $match, $imgmatch)) 
				{
					$title = 	$imgmatch[1];					
		}	
	
				$src = str_replace(array("\n", "\r"), '', $src);
				$title = str_replace(array("\n", "\r"), '', $title);				
				
	
				if(!in_array ($src, $imgscanned) && $src != "") 
				{
					$imgcount ++;	
					array_push($imgscanned,$src);
	
					$imgxml = $imgxml . "
			<image:image>
				<image:loc>" . $src . "</image:loc>";
					if($title != "") 
					{
				$imgxml = $imgxml .  "
				<image:title>" . $title . "</image:title>";
	}
	
	
					$imgxml = $imgxml . "			
			</image:image>";	
				}			
								
			}
			
			$imgxml = $imgxml . "			
		</url>";	
	
		}
	
		return $imgcount;
    }

	
	
	/*************************************************************		
	This adds the media landing pages from the file system	
	*************************************************************/			
	function ScanMediaFiles($media_path)
	{
		global $base_path,$pf,$freq,$base_url;
		$files = array();
		if (is_dir($base_path.$media_path)){
	
			if($debug)
	{
				echo '\nMedia Files\n';		
				echo $base_path.$media_path.'\n';
			}	
			$files = scandir($base_path.$media_path);
	}
    
		// add each landing page
		foreach($files as $file) 
		{
			if($file != '.' && $file != '..')
			{
				fwrite ($pf, "<url>\n  <loc>".$base_url.$media_path.$file."/</loc>\n" .
							 "  <changefreq>".$freq."</changefreq>\n" .
							 "</url>\n");
			}					
		}	
	
	
	
	}
	
?>