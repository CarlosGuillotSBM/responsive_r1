<script>
// make this script work by adding into correct js file
//jQuery('.recent-messages-expand').on('click',function(){
//$(this).toggleClass('collapsed expanded');
//});
</script>
<!-- My Messages Box -->
<div class="lobby-wrap__content-box">
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon" >
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-messages.php'; ?>
                <!-- end include svg icon partial -->
            </div >
            <div class="title__text messages-section">MY MESSAGES</div>
            <div class="title__option-links">
                
                
                <span class="select-all-messages"><input type="checkbox" data-bind="click: PlayerAccount.selectAllToggle"> </span>
            </div>

            <button href="#" data-dropdown="msg-actions" aria-controls="msg-actions" aria-expanded="false" class="tiny button dropdown">Options</button><br>
            <ul id="msg-actions" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                <li><a data-bind="click: PlayerAccount.markAsRead">Mark as Read</a></li>
                <li><a data-bind="click: PlayerAccount.markAsUnread">Mark as Unread</a></li>
                <li><a data-bind="click: PlayerAccount.delete">Delete</a></li>
            </ul>

        </div>
    </div>
    <!-- end header bar -->
    
    <div class="lobby-wrap__content-box__content">
        <!-- make this content a partial -->
        <!-- recent messagages -->
        <div class=" lobby-wrap__content-box__content__innerwrap--border" >
            <ul class="recent-messages" data-bind="css:{collapsed: !PlayerAccount.expanded(),
                                                        expanded: PlayerAccount.expanded}, foreach: PlayerAccount.messages">
                <!-- notice the uread class in this message JS needs to append unread class to show its unread also the svg partials needs changing to have read or  -->
                
                <!-- single message preview -->
                <li  class="recent-messages__message" data-bind="css: { unread: MailStatus() === 1 }, click: $parent.PlayerAccount.revealSingleMessage.bind($data, Id)"> <!--data-reveal-id="single-message"-->
                    <span class="recent-messages__message__icon">
                        <!-- include svg icon partial -->

                        <span data-bind="css: { hide: MailStatus() === 2 }" ><?php include 'icon-message-unread.php'; ?></span>
                        <span data-bind="css: { hide: MailStatus() === 1 }" ><?php include 'icon-message-read.php'; ?></span>
                        <!-- end include svg icon partial -->
                    </span>
                    <span class="recent-messages__message__date" data-bind="text: Date"></span>
                    <span class="recent-messages__message__preview" data-bind="text: Subject"></span>
                    <span class="recent-messages__message__time" data-bind="text: Time"></span>
                    <span class="recent-messages__message__select"><input type="checkbox" data-bind="checked: Clicked, click: $parent.PlayerAccount.check, clickBubble: false"></span>
                </li>
                <!-- End single message preview -->
                
            </ul>
             <!-- shown by default -->
            <div class="recent-messages__switch" data-bind="click: PlayerAccount.expandToggle">
                <span data-bind="visible: !PlayerAccount.expanded()">&#9660; Expand all messages &#9660;</span>
                <span data-bind="visible: PlayerAccount.expanded">&#9650; Collaspse messages &#9650;</span>
            </div>
            
        </div>
        <!-- end recent messagages -->
    </div>
</div>
<!-- End My Messages Box -->