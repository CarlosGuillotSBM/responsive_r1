var sbm = sbm || {};

sbm.Global = function() {
    "use strict";

    return {
        run: function() {


      


            // useful variables for the site
            var page = window.location.pathname;
            var fullurl = window.location
            var device = sbm.serverconfig.device;


            // ------------------------------
            // ----
            //hightlight selected nav element
            // ----
            // ------------------------------
            var _current =  location.pathname.split('/')[1]
            var itemID;
            var item;

           //        if (page == "/") {
           //           $('.header').css('position', 'fixed');
           //       }
             
           // else   {
                   
           //       $('.header').css('position', 'relative');
        
           //  };
// fix the header when scrolling
           var $headerscroll = $('#ui-header-scroll, .nav-for-mobile');
$(document).scroll(function() {
    $headerscroll.css({position: $(this).scrollTop() > 2? "fixed":"relative"});
});
            if (_current == "" || _current == "start") {
                _current = "home";
            };

            if (_current == "register") {
                _current = "join-now";
            };
            // Changing the id of the body when we get the home through ajax navigation.
            $('body').attr('id', _current);
     
            $(".desktop-nav__link").each(function(index) {
                itemID = $(this).attr('id');
                if (itemID != _current) {
                    $(this).find("a").removeClass("current");
                } else {
                    $(this).find("a").addClass("current");
                }
            });


            $(".ui-accordion").on("click", function () {
                $(this).next().toggle();
                $(this).toggleClass("active");
            });

            // change name of Home link and href attr
            if ($.cookie("SessionID")) {
                $(".ui-go-home").attr("href", "/start/").text("START");
                $(".ui-go-home-logo").attr("href", "/start/");
            }

            // --------------
            // SHARE BUTTON
            // --------------

            var config = {
                ui: {
                    flyout: 'middle left'
                },
                networks: {
                    facebook: {
                        url: this.fullurl
                    },
                    twitter: {
                        url: this.fullurl
                    },
                    google_plus: {
                        url: this.fullurl
                    },

                    email: {
                        enabled: false
                    },
                    pinterest: {
                        enabled: false
                    }
                }
            }

            var share = new Share('.share-button', config);

            //We show it if it is not the cashier.
            if (page == '/cashier/' || page == '/my-account/' || page == "/register/" || page == "/welcome/" || page == "/login/") {
                $('.share-button').attr("style", "display: none");
            } else {
                $('.share-button').attr("style", "display: inline");
            }

            // --------------
            // /SHARE BUTTON
            // --------------


            // --------------
            // /cropping text in side promotions
            // --------------
            // $(document).ready(function() {
            //     $(".promotion__content").dotdotdot({
            //         // configuration goes here
            //         watch: "window"
            //     });
            // });

            // SEO Hidden Text 
            $('.ui-toggle').click(function() {
                $(this).toggleClass('open');
            });
          

            var page = window.location.pathname;

       

         //jQuery for click effect on search results screen to show growing circle background
var parent, ink, d, x, y;
$(".ripple li a").click(function(e){
    parent = $(this).parent();
    //create .ink element if it doesn't exist
    if(parent.find(".ink").length == 0)
        parent.prepend("<span class='ink'></span>");
        
    ink = parent.find(".ink");
    //incase of quick double clicks stop the previous animation
    ink.removeClass("animate");
    
    //set size of .ink
    if(!ink.height() && !ink.width())
    {
        //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
        d = Math.max(parent.outerWidth(), parent.outerHeight());
        ink.css({height: d, width: d});
    }
    
    //get click coordinates
    //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
    x = e.pageX - parent.offset().left - ink.width()/2;
    y = e.pageY - parent.offset().top - ink.height()/2;
    
    //set the position and add class .animate
    ink.css({top: y+'px', left: x+'px'}).addClass("animate");
})


        }
    };

};