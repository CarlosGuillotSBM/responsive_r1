var sbm = sbm || {};

sbm.Slick = function () {
    "use strict";

    return {
        run : function () {

            $('.games-list__promoted-games-container').slick({

                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            /**
             * BxSlider plugin initialization for homepage slider
             */

            // cache dom elem
            var $bxSlider = $('.bxslider');

            // make all the images visible
            $bxSlider.find("li").show();

            // start bxSlider plugin
            $bxSlider.bxSlider({
                auto: true,
                pause: '7000',
                autoHover: true,
             
                minSlides: 1,
                slideMargin: 0,
                mode: 'fade',
                pager: false
            });

            // handles clicks on banners

            $bxSlider.find("a").on("click", function (e) {

                var $target = $(e.currentTarget);
                var url = $target.attr("href");

                if (!url) {
                    return;
                }

                var pos = $bxSlider.getCurrentSlide();
                var images = $target.find("img");
                var img = images.length ? $(images).attr("src") : "";
                var playerId = $.cookie("PlayerID") || "";

                var label = playerId ? "player=" + playerId + ";url=" + url + ";img=" + img : "url=" + url + ";img=" + img + ";pos=" + pos;

                sendDataToGoogle("carousel", label);

            });

            var sendDataToGoogle = function (category, label) {

                if (window.dataLayer) {

                    window.dataLayer.push({
                        "event": "GAevent",
                        "eventCategory": category,
                        "eventAction": window.location.pathname,
                        "eventLabel": label
                    });
                }
            };

        }
    };

}