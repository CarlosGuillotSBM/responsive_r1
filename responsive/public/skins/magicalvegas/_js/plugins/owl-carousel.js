var sbm = sbm || {};

/**
 * Owl Carousel
 * Creates an horizontal carousel
 */
sbm.OwlCarousel = function () {
    "use strict";

    return {
        run: function () {

        // Owl Carousel
		var owl = $("#owl-demo");

		owl.owlCarousel({

		items : 10, //10 items above 1000px bser width
		itemsDesktop : [1000,5], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0;
		itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option

		});

		// Custom Navigation Events
		$(".next").click(function(){
		owl.trigger('owl.next');
		})
		$(".prev").click(function(){
		owl.trigger('owl.prev');
		})
		$(".play").click(function(){
		owl.trigger('owl.play',1000);
		})
		$(".stop").click(function(){
		owl.trigger('owl.stop');
		});
		$(".ui-owl-loading-slide").remove();
		$(".ui-owl-slide").show();

		$(".ui-owl-carousel").owlCarousel({

		      autoPlay: false, //Set AutoPlay to 3 seconds
		      items : 4,
		      navigation : true,
		      navigationText : [" "," "],
		      singleItem: true,
		      itemsTablet : [768,1],
		      itemsDesktop : [1199,1],
		      itemsDesktopSmall : [979,1]

		  });
        }
    };

};