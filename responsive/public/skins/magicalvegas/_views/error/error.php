
<!-- MIDDLE CONTENT -->	
<div class="middle-content">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<div class="middle-content__box">
	<!-- breadcrumbs -->
	
	<!-- END breadcrumbs -->
	<section class="section">
		<!-- CONTENT -->
		<div class="content-template">
			
			<div class="error-page">
				<div class="error-page__image">
					<img src="/_images/common/melvis-guitar.png">
				</div>
				<div class="error-page__text">
					<h1 class="error-page__error">
					Whoops!
					</h1>
					<p class="error-page__msg">
						The page you requested is not available.
					</p>
					<div class="error-page__suggestion clearfix">
						<p><a href="/">Please try again. Thanks!</a></p>
						<div class="games-list__grid games-list__promoted-games">
							
							<?php //$this->repeatData($this->games,'_partials/games/games-list__slot.php'); ?>
						</div>
						
					</div>
					<!-- <a class="join-now-button" href="/register/" data-hijack="true">JOIN NOW</a> -->
				</div>
				
			</div>
			
		</div>
		<!-- /CONTENT-->
	</section>
</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->