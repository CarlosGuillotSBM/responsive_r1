<?php
// the hub data is common for all the same controller
// special exception for exclusive slots so it can have it's own hub
$page = $this->controller;
if ($this->action == 'exclusive') $page.= 'exclusive'; ?>
<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->

     <!-- banner -->


    <div id="order-edit-point">
        <!-- Edit point Banner -->
        <?php edit($this->controller,'games-latest-offer'); ?>
    <div class="games-content__banner ui-scheduled-content-container">
        <?php @$this->repeatData($this->content['games-latest-offer']); ?>
    </div>
    
    
        <?php edit($page,'acquisition-banner');?>
    <div id="page-banner-edit" class="page-banner">
        <?php @$this->getPartial($this->content['acquisition-banner'], 1); ?>
    </div>
</div>


    <div class="games-pages">
        <?php include '_global-library/partials/games/games-list.php'; ?>
    </div>

    <div class="bottom-content">
        <?php
            if ($this->action == 'exclusive') {
                edit($this->controller.$this->action,'footer-seo');
            } else {
                edit($this->controller,'footer-seo',$this->action);
            } ?>
        <?php @$this->getPartial($this->content['footer-seo'],$this->action); ?>
    </div>

</div>
<!-- END MIDDLE CONTENT -->