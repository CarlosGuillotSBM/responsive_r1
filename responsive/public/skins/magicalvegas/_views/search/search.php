<div class="search-screen" style="visibility: visible; bottom: 0px;">
    <div class="search-inner">

        <h4 style="color: rgb(255, 255, 255); display: none;" 
            data-bind="visible: GamesMenuSearch.gamesFound().length, text: GamesMenuSearch.gamesFound().length + ' Games Found'">
            </h4>

        <ul class="search-results ripple" data-bind="visible: GamesMenuSearch.gamesFound().length, foreach: GamesMenuSearch.gamesFound" style="display:none">
            <li class="search-result" data-bind="click: $root.GamesMenuSearch.launchGame">
                <a>
                    <div class="search-result__thumb">
                        <img class="search-result__thumb__img" itemprop="game-icon"
                            data-bind="attr: { src: icon }">
                    </div>
                    <div class="search-result__name">
                        <div class="search-result__name__inner"
                            data-bind="text: title"></div>
                    </div>
                    <div class="search-result__cta">
                        <?php include "_partials/common/play-icon-svg.php"; ?>
                    </div>
                </a>
            </li>
        </ul>

    </div>
</div>



