<!-- MIDDLE CONTENT -->
<div class="middle-content">
	<!-- breadcrumbs -->
	<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
	<!--/ breadcrumbs -->
	<!-- MIDDLE CONTENT -->
	<div class="middle-content__box community">
		<section class="section">
			<h1 class="community-heading">COMMUNITY</h1>
			<div class="two-columns-content-wrap">
				<!-- community nav -->
				
				<?php include "_partials/community-nav/community-nav.php" ?>
					<?php edit($this->controller,$this->action,$this->route[0]);?>
				<!-- /community nav -->
				<!-- left column -->
				<div class="two-columns-content-wrap">
					<div class="section__header">
						<h1 class="section__title" itemprop="name"><?php echo @$this->content['Title'];?>
						<span class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></span>
						</h1>
					</div>
					<article class="article community-detail">						
						<div class="article__content">
							<div class="article__content__body" itemprop="articleBody">
								<img class="article__image" src="<?php echo @$this->content['Image'];?>" alt="<?php echo @$this->content['ImageAlt'];?>">
								<?php echo @$this->content['Body'];?>
							</div>
						</div>
						
						
						<!-- Winner Slot Details -->
			<?php 
			if(@$this->content['Text7'] != '')
			{
				include "_global-library/_editor-partials/community-winner-game.php"; 
			}	
			?>
			<!-- End Winner Slot Details -->
			
						<!-- Author Block -->
						<?php if(exists(@$this->content['Image1'])):?>
						<div class="_author-block">
							<div class="_author-block__avatar">
								<img width="" src="<?php echo @$this->content['Image1'];?>" alt="<?php echo @$this->content['ImageAlt1'];?>">																
							</div>
							
							<h4>
							
							<span itemprop="author" itemscope itemtype="http://schema.org/Person">
								<span itemprop="name"><?php echo @$this->content['Text1'];?></span>
								
								</h4>
								
								<div class="_author-block__line">
									<div class="_author-block__lineleft"></div>
									<div class="_author-block__lineright"></div>
								</div>
								
								<div class="_author-block__biography">
									<p><?php echo @$this->content['Terms'];?></p>
									
								</div>
								<ul>
									<a target="_blank" href="<?php echo @$this->content['Text2'];?>" class="_author-block__link" target="_author">
										<li class="_author-block__social-icons">Author's Social Profile: <img src="/_images/common/social/icon-gl.png"></li>
									</a>
								</ul>
								
							</div>
							<?php endif;?>
							<!-- Author Block -->
						</article>
					</div>
					<!--  /left column -->
					<!-- right column -->
			
				</section>
			</div>
			<!-- END MIDDLE CONTENT BOX -->
		</div>
		<!-- END MIDDLE CONTENT -->