<script type="application/javascript">

	var sbm = sbm || {};
	sbm.games = sbm.games || [];
	sbm.games.push({
		"id"    : <?php echo json_encode(@$this->content['DetailsURL']);?>,
		"icon"  : "<?php echo @$this->content['Image'];?>",
		"bg"    : <?php echo json_encode(@$this->content['Text1']);?>,
		"title" : <?php echo json_encode(@$this->content['Title']);?>,
		"type"  : "game",
		"desc"  : <?php echo json_encode(@$this->content["Intro"]);?>,
		"thumb" : "<?php echo @$this->content['Image1'];?>",
		"detUrl": <?php echo json_encode(@$this->content['Path']);?>,
		"demo": <?php echo json_encode(@$this->content['DemoPlay']);?>
	});

</script>
<!-- MIDDLE CONTENT -->
<div class="middle-content">
	<!-- breadcrumbs -->
	<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
	<!--/ breadcrumbs -->
	<!-- MIDDLE CONTENT -->
	<div class="middle-content__box community">
		<section class="section">
			<h1 class="community-heading">COMMUNITY</h1>
			<div class="two-columns-content-wrap">
				<!-- community nav -->
				
				<?php include "_partials/community-nav/community-nav.php" ?>
				<!-- /community nav -->
				<!-- left column -->
				<div class="two-columns-content-wrap">
					<div class="section__header">
						<h1 class="section__title" itemprop="name"><?php echo @$this->content['Title'];?>
						<span class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></span>
						</h1>
					</div>
					<?php edit($this->controller,$this->action,$this->subaction); ?>
					<article class="article community-detail">
						
						<div class="article__content">
						
							<div class="article__content__body" itemprop="articleBody">
								<img class="article__image" src="<?php echo @$this->content['Text3'];?>" alt="<?php echo @$this->content['ImageAlt'];?>">
								<?php echo @$this->content['Body'];?>
							</div>
						</div>
						
						
						<!-- Winner Slot Details -->
						<div class="article__content" style="clear:both">
							<div class="winner-game">
								<div class="winner-game__left">
									<h4>Won on: <a href="<?php echo @$this->content['Path'];?>"><?php echo @$this->content['Name'];?></a></h4>
									<h4>Won amount: <span><?php echo @$this->content['Text11'];?></span></h4>
									<p><?php echo @$this->content['Text12'];?><br /><a href="<?php echo @$this->content['Path'];?>">More Info on <em><?php echo @$this->content['Name'];?></em> »</a></p>
								</div>

								<div class="winner-game__right">
									<article class="game-icon">
										<div class="game-icon__wrapper">							
											<div class="game-icon__image">
												<img data-bind="click: GameLauncher.openGame"
													 data-gameid="<?php echo @$this->content["DetailsURL"];?>"
													 class="lazy" src="/_images/common/no-image.jpg" alt="<?php echo @$this->content["Alt"];?>"
													 data-original="<?php echo @$this->content["Image"];?>">
												<div class="clearfix"></div>
										
											</div>
											<!-- overlay -->
											<div  class="game-icon__overlay">

												<div data-bind="click: GameLauncher.openGame" class="ui-launch-game play-icon "
													data-gameid="<?php echo @$this->content["DetailsURL"];?>">
													<img class="playimg" src="/_images/common/icons/play.png">
												</div>
											</div>									
											<!-- end overlay -->
										</div>						
									</article>							
									<a data-bind="click: GameLauncher.openGame" data-gameid="<?php echo @$this->content["DetailsURL"];?>" class="button winner-game__right-btnJoinnow">PLAY NOW</a>
								</div>
							</div>
						</div>			
						<!-- End Winner Slot Details -->
			
						<!-- Author Block -->
						<div class="_author-block">
							<?php if(exists(@$this->content['Text7'])):?>
							<div class="_author-block__avatar">
								<img width="" src="<?php echo @$this->content['Text7'];?>" alt="<?php echo @$this->content['Text8'];?>">
							</div>
							
							<h4>
							
							<span itemprop="author" itemscope itemtype="http://schema.org/Person">
								<span itemprop="name"><?php echo @$this->content['Text5'];?></span>
								
								</h4>
								
								<div class="_author-block__line">
									<div class="_author-block__lineleft"></div>
									<div class="_author-block__lineright"></div>
								</div>
								
								<div class="_author-block__biography">
									<p><?php echo @$this->content['Terms'];?></p>
									
								</div>
								<ul>
									<a target="_blank" href="<?php echo @$this->content['Text6'];?>" class="_author-block__link" target="_author">
										<li class="_author-block__social-icons">Author's Social Profile: <img src="/_images/common/social/icon-gl.png"></li>
									</a>
								</ul>
								
							</div>
							<?php endif;?>
							<!-- Author Block -->
						</article>
					</div>
					<!--  /left column -->
					<!-- right column -->
			
				</section>
			</div>
			<!-- END MIDDLE CONTENT BOX -->
		</div>
		<!-- END MIDDLE CONTENT -->