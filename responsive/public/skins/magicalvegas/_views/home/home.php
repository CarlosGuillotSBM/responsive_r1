<!-- CONTENT -->
<main>
<!-- OFFER SECTION -->
<section class="offer-container">
  <div class="offer">
    <div class="text-center">

      <!-- OFFER TEXT -->
       <div class="bonus">
          <h1 class="wow fadeInUp animated" data-wow-delay=".15s" style="visibility: visible; animation-delay: 0.15s; animation-name: fadeInUp;">100% bonus</h1>
          <p class="no-deposit wow fadeInDown animated" data-wow-delay=".60s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInDown;">up to &pound;500</p>
          <!-- <p class="wow fadeInDown animated" data-wow-delay=".20s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInDown;">STARBURST <span>or</span> PYRAMID <span>or</span> TWIN SPIN</p> -->
        </div>

      <!-- CTA -->
      <a class="join-now" href="/register/">join now</a>

    </div>
  </div>

  <div class="hero-container"></div>

  <!-- overlay -->
  <span class="overlay"></span>

  
  <!-- VIDEO -->
<!--   <div class="video-container">
    <video autoplay loop muted playsinline poster="../_images/videos/bg-offer.jpg" class="fullscreen-bg__video">
      <source src="../_images/videos/video.mp4" webkit-playsinline autoplay="true" loop="true" type="video/mp4">
      <source src="../_images/videos/video.webm " webkit-playsinline autoplay="true" loop="true" type="video/webm">
      <source src="../_images/videos/video.ogv" webkit-playsinline autoplay="true" loop="true" type="video/ogv" >
    </video>
    <div itemprop="video" itemscope itemtype="http://schema.org/VideoObject" class="video-seo">
      <h2><span itemprop="name">Spinning Slots - Join Magical Vegas Online Casino and enjoy a 100% bonus.</span></h2>
      <meta itemprop="thumbnailUrl" content="../_images/videos/bg-offer.jpg" />
      <span itemprop="description">Join Magical Vegas Online Casino and enjoy a 100% bonus.</span>
    </div>
  </div> -->
</section>

<!-- TCS -->
  <div class="tcs twelve">

            <?php edit($this->controller,'hero'); ?>
                <?php @$this->getPartial($this->content['hero'],1, "_partials/banner/homepage-hero-banner.php"); ?>

</div>


<script type="application/javascript">
skinPlugins.push({
id: "OwlCarousel"
});
</script>

<!-- FEATURE SLOTS SECTION  -->
<ul class="section container feature-slots">
<?php $notLazy = false; //we can't use lazy display since it applies before the carousel and it turns out a game dissapears ?>

  <!-- Slots We Love  -->
  <li class="vegas-favourites">
    <nav>
      <h3>slots we love</h3>

      <a data-hijack="true" class="view-all" href="/slots/featured/">View all</a>
    </nav>
     <?php $editorBlock = 'slots-we-love';
           $slideTemplate = '_global-library/_editor-partials/game-icon-banner.php' ?>

      <?php include '_global-library/partials/common/owl-carousel.php'; ?>
  </li>

  <!-- Vegas Favourites -->
  <li class="rocking-slots">
    <nav>
      <h3>vegas favourites</h3>

      <a data-hijack="true" class="view-all" href="/slots/exclusive/">View all</a>
    </nav>
     <?php $editorBlock = 'vegas-favourites';?>
      <?php include '_global-library/partials/common/owl-carousel.php'; ?>
  </li>

  <!-- Hot on the Strip -->
  <li class="new-on-the-strip">
    <nav>
      <h3>hot on the strip</h3>

      <a data-hijack="true" class="view-all" href="/slots/new/">View all</a>
    </nav>
     <?php $editorBlock = 'hot-on-the-strip';?>
      <?php include '_global-library/partials/common/owl-carousel.php'; ?>

  </li>
</ul>


<!-- CATEGORIES SLOTS SECTION -->
<div class="categories-slots-wrap">
  <!-- DESKTOP -->
  <ul class="categories-slots">
    <li><a data-hijack="true" href="/slots/"><img src="../_images/home-new/slots-categories/slots.png"></a></li>
    <li><a data-hijack="true" href="/live-casino/"><img src="./_images/home-new/slots-categories/live-casino.png"></a></li>
    <li><a data-hijack="true" href="/table-card/"><img src="./_images/home-new/slots-categories/table-card.png"></a></li>
    <li><a data-hijack="true" href="/roulette/"><img src="./_images/home-new/slots-categories/roulette.png"></a></li>
    <li><a data-hijack="true" href="/scratch-and-arcade/"><img src="./_images/home-new/slots-categories/scratch-arcade.png"></a></li>
  </ul>
</div>
<div class="seo container">
   <?php edit($this->controller,'SEO-Title');?>
  <h1><?php $removeTags = true; @$this->repeatData($this->content['SEO-Title'],0,"_global-library/_editor-partials/text-removing-tags.php");?></h1>
   <?php edit($this->controller,'SEO-Content');?>
  <div class="ui-toggle seo-copy">

    <?php 
    /*
          $removeTags = true; @$this->repeatData($this->content['SEO-Content'],0,"_global-library/_editor-partials/text-removing-tags.php");
    */
    ?>
    <?php @$this->repeatData($this->content['SEO-Content'],0);?>
</div>
</div>

</main>
<!-- / CONTENT -->
