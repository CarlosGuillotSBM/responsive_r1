<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<div class="middle-content__box">

	<section class="section middle-content__box">
		<div class="section__header">
			<h1 class="section__title">My Favourites</h1>
		</div>
		<!-- Favourites row -->
		<div class="favourites">
			<?php include "_global-library/_editor-partials/favourite-game-widget.php" ?>
		</div>
		<!-- /Favourites row -->
	</section>
	<!-- /FAVOURITES -->
</div>	
