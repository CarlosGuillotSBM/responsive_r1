<?php

// old luckypants affiliate re-directs
$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';

if(preg_match("#^(\/free-bingo-slots\/|\/30-free-plus-10-free-spins\/|\/deposit-10-get-40\/)$#", $path) == 1) //|\/kitty-club\/
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if(preg_match("#^(\/30free\/|\/30-free-as-seen-on-tv-video\/|\/bingo-kittens\/|\/first-bonus\/|\/free-bingo-bonus\/|\/free-mobile-bingo\/|\/free30\/|\/get-30-free\/|\/meow-bingo-30-free\/|\/slots-bonus\/|\/treat\/|\/wired-300-signup-bonus\/|\/your-cat\/)$#", $path) == 1) //|\/kitty-club\/
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/affiliates" . $path. $queryString);
    exit;
}

if(preg_match("#^(double-slots-bonus\/|no-deposit-required\/|\/30-free-as-seen-on-tv\/|\/play-with-40-as-seen-on-tv\/|\/play-with-40-as-seen-on-tv\/|\/30-free-plus-15-free-spins\/|\/300-bonus\/|\/free-bonus-rain\/)$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/affiliates" . $path. $queryString);
    exit;
}

if(preg_match("#^(\/jackpot-cafe-recommends-kitty-bingo\/|\/jackpot-liner-recommends-kitty-bingo\/|\/king-jackpot-recommends-kitty-bingo\/|\/love2shop\/|\/amazon-voucher-10\/)$#", $path) == 1) //|\/kitty-club\/
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive" . $path. $queryString);
    exit;
}

if(preg_match("#^(\/general-promotional-terms-and-conditions\/|\/general-promotions-terms-and-conditions\/)$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /terms-and-conditions/general-promotional-terms-and-conditions/" . $queryString);

    exit;
}

if(preg_match("#^\/giveaway\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/great-british-giveaway/". $queryString);
    exit;
}


if(preg_match("/\/bingo-schedule\/$/", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if(preg_match("/\/easter-text\/easter-special-page-text\/$/", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if(preg_match("/\/hiddenpromo\/kitty-friends\/$/", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

//Landing page redirects
if(preg_match("#^\/claim\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/exclusive/claim/' );
    exit;
}

if(preg_match("#^\/swing\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporary');
    header("Location: /register/swing/". $queryString);
    exit;
}

if(preg_match("#^\/wow\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporary');
    header("Location: /login/200spins/". $queryString);
    exit;
}

// DEV-10368
if (preg_match("#^\/free\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/free/". $queryString);
    exit;
}

// DEV-10401

if (preg_match("#^\/play\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/play/". $queryString);
    exit;
}

if (preg_match("#^\/spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/spins/". $queryString);
    exit;
}

// 10983
if (preg_match("#^\/advent-1\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-1/". $queryString);
    exit;
}
if (preg_match("#^\/advent-4\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-4/". $queryString);
    exit;
}
if (preg_match("#^\/advent-6\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-6/". $queryString);
    exit;
}
if (preg_match("#^\/advent-8\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-8/". $queryString);
    exit;
}
if (preg_match("#^\/advent-12\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-12/". $queryString);
    exit;
}
if (preg_match("#^\/advent-13\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-13/". $queryString);
    exit;
}
if (preg_match("#^\/advent-15\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-15/". $queryString);
    exit;
}
if (preg_match("#^\/advent-17\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-17/". $queryString);
    exit;
}
if (preg_match("#^\/advent-18\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-18/". $queryString);
    exit;
}
if (preg_match("#^\/advent-22\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-22/". $queryString);
    exit;
}
if (preg_match("#^\/advent-25\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-25/". $queryString);
    exit;
}

//11246
if (preg_match("#^\/purrfect-catch\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/purrfect-catch/". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/30free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/30free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/30-free-as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/30-free-as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/30-free-as-seen-on-tv-video\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/amazon-voucher-10\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/bingo-kittens\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/bingo-kittens\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/first-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/first-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/first-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/first-bonus-amp\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/first-bonus-amp\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/first-bonus-amp\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/first-bonus-cleo-amp\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/free30\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/free-bingo-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/free-bingo-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/free-bingo-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/free-mobile-bingo\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/free-mobile-bingo\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/free-mobile-bingo\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/get-30-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/get-30-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/get-30-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/love2shop\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/meow-bingo-30-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/meow-bingo-30-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/meow-bingo-30-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/treat\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/treat\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/treat\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/welcome-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/welcome-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/welcome-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/ppc/welcome-bonus-2\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/wired-300-signup-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/affiliates/your-cat\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if (preg_match("#^\/media/exclusive/your-cat\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

//R10-204
if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/kittybingo/". $queryString);
    exit;
}
//R10-1009
if (preg_match("#^\/excluded\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/excluded/". $queryString);
    exit;
}

//R10-1251
if (preg_match("#^\/deal-no-deal\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/deal-no-deal/". $queryString);
    exit;
}