<?php
if(Env::isLive())
{?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W7WCV8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W7WCV8');</script>
<!-- End Google Tag Manager -->

	<?php if(substr(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),0,10) == '/media/ppc'){?>
		<!--  Clickcease.com tracking ONLY USED ON PPC PAGES-->

		<script type="text/javascript">
		var secured = (("https:" == document.location.protocol) ? "https://" : "http://");
		document.write("<sc"+"ript type='text/javascript' src='" + secured + "monitor.clickcease.com/stats/stat.js'></"+"script>");
		</script>

		<noscript><div>
		<a title="Click fraud detection and prevention software" href="https://clickcease.com" target="_blank"><img src="https://monitor.clickcease.com/stats/stats.aspx" alt="Click Fraud Protection"></a></div></noscript>
		<!--  end Clickcease.com tracking -->

	<?php }
}?>	