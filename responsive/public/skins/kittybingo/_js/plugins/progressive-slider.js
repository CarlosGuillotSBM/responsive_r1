var sbm = sbm || {};

sbm.ProgressiveSlider = function (options) {
    "use strict";

    return {

        $slides: null,
        minSlides: options ? (sbm.serverconfig.device > 1 ? 2 : options.slidesToShow)  : 1,
        mode: options ? options.mode : "vertical",

        setAmount: function () {
            var $this,
                amount,
                $amounts = $(".ui-prog-jackpot-amount");

            $amounts.each(function () {
                $this = $(this);
                // get amount
                amount = $this.data("amount");
                if (isNaN(amount)) {
                    amount = amount.replace(",", "");
                }

                // decrement the value and auto increment based on timer

                var amountFrom = amount - (amount*0.1) > 0 ? amount - (amount*0.1) : 0;

                $this.countTo({
                    from: amountFrom,
                    to:  amount,
                    speed: 1200000,
                    refreshInterval: 2000,
                    currencySymbol: $.cookie("CurrencySymbol")
                });

            });

        },

        run : function () {

            // get the slides
            this.$slides = $(".progressive-jackpots__progressive.slide");

            // wrap all slides around a parent div
            this.$slides.wrapAll("<div class='jackpot-slider' />");

            // if needs to slide
            if (this.$slides.length > this.minSlides) {

                // apply bxSlider plugin onto the parent div
                $(".jackpot-slider").bxSlider({
                    mode: 'vertical',
                    minSlides: this.minSlides,
                    auto: true,
                    pager: false,
                    controls: false,
                    speed: 1700,
                    pause: 5000,
                    touchEnabled: false
                });
            }

            this.setAmount();

        }

    };
};