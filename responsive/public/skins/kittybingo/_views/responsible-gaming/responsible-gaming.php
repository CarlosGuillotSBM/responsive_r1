<div class="main-content responsible-gaming-content">

  <!-- Title -->
  <div class="section-left__title">
    <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
  </div>

    <!-- content -->
    <?php edit($this->controller,'responsible-gaming'); ?>
    <?php @$this->repeatData($this->content['responsible-gaming']);?>

    <!-- reality check dropdown -->
    <?php include '_global-library/partials/reality-check/reality-check-settings.php'; ?>

</div> 