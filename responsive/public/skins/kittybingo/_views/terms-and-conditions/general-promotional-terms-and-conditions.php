<div class="main-content general-promotional-terms-and-conditions">

  <!-- Title -->
  <div class="section-left__title">
    <h4>General Promotional Terms &amp; Conditions</h4>
  </div>

  <!-- wrap -->
  <section class="content-holder">

    <!-- inner wrap -->
    <div class="content-holder__inner">
      <!-- content -->
      <?php edit($this->controller,'generalTerms'); ?>
      <ul class="accordion accordion-wrap">
        <?php @$this->repeatData($this->content['generalTerms'], 1, "_global-library/partials/common/text-accordion.php");?>
      </ul>
      <!-- /content -->
    </div>
    <!-- inner wrap -->
  
  </section>
  <!-- /wrap -->

</div>