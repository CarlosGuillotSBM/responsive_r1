<script type="application/javascript">
//skinModules.push({ id: "BingoRoomDetailsPage", options: { roomId: <?php echo @$this->bingo["Text1"]; ?> } });
</script>

<!-- bingo content -->
<div class="two-columns-content-wrap bingo-details-content">



  <!-- left column -->
  <div class="two-columns-content-wrap__left">


    <section class="content-holder">

      <!-- Room Acquisition Banner -->
      <?php edit($this->controller, "bingo-detail-acquisition-banner",$this->action); ?>
      <div class="bingo-detail-content__img">
        <?php @$this->repeatData($this->category["bingo-detail-acquisition-banner"], 1, "_global-library/_editor-partials/banner.php"); ?>
      </div>

                <!-- ACQUISITION CONTENT -->
          <div class="bingo-detail-content__acquisition" data-bind="visible: !validSession()">
            <div class="bingo-detail-content__acquisition__cta">
              <a class="cta-login ui-scroll-top" data-reveal-id="custom-modal">LOGIN</a>
              <a class="cta-join" data-nohijack="true" href="/register/">Join Now</a>
            </div>
          </div>

    </section>

    <!-- Bingo Schedule Holder Row -->
    <section class="content-holder bingo-content">
      <div class="mobile-hide-bingo">
          <!-- Bingo Schedule Holder Row -->
          <?php include'_partials/bingo-schedule/bingo-schedule-holder-home.php'; ?>
      </div>
      <!-- bingo content part1 -->
      <div class="mobile-show-bingo">
            <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>

      </div>
    </section>

    <section class="content-holder text-partial">
      <?php edit($this->controller,"bingo-copy-text-".$this->action,"","margin-top: -20px"); ?>
      <?php @$this->repeatData($this->category["bingo-copy-text-".$this->action],1); ?>
    </section>

    <!-- BINGO ROOMS -->
    <div class="bingo__rooms">
      <ul class="bingo-detail-page__rooms">
        <?php edit($this->controller,'bingo-rooms',$this->action); ?>
        <?php @$this->repeatData($this->category['bingo-rooms'],1,'_partials/bingo-rooms/bingo-room-games.php'); ?>
      </ul>
    </div>
  </div>
  <!-- /left column -->

  <!-- right column -->
  <div class="two-columns-content-wrap__right">
    <div class="content-holder__content_right">
      <?php edit($this->controller, 'bingo-detail-right-column', $this->action); ?>
      <?php @$this->repeatData($this->category["bingo-detail-right-column"], 1); ?>
    </div>
  </div>
  <!-- /right column -->

</div>

