<!-- bingo content -->
<div class="two-columns-content-wrap bingo-content">

  <!-- left column -->
  <div class="two-columns-content-wrap__left">



    <div class="mobile-hide-bingo">
        <!-- Bingo Schedule Holder Row -->
        <?php include'_partials/bingo-schedule/bingo-schedule-holder-home.php'; ?>
    </div>

    <!-- bingo content part1 -->
    <div class="mobile-show-bingo">
          <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>
    </div>

    <section class="content-holder">
        <!-- bingo rooms slider -->
        <div class="bingo-room-slider">
            <!-- title -->
            <h1 class="content-holder__title">
          <?php @$this->getPartial($this->content['bingo-top-title'],1); ?>
        </h1>
          <?php include "_partials/bingo-rooms/bingo-rooms-slider.php"; ?>
        </div>
    </section>

    <section class="content-holder">
        <?php edit($this->controller,'footer-'.$this->action); ?>
        <?php @$this->repeatData($this->content['footer-'.$this->action],1); ?>
    </section>
  </div>

  <!-- right column wrap -->
  <div class="two-columns-content-wrap__right">
    
          <div class="content-holder__content_right">
                
                <!-- title -->
                <div class="content-holder__title">
                    <h1>PROGRESSIVE JACKPOTS</h1>
                    <!-- content holder title right link -->
                    <?php
                    $titleHeaderHeight = 25;
                    $slideHeight = 95;
                    $slidesToShow = 2;
                    ?>
                    <script type="application/javascript">
                    skinPlugins.push({
                    id: "ProgressiveSlider",
                    options: {
                    slidesToShow: <?php echo $slidesToShow ?>,
                    mode: "vertical"
                    }
                    });
                    </script>
                </div>

                <div class="content-holder__inner progressive-jackpot-wrap"> 
                    <?php edit($this->controller,'home-progressives'); ?>
                    <div class="progressive-jackpots__wrapper">
                        <div class="progressive-jackpots">
                            <?php
                            if(config("RealDevice") < 3)
                            {
                            @$this->repeatData($this->content['home-progressives'],1);
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>

            </div>


            <div class="content-holder ui-scheduled-content-container">
               <!-- Edit point Banner -->             
              <?php edit($this->controller,'bingo-side-promotions'); ?>

                  <?php
                    if(config("RealDevice") < 3)
                    {
                    @$this->repeatData($this->content['bingo-side-promotions'],3);
                    }
                  ?>
              <!-- Edit point Banner -->
           </div>



</div>