
<div class="two-columns-content-wrap help-content">

  <!-- left column wrap -->
  <div class="two-columns-content-wrap__left">

      <!-- title -->
      <div class="section-left__title">
        <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
      </div>

     <!-- CONTENT text only-->
     <div class="content-template">
        <?php edit($this->controller,'support'); ?>
        <?php @$this->repeatData($this->content['support']);?>
    </div>


      <section class="text-partial">
        <div class="text-partial-content">

          <section class="help-content-top">
            
            <!-- EMAIL -->
            <div class="help-content__section">
              <img src="/_images/common/support/email.svg" class="email">
              <div class="help-content__info">
                <h1>Email</h1>
                <a data-hijack="false" href="mailto:support@kittybingo.com">support@kittybingo.com<em>(for all account-related queries)</em></a>
                <a data-hijack="false" href="mailto:promotions@kittybingo.com">promotions@kittybingo.com<em>(for all promotion-related queries)</em></a>
                <a data-hijack="false" href="mailto:vip@kittybingo.com">vip@kittybingo.com<em>(for all VIP-related queries)</em></a>
                <p>Please note that emails will be responded to within 24 hours after receipt</p>
              </div>
            </div>
            <!-- PHONE NUMBERS -->
            <div class="help-content__section">
              <img src="/_images/common/support/phone.svg">
              <div class="help-content__info">
                <h1>Phone Numbers</h1>
                <h2>United Kingdom</h2>
                <span itemprop="telephone"><a target="_blank" data-hijack="false" href="tel:08002793221">0800 279 3221 (Freephone)</a></span>
                <span itemprop="telephone"><a target="_blank" data-hijack="false" href="tel:02035446725">0203 544 6725 (Landline)</a></span>
                <p>Please note that inbound and outbound calls may be recorded for security and training purposes.</p>
              </div>
            </div>
            <!-- LIVE CHAT -->
            <div class="help-content__section">
              <img src="/_images/common/support/live-chat.svg">
              <div class="help-content__info">
                <h1>Live Chat</h1>
                <a href="#" onclick="javascript:void window.open('<?php echo config('LiveChatURL'); ?>','1323353895481','width=660,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;"><span>24h live chat</span></a>
              </div>
            </div>
          </section>
          <!-- CONTENT WRAP BOTTOM -->

          <!-- CONTENT WRAP PASSWORD -->
          <section class="help-content-password"  style="display: none; cursor: pointer"  data-bind="visible: !validSession()" >
            <div class="help-content__section">
              <img src="/_images/common/support/lock.svg">
              <!-- COLLAPSE
              http://foundation.zurb.com/sites/docs/v/5.5.3/components/accordion.html
              -->
              <ul class="accordion help-content__info">
                <li class="accordion-navigation">
                  <a data-hijack="false" class="ui-accordion">
                    <h1>Forgot Password / Username?</h1>
                    <img src="/_images/common/accordion-down-arrow.svg">
                  </a>
                  <div class="content" style="display: none">
                    <!--  <span>Enter your email address and we will send you a password reset email.</span> -->
                    <?php include '_partials/common/forgot-password.php'; ?>
                  </div>
                </li>
              </ul>
            </div>
          </section>
        </div>
      </section>
      <!-- / CONTENT WRAP -->
      <!-- repeatable content -->
     <?php // $this->repeatData($this->content['about-us']);?>

  </div>


  <!-- right column wrap -->
  <?php if (config("RealDevice") != 3) { ?>
  <div class="two-columns-content-wrap__right">
    
    <!-- content -->
    <div class="content-holder">
     <?php edit($this->controller,'Help__side-content'); ?>
      <?php @$this->repeatData($this->content['Help__side-content']); ?>
    </div>
          
  </div>
  <?php } ?>

</div>
<!--/two columns content wrap -->




