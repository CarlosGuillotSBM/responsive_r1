 <!-- single post main content -->
<div class="main-content community-chat-news"> 

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>

	<!-- content -->
	<div class="middle-content__box">			
	<!-- community nav -->
	<?php include "_partials/community-nav/community-nav.php" ?>
	<!-- /community nav -->
	<div class="two-columns-content-wrap middle-content__box">
		<!-- left column -->
		<div class="two-columns-content-wrap__left">

		<div class="section-left__title__community">
		<h4><span>Chat Moderator Bios</span></h4>
		</div>
			<!-- chat hosts area -->
			<div class="community-chat-news__hosts">
				<?php edit($this->controller,'chat-host'); ?>
				<?php @$this->repeatData($this->content['chat-host']);?>
			</div>
			<!-- /chat hosts area -->
			<div class="clearfix">&nbsp;</div>
			<div class="community-chat-news__article">
				<?php edit($this->controller,'chat-news'); ?>
				<!-- repeatable content -->
				<?php @$this->repeatData($this->content['chat-news']);?>
				<!-- /repeatable content -->
			</div>
			<div class="clearfix"></div>
		</div>
		<!--  /left column -->
		<!-- right column -->
		<div class="two-columns-content-wrap__right">
		<?php if (config("RealDevice") != 3) { ?>			
			<?php edit($this->controller,'chat-news-side-content'); ?>
			<!-- repeatable content -->
			<?php @$this->repeatData($this->content['chat-news-side-content']);?>
			<!-- /repeatable content -->
		<?php } ?>
		</div>
		<!-- /right column -->
	</div>
	<!-- /content  -->

</div>

</div>
<!-- /single post main content -->

