<!-- single post main content -->
<div class="main-content community-blog">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  
	<!-- content -->
	<div class="middle-content__box">			<!-- community nav -->
	<?php include "_partials/community-nav/community-nav.php" ?>
	<!-- /community nav -->
	<div class="two-columns-content-wrap middle-content__box">
		<!-- left column -->
		<div class="two-columns-content-wrap__left">
			<?php edit($this->controller,'tv'); ?>
			<!-- repeatable content -->
			<?php @$this->repeatData($this->content['tv']);?>
			<!-- /repeatable content -->
		</div>
		<!--  /left column -->
		<!-- right column -->
		<div class="two-columns-content-wrap__right">

		<?php if (config("RealDevice") != 3) { ?>
			<?php edit($this->controller,'blog-side-content'); ?>
			<!-- repeatable content -->
			<?php @$this->repeatData($this->content['blog-side-content']);?>
			<!-- /repeatable content -->
			<!-- /right column -->
		<?php } ?>
		</div>
		<!-- /content  -->
	</div>

</div>
</div>
</div>
<!-- /single post main content -->

