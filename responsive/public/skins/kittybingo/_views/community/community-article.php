<!--single post main content -->
<div class="two-columns-content-wrap community-article">

	<div class="two-columns-content-wrap__left">

	  <!-- Title -->
	  <div class="section-left__title">
	    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
	  </div>
  
	<!-- content -->
	<?php 
	edit($this->controller,$this->action,$this->route[0]); ?>
	
	<div class="content-template middle-content__box">
		<!-- POST CONTENT -->
		<article class="article">
			<div class="article__content">
				
				
				<div class="article__content__body" itemprop="articleBody">
					<img width="" src="<?php echo @$this->content['Image'];?>" alt="<?php echo @$this->content['ImageAlt'];?>">

					<h1 class="article__content__title" itemprop="name">
				<div class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></div> 
				<a class="article__url" href="" itemprop="url" ><?php echo @$this->content['Title'];?> </a>
				</h1>
				
					<?php echo @$this->content['Body'];?>
				</div>
			</div>
			<span class="article__publisher" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
				<span itemprop="name"><?php echo config("Name");?></span>
			</span>
			</article>
			<!-- /POST CONTENT -->
		</div>
		</div> <!--End of left column-->

		<!-- right column wrap -->
	   <?php if (config("RealDevice") != 3) { ?>
	  		<div class="two-columns-content-wrap__right">
	    <!-- content -->
	    	<div class="content-holder">
		     	<?php edit($this->controller,'side-content'); ?>
		      	<?php @$this->repeatData($this->sidebanners); ?>
	    	</div>
	          
	  		</div>
	  	<?php } ?>
	</div>
	<!-- /content -->
</div>
