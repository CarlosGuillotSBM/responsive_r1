<script type="application/javascript">

	var sbm = sbm || {};
	sbm.games = sbm.games || [];
	sbm.games.push({
		"id"    : <?php echo json_encode(@$this->content['DetailsURL']);?>,
		"icon"  : "<?php echo @$this->content['Image'];?>",
		"bg"    : <?php echo json_encode(@$this->content['Text1']);?>,
		"title" : <?php echo json_encode(@$this->content['Title']);?>,
		"type"  : "game",
		"desc"  : <?php echo json_encode(@$this->content["Intro"]);?>,
		"thumb" : "<?php echo @$this->content['Image1'];?>",
		"detUrl": <?php echo json_encode(@$this->content['Path']);?>,
		"demo": <?php echo json_encode(@$this->content['DemoPlay']);?>
	});

</script>
<!--single post main content -->
<div class="main-content community-article">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  
	<!-- content -->
	<?php edit($this->controller,$this->action,$this->subaction); ?>
	
	<div class="content-template middle-content__box">
		<!-- POST CONTENT -->
		<article class="article__winner">
			<div class="article__content">
				<h1 class="article__content__title" itemprop="name">
				<div class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></div>
				<a class="article__url" href="" itemprop="url" ><?php echo @$this->content['Title'];?> </a>
				</h1>
				
				<div class="article__content__body" itemprop="articleBody">
					<img width="" class="winners-image" src="<?php echo @$this->content['Text3'];?>" alt="<?php echo @$this->content['ImageAlt'];?>">
					<?php echo @$this->content['Body'];?>
				</div>
			</div>
			<span class="article__publisher" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
				<span itemprop="name"><?php echo config("Name");?></span>
			</span>
			</article>
			<!-- /POST CONTENT -->
		</div>

</div>
	<!-- /content -->

