<!-- two columns content wrap -->
<div class="two-columns-content-wrap community-article-content">
	

	<!-- left column wrap -->
	<div class="two-columns-content-wrap__left">


			<!-- winners menu -->
	<?php include '_partials/games-menu/winners-menu.php'; ?>
		
		<!-- title -->
		<div class="section-left__title">
			<h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
		</div>

		<!-- content images -->
		<div class="article-content__img__wrapper">
			<a data-hijack="true" data-bind="click: playBingo">
			<img src="<?php echo $this->content[$this->action][0]['Image']?>" alt="<?phpecho $this->content[$this->action][0]['Alt']?>">
			</a>
		</div>

		<!-- CONTENT -->
		<?php edit($this->controller,$this->action); ?>

		<!-- POST CONTENT -->
		<article class="article">
			<div class="article__content">
				<h1 class="article__content__title" itemprop="name">
					<div class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></div>
					<a class="article__url" href="" itemprop="url" ><?php echo @$this->content['Title'];?> </a>
				</h1>

				<div class="article__content__body" itemprop="articleBody">
					<?php echo @$this->content[$this->action][0]['Body'];?>
				</div>

			</div>
			
			<span class="article__publisher" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
				<span itemprop="name"><?php echo config("Name");?></span>
			</span>
		</article>
		<!-- /POST CONTENT -->

	</div>
	<!-- /left column wrap -->

	<!-- right column wrap -->
	<?php if (config("RealDevice") != 3) { ?>
	<div class="two-columns-content-wrap__right">
		
		<!-- content -->
		<?php edit($this->controller,'community-article__side-content'); ?>
		<div class="content-holder">
			<?php @$this->repeatData($this->content['community-article__side-content']); ?>
		</div>
		
	</div>
	<!-- /right column wrap -->

	<?php } ?>
</div>
<!--/two columns content wrap -->