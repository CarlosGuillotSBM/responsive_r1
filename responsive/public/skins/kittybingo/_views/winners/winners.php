<script type="application/javascript">
// skinPlugins.push( {id: "rad_dotdotdot"} );
</script>
<!-- single post main content -->
<div class="main-content community">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  


	<!-- content -->
	<div class="middle-content__box">

		<!-- left column -->
		<div class="two-columns-content-wrap">
			<div class="two-columns-content-wrap__left">
				<!-- top left area -->
				<!-- latest winners slider -->
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Winner quotes</span></h4>
					</div>
					<!-- Slider -->
					<?php include "_partials/community/community-latest-winners-slider.php"; ?>
				</section>
				<!-- blog area -->
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Winner Stories</span></h4>
					</div>
					<?php edit($this->controller,'blog'); ?>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],1); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],3); ?></div>
				</section>
			</div>
			<!--  /left column -->
			<!-- right column -->
			<div class="two-columns-content-wrap__right">
				<?php if (config("RealDevice") != 3) { ?>
				<?php edit($this->controller,'blog-side-content'); ?>
				<!-- repeatable content -->
				<?php @$this->repeatData($this->content['blog-side-content']);?>
				<!-- /repeatable content -->
				
				<?php } ?>
			</div>
			<!-- /right column -->
		</div>
	</div>
</div>
</div>
<!-- /single post main content -->	<!-- /content  -->
