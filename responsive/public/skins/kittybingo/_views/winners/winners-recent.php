<!-- two columns content wrap -->
<div class="two-columns-content-wrap community-article-content">
	
	<!-- left column wrap -->
	<div class="two-columns-content-wrap__left">

			<!-- winners menu -->
	<?php include '_partials/games-menu/winners-menu.php'; ?>
		
		<!-- title -->
		<div class="section-left__title">
			<h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
		</div>
		<!-- content images -->
		<div class="article-content__img__wrapper">
			<!-- <a data-hijack="true" data-bind="click: playBingo"> -->
			<!-- <img src="<?php //echo $this->content[$this->action][0]['Image']?>" alt="<?phpecho $this->content[$this->action][0]['//Alt']?>"> -->
		<!-- 	</a> -->
		</div>

		<!-- CONTENT -->
<!-- 		<?php edit($this->controller,$this->action); ?> -->

		<article class="article">
			<div class="article__content">
			

<div class="ninety-ball-winner">
<span class="title">YESTERDAY’S 90 BALL WINNINGS</span>
<div class="prize">
<span class="prize_ammount">£5,242.84</span>
</div></div>

<div class="seventy-five-ball-winner">
<span class="title">YESTERDAY’S 75 BALL WINNINGS</span>
<div class="prize">
	<span class="prize_ammount">£5,242.84</span>
</div>
</div>

<div class="instant-game-winnings">
<span class="title">YESTERDAY’S INSTANT GAME WINNINGS</span>
<div class="prize">
	<span class="prize_ammount">£5,242.84</span>
</div>
</div>



<div class="bingo_winner">
<p class="bingo_winner-top-table">YESTERDAY’S 90 BALL WINNINGS</p>
<table class="nonresponsive">
  <thead class="bingo_winner-head">
    <tr >
      <th class="header-tble">Alias</th>
      <th class="header-tble">Winnings</th>
    </tr>
  </thead>
  <tbody>
    <tr >
      <td class="odd-row">
  <!--     <img src="../_images/common/recent-winner-avatar.svg"> -->
      SC</td>
      <td class="odd-row">£991,00</td>
    </tr>
    <tr >
      <td class="on-row">

      SC</td>
      <td class="on-row">£991,00</td>
    </tr>
    <tr >
      <td class="odd-row">SC</td>
      <td class="odd-row">£991,00</td>
    </tr>
     <tr >
      <td class="on-row">SC</td>
      <td class="on-row">£991,00</td>

    </tr>
    <tr >
      <td class="odd-row">SC</td>
      <td class="odd-row">£991,00</td>
    </tr>
    <tr >
      <td class="on-row">SC</td>
      <td class="on-row">£991,00</td>
    </tr>
  </tbody>
</table>
  <div class="bottom_table"></div>
</div>



<div class="popular-games">
<p class="popular-games-top-table">YESTERDAY’S POPULAR GAMES</p>
<table class="nonresponsive">
  <thead class="popular-games-head">
    <tr >
      <th class="header-tble">Game</th>
    </tr>
  </thead>
  <tbody>
    <tr >
      <td class="popular-games_odd-row">Content Goes Here</td>

    </tr>
    <tr >
      <td class="popular-games_on-row">Content Goes Here</td>

    </tr>
    <tr >
      <td class="popular-games_odd-row">Content Goes Here</td>

    </tr>
     <tr >
      <td class="popular-games_on-row">Content Goes Here</td>


    </tr>
    <tr >
      <td class="popular-games_odd-row">Content Goes Here</td>

    </tr>
    <tr >
      <td class="popular-games_on-row">Content Goes Here</td>

    </tr>
  </tbody>

</table>
  <span class="bottom_table"></span>
</div> 




<div class="instant-games">
<p class="instant-games-top-table">TOP INSTANT GAME WIN</p>
<table class="nonresponsive">
  <thead class="instant-games-head">
    <tr >
      <th class="header-tble">Game</th>
      <th class="header-tble">Winning</th>
      <th class="header-tble">Alias</th>
    </tr>
  </thead>
  <tbody>
    <tr >
    <td class="instant-games_odd-row">Content Goes Here</td>
    <td class="instant-games_odd-row">£991,00</td>
	<td class="instant-games_odd-row">SC</td>
    </tr>

    <tr >
      <td class="instant-games_on-row">Content Goes Here</td>
       <td class="instant-games_on-row">£991,00</td>
        <td class="instant-games_on-row">SC</td>

    </tr>
    <tr >
    <td class="instant-games_odd-row">Content Goes Here</td>
    <td class="instant-games_odd-row">£991,00</td>
	<td class="instant-games_odd-row">SC</td>

    </tr>
     <tr >
      <td class="instant-games_on-row">Content Goes Here</td>
       <td class="instant-games_on-row">£991,00</td>
        <td class="instant-games_on-row">SC</td>


    </tr>
    <tr >
    <td class="instant-games_odd-row">Content Goes Here</td>
    <td class="instant-games_odd-row">£991,00</td>
	<td class="instant-games_odd-row">SC</td>

    </tr>
    <tr >
      <td class="instant-games_on-row">Content Goes Here</td>
       <td class="instant-games_on-row">£991,00</td>
        <td class="instant-games_on-row">SC</td>
    </tr>
  </tbody>
</table>
  <span class="bottom_table"></span>
</div> 















					
			</div>
			<!-- Author Block -->
		</article>
		<!-- /POST CONTENT -->
	</div>
	<!-- /left column wrap -->

	<!-- right column wrap -->
	<?php if (config("RealDevice") != 3) { ?>
	<div class="two-columns-content-wrap__right">
		
		<!-- content -->
		<?php edit($this->controller,'community-article__side-content'); ?>
		<div class="content-holder">
			<?php @$this->repeatData($this->content['community-article__side-content']); ?>
		</div>
	</div>
	<!-- /right column wrap -->

	<?php } ?>
</div>
<!--/two columns content wrap -->