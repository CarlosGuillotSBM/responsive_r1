<!-- MAIN CONTENT AREA -->
<div class="main-content archive-content">

  <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
  </div>

  <!-- CONTENT -->
  <div class="content-holder">
    <div class="content-holder__inner">

    <?php edit($this->controller,'archive'); ?>
    <?php @$this->repeatData($this->content['archive']);?>
    <a href='/archive/promotions/'>Promotions Archive</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href='/archive/winners/'>Winners Archive</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href='/archive/blogs/'>Blog Archive</a>

    </div>
  </div>

</div>


