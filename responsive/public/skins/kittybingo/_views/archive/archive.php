<!-- MAIN CONTENT AREA -->
<div class="main-content archive-content">

  <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
  </div>

  <?php //echo $this->controller; ?>
  <?php //edit($this->controller,'archive'); ?>  

  <!-- CONTENT -->
  <div class="content-holder">
    <div class="content-holder__inner">

        <!-- Archive Page Content -->
        <div class="archive-content__box">

          <a href="/archive/promotions/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic">
              </div>
              <h4>PROMOTIONS ARCHIVE</h4>
            </div>
          </a>
        </div>
        <div class="archive-content__box">
          <a href="/archive/blog/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic"></div>
              <h4>BLOG ARCHIVE</h4>
            </div>
          </a>
        </div>
        <div class="archive-content__box">
          <a href="/archive/winners/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic"></div>
              <h4>WINNERS ARCHIVE</h4>
            </div>
          </a>
        </div>
        <div class="archive-content__box">
          <a href="/archive/games/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic"></div>
              <h4>GAMES ARCHIVE</h4>
            </div>
          </a>
        </div>

      </div>
      <!-- End Archive Page Content -->

    </div>
  </div>


<!-- /MAIN CONTENT AREA -->
</div>
