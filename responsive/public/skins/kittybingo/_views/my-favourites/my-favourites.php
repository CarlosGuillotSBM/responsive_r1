<div class="main-content my-favourites">

  <!-- Title -->
  <div class="section-left__title">
    <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
  </div>

  <!-- wrap -->
  <section class="content-holder">

      <!-- inner wrap -->
      <div class="content-holder__inner favourites">

            <?php include "_global-library/_editor-partials/favourite-game-widget.php" ?>

      </div>
      <!-- inner wrap -->
  
  </section>
  <!-- /wrap -->

</div>