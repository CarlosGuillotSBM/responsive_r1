<!--games content -->
<div class="main-content games-content">

	<!-- banner -->	
	<div class="games-content__banner ui-scheduled-content-container">
		<!-- Edit point Banner -->
		<?php edit($this->controller,'games-latest-offer'); ?>
		<?php @$this->repeatData($this->content['games-latest-offer']); ?>
	</div>
	<!-- games menu -->
	<?php include '_partials/games-menu/games-menu.php'; ?>

	<!-- games list -->
	<section class="section middle-content__box games-section">
		<?php include '_global-library/partials/games/games-list.php'; ?>
	</section>

	<!-- seo -->
	<section class="section middle-content__box games-section">
		<?php
			if ($this->action != "slots") {
				edit($this->controller.$this->action,'footer-seo', $this->action);
				@$this->getPartial($this->content['footer-seo'], 1); 
			} else {
				edit($this->controller,'footer-seo',$this->action);
				@$this->getPartial($this->content['footer-seo'],$this->action); 
			}
		?>
	</section>
	<!-- BOTTOM CONTENT -->
</div>