<?php
$cmaFriendly = @$this->row['CMAFriendly'] ? $this->row['CMAFriendly'] : 0;
$gameProvider = @$this->row['GameProviderID'] ? $this->row['GameProviderID'] : 0;
?>
<script type="application/javascript">
var sbm = sbm || {};
sbm.games = sbm.games || [];
sbm.games.push({
"id"    : <?php echo json_encode(@$this->row['DetailsURL']);?>,
"icon"  : "<?php echo @$this->row['Image'];?>",
"bg"    : <?php echo json_encode(@$this->row['Text1']);?>,
"title" : <?php echo json_encode(@$this->row['Title']);?>,
"type"  : "game",
"desc"  : <?php echo json_encode(@$this->row['Intro']);?>,
		"cma": <?php echo json_encode($cmaFriendly);?>,
		"provider": <?php echo json_encode($gameProvider);?>,
"thumb" : "<?php echo @$this->row['GameScreen'];?>",
"detUrl": <?php echo json_encode(@$this->row['Path']);?>
});
</script>
<!-- MAIN CONTENT AREA  -->
<div class="game-details-page main-content">
    <!-- MIDDLE CONTENT -->
    <div class="middle-content__box">
        <!-- GAMES DETAILS  -->
        <?php include '_partials/games/games-details-page.php'; ?>
        <!-- /END GAMES DETAILS  -->
    </div>
    <!-- /END MIDDLE CONTENT -->
</div>
<!-- /MAIN CONTENT AREA  -->
