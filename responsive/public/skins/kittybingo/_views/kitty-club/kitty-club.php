<div class="main-content kitty-club-content">
  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>

  <div class="content-holder">
    <div class="content-holder__inner">

      <!-- INTRO -->
      <div class="kitty-club__intro">
        <div class="content-template">
          <?php edit($this->controller,'kitty-club-intro'); ?>
          <?php @$this->repeatData($this->content['kitty-club-intro']);?>
        </div>
      </div>

      <!-- CTA -->
      <div class="kitty-club__cta">
        <!-- Play Now -->
        <a class="registercta" data-hijack="true" data-bind="visible: validSession() , click: playBingo" style="display:none;">Play Bingo</a>
        <!-- Login Register Area OUT-->
        <div class="logged-out-buttons" data-bind="visible: !validSession()" style="display:none;">
          <a class="registercta" data-hijack="true" href="/register/">Join Now</a>
        </div>
      </div>

      <!-- Swipe Left/Right Icon -->
      <div class="kitty-club__table-arrows-icon">
        <img src="/_images/common/icons/touch-icon.png" alt="Left and Right Arrows">
      </div>

      <!-- LUCKY CLUB TABLE WRAP -->
      <div class="kitty-club__table-wrap">
        <div class="kitty-club__table">
          <!-- Touch Icon Inside Table -->
          <!-- <div class="kitty-club__table-touch-icon">
            <img src="/_images/common/icons/touch-icon.png" alt="Touch">
          </div> -->
          <!-- Loyalty Icons Row -->
          <div class="kitty-club__table-row__header">
            <div class="kitty-club__table-row__title">Benefits to enjoy</div>
            <ul class="kitty-club__table-row__loyalty">
              <li><img src="/_images/common/loyalty-club/loyalty-club-newbie.png" alt="Checkmark"></li>
              <li><img src="/_images/common/loyalty-club/loyalty-club-bronze.png" alt="Checkmark"></li>
              <li><img src="/_images/common/loyalty-club/loyalty-club-silver.png" alt="Checkmark"></li>
              <li><img src="/_images/common/loyalty-club/loyalty-club-gold.png" alt="Checkmark"></li>
              <li><img src="/_images/common/loyalty-club/loyalty-club-ruby.png" alt="Checkmark"></li>
              <li><img src="/_images/common/loyalty-club/loyalty-club-emerald.png" alt="Checkmark"></li>
            </ul>
          </div>
          <!-- 1st, 2nd & 3rd Deposit Bonus Row -->
          <div class="kitty-club__table-row__even">
            <div class="kitty-club__table-row__title">1st, 2nd &amp; 3rd Deposit Bonus</div>
            <ul class="kitty-club__table-row__loyalty">
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
            </ul>
          </div>
          <!-- Re-Deposit Bonus Row -->
          <div class="kitty-club__table-row">
            <div class="kitty-club__table-row__title">Re-Deposit Bonus</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>25% or 50%</li>
              <li>25% or 50% </li>
              <li>25% or 50%</li>

              <li>25% or 50%</li>
              <li>25% or 50%</li>
            </ul>
          </div>
          <!-- CashBack Row -->
          <div class="kitty-club__table-row__even">
            <div class="kitty-club__table-row__title">CashBack</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>12%</li>
              <li>15%</li>
            </ul>
          </div>
          <!-- BonusBack Row -->
          <div class="kitty-club__table-row">
            <div class="kitty-club__table-row__title">BonusBack</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>5%</li>
              <li>-</li>
              <li>-</li>
            </ul>
          </div>
          <!--Upgrade Bonus -->
          <div class="kitty-club__table-row__even">
            <div class="kitty-club__table-row__title">Upgrade Bonus</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
          <!-- Withdrawal Pending Period Row -->
          <div class="kitty-club__table-row">
            <div class="kitty-club__table-row__title">Reduced Withdrawal Pending Period</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
          <!-- Free Bingo Row -->
          <div class="kitty-club__table-row__even">
            <div class="kitty-club__table-row__title">Free Bingo</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
          <!-- Free Bingo £50 a day Row -->
          <div class="kitty-club__table-row">
            <div class="kitty-club__table-row__title">Free Bingo £50 a day</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
          <!-- Free Bingo £100 a week Row -->
          <div class="kitty-club__table-row__even">
            <div class="kitty-club__table-row__title">Free Bingo £100 a week</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
          <!-- Free Bingo £500 a month Row -->
          <div class="kitty-club__table-row">
            <div class="kitty-club__table-row__title">Free Bingo £500 a month</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
          <!-- Win a iPad Row -->
          <div class="kitty-club__table-row__even">
            <div class="kitty-club__table-row__title">Exclusive VIP &pound;500 Cash Bingo Game</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
          <!-- END Win a iPad Row -->
          <!-- Dedicated VIP Manager -->
          <div class="kitty-club__table-row">
            <div class="kitty-club__table-row__title">Dedicated VIP Manager</div>
            <ul class="kitty-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>
           <!-- END Dedicated VIP Manager -->
        </div>
      </div>
      
      
        </div>
      </div>
      <!-- /LUCKY CLUB TABLE WRAP -->
    
      <!-- LOYALTY POINTS TABLES -->
      <div class="content-holder">
        <?php edit($this->controller,'kitty-club-points'); ?>
        <?php @$this->repeatData($this->content['kitty-club-points']);?>
      </div>

      <!-- Terms &amp; Conditions -->
       <div class="content-holder">
         <?php edit($this->controller,'kitty-club-tcs'); ?>
         <dl class="kitty-club__terms-conditions accordion" data-accordion="">
          <dd class="accordion-navigation active">
            <a href="#panelTC">Terms &amp; Conditions</a>
            <div id="panelTC" class="content active">
              <?php @$this->repeatData($this->content['kitty-club-tcs']);?>
            </div>
          </dd>
        </dl>
      </div>
      <!-- / Terms &amp; Conditions -->

    </div>

  </div>
</div>
<!-- END MIDDLE CONTENT  -->