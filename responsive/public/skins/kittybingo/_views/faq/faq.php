
<div class="two-columns-content-wrap help-content">

  <!-- left column wrap -->
  <div class="two-columns-content-wrap__left">

 <!--  <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div> 
  
      <!-- TWO COLUMNS LAYOUT -->
    <div class="two-columns-content-wrap__left__title">
      
      <!-- LEFT COLUMN -->
      <div class="two-columns-content-wrap__left">
      <article class="text-partial">
        <!-- WRAP CONTENT LEFT -->
        <div class="two-columns-content-wrap__left__inner" itemscope itemtype="http://schema.org/SoftwareApplication">

        <!-- ACCORDION LIST -->
        <?php edit($this->controller,'faq-details'); ?>
        <ul class="accordion accordion-wrap">
          <?php @$this->repeatData($this->content['faq-details'], 1, "_global-library/partials/common/text-accordion.php");?>
        </ul>

        </div>
         </article>
    </div>
    <!-- /TWO COLUMNS LAYOUT -->
    </div>
  </div>

    <?php if (config("RealDevice") != 3) { ?>

      <!-- RIGHT COLUMN -->
      <div class="two-columns-content-wrap__right">
        <?php edit('faq','faq__side-content',$this->subaction); ?>
        <?php @$this->repeatData($this->content['faq__side-content']); ?>
        <div class="clearfix"></div>
      </div>

      <?php } ?>

</div>
<!--/two columns content wrap -->




