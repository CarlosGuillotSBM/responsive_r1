<script type="application/javascript">
     skinModules.push( { id: "ClaimCashback" } );
     skinModules.push( { id: "ClaimUpgradeBonus" } );
</script>
<script type="application/javascript">
    skinModules.push( { id: "AccountActivity" } );
    skinModules.push( { id: "PersonalDetails" } );
    skinModules.push( { id: "PersonalDetailsNew" } );
    skinModules.push( { id: "PlayerAccount" } );
    skinModules.push( { id: "ConvertPoints", options: { name: "Loyalty Points", url: "/kitty-club/" } });
    skinModules.push( { id: "PlayerReferAFriend" } );
</script>

<?php

/**
 * Include the different partials depending on the device
 * This page is no responsive and some tablets need to have both views
 * because they show the phone view in portrait and desktop oin landscape
 */

if (config("RealDevice") == 3) {
    // phone
    include '_views/home/home-mobile.php';
    include '_global-library/partials/cma/notification-container-mobile.php';
} else {
    include'_views/home/home-mobile.php';
    include 'start-desktop.php';
}

// dont miss out popup
include "_global-library/partials/modals/dont-miss-out.php";