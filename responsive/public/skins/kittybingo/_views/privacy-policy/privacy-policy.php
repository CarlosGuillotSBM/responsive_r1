
<div class="main-content two-columns-content-wrap privacy-policy-content">

 
  <!-- left column wrap -->
  <div class="two-columns-content-wrap__left">

      <!-- title -->
      <div class="section-left__title">
      <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
      </div>


      <div class="content-holder">
        <?php edit($this->controller,'privacy-policy__content',$this->subaction); ?>
        <?php @$this->repeatData($this->content['privacy-policy__content']); ?>

        <!-- accordion content -->
        <?php edit($this->controller,'privacy-policy-details'); ?>
        <ul class="accordion accordion-wrap">
        <?php @$this->repeatData($this->content['privacy-policy-details'], 1, "_global-library/partials/common/text-accordion.php");?>    
        </ul>

        </div>
      </div>

      <!-- right column wrap -->
  <?php if (config("RealDevice") != 3) { ?>
  <div class="two-columns-content-wrap__right">
    
    <!-- content -->
    <div class="content-holder">
     <?php edit($this->controller,'privacy-policy__side-content'); ?>
      <?php @$this->repeatData($this->content['privacy-policy__side-content']); ?>
    </div>
          
  </div>
  <?php } ?>
  </div>

</div>
<!--/two columns content wrap -->