<!-- wrapper -->
<div class="main-content loginpage">
	<!-- title -->
	<div class="section-left__title" style="display: none;">
		<h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
	</div>

	<!-- CONTENT -->
	<div class="login-box--loginpage">
		<!-- LOGIN BOX -->
		<article class="login-box">
			<div class="login-box__form-wrapper">
				<h1>login</h1>
				<!-- FORM -->
				<form class="login-box__form" id="ui-login-form">
					<div class="login-box__input-wrapper">
						<div class="cat-avatar"></div>
						<div class="login-box__username">
							<input id="login_name" name="Username" placeholder="Username" type="text" placeholder="User Name" data-bind="value: LoginBox.username, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" />
							<img src="/_images/common/icons/login-username.svg">
						</div>
						<span style="display: none" data-bind="visible: LoginBox.invalidUsername" class="error">This field is required.</span>
					</div>
					<div class="login-box__input-wrapper">
						<div class="login-box__password">
							<input name="Password" id="login_password" placeholder="Password" type="password" placeholder="Password" data-bind="attr:{type: LoginBox.passwordShowText() == 'Hide' ? 'text': 'password'}, value: LoginBox.password, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}"/>

							<span class="login-password-img"><img src="/_images/common/icons/login-password.svg"></span>
							<div class="login-box__show-password" style="margin-top: 9px;" data-bind="click: LoginBox.togglePassword">
								<a class="right" data-bind="text: LoginBox.passwordShowText">Show</a>
							</div>
							<span style="display: none" data-bind="visible: LoginBox.invalidPassword" class="error">This field is required.</span>
						</div>


					</div>
					<a style="display: none" data-bind="visible: LoginBox.lockedUser, text: LoginBox.loginError" class="error" href="/help/"></a>
				<!-- ko ifnot: LoginBox.lockedUser -->
					<span style="display: none" data-bind="visible: LoginBox.loginError, html: LoginBox.loginError" class="error"></span>
				<!-- /ko -->
					<a href="/forgot-password/" class="login-box__forgot-password">Forgot your password?</a>

					<a class="cta-login-header" data-bind="click: LoginBox.doLogin.bind($data, GameLauncher.gameId(), GameLauncher.gamePosition(), GameLauncher.gameContainerKey(), GameLauncher.roomId())">LOGIN</a>

					<a class="registercta" href="/register/" data-hijack="true" >Join Now</a>
					</form>
					<!-- / FORM -->
				</div>
		</article>

		<div class="login-banner">
			<?php edit($this->controller,'login',$this->action); ?>
			<?php @$this->repeatData($this->content[$this->controller],1,''); ?>
		</div>

			<div class="login-terms">
          <!-- Accordion -->
        <?php edit($this->controller,'Terms-and-coditions-login'.$this->action);?>

        <dl id="ui-promo-tcs" class="accordion accordion_login" data-accordion>

          <dd class="accordion-navigation active">

            <a href="#panelTC" class="accordion-navigation__toggle accordion_login-terms">Terms & Conditions</a>

            <div id="panelTC" class="content active">
              <?php @$this->getPartial($this->content['Terms-and-coditions-login'.$this->action],1); ?>
            </div>
          </dd>
        </dl>
        <!-- /Accordion -->
	</div>
	</div>


</div>
