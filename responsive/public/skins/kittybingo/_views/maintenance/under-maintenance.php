<!DOCTYPE html>
<html>
<head>
	<title>Under Maintenance</title>
<style type="text/css">
	body{
		background-color:white;
		color:#6b57c8;
		text-align: center;
		font-size: 20px;
		font-family: arial;
	}
	h1{
		font-size: 40px;
		color: #f91a8b;
		margin-bottom: -10px;		
	}
	.logo{
		width: 180px;
		margin-top: 30px;
	}
	p{
		font-size: 14px;
	}
</style>
</head>
<body>
<br>
<br><br><br><br><br><br><br>

<div>
<img class="logo" src="/_images/logo/logo-sml.png"><br><br> 
Upgrade in progress – we’ll be right back!<br>Kitty Bingo will be unavailable while we make upgrades to improve your playing experience.
<br>
<br>
We are sorry for any inconvenience this may cause and promise to be back soon, better than ever! 
<br><br>
The Kitty Team
</div>
</body>
</html>