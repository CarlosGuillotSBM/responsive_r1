<div class="main-content error-page-content">

  <!-- Title -->
  <div class="section-left__title">
    <h4>Page not found</h4>
  </div>

  <!-- wrap -->
  <section class="content-holder">

	  <!-- inner wrap -->
	  <div class="content-holder__inner">

		<!-- image -->
		<div class="error-page__image">
			<img src="/_images/switch-old-to-new/Cat-error404.png">
		</div>

		<!-- content -->
		<div class="error-page__text">
		
			<h1 class="error-page__error">Oops!</h1>

			<p class="error-page__msg">
			We can’t seem to find the page you’re looking for.</p>

			<p>Error code: 404</p>

			<p class="error-page__help-links">Here are some helpful links for you:</p>

			<!-- links -->
			<ul>
				<li><a href="/" data-hijack="true">Home</a></li>
				<li data-bind="visible: validSession, click: playBingo" ><a href="/bingo/">Play Bingo</a></li>
				<li><a href="/slots/" data-hijack="true">Slots &amp; Games</a></li>
				<li><a href="/promotions/" data-hijack="true">Promotions</a></li>
				<li><a href="/help/" data-hijack="true">Help</a></li>
			</ul>

		</div>
		<!-- /content -->

	  </div>
	  <!-- inner wrap -->
  
  </section>
  <!-- /wrap -->

</div>