<div class="main-content forgot-password-content">

  <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h4><span>Forgot Password</span></h4>
  </div>
  
  <!-- CONTENT WRAP -->
  <section class="text-partial">
    <div class="text-partial-content">


      <!-- CONTENT WRAP PASSWORD -->
      <section class="forgot-password-content__wrap">

              <h1>Forgot your password?</h1>
              <?php include '_partials/common/forgot-password.php'; ?>

      </section>

    </div>
  </section>
  <!-- / CONTENT WRAP -->

</div>
<!-- END MIDDLE CONTENT -->