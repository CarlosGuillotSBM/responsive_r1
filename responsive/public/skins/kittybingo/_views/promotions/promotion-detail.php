<script type="application/javascript">
skinModules.push({
id: "OptIn",
options: {
modalSelectorWithoutDetails: "#custom-modal",
details : true
}
});

  skinModules.push({
    id: "ContentScheduler",
    options: {
      showAllElements: true
    }
  });

</script>

<!-- two columns content wrap -->
<div class="two-columns-content-wrap promotions-detail-content">

  <!-- left column wrap -->
  <div class="two-columns-content-wrap__left">
    <?php edit($this->controller,'promotions'); ?>

    <!-- title -->
<!--     <div class="section-left__title">
      <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
    </div> -->

<!--         <div class="section__header section-full-width__title">
         
         <h1 class="section-left__title"><?php echo $this->content["Title"];?></h1>
        </div> -->

    <!-- content images -->
    <div class="promotion__content__img__wrapper">
      <a data-hijack="true" data-bind="click: playBingo">
        <img src="<?php echo $this->content["Text8"] != '' ? $this->content["Text8"] : $this->content["Image"]?>" alt="<?php //echo $this->content["Alt"]?>">
      </a>
    </div>

    <!-- BOTTOM CTA'S -->
    <div class="promo__container__row">
      <a data-hijack="true" style="display: none" class="promo__container__button cta-opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>
      
      <!-- span to be removed by javascript - DO NOT REMOVE!! -->
      <span></span>
      <?php if($this->content['Text4'] === '') {?>

      <!-- PLAY NOW -->
      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Join Now'; else echo $this->content['Text3']; ?></a>
      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
      <?php } else { ?>

      <!-- PLAY NOW -->
      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Join Now'; else echo $this->content['Text3']; ?></a>
      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
      <?php }?>
    </div>




      <!-- CONTENT -->
      <div class="promo__container__content">
        <?php echo $this->content["Body"]; ?>

<div class="promo__container__row">
     <?php if($this->content['Text4'] === '') {?>

      <!-- PLAY NOW -->
      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Join Now'; else echo $this->content['Text3']; ?></a>
      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
      <?php } else { ?>

      <!-- PLAY NOW -->
      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Join Now'; else echo $this->content['Text3']; ?></a>
      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
      <?php }?>
</div>
      
              <!-- Accordion -->
      <dl id="ui-promo-tcs" class="promotion__terms-conditions accordion" data-accordion>
        <dd class="accordion-navigation active">
          <a href="#panelTC" class="accordion-navigation__toggle">Terms &amp; Conditions</a>
          <div id="panelTC" class="content active">
            <?php echo @$this->content["Terms"];?>
          </div>
        </dd>
      </dl>
      <!-- /Accordion -->

</div>
    </div>


    <!-- /left column wrap -->

     <!-- right column wrap -->
    <?php if (config("RealDevice") != 3) { ?>
    <div class="two-columns-content-wrap__right ui-scheduled-content-container">
      
      <!-- content -->
      <div class="content-holder ui-scheduled-content-container">
        <?php edit($this->controller,'promotion-details__side-content',$this->action); ?>
        <?php @$this->repeatData(@$this->sideBanners); ?>
      </div>
      
    </div>

     <?php } ?>
    <!-- /right column wrap -->


      <!-- RELATED PROMOS -->
    <div class="middle-content__box related-promos">
      <!-- Title -->
      <h1 class="section-left__title">Related Promos</h1>
      <!-- /Title -->
      <div class="related-promos__wrap">
        <!-- promos -->
        <?php @$this->getPartial($this->promotions['promotions'],2, "_global-library/partials/promotion/promotion.php"); ?>
        <?php @$this->getPartial($this->promotions['promotions'],3, "_global-library/partials/promotion/promotion.php"); ?>
        <?php @$this->getPartial($this->promotions['promotions'],4, "_global-library/partials/promotion/promotion.php"); ?>
      </div>

      <!-- related promos slider on mobile -->
      <?php include "_partials/promos/related-promos-slider.php"; ?>

    </div>
    <!-- /RELATED PROMOS -->

      <!-- PROMO FOOTER NAV MOBILE -->
      <div class="footer-nav__container">
          <a data-hijack="true" style="display: none" data-tournament="<?php echo $this->content['DetailsURL'] ?>" class="cta-opt-in">OPT-IN</a>
          <?php if($this->content['Text4'] === '') {?>
            <!-- PLAY NOW -->
            <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
            <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
          <?php } else { ?>
            <!-- PLAY NOW -->
            <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
            <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
          <?php }?>                     
          <a data-hijack="true" href="/promotions/<?php echo $this->getNextPromoURL($this->content["DetailsURL"]); ?>" class="cta-next-promo">NEXT PROMO ></a>
      </div>

   

   
</div>
<!--/two columns content wrap -->