<div class="main-content promotions-content">
  <script type="application/javascript">   
      skinModules.push({
          id: "RadFilter"
      });
  </script>
  <!-- Title -->
  <div class="section-left__title">
    <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
  </div>

  <!-- wrap -->
  <section class="promotions">

      <!-- DESKTOP PROMOTIONS CATEGORY MENU -->
      <dl class="tabs" data-tab>
        <dd class="first active"><a id="ui-activity-tab" href="#promotions-2">Today</a></dd>
        <dd><a id="ui-activity-tab" href="#promotions-3">Tomorrow</a></dd>
        <dd><a id="ui-activity-tab" href="#promotions-4">Weekend</a></dd>
        <dd class="newbie"><a id="ui-activity-tab" href="#promotions-5">Newbie</a></dd>
        <dd><a id="ui-activity-tab" href="#promotions-6">Specials</a></dd>
        <dd><a id="ui-activity-tab"  href="#promotions-0">All Promos</a></dd>
        <!-- <dd><a class="last" href="#promotions-7">Past Promos</a></dd> -->
      </dl>

      <!-- MOBILE PROMOTIONS CATEGORY MENU -->
      <div class="mobile-tabs-menu__wrap">
       <a href="#" data-dropdown="id02" data-options="is_hover:false" class="button dropdown mobile-tabs-menu__top">Today</a>
        <div id="id02" data-dropdown-content class="f-dropdown mobile-tabs-menu__dropdown">
          <dl class="tabs show-for-small-only" data-tab>
            
            <dd class="first active"><a href="#promotions-2">Today</a></dd>
            <dd><a href="#promotions-3">Tomorrow</a></dd>
            <dd><a href="#promotions-4">Weekend</a></dd>
            <dd class="newbie"><a href="#promotions-5">Newbie</a></dd>
            <dd><a href="#promotions-6">Specials</a></dd>
            <dd><a href="#promotions-0">All Promos</a></dd>
<!--             <dd><a href="#promotions-7">Past Promos</a></dd> -->

          </dl>
        </div>
      </div>
      <!-- / MOBILE PROMOTIONS CATEGORY MENU -->
      
      <div class="content-holder">
        <div class="tabs-content">

          <!-- PROMOTIONS CATEGORY TABS -->
          <?php edit($this->controller,'promotions'); ?>

          <!-- ALL -->
          <div class="content" id="promotions-0">
            <div class="content-holder__inner">
              <?php //@$this->repeatDataWithFilter($this->promotions,'ALL',1, "_partials/promos/promo.php"); ?>
              <?php $variables = array(); 
                $variables["WhichTab"] = 1; ?>
              <?php $this->repeatData($this->content["promotions"],1,"_global-library/partials/promotion/promotion.php",$variables); ?>
            </div>
          </div>

          <!-- Today -->
          <div class="content active" id="promotions-2">
            <div class="content-holder__inner">
              <?php $variables["WhichTab"] = 2; ?>
              <?php @$this->repeatDataWithFilter($this->content["promotions"],getToday().',ALL',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
            </div>
          </div>

          <!-- Tomorrow -->
          <div class="content" id="promotions-3">
            <div class="content-holder__inner">
              <?php $variables["WhichTab"] = 3; ?>
              <?php @$this->repeatDataWithFilter($this->content["promotions"],getTomorrow().',ALL',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
            </div>
          </div>

          <!-- Weekend -->
          <div class="content" id="promotions-4">
            <div class="content-holder__inner">
              <?php $variables["WhichTab"] = 4; ?>
              <?php @$this->repeatDataWithFilter($this->content["promotions"],'FRI,SAT,SUN,ALL',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
            </div>
          </div>

          <!-- Newbie -->
          <div class="content" id="promotions-5">
            <div class="content-holder__inner">
              <?php $variables["WhichTab"] = 5; ?>
              <?php @$this->repeatDataWithFilter($this->content["promotions"],'NEWBIE',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
            </div>
          </div>

          <!-- Specials -->
          <div class="content" id="promotions-6">
            <div class="content-holder__inner">
              <?php $variables["WhichTab"] = 6; ?>
              <?php @$this->repeatDataWithFilter($this->content["promotions"],'SPECIALS',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
            </div>
          </div>

          <!-- Past Promos -->
          <div class="content" id="promotions-7">
            <div class="content-holder__inner">
              <?php @$this->repeatDataWithFilter($this->pastPromos,7,1, "_partials/promos/promo.php"); ?>
            </div>
          </div>

          <a class="promotional-tcs" href="/terms-and-conditions/general-promotional-terms-and-conditions/">General Promotional Terms &amp; Conditions</a>

        </div>
        <!-- END TABS CONTENT -->
      </div>
  
  </section>
  <!-- /wrap -->

  <!--  PROMOTIONS SEO CONTENT-->
  <section class="content-holder">
      <div class="content-holder__inner">
      <?php edit($this->controller,'promotions-seo-content'); ?>
      <?php @$this->getPartial($this->content['promotions-seo-content'],1); ?>
      </div>
  </section>

</div>