<!-- two columns content wrap -->
<div class="two-columns-content-wrap about-us-content">

  <!-- left column wrap -->
  <div class="two-columns-content-wrap__left">

      <!-- title -->
      <div class="section-left__title">
        <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
      </div>

      <!-- content -->
      <?php edit($this->controller,'about-us'); ?>
      <?php $this->repeatData(@$this->content['about-us']);?>

  </div>


  <!-- right column wrap -->
  <?php if (config("RealDevice") != 3) { ?>
  <div class="two-columns-content-wrap__right">
    
    <!-- content -->
    <div class="content-holder">
     <?php edit($this->controller,'About-us__side-content'); ?>
      <?php @$this->repeatData($this->content['About-us__side-content']); ?>
    </div>
          
  </div>
  <?php } ?>

</div>
<!--/two columns content wrap -->