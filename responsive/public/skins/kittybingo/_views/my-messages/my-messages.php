<script>
// Go Back Button Hardcoded by Joao
function goBack() {
    window.history.back();
}
</script>

<!-- MIDDLE CONTENT -->
<div class="main-content my-messages-mobile-content">

	<div class="section-left__title">
        <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
    </div>

    <!-- <h1 class="my-messages-mobile-content__header" onclick="goBack()"><?php //echo str_replace('-', ' ', $this->controller_url)?></h1> -->

    <div class="my-messages-mobile-content__container">
        <?php if(config("inbox-msg-on")): ?> 
        <?php  include'_global-library/partials/start/start-my-messages-section.php'; ?>
        <?php endif; ?>
    </div>

</div>
<!-- /MIDDLE CONTENT -->