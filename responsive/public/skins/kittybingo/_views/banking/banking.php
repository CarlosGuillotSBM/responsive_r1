<div class="main-content banking-content">

  <!-- Title -->
  <div class="section-left__title">
    <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
  </div>

  <!-- BANKING LIST -->
  <div class="content-holder">
    <div class="content-holder__inner">
      <?php edit($this->controller,'banking-list'); ?>
      <?php @$this->repeatData($this->content['banking-list']);?>
    </div>
  </div>

</div> 