<!-- MOBILE CASHBACK -->
<div style="display: none;" data-alert class="mobile-cashback alert-box" data-bind="visible: validSession">
    <!--<a href="#" class="close" data-bind="visible: ClaimCashback.cbAmount() > 0" >X</a>-->
    <?php include'_global-library/partials/start/start-claim-cashback.php'; ?>
</div>
<!-- / MOBILE CASHBACK -->

<!-- MOBILE UPGRADE BONUS -->
<div style="display: none;" data-alert class="mobile-cashback alert-box" data-bind="visible: validSession">
    <!--<a href="#" class="close" data-bind="visible: ClaimUpgradeBonus.ubAmount() > 0" >X</a>-->
    <?php include'_global-library/partials/start/start-claim-upgrade-bonus.php'; ?>
</div>
<!-- / MOBILE UPGRADE BONUS -->

<!-- MOBILE HOME VIEW -->
<div class="home-mobile">


    <?php //edit($this->controller,'top-banner'); ?>
    <?php edit($this->controller,'home-adaptive-banners', ''); ?>
    <!-- offer -->
    <div class="home-main-slider desktop-mobile ui-scheduled-content-container">
        <ul class="">
            <!-- Hero Area -->
            <li>
<?php @$this->repeatData($this->content['home-adaptive-banners']);?></a>
            </li>
        </ul>

    </div>
    <!--
    <div class="slider-text-link"><a href="/promotions/welcome-offer/">
        <?php
        /* edit($this->controller,'hero'); ?>
        <? @$this->getPartial($this->content['hero'],1, "_partials/banner/homepage-hero-banner.php"); ?>
            <?php include("_partials/banner/homepage-adaptive-banner.php");
        */
            ?>
    </div>
    -->

    <!-- Featured Games -->
    <?php include '_partials/home/home-featured-games/home-featured-games-holder.php'; ?>

    <!-- bingo progressives ticker -->
    <?php include'_partials/home/home-bingo-progressives/home-bingo-progressives-holder.php'; ?> 

    <!-- bingo schedule -->
    <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>

     <!-- winners ticker -->
    <?php include'_partials/home/home-winners/home-winners-holder.php'; ?>

    <!-- Featured Games Box with tabbed content -->
    <div class="home-mobile-carousels">
        <?php edit($this->controller,'home-carousels'); ?>
        <?php @$this->repeatData($this->content['home-carousels']);?>
    </div>
    <!-- SEO AREA -->
    <?php include'_partials/home/home-seo/home-seo-holder.php'; ?>

    <!-- footer navbar -->
    <?php include'_partials/nav-mobile/footer-navigation-mobile.php'; ?>

</div>
<!-- /MOBILE HOME VIEW -->

<!-- MOBILE MY ACCOUNT OVERLAY -->
<div class="account-my-bonuses-mobile">
    <!-- future US to be implemented -->
</div>

<div class="myaccount-claim-code-mobile">
    <?php include '_partials/my-account-mobile/myaccount-claim-code-mobile.php' ?>
</div>

<div class="myaccount-details-mobile">
    <?php include '_partials/my-account-mobile/myaccount-details-mobile.php' ?>
</div>

<div class="myaccount-convert-points-mobile">
    <?php include '_partials/my-account-mobile/myaccount-convert-points-mobile.php' ?>
</div>