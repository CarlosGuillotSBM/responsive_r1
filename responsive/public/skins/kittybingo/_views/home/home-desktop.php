

<div class="main-content home">
    <!-- main offer + progressive jackpots -->
    <?php  include '_partials/home/home-top/home-top-holder.php'; ?>

    <!-- Featured Games -->
    <?php include '_partials/home/home-featured-games/home-featured-games-holder.php'; ?>

    <!-- bingo progressives ticker -->
    <?php include'_partials/home/home-bingo-progressives/home-bingo-progressives-holder.php'; ?>

    <!-- Bingo Schedule -->
    <?php include'_partials/bingo-schedule/bingo-schedule-holder-home.php'; ?>

    <!-- winners ticker -->
    <?php include'_partials/home/home-winners/home-winners-holder.php'; ?>

    <!-- Promos -->
   <?php include'_partials/home/home-promos/home-promos-holder.php'; ?>

    <!-- Community -->
    <?php include'_partials/home/home-community/home-community-holder.php'; ?>

    <!-- SEO AREA -->
    <?php include'_partials/home/home-seo/home-seo-holder.php'; ?>

</div>