<!-- MIDDLE CONTENT -->
<div class="main-content">

  <section class="section">
    <!-- Title -->
    <div class="section-left__title">
      <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>
    <!-- /Title -->
    <!-- CONTENT -->
    <?php edit($this->controller,'player-verification'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['player-verification']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>  
  
</div>
<!-- END MIDDLE CONTENT -->