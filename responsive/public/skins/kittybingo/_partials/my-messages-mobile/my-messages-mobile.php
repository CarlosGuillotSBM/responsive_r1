<div class="my-messages-mobile" style="display: none" data-bind="visible: validSession">
      
    <!-- TITLE -->
    <div class="my-messages-mobile__header">My Messages</div>

    <!-- my messages section -->

    <div class="my-messages-mobile__container">
        <?php if(config("inbox-msg-on")): ?>
            <?php  include'_global-library/partials/start/start-my-messages-section.php'; ?>
        <?php endif; ?>
    </div>

    <!-- HINT -->
    <!-- <div class="my-messages-mobile__hint">Hint: Swipe a subject line left or right</div> -->

    <!-- MESSAGES HARDCODED -->
<!--     <ul>
        <li class="unread">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">You have a bonus waiting for you...</span><span class="date">Thu</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="unread">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">Summer scorcher offer for...</span><span class="date">Wed</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="unread">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">Exclusive new slots are now...</span><span class="date">Fri</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="read">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">Valentines Bingo Bonus offer has...</span><span class="date">Feb 14</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="read">
           <div class="swipe-unread">Mark as Unread</div>
           <div class="message-wrap">
                <span class="title">Gun’s and Rosed Slot Launch...</span><span class="date">Feb 4</span>
           </div>
           <div class="swipe-delete">Delete</div>
        </li>
        <li class="save">
             <div class="swipe-unread">Mark as Unread</div>
             <div class="message-wrap">
                <span class="title">Unmissable promotions up for gr...</span><span class="date">Feb 3</span>
             </div>
             <div class="swipe-delete">Delete</div>
        </li>
        <li class="delete">
             <div class="swipe-unread">Mark as Unread</div>
             <div class="message-wrap">
                <span class="title">Unmissable promotions up for gr...</span><span class="date">Feb 3</span>
             </div>
             <div class="swipe-delete">Delete</div>
        </li>
        <li class="read">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">It’s Friday so here’s a free bonus...</span><span class="date">Jan 28</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="read">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">Gun’s and Rosed Slot Launch...</span><span class="date">Jan 25</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="unread">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">You have a bonus waiting for you...</span><span class="date">Thu</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="unread">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">Summer scorcher offer for...</span><span class="date">Wed</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="unread">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">Exclusive new slots are now...</span><span class="date">Fri</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
        <li class="read">
            <div class="swipe-unread">Mark as Unread</div>
            <div class="message-wrap">
                <span class="title">Valentines Bingo Bonus offer has...</span><span class="date">Feb 14</span>
            </div>
            <div class="swipe-delete">Delete</div>
        </li>
    </ul> -->

</div>