<script type="application/javascript">
    skinPlugins.push({
        id: "Slick",
        options: {
            slideWidth: 250,
            controls: true,
            responsive: true,
            minSlides: 4,
            maxSlides: 4,
            slideMargin: 10,
            auto: false,
            infinite: true,
            element: "winners-slider",
            pager: false
        }
    });
</script>

<div class="content-holder home-winners-holder">
    <div class="content-holder__content">
        <!-- title -->
        <div class="content-holder__title">
            <h1>WINNERS</h1>
        </div>
        <!-- winners ticker -->
        <div class="content-holder__ticker ">
            <ul class="winners-slider">
                <?php //include '_partials/home/home-winners/home-winners-slide.php'; ?>
                <?php include '_partials/home/home-winners/home-winners-slide.php';?>
            </ul>
        </div>
    </div>
</div>
