<!-- winner repeater-->
<?php
    foreach($this->winners as $winner):
?>
<li>
  <!-- avatar -->
<!--   <div class="ticker-avatar">
      <img src="_images/avatar.png" alt="">
  </div>
 -->
  <!-- winner info -->
  <div class="ticker-info" data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $winner["DetailURL"]; ?>">
      <!-- slot + user -->
      <p class="ticker-slot"><?php echo ellipsis($winner["Title"],15); ?><span class="ticker-alias"><?php echo $winner["Firstname"]; ?></span></p>
  </div>
  <!-- amount -->
  <span class="ticker-amount" class="ui-latest-winner-amount"><?php echo $winner["CurrencyCode"] . $winner["Win"]; ?></span>
</li>
<?php endforeach;?>
