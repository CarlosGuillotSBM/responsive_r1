<li>
    <!-- avatar -->
    <div class="ticker-avatar">
        <img src="/_images/common/icons/star.svg" alt="">
    </div>
    <!-- winner info -->
    <div class="ticker-info" data-bind="click: validSession ? playBingo : null">
        <!-- slot -->
        <p class="ticker-slot"><?php echo ellipsis($row["Title"], 15); ?></p>
    </div>
    <!-- amount -->
    <span class="ticker-amount ui-prog-jackpot-amount" data-amount="<?php echo $this->getProgressiveByName(@$row['Text2']);?>">...</span>
</li>
