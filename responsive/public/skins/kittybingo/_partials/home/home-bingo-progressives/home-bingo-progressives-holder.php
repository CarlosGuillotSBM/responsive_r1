<script type="application/javascript">
    skinPlugins.push({
        id: "Slick",
        options: {
            slideWidth: 250,
            controls: true,
            responsive: true,
            minSlides: 4,
            maxSlides: 4,
            slideMargin: 10,
            auto: false,
            infinite: true,
            element: "bingo-progressives-slider",
            pager: false
        }
    });
</script>

<?php if (config("RealDevice") != 1): ?>
<script type="application/javascript">
    skinPlugins.push({
        id: "ProgressiveSlider"
    });
</script>
<?php endif; ?>


<div class="content-holder home-bingo-progressives-holder">
	<div class="content-holder__content">
        <!-- title -->
        <div class="content-holder__title">
            <h1>BINGO PROGRESSIVES</h1>
        </div>

        <!-- bingo-progressives ticker -->
        <div class="content-holder__ticker">
            <?php edit($this->controller,'bingo-progressives'); ?>
            <ul class="bingo-progressives-slider progressive-jackpots">
                <?php @$this->repeatData($this->content['bingo-progressives'], 0, "_partials/home/home-bingo-progressives/home-bingo-progressives-slide.php");?>
            </ul>
        </div>
	</div>
</div>
