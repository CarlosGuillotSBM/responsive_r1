    <!-- main content top -->
    <div class="content-holder main-content__top desktop-slider">

        <div class="content-holder__content">

            <!-- content left -->
            <div class="content-holder__content_left">
                
                <div class="content-holder__inner">
                    <?php include "_partials/home/home-top/home-main-offer.php"; ?>
                </div>
                
            </div>


            <!-- content right -->
            <div class="content-holder__content_right">
                
                <!-- title -->
                <div class="content-holder__title">
                    <h1>SLOTS PROGRESSIVES</h1>
                    <!-- content holder title right link -->
                    <?php
                        $titleHeaderHeight = 25;
                        $slideHeight = 95;
                        $slidesToShow = 2;
                    ?>
                    <script type="application/javascript">
                    skinPlugins.push({
                            id: "ProgressiveSlider",
                            options: {
                            slidesToShow: <?php echo $slidesToShow ?>,
                            mode: "vertical"
                        }
                    });
                    </script>
                </div>

                <div class="content-holder__inner progressive-jackpot-wrap">
                    
                    <?php edit($this->controller,'home-progressives'); ?>
                    <div class="progressive-jackpots__wrapper">
                        <div class="progressive-jackpots">
                            <?php
                            if(config("RealDevice") < 3) {
                                @$this->repeatData($this->content['home-progressives'],1);
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /main content top -->