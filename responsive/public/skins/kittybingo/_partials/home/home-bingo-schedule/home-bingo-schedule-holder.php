<script type="application/javascript">
skinModules.push({id: "BingoScheduler"});
</script>

<div class="content-holder bingo-schedule">
	<div class="content-holder__content">
		<div class="content-holder__content_full-width">
			
			<!-- title row -->
			<div class="content-holder__title">

				<!-- title -->
				<h1>BINGO SCHEDULE</h1>

	            <!-- Bingo Category Tabs -->
	            <dl class="bingo-schedule__tabs">
	                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'ALL'), css: { active: BingoScheduler.filter() == 'ALL'}"><a >All</a></dd>
	                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'90') , css: { active: BingoScheduler.filter() == '90'}"><a>Bingo 90</a></dd>
	                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'75') , css: { active: BingoScheduler.filter() == '75'}"><a>Bingo 75</a></dd>
	                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'5L') , css: { active: BingoScheduler.filter() == '5L'}"><a>Bingo 5L</a></dd>
	                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'PRE'), css: { active: BingoScheduler.filter() == 'PRE'}"><a>Prebuys</a></dd>
	            </dl>

			</div>
			
			<!-- bingo rooms caroussels -->
			<div class="content-holder__inner">

					<div class="bingo-schedule-mobile">

		                <!-- ALL -->
		                <ul style="display: none" data-bind="foreach: BingoScheduler.openedRooms, visible: BingoScheduler.filter() == 'ALL'">
		                  <?php include "_global-library/partials/bingo/bingo-room-slide.php"; ?>
		                </ul>

		                <!-- 90 -->
		                <ul style="display: none" data-bind="foreach: BingoScheduler.rooms90, visible: BingoScheduler.filter() == '90'">
		                  <?php include "_global-library/partials/bingo/bingo-room-slide.php"; ?>
		                </ul>

		                <!-- 75 -->
		                <ul style="display: none" data-bind="foreach: BingoScheduler.rooms75, visible: BingoScheduler.filter() == '75'">
		                  <?php include "_global-library/partials/bingo/bingo-room-slide.php"; ?>
		                </ul>

		                <!-- 5L -->
		                <ul style="display: none" data-bind="foreach: BingoScheduler.rooms5L, visible: BingoScheduler.filter() == '5L'">
		                  <?php include "_global-library/partials/bingo/bingo-room-slide.php"; ?>
		                </ul>

		                <!-- PRE -->
		                <ul style="display: none" data-bind="foreach: BingoScheduler.roomsPre, visible: BingoScheduler.filter() == 'PRE'">
		                  <?php include "_global-library/partials/bingo/bingo-room-slide.php"; ?>
		                </ul>

              		</div>
              		
			</div>
			<!-- / bingo rooms caroussels -->
			
		</div>
	</div>
</div> 