<div class="content-holder home-community-holder">
	<div class="content-holder__content">
		<div class="content-holder__content_full-width">
			
			<div class="content-holder__title">
				<h1>COMMUNITY</h1>
				<span class="content-holder__title__link"><a href="/community/">View All</a></span>
			</div>
			
			<div class="content-holder__inner">
				<?php include "_partials/home/home-community/home-community-inner.php"; ?>
			</div>
			
		</div>
	</div>
</div>