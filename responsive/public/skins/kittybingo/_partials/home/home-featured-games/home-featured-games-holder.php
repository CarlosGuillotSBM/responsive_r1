<div class="content-holder home-featured-games-holder">
	<div class="content-holder__content">

		<div class="content-holder__content_left">
            <div class="content-holder__inner start-content">
                <?php include '_partials/start-skin-only/start-featured-games-section.php'; ?>
            </div>
		</div>

		<div style="display: none" data-bind="visible: !validSession()" class="content-holder__content_right">
			<div class="content-holder__title">
				<h1>LATEST OFFER</h1>
					<!-- content holder title right link -->
			</div>


			<div class="content-holder__inner latest-offer-promo ui-scheduled-content-container">
				<!-- Edit point Banner -->
				<?php edit($this->controller,'home-latest-offer'); ?>
				
				<?php @$this->repeatData($this->content['home-latest-offer']); ?>
				<!-- Edit point Banner -->
			</div>

		</div>

	</div>
</div>
