<script type="application/javascript">
skinModules.push({
		id: "LoginBox",
		options: {
			modalSelectorWithoutDetails: "#custom-modal",
			global: true
		}
	});
</script>
<!-- LOGIN BOX -->
<article class="login-box">

	<div class="login-box__form-wrapper">
		<h1>login</h1>
		<!-- FORM -->
		<form class="login-box__form" id="ui-login-form">
			<div class="login-box__input-wrapper">
				<div class="cat-avatar"></div>
				<div class="login-box__username">
					<input id="login_name" name="Username" placeholder="Username" type="text" placeholder="User Name" data-bind="value: LoginBox.username, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" onFocus="sbm.core.publish('show-recaptcha')" onfocusout="sbm.core.publish('hide-recaptcha')" />
						<img src="/_images/common/icons/login-username.svg">

				</div>

			</div>
			<div class="login-box__input-wrapper">
				<div class="login-box__password">
					<input name="Password" id="login_password" placeholder="Password" type="password" placeholder="Password" data-bind="attr:{type: LoginBox.passwordShowText() == 'Hide' ? 'text': 'password'}, value: LoginBox.password, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}"  onFocus="sbm.core.publish('show-recaptcha')" onfocusout="sbm.core.publish('hide-recaptcha')"/>
										<img src="/_images/common/icons/login-password.svg" class="login-password-img">
					<div class="login-box__show-password" data-bind="click: LoginBox.togglePassword">
						<a class="right" data-bind="text: LoginBox.passwordShowText">Show</a>
					</div>

				</div>

				<span style="display: none" data-bind="visible: LoginBox.invalidPassword" class="error">This field is required.</span>
				<span style="display: none" data-bind="visible: LoginBox.invalidUsername" class="error">This field is required.</span>

			</div>
			<a style="display: none" data-bind="visible: LoginBox.lockedUser, text: LoginBox.loginError" class="error" href="/help"></a>
		<!-- ko ifnot: LoginBox.lockedUser -->
			<span style="display: none" data-bind="visible: LoginBox.loginError, html: LoginBox.loginError" class="error"></span>
		<!-- /ko -->

			<!-- <a href="/register/" class="login-box__new-player">New Player sign up</a> -->

			<a class="cta-login-header" data-bind="click: LoginBox.doLogin.bind($data, GameLauncher.gameId(), GameLauncher.gamePosition(), GameLauncher.gameContainerKey(), GameLauncher.roomId())">LOGIN</a>
			
			<a href="/forgot-password/" class="login-box__forgot-password">Forgot Password?</a>
		</form>
		<!-- / FORM -->

		<!-- Account locked -->
		<div style="display:none" class="account-locked">
			<p>For security reasons, your account is temporarily locked.
			Please contact support.</p>
			<a target="_blank" data-hijack="false" href="mailto:support@kittybingo.com"> support@kittybingo.com</a><br>
			<a target="_blank" data-hijack="false" href="tel:08082386070"> 0808 238 6070 (Freephone)</a><br>
			<a  target="_blank" data-hijack="false" href="tel:02035519705"> 0203 551 9705 (Landline)</a>
		</div>
		<!-- End Account locked -->

	</div>

	<!-- RIBBONS -->
<!-- 	<span class="ribbon-left"></span>
	<span class="ribbon-right"></span> -->

	<!-- LOGIN OFFER CTA -->
	<div class="join-now-box">
		<p>Not Registered yet?</p>
		<span>
			<!-- Edit point  -->
			<?php edit($this->controller,'login-box-offer'); ?>
			<?php @$this->getPartial($this->content['login-box-offer'],1); ?>
		</span>
		<!-- CTA -->
		<a class="registercta" href="/register/" data-hijack="true" >Join Now</a>
	</div>
	<!-- /LOGIN OFFER CTA -->
</article>
