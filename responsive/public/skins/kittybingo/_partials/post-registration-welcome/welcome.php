<div class="welcome-post-reg__modal">
    <!-- TOP HEADER CLOSE MODAL -->

    <a href="/start/" data-gtag="Welcome Start" target="_top" >


      <div class="header-wrap">

        <div class="header-container">

          <div class="logo"></div>

        <p class="header-username">
        Welcome <span data-bind="text: username">...</span>, thanks for joining!</p>
        </div>

      </div>
    </a>
  

    <!-- MIDDLE CONTENT -->
    <div class="main-content welcome-post-reg__content">
      <!--     <div class="inline_logo"></div> -->
      <!-- OFFERS -->

<!--      <div class="bg-clouds"></div>

  -->
      <section class="deposit">

        <h2>Plus, your 1st deposit offer is waiting – CHOOSE ONE</h2>
        

        <ul>

          <!-- 1st offer -->

          <li class="claim-wrap">

                <div class="claim-container">
                  <span>300% Bingo Bonus + 100 Free Spins</span>
                 </div>

                <a href="/cashier/" class="deposit-cta" data-gtag="Welcome Left Deposit">DEPOSIT TO CLAIM</a>
  
                <div class="claim-container__right-panel">
            <span class="supercat">
                <img src="/_images/welcome/supercat1.png" alt="" class=""></span>
            </div>

          </li>


          <!-- 2nd offer -->
        <li class="claim-wrap">

              <div class="claim-container">
              <span>100% SLOTS BONUS + 100 Free Spins</span>
              </div>

              <a href="/cashier/" class="deposit-cta" data-gtag="Welcome Right Deposit">DEPOSIT TO CLAIM</a>

           <!--  </div> -->

            <div class="icon">
              <span class="imagery4">
              <img src="/_images/switch-old-to-new/luke-hands-up.png" alt="">
              </span>
            </div>

          </li>
        </ul>
      </section>


      <a href="/cashier/" data-gtag="Welcome Footer Cashier Link" class="no-bonus-link">You can also choose to play without taking a bonus. Find this option in the <span>cashier.</span></a>

      <!-- LOCK -->
         <!--  <section class="lock"> -->
<!--         <ul>
          <li>
            <div class="claim-wrap">
              <div class="claim-container">

              <span>2ND DEPOSIT BONUS 150% BINGO BONUS</span></div>
            </div>
            <div class="icon"></div>
          </li>
          <li>
            <div class="claim-wrap">
              <div class="claim-container"><span>2ND DEPOSIT BONUS 2ND DEPOSIT BONUS</span></div>
            </div>
            <div class="icon"></div>
          </li>
        </ul>
      </section>
       -->
    </div>
    <!-- END MIDDLE CONTENT -->

  </div>