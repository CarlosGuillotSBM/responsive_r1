<!-- END Games info modal -->
</div>
<!-- END HOME MAIN CONTENT AREA -->
</div>
<!-- END AJAX WRAPPER - It is needed to add content inside dynamically -->

<!-- FOOTER AREA -->
<footer id="footer" class="footer" data-bind="css: { 'logged-in': validSession() }">
<!-- FOOTER PROMO -->
<?php include'_partials/footer-promo/footer-promo.php'; ?>
<div class="footer-container">
<!-- FOOTER NAVIGATION  -->
<div class="footer-nav-wrap">
	<?php include "_partials/common/footer-nav.php" ?>
	<!-- SUPPORT ONLINE  -->
	<div class="online-support">
		<div target="_blank" data-hijack="false" class="support-online-title">
			<a href="#" onclick="javascript:void window.open('<?php echo config('LiveChatURL'); ?>','1323353895481','width=660,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;" class="chat">
				<h1>Online Support</h1>
				<span>24h live chat</span>
				<img src="/_images/common/nav-social-icons/support-orange.svg" alt="24h Live chat">
				<!-- <img src="/_images/common/support.svg"> -->
			</a>
		</div>
	</div>
</div>
<!-- /FOOTER NAVIGATION  -->

<!-- Container  -->
<div class="footer__inner">
	<div class="footer-legal">
		<!-- FOOTER FLOATING SOCIAL ICONS  -->
		<div class="follow-us">
			<a target="_blank" href="https://www.facebook.com/kittybingo/" class="hi-icon"  data-hijack="false"><img src="/_images/common/social/facebook.svg"></a>
			<a target="_blank" href="https://twitter.com/Kitty_Bingo" class="hi-icon" data-hijack="false"><img src="/_images/common/social/twitter.svg" onerror="this.onerror=null; this.src='image.png'"></a>
			<a target="_blank" href="https://www.youtube.com/channel/UCZS4ymIIfyavJ3gjKXHqWwQ" class="hi-icon"  data-hijack="false"><img src="/_images/common/social/youtube.svg"></a>
		</div>
		<!-- FOOTER FLOATING PAYMENT METHODS  -->
		<ul class="footer__row__payments">
			<li> <img class="paypal" src="/_images/common/pay-methods/paypal.svg"></li>
			<li> <img class="maestro" src="/_images/common/pay-methods/maestro.svg"></li>
			<li> <img class="mastercard" src="/_images/common/pay-methods/mastercard.svg"></li>
			<li> <img class="visa" src="/_images/common/pay-methods/visa.svg"></li>
			<li> <img class="neteller" src="/_images/common/pay-methods/neteller.svg"></li>
			<li> <img class="paysafecard" src="/_images/common/pay-methods/paysafecard.svg"></li>
			<li><a href="/terms-and-conditions/"><img src="/_images/common/commissions/18-plus.svg"></a></li>
			<li><a target="_blank" data-hijack="false" href="http://www.gamcare.org.uk/"><img src="/_images/common/commissions/gamcare.svg"></a></li>
			<li><a target="_blank" data-hijack="false" rel="nofollow"  href="https://www.gamstop.co.uk"><img src="/_images/common/commissions/gamstop.svg"> </a></li>
			<li><a target="_blank" data-hijack="false"   href="http://www.gamblingcontrol.org/"><img src="/_images/common/commissions/gambling-control.svg"></a></li>
			<li><a target="_blank" data-hijack="false" rel="nofollow" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022"><img class="gambling" src="/_images/common/commissions/gambling-commission.svg"  rel="nofollow"></a></li>
			<li><a target="_blank" data-hijack="false" href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&lng=EN"><img src="/_images/common/commissions/ecogra.svg"></a></li>
		</ul>

		<div class="clearfix"></div>
		<!-- FOOTER COMMISSIONS ICONS -->
		<div class="footer-commissions">
			<ul class="commissions-list-2">
				<!-- <li><a target="_blank" data-hijack="false" href="http://www.stridegaming.com/"><img src="/_images/common/commissions/stridegaming-logo.png"></a></li> -->
				<!-- <li><a target="_blank" data-hijack="false" href="http://www.londonstockexchange.com/exchange/prices-and-markets/stocks/summary/company-summary/JE00BWT5X884JEGBXASQ1.html"><img src="/_images/common/commissions/london-stock-exchange.png"></a></li> -->
			</ul>
		</div>
		<div class="clearfix"></div>
		<!-- FOOTER FLOATING CONTENT LEGAL AND CONTACT  -->
		<div class="footer-terms">
			All Financial Transactions processed by WorldPay. © Kitty Bingo. All rights reserved.<br />
			Kitty Bingo is licensed and regulated to offer Gambling Services in Great Britain by the UK Gambling Commission, license Number 000-039022-R-319427-004. All the games offered on the website have been approved by the UK Gambling Commission. Details of its current licensed status as recorded on the Gambling Commission's website can be found <a target="_blank" data-hijack="false" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022">here</a>.
			Kitty Bingo is also licensed and regulated by the Alderney Gambling Control Commission, License Number: 71 C1, to offer Gambling facilities in jurisdictions outside Great Britain.
			Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3UL.
		</div>
		<!-- BACK TO TOP -->
		<a class="back-to-top" href="#ajax-wrapper" data-hijack="false">
			<img src="/_images/common/accordion-up-arrow.png" alt="Back To Top">
		</a>
	</div>
	<div class="clearfix"></div>
</div>
</div>
<!-- /Container -->
<!-- /FOOTER FLOATING CONTENT LEGAL AND CONTACT -->
<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>
</footer>
<!-- END FOOTER AREA -->
</div>
<!-- END INNER WRAPPER. It is necesary for foundation off canvas nav -->
</div>
<!-- login modal -->
<div id="login-modal" class="reveal-modal" data-reveal>
<?php include "_partials/login/login-box.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- login modal more info -->
<div id="login-modal-more-info" class="reveal-modal" data-reveal>
<?php include "_partials/login/login-box-more-info.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal more info -->
<!-- Claimed prize modal -->
<div id="claimed-prize-modal" class="reveal-modal" data-reveal>
<?php include "_global-library/partials/promotion/promotion-claimed-prize.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- login modal deposit -->
<div id="login-modal-deposit" class="reveal-modal" data-reveal>
<?php include "_partials/login/login-box-deposit.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- terms modal -->
<!-- <div id="terms-modal" class="reveal-modal" data-reveal style="display:block">
<span class="close-reveal-modal">&#215;</span>
</div> -->
<!-- /terms modal -->
<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">
<!-- END OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->

<!-- Games info modal -->
<div id="games-info" class="games-info reveal-modal" data-reveal>
<?php include "_partials/games/games-details-modal.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Games info modal -->
<!-- Login Box modal -->
<!-- <div id="login-box-modal" class="reveal-modal" data-reveal>
<?php //include "_partials/login/login-box-modal.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<a href="#" data-reveal-id="login-box-modal">login-box</a> -->
<!-- Login Box modal -->

<!-- Bingo room modal -->
<div id="rooms-info" class="games-info reveal-modal" data-reveal>
<?php include "_partials/bingo-rooms/room-detail-modal.php" ?>
<?php //include "_global-library/partials/bingo/room-detail-modal.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Bingo room modal -->

<!-- Custom modal -->
<div id="custom-modal" class="games-info reveal-modal" data-reveal>
<?php include "_partials/bingo-rooms/no-room-detail-modal.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Custom modal -->

<!-- footer navbar -->
<?php include'_partials/nav-mobile/footer-navigation-mobile.php'; ?>

<?php
	include "_global-library/partials/modals/alert.php";
	include "_global-library/partials/modals/notification.php";
	include "_partials/common/loading-modal.php";
	include "_global-library/partials/games/games-iframe.php";
	include "_global-library/partials/games/games-last-played.php";
	include "_global-library/partials/common/app-js.php";
	include "_global-library/partials/common/recaptcha.php";
?>
<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">
</body>
</html>