<script type="application/javascript">
	window.setTimeout(function () {
		var el = document.getElementById("ui-first-loader");
		if (el) el.parentNode.removeChild(el);
		el = document.getElementById("ui-loader-animation");
		if (el) el.style.display = "none";
	},1000);
</script>
<!-- OFF CANVAS -->
<!-- This is the Foundation side nav, everything should be wrapped inside off-canvas-wrap -->
<!-- OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->

<!-- SWITCH TOP MESSAGE NEW LP -->
<?php include "_partials/switch-old-to-new/switch-top-message-new-lp.php" ?>

<!-- SWITCH TOP MESSAGE OLD LP -->
<?php // include "_partials/switch-old-to-new/switch-top-message-old-lp.php" ?>

<div class="main-wrap top-message">
	<!-- header  -->
	<header class="header header--<?php echo $this->controller; ?>">
		
		<!-- HEADER DESKTOP -->
		<div class="header-desktop">

			
			
			<div class="header-wrap">
				<div class="navigation__container">
					<?php include "_partials/common/navigation-desktop.php"  ?>
				</div>
			</div>

		</div>
		<!-- /HEADER DESKTOP -->
		
		<!-- HEADER MOBILE old-->
		<!-- 		<div class="header-mobile">
			<?php// include "_partials/common/navigation-mobile.php" ?>
		</div> -->
		<!-- nav for mobile -->
		<?php include "_partials/nav-mobile/browser-not-supported-nav-mobile.php"; ?>
		<!-- / nav for mobile -->
		<!-- search screen for mobile -->
		<?php include "_partials/search-screen/search-screen.php"; ?>
		<!-- / search screen for mobile -->
	</header>
	
	
</div>
<!-- /HEADER MOBILE -->
<!-- /header  -->
<div class="clearfix"></div>
<!-- AJAX CONTENT WRAPPER -->
<div id="ajax-wrapper">
	<!-- MAIN CONTENT AREA -->
	<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>  ">
		<!-- TOP CONTENT -->
		<!-- <div class="top-content">
		</div> -->
		<!-- /TOP CONENT -->