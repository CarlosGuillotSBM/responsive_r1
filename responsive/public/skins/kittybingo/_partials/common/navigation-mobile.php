<!-- TAB NAV -->
<div class="tab-bar nav-for-mobile">
  <div class="left-small">
    <script type="text/javascript">
    </script>
    <a class="left-off-canvas-toggle menu-icon" data-gtag="Opened Left Menu"><span></span></a>
  </div>
  <div class="middle tab-bar-section">
    <a data-hijack="true" href="/" class="site-logo ui-go-home-logo">
      <!-- <h1 href="/"> &nbsp;<?php //echo config("Name"); ?> </h1> -->
      <img src="/_images/common/logo-horitzontal.svg">
    </a>
        <a data-hijack="true" href="/" class="site-logo ui-go-home-logo">
      <!-- <h1 href="/"> &nbsp;<?php //echo config("Name"); ?> </h1> -->
      <img src="/_images/common/logo-horitzontal.svg">
    </a>
  </div>
  <div style="display: none" class="right-small" data-bind="visible: !validSession()">
    <div class="right-small__col">
      <a data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login-button--nav-for-mobile ui-scroll-top" href="/login/">LOGIN</a>
    </div>
    
    <div class="right-small__col">
      <a data-gtag="Join Now,Mobile Top Nav" data-bind="visible: !validSession()" class="join-now-button--nav-for-mobile" href="/register/" data-hijack="true">JOIN NOW</a>
    </div>
  </div>
  <!-- /MAGIC MENU ONLY IF LOGGED IN FOR MOBILE -->
  <!--     <div style="display: none" data-bind="visible: validSession" class="right-small--balances"> -->
  <div style="display: none" data-bind="visible: validSession" class="right-small--balances">
    <!--   <a href="/my-account/" class="right-off-canvas-toggle icons__balance-white" data-nohijack="true"></a> -->
    <a href="/cashier/" class=" icons__deposit" data-hijack="true" data-gtag="Mobile Menu Cashier Click"></a>
  </div>
  <div class="right-small--balances-amount">
    <span class="balance-area__value" data-bind="text: balance"></span></div>
    
  </div>
  <!-- /TAB NAV -->
  <!-- OFF CANVAS NAV -->
  <aside class="left-off-canvas-menu">
    <ul class="off-canvas-list">
      <!--  <li><label>Navigation</label></li> -->
      <li data-bind="visible: validSession"><a href="/start/" data-hijack="true">START</a></li>
      <li data-bind="visible: validSession"><a href="/cashier/" data-hijack="true">CASHIER</a></li>
      <li data-bind="visible: !validSession()" class="active"><a data-hijack="true" href="/home/">HOME</a></li>
      <li data-bind="visible: !validSession()" class="active"><a data-hijack="true" href="/register/">JOIN NOW</a></li>
      <li data-bind="visible: validSession"><a href="/my-account/" data-hijack="true">MY ACCOUNT</a></li>
      <!-- games -->
      <!--     <li><label>Games</label></li> -->
      <li ><a data-hijack="true" data-bind="click: playBingo">BINGO ROOMS</a></li>
      <li ><a href="/games/" data-hijack="true">ALL GAMES</a></li>
      <li><a href="/slots/" data-hijack="true">SLOTS</a></li>
      <li><a href="/table-card/" data-hijack="true">TABLE &amp; CARD GAMES</a></li>
      <li><a href="/promotions/" data-hijack="true">PROMOTIONS</a></li>
      <li><a href="/community/" data-hijack="true">COMMUNITY</a></li>
      <li><a href="/community/winners/" data-hijack="true">WINNERS</a></li>
      <li><a href="/kitty-club/" data-hijack="true">LUCKY CLUB</a></li>
      <li><a href="/help/" data-hijack="true">HELP</a></li>
      <!--     <li><a href="/about-us/" data-hijack="true">ABOUT</a></li> -->
      <!--     <li><a href="/faq/" data-hijack="true">FAQ</a></li> -->
      <li><a href="/banking/" data-hijack="true">BANKING</a></li>
      <li><a href="/terms-and-conditions/" data-hijack="true">TERMS &amp; CONDITIONS </a></li>
      <li><a href="" style="display: none" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">LOGOUT</a></li>
      <!--  <li><a href="/privacy-policy/" data-hijack="true">PRIVACY POLICY</a></li> -->
      <!--  <li><a href="/responsible-gaming/" data-hijack="true">RESPONSIBLE GAMING</a></li> -->
      <!--  <li><a href="http://www.luckyjar.com" data-hijack="true">AFFILIATES</a></li> -->
      
      
    </ul>
  </aside>
  <aside class="right-off-canvas-menu">
    <ul class="off-canvas-list">
      <li data-bind="visible: validSession"><a href="/my-account/" data-hijack="true"><span data-bind="text: username"></span></a></li>
      <li data-bind="visible: validSession"><a href="/cashier/" data-hijack="true">CASHIER</a></li>
      <li data-bind="visible: validSession"><a href="/my-account/" data-hijack="true">MY BALANCE: <span data-bind="text: balance">£0000,000</span></a></li>
      <li data-bind="visible: validSession" class="active"><a data-hijack="true" href="/my-account/">MY FREE SPINS: <span data-bind="text: bonusSpins">4000</span></a></li>
      <li data-bind="visible: validSession" class="active"><a data-hijack="true" href="/my-account/">POINTS: <span>0</span></a></li>
      <li data-bind="visible: validSession"><a href="/start/" data-hijack="true">MY GAMES START</a></li>
      <li data-bind="visible: validSession"><a href="/my-account/" data-hijack="true">MY ACCOUNT</a></li>
      <li><a href="" style="display: none" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">LOGOUT</a></li>
      
    </ul>
  </aside>
  <!-- /OFF CANVAS NAV -->