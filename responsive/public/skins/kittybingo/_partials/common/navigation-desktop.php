<!--  *********** Navigation Desktop Loogged out  *********** -->
<div class="navigation-desktop">

	<!-- logo -->
	<h1 class="seo-logo">
		<a href="/" data-hijack="true" class="header__logo ui-go-home-logo">
			<img src="/_images/logo/logo-sml.png">
		</a>
		<span style="display:none;">Kitty Bingo Games Play bingo online!</span>
	</h1>
	

	<!-- Login  -->
	<div class="header__login" >

		<!-- Login Register Area IN-->
		<div class="user-nav" data-bind="visible: validSession()" style="display:none;">

			<!-- Nav Left -->
			<div class="user-nav__left" >
				<span class="user-nav__left__avatar">
					<img data-bind="attr{src: avatarRoute} , click: playBingo">
				</span>
				<a class="logout" data-bind="click: logout" data-hijack="true" data-gtag="Logout Desktop">LOGOUT</a>
			</div>

			<!-- Nav middle -->
			<div class="user-nav-middle">
				<div class="user-nav__alias" title="Account Balance">
					<a href="/start/" data-hijack="true">
						<p>Balance</p>
						<span data-bind="text: balance" class="user-nav__container"></span>
					</a>
				</div>
				<div class="user-nav__balance" title="Real Money">
					<a href="/start/"  data-hijack="true">
						<p>Real</p>
						<span data-bind="text: real" class="user-nav__container"></span>
					</a>
				</div>
				<div class="user-nav__slots" title="Bonus Money">
					<a href="/start/"  data-hijack="true">
						<p>Bonus</p>
						<span data-bind="text: bonusBalance" class="user-nav__container"></span>
					</a>
				</div>
			</div>

					<!-- Nav right -->
					<div class="user-nav__right" >
						<a class="user-nav__right__button" href="/cashier/" data-hijack="true" data-gtag="Logout Desktop">CASHIER</a>
						<span class="cashier-graphic"><img src="/_images/header/cashier-bt.png"></span>
					</div>

					<!-- Nav Right -->
					<div class="user-nav-right">
						<!-- Message Inbox-->
						<?php if(config("inbox-msg-on")): ?>
							<div class="user-nav__message-inbox" title="Message Inbox" data-bind="visible: MailInbox.unreadMessages() > 0">
								<a href="/start/"  data-hijack="true" >
									<img src="/_images/balance-area/icon-message-inbox.svg">
									<span class="user-nav__message-inbox__value" data-bind=" text: MailInbox.unreadMessages()"></span>
								</a>
							</div>
						<?php endif; ?>
						<!-- /Message Inbox-->
						<!-- <div id="cashier" class="user-nav__cashier" title="Launch the Cashier"><a href="/cashier/"  data-hijack="true">CASHIER</a></div> -->
						<!-- <div id="my-account"  class="user-nav__my-account"><a href="/my-account/"  data-hijack="true">My Account</a></div> -->
					</div>
				</div>
				<!-- / Login Register Area IN-->

				<!-- PLAY BINGO-->
				<a class="button--play" data-hijack="true" data-bind="visible: validSession() , click: playBingo"></a>

				<!-- Login Register Area OUT-->
				<div class="logged-out-buttons" data-bind="visible: !validSession()" style="display:none;">
					<?php include'_partials/login/login-box.php'; ?>
					<a class="button--join" href="/register/"></a>
				</div>

			</div>
			<!-- /Login Register Area IN-->
			<!-- / Login  -->

			<!-- menu -->
			<ul class="navigation-desktop__nav">
				<li id="home"><a class="ui-go-home" href="/" data-hijack="true">Home</a></li>
				<li id="register" data-bind="visible: !validSession()"><a href="/register/" data-hijack="true">Register</a></li>
				<!-- <li id="register" data-bind="visible: validSession()" style="display: none;"><a href="/start/" data-hijack="true">My account</a></li> -->
				<li id="bingo" data-bind="visible: !validSession()" style="display: none;"><a href="/bingo/" data-hijack="true" class="current">Bingo</a></li>
				<li data-bind="visible: validSession, click: playBingo" style="display: none"><a>Bingo</a></li>
				<li id="slots"><a href="/slots/" data-hijack="true">Slots &amp; Games</a></li>
				<li id="my-favourites" data-bind="visible: validSession()" style="display: none;"><a href="/my-favourites/" data-hijack="true" data-bind="visible: Favourites.favourites().length > 0">My favourites</a></li>
				<li id="promotions"><a href="/promotions/" data-hijack="true">Promos</a></li>
				<li class="radical-dice" data-bind="visible: displayThreeRadical(), click: ThreeRadical.requestToken" style="display: none" class>
					<svg>
						<g id="sparkle1">
							<path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
							<path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
							<path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
						</g>
					</svg>
					<a href="" data-hijack="true">Daily play <img class="bounce-dice" src="/_images/common/nav-social-icons/dice.svg" alt="Extras"/></a>
				</li>
				<li id="kitty-club"><a href="/kitty-club/" data-hijack="true">Kitty Club</a></li>
				<li id="winners"><a href="/community/winners/" data-hijack="true">Winners</a></li>
				<li id="help"><a href="/help/" data-hijack="true">Help</a></li>
				

				<!-- social icons -->
				<ul class="nav-social-icons">
					<li class="tooltip-info"><span class="tooltip-content">Live Chat</span><a href="#" onclick="javascript:void window.open('<?php echo config('LiveChatURL'); ?>','1323353895481','width=660,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;" class="chat"><img src="/_images/common/nav-social-icons/support-orange.svg" alt="24h Live chat"></a></li>
				</ul>
			</ul>

			<!-- ribbons -->
			<img class="navigation-desktop__left-ribon" src="/_images/header/left-ribbon.png">
			<img class="navigation-desktop__right-ribbon" src="/_images/header/right-ribbon.png">

		</div>
