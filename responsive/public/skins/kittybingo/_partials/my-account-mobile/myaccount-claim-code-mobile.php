<div class="mobile-main-content__modal">
  <!-- HEADER CLOSE MODAL -->
  <a class="mobile-main-content__header">Enter a Claim Code</a>
  <!-- CONTENT -->
  <div class="mobile-main-content__content">
	<?php include '_partials/start-skin-only/start-claim-code.php' ?>
  </div>
  <!-- / CONTENT -->
</div>