<script type="application/javascript">
//    skinModules.push({
			// 	id: "TermsModal",
			// 	options: {
					// 		global: true
			// 	}
	// });
</script>

<div class="terms-modal__top-row">
<h1>Our Terms &amp; Conditions have changed</h1>
</div>

<?php edit($this->controller,'terms-and-conditions'); ?>
<div class="terms-modal__text-wrapper">
	<?php @$this->repeatData($this->content['terms-and-conditions']);?>
</div>
<div class="terms-modal__bottom-row">
	<div class="terms-cancel"><a href="/home/">I do not agree to the Terms &amp; Conditions</a></div>
	<a class="button--join" data-bind="click: LoginBox.acceptTC" >I accept the Terms &amp; Conditions</a>
</div>