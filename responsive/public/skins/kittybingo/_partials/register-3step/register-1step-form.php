
<!-- 3 STEP REGISTRATION FORM -->
<div class="registration-3step-form">

  <form id="registration-3step-form">

      <p class="registration-3step__mandatory-fields">Mandatory fields*</p>

      <!-- 1 STEP 
      ////////////////////////////////////////////////////////////////////////////// -->

      <!-- TITLE -->
      <ul class="registration__title" data-bind="value: Register.title">
        <label for="title">Title</label>
        <li class="ticked"><button>Mr</button></li>
        <li><button>Mrs</button></li>
        <li><button>Mrs</button></li>
        <li><button>Miss</button></li>
        <li><button>Other</button></li>
      </ul>
      
      <!-- FIRST NAME -->
      <input data-bind="value: Register.firstName" class="textField" type="text" placeholder="First Name*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      
      <!-- LAST NAME -->
      <input data-bind="value: Register.lastName" class="textField" type="text" placeholder="Last Name*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      
      <!-- DATE OF BIRTH 
      http://zurb.com/university/lessons/date-night-with-zurb-foundation-how-to-set-up-a-date-picker
      -->
      <div class="date">
        <label for="startDate">Date of birth</label>
        <input type="text" name="startDate" class="fdatepicker" placeholder="7/11/2015" data-bind="value: Register.dayOfBirth">
      </div>

    <!-- COUNTRY -->
    <select data-bind="options: Register.countries, optionsCaption: 'Country', optionsValue: 'Code', optionsText: 'Desc', value: Register.selectedCountry" class="textField"></select>
    
    <!-- POST CODE -->
    <div>
      <input class="post-code" data-bind="value: Register.postcode, valueUpdate: ['input', 'afterkeydown']" type="text" placeholder="Post Code*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <a href="#" data-nohijack="true" data-bind="visible: Register.showFindAddress, click: Register.postcodeLookup" class="button postfix findaddress" >Find Address</a>
      <div style="display: none" data-bind="visible: Register.postcodeIsValid">
        <select data-bind="visible:  Register.addresses().length > 0,
          options: Register.addresses,
          optionsCaption: 'Please select',
          value: Register.selectedAddress1" class="textField border-important">
        </select>
        <input style="display: none" data-bind="visible: Register.postcodeIsValid && Register.addresses().length === 0, value: Register.address1" class="textField" type="text" placeholder="Address 1*"
        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <input data-bind="value: Register.address2" class="textField" type="text" placeholder="Address 2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <input data-bind="value: Register.city" class="textField" type="text" placeholder="City*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <input data-bind="value: Register.state" class="textField" type="text" placeholder="County / State*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      </div>
    </div>



    <!-- 2 STEP  
    ////////////////////////////////////////////////////////////////////////////// -->

    <!-- EMAIL -->
    <div>
      <!-- <label class="error">Error</label> -->
      <input data-bind="value: Register.email" class="textField" type="email" placeholder="E-Mail*" autofocus>
      <!-- <small class="error">Invalid entry</small> -->
      <span style="display: none" data-bind="text: Register.emailNotAvailable, visible: Register.emailNotAvailable() != ''" class="error"></span>
    </div>

    <!-- USERNAME -->
    <div>
      <input data-bind="value: Register.username" class="textField" type="text" placeholder="Username*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      <span style="display: none" data-bind="text: Register.usernameNotAvailable, visible: Register.usernameNotAvailable() != ''" class="error"></span>
    </div>

    <!-- PASSWORD -->
    <div class="registration__password">
        <input class="textField" data-bind="value: Register.password, attr: { type: Register.showPassword() ? 'text' : 'password' }" type="password" placeholder="Enter password*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <div class="show-password" data-bind="click: LoginBox.togglePassword">
          <a class="right" data-bind="text: LoginBox.passwordShowText">Show</a>
        </div>
        <!-- <label data-tooltip title="Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces">Password Help</label>-->
    </div>

    <!-- CURRENCY -->
    <select data-bind="options: Register.currencies, optionsCaption: 'Currency', optionsValue: 'Code', optionsText: 'Desc', value: Register.selectedCurrency" class="textField"></select>
    
    


    <!-- 3 STEP  
    ////////////////////////////////////////////////////////////////////////////// -->

    <!-- MOBILE PHONE -->
    <input data-bind="value: Register.mobilePhone" class="textField" type="tel" placeholder="Mobile phone*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">

    <!-- PROMO CODE -->
    <input data-bind="value: Register.promoCode" class="textField" type="text" placeholder="Promo Code" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">

    <!-- MAILING OPT -->
    <ul class="mailing-opt">
      <p>Yes please keep me up to date with promotions, new games and other information:</p>

      <!-- EMAIL -->
      <li>
        <label class="ticked" for="cbEmail">
          <input data-bind="checked: Register.wantsEmail" type="checkbox" checked id="cbEmail">
          <span class="custom checkbox"></span>
          Email
        </label>
      </li>

      <!-- SMS -->
      <li>
        <label for="cbSms">
          <input data-bind="checked: Register.wantsSMS" type="checkbox" checked id="cbSms">
          <span class="custom checkbox"></span>
          SMS
        </label>
      </li>

      <!-- OVER 18 -->
      <li>
        <label for="cbOver18">
        <input data-bind="checked: Register.over18" type="checkbox" id="cbOver18" name="cbOver18">
          <span class="custom checkbox"></span>
          I am 18 or over and agree to the <a data-nohijack="true" class="register-3step-tc" href="/terms-and-conditions/" target="terms">TC'S</a>
        </label>
      </li>

    </ul>
    <!-- /MAILING OPT -->

    <!-- CTA -->
    <a class="button register-3step-cta" data-nohijack="true" data-bind="click: Register.doRegistration">Join Now</a>

</form>

</div>
<!-- /3 STEP REGISTRATION FORM -->
