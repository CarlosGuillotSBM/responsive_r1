
<!-- 3 STEP REGISTRATION FORM -->
<div class="registration-3step-form">

  <form id="registration-3step-form">

      <!-- 1 STEP  -->
<fieldset class="step" data-bind="visible: Register.currentStep() == 1">
      <!-- TITLE -->
      <ul class="registration__title" data-bind="value: Register.title">
        <label for="title">Title</label>
        <li data-bind="css: {ticked: Register.title() == 'Mr'}, click: function() { Register.title('Mr');}" class="ticked"><button>Mr.</button></li>
        <li data-bind="css: {ticked: Register.title() == 'Mrs'}, click: function() { Register.title('Mrs');}"><button>Mrs.</button></li>
        <li data-bind="css: {ticked: Register.title() == 'Ms'}, click: function() { Register.title('Ms');}" ><button>Ms.</button></li>
        <li data-bind="css: {ticked: Register.title() == 'Miss'}, click: function() { Register.title('Miss');}"><button>Miss</button></li>
      </ul>
      
      <!-- FIRST NAME -->
      <div class="field-wrap">
      <span class="field-wrap__label">First Name</span>
      <div id="icon"></div>
      <input class="textField field-wrap__field" data-bind="value: Register.firstName" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      </div>
      
      <!-- LAST NAME -->
         <div class="field-wrap">
           <span class="field-wrap__label">Last Name</span>
             <div id="icon"></div>
      <input class="textField field-wrap__field" data-bind="value: Register.lastName"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
       </div>

       <!-- EMAIL -->
     <div class="field-wrap">
      <!-- <label class="error">Error</label> -->
         <span class="field-wrap__label">E-mail</span>
         <div id="icon"></div>
      <input id="ui-email" class="textField field-wrap__field" data-bind="value: Register.email"  type="email">
      <!-- <small class="error">Invalid entry</small> -->
      <span style="display: none" data-bind="text: Register.emailNotAvailable, visible: Register.emailNotAvailable() != ''" class="error"></span>
    </div>

    <!-- USERNAME -->
      <div class="field-wrap">
              <span class="field-wrap__label" >Username</span>
          <div id="icon" style="height: 96px"></div>
              <p class="field-wrap__requirements">Minimum of 3 characters</p>
      <input id="ui-username" class="textField field-wrap__field" data-bind="value: Register.username"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      <span style="display: none" data-bind="text: Register.usernameNotAvailable, visible: Register.usernameNotAvailable() != ''" class="error"></span>
    </div>

    <!-- PASSWORD -->
      
<div class="registration__password">
        <div class="field-wrap">
        <span class="field-wrap__label">Password</span>
            <div id="icon" style="height: 96px"></div>
        <p class="field-wrap__requirements">Minimum of 5 characters include numbers</p>
        <input class="textField field-wrap__field registration__password" data-bind="value: Register.password, attr: { type: Register.showPassword() == true ? 'text' : 'password' }" type="password" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <div class="show-password eye-shut" data-bind="click: Register.togglePassword">
            
        </div>
        <!-- <label data-tooltip title="Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces">Password Help</label>-->


     
        <!-- <label data-tooltip title="Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces">Password Help</label>-->
    </div>
    </div>
      
      <!-- DATE OF BIRTH 
      http://zurb.com/university/lessons/date-night-with-zurb-foundation-how-to-set-up-a-date-picker
      -->

  <div class="field-wrap">
    <span class="field-wrap__label">Date of Birth</span>
      <div class="date">

        <!-- tel used to bring up friendlier number input keyboard -->
        <input type="number" pattern="\d*" id="dayOfBirth" name="dayOfBirth" class="fdatepicker" placeholder="DD" maxlength="2" data-bind="value: Register.dayOfBirth, valueUpdate: 'afterkeydown'">
   
        <input type="number" pattern="\d*" id="monthOfBirth" name="monthOfBirth" class="fdatepicker" placeholder="MM" maxlength="2" data-bind="value: Register.monthOfBirth, valueUpdate: 'afterkeydown'">

          <div id="icon"></div>
        <input type="number" pattern="\d*" id="yearOfBirth" name="selectedYearOfBirth" class="fdatepicker" placeholder="YYYY" maxlength="4" pattern="\d{4}" data-bind="value: Register.selectedYearOfBirth, valueUpdate: 'afterkeydown'">
        
        <span class="error" data-bind="validationMessage: Register.DOB"></span>
      </div>
</div>

   
   
<!-- end step 1 -->
</fieldset>

    <!-- 2 STEP   -->
<fieldset class="step" style="display: none" data-bind="visible: Register.currentStep() == 2">
    

    <!-- CURRENCY -->
       <div class="field-wrap">
     <span class="field-wrap__label">Currency</span>
           <div id="icon"></div>
     <select class="textField field-wrap__field" data-bind="options: Register.currencies, optionsCaption: 'Currency', optionsValue: 'Code', optionsText: 'Desc', value: Register.selectedCurrency" class="textField"></select> 
    </div>

        <!--   <ul class="registration__title">
        <label for="title">Select currency</label>
        <li  class="ticked"><button>GBP</button></li>
        <li><button>EUR</button></li>
        <li><button>CAD</button></li>
     
      </ul> -->

 

     <!-- COUNTRY -->
    <div class="field-wrap">
     <span class="field-wrap__label">Country</span>
        <div id="icon"></div>
    <select class="textField field-wrap__field" data-bind="options: Register.countries, optionsCaption: 'Choose Country', optionsValue: 'Code', optionsText: 'Desc', value: Register.selectedCountry">
        <option value="GBR">UNITED KINGDOM </option>
    </select>
    </div>



         <!-- MOBILE PHONE -->
   <!--     <div class="field-wrap">
          <span class="field-wrap__label">Landline</span>
    <input  class="textField field-wrap__field" data-bind="value: Register.mobilePhone" type="tel" placeholder="Your landline number" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
    </div> -->

         <!-- MOBILE PHONE -->
       <div class="field-wrap">
          <span class="field-wrap__label">Mobile*</span>
          <input id="ui-phone" class="phone field-wrap__field" data-bind="value: Register.mobilePhone" type="tel" placeholder=" " autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
    </div>
    
    <!-- POST CODE -->
    <div class="field-wrap">
     <span class="field-wrap__label">Post Code</span>
        <div id="icon"></div>
      <input class="post-code field-wrap__field" data-bind="value: Register.postcode, valueUpdate: ['input', 'afterkeydown']" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <a href="#" data-nohijack="true" data-bind="visible: Register.showFindAddress, click: Register.postcodeLookup, css: {active: Register.postcodeIsValid}" class="button postfix findaddress" >Find Address</a>
       </div>


<!-- Show this section is postcode is vaild -->
      <div style="display: none" data-bind="visible: Register.postcodeIsValid">

          <select data-bind="visible:  Register.addresses().length > 0,
          options: Register.addresses,
          optionsCaption: 'Please select',
          value: Register.selectedAddress1" class="textField border-important">
        </select>

         <div class="field-wrap" data-bind="visible: Register.postcodeIsValid && Register.addresses().length === 0">
        
         <span class="field-wrap__label">Address Line 1</span>
             <div id="icon"></div>
        <input class="field-wrap__field" data-bind="value: Register.address1" class="textField" type="text"
        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        </div>
        
         <div class="field-wrap">
          <span class="field-wrap__label">Address Line 2</span>
             <div id="icon"></div>
        <input class="field-wrap__field" data-bind="value: Register.address2" class="textField" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        </div>

         <div class="field-wrap">
                <span class="field-wrap__label">City</span>
             <div id="icon"></div>
        <input class="field-wrap__field" data-bind="value: Register.city" class="textField" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        </div>
        
         <div class="field-wrap">
           <span class="field-wrap__label">County/State</span>
             <div id="icon"></div>
        <input class="field-wrap__field" data-bind="value: Register.state" class="textField" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      </div>
      </div>

    <!-- end step 2 -->
  </fieldset>
    
    


    <!-- 3 STEP  -->
<fieldset class="step" style="display: none" data-bind="visible: Register.currentStep() == 3">
  

    <!-- PROMO CODE -->
     <div class="field-wrap">
        <span class="field-wrap__label">Promo Code (optional)</span>
         <div id="icon"></div>
    <input class="textField field-wrap__field" data-bind="value: Register.promoCode"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
    </div>

    <!-- MAILING OPT -->
    <ul class="mailing-opt">
      <p>Please keep me up to date with promotions, new games and other information:</p>

      <!-- EMAIL -->
      <li>
        <label data-bind="css: {ticked: Register.wantsEmail}" for="cbEmail">
          <input data-bind="checked: Register.wantsEmail" type="checkbox" checked id="cbEmail">
          <span class="custom checkbox"></span>
          By E-mail
        </label>
             <!-- SMS -->
             <label data-bind="css: {ticked: Register.wantsSMS}" for="cbSms">
          <input data-bind="checked: Register.wantsSMS" type="checkbox" checked id="cbSms">
          <span class="custom checkbox"></span>
           By SMS
        </label>
      </li>

 


      <!-- OVER 18 -->
     <p>Please tick below to verify that you are 18 or over and agree with the terms and conditions *</p>
      <li>
        <label data-bind="css: {ticked: Register.over18}" for="cbOver18" class="terms-over18">
        <input data-bind="checked: Register.over18" type="checkbox" id="cbOver18" name="cbOver18">
          <span class="custom checkbox"></span>
          I am 18 or over and agree to <a data-nohijack="true" class="register-3step-tc" href="/terms-and-conditions/" target="terms">T&C'S</a>
        </label>
      </li>
    <!--   <p data-bind="validationMessage: Register.over18"></p> -->     <span class="error" data-bind=" validationMessage: Register.DOBvOver18"></span>

    </ul>
    <!-- /MAILING OPT -->
<!-- end step 3 -->
</fieldset>
    <!-- CTA -->
     
    <a class="button register-3step-cta next create-account-cta" data-nohijack="true" data-bind="click: Register.doRegistration, visible: Register.totalSteps() == Register.currentStep()">JOIN NOW</a>
   <a class="registerNextCta" data-bind="click: Register.nextStep, visible:  Register.totalSteps() > 0 && Register.totalSteps() > Register.currentStep() ">Next</a>
</form>

</div>
<!-- /3 STEP REGISTRATION FORM -->
