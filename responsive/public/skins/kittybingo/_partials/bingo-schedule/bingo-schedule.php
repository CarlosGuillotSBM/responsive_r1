<script type="application/javascript">
    skinModules.push({id: "BingoScheduler"});
</script>


<div class="bingo-schedule">
    <!-- Bingo Category Tabs -->
    <div class="bingo-schedule__tabs">
        <dl class="tabs" data-tab role="tablist">
            <dd data-bind="click: BingoScheduler.setFilter.bind($data,'ALL') , css: { active: BingoScheduler.filter() == 'ALL'}"><a class="ui-bingo-filter-all" href="#bingo-schedule-cat1" >All</a></dd>
            <dd data-bind="click: BingoScheduler.setFilter.bind($data,'90') , css: { active: BingoScheduler.filter() == '90'}"><a class="ui-bingo-filter-90" href="#bingo-schedule-cat2" >Bingo 90</a></dd>
            <dd data-bind="click: BingoScheduler.setFilter.bind($data,'75') , css: { active: BingoScheduler.filter() == '75'}"><a class="ui-bingo-filter-75" href="#bingo-schedule-cat3" >Bingo 75</a></dd>
            <dd data-bind="click: BingoScheduler.setFilter.bind($data,'5L') , css: { active: BingoScheduler.filter() == '5L'}"><a class="ui-bingo-filter-5L" href="#bingo-schedule-cat4" >Bingo 5L</a></dd>
            <dd data-bind="click: BingoScheduler.setFilter.bind($data,'PRE') , css: { active: BingoScheduler.filter() == 'PRE'}"><a class="ui-bingo-filter-prebuy" href="#bingo-schedule-cat5" >Prebuys</a></dd>
        </dl>
    </div>

    <!-- End Bingo Category Tabs -->
    <ul class="bingo-schedule__header">
        <li class="bingo-schedule__header__title room-name-col">Room</li>
<!--        <li class="bingo-schedule__header__title feature-col">Feature</li>-->
        <li class="bingo-schedule__header__title jackpot-col">Jackpot</li>
        <li class="bingo-schedule__header__title price-col">Price</li>
        <li class="bingo-schedule__header__title starts-col">Starts In</li>
        <li class="bingo-schedule__header__title play-col"></li>
    </ul>

    <div class="bingo-schedule__rows">

        <!--  Bingo Schedule Single Row -->

        <div class="tabs-content">

            <!-- ALL -->
            <div data-bind="foreach: BingoScheduler.openedRooms, visible: BingoScheduler.filter() == 'ALL'">

          

              <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

      

            </div>

            <!-- 90 -->
            <div data-bind="foreach: BingoScheduler.rooms90, visible: BingoScheduler.filter() == '90'"
                 class="content ui-bingo-schedule ui-bingo-90" id="bingo-schedule-cat2" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>

            <!-- 75 -->
            <div data-bind="foreach: BingoScheduler.rooms75, visible: BingoScheduler.filter() == '75'"
                 class="content ui-bingo-schedule ui-bingo-75" id="bingo-schedule-cat3" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>


            <!-- 5L -->
            <div data-bind="foreach: BingoScheduler.rooms5L, visible: BingoScheduler.filter() == '5L'"
                 class="content ui-bingo-schedule ui-bingo-5l" id="bingo-schedule-cat4" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>


            <!-- PRE -->
            <div data-bind="foreach: BingoScheduler.roomsPre, visible: BingoScheduler.filter() == 'PRE'"
                 class="content ui-bingo-schedule ui-bingo-prebuy" id="bingo-schedule-cat5" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>


        </div>
    </div>
    <div class="clearfix"></div>

</div>