<div class="ui-adaptive-banner ui-adaptive-wrapper show-for-large-up" data-device='<?=config("RealDevice")?>' data-adaptive-name='<?=$adaptive_banner_name?>'>
    <a data-gtag="Join Now,Web Promo" class='ui-adaptive-link'  href="#">
        <img alt="loading" data-gtag="Join Now," class="banner ui-adaptive ui-adaptive-img" src="/_images/common/kb-holding-image.png">
        <div class="slider-text-link adaptive-banner--tc">
            <a href="#" class='ui-adaptive-tclink'>
                <p class="slider-text-link-copyright ui-adaptive-text">Loading...</p>
            </a>
        </div>
    </a>
</div>
<div class="ui-adaptive-banner ui-adaptive-wrapper hide-for-large-up" data-adaptive-name='<?=$adaptive_banner_name?>'>
  <a data-gtag="Join Now,Web Promo" class='ui-adaptive-link' href="#">
        <img alt="loading" data-gtag="Join Now," class="banner ui-adaptive ui-adaptive-img" src="/_images/common/kb-holding-image.png">
        <a class="adaptive-banner--tc copyright-text-link  ui-adaptive-tclink">
            <p class="ui-adaptive-text">Loading...</p>
        </a>
    </a>
</div>