<script type="application/javascript">

    skinPlugins.push({
        id: "Slick",
        options: {
            slideWidth: <?php if (config("RealDevice") == 1) echo 350; else if (config("RealDevice") == 2) echo 310; else echo 600?>,
            responsive: true,
            minSlides: 2,
            maxSlides: <?php if (config("RealDevice") == 1) echo 3; else if (config("RealDevice") == 2) echo 3; else echo 2?>,
            slideMargin: 10,
            auto: true,
            infinite: true,
            element: "bxslider-winners",
            arrows: false
        }
    });

</script>

<div class="latest-winners-slider">
    <?php edit($this->controller,'winners-feed'); ?>
        <?php //var_dump($this->content["winners-feed"]); ?>
    <ul class="bxslider bxslider-winners"> 
        <?php @$this->repeatData($this->content["winners-feed"],0,"_partials/community/community-latest-winners-slide.php"); ?>
    </ul>
</div>