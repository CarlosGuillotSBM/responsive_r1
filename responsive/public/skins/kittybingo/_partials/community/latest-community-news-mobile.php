<script type="application/javascript">

    skinPlugins.push({
        id: "Slick",
        options: {
            slideWidth: <?php if (config("RealDevice") == 1) echo 310; else if (config("RealDevice") == 2) echo 360; else echo 600?>,
            responsive: true,
            minSlides: 1,
            maxSlides: <?php if (config("RealDevice") == 1) echo 3; else if (config("RealDevice") == 2) echo 2; else echo 1?>,
            slideMargin: 20,
            auto: true,
            infinite: true,
            element: 'bxslider-community-news'
        }
    });

</script>

<!-- CONTENT HOLDER WRAP MOBILE -->
<div class="holder-mobile">

	<!-- Title -->
	<h1 class="title-mobile">LATEST COMMUNITY NEWS</h1>

	<!-- Content Wrap -->
	<div class="content-holder-mobile">
		<!-- <ul class="bxslider"> -->
		<ul class="bxslider-community-news">
			<?php // @$this->repeatData($this->content["unmissable-promos-mobile"],0,"_partials/community/latest-community-news-for-mobile-slide.php"); ?>
            <?php include'_partials/community/latest-community-news-mobile-slide.php'; ?>
		</ul>
	</div>
	<!-- /Content Wrap -->

</div>
<!-- /CONTENT HOLDER WRAP MOBILE -->