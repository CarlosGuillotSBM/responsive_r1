<script type="application/javascript">

    skinPlugins.push({
        id: "Slick",
        options: {
            slideWidth: <?php if (config("RealDevice") == 1) echo 310; else if (config("RealDevice") == 2) echo 360; else echo 600?>,
            responsive: true,
            minSlides: 1,
            maxSlides: <?php if (config("RealDevice") == 1) echo 3; else if (config("RealDevice") == 2) echo 2; else echo 1?>,
            slideMargin: 20,
            auto: true,
            infinite: true,
            dots: false,
            element: 'bxslider-promo'
        }
    });

</script>

<div class="promos-slider">
    <ul class="bxslider bxslider-promo">
        <?php @$this->repeatData($this->content["promotions"],0,"_partials/promos/promo-slide.php"); ?>
    </ul>
</div>
