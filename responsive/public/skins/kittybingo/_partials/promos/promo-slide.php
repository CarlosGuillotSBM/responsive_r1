<script type="application/javascript">
    skinModules.push({
        id: "RadFilter"
    });
</script>
<?php
$pFilter=$row["Text5"];

?>
<!-- PROMO -->
<li style="display: none" data-filterclass="promotion-eng" data-playerfilter="<?php echo $pFilter?>">
    <div class="bxslider__item" >
        <a data-hijack="true" data-gtag="Promo Slide" onclick="location.href='/promotions/<?php echo $row["DetailsURL"]?>/'">
            <img class="promo__image" src="<?php echo @$row['Image']; ?>" alt="<?php echo @$row['Alt']; ?>" >
        </a>
    </div>

    <div class="promotion__text_terms-wrapper">
            <?php
                //Limiting the Terms and Conditions text from the database to 135 characters
                $TCstring = $row["Text6"]; 
                $TCstring = (strlen($TCstring) > 135) ? substr($TCstring,0,135).'...' : $TCstring; 
            ?>
          <span class="promotion__text_terms"><?php echo $TCstring ?></span><a class="promotion-eng__terms" data-reveal-id="promo-terms-and-conditions-modal-<?php echo $row["ID"]; ?>"> T&amp;Cs apply &#10095;</a>
        </div>

</li>

