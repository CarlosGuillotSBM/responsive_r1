<div class="myaccount-details__edit-landline">
	<div class="edit-wrap">
		<div class="edit-wrap__inner">
			<!-- Extention -->
			<div class="edit-row-field">
				<label>Landline:</label>
				<input class="landline" placeholder="Landline phone" data-bind="value: PersonalDetailsNew.landPhone" type="text" maxlength="50">
			</div>
			<!-- Password Confirmation -->
			<label>For security purposes enter your password.</label>
			<div class="edit-row">
				<input class="password" data-bind="value: PersonalDetailsNew.newPassword" type="password" maxlength="15" placeholder="Password">
				<div class="show-password" data-bind="click: LoginBox.togglePassword"><a class="right" data-bind="text: LoginBox.passwordShowText">Show</a></div>
				<span data-bind="visible: PersonalDetailsNew.invalidNewPassword" class="error">Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces.</span>
			</div>
			<!-- Buttons -->
			<div class="edit-row">
				<a data-bind="click: PersonalDetailsNew.updateDetails" href="" class="button confirm">Confirm</a>
				<a data-bind="click: PersonalDetailsNew.updateDetails" href="" class="button cancel">Cancel</a>
			</div>
			
		</div>
	</div>
</div>
<div class="myaccount-details__edit-landline-bg"></div>