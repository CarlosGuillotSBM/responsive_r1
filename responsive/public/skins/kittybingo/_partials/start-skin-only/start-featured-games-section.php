<script>

    skinPlugins.push({
        id: "FeaturedGames",
        options: {
            global: true,
            auto: false
        }
    });

</script>


<!-- Featured Games Box with tabbed content -->
<div class="lobby-wrap__content-box__header heading--skin">
	<?php edit($this->controller,'start-featured-games-section', "", "margin-top: 70px;"); ?>
	<?php @$this->repeatData($this->content['start-featured-games-section']);?>
</div>
<!-- end featured games -->