<!--  My Promotions -->
<div class="lobby-wrap__content-box">
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-promotions.php'; ?>
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text">BINGO SCHEDULE</div>
            <div class="title__link">
                <a href="/promotions/">Play Bingo <span class="title__link__arrow">&rtrif; </span></a>
            </div>
        </div>
    </div>
    <!-- end header bar -->
    <div class="lobby-wrap__content-box__content ">
             <?php  include'_partials/bingo-schedule/bingo-schedule.php'; ?>
    </div>
</div>
<!-- End My Promotions -->