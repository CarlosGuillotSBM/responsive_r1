<script type="application/javascript">
var sbm = sbm || {};
sbm.games = sbm.games || [];
sbm.games.push({
"id"    : <?php echo json_encode(@$this->row['DetailsURL']);?>,
"icon"  : "<?php echo @$this->row['Image'];?>",
"bg"    : <?php echo json_encode(@$this->row['Text1']);?>,
"title" : <?php echo json_encode(@$this->row['Title']);?>,
"type"  : "game",
"desc"  : <?php echo json_encode(@$this->row['Intro']);?>,
"thumb" : "<?php echo @$this->row['GameScreen'];?>",
"detUrl": <?php echo json_encode(@$this->row['DetailsURL']);?>
});
</script>
<div class="main-content game-detail-content">
    <!-- Title -->
    <div class="section-left__title">
        <h4><?php echo @$this->row["Title"];?></h4>
    </div>
    <!-- wrap -->
    <section class="content-holder">
        <!-- inner wrap -->
        <div class="content-holder__inner">
    
                <!-- GAME SLIDER -->
                <div class="game-detail-content__game">
                    <?php include '_partials/games/game-slider.php'; ?>
                </div>


                  <!-- CTAS MOBILE -->
                    <span <?php if(@$this->row["Status"]==1) echo 'data-bind="click: GameLauncher.openGame"';?> data-infopage="true" data-gameid="<?php echo @$this->row["DetailsURL"];?>" data-gametitle="<?php echo @$this->row["Title"] ?>" data-icon="<?php echo @$this->row["Image"];?>" data-nohijack="true" class="games-info__try">
                        <!-- PLAY NOW -->
                        <div style="display: none;" data-bind="visible: validSession() && <?php echo @$this->row["Status"];?>" class="overlay">
                            <p alt="Play Now" class="play-now-button--mobile">Play Now</p>
                        </div>
                    </span>

                    <!-- LOGIN BUTTON -->
                    <a style="display: none;" href="/login/" data-bind="visible: !validSession(), click: GameLauncher.openGame" data-gameid="<?php echo @$this->row["DetailsURL"];?>" class="login-button--mobile ui-scroll-top">LOGIN</a>
                    <!-- JOIN NOW -->
                    <a style="display: none;" data-bind="visible: !validSession()" href="/register/" alt="Join Now" class="join-now-button--mobile">Join Now</a>
                    <!-- / CTAS MOBILE -->

                <!-- GAME DETAILS -->
                <aside class="game-detail-content__details-table">
                    <h3>Game Details</h3>
                    <ul>
                        <li><span class='game-detail-content__details__game-table__title'>Publisher</span><span itemprop="publisher" class="game-detail-content__right__game-table__provider"><?php echo @$this->row["ProviderName"];?></span></li>
                        <li><span class='game-detail-content__details__game-table__title'>Paylines</span><?php echo @$this->row["PayLines"];?></li>
                        <li><span class='game-detail-content__details__game-table__title'>Feature</span><?php echo @$this->row["Feature"];?></li>
                        <li><span class='game-detail-content__details__game-table__title'>Minimum Bet</span><?php echo @$this->row["MinBet"];?></li>
                        <li><span class='game-detail-content__details__game-table__title'>Progressive</span><?php echo @$this->row["Progressive"];?></li> 
                        <li><span class='game-detail-content__details__game-table__title'>RTP</span><?php  echo @$this->row["RTP"];?></li>
                    </ul>

                    <!-- LOGIN BUTTON -->
                    <a style="display: none;" href="/login/" data-bind="visible: !validSession(), click: GameLauncher.openGame" data-gameid="<?php echo @$this->row["DetailsURL"];?>" class="login-button--desktop ui-scroll-top">LOGIN</a>
                    <!-- JOIN NOW -->
                    <a style="display: none;" data-bind="visible: !validSession()" href="/register/" alt="Join Now" class="join-now-button--desktop">Join Now</a>
                    <!-- / CTAS MOBILE -->

                     <div style="display: none; cursor: pointer" data-bind="visible: validSession() && <?php echo @$this->row["Status"];?>" class="overlay">
                        <p alt="Play Now" data-bind="click: GameLauncher.openGame" data-gameid="<?php echo @$this->row["DetailsURL"];?>" class="play-now-button--desktop">Play Now</p>
                     </div> 
                </aside>

                <!-- BANNER -->
                <div class="game-detail-content__promo">
                    <?php edit('gamesdetail','aquisition-banner');?>
                    <?php @$this->repeatData($this->content['aquisition-banner']);?>
                </div>               


                <!-- FULL WIDTH AQUISITION BANNER -->
                <!-- <div class="game-detail-content__acquisition">
                    <?php edit('gamesdetail','aquisition-banner-full');?>
                    <?php @$this->repeatData($this->content['aquisition-banner-full']);?>
                </div> -->

                <!-- GAME INFO -->
                <div class="games-detail-content__game-review">
                    <?php edit('gamesdetail','games-detail',$this->row['DetailsURL']); ?>
                    <?php @$this->repeatData($this->content['games-detail']); ?>
                </div>

        </div>
        <!-- inner wrap -->
    </section>
    <!-- /wrap -->
</div>