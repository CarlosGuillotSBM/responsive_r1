<div class="game-detail-modal">

    <!-- GAME TITLE -->
    <a class="game-detail-wrap__title-bar" data-bind="attr: { href: GameLauncher.detailsUrl() + '/' }">
        <h1 data-bind="text: GameLauncher.gameTitle" class="ui-game-title"></h1>
    </a>
    
    <!-- GAME CONTENT -->
    <div class="game-detail-content">

        <!-- GAME IMAGE + GAME DETAILS LINK -->
        <div class="game-detail-content__left">
            <span data-bind="attr: { 'data-gameid': GameLauncher.gameId, 'data-demoplay': GameLauncher.demoPlay }" data-nohijack="true" class="games-info__try">
                <img data-bind="attr: { src: GameLauncher.icon }">
                <!--img class="free-spins" src="/_images/common/game-free-spins.svg"-->

                <!-- IMAGE CTA -->
                <div class="game-cta">
<!--                 <a href="/login/" data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login">Login</a> -->
                    <!--  <a alt="Try Now" data-bind="visible: GameLauncher.demoPlay, click: GameLauncher.tryGame.bind($data, false)" class="try-game">Try Game</a> -->
                     <a href="/register/" data-bind="visible: GameLauncher.demoPlay" alt="Join Now" class="join-now-full">Join Now</a>
                     <a href="/register/" data-bind="visible: !GameLauncher.demoPlay()" alt="Join Now" class="join-now-full">Join Now</a>
                    <!-- <a alt="Try Now" data-bind="visible: GameLauncher.demoPlay, click: GameLauncher.tryGame.bind($data, false)" class="try-game">Try Game</a> -->
                </div>

            </span>
        </div>

		

        <!-- CTA'S -->
        <div class="game-detail-content__right">

            <div class="login-box--loginpage">
                <?php include'_partials/login/login-box.php' ?> 
            </div>
            
            <!-- <span <?php //if(@$this->row["Status"]==1) echo 'data-bind="click: GameLauncher.openGame"';?> data-infopage="true" data-gameid="<?php //echo @$this->row["DetailsURL"];?>" data-gametitle="<?php //echo @$this->row["Title"] ?>" data-icon="<?php //echo @$this->row["Image"];?>" data-nohijack="true" class="games-info__try">
                <div style="display: none; cursor: pointer" data-bind="visible: validSession() && <?php //echo @$this->row["Status"];?>" class="overlay">
                    <a alt="Play Now" class="play-now">Play Now</a>
                </div>
                <div style="display: none; cursor: pointer" data-bind="visible: !validSession() && <?php //echo @$this->row["DemoPlay"];?> && <?php //echo @$this->row["Status"];?>, click: GameLauncher.tryGame.bind($data, '<?php //echo @$this->row["DetailsURL"];?>')" class="overlay">
                    <a alt="Try Now" class="try-now">Try Now</a>
                </div>
            </span> -->
        </div>

    </div>

    <!-- GAME DETAILS -->
    <div class="game-detail-content__details">
        <article  class="game-detail__description">
            <h3>Game Description</h3>
            <p data-bind="html: GameLauncher.gameDescription"></p>
            <a data-bind="attr: { href: GameLauncher.detailsUrl() + '/' }">FULL GAME DETAILS ></a>
        </article>
    </div>

    <!-- / GAME CONTENT -->
  <!--    <div data-bind="html: GameLauncher.gameDescription"></div> -->
</div>