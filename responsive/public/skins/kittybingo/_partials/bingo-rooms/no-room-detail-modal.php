<div class="room-detail-modal">
    <h1 class="room-detail-wrap__title-bar">PROMOS</h1>

    <!-- GAME CONTENT -->
    <div class="room-detail-content">
        <!-- GAME IMAGE + GAME DETAILS LINK -->
        <div class="room-detail-content__left">

            <?php edit($this->controller,'image-promotion-modal'); ?>
            <?php if (isset($this->action) && $this->action !== ""): ?>
                <?php @$this->content = $this->promotions ?>
            <?php endif ?>
            <?php @$this->getPartial($this->content['image-promotion-modal'],1); ?>
            

            <!-- IMAGE CTA -->
            <div class="game-cta">
                 <a href="/register/" alt="Join Now" class="join-now-full">Join Now</a>
            </div>
        </div>

        <div class="room-detail-content__right">
            <!-- LOGIN -->
            <div class="login-box--loginpage">
                <?php include'_partials/login/login-box.php' ?> 
            </div>
        </div>
    </div>
    <!-- / GAME CONTENT -->
</div>
