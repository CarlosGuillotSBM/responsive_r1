<li class="bingo-room__wrapper" style="display: none">
    <a data-gtag="#" data-hijack="true" href="<?php echo $row['Path']; ?>">
        <img class="bingo-room__image" src="<?php echo $row['Image']; ?>" alt="<?php echo $row['Alt']; ?>">
        <div class="bingo-room__content-wrap">
            <h1 class="bingo-room__title"><?php echo $row['Title'];?></h1>
            <div class="bingo-room__content"><?php echo $row['Intro'];?></div>
            <span class="bingo-room__more-info">Read More ></span>
        </div>
    </a>
</li>