<!-- Mobile Top Bar -->
<div class="tab-bar nav-for-mobile">
	<!-- MOBILE LOGO -->
	<a class="site-logo ui-go-home-logo menu-link">
		<h1 class="image-replacement"><?php echo config("Name"); ?></h1>
	</a>
	<!-- MOBILE ICON -->
	<a class="site-logo-mobile ui-go-home-logo">
		<h1 class="image-replacement"><?php echo config("Name"); ?></h1>
	</a>

</div>
<!-- / Mobile Top Bar -->