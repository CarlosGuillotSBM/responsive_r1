<div class="footer-bar-mobile" data-bind="visible: validSession() && startView()" style="display: none">
    <a data-hijack="true" data-bind="click: playBingo" class="cta-play">PLAY BINGO</a>
    <a data-hijack="true" href="/cashier/" class="cta-cashier">DEPOSIT NOW</a>
</div>