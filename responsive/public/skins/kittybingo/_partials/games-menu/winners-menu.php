<script type="application/javascript">
	skinPlugins.push({
		id: "magicalMenu",
		options: {
			global: true
		}
	});
</script>
<div class="games-menu">
	<div class="games-menu__container">
	
		<!-- games nav -->
		<ul class="games-menu__games-nav">
			<li id="special"><a href="/games/" data-hijack="true">Special Winners</a></li>
			<li id="winners"><a href="/games/" data-hijack="true">Winners Stories</a></li>
			<li id="recent"><a href="/games/" data-hijack="true">Recent Winners</a></li>

<!-- /games nav -->
</div>
</div>