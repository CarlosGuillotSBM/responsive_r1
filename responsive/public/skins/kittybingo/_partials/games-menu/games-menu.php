<script type="application/javascript">
	skinPlugins.push({
		id: "magicalMenu",
		options: {
			global: true
		}
	});
</script>
<div class="games-menu">
	<div class="games-menu__container">
<!-- 		<div class="section-left__title">
		    <h4>GAMES</h4>
		</div> -->
		<!-- games nav -->
		<ul class="games-menu__games-nav">
			<li id="allslots"><a href="/games/" data-hijack="true">ALL SLOTS</a></li>
			<li id="table-card"><a href="/table-card/" data-hijack="true">TABLE &amp; CARD GAMES</a></li>
			<li id="scratch-and-arcade" class="custom-hide-for-mobile"><a href="/scratch-and-arcade/" data-hijack="true">Scratch & Arcade</a></li>
			<li id="magicalMenu" class="games-filter-menu">
				<a id="magicalMenuBtnLnk"  data-hijack="false">SLOTS BY TYPE</a>
				<?php include "_partials/games-menu/magic-menu-popover.php"; ?>
			</li>
			<li class="games-menu__games-nav__search"><?php include '_global-library/partials/search/search-input.php'; ?></li>
		</ul>
<!-- /games nav -->
</div>
</div>