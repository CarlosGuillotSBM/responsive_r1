<?php if(Env::isLive()) {?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NL6T8LM');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook pixel only for REGISTRATION page - should only fire once! -->
    <?php
        if(!isset($_COOKIE['fb-reg-tracker']) && strpos($_SERVER['REQUEST_URI'], 'register')) {
            // set the cookie
            // setcookie("fb-reg-tracker", true, time()+60*60*24*45); // expires in 45 days
        }
    ?>
    <!-- End Facebook pixel only for REGISTRATION page - should only fire once! -->

<?php } ?>