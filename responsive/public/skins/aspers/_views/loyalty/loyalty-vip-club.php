
<div class="loyaltycrumbs"> 
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
</div>
<!-- MIDDLE CONTENT -->
<div class="middle-content__box">

  <section class="section">
    <h1 class="section__title"><?php echo $this->controller; ?></h1>
   <!-- CONTENT -->
    <div class="content-template">
      
      <div class="loyalty-top">
        <article class="loyalty-left">
          <?php edit($this->controller,'loyalty-welcome'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome']);?>
        </article>
        <aside class="loyalty-right">
          <?php edit($this->controller,'loyalty-banner'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner']);?>
        </aside>
      </div>
    </div>


 <!-- 1. Become a VIP -->
      <li class="loyalty-vip-wrap">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-1'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-1']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-1'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-1']);?>
        </div>
        <div class="loyalty-vip-icon">
          <img src="/_upload-images/2.custom-size/loyalty/vip-badge2.png">
        </div>
      </li>


 <!-- 2. Perks -->
      <li class="loyalty-vip-wrap">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-2'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-2']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-2'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-2']);?>
        </div>
        <div class="loyalty-vip-icon">
          <img src="/_upload-images/2.custom-size/loyalty/vip-badge2.png">
        </div>
      </li>

      <!-- 3. Vip Promotions -->
      <li class="loyalty-vip-wrap">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-3'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-3']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-3'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-3']);?>
        </div>
        <div class="loyalty-vip-icon">
          <img src="/_upload-images/2.custom-size/loyalty/vip-badge2.png">
        </div>
      </li>

      <!-- 4. Contact us -->
      <li class="loyalty-vip-wrap">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-4'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-4']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-5'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-5']);?>
        </div>
        <div class="loyalty-vip-icon">
          <img src="/_upload-images/2.custom-size/loyalty/vip-badge2.png">
        </div>
      </li>
    </ul>



 <!-- / VIP PROMOTIONS BOX -->

    <!-- T&C'S COLLAPSE -->

    <dl id="ui-promo-tcs" class="loyalty__terms-conditions accordion" data-accordion>

    <?php edit($this->controller,'loyalty-terms-conditions'); ?>
      <dd class="accordion-navigation active">
      <a href="#panelTC">Terms & Conditions <span style="font-size:25px;">&#x2304;</span> </a>
      <div id="panelTC" class="content active">
      
       <?php @$this->getPartial($this->content['loyalty-terms-conditions'],1); ?>
      </div>
      </dd>
    </dl>

  </section>
</div>
<!-- / MIDDLE CONTENT




      </div>
      <div class="cleafix"></div>
    </div>
    <!-- /CONTENT-->
  </section>
</div>