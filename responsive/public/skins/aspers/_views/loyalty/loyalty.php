
<!-- MIDDLE CONTENT -->
<div class="middle-content__box">

<?php  include'_partials/common/aspers-responsivebanner-1.php'; ?>

  <section class="section">
   <!-- CONTENT -->
    <div class="content-template">
      
      <div class="loyalty-top">
        <article class="loyalty-left">
          <?php edit($this->controller,'loyalty-welcome'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome']);?>
        </article>
      </div>
    </div>


 <!-- 1. Become a VIP -->
      <li class="loyalty-vip-wrap">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-1'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-1']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-1'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-1']);?>
        </div>
      </li>


 <!-- 2. Perks -->
      <li class="loyalty-vip-wrap tablet-align">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-2'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-2']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-2'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-2']);?>
        </div>
      </li>

      <!-- 3. Vip Promotions -->
      <li class="loyalty-vip-wrap">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-3'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-3']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-3'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-3']);?>
        </div>
      </li>

      <!-- 4. Contact us -->
      <li class="loyalty-vip-wrap tablet-align contact-us">
        <div class="loyalty-vip-img">
          <?php edit($this->controller,'loyalty-banner-4'); ?>
          <?php @$this->repeatData($this->content['loyalty-banner-4']);?>
        </div>
        <div class="loyalty-vip-content">
          <?php edit($this->controller,'loyalty-welcome-5'); ?>
          <?php @$this->repeatData($this->content['loyalty-welcome-5']);?>
        </div>
      </li>
    </ul>



 <!-- / VIP PROMOTIONS BOX -->

    <!-- T&C'S COLLAPSE -->

    <dl id="ui-promo-tcs" class="loyalty__terms-conditions accordion" data-accordion>

    <?php edit($this->controller,'loyalty-terms-conditions'); ?>
      <dd class="accordion-navigation active">
      <a class="tc-header" href="#panelTC">Terms & Conditions</a>
      <div id="panelTC" class="content active">
      
       <?php @$this->getPartial($this->content['loyalty-terms-conditions'],1); ?>
      </div>
      </dd>
    </dl>

  </section>
</div>
<!-- / MIDDLE CONTENT




      </div>
      <div class="cleafix"></div>
    </div>
    <!-- /CONTENT-->
  </section>
</div>