<!-- MIDDLE CONTENT -->	
<div class="middle-content">
<div class="middle-content__box">
  <?php 
  if (config("RealDevice") == 3) {
  	include '_global-library/partials/games/games-details-page-aspers_mobile.php';
  } else {
  	include '_global-library/partials/games/games-details-page-aspers.php'; 
  }

  ?>
  <div class="games-detail-page__bottom">
    <?php edit('gamesdetail','games-detail',$this->row['DetailsURL']); ?>
    <?php @$this->repeatData($this->content['games-detail']); ?>
  </div>
</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->