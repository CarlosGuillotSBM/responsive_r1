<?php
$page = $this->controller;
if($this->action == 'exclusive') $page.= 'exclusive';
?>
<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <div class=" games-pages">
        <!-- Hub grid -->
        <section class="section games-list middle-content__box ui-scheduled-content-container">
          
        <?php edit($this->controller,'games-latest-offer'); ?>
            <?php @$this->repeatData($this->content['games-latest-offer']); ?>
        </section>
        
        <!-- games list-->
        <?php include '_global-library/partials/games/games-list-aspers.php'; ?>
        <!-- end games list -->

        <!-- END Games info modal -->
    </div>

    <div class="bottom-content">
        <section class="section middle-content__box">
            <?php
                if ($this->action != "slots") {
                    edit($this->controller.$this->action,'footer-seo', $this->action);
                    @$this->getPartial($this->content['footer-seo'], 1); 
                } else {
                    edit($this->controller,'footer-seo',$this->action);
                    @$this->getPartial($this->content['footer-seo'],$this->action); 
                }
            ?>
        </section>
    </div>
</div>
<!-- END MIDDLE CONTENT -->