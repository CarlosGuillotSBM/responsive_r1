<!-- MIDDLE CONTENT --> 
<div class="middle-content faq--page">
<div class="middle-content__box">
  <section class="section">
    <h1 class="section__title"><?php echo $this->controller; ?></h1>
    <!-- CONTENT -->
    <?php edit($this->controller,'faq'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['faq']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->