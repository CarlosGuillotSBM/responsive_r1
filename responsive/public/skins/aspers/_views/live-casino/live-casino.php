<?php
// the hub data is common for all the same controller
// special exception for exclusive slots so it can have it's own hub
$page = $this->controller;
if($this->action == 'exclusive') $page.= 'exclusive';
?>
<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <div class="games-pages">

          <!-- banner -->
          <div class="middle-content__box spacing-bottom">
    <div class="games-content__banner ui-scheduled-content-container">
        <!-- Edit point Banner -->
        <?php edit($this->controller,'games-latest-offer'); ?>
        <?php @$this->repeatData($this->content['games-latest-offer']); ?>
    </div>
</div>
    
        <div class="middle-content__box">
            <div class="live-casino-banner">
                <?php edit($this->controller,'live-casino-main-offer-banner', $this->action); ?>
                <?php @$this->repeatData($this->content['live-casino-main-offer-banner']);?>
            </div>
        </div>

        <!-- Hub grid -->
        <section  class="section games-list live-casino-list middle-content__box">
            <?php edit($page,'acquisition-title');?>
            <?php @$this->getPartial($this->content['acquisition-title'], 1); ?>

            <?php edit($page,'acquisition');
                if(is_array(@$this->content['acquisition']) && count(@$this->content['acquisition']) > 0):
            ?>

            <div class=" games-list middle-content__box top-games-hub">
                <?php
                $hub = 'acquisition';
                include "_global-library/partials/hub/hub-grid-9.php"
                ?>
            </div>
        </section>
        <?php endif;?>
        <!-- games list-->
        <?php include '_partials/slots/games-list.php'; ?>
        <!-- end games list -->
        <!-- END Games info modal -->
    </div>

    <div class="bottom-content">
        <section class="section middle-content__box">
            <?php
                if ($this->action == 'exclusive') {
                    edit($this->controller,'footer-seo',$this->action);
                } else {
                    edit($this->controller,'footer-seo',$this->action);
                }
            ?>
            <?php @$this->getPartial($this->content['footer-seo'],$this->action); ?>
        </section>
    </div>

</div>
<!-- END MIDDLE CONTENT -->