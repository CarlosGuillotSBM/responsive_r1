<script type="application/javascript">
     skinModules.push( { id: "ClaimCashback" } );
     skinModules.push( { id: "ClaimUpgradeBonus" } );
</script>
<!-- Lobby Wrap -->
<div class="middle-content lobby-wrap start-page">

     <?php  include'_partials/common/aspers-responsivebanner-1.php'; ?>
     <!-- Lobby Left Column -->
     <div class="lobby-wrap__left">
          <!-- lobby claim cashback button -->
          <?php  include'_global-library/partials/start/start-claim-cashback.php'; ?>
          <?php  include'_global-library/partials/start/start-claim-upgrade-bonus.php'; ?>
          <!-- welcome section -->
          <!-- <?php  include'_global-library/partials/start/start-right-banner.php'; ?> -->
          <!-- featured games section -->
          <?php  include'_global-library/partials/start/start-featured-games-section.php'; ?>
          <!-- my messages section -->
          <?php if(config("inbox-msg-on")): ?>
               <?php  include'_partials/lobby/lobby-my-messages-section.php'; ?>
          <?php endif; ?>
          <!-- my promotions section -->
          <?php  include'_global-library/partials/start/start-my-promotions-section.php'; ?>
     </div>
     <!-- End Left Column -->
     <!-- Right Column -->
     <div class="game-url-seo lobby-wrap__right">
          <div class="two-columns-content-wrap__right">
               <!-- lobby right progressives -->
               <?php  include'_global-library/partials/start/start-right-progressives.php'; ?>
               <div class="promo__jackpot">
                    <div class="promo__jackpot--wrapper">
                         <?php edit($this->controller,'home-progressive-jackpot-group'); ?>
                         <?php @$this->getPartial($this->content['home-progressive-jackpot-group'],1); ?>
                    </div>
               </div>
               <!-- lobby right latest winners -->
               <?php  include'_partials/start/start-right-latest-winners.php'; ?>
          </div>
     </div>
     <!-- End Right Column -->
</div>
<!-- End Lobby Wrap -->

<?php 
    // dont miss out popup
    include "_global-library/partials/modals/dont-miss-out.php";
?>
