<div class="middle-content chat--unavailable">
  <div class="middle-content__box">
  <section class="section">
    <!-- CONTENT -->
    <?php edit($this->controller,'chat-unavailable'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['chat-unavailable']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
  </div>
</div>
<!-- END MIDDLE CONTENT -->