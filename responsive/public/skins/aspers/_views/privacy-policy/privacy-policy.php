<!-- MIDDLE CONTENT -->
<div class="middle-content privacy--policy">
  <!-- MIDDLE CONTENT -->
  <div class="middle-content__box">
    <section class="section">
      <h1 class="section__title"><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
      <!-- CONTENT -->
      <?php edit($this->controller,'privacy-policy'); ?>
      <div class="content-template">
        
        <!-- repeatable content -->
        <?php @$this->repeatData($this->content['privacy-policy']);?>
        <!-- /repeatable content -->
      </div>
      <!-- /CONTENT-->
    </section>
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->