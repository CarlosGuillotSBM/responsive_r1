<?php
	$titleHeaderHeight = 25;
	$slideHeight = 82;
	$slidesToShow = 3;
?>
<script type="application/javascript">
	skinPlugins.push({
		id: "ProgressiveSlider",
		options: {
			slidesToShow: <?php echo $slidesToShow ?>,
			mode: "vertical"
		}
	});
	skinPlugins.push({
		id: "WinnersSlider",
		options: {
			slidesToShow: <?php echo $slidesToShow ?>,
			mode: "vertical"
		}
	});
</script>



<!-- MIDDLE CONTENT -->	
<div class="middle-content">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<!-- MIDDLE CONTENT -->
<div class="middle-content__box community">
	<section class="section">
		<h1 class="community-heading">COMMUNITY</h1>
		<div class="two-columns-content-wrap">
			<!-- community nav -->
			
			<?php include "_partials/community-nav/community-nav.php" ?>
			<!-- /community nav -->
			<!-- left column -->
			<div class="two-columns-content-wrap__left">
				<div class="section__header">
					<h1 class="section__title">COMMUNITY WINNERS</h1>
				</div>
				<?php edit($this->controller,'winners'); ?>
				<!-- repeatable content -->
				<?php @$this->repeatData($this->content['winners']);?>
				<!-- /repeatable content -->
			</div>
			<!--  /left column -->
			<!-- right column -->
<div class="two-columns-content-wrap__right">
		<?php if (config("RealDevice") != 3) { ?>
				<?php edit($this->controller,'blog-side-content'); ?>
				<!-- repeatable content -->
				<?php @$this->repeatData($this->content['blog-side-content']);?>
				<!-- /repeatable content -->
				<!-- temporary static html prog jackpots -->
				<div class="progressive-jackpots__wrapper slider">

					<img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
					
					<article class="progressive-jackpots" style="height: <?php echo ($slidesToShow * $slideHeight + $titleHeaderHeight). 'px' ?> ; overflow: hidden">
						
						<div class="progressive-jackpots__title">
							<h1>Progressive Jackpots</h1>
						</div>

						<?php edit($this->controller,'progressives'); ?>
						
						<?php @$this->repeatData($this->content['progressives']);?>
						
					</article>
				</div>
				<!-- /temporary static html prog jackpots -->
				<!-- temporary static html yesterdays winning -->
				<div class="yesterdays-win__wrapper">
					
					<img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
					
					<article class="yesterdays-win">
						
						<div class="yesterdays-win__title">
							<h1>Yesterdays Total Winnings</h1>
						</div>
						
						<div class="yesterdays-win__box">
							<div class="yesterdays-win__box__amount"><span>&pound;<?php echo @$this->winners_total[0]['Win'];?></span>
						</div>
					</div>
				</article>
			</div>
			<!--/ temporary static html yesterdays winning -->
						<!-- latest winners -->
				<div class="latest-winners__wrapper slider">
					<img class="hub-shape-holder" src="/_images/common/4x3-bg.png">

					<article class="latest-winners" style="height: <?php echo ($slidesToShow * $slideHeight + $titleHeaderHeight). 'px' ?> ; overflow: hidden">

						<div class="latest-winners__title">
							<h1>Recent Winners</h1>
						</div>
						<?php edit($this->controller,'latest-winners'); ?>
						<?php @$this->repeatData($this->content['latest-winners']);?>
					</article>
				</div>
				<!-- /latest winners -->
			<?php } ?>
		</div>
	
			<!-- /right column -->
		</section>
	</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->