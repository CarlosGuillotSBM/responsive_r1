<!-- CONTENT -->
<main class="aspers--homepage">
    <section class="row animated delay-start-hero wow fadeInDown" data-wow-delay="1s" data-wow-duration=".3s">
      <!-- Hero Banner -->
        <?php edit($this->controller,'hero'); ?>
        <?php @$this->repeatData($this->content['hero'],1); ?>
        <div class="hero-animation-container">
            <!-- <span class="aspers-main-hero-elephant wow bounceInUp show-for-large-up" data-wow-delay="1.7s" data-wow-duration=".5s"></span> -->
            <!-- <span class="aspers-main-hero-flower-fly wow bounceInUp show-for-large-up" data-wow-delay="1.8s" data-wow-duration=".5s"></span> -->
            <!-- <span class="aspers-main-hero-gonzo wow fadeIn" data-wow-delay="1.9s" data-wow-duration="1s"></span> -->
            <!-- <span class="aspers-main-hero-starburst wow bounceInUp show-for-large-up" data-wow-delay="2s" data-wow-duration=".5s"></span> -->
            <!--
            <span class="aspers-main-hero-dice animated delay-12-hero-content wow bounceInUp"></span>
            <span class="aspers-main-hero-tiger animated delay-11-hero-content wow fadeIn"></span>
            <span class="aspers-main-hero-twin-spin-normal animated delay-13-hero-content wow bounceInUp show-for-large-up" data-wow-delay="2.4s" data-wow-duration=".5s"></span>
            -->

            <!-- overlay --> 
            <span class="overlay-modal hide-for-small-only"></span>

            <!-- overlay --> 
            <span class="overlay-bg-mobile hide-for-large-up"></span>
        </div>
    </section>

    <!-- Banner divider -->
    <section class="divider-all">
        <!-- Don't miss -->
        <div class="divider__dont-miss small-6 large-6">
            <h3>
              <img src="/_images/home/icons/dont-miss.svg" alt="Don't miss">Don't miss
            </h3>
        </div>
        <!-- / Don't miss -->
        <!-- View all promotions -->
        <div class="small-6 large-6 divider-view-all">
            <a href="/promotions/">view all promotions</a>
        </div>
        <!-- / View all promotions -->
      </section>
     <!-- / Banner divider -->

     <!-- Secondary Banners -->
     <section class="row secondary-banner-wrapper animated secondary-banners wow fadeIn" data-wow-delay=".5s" data-wow-duration=".5s">
        <?php edit($this->controller,'promo-adaptive'); ?>
        <?php @$this->repeatData($this->content['promo-adaptive'],1); ?>
     </section>
    <!-- / Secondary Banners -->

     <!-- Top Picks -->
     <section class="top-picks">
          <div class="divider__top-picks small-12">
              <h3>
                <img src="/_images/home/icons/top-picks.svg" alt="Top Picks">
                top picks
              </h3>
          </div>
      <div class="aspers-grid-wrapper">
          <div class="grid aspers">
             <?php edit($this->controller,'new-games'); ?>
                 <?php
                 $hub = 'new-games';
                 include "_global-library/partials/hub/hub-grid-15.php"?>
          </div>
      </div>
    <!-- / Top Picks -->
    </section>

    <!-- Jackpot Widget -->
    <section class="promos-container wow fadeIn" data-wow-delay=".1s" data-wow-duration=".5s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.1s; animation-name: fadeIn;">
      <article class="columns small-12 medium-12 large-12 promo-jackpot grid-x align-middle">
          <?php edit($this->controller,'home-progressive-jackpot-group'); ?>
          <?php @$this->getPartial($this->content['home-progressive-jackpot-group'],1); ?>
      </article>
         <span class="overlay-modal hide-for-small-only"></span>
    </section>
    <!-- Jackpot Widget -->
    
    <!-- SEO CONTENT -->
     <section class="section middle-content__box">
        <!-- content -->

        <div class="ui-accordion">
          <?php
            $seoKey = "home-seo-content";
            include'_global-library/partials/home-seo/mobile.php';  
          ?>
        </div>
     
        <!-- /content -->
     </section>
    <!-- /SEO CONTENT -->

</main>
<!-- / CONTENT -->
