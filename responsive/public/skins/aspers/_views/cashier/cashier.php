<!-- MIDDLE CONTENT -->
<div class="middle-content">
     <div class="middle-content__box">
          <section class="section">
               <div class="cashier__secure-payment"><img src="/_images/cashier/secure-payment.png"></div>
               <!-- CONTENT -->
               <div class="content-template">
                    <!-- PRD CASHIER -->
                    <script type="application/javascript">
                         skinModules.push( { id: "Cashier", options: { source: "Cas" } } );
                    </script>
                    <?php include '_global-library/partials/cashier/cashier.php'; ?>
                    <!-- /PRD CASHIER -->
               </div>
          </section>
     </div>
     <!-- /CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->
