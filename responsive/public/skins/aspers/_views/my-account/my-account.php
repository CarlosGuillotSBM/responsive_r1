<script type="application/javascript">
	skinModules.push( { id: "AccountActivity" } );
	skinModules.push( { id: "PersonalDetails" } );
	skinModules.push( { id: "PlayerAccount" } );
	skinModules.push( { id: "ConvertPoints", options: { name: "Moolahs", url: "/loyalty/vip-club/" } });
	skinModules.push( { id: "PlayerReferAFriend" } );
</script>

<!-- MIDDLE CONTENT -->
<div class="middle-content myaccount-content middle-content__box">

	<div class="tabs--wrapper">
		<dl class="tabs" data-tab>
			<dd class="first active"><a id="ui-redeem-tab" href="#myaccount-5">Claim Code</a></dd>
			<dd><a id="ui-account-activity-tab" href="#myaccount-3" data-bind="click: AccountActivity.getDefaultData">Account Activity</a></dd>
			<dd><a id="ui-convert-points-tab" class="convert-points-tab" href="#myaccount-4">Points</a></dd>
			<dd class="last"><a id="ui-my-details-tab" href="#myaccount-2" data-bind="click: PersonalDetails.getPersonalDetails">Personal Details</a></dd>
			<!-- <dd class="first active"><a href="#myaccount-1" data-bind="click: PersonalDetails.getPlayerAccount">Account Balances</a></dd> -->
		</dl>
	</div>
	<div class="tabs-content">			
					<!-- tab  content 1 -->
			<!-- 	<div class="content active" id="myaccount-1">
			<?php //include '_global-library/partials/my-account/myaccount-balances.php' ?>
			<?php //include '_global-library/partials/my-account/myaccount-promotions.php' ?>
		</div> --><!-- end  tab 1 -->
		<!-- end tab content 2 -->
		<!-- tab content 2 -->
		<div class="content" id="myaccount-3">
			<?php include '_global-library/partials/my-account/myaccount-transactions.php' ?>
		</div>
		<!-- end  tab content 2 -->
		<!-- tab content 3-->
		<div class="content" id="myaccount-2">
			<?php include '_global-library/partials/my-account/myaccount-details-aspers.php' ?>
		</div>

		<!--Points Tab-->
		<div class="content" id="myaccount-4">
			<?php include '_partials/my-account/my-account_convert-points.php' ?>
		</div>
		<!-- tab content 5 -->
		<div class="content active" id="myaccount-5">
			<?php include '_global-library/partials/my-account/myaccount-redeem.php' ?>
		</div>
		<!-- end  tab content 5 -->
		<!--			<div class="content" id="myaccount-6">-->
		<!--				--><?php //include '_global-library/partials/my-account/myaccount-refer-a-friend.php' ?>
		<!--			</div>-->
		<!-- end  tab content 5 -->

		</div><!-- tabs-content -->
</div>
	<!-- END MIDDLE CONTENT CENTRE COLUMN -->