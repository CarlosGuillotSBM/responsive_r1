<script type="application/javascript">
     skinModules.push( { id: "CashierPRD", options: { source: "PRD"} });
</script>

<!-- OFFERS -->
<section class="deposit" data-bind="visible: CashierPRD.visible() && CashierPRD.isNotVip()" style="display: none">
     <h2>
          <?php edit($this->controller,'welcome-deposit-offers-title',@$this->action); ?>
          <?php @$this->repeatData($this->content['welcome-deposit-offers-title']); ?>
     </h2>
     <ul data-bind="foreach: CashierPRD.promos">
          <!-- 1st offer -->
          <li>
               <div class="claim-wrap">
                    <div class="claim-container"><span data-bind="text: Description"></span></div>
                    <a href="/cashier/" class="deposit-cta" data-gtag="Welcome Left Deposit">DEPOSIT TO CLAIM</a>
               </div>
               <div class="icon">
                    <span class="bingo">
                         <img src="/_images/welcome/deposit-offer-icon.png" alt="">
                    </span>
               </div>
          </li>
     </ul>
</section>
<a href="/cashier/" data-gtag="Welcome Footer Cashier Link" class="no-bonus-link" data-bind="visible: CashierPRD.visible() && CashierPRD.isNotVip()" style="display: none">You can also choose to play without taking a bonus. Find this option in the <span>cashier.</span></a>
