<!-- MIDDLE CONTENT --> 
<div class="middle-content">
  <!-- CONTENT -->
<div class="middle-content__box">
  <?php edit($this->controller,'archive'); ?>
  <div class="content-template">
    
    <!-- repeatable content -->
    <?php @$this->repeatData($this->content['archive']);?>
    <!-- /repeatable content -->
    <a href='/archive/promotions/'>Promotions Archive</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href='/archive/winners/'>Winners Archive</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <a href='/archive/blogs/'>Blog Archive</a>
  </div>
  <!-- /CONTENT BOX-->
</div>
</div>
<!-- END MIDDLE CONTENT -->