<div class="main-content banking-content">

  <!-- CONTENT WRAP -->
  <div class="section middle-content__box">

      <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h2>Banking</h2>
  </div>


    <div class="text-partial-content">

        <!-- BANKING LIST -->
        <?php edit($this->controller,'banking-list'); ?>

        <?php @$this->repeatData($this->content['banking-list']);?>

    </div>
  </div>
  <!-- / CONTENT WRAP -->

</div>
<!-- END MIDDLE CONTENT -->