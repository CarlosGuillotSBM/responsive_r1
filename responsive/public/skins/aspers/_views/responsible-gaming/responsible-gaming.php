<!-- MIDDLE CONTENT -->
<div class="middle-content responsible--gaming">
  <!-- MIDDLE CONTENT -->
  <div class="middle-content__box">
    <section class="section">
      <h1 class="section__title responsible--gaming_title"><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
      
      <!-- CONTENT -->
      <?php edit($this->controller,'responsible-gaming'); ?>
      <div class="content-template">
        <!-- repeatable content -->
        <?php @$this->repeatData($this->content['responsible-gaming']);?>
        <!-- reality check dropdown -->
        <?php include '_global-library/partials/reality-check/reality-check-settings.php'; ?>
      </div>
      <!-- /CONTENT-->

    </section>
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->