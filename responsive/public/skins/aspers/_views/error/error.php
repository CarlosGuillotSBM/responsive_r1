
<!-- MIDDLE CONTENT -->	
<div class="middle-content">

	<div class="middle-content__box">
		<!-- breadcrumbs -->

		<!-- END breadcrumbs -->
		<section class="section">
			<!-- CONTENT -->
			<div class="content-template">

				<div class="error-page">
					<div class="error-page__image">
						<img src="/_images/common/404-error-image.svg">
					</div>
					<div class="error-page__text">
						<h2 class="error-page__error">
						Whoops!
						</h2>
						<p class="error-page__msg">
							We’re sorry, we can’t find the page you’re looking for. What can you do?
						</p>
						<div class="error-page__suggestion clearfix">
							<p>Go to the <a data-hijack="true" href="/">homepage</a>
								or check out our latest <a data-hijack="true" href="/promotions/">promotions.</a>

							<div class="games-list__grid games-list__promoted-games">

								<?php //$this->repeatData($this->games,'_partials/games/games-list__slot.php'); ?>
							</div>

						</div>
						<!-- <a class="join-now-button" href="/register/" data-hijack="true">JOIN NOW</a> -->
					</div>

				</div>

			</div>
			<!-- /CONTENT-->
		</section>
	</div>
	<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->