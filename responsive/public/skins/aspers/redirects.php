<?php

/*******************************************************
	PAGE REDIRECTS
********************************************************/

$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';


// Hello landing page

if(preg_match("#^\/hello\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/hello/?aid=1753&cid=14796&tid=1". $queryString);
    exit;
}

// VIP welcome offer

if(preg_match("#^\/vipwelcomeoffer\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/vipwelcomeoffer/". $queryString);
    exit;
}

// DEV-11021

if(preg_match("#^\/christmas\/$#", $path) == 1) {
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/christmas/". $queryString);
    exit;
}

// DEV-10636
if(preg_match("#^\/loyalty\/vip-club\/$#", $path) == 1) {
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /error/". $queryString);
    exit;
}
if(preg_match("#^\/loyalty\/vip-club-points\/$#", $path) == 1) {
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /error/". $queryString);
    exit;
}

//DEV-11181
if(preg_match("#^\/members\/$#", $path) == 1) {
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/affiliates/members/". $queryString);
    exit;
}
if(preg_match("#^\/aspers10\/$#", $path) == 1) {
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /register/aspers10/". $queryString);
    exit;
}

//R10-102 
if(preg_match("#^\/online\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/online/". $queryString);
    exit;
}

//R10-178
if(preg_match("#^\/hello\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/hello/". $queryString);
    exit;
}

if(preg_match("#^\/invited\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/invited/". $queryString);
    exit;
}


//R10-204
if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/asperscasino/". $queryString);
    exit;
}

//R10-365
if(preg_match("#^\/gold\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/gold/". $queryString);
    exit;
}
//R10-1009
if(preg_match("#^\/excluded\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/excluded/". $queryString);
    exit;
}

//R10-1277
if(preg_match("#^\/promotions/wimbledon-prize-draw\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if(preg_match("#^\/promotions/wimbledon-winners\/$#", $path) == 1) {
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}