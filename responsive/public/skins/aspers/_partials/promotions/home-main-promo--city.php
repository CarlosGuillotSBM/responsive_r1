<article class="home-main-promo--city">
	
	<div class="home-main-promo__image">
		
		<div class="home-main-promo__content">
			<div class="home-main-promo__super">
				<h1 class="home-main-promo__super__text">
				<strong class="home-main-promo__super__text__bold">GET</strong>
				<strong class="home-main-promo__super__text__xl">£1000</strong>
				<br> + 100 FREE SPINS
				</h1>
				<a data-gtag="Join Now,Web Promo" href="/register/" class="home-main-promo__cta">
					JOIN NOW
				</a >
			</div>
			<div class="home-main-promo__read-more">
				<a href="/promotions/welcome-offer/">read more</a>
			</div>
		</div>
		<div class="home-main-promo__image__character">
			
		</div>
	</div>
	
	<div class="home-main-promo__link">
		<a  data-gtag="Join Now,Mobile Promo" href="/register/">
		</a>
	</div>
	
</article>