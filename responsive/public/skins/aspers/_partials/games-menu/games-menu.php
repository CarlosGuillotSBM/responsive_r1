<script type="application/javascript">
	skinPlugins.push({
		id: "magicalMenu",
		options: {
			global: true
		}
	});
</script>
<div class="games-menu">
	<div class="games-menu__container">
		<!-- games nav -->
		<ul class="games-menu__games-nav">

		<li id="magicalMenu" class="games-filter-menu"><a id="magicalMenuBtnLnk" class="ui-game-filter-btn"><span>SLOTS FILTER</span></a>
		<?php include "_partials/games-menu/mobile-magic-menu-popover.php"; ?>
		</li>

	<li class="games-menu__games-nav__search"><?php include '_global-library/partials/search/search-input.php'; ?></li>
</ul>
<!-- /games nav -->
</div>
</div>