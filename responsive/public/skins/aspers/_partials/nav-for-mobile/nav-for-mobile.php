<!-- Mobile Top Bar -->
<div class="mobilemenu tab-bar nav-for-mobile">
     <!-- COOKIE POLICY -->
     <?php include "_global-library/partials/modals/cookie-policy.php" ?>
     <div class="mobilemenu--logo">
          <h1 class="seo-logo">
               <a data-hijack="true" href="/" class="site-logo ui-go-home-logo menu-link">
                    <img src="/_images/common/header/aspers-online-casino-logo.svg" class="aspersheader__logo--img" />
               </a>
               <span style="display: none;">Aspers Casino Online The best online casino on mobile and desktop.</span>
          </h1>
     </div>
     <div class="mobilemenu--menu">
          <div class="mobilemenu--burger">
               <!-- Start of navigation toggle -->
               <div class="menu-toggle-wrap">
                    <div class="burger">
                         <div class="span"></div>
                    </div>
               </div>
          </div>

          <!-- START Logged out menu -->
          <div class="mobilemenu--inner" style="display: none" data-bind="visible: !validSession()">
               <a class="login-button--nav-for-mobile menu-link" href="/login/">Login</a>
               <div class="aspersbutton aspersbutton--mobilemenu">
                    <div class="inner">
                         <a class="menu-link aspersbutton__copy" href="/register/">Join Now</a>
                    </div>
               </div>
               <!-- Search Icon -->
               <div class="search--icon--loggedout ui-search-screen">
                    <a href="#" class="search--link">
                         <?php include "_partials/common/header-icons/search.php";?>
                    </a>
               </div>
          </div>
          <!-- END Logged out menu -->

          <!-- START Logged IN menu -->
          <div class="mobilemenu--inner--loggedin" style="display: none" data-bind="visible: validSession">
               <div class="mobilemenu--item mobilemenu__balance">
                    <a href="#" class="js-open-component" data-component="myBalance">
                         <span data-bind="text: balance"></span>
                         <?php include "_partials/common/header-icons/dropdown-arrow.php";?>
                    </a>
               </div>
               <div class="mobilemenu--item" >
                    <div class="mobilemenu__cashier">
                         <a data-hijack="true" href="/cashier/" class="mobilemenu__cashier--copy">Cashier</a>
                    </div>
               </div>
               <div class="mobilemenu--item mobilemenu__dailyplay">
                    <div class="dice-icon" data-bind="visible: displayThreeRadical()" style="display: none">
                    <a href="/game-frame/three-radical" data-hijack="false" target="threerad">
                         <svg>
                              <g id="sparkle1">
                                   <path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
                                   <path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
                                   <path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
                              </g>
                         </svg>
                         <img class="bounce-dice" src="/_images/common/icons/dice-aspers.svg" alt="Extras"/>
                    </a>
                    </div>
               </div>
               <div class="mobilemenu--item mobilemenu__myaccount">

                    <!-- <a href="#" data-reveal-id="myAccount" > -->
                    <a href="#" data-component="myAccount" class="js-open-component">
                         <?php include "_partials/common/header-icons/my-account.php";?>

                         <span data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>

                    </a>
               </div>
               <!-- Search Icon -->
               <div class="search--icon--loggedin ui-search-screen">
                    <a href="#" class="search--link">
                         <?php include "_partials/common/header-icons/search.php";?>
                    </a>
               </div>

          </div>
          <!-- END Logged IN menu -->
     </div>
</div>

<nav class="mobile-menu" id="js-mobile-menu">
     <div class="mobile-menu-lists">
          <div class="menu__top">
               <h3 class="menu__top--header">Menu</h3>
          </div>
          <h4 class="menu__top--home"><a data-hijack="true" href="/start/" style="display: none" data-bind="visible: validSession()">Start</a>
               <a href="/" data-bind="visible: !validSession()">Home</a>
          </h4>
          <!--  <ul data-bind="visible: !validSession()" class="menu__list aspers__chinese">
               <li class="menu__list--item"><img src="/_images/header/icon-flag-chinese.svg" /><a href="http://cn.aspers.com">简体中文</a></li>
          </ul> -->
          <h4 class="menu__header">Games</h4>
          <!-- menu lists -->
          <ul class="menu__list">
               <li class="menu__list--item"><a data-hijack="true" href="/games/">All Games</a></li>
               <li style="display:none" class="menu__list--item" data-bind="visible: Favourites.favourites().length > 0 && validSession()"><a data-hijack="true" href="/my-favourites/">My Favs</a></li>

               <li class="menu__list--item"><a data-hijack="true" href="/slots/">Slots</a></li>
               <li class="mobile-menu-item has-submenu">
                    <span id="ui-slots-menu-link" class="menu-link js-toggle-filters">
                         Filter by type

                         <svg class="chevron-icon" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
                              <path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#ffffff">
                              </path>
                         </svg>
                    </span>

                    <div class="mobile-menu-item__submenu">
                         <span class="nub"></span>
                         <!-- include game categories-->
                         <div class="mobile-menu-item__submenu__content">
                              <!-- Slots Categories Hidden Slide Down Panel -->
                              <div class="games-filter-menu__popover-wrap">
                                   <div class="games-filter-menu__popover game-filter-menu" align="center">
                                        <span class="nub"></span>
                                        <!-- include game categories-->
                                        <?php include "_global-library/partials/game-filter-menu/game-filter-categories.php" ?>
                                   </div>
                              </div>
                         </div>
                    </div>
               </li>
               <li class="menu__list--item"><a data-hijack="true" href="/live-casino/">Live Casino</a></li>
               <li class="menu__list--item"><a data-hijack="true" href="/table-card/">Table &amp; Card</a></li>
               <li class="menu__list--item"><a data-hijack="true" href="/roulette/">Roulette</a></li>
               <li class="menu__list--item"><a data-hijack="true" href="/novomatic/">Novomatic</a></li>
               <!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
               <li class="menu__list--item u-hidden"><a data-hijack="true" href="/sports/">Sports</a></li>
               <!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
               <!--  <li class="menu__list--item"><a href="#">Scratch &amp; Arcade</a></li> -->
          </ul>
          <h4 class="menu__header">Rewards</h4>
          <ul class="menu__list">
               <li class="menu__list--item"><a data-hijack="true" href="/promotions/">Promotions</a></li>
               <li class="menu__list--item daily-play--item" data-bind="visible: displayThreeRadical()" href="/game-frame/three-radical" data-hijack="false" target="threerad">
               
               <a href="/game-frame/three-radical" data-hijack="false" target="threerad">
                    <img class="bouncing-dice" src="/_images/common/icons/dice-aspers.svg" alt="Extras"/>
                    Daily play
                     <svg class="dice-svg">
                         <g id="sparkle1">
                              <path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
                              <path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
                              <path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
                         </g>
                    </svg>
               </a>
               </li>
               <li class="menu__list--item"><a href="/loyalty/">Loyalty</a></li>
          </ul>

          <h4 class="menu__header" data-bind="visible: validSession()">Account</h4>
          <ul class="menu__list" data-bind="visible: validSession()">
               <li class="menu__list--item"><a data-hijack="true" href="/my-account/">My Account</a></li>
               <li class="menu__list--item"><a data-hijack="true" href="/cashier/" >Cashier</a></li>
          </ul>

          <h4 class="menu__header">About Us</h4>
          <ul class="menu__list">
               <li class="menu__list--item"><a href="http://www.aspers.co.uk" target="_blank">Land Based Casinos</a></li>
               <!--             <li class="menu__list--item"><a href="#">Community</a></li> -->
               <li class="menu__list--item"><a data-hijack="true" href="/support/">Support</a></li>
               <li class="menu__list--item"><a href="/faq/" data-hijack="true">FAQ</a></li>
               <li class="menu__list--item"><a class="footer__nav--items" href="/banking/" data-hijack="true">Banking</a></li>
               <li class="menu__list--item"><a href="/terms-and-conditions/" data-hijack="true">Terms &amp; Conditions</a></li>
               <li class="menu__list--item"><a href="/about-us/" data-hijack="true">About</a></li>
               <li class="menu__list--item"><a href="/privacy-policy/" data-hijack="true">Privacy Policy</a></li>
               <li class="menu__list--item" data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none">
                    <a data-hijack="true" href="/cashier/?settings=1" title="Player Settings">Player Settings</a>
               </li>
               <li class="menu__list--item"><a href="/responsible-gaming/" data-hijack="true">Responsible Gaming</a></li>
               <li class="menu__list--item"><a href="/complaints-and-disputes/" data-hijack="true">Complaints and Disputes</a></li>
                <li class="menu__list--item"><a class="footer__nav--items" href="/archive/" title="Archive" data-hijack="false">Archive</a></li>
          </ul>
          <ul class="menu__list" style="display: none" data-bind="visible: validSession()">
               <h4 class="menu__header"></h4>
               <li class="menu__list--item"><a data-hijack="true" href="" data-bind="click: logout">Logout</a></li>
          </ul>
     </div>
     <!-- / menu lists -->
</nav>