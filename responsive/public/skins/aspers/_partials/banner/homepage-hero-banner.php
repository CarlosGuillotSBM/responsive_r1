<?php
     $content = array(
          'line-1' =>  $row['Text1'],
          'line-2' =>  $row['Text2'],
          'line-3' =>  $row['Text3'],
          'cta-text' =>  $row['Text4'],
          'cta-url' =>  $row['Text5'],
          't&c-text' =>  $row['Text6'],
          't&c-url' =>  $row['Text7'],
          'extra-copy' =>  $row['Text8'],
          'image-desktop' =>  $row['Text9'],
          'image-desktop-retina' =>  $row['Text10'],
          'image-tablet' =>  $row['Text11'],
          'image-tablet-retina' =>  $row['Text12'],
          'image-mobile' =>  $row['Text13'],
          'image-mobile-retina' =>  $row['Text14'],
     );
?>

<style media="screen">
     .hero {
          background: url('<?php echo $content['image-desktop'] ?>');
          background-image: -webkit-image-set(url('<?php echo $content['image-desktop'] ?>') 1x,
                                              url('<?php echo $content['image-desktop-retina'] ?>') 2x);
          background-size: cover;
          background-repeat: no-repeat;
          background-position: center center;
     }

     @media only screen and (max-width: 64em) and (min-width: 40.0625em) {
          .hero {
               background: url('<?php echo $content['image-desktop'] ?>');
               background-image: -webkit-image-set(url('<?php echo $content['image-tablet'] ?>') 1x,
                                                   url('<?php echo $content['image-tablet-retina'] ?>') 2x);
               background-size: cover;
               background-repeat: no-repeat;
               background-position: center center;
          }
     }
     @media only screen and (max-width: 40em) {
          .hero {
               background: url('<?php echo $content['image-desktop'] ?>');
               background-image: -webkit-image-set(url('<?php echo $content['image-mobile'] ?>') 1x,
                                                   url('<?php echo $content['image-mobile-retina'] ?>') 2x);
               background-size: cover;
               background-repeat: no-repeat;
               background-position: center center;
          }
     }
</style>

<div class="small-12 columns hero">
     <span class="hero--icon-18plus"><img src="/_images/home/icons/18plus.svg" alt="18+"></span>
     <div class="hero--html">
          <h2 class="hero--firstline">
               <?php echo $content['line-1'] ?>
          </h2>
          <h2 class="hero--secondline animated delay-06-hero-content fadeIn">
               <?php echo $content['line-2'] ?>
          </h2>
          <h3 class="hero--fourthline animated delay-08-hero-content fadeIn">
               <?php echo $content['line-3'] ?>
          </h3>
          <div class="aspersbutton--large animated delay-09-hero-content fadeIn">
               <a href="<?php echo $content['cta-url']?>" class="aspersbutton__copy">
                    <?php echo $content['cta-text'] ?>
               </a>
          </div>
          <p class="hero--terms animated delay-10-hero-content fadeIn">
               <a href="<?php echo $content['t&c-url'] ?>">
                    <?php echo $content['t&c-text'] ?>
               </a>
               <br> <?php echo $content['extra-copy'] ?>
          </p>
     </div>
</div>
