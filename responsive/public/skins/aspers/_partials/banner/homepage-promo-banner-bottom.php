<?php
     $content = array(
          'line-1' =>  $row['Text1'],
          'line-2' =>  $row['Text2'],
          'line-3' =>  $row['Text3'],
          'cta-text' =>  $row['Text4'],
          'cta-url' =>  $row['Text5'],
          't&c-text' =>  $row['Text6'],
          't&c-url' =>  $row['Text7'],
          'extra-copy' =>  $row['Text8'],
          'image-desktop' =>  $row['Text9'],
          'image-desktop-retina' =>  $row['Text10'],
          'image-tablet' =>  $row['Text11'],
          'image-tablet-retina' =>  $row['Text12'],
          'image-mobile' =>  $row['Text13'],
          'image-mobile-retina' =>  $row['Text14'],
     );
?>

<style media="screen">
     .promo__bottom {
          background: url(<?php echo $content['image-desktop']; ?>);
          background-image: -webkit-image-set(url(<?php echo $content['image-desktop']; ?>) 1x,
                                             url(<?php echo $content['image-desktop-retina']; ?>) 2x);
          background-size: cover;
          background-repeat: no-repeat;
          background-position: center center;
     }

     @media only screen and (max-width: 64em) and (min-width: 40.0625em) {
          .promo__bottom {
               background: url(<?php echo $content['image-tablet']; ?>);
               background-image: -webkit-image-set(url(<?php echo $content['image-tablet']; ?>) 1x,
                                                  url(<?php echo $content['image-tablet-retina']; ?>) 2x);
               background-size: cover;
               background-repeat: no-repeat;
               background-position: center center;
          }
     }

     @media only screen and (max-width: 40em) {
          .promo__bottom {
               background: url(<?php echo $content['image-mobile']; ?>);
               background-image: -webkit-image-set(url(<?php echo $content['image-mobile']; ?>) 1x,
                                                  url(<?php echo $content['image-mobile-retina']; ?>) 2x);
               background-size: cover;
               background-repeat: no-repeat;
               background-position: center center;
          }
     }

</style>


<div class="small-12 large-12 promo__bottom">
     <div class="promo__bottom--wrapper">
           <h3 class="promo__bottom--firstline">
                <?php echo $content['line-1']; ?>
           </h3>

            <h4 class="promo__bottom--secondline">
                <?php echo $content['line-2']; ?>
           </h4>

           <div class="aspersbutton--promobottom">
                <a href="<?php echo $content['cta-url']; ?>" class="aspersbutton__copy">
                     <?php echo $content['cta-text']; ?>
                </a>
           </div>
           <p class="promo__bottom--terms">
                <a href="<?php echo $content['t&c-url']; ?>">
                     <?php echo $content['t&c-text']; ?>
                </a>
           </p>
       </div>
</div>
