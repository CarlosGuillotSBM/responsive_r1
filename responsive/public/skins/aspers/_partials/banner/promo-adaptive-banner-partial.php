<?php
     $cols = 12 / $counter;
     $int_counter = 1;
     foreach($playerType as $filtered_content){ ?>
          <style media="screen">
               <?= ".promo_bg".$int_counter ?> {
                    background: url(<?=$filtered_content['image-desktop']; ?>);
                    background-image: -webkit-image-set(url(<?=$filtered_content['image-desktop']; ?>) 1x,
                                                       url(<?=$filtered_content['image-desktop-retina']; ?>) 2x);
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: center center;
               }
               @media only screen and (max-width: 64em) and (min-width: 40.0625em) {
                    <?= ".promo_bg".$int_counter ?> {
                         background: url(<?=$filtered_content['image-tablet']; ?>);
                         background-image: -webkit-image-set(url(<?=$filtered_content['image-tablet']; ?>) 1x,
                                                            url(<?=$filtered_content['image-tablet-retina']; ?>) 2x);
                    }
               }
               @media only screen and (max-width: 40em) {
                    <?= ".promo_bg".$int_counter ?> {
                         background: url(<?=$filtered_content['image-mobile']; ?>);
                         background-image: -webkit-image-set(url(<?=$filtered_content['image-mobile']; ?>) 1x,
                                                            url(<?=$filtered_content['image-mobile-retina']; ?>) 2x);
                    }
               }
          </style>
          <div class="small-12 large-<?=$cols?> columns promo--wrapper">
               <div class="row">
                    <div class="small-12 large-12 promo__top promo_bg<?= $int_counter ?>">
                         <div class="promo__top--wrapper">
                              <h3 class="promo__top--firstline"><?=$filtered_content['line-1']; ?></h3>
                              <h4 class="promo__top--secondline"><?=$filtered_content['line-2']; ?></h4>
                              <div class="aspersbutton--promotop">
                                   <a href="<?=$filtered_content['cta-url']; ?>" class="aspersbutton__copy"><?=$filtered_content['cta-text']; ?></a>
                              </div>
                              <p class="promo__top--terms">
                                   <a href="<?=$filtered_content['t&c-url']; ?>"><?=$filtered_content['t&c-text']; ?></a>
                              </p>
                         </div>
                    </div>
               </div>         
          </div>
     <?php $int_counter++; }
?>

