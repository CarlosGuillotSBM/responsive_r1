<!--
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$$ SEO MICRO DATA FOR THIS PARTIAL
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
Name: itemprop="name"
Image: itemprop="image"
URL: itemprop="url"
Screenshot: itemprop="screenshot"
-->
<script type="application/javascript">
skinPlugins.push({ id: "Slick", options: { auto: true, pause: 5000,element: 'bxslider-games' } });
</script>
<ul class="bxslider-games"> 

    <!-- GAME IMAGE -->
    <li style="z-index: 0">
        <!--img class="free-spins" src="/_images/common/game-free-spins.svg"-->
        <span <?php if(@$this->row["Status"]==1) echo 'data-bind="click: GameLauncher.openGame"';?> data-infopage="true" data-gameid="<?php echo @$this->row["DetailsURL"];?>" data-gametitle="<?php echo @$this->row["Title"] ?>" data-icon="<?php echo @$this->row["Image"];?>" data-nohijack="true" class="games-info__try">
            <div style="display: none; cursor: pointer" data-bind="visible: validSession() && <?php echo @$this->row["Status"];?>" class="overlay">
                <p alt="Play Now" class="play-now-button">Play Now</p>
            </div>
            <img alt="<?php echo @$this->row["Title"] . " " . $this->controller;?>" itemprop="screenshot" src="<?php echo @$this->row["GameScreen"];?>" />
        </span>
    </li>

    <!-- GAME ICON -->
    <li style="z-index: 0">
        <!--img class="free-spins" src="/_images/common/game-free-spins.svg"-->
        <span <?php if(@$this->row["Status"]==1) echo 'data-bind="click: GameLauncher.openGame"';?> data-infopage="true" data-gameid="<?php echo @$this->row["DetailsURL"];?>" data-gametitle="<?php echo @$this->row["Title"] ?>" data-icon="<?php echo @$this->row["Image"];?>" data-nohijack="true" class="games-info__try">
            <div style="display: none; cursor: pointer" data-bind="visible: validSession() && <?php echo @$this->row["Status"];?>" class="overlay">
                <p alt="Play Now" class="play-now-button">Play Now</p>
            </div>
            <img alt="<?php echo @$this->row["Title"];?>" itemprop="image" src="<?php echo @$this->row["Image"];?>" />
        </span>
    </li>
    
</ul>