<?php include '_global-library/partials/reality-check/games-reality-check.php'; ?>


<!--Contains the game baground full size image -->
<div id="ui-game-background" class="game-ifr-container" style="display: none">

</div>

<!-- /game iframe outer holder outer -->
    <?php include '_global-library/partials/cashier/cashier__quick-deposit.php'; ?>

    <!-- Game iframe -->
    <div id="ui-game-launch-holder" class="games-iframe-content" style="display:none;">
        <div class="games-iframe-content__iframe flex-container flex-container-style"
            >
            <div class="games-iframe-content__top-bar flex-item">
                <ul class="games-iframe-content__btn-container">
                    <!-- message inbox -->
                    <?php if(config("game-iframe-inbox-msg-on")): ?>
                        <li>
                            <a data-gtag="Message Inbox, Game Slots Frame" class="game-message-inbox ui-close-iframe" style="display: block;" data-bind="visible: validSession" href="<?php echo getHomeURL() . (config("RealDevice") == 1)? '/start/' : '/my-messages/'; ?>" data-hijack="true">
                                <img src="/_global-library/images/game-launch/message-inbox.svg" style="display: none;" data-bind="visible: validSession" data-gtag="Toggle Favourite On Game Client">
                                <span class="game-message-inbox__value" data-bind="text: MailInbox.unreadMessages(), visible: MailInbox.unreadMessages() > 0"></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <!-- Responsible gaming link -->
                    <li class="gfr-responsible-gaming">
                        <a href="/responsible-gaming/" alt="Responsible Gaming" target="_blank"><img src="/_global-library/images/game-launch/responsible-gaming.svg">responsible gaming</a>
                    </li>
                    <!-- quick deposit button-->
                    <li><a class="game-quick-deposit-btn" style="display: none;" id="ui-quick-deposit"
                        data-bind="click: QuickDeposit.start" data-gtag="Opened Quick Deposit"><img
                    src="/_global-library/images/game-launch/quick-deposit-aspers.svg">

                    </a></li>
                         <!-- fav icon -->
                    <li title="Add or remove game from favourites">
                        <a class="game-lauch-fav" style="display: none;" id="ui-set-favourite-client"
                            data-bind="click: Favourites.toggleFavouriteFromGameLauncher">

                            <img 

                            <?php if (in_array(config("SkinID"), array(Skin::Aspers))) : ?>
                                data-bind="attr: { src: '/_global-library/images/game-launch/aspers/' + Favourites.gameLauncherIcon() }"
                            <?php else : ?>
                                data-bind="attr: { src: '/_global-library/images/game-launch/' + Favourites.gameLauncherIcon() }"
                            <?php endif ?>

                            data-gtag="Toggle Favourite On Game Client">

                        </a>
                    </li>
                    <!-- maximize game -->
                    <li title="Expand game screen"><a class="game-fullscreen" style="display: none;" id="ui-maximize-client"><img
                                src="/_global-library/images/game-launch/fullscreen.svg" data-gtag="Maximized Game Window"></a>
                    </li>
                    <!-- minimize game -->
                    <li title="Shrink game screen"><a class="game-fullscreen-exit" style="display: none;" id="ui-minimize-client"><img
                                src="/_global-library/images/game-launch/fullscreen-exit.svg"
                                data-gtag="Minimize Game Window"></a>
                    </li>
                    <!-- close game -->
                    <li title="Exit game"><a style="display: none;" class="game-close-iframe ui-close-iframe" id="ui-close-iframe"><img
                    src="/_global-library/images/game-launch/close.svg"></a></li>
                </ul>
            </div>
        <div class="ui-game-ifr-iframe-wrapper flex-item">
            <iframe style="display: none" id="ui-game-ifr" frameborder="0px" scrolling="no">
            </iframe>
        </div>
        <div class="games-iframe-content__right-panel flex-item" >
            <div class="row recommended-games--side">
                <div class="large-12 columns">
                <h1>Recommended</h1>
                    <!-- category selector -->
                    <div class="selector-container">
                    </div>
                    <!-- / category selector -->
<!--                    <span>Recommended For You</span>-->
                    <!--
                    <a href="#"  class="featured-game__title" data-dropdown="hover1" data-options="is_hover:true; hover_timeout:200">GAMES FOR YOU<span></span></a>
                    <ul id="hover1" class="f-dropdown" data-dropdown-content>
                        <li><a href="#">This is a link</a></li>
                        <li><a href="#">This is another</a></li>
                        <li><a href="#">Yet another</a></li>
                    </ul>
                    -->
                    <!-- Your Games Slider -->
                    <script id="ui-recommended-games-tmpl" type="text/tmpl">
                        <img data-bind="click: $root.GameWindow.launchRecommended, attr: { src: Image }" style="width: 100% !important">
                    </script>

                    <div class="game-launch-side-slider ui-recommend-games-container"
                        data-bind="foreach: GameWindow.recommendedGames" style="visibility: hidden">
                    </div>
                    <!-- / game-launch-side-slider -->

                </div>
                <!-- / large 12 col row -->

            </div>
            <!-- /recommended-games--side -->
        </div>
        <!-- /games-iframe-content__right-panel -->

    </div>
    <!-- /Game iframe -->

</div>
<!-- /ui-game-ifr-container -->
