<script>
// make this script work by adding into correct js file
//jQuery('.recent-messages-expand').on('click',function(){
//$(this).toggleClass('collapsed expanded');
//});
</script>
<!-- My Messages Box -->
<div class="lobby-wrap__content-box" id="messages">
     <!-- header bar -->
     <div class="lobby-wrap__content-box__header">
          <div class="title hasicon" >
               <div class="title__icon">
                    <!-- include svg icon partial -->
                    <?php include 'icon-messages.php'; ?>
                    <!-- end include svg icon partial -->
               </div >
               <div class="title__text messages-section">MY MESSAGES</div>
               <div class="title__option-links">
                    <span class="select-all-messages">
                         <input type="checkbox" data-bind="click: MailInbox.selectAllToggle">
                    </span>
               </div>

               <button href="#" data-dropdown="msg-actions" aria-controls="msg-actions" aria-expanded="false" class="tiny button dropdown">Options</button><br>
               <ul id="msg-actions" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                    <li><a data-bind="click: MailInbox.markAsRead">Mark as Read</a></li>
                    <li><a data-bind="click: MailInbox.markAsUnread">Mark as Unread</a></li>
                    <li><a data-bind="click: MailInbox.delete">Delete</a></li>
               </ul>
          </div>
     </div>
     <!-- end header bar -->

     <div class="lobby-wrap__content-box__content">
          <!-- make this content a partial -->
          <!-- recent messagages -->
          <div class=" lobby-wrap__content-box__content__innerwrap--border" style="display:none" data-bind="visible: MailInbox.messages().length > 0" >
               <!-- Accordion Message Foundation -->
               <ul class="recent-messages expanded" data-bind="foreach: MailInbox.messages">
                    <!-- notice the uread class in this message JS needs to append unread class to show its unread also the svg partials needs changing to have read or  -->
                    <!-- single message preview -->
                    <li class="recent-messages__message" data-bind="visible: $index() < 3 || $parent.MailInbox.expanded(), css: { unread: MailStatus() === 1 }, click: $parent.MailInbox.revealSingleMessage.bind($data, Id)" data-reveal-id="single-message-modal">
                         <!-- <li class="recent-messages__message" data-bind="visible: $index() < 3 || $parent.MailInbox.expanded(), css: { unread: MailStatus() === 1, active: $parent.MailInbox.selectedMessage() && $parent.MailInbox.selectedMessage().Id === Id }, click: $parent.MailInbox.revealSingleMessage.bind($data, Id)" data-reveal-id="single-message-modal"> -->
                         <span class="recent-messages__message__icon">
                              <!-- include svg icon partial -->
                              <span data-bind="css: { hide: MailStatus() === 2 }" ><?php include '_partials/lobby/icon-message-unread.php'; ?></span>
                              <span data-bind="css: { hide: MailStatus() === 1 }" ><?php include '_partials/lobby/icon-message-read.php'; ?></span>
                              <!-- end include svg icon partial -->
                         </span>
                         <span class="recent-messages__message__preview" data-bind="text: Subject"></span>
                         <span class="recent-messages__message__date" data-bind="text: Date"></span>
                         <span class="recent-messages__message__time" data-bind="text: Time"></span>
                         <span class="recent-messages__message__select"><input type="checkbox" data-bind="checked: Clicked, click: $parent.MailInbox.check, clickBubble: false"></span>
                    </li>
                    <!--li>
                    <?php //include'_global-library/partials/start/start-my-messages-single-modal.php'; ?>
               </li-->
               <!-- End single message preview -->
               </ul>
          <!-- End Accordion Message Foundation -->
          <!-- lobby my messages single modal -->
          <!-- SINGLE MESSAGE MODAL -->
               <div id="single-message-modal" class="single-message-modal reveal-modal" data-reveal>
                    <?php include "_global-library/partials/modals/single-message.php"; ?>
               </div>
               <!-- End single message preview -->

               <!-- shown by default -->
               <div data-bind="click: MailInbox.expandToggle, visible: MailInbox.messages().length > 3" class="recent-messages__switch">
                    <span data-bind="visible: !MailInbox.expanded()">&#9660; Expand all messages &#9660;</span>
                    <span data-bind="visible: MailInbox.expanded">&#9650; Collapse messages &#9650;</span>
               </div>
          </div>

          <div data-bind="visible: MailInbox.messages().length == 0" style="display:none" class="recent-messages__switch">
               <span data-bind="visible: MailInbox.messages().length == 0">You currently have no messages</span>
          </div>
     <!-- end recent messagages -->
     </div>
</div>
<!-- End My Messages Box -->
