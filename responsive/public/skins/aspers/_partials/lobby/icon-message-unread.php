<svg viewBox="0 0 35 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display:none;" id="asper-mail-icon">
    <!-- Generator: Sketch 44.1 (41455) - http://www.bohemiancoding.com/sketch -->
    <title>mail-icon</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Tablet" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="mailicon" transform="translate(-35.000000, -610.000000)">
            <g id="my-messages" transform="translate(20.000000, 560.000000)">
                <g id="Group" transform="translate(1.112214, 39.000000)">
                    <g id="message-tab" transform="translate(0.000000, 7.000000)">
                        <g id="mail-icon" transform="translate(12.659745, 4.000000)">
                            <g>
                                <rect id="Rectangle" stroke="#000000" stroke-width="1.8" x="2.16597454" y="0.9" width="32.3813126" height="15.2"></rect>
                                <path d="M1.85533312,2.78541882 L17.6985326,10.6001557 C18.3123965,10.9029471 19.1474915,10.7864392 19.5637694,10.3399281 C19.9800473,9.89341695 19.8198722,9.2859872 19.2060083,8.98319577 L3.36280882,1.16845893 C2.7489449,0.865667499 1.91384994,0.982175351 1.49757202,1.4286865 C1.0812941,1.87519764 1.2414692,2.4826274 1.85533312,2.78541882 Z" id="Line" fill="#000000" fill-rule="nonzero"></path>
                                <path d="M33.4528824,1.16845893 L17.6096829,8.98319577 C16.995819,9.2859872 16.8356439,9.89341695 17.2519218,10.3399281 C17.6681998,10.7864392 18.5032947,10.9029471 19.1171586,10.6001557 L34.9603581,2.78541882 C35.574222,2.4826274 35.7343971,1.87519764 35.3181192,1.4286865 C34.9018413,0.982175351 34.0667463,0.865667499 33.4528824,1.16845893 Z" id="Line" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
