<script type="application/javascript">
skinPlugins.push({
id: "WinnersSlider",
options: {
slidesToShow: <?php echo $slidesToShow ?>,
mode: "vertical"
}
});
</script>
<!-- latest winners -->
<div class="latest-winners__wrapper slider">
    <img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
    <article class="latest-winners" style="height: <?php echo ($slidesToShow * $slideHeight + $titleHeaderHeight). 'px' ?> ; overflow: hidden">
        <div class="latest-winners__title">
            <h1>Recent Winners</h1>
        </div>
        <?php edit($this->controller,'latest-winners'); ?>
        <?php @$this->repeatData($this->content['latest-winners'], 0, "_partials/start/widget-winners.php");?>
    </article>
</div>
<!-- /latest winners -->