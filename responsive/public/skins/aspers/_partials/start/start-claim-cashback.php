<!-- <div> -->
<!-- <div  class="lobby-claim-cashback" >
        <p>You have <span>£</span><span>3.00</span> cashback waiting.</p>
        <p >Expires on <span >12 August 2017</span>.</p>
        <a><div class="lobby-claim-cashback-button">CLAIM NOW</div></a>
    </div>
<div class="lobby-claim-cashback alert-box secondary" >
        <h3>Cashback Redeemed. Good Luck!</h3>
     
        <a href="#" class="close">&times;</a>
    </div>
</div>
 -->

<div  style="display:none" data-bind="visible: ClaimCashback.cbID() != 0" >
<div  class="lobby-claim-cashback" data-bind="visible: ClaimCashback.cbAmount() > 0">
        <p>You have <span data-bind="text: ClaimCashback.currency"></span><span data-bind="text: ClaimCashback.cbAmount"></span> cashback waiting.</p>
        <p data-bind="visible: ClaimCashback.lastDays">Expires on <span data-bind="text: ClaimCashback.cbDate"></span>.</p>
        <a><div class="lobby-claim-cashback-button" data-bind="click: ClaimCashback.claimPlayerCashback"> CLAIM NOW</div> </a>
    </div>
<div class="lobby-claim-cashback alert-box secondary" data-bind="visible: ClaimCashback.cbClaimAmount() > 0">
        <h1>Cashback Redeemed</h1>
        <p>Good Luck!</p>
        <a href="#" class="close" data-bind="click: ClaimCashback.hidePanel">&times;</a>
    </div>
</div>