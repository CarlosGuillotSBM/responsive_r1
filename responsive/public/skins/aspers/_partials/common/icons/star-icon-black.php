<?xml version="1.0" encoding="UTF-8"?>
<svg viewBox="0 0 25 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="hub-star">
    <title>star-icon-black</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="hub-star" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="hub-star" transform="translate(-968.000000, -610.000000)" fill="#000000">
            <g id="most-popular-widget" transform="translate(952.000000, 598.000000)">
                <g id="most-popular-games" transform="translate(16.000000, 11.000000)">
                    <path d="M24.4403582,9.39955665 L15.5258577,9.39955665 L12.2201808,1 L8.91449699,9.39955665 L0,9.39955665 L7.26165854,14.3318513 L4.66046742,23 L12.2201808,17.7991133 L19.7527933,23 L17.1786997,14.3318513 L24.4403582,9.39955665 Z M12.2201808,15.3573817 L8.04743329,18.4827981 L9.75446669,14.0144278 L5.79848853,11.1576034 L10.7028194,11.2796884 L12.2201808,6.22530246 L13.7375423,11.2796884 L18.6418697,11.1576034 L14.6858915,14.0144278 L16.3929215,18.4827981 L12.2201808,15.3573817 Z" id="star-icon-black"></path>
                </g>
            </g>
        </g>
    </g>
</svg>