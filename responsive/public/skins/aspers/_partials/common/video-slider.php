<section class="offer-container">
        <div class="offer section twelve">
          <div class="text-center">

            <!-- OFFER TEXT -->
            <div class="bonus twelve mobile-eleven center mobile-center-text">
              <p class="no-deposit wow fadeInDown animated" data-wow-delay=".60s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInDown;">No Deposit Required</p>
              <h1 class="wow fadeInUp animated" data-wow-delay=".15s" style="visibility: visible; animation-delay: 0.15s; animation-name: fadeInUp;">20 FREE SPINS ON</h1>
              <p class="wow fadeInDown animated" data-wow-delay=".20s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInDown;">STARBURST <span>or</span> PYRAMID <span>or</span> TWIN SPIN</p>
            </div>
             
             <!-- CTA -->
             <a class="join-now three mobile-ten tablet-four center" href="https://www.magicalvegas.com/register/">JOIN NOW</a>
        <!-- <h4 class="col twelve mobile-center-text show-mobile no-deposit wow shake" wow-data-offset="200" data-wow-delay="1.2s">FREE! NO DEPOSIT REQUIRED.</h4> -->
            </div>
          </div>

        <!-- TCS -->
        <a class="tcs twelve mobile-ten center center-text" href="https://www.magicalvegas.com/promotions/20-free-spins/?showtcs=1">New players only. 25x wagering and T&amp;Cs apply.</a>

        <!-- overlay -->
        <span class="overlay-pixel"></span>
        <span class="overlay"></span>

        <!-- VIDEO -->
        <div class="video-container center-text">

          <!-- <div class='embed-container'> -->
            <!-- https://developers.google.com/youtube/player_parameters?csw=1 -->
            <!-- <iframe width="100%" height="480"  src='https://www.youtube.com/embed/KNFss9XWJb4?version=3&autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&modestbranding=1&controls=0&playsinline=0&rel=0&showinfo=0&loop=1&autohide=1' frameborder='0' allowfullscreen></iframe> -->
          <!-- </div> -->

          <video autoplay="" loop="" muted="" playsinline="" poster="../_images/slider/_video-slider/bg-offer.jpg" class="fullscreen-bg__video">
            <source src="../_images/slider/_video-slider/video.mp4" webkit-playsinline="" autoplay="true" loop="true" type="video/mp4">
            <source src="../_images/slider/_video-slider/video.webm" webkit-playsinline="" autoplay="true" loop="true" type="video/webm">
            <source src="../_images/slider/_video-slider/video.ogv" webkit-playsinline="" autoplay="true" loop="true" type="video/ogv">
          </video>

     <!-- <div id="background-video" class="background-video" >
            <img src="images/placeholder.jpg" alt="" class="placeholder-image">
          </div> -->


        </div>
      </section>

