<?xml version="1.0" encoding="UTF-8"?>
<svg width="39px" height="35px" viewBox="0 0 39 35" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <title>your-favs-icon</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="favs" transform="translate(-718.000000, -34.000000)" fill="#FEFEFE">
            <g id="header" transform="translate(19.000000, 7.000000)">
                <g id="menu" transform="translate(103.000000, 19.000000)">
                    <g id="My-Favs" transform="translate(579.000000, 8.000000)">
                        <path d="M55.8823881,13.362931 L41.7002282,13.362931 L36.4411968,-7.10542736e-15 L31.1821543,13.362931 L17,13.362931 L28.5526386,21.2097635 L24.41438,35 L36.4411968,26.7258621 L48.4248985,35 L44.3297495,21.2097635 L55.8823881,13.362931 Z M36.4411968,22.8412891 L29.8027348,27.8135425 L32.5184697,20.7047715 L26.2248681,16.1598236 L34.0272126,16.3540497 L36.4411968,8.31298119 L38.855181,16.3540497 L46.6575199,16.1598236 L40.3639183,20.7047715 L43.0796478,27.8135425 L36.4411968,22.8412891 Z" id="your-favs-icon"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>