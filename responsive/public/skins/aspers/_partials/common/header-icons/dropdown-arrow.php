<?xml version="1.0" encoding="UTF-8"?>
<svg  viewBox="0 0 10 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <title>dropdown-arrow</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dropdown-arrow" transform="translate(-1188.000000, -40.000000)" stroke-width="2" stroke="#FFFFFF">
            <g id="header-logged-in">
                <g id="balances-menu">
                    <g transform="translate(1054.000000, 25.000000)">
                        <g id="my-account" transform="translate(51.000000, 7.000000)">
                            <g id="dropdown-arrow" transform="translate(83.000000, 8.000000)">
                                <polyline transform="translate(4.550452, 2.816026) rotate(90.000000) translate(-4.550452, -2.816026) " points="2.62080116 -1.04327529 6.48010267 2.92389588 2.62080116 6.67532773"></polyline>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>