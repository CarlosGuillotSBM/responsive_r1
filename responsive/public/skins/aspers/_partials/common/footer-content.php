<!-- FOOTER AREA -->
<!-- <div class="clearfix"></div> -->

<footer id="footer" class="footer">
<div class="footer--inner">
    <div class="footer__row">
      <div class="footer__payments--wrapper">
          <ul class="footer__payments">
              <li class="footer__payments--icons icons-paypal"><?php include "footer-icons/paypal.php";?></li>
              <li class="footer__payments--icons icons-maestro"><?php include "footer-icons/maestro.php";?></li>
              <li class="footer__payments--icons icons-mastercard"><?php include "footer-icons/mastercard.php";?></li>
              <li class="footer__payments--icons icons-visa"><?php include "footer-icons/visa.php";?></li>
              <li class="footer__payments--icons icons-paysafecard"><?php include "footer-icons/paysafecard.php";?></li>
              <li class="footer__payments--icons icons-neteller"><?php include "footer-icons/neteller.php";?></li>
          </ul>
        </div>
    </div>

    <div class="footer__row">
        <div class="footer__legal">
            <p>Aspers Casino Online is licensed and regulated to offer Gambling Services in Great Britain by the UK Gambling Commission, license Number 000-039022-R-319427-004. All the games offered on the website have been approved by the UK Gambling Commission. Details of its current licensed status as recorded on the Gambling Commission's website can be found <a href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022" target="_blank" data-hijack="false">here.</a></p>
            <p>Aspers Casino Online is also licensed and regulated by the Alderney Gambling Control Commission, License Number: 71 C1, to offer Gambling facilities in jurisdictions outside Great Britain. Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3 UL.</p>
        </div>
    </div>


    <div class="footer__row">
    <div class="footer__compliance--wrapper">
        <div class="footer__compliance">
          <li>
               <a target="_blank" data-hijack="false" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022"  rel="nofollow">
                    <?php include "footer-icons/gambling-commission.php";?></a>
          </li>
          <li>
               <a target="_blank" data-hijack="false" href="http://www.gamblingcontrol.org/" >
                    <?php include "footer-icons/gambling-control.php";?></a>
          </li>
          <li>
               <a href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&lng=EN" target="_blank">
                    <?php include "footer-icons/ecogra.php";?></a>
          </li>
          <li>
               <a href="http://www.gamcare.org.uk/" target="_blank">
                    <?php include "footer-icons/gamcare.php";?></a>
          </li>
          <li>
            <a class="gamstop" href="https://www.gamstop.co.uk" target="_blank"  rel="nofollow">
              <?php include "footer-icons/gamstop.php";?>
            </a>
          </li>
          <li>
            <a href="/terms-and-conditions/">
              <?php include "footer-icons/18plus.php";?>
            </a>
          </li>
        </div>
    </div>
    </div>
    <div class="footer__row">
        <div class="footer__social--wrapper">
          <ul class="footer__social">
            <li class="footer__social--icons"><a href="https://www.facebook.com/AspersCasino/" target="_blank"><?php include "footer-icons/facebook.php";?></a></li>
            <li class="footer__social--icons"><a href="https://twitter.com/CasinoAspers/" target="_blank"><?php include "footer-icons/twitter.php";?></a></li>
            <li class="footer__social--icons"><a href="https://www.instagram.com/asperscasino/" target="_blank"><?php include "footer-icons/instagram.php";?></a></li>
            <li class="footer__social--icons"><a href="https://www.youtube.com/channel/UCfhZc9AVJmxhDGlZVdLHVOg" target="_blank"><?php include "footer-icons/youtube.php";?></a></li>
          </ul>
        </div>
    </div>
    <div class="footer__row">
          <div class="footer__nav--wrapper">
            <ul class="footer__nav">
                <li><a class="footer__nav--items" href="/about-us/" >About</a></li>
                <li><a class="footer__nav--items" href="/support/" >Support</a></li>
                <li><a class="footer__nav--items" href="/faq/" >FAQ</a></li>
                <li><a class="footer__nav--items" href="/banking/" >Banking</a></li>
                <li><a class="footer__nav--items" href="/terms-and-conditions/" >Terms &amp; Conditions</a></li>
                <li><a class="footer__nav--items" href="/privacy-policy/" >Privacy Policy</a></li>
                <li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none"><a class="footer__nav--items" href="/cashier/?settings=1" >Player Settings</a></li>
                <li><a class="footer__nav--items" href="/responsible-gaming/" >Responsible Gaming</a></li>
                <li><a class="footer__nav--items" href="/complaints-and-disputes/" >Complaints and Disputes</a></li>
                <li class="affiliates-hide_mobile"><a class="footer__nav--items" href="https://www.luckyjar.com/uk/" target="_blank" data-hijack="false">Affiliates</a></li>
                <li><a class="footer__nav--items" href="/archive/" data-hijack="false">Archive</a></li>
            </ul>
          </div>
      </div>

    </div>
</div>

</footer>
<!-- END FOOTER AREA -->
