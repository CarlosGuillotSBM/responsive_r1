<script type="application/javascript">
    window.setTimeout(function () {
        var el = document.getElementById("ui-first-loader");
        if (el) el.parentNode.removeChild(el);
        el = document.getElementById("ui-loader-animation");
        if (el) el.style.display = "none";
    }, 1000);
</script>

<!-- header desktop -->
<div class="aspersheader__wrapper">
    <header id="ui-header-scroll" class="aspersheader row">
        <!-- data-magellan-expedition="fixed"  -->
        <!-- COOKIE POLICY -->
        <?php include "_global-library/partials/modals/cookie-policy.php" ?>
        <nav class="aspersheader__menu">
            <h1 class="small-1 columns aspersheader__logo">
                <a href="/" data-hijack="true">
                    <img src="/_images/common/header/aspers-online-casino-logo.svg" class="aspersheader__logo--img" />
                </a>
                <span style="display: none;">Aspers Casino The best online casino on mobile and desktop.</span>
            </h1>
            <!-- SVG images converted to partials so they are within the DOM --> 
            <!--
               START Logged Out
          -->
            <div class="small-6 columns aspersheader__menuleft" data-bind="visible: !validSession()">
                <ul class="aspersheader__menuleft__list">
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--allgames">
                        <a data-hijack="true" href="/games/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/all-games.php";?>
                            </span>
                            All Games
                        </a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--tableandcard">
                        <a data-hijack="true" href="/table-card/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/table-and-card.php";?>
                            </span>
                            Table &amp; Card</a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--livecasino">
                        <a data-hijack="true" href="/live-casino/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/live-casino.php";?>
                            </span>
                            Live Casino</a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--slots">
                        <a data-hijack="true" href="/slots/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/slots.php";?>
                            </span>
                            Slots
                        </a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--roulette">
                        <a data-hijack="true" href="/roulette/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/roulette.php";?>
                            </span>
                            Roulette
                        </a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--novomatic">
                        <a data-hijack="true" href="/novomatic/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/novomatic-logo.php";?>
                            </span>
                            Novomatic
                        </a>
                    </li>
                    <!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--sports u-hidden">
                        <a data-hijack="true" href="/sports/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/sports.php";?>
                            </span>
                            Sports
                        </a>
                    </li>
                    <!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
                </ul>
            </div>
            <div class="small-5 columns" data-bind="visible: !validSession()">
                <div class="row">
                    <ul class="small-12 columns valign-middle aspersheader__menuright aspersheader__menuright--top aspersheader__menuright--loggedin">
                        <li class="small-7 columns aspersheader__menuright--item">
                                <input id="search" type="text" placeholder="Search Games" class="aspersheader__menuright--inputsearch ui-search-input"
                                data-bind="value: GamesMenuSearch.searchTerm, valueUpdate:'input'" autocomplete="off">
                            <?php include('_global-library/partials/search/search-results-container.php'); ?>
                        </li>
                        <li class="small-2 columns aspersheader__menuright--item aspersheader__menuright--login" data-bind="visible: !validSession()">
                            <a href="/login/" class="right aspersheader__menuright--loginlink">Login</a>
                        </li>
                        <li class="small-3 columns aspersheader__menuright--item aspersheader__menuright--joinnow" data-bind="visible: !validSession()">
                            <div class="aspersbutton right">
                                <a href="/register/" class="aspersbutton__copy">Join Now</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="row right">
                    <ul class="small-12 columns aspersheader__menuright aspersheader__menuright--bottom">
                        <li class="aspersheader__menuright--item aspersheader__menuright--landbasedcasinos">
                            <a href="http://www.aspers.co.uk" target="_blank" class="right aspersheader__menuright--link">Land Based Casinos</a>
                        </li>
                        <li class="aspersheader__menuright--item aspersheader__menuright--promotions">
                            <a data-hijack="true" href="/promotions/" class="right aspersheader__menuright--link">Promotions</a>
                        </li>
                        <li class="aspersheader__menuright--item aspersheader__menuright--loyalty">
                            <a data-hijack="true" href="/loyalty/" class="right aspersheader__menuright--link">Loyalty</a>
                        </li>
                        <li class="aspersheader__menuright--item aspersheader__menuright--support">
                            <a data-hijack="true" href="/support/" class="right aspersheader__menuright--link">Support</a>
                        </li>
                        <!-- This might be re-used - NOT to delete  -->
                        <!-- <li class="aspersheader__menuright--item aspersheader__menuright--chinese">
                            <img src="/_images/header/icon-flag-chinese.svg" />
                            <a href="http://cn.aspers.com" class="right aspersheader__menuright--link">简体中文</a>
                        </li> -->
                        <!--                     <li class="aspersheader__menuright--item aspersheader__menuright--community"><a data-hijack="true" href="/community/" class="right aspersheader__menuright--link">Community</a></li>
                                   <li class="aspersheader__menuright--item aspersheader__menuright--vip"><a data-hijack="true" href="/vip/" class="right aspersheader__menuright--link">VIP</a></li> -->
                    </ul>
                </div>
            </div>
            <!--
                    END Logged OUT
               -->
            <!--
               START Logged In menu top
          -->
            <div class="small-6 columns aspersheader__menuleft--loggedin" data-bind="visible: validSession" style="display:none;">
                <ul class="aspersheader__menuleft__list">
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--start">
                        <a data-hijack="true" href="/start/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/start-icon.php";?>
                            </span>
                            Start
                        </a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--allgames">
                        <a data-hijack="true" href="/games/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/all-games.php";?>
                            </span>
                            All Games</a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--tableandcard">
                        <a data-hijack="true" href="/table-card/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/table-and-card.php";?>
                            </span>
                            Table &amp; Card</a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--livecasino">
                        <a data-hijack="true" href="/live-casino/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/live-casino.php";?>
                            </span>
                            Live Casino</a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--slots">
                        <a data-hijack="true" href="/slots/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/slots.php";?>
                            </span>
                            Slots
                        </a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--roulette">
                        <a data-hijack="true" href="/roulette/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/roulette.php";?>
                            </span>
                            Roulette
                        </a>
                    </li>
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--novomatic">
                        <a data-hijack="true" href="/novomatic/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/novomatic-logo.php";?>
                            </span>
                            Novomatic
                        </a>
                    </li>
                    <!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--sports u-hidden">
                        <a data-hijack="true" href="/sports/" class="aspersheader__menuleft--link">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/sports.php";?>
                            </span>
                            Sports
                        </a>
                    </li>
                    <!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
                    <li class="aspersheader__menuleft--item aspersheader__menuleft--favs">
                        <a data-hijack="true" href="/my-favourites/" class="aspersheader__menuleft--link" data-bind="visible: Favourites.favourites().length > 0">
                            <span class="aspersheader__menuleft__icon">
                                <?php include "header-icons/your-favs.php";?>
                            </span>
                            My Favs
                        </a>
                    </li>
                </ul>
            </div>
            <div class="small-5 columns" data-bind="visible: validSession" style="display:none;">
                <div class="row aspersheader__menuright--loggedin">
                    <ul class="small-12 columns valign-middle aspersheader__menuright aspersheader__menuright--top">
                        <li class="small-4 columns aspersheader__menuright--item">
                            <form autocomplete="off">
                                <input id="search" type="text" placeholder="Search Games" class="aspersheader__menuright--inputsearch autocomplete ui-search-input"
                                    data-bind="value: GamesMenuSearch.searchTerm, valueUpdate:'input'">
                            </form>
                            <?php include('_global-library/partials/search/search-results-container.php'); ?>
                        </li>
                        <li class="small-4 columns aspersheader__menuright--item aspersheader__menuright--item">
                            <a href="" data-component="myAccount" class="aspersheader__menuright--myaccount js-open-component">
                                <?php include "header-icons/my-account.php";?> My Account
                                <?php include "header-icons/dropdown-arrow.php";?>
                                <span data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
                            </a>
                        </li>
                        <li class="small-2 columns aspersheader__menuright--item aspersheader__menuright--balancewrapper">
                            <a href="#" data-component="myBalance" class="aspersheader__menuright--balance js-open-component">
                                <span data-bind="text: balance"></span>
                                <?php include "header-icons/dropdown-arrow.php";?>
                            </a>
                        </li>

                        <li class="small-3 columns aspersheader__menuright--item">
                            <div class="aspersbutton__cashier">
                                <a data-hijack="true" href="/cashier/" class="aspersbutton__cashier--copy">Cashier</a>
                            </div>
                        </li>


                    </ul>
                </div>
                <div class="row right aspersheader__menuright--loggedin">
                    <ul class="small-12 columns aspersheader__menuright aspersheader__menuright--bottom">
                        <li class="aspersheader__menuright--item aspersheader__menuright--landbasedcasinos">
                            <a href="http://www.aspers.co.uk" class="right aspersheader__menuright--link" target="_blank">Land Based Casinos</a>
                        </li>
                        <li class="aspersheader__menuright--item aspersheader__menuright--promotions">
                            <a data-hijack="true" href="/promotions/" class="right aspersheader__menuright--link">Promotions</a>
                        </li>
                        <li class="aspersheader__menuright--item aspersheader__menuright--dailyplay radical-dice" data-bind="visible: displayThreeRadical(), click: ThreeRadical.requestToken" style="display: none" class>
                            <svg>
                                <g id="sparkle1">
                                    <path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
                                    <path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
                                    <path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
                                </g>
                            </svg>
                            <a href="" data-hijack="true">Daily play<img class="bounce-dice" src="/_images/common/icons/dice-aspers.svg" alt="Extras"/></a>
                        </li>
                        <li class="aspersheader__menuright--item aspersheader__menuright--loyalty">
                            <a data-hijack="true" href="/loyalty/" class="right aspersheader__menuright--link">Loyalty</a>
                        </li>
                        <!--                     <li class="aspersheader__menuright--item aspersheader__menuright--community"><a data-hijack="true" href="/community/" class="right aspersheader__menuright--link">Community</a></li>
                                   <li class="aspersheader__menuright--item aspersheader__menuright--vip"><a data-hijack="true" href="/vip/" class="right aspersheader__menuright--link">VIP</a></li> -->
                        <li class="aspersheader__menuright--item aspersheader__menuright--support">
                            <a data-hijack="true" href="/support/" class="right aspersheader__menuright--link">Support</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--
                    END Logged In
               -->
        </nav>
    </header>
</div>

<div class="aspersheader__menubottom">
    <ul class="aspersheader__menubottom--wrapper">


        <li class="aspersheader__menubottom--item aspersheader__menubottom--start" data-bind="visible: validSession" style="display:none;">
            <a data-hijack="true" href="/start/" class="aspersheader__menubottom--link">
                <?php include "header-icons/start-icon.php";?>Start
            </a>
        </li>


        <li class="aspersheader__menubottom--item aspersheader__menubottom--allgames">
            <a data-hijack="true" href="/games/" class="aspersheader__menubottom--link">
                <?php include "header-icons/all-games.php";?>All Games</a>
        </li>
        <li class="aspersheader__menubottom--item aspersheader__menubottom--tableandcard">
            <a data-hijack="true" href="/table-card/" class="aspersheader__menubottom--link">
                <?php include "header-icons/table-and-card.php";?>Table &amp; Card</a>
        </li>
        <li class="aspersheader__menubottom--item aspersheader__menubottom--livecasino">
            <a data-hijack="true" href="/live-casino/" class="aspersheader__menubottom--link">
                <?php include "header-icons/live-casino.php";?>Live Casino</a>
        </li>
        <li class="aspersheader__menubottom--item aspersheader__menubottom--slots">
            <a data-hijack="true" href="/slots/" class="aspersheader__menubottom--link">
                <?php include "header-icons/slots.php";?>Slots
            </a>
        </li>
        <li class="aspersheader__menubottom--item aspersheader__menubottom--roulette">
            <a data-hijack="true" href="/roulette/" class="aspersheader__menubottom--link">
                <?php include "header-icons/roulette.php";?>Roulette
            </a>
        </li>
        <li class="aspersheader__menubottom--item aspersheader__menubottom--novomatic">
            <a data-hijack="true" href="/novomatic/" class="aspersheader__menubottom--link">
                <?php include "header-icons/novomatic-logo.php";?>Novomatic
            </a>
        </li>
        <!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
        <li class="aspersheader__menubottom--item aspersheader__menubottom--sports u-hidden">
            <a data-hijack="true" href="/sports/" class="aspersheader__menubottom--link">
                <?php include "header-icons/sports.php";?>Sports
            </a>
        </li>
        <!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
        <li class="aspersheader__menubottom--item aspersheader__menubottom--favs" data-bind="visible: Favourites.favourites().length > 0">
            <a data-hijack="true" href="/my-favourites/" class="aspersheader__menubottom--link">
                <?php include "header-icons/your-favs.php";?>My Favs
            </a>
        </li>
    </ul>
</div>

<!-- START My Account Modal -->
<div id="myAccount" class="overlay__myaccount js-modal aspers-modal-menu" aria-labelledby="modalTitle" aria-hidden="true"
    role="dialog">
    <div class="overlay__myaccount--arrow"></div>
    <div class="overlay__myaccount--header">
        <div class="overlay__myaccount--level" data-bind="css: vipLevelCssClass"></div>
        <h3 class="overlay__myaccount--welcome">Hello
            <span data-bind="text: username"></span>
        </h3>

    </div>
    <nav class="overlay__myaccount--menu">
        <ul>
            <li class="overlay__myaccount--menuitem">
                <a data-hijack="true" href="/cashier/">Cashier</a>
            </li>
            <li class="overlay__myaccount--menuitem">
                <a data-hijack="false" href="/my-account/?tab=ui-redeem-tab">Claim Code</a>
            </li>
            <li class="overlay__myaccount--menuitem overlay__myaccount--messages">
                <a data-hijack="true" href="/start/">My Messages</a>
                <span data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
            </li>
            <li class="overlay__myaccount--menuitem">
                <a data-hijack="false" href="/my-account/?tab=ui-account-activity-tab">Account Activity</a>
            </li>
            <li class="overlay__myaccount--menuitem">
                <a data-hijack="false" href="/my-account/?tab=ui-convert-points-tab">Points</a>
            </li>
            <li class="overlay__myaccount--menuitem">
                <a data-hijack="false" href="/my-account/?tab=ui-my-details-tab">
                    Personal Details
                </a>
            </li>
            <li class="overlay__myaccount--menuitem">
                <a data-hijack="true" href="" data-bind="click: logout" class="overlay__myaccount--logout">Logout</a>
            </li>
        </ul>
    </nav>
    <a class="close-modal js-close-component" data-component="myAccount">&#215;</a>
</div>
<!-- END My Account Modal -->


<!-- START My Balances Modal -->
<div id="myBalance" class="overlay__mybalance js-modal aspers-modal-menu" aria-labelledby="modalTitle" aria-hidden="true"
    role="dialog">
    <div class="overlay__mybalance--arrow"></div>
    <div class="overlay__mybalance--header">
        <h3 class="overlay__mybalance--welcome">My Balances</h3>
    </div>
    <nav class="overlay__mybalance--menu">
        <ul>
            <li id='ui-balance-switch' class="balance active">
                <span class="balance-text">HIDE BALANCE</span>
                <span class="overlay__mybalance--value">
                    <fieldset class="switch round" tabindex="0">
                        <input id="switch__mybalance" type="checkbox">
                        <label for="switch__mybalance"></label>
                    </fieldset>
                </span>
            </li>

            <li class="overlay__mybalance--menuitem">Real Money
                <span data-bind="text: real" class="overlay__mybalance--value"></span>
            </li>
            <li class="overlay__mybalance--menuitem">Bonus Money
                <span data-bind="text: bonus" class="overlay__mybalance--value"></span>
            </li>
            <li class="overlay__mybalance--menuitem">Bonus Win
                <span data-bind="text: bonusWins" class="overlay__mybalance--value"></span>
            </li>
            <li class="overlay__mybalance--menuitem with-link">
                <a href="/slots/exclusive/">
                    Free Slot Spins
                </a>
                <span data-bind="text: bonusSpins" class="overlay__mybalance--value"></span>
            </li>
            <li class="overlay__mybalance--menuitem loyalty_points--aspers with-link">
                <span class="loyalty_points-placeholder">Points</span>
                <span data-bind="text: points" class="overlay__mybalance--value"></span>
            </li>
            <li class="overlay__mybalance--menuitem">Loyalty Reward
                <span data-bind="text: cashback" class="overlay__mybalance--value"></span>
            </li>
        </ul>
    </nav>
    <a class="close-modal js-close-component" data-component="myBalance">&#215;</a>
</div>
<!-- END My Balances Modal -->

<!-- /header desktop -->
<!-- header for tablet and mobile -->
<!-- nav for mobile -->
<?php include "_partials/nav-for-mobile/nav-for-mobile.php"; ?>
<!-- / nav for mobile -->
<!-- search screen for mobile -->
<?php include "_partials/search-screen/search-screen.php"; ?>
<!-- / search screen for mobile -->

    <?php
        include("_global-library/widgets/prize-wheel/prize-wheel-notification.php");
    ?>
<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>  ">
    <div id="js-content-wrapper-overlay" class="content-wrapper-overlay"></div>
    <!-- AJAX CONTENT WRAPPER -->
    <div id="ajax-wrapper">