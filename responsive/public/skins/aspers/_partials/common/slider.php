<?php include "_global-library/partials/home/home-slide.php" ?>
<div class="home-main-slider" style="width:100%">
    <ul class="" style="position:relative">
        <!--slider now permantly disabled due to marketing and UX findings only 1st image showing no arrows or slides-->
        <li>
            <a data-gtag="Join Now,Web Promo" href="/register/" data-hijack="true">
                <!-- xlarge width screens on desktop -->
                <img width="100%" class="xlarge-slider" src="../_images/slider/xlarge/slide1.jpg">
                <!-- medium width screens on desktop -->
                <img width="100%" class="desktop-slider" src="../_images/slider/desktop/slide1.jpg">
                <!-- mobile -->
                <img class="mobile-slider" src="../_images/slider/mobile/slide1.jpg">
            </a>
        </li>
    </ul>
</div>
