<script type="application/javascript">

	window.setTimeout(function () {
		var el = document.getElementById("ui-first-loader");
		if (el) el.parentNode.removeChild(el);
		el = document.getElementById("ui-loader-animation");
		if (el) el.style.display = "none";
	},1000);

</script>
<!-- header desktop -->
<header id="ui-header-scroll" data-bind="css: { 'header--logged-in' :validSession }" class="header header--logged-<?php echo logged(); ?>">
	<nav class="header-inner">
		<div class="header-inner__left">
			<div class="header-inner__left__top">
				<!-- left nav logged out view -->
				<ul class="desktop-nav loggedout-left" data-bind="visible: !validSession()">
					<li id="slots" class="desktop-nav__link"><a  data-hijack="true" href="/slots/">SLOTS</a></li>
					<li id="live-casino" class="desktop-nav__link"><a data-hijack="true" href="/live-casino/">LIVE CASINO</a></li>
					<li id="table-card" class="desktop-nav__link"><a data-hijack="true" href="/table-card/">TABLE &amp; CARD</a></li>
					<li id="scratch-and-arcade" class="desktop-nav__link"><a data-hijack="true" href="/scratch-and-arcade/">SCRATCH & ARCADE</a></li>
					<li id="my-favourites" class="desktop-nav__link"><a data-hijack="true" href="/roulette/">ROULETTE</a></li>
					<li id="games" class="desktop-nav__link"><a data-hijack="true" href="/games/">ALL</a></li>
				</ul>
				<!-- left nav logged in view -->
				<ul class="desktop-nav loggedin-left" data-bind="visible: validSession" style="display:none;">
					<li id="lobby" class="desktop-nav__link"><a data-hijack="true" href="/start/">START</a></li>
					<li id="slots" class="desktop-nav__link"><a data-hijack="true" href="/slots/">SLOTS</a></li>
					<li id="live-casino" class="desktop-nav__link"><a data-hijack="true" href="/live-casino/">LIVE CASINO</a></li>
					<li id="table-card" class="desktop-nav__link"><a data-hijack="true" href="/table-card/">TABLE &amp; CARD</a></li>
					<li id="scratch-and-arcade" class="desktop-nav__link"><a data-hijack="true" href="/scratch-and-arcade/">SCRATCH & ARCADE</a></li>

					<li id="games" class="desktop-nav__link"><a data-hijack="true" href="/games/">ALL</a></li>
					
				</ul>
				<!--  /left nav logged in view -->
			</div>
			<div class="header-inner__left__bottom">
				
				<div class="cat-search-bar">
					<div class="cat-search-bar__categories ui-game-filter-btn games-filter-menu"><?php include "_global-library/partials/game-filter-menu/game-filter-menu.php";?></div>
					<div class="cat-search-bar__games-search">
						<input type="text" placeholder="Search Games" class="autocomplete ui-search-input"
						data-bind="value: GamesMenuSearch.searchTerm, valueUpdate:'input'">
						<span class="cat-search-bar__games-search__icon"><img src="/_images/common/header/search.png" alt="search"></span>
						<?php include('_global-library/partials/search/search-results-container.php'); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="header-inner__middle">
		<h1 class="sitelogo"><a href="/" data-hijack="true"><img src="/_images/common/header/header-logo.png" alt="Magical Vegas"></a></h1>
	</div>
	<div class="header-inner__right">
		<div class="header-inner__right__top">
			<!-- right nav logged out view -->
			<ul class="desktop-nav loggedout-right" data-bind="visible: !validSession()">

				<li id="promotions" class="desktop-nav__link"><a data-hijack="true" href="/promotions/">PROMOTIONS</a></li>
				<li id="loyalty" class="desktop-nav__link"><a data-hijack="true" href="/loyalty/vip-club/">LOYALTY</a></li>
				<li id="community" class="desktop-nav__link"><a data-hijack="true" href="/banking/">BANKING</a></li>
				<!-- 	<li id="banking" class="desktop-nav__link"><a data-hijack="true" href="/banking/">BANKING</a></li> -->
				<li id="support" class="desktop-nav__link"><a data-hijack="true" href="/support/">SUPPORT</a></li>
			</ul>
			<!-- right nav logged in view -->
			<ul class="desktop-nav loggedin-right" data-bind="visible: validSession" style="display:none;">
				<li id="my-favourites" class="desktop-nav__link"><a data-hijack="true" href="/my-favourites/">MY FAVOURITES</a></li>				
				<li id="promotions" class="desktop-nav__link"><a data-hijack="true" href="/promotions/">PROMOS</a></li>
				<li id="loyalty" class="desktop-nav__link"><a data-hijack="true" href="/loyalty/vip-club/">VIP</a></li>
				<li id="community" class="desktop-nav__link"><a data-hijack="true" href="/banking/">BANKING</a></li> 
				<li id="support" class="desktop-nav__link"><a data-hijack="true" href="/support/">SUPPORT</a></li>
				<li id="my-account" class="desktop-nav__link"><a data-hijack="true" href="/my-account/">MY ACCOUNT</a></li>
				<li  class="desktop-nav__link"><a data-hijack="true" href="" data-bind="click: logout">LOGOUT</a></li>
			</ul>
			<!-- /right nav logged in view -->
		</div>
		<div class="header-inner__right__bottom">
			
			<div class="buttons-bar" style="display: none" data-bind="visible: !validSession()">
				<div class="buttons-bar__login"><a data-reveal-id="login-modal" href="/login/">LOGIN</a></div>
				<div class="buttons-bar__join"><a id="web-join-top" data-hijack="true" href="/register/">JOIN NOW</a></div>
			</div>
			<div class="balance-bar" data-bind="visible: validSession" style="display:none;">
				<ul class="balances-bar">
					<?php if(config("inbox-msg-on")): ?>
						<li class="balances-bar__link message-inbox">
							<a data-hijack="true" href="/start/" title="Message Inbox" class="balances-bar__link__inner">
								<span class="balances-bar__message-inbox__icon"></span>
								<span class="balances-bar__message-inbox__value" data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
							</a>
						</li>
					<?php endif; ?>
					<li class="balances-bar__link account-balance">
						<a data-hijack="true" href="/my-account/" title="Account Balance" class="balances-bar__link__inner" data-bind="text: balance">£000,000.00</a>
					</li>
					<li class="balances-bar__link moolah-balance"><a title="Moolah Balance" data-hijack="true" href="/my-account/" class="balances-bar__link__inner"><span class="balances-bar__link__icon"></span><span data-bind="text: points">...</span></a></li>
					<li class="balances-bar__link deposit-cta"><a class="balances-bar__link__inner" data-hijack="true" href="/cashier/">DEPOSIT</a></li>
				</ul>
			</div>
		</div>
	</div>
</nav>
</header>
<!-- /header desktop -->
<!-- header for tablet and mobile -->
<!-- nav for mobile -->
<?php include "_partials/nav-for-mobile/nav-for-mobile.php"; ?>
<!-- / nav for mobile -->

<!-- search screen for mobile -->
<?php include "_partials/search-screen/search-screen.php"; ?>
<!-- / search screen for mobile -->
<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>  ">
	<!-- AJAX CONTENT WRAPPER -->

	<div id="ajax-wrapper">