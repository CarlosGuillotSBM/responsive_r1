<?php
$currentUrl = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$isAspers = (config("SkinID") === 12);

    ?>
    <script type="application/javascript">
        skinModules.push({ id:"AspersAppTracking"});
    </script>
    <?php

?>

<script type="application/javascript">
skinModules.push({
     id: "LoginBox",
     options: {
          modalSelectorWithoutDetails: "#custom-modal",
          global: true
     }
});
</script>

<?php  include'_partials/common/aspers-responsivebanner-1.php'; ?>
<article class="login-box playlogin__wrapper">
     <h2 class="playlogin">Player login</h2>
     <div class="login-box__form-wrapper">
          <form class="" id="ui-login-form">
               <div class="login-box__input-wrapper">
                    <input type="text" placeholder="Username / E-mail" data-bind="value: LoginBox.username, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" class="playlogin__input playlogin__input--username" />
                    <span style="display:none" data-bind="visible: LoginBox.invalidUsername" class="error">This field is required.</span>
               </div>
               <div class="login-box__input-wrapper">
                    <input type="password" placeholder="Password" data-bind="attr:{type: LoginBox.showPassword() == true ? 'text' : 'password'}, value: LoginBox.password, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" class="playlogin__input playlogin__input--password" />
                    <span style="display:none" data-bind="visible: LoginBox.invalidPassword" class="error">This field is required.</span>
                    <div class="login-box__show-password show-password eye-shut" data-bind="click: LoginBox.togglePassword">
                    </div>
               </div>
               <span style="display:none" data-bind="visible: LoginBox.loginError, html: LoginBox.loginError" class="error"></span>
               <div class="aspersbutton">
                    <span class="login-box__login aspersbutton__copy" data-bind="click: LoginBox.doLogin.bind($data, GameLauncher.gameId(), GameLauncher.gamePosition(), GameLauncher.gameContainerKey(), GameLauncher.roomId())">Login</span>
               </div>
               <div class="clearfix"></div>
               <div class="playlogin__links--wrapper">
                   
                    <a data-gtag="Join Now,Login Box" href="/register/" data-hijack="true" class="login-box__register-link green-cta">JOIN NOW</a>
                    <a href="/forgot-password/" class="login-box__forgot-password">Forgot Password?</a>
               </div>
          </form>
     </div>
</article>