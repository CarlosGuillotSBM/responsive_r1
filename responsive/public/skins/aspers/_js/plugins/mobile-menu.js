var sbm = sbm || {};

/**
// MOBILE NAV ICON AND SLIDE OUT NAVIGATION
// Hamburger menu button toggle animation
// animation for the mobile-menu slide from right needs to toggle the height and opacity on .mobile-menu currently right: -320px; and 0 opacity
// new values AFTER ANIMATED should be right: 0PX; opacity: 1;

// Categories button toggle animation
// animation for the game categories slide down needs to be toggle the height and opacity on
// .games-filter-menu__popover-wrap is set currently 0px height and 0 opacity
// new values after animation should be height: 246px; opacity: 1;
*/

sbm.MobileMenu = function() {
     "use strict";

     return {

          /**
          * common properties
          */
          menuOffSet: "-414px",
          menuTopAreaOffset: "-414px",
          categoriesMenuExpanded: false,
          categoriesMenuHeight: "246px",
          $burgerMenu: $('.menu-toggle-wrap'),
          $burger: $(".burger"),
          $body: $("body"),
          $openComponent: $('.js-open-component'),
          $closeComponent: $('.js-close-component'),
          $popModals: $('.js-modal'),
          $contentOverlay: $('#js-content-wrapper-overlay'),
          $mobileMenu: $('#js-mobile-menu'),
          $accountContainer: $('#myAccount'),
          menuItemHeight: 50,

          /**
          * Handles the clicks on the burger icon
          */
          clicksOnBurger: function() {

               this.$burgerMenu.on("click", function() {
                    this.$burger.toggleClass("active");

                    if (this.$burger.hasClass("active")) {
                         // open menu
                         this.$mobileMenu.addClass('open');
                         this.$popModals.removeClass('open');
                         this.$contentOverlay.addClass('is-active');
                         this.stopBodyScrolling(true);
                    } else {
                         // close menu
                         this.$mobileMenu.removeClass('open');
                         this.$contentOverlay.removeClass('is-active');
                         this.stopBodyScrolling(false);
                    }

                    this.closeSearchMenu();

               }.bind(this));
          },
          closeSearchMenu: function() {
               //hide search screen
               $(".search-screen").css("visibility", "hidden").animate({
                    "bottom": "-100%"
               }, "slow");
          },
          closeAll: function() {
               this.$burger.removeClass("active");
               this.closeSearchMenu();
          },
          /**
          * Handles clicks on links
          * Should close menu and sub-menu
          */
          clicksOnLinks: function() {
               $(".mobile-menu").find("a").not(".tab__item").on("click", function() {
                    this.categoriesMenuExpanded = false;
                    this.closeSearchMenu();
                    this.$mobileMenu.removeClass('open');
                    this.$contentOverlay.removeClass('is-active');
                    this.stopBodyScrolling(false);
               }.bind(this));

               $(".search-screen-mobile__header").on("click", function() {
                    this.$mobileMenu.removeClass('open');
                    this.$contentOverlay.removeClass('is-active');
                    this.stopBodyScrolling(false);
                    this.categoriesMenuExpanded = false;
                    this.closeSearchMenu();

               }.bind(this));
          },
          highlightLinks: function(route) {
               var $links = $(".mobile-menu").find("a.menu-link");
               $links.removeClass("active");
               $links.each(function() {
                    if ($(this).attr("href") === route) {
                         $(this).addClass("active");
                         return false;
                    }
               });
          },
          clicksOnSearch: function() {
               // click to open game search screen
               var self = this;
               $(".ui-search-screen").click(function(e) {
                    e.preventDefault();
                    //show search screen
                    self.$popModals.removeClass('open');
                    self.$burger.removeClass('active');
                    $(".search-screen").css("visibility", "visible").delay(300).animate({
                         "bottom": "0px"
                    }, "normal");
               });
          },
          handleRouting: function() {
               $(window).on("newContentLoaded", function() {
                    this.closeAll();
               }.bind(this));
          },
          /**
          * The starting point
          * This function is called from the initialization of the framework
          */

          clickToOpenComponent: function(  ) {
               var self = this;
               this.$openComponent.on('click', function ( event ) {
                    event.preventDefault();
                    var $this = $(this);
                    var $component = self.getComponent( $this );
                    self.$popModals.removeClass('open');
                    self.$burger.removeClass('active');
                    self.$mobileMenu.removeClass('open');
                    $component.addClass('open');
                    self.$contentOverlay.addClass('is-active');
                    self.stopBodyScrolling(true);
               })
          },
          clickToCloseComponent: function(  ) {
               var self = this;
               this.$closeComponent.on('click', function ( event ) {
                    event.preventDefault();
                    var $this = $(this);
                    var $component = self.getComponent( $this );
                    $component.removeClass('open');
                    self.$contentOverlay.removeClass('is-active');
                    self.stopBodyScrolling(false);
               })
          },
          getComponent: function( $elementClicked ) {
               var componentSelector = '#' + $elementClicked.data('component');
               return $(componentSelector);
          },
          clickOutsideToCloseComponent: function ( ) {
               var self = this;
               self.$contentOverlay.on('click', function( ) {
                    self.$contentOverlay.removeClass('is-active');
                    self.$popModals.removeClass('open');
                    self.$burger.removeClass('active');
                    self.$mobileMenu.removeClass('open');
                    self.stopBodyScrolling(false);
               });
          },
          /**
         * Handles clicks on the Slots and Live Casino categories sub-menu
         */
        toggleSubmenu: function(toggleBtn) {
            var height = $(toggleBtn).siblings('.mobile-menu-item__submenu').outerHeight();
            var $parent = $(toggleBtn).closest('.mobile-menu-item');

            if ($parent.hasClass('is-active')) {
                $parent.removeClass('is-active');
                $parent.css('height', this.menuItemHeight);
                $parent.find('.chevron-icon').css('transform', 'scale(1, 1)');
            } else {
                $parent.addClass('is-active');
                $parent.css('height', this.menuItemHeight + height);
                $parent.find('.chevron-icon').css('transform', 'scale(1, -1)');
            }

            // this.closeSearchMenu();
        },

        initSubmenus: function() {
            var self = this;
            $('.js-toggle-filters').on('click', function() {
                self.toggleSubmenu.call(self, this);
            });
        },
          run: function() {
               this.clicksOnBurger();
               // this.clicksOnCategories();
               this.clicksOnLinks();
               this.highlightLinks(window.location.pathname);
               this.clicksOnSearch();
               this.handleRouting();
               this.clickToOpenComponent();
               this.clickToCloseComponent();
               this.clickOutsideToCloseComponent();
               this.clickOnAccount();
               this.initSubmenus();
          },
          clickOnAccount: function () {
               var self = this;
               self.$accountContainer.find('a').on('click', function( e ) {
                    self.$popModals.removeClass('open');
                    self.$mobileMenu.removeClass('open');
                    self.$contentOverlay.removeClass('is-active');
                    self.stopBodyScrolling(false);
               });
          },
          stopBodyScrolling: function (bool) {
               var self = this;
               if ( bool ) {
                    self.$body.addClass('burger-menu-active');

               } else {
                    self.$body.removeClass('burger-menu-active');
               }
          }
     };

     $( "#Welcome" ).remove();

};
