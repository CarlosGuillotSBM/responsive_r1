<?php if(config("Edit")):?>
<div class="editor-buttons" data-bind="visible: validSession()">
    <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="options-menu">Options</a>
    <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(-3) }" value="Logged Out"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(-1) }" value="Red"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(-2) }" value="Newbie"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(1) }" value="Bronze"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(2) }" value="Silver"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(3) }" value="Gold"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(4) }" value="Ruby"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(5) }" value="Emerald"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(6) }" value="Emerald Elite"></li>
    </ul>
</div>
<?php endif;?>


<span style="display: none" data-bind="visible: !validSession() || playerClass() == -3"><a data-gtag="Banner Widget,<?php echo $row['Text13'];?>" data-hijack="true" href="/<?php echo $row['Text13'];?>/"><img  src="<?php echo $row['Image'];?>" alt="<?php echo $row['Alt'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == -1"><a data-gtag="Banner Widget,<?php echo $row['Alt1'];?>" data-hijack="true" href="/<?php echo $row['Alt1'];?>/"><img  src="<?php echo $row['Image1'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == -2"><a data-gtag="Banner Widget,<?php echo $row['Text2'];?>" data-hijack="true" href="/<?php echo $row['Text2'];?>/"><img  src="<?php echo $row['Text1'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == 1"><a data-gtag="Banner Widget,<?php echo $row['Text4'];?>" data-hijack="true" href="/<?php echo $row['Text4'];?>/"><img  src="<?php echo $row['Text3'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == 2"><a data-gtag="Banner Widget,<?php echo $row['Text6'];?>" data-hijack="true" href="/<?php echo $row['Text6'];?>/"><img  src="<?php echo $row['Text5'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == 3"><a data-gtag="Banner Widget,<?php echo $row['Text8'];?>" data-hijack="true" href="/<?php echo $row['Text8'];?>/"><img  src="<?php echo $row['Text7'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == 4"><a data-gtag="Banner Widget,<?php echo $row['Text10'];?>" data-hijack="true" href="/<?php echo $row['Text10'];?>/"><img  src="<?php echo $row['Text9'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == 5"><a data-gtag="Banner Widget,<?php echo $row['Text12'];?>" data-hijack="true" href="/<?php echo $row['Text12'];?>/"><img  src="<?php echo $row['Text11'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && playerClass() == 6"><a data-gtag="Banner Widget,<?php echo $row['Text15'];?>" data-hijack="true" href="/<?php echo $row['Text15'];?>/"><img  src="<?php echo $row['Text14'];?>"></a></span>