<!-- You can add a class to style the main title. Take into account that usually it comes with a <p> tag on it -->
<div class="offer--headline">
<?php	echo @$row['Body']; ?>
</div>
<!-- You can add a class to style the fine print. Take into account that usually it comes with a <p> tag on it -->
<div class="offer--terms">
<?php
	if(@$row['Text1'] != '') echo '<a data-hijack="true" href="' . @$row['Text1'] . '">';

	echo @$row['Intro'];
	if(@$row['Text1'] != '') echo '</a>';
?>
</div>