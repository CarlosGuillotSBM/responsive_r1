<script type=""application/javascript"">

 var sbm = sbm || {};
 sbm.games = sbm.games || [];
 sbm.games.push({
  "id"    : <?php echo json_encode($row['Text1']);?>,
  "icon"  : <?php echo json_encode($row['Text4']);?>,
  "type"  : "game",
  "bg"    : <?php echo json_encode($row['Text3']);?>,
  "title" : <?php echo json_encode($row['Text5']);?>,
  "desc"  : <?php echo json_encode($row["Intro"]);?>,
  "thumb" : <?php echo json_encode($row['Image1']);?>
 });

</script>



<!-- 2nd bonus -->
<li>
	<div class="claim-wrap">

		<div class="claim-container"><span><?php echo $row['Title'];?></span></div>
		<a href="#" class="play-cta" data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row['Text1'];?>" data-gtag="Welcome <?php echo $row['Name'];?>"><?php echo $row['Text2'];?></a>
	</div>
	<div class="icon">
		<span class="spin"><img src="<?php echo $row['Image'];?>" alt="<?php echo $row['Alt'];?>"></span>
	</div>
</li>