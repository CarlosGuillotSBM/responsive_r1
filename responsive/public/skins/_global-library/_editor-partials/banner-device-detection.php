<?php //print_r($row); ?>
<?php if(config("Edit")):?>
    <input type='button' data-bind="click: function () { device('web') }" value="Web" style='position:absolute;margin:-25px 0px;'>
    <input type='button' data-bind="click: function () { device('tablet') }" value="Tablet" style='position:absolute;margin:-25px 45px;'>
    <input type='button' data-bind="click: function () { device('phone') }" value="Phone" style='position:absolute;margin:-25px 99px;'>
<?php endif;?>

<span style="display: none" data-bind="visible: device() === 'web'">
    <?php if($row['Alt'] != '') echo '<a data-hijack="true" href="'.$row['Alt'].'">';?>
        <img data-gtag="BannerDeviceDetection,web+<?php echo $row['Text3'] ?>" class="banner" src="<?php echo $row['Image'];?>"> 
    <?php if($row['Alt'] != '') echo '</a>'; ?>
</span>

<span style="display: none" data-bind="visible: device() === 'tablet'">
    <?php if($row['Alt1'] != '') echo '<a data-hijack="true" href="'.$row['Alt1'].'">';?>
        <img data-gtag="BannerDeviceDetection,tablet+<?php echo $row['Text4'] ?>" class="banner" src="<?php echo $row['Image1'];?>">
    <?php if($row['Alt1'] != '') echo '</a>'; ?>
</span>

<span style="display: none" data-bind="visible: device() === 'phone'">
    <?php if($row['Text2'] != '') echo '<a data-hijack="true" href="'.$row['Text2'].'">';?>
        <img data-gtag="BannerDeviceDetection,phone+<?php echo $row['Text5'] ?>" class="banner" src="<?php echo $row['Text1'];?>">
    <?php if($row['Text2'] != '') echo '</a>'; ?>
</span>