    <!-- winner repeater-->
    <?php 
		if(config("RealDevice") < 3)
		{
			foreach($this->winners as $winner){?>

		  <script type="application/javascript">

			var sbm = sbm || {};
			sbm.games = sbm.games || [];
			sbm.games.push({
			  "id"    : <?php echo json_encode($winner['DetailURL']);?>,
			  "icon"  : "<?php echo $winner['Image'];?>",
			  "bg"    : <?php echo json_encode($winner['Background']);?>,
			  "title" : <?php echo json_encode($winner['Title']);?>,
			  "type"  : "game",
			  "desc"  : <?php echo json_encode($winner["Description"]);?>,
			  "thumb" : "<?php echo $winner['Image1'];?>",
			  "detUrl": <?php echo json_encode($winner['DetailsURL']);?>
			});

		  </script>


		  <div class="latest-winners__winner slide" data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $winner["DetailURL"]; ?>">
			<div class="latest-winners__winner__icon"><span><img src="<?php echo $winner["Image"]; ?>" alt=""></span></div>
			<div class="latest-winners__winner__player-name"><span><?php echo $winner["Firstname"]; ?> Won</span></div>
			<div class="latest-winners__winner__amount"><span class="ui-latest-winner-amount" data-amount=""><?php echo $winner["CurrencyCode"] . $winner["Win"]; ?></span></div>
			<div class="latest-winners__winner__game-name"><span>On <?php echo ellipsis($winner["Title"],20); ?></span></div>
			<div class="latest-winners__winner__cta">
			  <span style="visibility: hidden" data-bind="text: validSession() ? 'Play Now' : 'Try Game', style: { visibility: 'visible' }">Play Now</span>
			</div>
		  </div>

		<?php }}		
	?>