<script>

    skinPlugins.push({
        id: "FeaturedGames",
        options: {
            global: true
        }
    });

</script>

<script type="application/javascript">

    skinPlugins.push({
        id: "Slick",
        noRepeat: true,
        options: {
            slideWidth: <?php if (config("RealDevice") == 1) echo 170; else if (config("RealDevice") == 2) echo 200; else echo 140?>,
            responsive: true,
            minSlides: 2,
            maxSlides: <?php if (config("RealDevice") == 1) echo 4; else if (config("RealDevice") == 2) echo 100; else echo 100?>,
            slideMargin: 10,
            auto: <?php echo (isset($row['Text3']) && !empty($row['Text3']))? 'false':'true';?>,
            infinite: false,
            element: 'bxslider-start-featured-games',
            onSliderLoad: function(){
                $(".ui-loading-carousel").css("display","none");
                $(".ui-featured-game-icons").css("visibility", "visible");
                $(".ui-featured-game-icons").css("height", "auto");
              }
        }
    });
</script>

<!-- Featured Games Box with tabbed content -->
<div class="category-carousel-lobby-wrap__content-box__header">
    <div class="title hasicon">
        <div class="title__icon">
            <!-- include svg icon partial -->
            <?php include '_global-library/partials/start/icon-featured-game.php'; ?>
            <!-- end include svg icon partial -->
        </div>
        <div class="title__text"><?php echo $row['Title'];?></div>

        <span class="content-holder__title__link"><a href="<?php echo (isset($row['Url']) && $row['Url'] !== '' ) ? $row['Url'] : (($row['Key'] == '' ? '/games' : '/slots/' . $row['Key'])); ?>">View All <span>&rsaquo;<span></a></span>
    </div>
</div>

<?php

    if (config("RealDevice") == 3) { // phone
        include '_global-library/_editor-partials/category-carousel/start-featured-games-slider-mobile.php';
    
    } else if (config("RealDevice") == 2) { // tablet
        include '_global-library/_editor-partials/category-carousel/start-featured-games-slider-mobile.php';
    
    } else { // desktop
        include '_global-library/_editor-partials/category-carousel/start-featured-games-slider-desktop.php';
    }

?>



