<?php if ($row['DetailsURL'] !== 'playBingo') { ?>
<script type="application/javascript">

	var sbm = sbm || {};
	sbm.games = sbm.games || [];
	sbm.games.push({
		"id"    : <?php echo json_encode($row['DetailsURL']);?>,
		"icon"  : "<?php echo $row['Image'];?>",
		"bg"    : <?php echo json_encode($row['Text1']);?>,
		"title" : <?php echo json_encode($row['Title']);?>,
		"type"  : "game",
		"desc"  : <?php echo json_encode($row["Intro"]);?>,
		"thumb" : "<?php echo $row['Image1'];?>",
		"detUrl": "/slots/game/<?php echo $row['DetailsURL'];?>"
	});

</script>    
	<?php } ?>
	<!-- jackpot -->
	<?php if ($row['DetailsURL'] !== 'playBingo') { ?>
	<div class="progressive-jackpots__progressive slide" data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row["DetailsURL"];?>">
	<?php } else {?>	
	<div class="progressive-jackpots__progressive slide" data-bind="click: playBingo">
	<?php } ?>
		<div class="progressive-jackpots__progressive__icon"><span><img src="<?php echo @$row['Image'];?>" alt=""></span></div>
		
	<div class="progressive-jackpots__progressive--wrapper">
		<div class="progressive-jackpots__progressive__title"><span><?php echo @$row['Title'];?></span></div>
		<div class="progressive-jackpots__progressive__amount"><span class="ui-prog-jackpot-amount" data-amount="<?php echo $this->getProgressiveByName(@$row['Text2']);?>">...</span></div>
	</div>
	<div class="progressive-jackpots__progressive__cta">
		<span style="visibility: hidden" data-bind="text: validSession() ? 'Play Now' : 'Try Game', style: { visibility: 'visible' }">Play Now</span>
	</div>
	</div>
    <!-- /jackpot -->