
<?php
     $content = array(
          'image-desktop' =>  $row['Text4'],
          'image-desktop-retina' =>  $row['Text5'],
          'image-tablet' =>  $row['Text6'],
          'image-tablet-retina' =>  $row['Text7'],
          'image-mobile' =>  $row['Text8'],
          'image-mobile-retina' =>  $row['Text9'],
     );
?>


    



<style media="screen">
      .promo__jackpot {
          background: url('<?php echo $content['image-desktop'] ?>');
          background-image: -webkit-image-set(url('<?php echo $content['image-desktop'] ?>') 1x,
                                              url('<?php echo $content['image-desktop-retina'] ?>') 2x);

          min-height: 285px;
         background-size: cover;
         background-repeat: no-repeat;
         background-position: 49.9% center;
         padding: 0 0 58.20% 0;
     }

     @media only screen and (max-width: 64em) and (min-width: 40.0625em) {
          .promo__jackpot {
               background: url('<?php echo $content['image-desktop'] ?>');
               background-image: -webkit-image-set(url('<?php echo $content['image-tablet'] ?>') 1x,
                                                   url('<?php echo $content['image-tablet-retina'] ?>') 2x);
               height: 274px;
              background-size: cover;
              background-repeat: no-repeat;
              background-position: center center;
              padding: 0;
          }
     }
     @media only screen and (max-width: 40em) {
          .promo__jackpot {
               background: url('<?php echo $content['image-desktop'] ?>');
               background-image: -webkit-image-set(url('<?php echo $content['image-mobile'] ?>') 1x,
                                                   url('<?php echo $content['image-mobile-retina'] ?>') 2x);
               height: 238px;
              background-size: cover;
              background-repeat: no-repeat;
              background-position: center center;
              padding: 0;
          }
     }
</style>


<div class="small-12 promo__jackpot">
     <div class="promo__jackpot--wrapper">
          <h3 class="promo__jackpot--firstline">
               <?php echo ($row['Title']);?>
          </h3>
          <h3 class="promo__jackpot--secondline">
               &pound;<?php 
               $contentCMS = ($row['Text3']);
               $cmsArr = explode(",",$contentCMS);
               echo (number_format($this->getProgressiveByNameFromArray($cmsArr)));  
              ?>
          </h3>
          <div class="aspersbutton--promobottom">
               <a href="<?php echo ($row['Text2']);?>" class="aspersbutton__copy">
                    <?php echo ($row['Text1']);?>
               </a>
          </div>
     </div>
</div>