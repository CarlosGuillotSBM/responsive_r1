<?php if(config("Edit")):?>
<div class="editor-buttons">
	<a data-dropdown="drop3" aria-controls="drop3" aria-expanded="false" class="options-menu">Options</a>
	<ul id="drop3" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(-1) }" value="Logged Out"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(0) }" value="0 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(1) }" value="1 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(2) }" value="2 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(3) }" value="3 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(4) }" value="+3 dep"></li>
	</ul>
</div>
<?php endif;?>

<span style="display: none" data-bind="visible: !validSession() || depositCount() == -1"><a data-gtag="Banner Widget,<?php echo $row['Text9'];?>" data-hijack="true" href="/<?php echo $row['Text9'];?>/"><img class="banner" src="<?php echo $row['Image'];?>" alt="<?php echo $row['Alt'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && depositCount() == 0"><a data-gtag="Banner Widget,<?php echo $row['Alt1'];?>" data-hijack="true" href="/<?php echo $row['Alt1'];?>/"><img class="banner" src="<?php echo $row['Image1'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && depositCount() == 1"><a data-gtag="Banner Widget,<?php echo $row['Text2'];?>" data-hijack="true" href="/<?php echo $row['Text2'];?>/"><img class="banner" src="<?php echo $row['Text1'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && depositCount() == 2"><a data-gtag="Banner Widget,<?php echo $row['Text4'];?>" data-hijack="true" href="/<?php echo $row['Text4'];?>/"><img class="banner"  src="<?php echo $row['Text3'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && depositCount() == 3"><a data-gtag="Banner Widget,<?php echo $row['Text6'];?>" data-hijack="true" href="/<?php echo $row['Text6'];?>/"><img class="banner"  src="<?php echo $row['Text5'];?>"></a></span>
<span style="display: none" data-bind="visible: validSession() && depositCount() > 3"><a data-gtag="Banner Widget,<?php echo $row['Text8'];?>" data-hijack="true" href="/<?php echo $row['Text8'];?>/"><img class="banner"  src="<?php echo $row['Text7'];?>"></a></span>