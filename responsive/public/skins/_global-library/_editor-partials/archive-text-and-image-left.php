<?php
// this is because when you content swap it doesn't add the /
$url = (substr($row['DetailsURL'],-1) != '/') ? $row['DetailsURL'] . '/' : $row['DetailsURL'];
?>

<article  class="text-image-left-partial">

	<h1><?php echo $row['Title'];?></h1>
		<?php if($row['DetailsURL'] != '') echo '<a data-hijack="false" href="'.$url.'">';?>
		<img alt="<?php echo $row['Alt']; ?>" src="<?php echo $row['Image']; ?>" class="text-image-left-partial__content__img">


		<?php if($row['DetailsURL'] != '') echo '</a>';?>
  <p><?php echo $row['Intro']; ?></p>

</article>