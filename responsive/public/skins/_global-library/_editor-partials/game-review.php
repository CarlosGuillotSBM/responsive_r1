<!--
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$$ SEO MICRO DATA FOR THIS PARTIAL
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
Review date:
Review body:
Reviewer:
Category:
-->
<article class="game-review" itemscope itemtype="http://schema.org/Review">
	<div class="game-review__content">
		
		<h1 class="game-review__content__title">
		<a class="game-review__url" data-hijack="true" href="" itemprop="url"><?php echo @$row['Title'];?> </a>
		<small class="game-review__content__date" itemprop="datePublished"><?php echo @$row['Date'];?></small>
		</h1>
		
		<div class="game-review__content__body" itemprop="reviewBody">
			<?php echo @$row['Body'];?>
		</div>
	</div>
	<div class="_author-block">
		<div class="_author-block__avatar">
			<?php if(exists(@$row['Image1'])):?>
			<a data-hijack="true" href="<?php echo @$row['Text2'];?>" target="_author">
				<img width="" src="<?php echo @$row['Image1'];?>" alt="<?php echo @$row['Text1'];?>">
			</a>
			<?php endif;?>
		</div>
		<h4>
		<a data-hijack="true" href="<?php echo @$row['Text2'];?>" class="_author-block__link" target="_author">
			<span itemprop="author" itemscope itemtype="http://schema.org/Person"> <span itemprop="name"><?php echo @$row['Text1'];?></span></span>
		</a>
		</h4>
		<div class="_author-block__line">
			<div class="_author-block__lineleft"></div>
			<div class="_author-block__lineright"></div>
		</div>
		
		<div class="_author-block__biography">
			<p><?php echo @$row['Terms'];?></p>
		</div>
		
	</div>
</article>