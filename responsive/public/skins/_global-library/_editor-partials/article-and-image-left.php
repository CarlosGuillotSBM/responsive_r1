<?php
//$$$$ SEO MICRODATA $$$$
//itemprop="url"
//itemprop="articleBody"
//itemprop="author"
//itemprop="name"
//itemprop="datePublished"
//itemprop="image"
//itemprop="publisher"
?>

<article class="article" <?php if($row['DetailsURL'] != '') echo "onclick=\"location.href='". @$row["Path"] . "'\""; ?> >
	<div class="article__content">


		<div class="article__content__date" itemprop="datePublished"><?php echo @$row['Date'];?></div>
		<div class="article__content__image">
			<?php if( $row['DetailsURL'] != ''): ?>
				<a data-hijack="true" href="<?php echo ($row['DetailsURL'] != ''? $row['Path'] : '#') ;?>">
					<img itemprop="image" src="<?php echo @$row['Image'];?>" alt="<?php echo @$row['Alt'];?>">
				</a>
			<?php else: ?>
				<img itemprop="image" src="<?php echo @$row['Image'];?>" alt="<?php echo @$row['Alt'];?>">
			<?php endif; ?>

		</div>
				<h1 class="article__content__title" itemprop="name">
			<?php if( $row['DetailsURL'] != ''): ?>
				<a class="article__url" data-hijack="true" href="<?php echo ($row['DetailsURL'] != ''? $row['Path'] : '#') ;?>" itemprop="url" ><?php echo @$row['Title'];?> </a>
			<?php else: ?>
				<?php echo @$row['Title'];?>
			<?php endif; ?>
		</h1>
		<div class="article__content__body" itemprop="articleBody">
			<?php echo $row["Intro"];?>
		</div>
		<div class="article__content__cta-container" style="display:none;">
			<!-- CTAs -->
			

			<!-- /CTAs -->
		</div>
	</div>
	<span class="article__publisher" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
			<!-- <span itemprop="name"><?php echo config('Name'); ?></span> -->
	</span>
</article>