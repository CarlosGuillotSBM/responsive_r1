

<script type="application/javascript">
        skinPlugins.push({
            id: "Slick",
            options: {
                slideWidth: 250,
                responsive: false,
                minSlides: 1,
                maxSlides: 100,
                slideMargin: 10,
                auto: false,
                infinite: true,
                element: 'bxslider-promo-carousel-s',
                pager: false
            }
        });
    skinModules.push({
        id: "RadFilter"
    });
</script>


<div class="holder-mobile promo-carrousel">
    <!-- Title -->
    <h1 class="title-mobile"><?php echo $row['Title'] ?>
        <a class="title-link" href="/promotions/" style="font-size: 13px !important;">View All <span>&rsaquo;<span></a>
    </h1>
    <!-- Carrousel -->
    <div class="content-holder-mobile">
        <ul class="bxslider bxslider-promo-carousel-s">
            <?php

                // hack - homepage and start have different structure for the array
                $promos = $this->promotions["promotions"] ? $this->promotions["promotions"] : $this->promotions;

                @$this->repeatData($promos,0,"_partials/promos/promo-slide.php");
            ?>
        </ul>


        <!-- this is to show the TandCs modal popup -->
        
        <?php

        $promos = $this->promotions["promotions"] ? $this->promotions["promotions"] : $this->promotions;

                @$this->repeatData($promos,0,"_partials/promos/promo-modal.php");  

        ?>
       
    </div>
</div>