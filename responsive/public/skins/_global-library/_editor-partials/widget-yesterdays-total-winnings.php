<div class="yesterdays-win__wrapper">
  
  <img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
  
  <article class="yesterdays-win">
    
    <div class="yesterdays-win__title">
      <h1>Yesterdays Total Winnings</h1>
    </div>
    
    <div class="yesterdays-win__box">
      <div class="yesterdays-win__box__amount"><span>&pound;<?php echo number_format(@$this->progressives[1]['Amount'], 2);?></span>
      </div>
    </div>

  </article>
</div>