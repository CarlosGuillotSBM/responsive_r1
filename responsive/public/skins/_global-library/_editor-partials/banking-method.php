<!-- PAYPAL -->
<section class="banking-content__wrap">
  <div class="banking-content__top">
    <h1><?php echo $row["Title"]; ?></h1>
    <span><img src="<?php echo $row["Text1"]; ?>"></span>
  </div>
  <article>
    <ul>
      <li><div class="banking-content__article__left">Deposit</div><div class="banking-content__article__right"><img src="<?php echo (strtoupper($row["Text2"]) == 'Y')? "/_images/common/icons/selected.svg":"/_images/common/icons/no-icon.svg"?>"></div></li>
      <li><div class="banking-content__article__left">Withdrawals</div><div class="banking-content__article__right"><img src="<?php echo (strtoupper($row["Text3"]) == 'Y')? "/_images/common/icons/selected.svg":"/_images/common/icons/no-icon.svg"?>"></div></li>
      <li><div class="banking-content__article__left">Min. Amount Units</div><div class="banking-content__article__right"><?php echo $row["Text4"]; ?></div></li>
      <li><div class="banking-content__article__left">Currency</div><div class="banking-content__article__right"><?php echo $row["Text5"]; ?></div></li>
    </ul>
  </article>
  <p><?php echo $row["Text6"]; ?></p>
</section>
<?php
?>