<?php
	$content = array(
		'line-1' =>  $row['Text1'],
		'line-2' =>  $row['Text2'],
		'line-3' =>  $row['Text3'],
		'cta-text' =>  $row['Text4'],
		'cta-url' =>  $row['Text5'],
		't&c-text' =>  $row['Text6'],
		't&c-url' =>  $row['Text7'],
		'extra-copy' =>  $row['Text8'],
		'image-desktop' =>  $row['Text9'],
		'image-desktop-retina' =>  $row['Text10'],
		'image-table' =>  $row['Text11'],
		'image-table-retina' =>  $row['Text12'],
		'image-mobile' =>  $row['Text13'],
		'image-mobile-retina' =>  $row['Text14'],
	);
?>
<!-- START aspers responsive banner -->
<div class="responsivebanner row">
	<div class="small-12 medium-8 columns responsivebanner__left">
		<div class="small-12 columns">
			<div class="responsivebanner--headline">
				<h3>
					<?php echo $content['line-1']; ?>
				</h3>
			</div>
		</div>
		<div class="small-12 columns">
			<div class="responsivebanner--terms">
				<p><?php echo $content['t&c-text'];?><a href="<?php echo $content['t&c-url'];?>" class="terms-acquisition-banner">T&amp;Cs apply ❯</a></p>
			</div>
		</div>
	</div>
	<div class="small-12 medium-4 columns responsivebanner__right">
		<div class="responsivebanner__cta">
			<a href="<?php echo $content['cta-url'];?> " class="responsivebanner__cta--copy"><?php echo $content['cta-text'];?></a>
		</div>
	</div>
</div>
<!-- END aspers responsive banner -->
<!-- other CMS values
		<?php echo $content['line-1']; ?>
		<?php echo $content['line-2']; ?>
		<?php echo $content['line-3']; ?>
		<?php echo $content['cta-url'];?>  ">
		<?php echo $content['cta-text'];?>
		<?php echo $content['t&c-url'];?>  ">
		<?php echo $content['t&c-text'];?>
		<?php echo $content['extra-copy'] ?>
-->
