<!-- post -->
<article class="row blog__post">
  <div class="small-4 columns blog__post__image">
    <img src="<?php echo $row["Image"];?>">
  </div>
  <div class="small-8 columns blog__post__blurb">
    <h1>
      <a data-hijack="true" href="<?php echo $row["DetailsURL"];?>"><?php echo $row["Title"];?></a> </h1>
  
      <?php echo $row["Intro"];?>
      
  </div>
</article>
      <!-- end post -->