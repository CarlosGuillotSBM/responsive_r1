<?php if(config("Edit")):?>
<div class="editor-buttons" data-bind="visible: validSession()">
	<a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="options-menu">Options</a>
	<ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(-1) }" value="Logged Out"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(0) }" value="0 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(1) }" value="1 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(2) }" value="2 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(3) }" value="3 dep"></li>
		<li class="options-menu__buttons"><input type='button' data-bind="click: function () { changeDepositCount(4) }" value="+3 dep"></li>
	</ul>
</div>
<?php endif;?>

<article class="text-partial" style="display: none" data-bind="visible: !validSession() || depositCount() == -1">
	<h1><?php echo $row['Text1']; ?></h1>
	
</article>

<article class="text-partial" style="display: none" data-bind="visible: validSession() && depositCount() == 0">
	<h1><?php echo $row['Text2']; ?></h1>
	
</article>

<article class="text-partial" style="display: none" data-bind="visible: validSession() && depositCount() == 1">
	<h1><?php echo $row['Text3']; ?></h1>
	
</article>

<article class="text-partial" style="display: none" data-bind="visible: validSession() && depositCount() == 2">
	<h1><?php echo $row['Text4']; ?></h1>
	
</article>

<article class="text-partial" style="display: none" data-bind="visible: validSession() && depositCount() == 3">
	<h1>	<?php echo $row['Text5']; ?></h1>

</article>

<article class="text-partial" style="display: none" data-bind="visible: validSession() && depositCount() > 3">
	<h1><?php echo $row['Text6']; ?></h1>

</article>