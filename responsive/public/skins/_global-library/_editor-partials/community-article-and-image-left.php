<?php
//$$$$ SEO MICRODATA $$$$
//itemprop="url"
//itemprop="articleBody"
//itemprop="author"
//itemprop="name"
//itemprop="datePublished"
//itemprop="image"
//itemprop="publisher"
?>
<article class="article" onclick="location.href='<?php echo "/community/" . @$row['Key'] . "/" . @$row["DetailsURL"];?>/'">
	<div class="article__content">

		<h1 class="article__content__title" itemprop="name">
			<a class="article__url" data-hijack="true" href="<?php echo "/community/" . @$row['Key'] . "/" . @$row['DetailsURL'];?>/" itemprop="url" ><?php echo @$row['Title'];?> </a>
		</h1>
		<div class="article__content__date" itemprop="datePublished"><?php echo @$row['Date'];?></div>
		<div class="article__content__image">
			<a data-hijack="true" href="<?php echo "/community/" . @$row['Key'] . "/" . @$row['DetailsURL'];?>/">
				<img itemprop="image" src="<?php echo @$row['Image'];?>" alt="<?php echo @$row['Alt'];?>">
			</a>
		</div>
		<div class="article__content__body" itemprop="articleBody">
			<?php echo $row["Intro"];?>
		</div>
		<div class="article__content__cta-container" style="display:none;">
			<!-- CTAs -->
			<?php if(@$row["Text4"] != ''):?>
				<a class="article__content__cta" data-hijack="true" href="<?php echo $row["Text4"];?>"><?php echo $row["Text3"];?></a>
			<?php endif; ?>

			<?php if(@$row["Text6"] != ''):?>
				<a class="article__content__cta" data-hijack="true" href="<?php echo $row["Text6"];?>"><?php echo $row["Text5"];?></a>
			<?php endif; ?>
			<!-- /CTAs -->
		</div>
	</div>
	<span class="article__publisher" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
			<span itemprop="name"><?php echo config('Name'); ?></span>
	</span>
</article>