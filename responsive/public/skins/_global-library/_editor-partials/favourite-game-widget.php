<!--
    FAVOURITE GAMES WIDGET
-->
<!-- NO FAVOURITES -->
<div data-bind="foreach: Favourites.noFavourites, visible: Favourites.noFavourites().length > 0" style="display: none">
    <div class="favourites__placeholder">
        <article class="favourite-game-widget">
            <div class="favourite-game-widget__wrapper">
                <div class="favourite-game-widget__bg">
                    <!-- <img src="/_global-library/images/4x3-bg.png"> -->
                    <img src="/_images/common/add-to-favourites.jpg">
                </div>
                <!-- <div class="favourite-game-widget__content">
                    Favourites here
                </div> -->
            </div>
        </article>
    </div>
</div>
<!-- /NO FAVOURITES -->


<!-- FAVOURITES -->
<div data-bind="foreach: Favourites.favourites, visible: Favourites.favourites().length > 0" style="display: none">
    <div class="favourites__placeholder">

        <article class="game-icon">

            <div class="game-icon__wrapper">

                <!-- Image  -->
                <div class="game-icon__image" data-bind="click: $root.Favourites.launchGame.bind($data, Game, Position, Title, Icon, DetailsURL)" >
                    <img data-bind="attr: { src: Icon }" src="/_images/common/no-image.jpg">
                    <div class="clearfix"></div>
                </div>
                <!-- footer  -->
                <div class="game-icon__bottom">
                    <!-- favorite icon  -->
                    <div data-bind="click: $root.Favourites.removeFavourite.bind($data, Game),
                                attr: { 'data-favgameid': Game }" class="game-icon__favourite">
                        <!-- title bar -->
                        <div class="game-icon__favourite__icon icons-star--on"></div>
                    </div>
                    <!-- icon -->
                    <a class="game-icon__title" data-bind="text: Title, attr: { href: DetailsURL }" data-hijack="true" href="/"></a>
                </div>
            </div>
        </article>

    </div>
</div>
<!-- /FAVOURITES -->