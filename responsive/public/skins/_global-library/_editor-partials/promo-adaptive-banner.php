<?php

$playerType = ($playerType !== NULL) ? $playerType : [];
$counter = ($counter == '' || $counter == NULL) ? 1 : $counter;
if($counter == 1){
    $new_count = 0;
    $dep0_count = 0;
    $dep1_count = 0;
    foreach($dataObject as $promoBanner){
        if($promoBanner['Text14'] == 'NEW')
            $new_count = ($new_count > 4) ? 4 : $new_count+1;
        if($promoBanner['Text14'] == 'DEP0')
            $dep0_count = ($dep0_count > 4) ? 4 : $dep0_count+1;
        if($promoBanner['Text14'] == 'DEP1')
            $dep1_count = ($dep1_count > 4) ? 4 : $dep1_count+1;
    }
}


$content = array(
    'line-1' =>  $row['Text1'],
    'line-2' =>  $row['Text2'],
    'line-3' =>  $row['Text3'],
    'cta-text' =>  $row['Text4'],
    'cta-url' =>  $row['Text5'],
    't&c-text' =>  $row['Text6'],
    't&c-url' =>  $row['Text7'],
    'image-desktop' =>  $row['Text8'],
    'image-desktop-retina' =>  $row['Text9'],
    'image-tablet' =>  $row['Text10'],
    'image-tablet-retina' =>  $row['Text11'],
    'image-mobile' =>  $row['Text12'],
    'image-mobile-retina' =>  $row['Text13'],
    'restriction' =>  $row['Text14']
);
if($_COOKIE['username'] == NULL){
    if($content['restriction'] == 'NEW'){
        array_push($playerType, $content);
        if($counter == $new_count){
            include '_partials/banner/promo-adaptive-banner-partial.php';
            $playerType = [];
        }
        $counter++;
    }
}else{
    if($content['restriction'] == 'DEP0' && $_COOKIE['DepositCount'] <= 0){
        array_push($playerType, $content);
        if($counter == $dep0_count){
            include '_partials/banner/promo-adaptive-banner-partial.php';
            $playerType = [];
        }
        $counter++;
    }
    if($content['restriction'] == 'DEP1'){
        array_push($playerType, $content);
        if($counter == $dep1_count){
            include '_partials/banner/promo-adaptive-banner-partial.php';
            $playerType = [];
        }
        $counter++;
    }
}


?>