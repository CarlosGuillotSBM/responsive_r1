<script type="application/javascript">

  var sbm = sbm || {};
  sbm.games = sbm.games || [];
  sbm.games.push({
    "id"    : <?php echo json_encode($row['DetailsURL']);?>,
    "icon"  : "<?php echo $row['Image'];?>",
    "bg"    : <?php echo json_encode($row['Text1']);?>,
    "title" : <?php echo json_encode($row['Title']);?>,
    "type"  : "game",
    "desc"  : <?php echo json_encode($row["Intro"]);?>,
    "thumb" : "<?php echo $row['Image1'];?>",
    "detUrl": <?php echo json_encode($row['Path']);?>,
    "demo": <?php echo json_encode($row['DemoPlay']);?>
  });

</script>
<?php
  // if display image is defined it means it is using a banner to launch the game
  $bannerLauncher = @$row["Text2"] != '' ? true : false;
  // set the image to display accordingly to the record set up
  $image = $bannerLauncher ? $row["Text2"] : $row["Image"];
?>
<img data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row["DetailsURL"];?>" <?php if (!isset($notLazy)) { ?>class="lazy" src="/_images/common/no-image.jpg" <?php } else { ?> src="<?php echo $image;?>"  <?php } ?> alt="<?php echo $row["Alt"];?>"
         data-original="<?php echo $image;?>" alt="">
<!--Game overlay link for desktop-->
<span class="game-overlay">
  <div class="game-overlay__inner">
    <a data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row["DetailsURL"];?>" class="ui-launch-game play-now" >Play Now</a>
    <a data-hijack="true" href="<?php echo $row["Path"];?>/" class="more-info">More info</a>
  </div>
</span>
<!--Game overlay link for tablets and mobile-->
<div class="game-overlay-small-up">
  <a data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row["DetailsURL"];?>"></a>
</div>