<script type="application/javascript">

	var sbm = sbm || {};
	sbm.games = sbm.games || [];
	sbm.games.push({
		"id"    : <?php echo json_encode($row['RoomID']);?>,
		"icon"  : "<?php echo $row['Image'];?>",
		"bg"    : <?php echo json_encode($row['Text2']);?>,
		"title" : <?php echo json_encode($row['Title']);?>,
		"type"  : "room",
		"desc"  : <?php echo json_encode($row['Intro']);?>,
		"thumb" : "<?php echo $row['Image'];?>",
		"detUrl": <?php echo json_encode($row['Path']);?>
	});

</script>

<article class="bingo-icon">
 	
 	<div style="display: none" class="bingo-room__closed-wrapper" data-bind="visible: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].closedText"></div>
 	<span style="display: none" class="bingo-room__closed-message"
	   data-bind="visible: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].closedText,
				  html: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].closedText">
 	</span>
 
 	<div class="bingo-icon__wrapper" data-bind="css: { 'bingo-room__closed': BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].closed }">

		<div class="bingo-icon__image">
			<img class="lazy" alt="<?php echo $row["Alt"];?>" src="/_images/common/no-image.jpg" data-original="<?php echo $row["Image"];?>" style="display: inline-block;">
			<div class="clearfix"></div>
		</div>
		<div class="bingo-icon__content">
			<div class="ui-launch-game play-icon " 
				data-gameid="<?php echo $row["DetailsURL"];?>" 
				data-gametitle="<?php echo $row["Title"];?>" 
				data-gamedescription="<?php echo $row["Intro"];?>"  
				data-details-url="<?php echo $row["Path"];?>" 
				data-icon="<?php echo $row["Image"];?>" 
				data-background="<?php echo $row["Text2"];?>" >
			</div>
			<!-- title of game with link -->
			<a data-hijack="true" class="bingo-icon__content__container" href="<?php echo $row["Path"];?>/">
				
				<span class="icons-type"
					  data-bind="visible: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].displayGameType,
					  			 text: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].displayGameType">
				</span>
				<span class="icons-type" style="display: none"
					  data-bind="visible: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].progressiveRules,
								 attr: { 'title': BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].progressiveRules}">
					<img src="/_global-library/images/bingo/feature-icons/ruleicons_jackpot.svg">
				</span>
				<span class="bingo-icon__content__container__time"
					  data-bind="text: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] ? BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].displaySeconds : 'Soon',
								 css: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].cssDisplayTime">...</span>
				<span class="bingo-icon__content__container__jackpot" data-bind="text: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].displayJackpot"></span>
				<span class="bingo-icon__content__container__cardprice" data-bind="text: BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'] && BingoLauncher.rooms()['<?php echo $row["RoomID"];?>'].cardPrice"></span>
				<span class="icons-info">&nbsp;</span> 
			</a>
			<!-- title bar -->
		</div>
		
		<!-- Game Hover info -->
		
		<div class="bingo-icon__overlay" data-bind="click: BingoLauncher.openRoom.bind($data,'<?php echo $row["RoomID"];?>', '<?php echo $row["Image"];?>', true, '/_global-library/_upload-images/games/backgrounds/cricket-star-slots-screenshot.jpg')">
		<img class="playimg" src="/_images/common/icons/play.png">
		</div>
	</div>
</article>