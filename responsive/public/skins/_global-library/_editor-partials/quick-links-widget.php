<ul class="quick-links-widget">
	<div class="quick-links-widget__title"><?php echo @$row['Title'];?></div>
	<li><a data-gtag="Quick Links Widget,<?php echo $row['Text1'];?>" data-hijack="true" href="<?php echo @$row['Text1'];?>"><?php echo @$row['Alt'];?></a></li>
	<li><a data-gtag="Quick Links Widget,<?php echo $row['Text3'];?>" data-hijack="true" href="<?php echo @$row['Text3'];?>"><?php echo @$row['Text2'];?></a></li>
	<li><a data-gtag="Quick Links Widget,<?php echo $row['Text5'];?>" data-hijack="true" href="<?php echo @$row['Text5'];?>"><?php echo @$row['Text4'];?></a></li>
	<li><a data-gtag="Quick Links Widget,<?php echo $row['Text7'];?>" data-hijack="true" href="<?php echo @$row['Text7'];?>"><?php echo @$row['Text6'];?></a></li>
	<li><a data-gtag="Quick Links Widget,<?php echo $row['Text9'];?>" data-hijack="true" href="<?php echo @$row['Text9'];?>"><?php echo @$row['Text8'];?></a></li>
</ul>