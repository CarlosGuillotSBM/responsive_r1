<script type="application/javascript">
skinModules.push({
id: "OptIn"
});
</script>

<!-- banner image -->
<div style="display: none" data-gtag="Competition Banner,<?php echo $row['DetailsURL'];?>" class="ui-scheduled-content promotion-eng-banner__container <?php if (strtoupper($row["Text4"]) === 'Y') {?> ui-scheduled-default <?php } ?>" style="display: none" data-scheduled-rules ="<?php echo $row["Text3"];?>">

    <a style="display: none" class="banner ui-comp-banner-out<?php echo $row['DetailsURL'];?>" href="/promotions/<?php echo $row['DetailsURL'];?>/">
        <img src="<?php echo $row['Text1'];?>">
    </a>

    <a style="display: none" class="banner ui-comp-banner-in<?php echo $row['DetailsURL'];?>" href="/promotions/<?php echo $row['DetailsURL'];?>/">
        <img src="<?php echo $row['Text2'];?>">
    </a>

    <!-- competition banner infos -->

    <div style="display: none" class="promotion-eng-banner ui-comp-banner-in<?php echo $row['DetailsURL'];?>">


        <ul>
            <li>
                <div class="title">Prize draw ends</div>
                <span class="comp-ends ui-comp-end<?php echo $row["DetailsURL"]?>">...</span>
            </li>
            <li>
                <div class="title">Your tickets</div>
                <span class="comp-entries ui-comp-entries<?php echo $row["DetailsURL"]?>">...</span>
            </li>

            <span class="get-more-tickets-btton">
             <a href="/promotions/<?php echo $row['DetailsURL'];?>/">Get More Tickets</a></span>
        </ul>

    </div>
</div>