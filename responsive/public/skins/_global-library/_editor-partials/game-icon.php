<?php
$cmaFriendly = @$row['CMAFriendly'] ? $row['CMAFriendly'] : 0;
$gameProvider = @$row['GameProviderID'] ? $row['GameProviderID'] : 0;
?>
<script type="application/javascript">

	var sbm = sbm || {};
	sbm.games = sbm.games || [];
	sbm.games.push({
		"id"    : <?php echo json_encode($row['DetailsURL']);?>,
		"icon"  : "<?php echo $row['Image'];?>",
		"bg"    : <?php echo json_encode($row['Text1']);?>,
		"title" : <?php echo json_encode($row['Title']);?>,
		"type"  : "game",
		"desc"  : <?php echo json_encode($row["Intro"]);?>,
		"thumb" : "<?php echo $row['Image1'];?>",
		"detUrl": <?php echo json_encode($row['Path']);?>,
		"cma": <?php echo json_encode($cmaFriendly);?>,
		"provider": <?php echo json_encode($gameProvider);?>,
		"demo": <?php echo json_encode($row['DemoPlay']);?>
	});

</script>
<?php
	// if display image is defined it means it is using a banner to launch the game
	$bannerLauncher = @$row["Text2"] != '' ? true : false;
	// set the image to display accordingly to the record set up
	$image = $bannerLauncher ? $row["Text2"] : $row["Image"];
?>

<article class="game-icon">
	<div class="game-icon__wrapper">

		<div class="game-icon__image">
		
			<img data-bind="click: GameLauncher.openGame"
				 data-gameid="<?php echo $row["DetailsURL"];?>"
				 <?php if (!isset($notLazy)) { ?>class="lazy" src="/_images/common/no-image.jpg" <?php } else { ?> src="<?php echo $image;?>"  <?php } ?> alt="<?php echo $row["Alt"];?>"
				 data-original="<?php echo $image;?>" data-cma-friendly="<?php echo $cmaFriendly?>">

			<!--img style="display:none;" class="free-spins" src="/_images/common/game-free-spins.svg"-->
			<span style="display:none;" class="favourite-toggle added-<?php echo $row["DetailsURL"];?>"></span>
			<!--span style="display:none;" class="game-jackpot" >£2,219,572</span-->

		</div>

		<!-- Game Hover info -->
		<?php if (!$bannerLauncher) : ?>
		<div class="game-icon__overlay">
			<div data-bind="click: GameLauncher.openGame" class="ui-launch-game play-icon "
				 data-gameid="<?php echo $row["DetailsURL"];?>">
				 <img class="playimg" src="/_images/common/icons/play.png">
			</div>
			
			<?php if (config('SkinID') != 9 ) : ?>
				<a data-hijack="true" href="<?php echo $row["Path"];?>/"><?php echo $row["Title"];?></a>
			<?php endif ?>
			
		</div>

		<div class="game-icon__bottom">
			<!-- favorite icon  -->
			<div class="game-icon__favourite">
				<?php
				$str = $row["Title"];
				$limit = 20;
				if (strlen($str) > $limit){
					$str = substr($str, 0, $limit) . ' ...';
				}
				?>
                <div class="game-icon__favourite__icon icons-star--off"
                    data-bind="favourite: '<?php echo $row["DetailsURL"];?>', click: Favourites.toggleFavourite"
                    data-favgameid="<?php echo $row["DetailsURL"];?>">
                </div>
			</div>

			<!-- title of game with link -->
			<a style="display:none;" class="game-icon__title" href="<?php echo $row["Path"];?>/"
                <?php if (in_array(config("SkinID"), array(Skin::GiveBackBingo, Skin::LuckyVIP, Skin::RegalWins))) : ?>
                    data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row["DetailsURL"];?>"
                <?php else : ?>
                    data-hijack="true"
                <?php endif ?>
            ><?php echo $str;?></a>

		</div>
		<?php endif; ?>
	</div>
</article>