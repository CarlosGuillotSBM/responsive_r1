<?php if(config("Edit")):?>
<div class="editor-buttons" data-bind="visible: validSession()">
    <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="options-menu">Options</a>
    <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(-3) }" value="Logged Out"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(-1) }" value="Red"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(-2) }" value="Newbie"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(1) }" value="Bronze"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(2) }" value="Silver"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(3) }" value="Gold"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(4) }" value="Ruby"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(5) }" value="Emerald"></li>
        <li class="options-menu__buttons"><input type='button' data-bind="click: function () { changePlayerClass(6) }" value="Emerald Elite"></li>
    </ul>
</div>
<?php endif;?>


<p style="display: none" data-bind="visible: !validSession() || playerClass() ==  -3"><?php echo $row['Text1']; ?></p>

<p style="display: none" data-bind="visible: validSession() && playerClass() ==  -2"><?php echo $row['Text2']; ?></p>
    
<p style="display: none" data-bind="visible: validSession() && playerClass() ==  -1"><?php echo $row['Text3']; ?></p>
    
<p style="display: none" data-bind="visible: validSession() && playerClass() ==  1"><?php echo $row['Text4']; ?></p>
    
<p style="display: none" data-bind="visible: validSession() && playerClass() ==  2"><?php echo $row['Text5']; ?></p>
    
<p style="display: none" data-bind="visible: validSession() && playerClass() ==  3"><?php echo $row['Text6']; ?></p>

<p style="display: none" data-bind="visible: validSession() && playerClass() ==  4"><?php echo $row['Text7']; ?></p>

<p style="display: none" data-bind="visible: validSession() && playerClass() ==  5"><?php echo $row['Text8']; ?></p>

<p style="display: none" data-bind="visible: validSession() && playerClass() ==  6"><?php echo $row['Text9']; ?></p>