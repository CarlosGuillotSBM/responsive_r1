<article  class="text-image-left-partial">

	<h1><?php echo $row['Title'];?></h1>
		<?php if($row['DetailsURL'] != '') echo '<a data-hijack="true" href="'.$row['Path'].'">';?>
		<img alt="<?php echo $row['Alt']; ?>" src="<?php echo $row['Image']; ?>" class="text-image-left-partial__content__img">
		<?php if($row['DetailsURL'] != '') echo '</a>';
		echo $row['Intro']; ?>

</article>