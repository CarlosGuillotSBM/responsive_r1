  <!-- MAIN CONTENT AREA -->
  <div class="content-wrapper">

    <!-- TOP CONTENT -->
    <div class="top-content">
      <!-- TOP CONTENT LEFT COLUMN -->
      <div class="top-content__left">
        <!-- breadcrumbs -->
        <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
        <!-- end breadcrumbs -->
      </div>
      <!-- END TOP CONTENT LEFT COLUMN -->

      <!-- TOP CONTENT RIGHT COLUMN -->
      <div class="top-content__right">
        <!-- social -->
        <?php include '_global-library/widgets/social-share-links/social-share-links.php'; ?>
        <!-- end social -->
      </div>
      <!-- END TOP CONTENT RIGHT COLUMN -->

    </div>
    <!-- END TOP CONENT -->

    <!-- MIDDLE CONTENT -->
    <div class="middle-content">

      <!-- VANILLA TEMPLATE CONTENT -->
      <a data-bind="edit: {page: '<?php echo $this->page;?>', key: '<?php echo $this->key;?>'}" style="display: none" data-nohijack="true" href="/_global-library/editor/editor.php">Edit Page</a>
      <div class="vanilla-template">
        <?php include '_global-library/templates/vanilla-template/vanilla-template__content.php'; ?>
      </div>
      <!-- /VANILLA TEMPLATE CONTENT-->

    </div>
    <!-- END MIDDLE CONTENT -->

    <!-- BOTTOM CONTENT -->
    <div class="bottom-content">

    </div>
    <!-- END Games info modal -->

    

  </div>
  <!-- END HOME MAIN CONTENT AREA -->





