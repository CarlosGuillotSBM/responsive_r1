<article  class="vanilla-template__content__child">

	<h1  class="vanilla-template__content__child__title"><?php echo $row['Title'];?></h1>
	<div class="vanilla-template__content__child__content">
		<?php echo $row['Body']; ?>
	</div>
	<div class="vanilla-template__content__child__img">
		<img src="<?php echo $row['Image']; ?>">
	</div>
	<div class="clearfix"></div>
</article>