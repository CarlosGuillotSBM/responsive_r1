<?php if(config("RealDevice") == 1 && $xLargeRoute != ''):?>
<!-- xlarge width screens on desktop -->                
<img width="100%" class="xlarge-slider" src="<?php echo $xLargeRoute?>" alt="<?php echo $altCopy?>">
<?php endif;?>

<?php if((config("RealDevice") == 1 || config("RealDevice") == 2) && $mediumRoute != ''):?>
<!-- medium width screens on desktop -->
<img width="100%" class="desktop-slider" src="<?php echo $mediumRoute?>" alt="<?php echo $altCopy?>">
<?php endif;?>

<?php if(config("RealDevice") != 2 && $smallRoute != ''):?>
<!-- mobile -->
<img class="mobile-slider" src="<?php echo $smallRoute?>" alt="<?php echo $altCopy?>">
<?php endif;?>
