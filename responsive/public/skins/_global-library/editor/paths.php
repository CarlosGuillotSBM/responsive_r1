<?php

if (!isset($_SERVER["COMPUTERNAME"])) {
    // A MAC
    @define('COMPUTER', 'MacDev');
} else {
    // A PC
    define('COMPUTER', $_SERVER["COMPUTERNAME"]);
}

if (empty($include_path)) {
    $pi = pathinfo(__FILE__);

    // editor to responsive dir is 4 levels
    $base = dirname($pi['dirname'], 4); // get 'responsive' directory
    $paths = [
        'app' => join(DIRECTORY_SEPARATOR, [$base, 'application']) . DIRECTORY_SEPARATOR,
        'skin' => join(DIRECTORY_SEPARATOR, [$base, 'public', 'skins']) . DIRECTORY_SEPARATOR,
        'base' => '.'
    ];

    $include_path = join(PATH_SEPARATOR, $paths);
}

set_include_path($include_path);