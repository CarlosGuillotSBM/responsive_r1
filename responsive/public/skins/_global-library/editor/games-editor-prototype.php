<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/_global-library/css/foundation.min.css" />
    <link rel="stylesheet" type="text/css" href="/_global-library/css/games-editor.css"/>
    <link rel="stylesheet" type="text/css" href="/_global-library/css/jquery-ui/jquery-ui.min.css"/>
    <script src="/_global-library/js/bower_components/modernizr/modernizr.js"></script>
    <script src="/_global-library/js/app/helpers/add-modules.js"></script>
    <script type="application/javascript">
        var skinModules = [], skinPlugins = [];
        skinModules.push({ id: "GamesEditorPrototype" });
        skinModules.push({ id: "Notifier" });
    </script>
</head>
<body>

<!-- NAVIGATION -->

<nav class="top-bar" data-topbar>
    <ul class="title-area">
        <li class="name">
            <h1>Games Editor</h1>
        </li>
    </ul>

    <section class="top-bar-section">
        <!-- Right Nav Section -->
        <ul class="right">
            <li class="active"
                data-bind="css: { active: GamesEditorPrototype.addingGames , inactive: !GamesEditorPrototype.addingGames() }">
                <a data-bind="click: GamesEditorPrototype.showSection.bind($data, 'add-games')" href="#">Add Games</a>
            </li>
            <li data-bind="css: { active: GamesEditorPrototype.orderingGames, inactive: !GamesEditorPrototype.orderingGames() }">
                <a data-bind="click: GamesEditorPrototype.showSection.bind($data, 'order-games')" href="#">Order Games</a>
            </li>
        </ul>
    </section>
</nav>
<!-- /NAVIGATION -->

<!-- CONTAINER -->
<div class="container">

    <!-- Adding Games Container -->
    <div data-bind="visible: GamesEditorPrototype.addingGames" class="adding-games">

        <!-- Add game -->
        <div class="row">
            <div class="small-2 columns">
                <label>Add New Game</label>
            </div>
            <div class="small-5 columns">
                <input type="text" placeholder="Game ID">
            </div>
            <div class="small-5 columns">
                <input data-bind="value: GamesEditorPrototype.gameToBeAdded" type="text" placeholder="Game Title">
            </div>
        </div>
        <!-- /Add game -->


        <div class="row">
            <div data-bind="visible: GamesEditorPrototype.gameToBeAdded" style="display: none">
                <span data-bind="text: 'You are adding ' + GamesEditorPrototype.gameToBeAdded()" class="success label"> </span>
            </div>
            <br/><br/>
        </div>

        <div class="row">

            <table id="ui-add-game-to-skins">
                <thead>
                <tr>
                    <th width="120">Category</th>
                    <th width="120">Spin And Win <input data-bind="click: GamesEditorPrototype.addToSkin" type="checkbox"></th>
                    <th width="120">Kitty Bingo <input type="checkbox"></th>
                    <th width="120">Lucky Pants <input type="checkbox"></th>
                    <th width="120">Direct Slot <input type="checkbox"></th>
                    <th width="120">Magical <input type="checkbox"></th>
                    <th width="120">Bingo Extra <input type="checkbox"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td></td>
                    <td>W T P</td>
                    <td>W T P</td>
                    <td>W T P</td>
                    <td>W T P</td>
                    <td>W T P</td>
                    <td>W T P</td>
                </tr>
                <tr>
                    <td>Home</td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                </tr>
                <tr>
                    <td>Slots</td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                </tr>
                <tr>
                    <td>3-Reel Slots</td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                </tr>
                <tr>
                    <td>5-Reel Slots</td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                </tr>
                <tr>
                    <td>Jackpot Slots</td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                    <td>
                        <input type="checkbox">
                        <input type="checkbox">
                        <input type="checkbox">
                    </td>
                </tr>
                </tbody>
            </table>
            <a href="#" class="button [radius round] right">Add Game</a>
        </div>

    </div>
    <!-- /Adding Games Container -->

    <!-- Ordering Games -->
    <div data-bind="visible: GamesEditorPrototype.orderingGames" class="ordering-games" style="display: none">
        <div class="row">
            <div class="small-4 columns">
                <select>
                    <option>Spin And Win</option>
                    <option>Kitty Bingo</option>
                    <option>Lucky Pants</option>
                    <option>Direct Slot</option>
                    <option>Magical Vegas</option>
                    <option>Bingo Extra</option>
                </select>
            </div>
            <div class="small-4 columns">
                <select>
                    <option>Web</option>
                    <option>Tablet</option>
                    <option>Phone</option>
                </select>
            </div>
            <div class="small-4 columns">
                <select data-bind="value: GamesEditorPrototype.selectedCategory">
                    <option>Select Category</option>
                    <option>Slots</option>
                    <option>Table</option>
                    <option>Scratch</option>
                    <option>All</option>
                </select>
            </div>
        </div>
        <div data-bind="visible: GamesEditorPrototype.selectedCategory() !== 'Select Category'">
            <div class="row">
                <a data-bind="click: GamesEditorPrototype.toggleCategory.bind($data, 'slots')" href="#"><h3>Slots</h3></a>
                <div data-bind="visible: GamesEditorPrototype.expandSlots" class="small-12 columns">
                    <button href="#" data-dropdown="drop11" aria-controls="drop11" aria-expanded="false" class="button dropdown tiny">Preview...</button>
                    <ul id="drop11" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
                        <li>SAW</li>
                        <li>KB</li>
                        <li>LP</li>
                        <li>DS</li>
                        <li>MV</li>
                        <li>BE</li>
                    </ul>
                    <button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="button dropdown tiny">Clone to...</button>
                    <ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
                        <li><input type="checkbox">SAW</li>
                        <li><input type="checkbox">KB</li>
                        <li><input type="checkbox">LP</li>
                        <li><input type="checkbox">DS</li>
                        <li><input type="checkbox">MV</li>
                        <li><input type="checkbox">BE</li>
                        <li><input type="button" value="Clone"></li>
                    </ul>
                    <button href="#" data-dropdown="drop1331" aria-controls="drop1331" aria-expanded="false" class="button dropdown tiny">Copy to Skin...</button>
                    <ul id="drop1331" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
                        <li><input type="checkbox">SAW</li>
                        <li><input type="checkbox">KB</li>
                        <li><input type="checkbox">LP</li>
                        <li><input type="checkbox">DS</li>
                        <li><input type="checkbox">MV</li>
                        <li><input type="checkbox">BE</li>
                        <li><input type="button" value="Copy"></li>
                    </ul>
                    <button href="#" data-dropdown="drop133" aria-controls="drop133" aria-expanded="false" class="button dropdown tiny">Copy to Category...</button>
                    <ul id="drop133" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
                        <li><input type="checkbox">Slots</li>
                        <li><input type="checkbox">Table</li>
                        <li><input type="checkbox">Scratch</li>
                        <li><input type="button" value="Copy"></li>
                    </ul>
                    <button href="#" data-dropdown="drop112" aria-controls="drop112" aria-expanded="false" class="button dropdown tiny">Release...</button>
                    <ul id="drop112" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
                        <li>SAW</li>
                        <li>KB</li>
                        <li>LP</li>
                        <li>DS</li>
                        <li>MV</li>
                        <li>BE</li>
                        <li><input type="button" value="Release"></li>
                    </ul>
                    <button href="#" data-dropdown="drop113" aria-controls="drop112" aria-expanded="false" class="button dropdown tiny">Revert...</button>
                    <ul id="drop113" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
                        <li>SAW</li>
                        <li>KB</li>
                        <li>LP</li>
                        <li>DS</li>
                        <li>MV</li>
                        <li>BE</li>
                        <li><input type="button" value="Revert"></li>
                    </ul>
                    <a data-bind="click: GamesEditorPrototype.auto" href="" class="tiny button">Auto</a>
                    <a href="#" class="tiny button">Remove</a>
                </div>
            </div>
            <div data-bind="visible: GamesEditorPrototype.expandSlots" class="row">
                <div class="small-12 columns">
                    <ul class="games-list inline-list">
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                    </ul>

                    <a href="#" class="tiny button alert">Save</a>
                </div>
            </div>



            <div class="row">
                <a data-bind="click: GamesEditorPrototype.toggleCategory.bind($data, 'table')" href=""><h3>Table</h3></a>

                <div data-bind="visible: GamesEditorPrototype.expandTable" class="small-12 columns">
                    <button href="#" data-dropdown="drop11" aria-controls="drop11" aria-expanded="false" class="button dropdown tiny">Preview...</button>
                    <button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="button dropdown tiny">Clone to...</button>
                    <button href="#" data-dropdown="drop1331" aria-controls="drop1331" aria-expanded="false" class="button dropdown tiny">Copy to Skin...</button>
                    <button href="#" data-dropdown="drop133" aria-controls="drop133" aria-expanded="false" class="button dropdown tiny">Copy to Category...</button>
                    <button href="#" data-dropdown="drop112" aria-controls="drop112" aria-expanded="false" class="button dropdown tiny">Release...</button>
                    <button href="#" data-dropdown="drop113" aria-controls="drop112" aria-expanded="false" class="button dropdown tiny">Revert...</button>
                    <a data-bind="click: GamesEditorPrototype.auto" href="" class="tiny button">Auto</a>
                    <a href="#" class="tiny button">Remove</a>
                </div>
            </div>
            <div data-bind="visible: GamesEditorPrototype.expandTable" class="row">
                <div class="small-12 columns">
                    <ul class="games-list inline-list">
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                    </ul>
                    <a href="#" class="tiny button alert">Save</a>
                </div>
            </div>


            <div class="row">
                <a data-bind="click: GamesEditorPrototype.toggleCategory.bind($data, 'scratch')" href=""><h3>Scratch</h3></a>

                <div data-bind="visible: GamesEditorPrototype.expandScratch" class="small-12 columns">
                    <button href="#" data-dropdown="drop11" aria-controls="drop11" aria-expanded="false" class="button dropdown tiny">Preview...</button>
                    <button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="button dropdown tiny">Clone to...</button>
                    <button href="#" data-dropdown="drop1331" aria-controls="drop1331" aria-expanded="false" class="button dropdown tiny">Copy to Skin...</button>
                    <button href="#" data-dropdown="drop133" aria-controls="drop133" aria-expanded="false" class="button dropdown tiny">Copy to Category...</button>
                    <button href="#" data-dropdown="drop112" aria-controls="drop112" aria-expanded="false" class="button dropdown tiny">Release...</button>
                    <button href="#" data-dropdown="drop113" aria-controls="drop112" aria-expanded="false" class="button dropdown tiny">Revert...</button>
                    <a data-bind="click: GamesEditorPrototype.auto" href="" class="tiny button">Auto</a>
                    <a href="#" class="tiny button">Remove</a>
                </div>
            </div>
            <div data-bind="visible: GamesEditorPrototype.expandScratch" class="row">
                <div class="small-12 columns">
                    <ul class="games-list inline-list">
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                        <li>
                            <img class="thumbnail" src="/_global-library/_upload-images/games/list-icons/3-wheel-roulette-game.jpg">
                        </li>
                    </ul>
                    <a href="#" class="tiny button alert">Save</a>
                </div>
            </div>

        </div>

    </div>
    <!-- /Ordering Games -->
</div>
<!-- /CONTAINER -->

<!--<p>DEBUG</p>-->
<!--<pre data-bind="text: ko.toJSON($data.GamesEditorPrototype, null, 2)"></pre>-->

<?php
include "_global-library/partials/common/app-js.php";
?>

<script src="/_global-library/js/bower_components/tinymce/tinymce.min.js"></script>
<script src="/_global-library/js/bower_components/tinymce/jquery.tinymce.min.js"></script>

</body>
</html>