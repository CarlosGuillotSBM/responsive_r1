
<?php if (isAdminInSkinEditor()): ?>

<!-- Add user -->
<div data-bind="visible: SkinEditor.addingUsers()" class="new-user-panel" style="display: none">

    <h4>New User</h4>

    Username: <input type="text" data-bind="value: SkinEditor.newUsername, valueUpdate: ['input', 'keypress']">
    Password: <input type="password" data-bind="value: SkinEditor.newPassword, valueUpdate: ['input', 'keypress']">
    Is Admin ? <input type="checkbox" data-bind="checked: SkinEditor.newIsAdmin" >
    Can Upload ? <input type="checkbox" data-bind="checked: SkinEditor.newCanUpload" >
    <select name="cars" data-bind="value: SkinEditor.newUserGroup, valueUpdate: ['input', 'keypress']">
        <option value="1">DaubK</option>
        <option value="2">8Ball</option>
    </select>

    <input type="button" value="Back List" data-bind="click: SkinEditor.backList">
    <div class="action-button">
        <input type="button" value="Add User" data-bind="click: SkinEditor.addNewUser">
    </div>

    <span class="error" data-bind="text: SkinEditor.displayError, visible: SkinEditor.displayError" style="margin-top: 6px;"></span>

</div>
<!-- Add user -->

<?php endif; ?>