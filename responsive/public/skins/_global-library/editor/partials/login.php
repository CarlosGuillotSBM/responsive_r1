<!-- LOGIN DIALOG -->
<div data-bind="visible: SkinEditor.loginVisible()" class="login-panel" style="display: none">
    <input data-bind="value: SkinEditor.username,
                          valueUpdate: ['input', 'keypress'],
                          event:{keypress: SkinEditor.checkForEnterKey}" type="text" placeholder="Username">
    <input data-bind="value: SkinEditor.password,
                          valueUpdate: ['input', 'keypress'],
                          event:{keypress: SkinEditor.checkForEnterKey}" type="password" placeholder="password">
    <input data-bind="click: SkinEditor.doLogin" type="button" value="Login">
    <span class="error" data-bind="text: SkinEditor.displayError, visible: SkinEditor.displayError" style="margin-top: 6px;"></span>
</div>
<!-- /LOGIN DIALOG -->