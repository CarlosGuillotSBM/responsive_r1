<!-- loding modal -->

<div data-bind="visible: Notifier.loading"
     style="display: block; position: fixed; top: 50%; left: 50%;
         width: 200px; height: 200px; margin-top: -100px; margin-left: -100px;
         z-index: 999999;">
    <img src="/_global-library/images/loading.gif">
</div>

<!-- /loding modal -->

<!-- notification -->

<div data-bind="visible: Notifier.notification() && Notifier.system() === 'notification',
                css: { 'success': Notifier.severity() ===  'success',
                       'alert': Notifier.severity() ===  'error',
                       'info': Notifier.severity() ===  'info',
                       'warning': Notifier.severity() ===  'warning' }"
     style="display: none; position: fixed; top: 0px; z-index: 999999999; width: 100%;"
     class="alert-box radius">
    <span data-bind="text: Notifier.notification"></span>
    <a data-bind="click: Notifier.clearNotification" href="#" class="close">&times;</a>
</div>

<!-- /notification -->

<!-- alert modal -->

<div class="dialog-modal" data-bind="visible: Notifier.notification() && Notifier.system() === 'alert',
                css: { 'success': Notifier.severity() ===  'success',
                       'alert': Notifier.severity() ===  'error',
                       'info': Notifier.severity() ===  'info',
                       'warning': Notifier.severity() ===  'warning' }"

     style="display: none; position: fixed; top: 50%; left: 50%; color: #FFFFFF;
     width: 400px; height: 200px; margin-top: -100px; margin-left: -200px; padding: 8px;
     z-index: 999999;background-color: #000000">

    <img data-bind="attr: { src: '/_global-library/images/alert/' + Notifier.severity() + '.png' }">
    <p data-bind="text: Notifier.notification"></p>
    <a data-bind="click: Notifier.clearNotification" href="#" class="button expand">OK</a>

</div>

<!-- /alert modal -->
