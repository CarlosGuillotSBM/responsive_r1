<!-- CHECK SKIN IMAGES -->

<div style="display: none" data-bind="visible: SkinEditor.checkingSkinImages() && SkinEditor.validSession()">

    <h3>Check Skin Images</h3>

    <div class="content">
        <div class="row">
            <div class="small-4 columns">
                <select data-bind="value: SkinEditor.checkImgEnvironment">
                    <option value="1">Live</option>
                    <option value="0">Staging</option>
                </select>
            </div>
            <div class="small-4 columns">
                <select data-bind="value: SkinEditor.checkImgDevice">
                    <option value="1">Web</option>
                    <option value="2">Mobile</option>
                </select>
            </div>
            <div class="small-4 columns">
                <input data-bind="click: SkinEditor.checkSkinImages" type="button" value="Check Images">
            </div>
        </div>

        <div>
            <table>
                <thead>
                <td width="50">Id</td>
                <td width="150">Page</td>
                <td width="200">Key</td>
                <td width="200">Content</td>
                <td>Images</td>
                </thead>
                <tbody data-bind="foreach: SkinEditor.skinPages">
                <tr>
                    <td data-bind="text: contentId"></td>
                    <td data-bind="text: id"></td>
                    <td><a data-bind="text: key, click: edit"></a></td>
                    <td><a data-bind="text: subkey, click: edit"></a></td>
                    <td data-bind="foreach: images">
                        <img class="img-zoom" data-bind="attr: { src: file, title: file }">
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>

</div>

<!-- /CHECK SKIN IMAGES -->