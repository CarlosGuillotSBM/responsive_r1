<!-- NAVIGATION -->

<nav class="top-bar" data-topbar>

    <section class="top-bar-section">

        <?php if (isLoggedInSkinEditor()): ?>
        <!--  Left Nav Section -->
        <ul class="left">
            <li class="has-dropdown">

                <a href="/_global-library/editor/editor.php">CONTENT EDITOR</a>
                <ul class="dropdown">
                    <li><a href="/_global-library/editor/games-editor.php">GAMES EDITOR</a></li>
                </ul>
            </li>
        </ul>
        <?php endif; ?>

        <span data-bind="text: SkinEditor.skinName" class="skin-colour"></span>


        <?php if (isLoggedInSkinEditor()): ?>
        <!-- Right Nav Section -->
        <ul class="right">

            <?php if (isAdminInSkinEditor()): ?>
            <li class="active" style="display: none"
                data-bind="css: { active: SkinEditor.editingUsers , inactive: !SkinEditor.editingUsers() },
                    visible: SkinEditor.key != 'SEO'">
                <a data-bind="click: SkinEditor.showSection.bind($data, 'USERS')" href="#">Users</a>
            </li>
            <?php endif; ?>

            <li class="active" style="display: none"
                data-bind="css: { active: SkinEditor.editingContent , inactive: !SkinEditor.editingContent() },
                    visible: SkinEditor.key != 'SEO'">
                <a data-bind="click: SkinEditor.showSection.bind($data, 'content')" href="#">Content</a>
            </li>
            <li class="active" style="display: none"
                data-bind="css: { active: SkinEditor.editingSubPages , inactive: !SkinEditor.editingSubPages },
                    visible: SkinEditor.key != 'SEO'">
                <a data-bind="click: SkinEditor.showSection.bind($data, 'SUBPAGES')" href="#">Sub-Pages</a>
            </li>
            <li data-bind="css: { active: SkinEditor.editingSEO, inactive: !SkinEditor.editingSEO() }">
                <a data-bind="click: SkinEditor.showSection.bind($data, 'SEO')" href="#">SEO</a>
            </li>
            <li>
                <a data-bind="click: SkinEditor.newTcsAndPP">Compliance</a>
            </li>
            <li>
                <a data-bind="click: SkinEditor.logout">Log out</a>
            </li>
            <li class="has-dropdown">
                <a>More...</a>
                <ul class="dropdown">
                    <li>
                        <a data-bind="click: SkinEditor.flushCache" href="#">Flush Cache</a>
                    </li>
                    <li>
                        <a data-bind="click: SkinEditor.showSkinImages" href="#">Check Skin Images</a>
                    </li>
                    <li>
                        <a data-bind="click: SkinEditor.compareLiveAndStaging" href="#">Live vs. Staging</a>
                    </li>
                </ul>
            </li>
        </ul>
        <?php endif; ?>
    </section>
</nav>

<!-- /NAVIGATION -->