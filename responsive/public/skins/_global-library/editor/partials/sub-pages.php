<!-- LIST SUBPAGES -->

<div style="display: none" data-bind="visible: SkinEditor.editingSubPages">
    <br/>
    <table class="list">
        <thead>
        <tr>
            <th width="200">URL</th>
            <th width="220">Date</th>
            <th width="80"></th>
        </tr>
        </thead>
        <tbody data-bind="foreach: SkinEditor.subpages">
        <tr class="ui-grid-item">
            <td data-bind="text: url"></td>
            <td><span data-bind="text: date"></span> <br/> <small data-bind="text: user"></small></td>
            <td>
                <input data-bind="click: $parent.SkinEditor.reloadContent.bind($data, subkey, url)" type="button" value="Edit">
            </td>
        </tr>
        </tbody>
    </table>
</div>

<!-- /LIST SUBPAGES -->