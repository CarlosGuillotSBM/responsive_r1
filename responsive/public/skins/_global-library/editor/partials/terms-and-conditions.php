<!-- Terms and Conditions -->

<div style="display: none" data-bind="visible: SkinEditor.editingTermsAndConditions() && SkinEditor.validSession()">

    <h3>Terms And Conditions</h3>

    <div class="content">
        
        <div class="row ">
            <div class="small-12 columns">
                <p class="text-warning" data-bind="visible: SkinEditor.termsAndConditions.editMode() === 'new'">Please Note: You are creating a new version of T&C's</p>
                <p class="text-warning" data-bind="visible: SkinEditor.termsAndConditions.editMode() === 'edit'">Please Note: You are editing T&C's and this won't increase the version</p>
            </div>
        </div>

        <div>
            <textarea class="tinymce" id="ui-terms-conditions"></textarea>
        </div>

        
        <div class="row">
            <div class="small-8 columns left">
                <input id="cbUpdatedFullTcs" type="checkbox" data-bind="checked: SkinEditor.termsAndConditions.updatedFullTcs" class="always-editable"><label class="always-editable" for="cbUpdatedFullTcs">I have already updated the <a href="/terms-and-conditions/" target="_blank">Full T&C's</a></label>
            </div>
            <div class="small-2 columns">
                <!-- <input type="button" value="Save"> -->
                <input type="button" value="Publish" data-bind="click: SkinEditor.publishTCs">
            </div>
        </div>

    </div>

</div>

<!-- /Terms and Conditions -->