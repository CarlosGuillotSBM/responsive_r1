<!-- EDIT CONTENT -->
<style>
.datatab__left{
    width: 30%;
    display: inline-block;
}
.datatab__left div{
    width: 100%;
}
.datatab__left select{
    width: 50%;
    display: inline-block;
}
.datatab__middle{
    display: inline-block;
    width: 40%;
    position: relative;
    top: -30px;
}
.datatab__middle label{
    width: 100%;
}
.datatab__middle select{
    width: 70%;
    display: inline-block;
}
.datatab__right{
    display: inline-block;
    width: 30%;
}
.datatab__right div{
    width: 100%;
}
.datatab__right input{
    width: 75%;
    display: inline-block;
}
</style>
<div style="display: none;margin-top: 24px;" data-bind="visible: SkinEditor.editingContent() && SkinEditor.validSession()">

    <h3 data-bind="text: SkinEditor.key + ' ' + SkinEditor.subKey "></h3>

    <!-- LIST -->

    <div data-bind="visible: !SkinEditor.editing()">
        <input class="top-buttons-action" data-bind="click: SkinEditor.editContent.bind($data, '0', SkinEditor.defaultTypeId(), 0)" type="button" value="Add New">
        <input class="top-buttons-action" data-bind="click: SkinEditor.saveOrder" type="button" value="Save Order">
        <input class="top-buttons-action" data-bind="click: SkinEditor.syncLive" type="button" value="Sync Live">
        <input class="top-buttons-action" data-bind="click: SkinEditor.revertAllContent" type="button" value="Revert All">
        <br/>
        <br/>
        <table class="list" data-page-length="25">
            <thead>
            <tr>
                <th data-class-name="priority" style="display: none">ID</th>
                <th width="20"></th>
                <th width="120">Type</th>
                <th width="200">Name</th>
                <th width="200">Title</th>
                <th width="80">Devices</th>
                <th width="80">Status</th>
                <th width="220">Date Updated</th>
                <th width="220">Publish Date</th>
                <th width="80">Edition</th>
                <th width="50">Sync</th>
            </tr>
            </thead>
            <tbody data-bind="foreach: SkinEditor.categories" data-order='[[ 1, "asc" ]]' data-page-length='25'>
            <tr data-bind="css: { 'modifying' : editingUser !== '' || status == 'Modifying',
                                          'hiding' : status == 'Hiding',
                                          'hidden' : status == 'Hidden',
                                          'archiving' : status == 'Archiving',
                                          'archived' : status == 'Archived' }" class="ui-grid-item">
                <td data-orderable="true" style="display: none" data-bind="text: gridId"></td>
                <td data-bind="text: order"></td>
                <td data-bind="text: typeName"></td>
                <td data-orderable="false" data-bind="text: name"></td>
                <td data-bind="html: title"></td>
                <td data-bind="text: devices"></td>
                <td data-bind="text: status"></td>
                <td><span data-bind="text: date"></span> <br/> <small data-bind="text: user"></small></td>
                <td><span data-bind="text: publishDate"></span></td>
                <td>
                    <input data-bind="visible: split == false, click: $parent.SkinEditor.editContent.bind($data, id, 0, 0)" style="display: none" type="button" value="Edit">
                    <input data-bind="visible: split == true, click: $parent.SkinEditor.editContent.bind($data, id, 0, 1)" style="display: none" type="button" value="Edit Web">
                    <input data-bind="visible: split == true, click: $parent.SkinEditor.editContent.bind($data, id, 0, 2)" style="display: none" type="button" value="Edit Mobile">
                </td>
                <td><input type="checkbox" data-bind="checked: toSync, value: toSync" > <br/></td>
            </tr>
            </tbody>
        </table>
    </div>
    
    <!-- /LIST -->

    <!-- EDIT FORM -->

    <div data-bind="visible: SkinEditor.editing" id="ui-editable-form" class="editor-panel" style="display: none">

        <div class="row">
            <div class="small-8 columns">
                <small> Id: <span data-bind="text: SkinEditor.editableContent().id"></span> </small> &nbsp;
                <small> Partial: <span data-bind="text: SkinEditor.editableContent().partial"></span> </small>
            </div>
            <div class="small-8 columns">
                <div class="right">
                    <input
                        data-bind="click: SkinEditor.saveEditableContent.bind($data, SkinEditor.editableContent(), null),
                                               visible: SkinEditor.editableContent().id > 0"
                        type="button" value="Save">
                    <input
                        data-bind="click: SkinEditor.saveEditableContent.bind($data, SkinEditor.editableContent(), 1),
                                               visible: SkinEditor.editableContent().id === 0"
                        type="button" value="Save to Top">
                    <input
                        data-bind="click: SkinEditor.saveEditableContent.bind($data, SkinEditor.editableContent(), 0),
                                               visible: SkinEditor.editableContent().id === 0"
                        type="button" value="Save to Bottom">
                    <input
                        data-bind="click: SkinEditor.deleteItem.bind($data, SkinEditor.editableContent().id),
                                               visible: SkinEditor.editableContent().id > 0"
                        type="button" value="Delete">
                    <input data-bind="click: SkinEditor.cancelEditing" type="button" value="Cancel">
                </div>
            </div>

        </div>
        <br/>
        <div class="row">
            <div class="small-6 columns">
                <p data-bind="text: SkinEditor.editableContent().DefaultContentCategoryTypeName" class="label small-4 label-content-type"></p>
            </div>
            <div class="small-6 columns">
                <select class="always-editable right" style="max-width: 400px"
                        data-bind="options: SkinEditor.editableContent().types,
                                    optionsValue: 'ID',
                                    optionsText: 'Name',
                                    value: SkinEditor.editableContent().contentCategoryTypeID"
                        class="right">
                </select>
            </div>
        </div>
        <div>
            <input id="cbWeb" type="checkbox" data-bind="checked: SkinEditor.editableContent().forWeb" class="always-editable"><label class="always-editable" for="cbWeb">Web</label>
            <input id="cbMobile" type="checkbox" data-bind="checked: SkinEditor.editableContent().forMobile" class="always-editable"><label class="always-editable" for="cbMobile">Mobile</label>
            <dl class="tabs" data-tab>
                <dd class="active"><a href="#pDetails" id="Details">Details</a></dd>
                <dd data-bind="visible: SkinEditor.introEnabled"><a id="tabIntro" href="#pIntro">Intro</a></dd>
                <dd data-bind="visible: SkinEditor.bodyEnabled"><a id="tabBody" href="#pBody">Body</a></dd>
                <dd data-bind="visible: SkinEditor.termsEnabled"><a id="tabTerms" href="#pTerms">Terms</a></dd>
            </dl>

            <div class="tabs-content">
                <div class="content active" id="pDetails">
                    <label for="Active">Active</label><select id="Active" class="always-editable" data-bind="value: SkinEditor.editableContent().status">
                        <option value="0">Hidden</option>
                        <option value="1">Active</option>
                        <option value="2">Archive</option>
                    </select>
                    <label for="Name">Name</label> <input id="Name" type="text" data-bind="value: SkinEditor.editableContent().name">
                    <label for="Title">Title</label> <input  id="Title" type="text" data-bind="value: SkinEditor.editableContent().title">
                    <label for="DetailsURL">Details URL</label><input id="DetailsURL" type="text" data-bind="value: SkinEditor.editableContent().detailUrl">

                    <!-- Image -->
                    <div id="editImageContiner">
                        <div class="row">
                            <div id="ImagePick" class="small-8 columns">
                                <label for="Image">Image</label>
                                <input id="Image" type="text" data-bind="value: SkinEditor.editableContent().image" disabled>

                                <input data-bind="click: SkinEditor.clearImage.bind($data, 1)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 1)" type="button" value="Select Image...">
                                <small>
                                    Ideal image size: <span data-bind="text: SkinEditor.editableContent().imageWidth + ' x ' + SkinEditor.editableContent().imageHeight"></span>
                                </small>
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().image,
                                                attr: { src : SkinEditor.editableContent().image,
                                                alt: SkinEditor.editableContent().imageAlt }">
                            </div>
                        </div>
                        <div class="row">

                        </div>
                    </div>

                    <label for="ImageAlt">Image Alt</label> <input id="ImageAlt" type="text" data-bind="value: SkinEditor.editableContent().imageAlt">
                    <!-- /Image -->

                    <!-- Image 1 -->
                    <div id="editImageContiner2">
                        <div class="row">
                            <div id="ImagePick1" class="small-8 columns">
                                <label for="Image1">Image 2</label>
                                <input id="Image1" type="text" data-bind="value: SkinEditor.editableContent().image1" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 2)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 2)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().image1,
                                                attr: { src : SkinEditor.editableContent().image1,
                                                alt: SkinEditor.editableContent().imageAlt1 }">
                            </div>
                        </div>
                    </div>

                    <label for="ImageAlt1">Image Alt 2</label> <input id="ImageAlt1" type="text" data-bind="value: SkinEditor.editableContent().imageAlt1">
                    <!-- /Image 1 -->

                    <label for="DatesTable">Dates</label>
                    <table id="DatesTable" class="layout">
                        <tr>
                            <td>
                                <label for="DateFrom_DST">Date From</label> <input type="text" id="DateFrom_DST" data-bind="value: SkinEditor.editableContent().dateFrom">
                            </td>
                            <td>
                                <label for="DateTo_DST">Date To</label> <input id="DateTo_DST" type="text" id="ui-datepicker-to" data-bind="value: SkinEditor.editableContent().dateTo">
                            </td>
                        </tr>

                    </table>

                    <label for="ButtonText">Call to Action</label> <input id="ButtonText" type="text" data-bind="value: SkinEditor.editableContent().buttonText">
                    <label for="ExtraText">Extra Text</label> <input id="ExtraText" type="text" data-bind="value: SkinEditor.editableContent().extraText">

                    <!-- Add game -->
                    <div id="ui-search-game">
                        <div class="row">
                            <div class="small-2 columns">
                                <label for="GameID"></label>
                                <input id="GameID" data-bind="value: SkinEditor.editableContent().gameId"
                                       type="text" placeholder="ID">
                            </div>
                            <div class="small-8 columns">
                                <label for="GameTitle">Title</label>
                                <input id="GameTitle" type="text" placeholder="Title" class="ui-game-search">
                            </div>
                            <div class="small-2 columns">
                                <label></label>
                                <input data-bind="click: SkinEditor.getGameInfo" type="button" value="Get Game Info" style="margin: 26px 0px 0px -8px">
                            </div>
                        </div>

                    </div>
                    <!-- /Add game -->

                    <label for="OfferDD">Offer</label><select id="OfferDD" data-bind="options: SkinEditor.editableContent().offers,
                                               optionsValue: 'ID',
                                               optionsText: 'Name',
                                               value: SkinEditor.editableContent().offerId">
                    </select>

                    <div class="row">
                        <input id="sunday" data-bind="checked: SkinEditor.editableContent().sunday" type="checkbox"/><label for="sunday">Sunday</label>
                        <input id="monday" data-bind="checked: SkinEditor.editableContent().monday" type="checkbox"/><label for="monday">Monday</label>
                        <input id="tuesday" data-bind="checked: SkinEditor.editableContent().tuesday" type="checkbox"/><label for="tuesday">Tuesday</label>
                        <input id="wednesday" data-bind="checked: SkinEditor.editableContent().wednesday" type="checkbox"/><label for="wednesday">Wednesday</label>
                        <input id="thursday" data-bind="checked: SkinEditor.editableContent().thursday" type="checkbox"/><label for="thursday">Thursday</label>
                        <input id="friday" data-bind="checked: SkinEditor.editableContent().friday" type="checkbox"/><label for="friday">Friday</label>
                        <input id="saturday" data-bind="checked: SkinEditor.editableContent().saturday" type="checkbox"/><label for="saturday">Saturday</label>
                    </div>

                    <label for="Text1"></label> <input id="Text1" type="text" data-bind="value: SkinEditor.editableContent().text1">
                    <div class="ui-edit-text-image" id="editText1Image">
                        <div class="row">
                            <div id="Text1ImagePick" class="small-8 columns">
                                <label for="Text1Image">Text 1 Image</label>
                                <input id="Text1Image" type="text" data-bind="value: SkinEditor.editableContent().text1" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 3)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 3)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text1,
                                                attr: { src : SkinEditor.editableContent().text1 }">
                            </div>
                        </div>

                    </div>
                    <select data-bind="options: SkinEditor.returnedCategories,
                                           optionsCaption: 'Select Category',
                                           optionsValue: 'id',
                                           optionsText: 'name',
                                           value: SkinEditor.editableContent().selectText1" 
                                id="editText1DropdownBox"            
                            class="textField">
                    </select>
                    <label for="Text2"></label> <input id="Text2" type="text" data-bind="value: SkinEditor.editableContent().text2">
                    <div class="ui-edit-text-image" id="editText2Image">
                        <div class="row">
                            <div id="Text2ImagePick" class="small-8 columns">
                                <label for="Text2Image">Text 2 Image</label>
                                <input id="Text2Image" type="text" data-bind="value: SkinEditor.editableContent().text2" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 4)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 4)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text2,
                                                attr: { src : SkinEditor.editableContent().text2 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text3"></label> <input id="Text3" type="text" data-bind="value: SkinEditor.editableContent().text3">
                    <div class="ui-edit-text-image" id="editText3Image">
                        <div class="row">
                            <div id="Text3ImagePick" class="small-8 columns">
                                <label for="Text3Image">Text 3 Image</label>
                                <input id="Text3Image" type="text" data-bind="value: SkinEditor.editableContent().text3" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 5)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 5)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text3,
                                                attr: { src : SkinEditor.editableContent().text3 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text4"></label> <input id="Text4" type="text" data-bind="value: SkinEditor.editableContent().text4">
                    <div class="ui-edit-text-image" id="editText4Image">
                        <div class="row">
                            <div id="Text4ImagePick" class="small-8 columns">
                                <label for="Text4Image">Text 4 Image</label>
                                <input id="Text4Image" type="text" data-bind="value: SkinEditor.editableContent().text4" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 6)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 6)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text4,
                                                attr: { src : SkinEditor.editableContent().text4 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text5"></label> <input id="Text5" type="text" data-bind="value: SkinEditor.editableContent().text5">
                    <div class="ui-edit-text-image" id="editText5Image">
                        <div class="row">
                            <div id="Text5ImagePick" class="small-8 columns">
                                <label for="Text5Image">Text 5 Image</label>
                                <input id="Text5Image" type="text" data-bind="value: SkinEditor.editableContent().text5" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 7)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 7)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text5,
                                                attr: { src : SkinEditor.editableContent().text5 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text6"></label> <input id="Text6" type="text" data-bind="value: SkinEditor.editableContent().text6">
                    <div class="ui-edit-text-image" id="editText6Image">
                        <div class="row">
                            <div id="Text6ImagePick" class="small-8 columns">
                                <label for="Text6Image">Text 6 Image</label>
                                <input id="Text6Image" type="text" data-bind="value: SkinEditor.editableContent().text6" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 8)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 8)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text6,
                                                attr: { src : SkinEditor.editableContent().text6 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text7"></label> <input id="Text7" type="text" data-bind="value: SkinEditor.editableContent().text7">
                    <div class="ui-edit-text-image" id="editText7Image">
                        <div class="row">
                            <div id="Text7ImagePick" class="small-8 columns">
                                <label for="Text7Image">Text 7 Image</label>
                                <input id="Text7Image" type="text" data-bind="value: SkinEditor.editableContent().text7" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 9)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 9)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text7,
                                                attr: { src : SkinEditor.editableContent().text7 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text8"></label> <input id="Text8" type="text" data-bind="value: SkinEditor.editableContent().text8">
                    <div class="ui-edit-text-image" id="editText8Image">
                        <div class="row">
                            <div id="Text8ImagePick" class="small-8 columns">
                                <label for="Text8Image">Text 8 Image</label>
                                <input id="Text8Image" type="text" data-bind="value: SkinEditor.editableContent().text8" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 10)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 10)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text8,
                                                attr: { src : SkinEditor.editableContent().text8 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text9"></label> <input id="Text9" type="text" data-bind="value: SkinEditor.editableContent().text9">
                    <div class="ui-edit-text-image" id="editText9Image">
                        <div class="row">
                            <div id="Text9ImagePick" class="small-8 columns">
                                <label for="Text9Image">Text 9 Image</label>
                                <input id="Text9Image" type="text" data-bind="value: SkinEditor.editableContent().text9" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 11)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 11)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text9,
                                                attr: { src : SkinEditor.editableContent().text9 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text10"></label> <input id="Text10" type="text" data-bind="value: SkinEditor.editableContent().text10">
                    <div class="ui-edit-text-image" id="editText10Image">
                        <div class="row">
                            <div id="Text10ImagePick" class="small-8 columns">
                                <label for="Text10Image">Text 10 Image</label>
                                <input id="Text10Image" type="text" data-bind="value: SkinEditor.editableContent().text10" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 12)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 12)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text10,
                                                attr: { src : SkinEditor.editableContent().text10 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text11"></label> <input id="Text11" type="text" data-bind="value: SkinEditor.editableContent().text11">
                    <div class="ui-edit-text-image" id="editText11Image">
                        <div class="row">
                            <div id="Text11ImagePick" class="small-8 columns">
                                <label for="Text11Image">Text 11 Image</label>
                                <input id="Text11Image" type="text" data-bind="value: SkinEditor.editableContent().text11" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 13)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 13)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text11,
                                                attr: { src : SkinEditor.editableContent().text11 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text12"></label> <input id="Text12" type="text" data-bind="value: SkinEditor.editableContent().text12">
                    <div class="ui-edit-text-image" id="editText12Image">
                        <div class="row">
                            <div id="Text12ImagePick" class="small-8 columns">
                                <label for="Text12Image">Text 12 Image</label>
                                <input id="Text12Image" type="text" data-bind="value: SkinEditor.editableContent().text12" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 14)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 14)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text12,
                                                attr: { src : SkinEditor.editableContent().text12 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text13"></label> <input id="Text13" type="text" data-bind="value: SkinEditor.editableContent().text13">
                    <div class="ui-edit-text-image" id="editText13Image">
                        <div class="row">
                            <div id="Text13ImagePick" class="small-8 columns">
                                <label for="Text13Image">Text 13 Image</label>
                                <input id="Text13Image" type="text" data-bind="value: SkinEditor.editableContent().text13" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 15)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 15)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text13,
                                                attr: { src : SkinEditor.editableContent().text13 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text14"></label> <input id="Text14" type="text" data-bind="value: SkinEditor.editableContent().text14">
                    <div class="ui-edit-text-image" id="editText14Image">
                        <div class="row">
                            <div id="Text14ImagePick" class="small-8 columns">
                                <label for="Text14Image">Text 14 Image</label>
                                <input id="Text14Image" type="text" data-bind="value: SkinEditor.editableContent().text14" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 16)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 16)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text14,
                                                attr: { src : SkinEditor.editableContent().text14 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text15"></label> <input id="Text15" type="text" data-bind="value: SkinEditor.editableContent().text15">
                    <div class="ui-edit-text-image" id="editText15Image">
                        <div class="row">
                            <div id="Text15ImagePick" class="small-8 columns">
                                <label for="Text15Image">Text 15 Image</label>
                                <input id="Text15Image" type="text" data-bind="value: SkinEditor.editableContent().text15" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 17)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 17)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text15,
                                                attr: { src : SkinEditor.editableContent().text15 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text16"></label> <input id="Text16" type="text" data-bind="value: SkinEditor.editableContent().text16">
                    <div class="ui-edit-text-image" id="editText16Image">
                        <div class="row">
                            <div id="Text16ImagePick" class="small-8 columns">
                                <label for="Text16Image">Text 16 Image</label>
                                <input id="Text16Image" type="text" data-bind="value: SkinEditor.editableContent().text16" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 18)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 18)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text16,
                                                attr: { src : SkinEditor.editableContent().text16 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text17"></label> <input id="Text17" type="text" data-bind="value: SkinEditor.editableContent().text17">
                    <div class="ui-edit-text-image" id="editText17Image">
                        <div class="row">
                            <div id="Text17ImagePick" class="small-8 columns">
                                <label for="Text17Image">Text 17 Image</label>
                                <input id="Text17Image" type="text" data-bind="value: SkinEditor.editableContent().text17" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 19)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 19)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text17,
                                                attr: { src : SkinEditor.editableContent().text17 }">
                            </div>
                        </div>
                    </div>
                    <label for="Text18"></label> <input id="Text18" type="text" data-bind="value: SkinEditor.editableContent().text18">
                    <div class="ui-edit-text-image" id="editText18Image">
                        <div class="row">
                            <div id="Text18ImagePick" class="small-8 columns">
                                <label for="Text18Image">Text 18 Image</label>
                                <input id="Text18Image" type="text" data-bind="value: SkinEditor.editableContent().text18" disabled>
                                <input data-bind="click: SkinEditor.clearImage.bind($data, 20)" type="button" value="Clear">
                                <input data-bind="click: SkinEditor.openImgPicker.bind($data, 20)" type="button" value="Select Image...">
                            </div>
                            <div class="small-4 columns">
                                <img class="thumbnail"
                                     data-bind="visible: SkinEditor.editableContent().text18,
                                                attr: { src : SkinEditor.editableContent().text18 }">
                            </div>
                        </div>
                    </div>

                    <br/>
                    Last Updated <span data-bind="text: SkinEditor.editableContent().lastUpdate + ' by ' + SkinEditor.editableContent().user"></span>
                </div>
                <div class="content" id="pIntro">
                    <textarea class="tinymce" id="Intro"></textarea>
                </div>
                <div class="content" id="pBody">
                    <textarea class="tinymce" id="Body"></textarea>
                </div>
                <div class="content" id="pTerms">
                    <textarea class="tinymce" id="Terms"></textarea>
                </div>
            </div>

        </div>

        <div class="right">
            <input
                data-bind="click: SkinEditor.saveEditableContent.bind($data, SkinEditor.editableContent(), null),
                                               visible: SkinEditor.editableContent().id > 0"
                type="button" value="Save">
            <input
                data-bind="click: SkinEditor.saveEditableContent.bind($data, SkinEditor.editableContent(), 1),
                                               visible: SkinEditor.editableContent().id === 0"
                type="button" value="Save to Top">
            <input
                data-bind="click: SkinEditor.saveEditableContent.bind($data, SkinEditor.editableContent(), 0),
                                               visible: SkinEditor.editableContent().id === 0"
                type="button" value="Save to Bottom">
            <input
                data-bind="click: SkinEditor.deleteItem.bind($data, SkinEditor.editableContent().id),
                                               visible: SkinEditor.editableContent().id > 0"
                type="button" value="Delete">
            <input data-bind="click: SkinEditor.cancelEditing" type="button" value="Cancel">
        </div>

    </div>

    <!-- /EDIT FORM -->

</div>

<!-- /EDIT CONTENT -->