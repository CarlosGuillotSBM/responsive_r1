<!-- CHECK SKIN IMAGES -->

<div style="display: none" data-bind="visible: SkinEditor.checkingModifications() && SkinEditor.validSession()">

    <h3>Compare LIVE with STAGING</h3>

    <div class="content">

        <table>
            <thead>
            <td width="200">Page</td>
            <td width="200">Name</td>
            <td width="200">Key</td>
            <td width="200">Details URL</td>
            <td width="200">State</td>
            <td width="100">User</td>
            <td width="100"></td>
            </thead>
            <tbody data-bind="foreach: SkinEditor.stagingAndLiveModifications">
            <tr>
                <td data-bind="text: Page"></td>
                <td data-bind="text: Name"></td>
                <td data-bind="text: Key"></td>
                <td data-bind="text: DetailsURL"></td>
                <td data-bind="text: State"></td>
                <td data-bind="text: User"></td>
                <td><input type="button" value="Edit" data-bind="click: $root.SkinEditor.editModification.bind($data)"></td>
            </tr>
            </tbody>
        </table>

    </div>

</div>

<!-- /CHECK SKIN IMAGES -->