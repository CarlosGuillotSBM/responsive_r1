<!-- Privacy Policy -->

<div style="display: none" data-bind="visible: SkinEditor.editingPrivacyPolicy() && SkinEditor.validSession()">

    <h3>Privacy Policy</h3>

    <div class="content">
        <div>
            <div class="row ">
                <div class="small-12 columns">
                    <p class="text-warning" data-bind="visible: SkinEditor.privacyPolicy.editMode() === 'new'">Please Note: You are creating a new version of Privacy Policy</p>
                    <p class="text-warning" data-bind="visible: SkinEditor.privacyPolicy.editMode() === 'edit'">Please Note: You are editing Privacy Policy and this won't increase the version</p>
                </div>
            </div>
            <textarea class="tinymce" id="ui-privacy-policy"></textarea>
        </div>

        <div class="row">
            <div class="small-8 columns left">
                <input id="cbUpdatedFullPP" type="checkbox" data-bind="checked: SkinEditor.privacyPolicy.updatedFullPP" class="always-editable"><label class="always-editable" for="cbUpdatedFullPP">I have already updated the <a href="/terms-and-conditions/" target="_blank">Full T&C's</a></label>
            </div>
            <div class="small-4 columns">
                <!-- <input type="button" value="Save"> -->
                <input type="button" value="Publish" data-bind="click: SkinEditor.publishPP">
            </div>
        </div>

    </div>

</div>

<!-- /Privacy Policy -->