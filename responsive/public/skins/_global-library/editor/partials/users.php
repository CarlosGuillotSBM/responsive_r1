<!-- LIST USERS -->

<?php include "new-user.php" ?>

<div style="display: none" data-bind="visible: SkinEditor.editingUsers()">
    <br/>

    <input class="top-buttons-action" type="button" value="Add User" data-bind="click: SkinEditor.displayNewUserPanel">

    <table>
        <thead>
            <tr>
                <th width="80">ID</th>
                <th width="200">Username</th>
                <th width="80">IsAdmin</th>
                <th width="150">Group</th>
                <th width="100">CanUpload</th>
                <th width="80"></th>
            </tr>
        </thead>
        <tbody data-bind="foreach: SkinEditor.users">
        <tr class="ui-grid-item">
            <td data-bind="text: ID"></td>
            <td><span data-bind="text: Username"></span> <br/></td>
            <td><input type="checkbox" data-bind="checked: IsAdmin, value: IsAdmin" > <br/></td>
            <td>
                <select name="cars" data-bind="value: UserGroup">
                    <option value="1">DaubK</option>
                    <option value="2">8Ball</option>
                </select>
            </td>
            <td><input type="checkbox" data-bind="checked: CanUpload, value: CanUpload" > <br/></td>
            <td><input style="min-width: 60px"
                       type="button"
                       data-bind="click: $parent.SkinEditor.saveEditableUser.bind($data, $data)"
                       value="Save"></td>
        </tr>
        </tbody>
    </table>
    <!-- TO IMPLEMENT LATER <input type="button" value="Save All"> -->
</div>

<!-- /LIST USERS -->