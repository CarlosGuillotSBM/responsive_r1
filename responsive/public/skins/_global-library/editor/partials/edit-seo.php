<!-- EDIT SEO -->

<div style="display: none" data-bind="visible: SkinEditor.editingSEO() && SkinEditor.validSession()">

    <h3>Page SEO</h3>

    <div class="content">
        <input data-bind="click: SkinEditor.saveSEO.bind($data, SkinEditor.seoContent())" type="button" value="Save" class="right">
        <textarea class="seo-content" data-bind="value: SkinEditor.seoContent().seoContent"></textarea>
    </div>

</div>

<!-- / EDIT SEO -->