<div style="display: none" data-bind="visible: SkinEditor.editingTcsAndPP() && SkinEditor.validSession()">

    <dl class="tabs" data-tab style="margin-top: 8px">
        <dd class="active"><a href="#pTCs" id="tabTcs">Terms and Conditions</a></dd>
        <dd><a href="#pPP" id="tabPP">Privacy Policy</a></dd>
    </dl>

    <div class="tabs-content">
        <div class="content active" id="pTCs">
            <div>
                <textarea class="tinymce" id="ui-terms-conditions"></textarea>
            </div>
        </div>
        <div class="content" id="pPP">
            <div>
                <textarea class="tinymce" id="ui-privacy-policy"></textarea>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="small-10 columns left">
            <input id="cbUpdatedFullTCs" type="checkbox" data-bind="checked: SkinEditor.updatedFullTcs" class="always-editable"><label class="always-editable" for="cbUpdatedFullTCs">I have already updated the Full T&C's and/or Privacy Policy</a></label>
        </div>
        <div class="small-2 columns right">
            <input type="button" value="Sync Live" data-bind="click: SkinEditor.publishTcsAndPP">
        </div>
    </div>

</div>