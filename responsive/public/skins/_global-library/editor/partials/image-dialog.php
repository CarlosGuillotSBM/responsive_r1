<!-- IMAGE DIALOG -->

<div style="display: none" data-bind="visible: SkinEditor.imgPickerVisible" id="imgPicker" class="panel">
    <input type="text" class="search" placeholder="Search box" data-bind="value: SkinEditor.searchText, valueUpdate: ['input', 'keypress']"/>
    <a data-bind="click: SkinEditor.toggleSize, text: SkinEditor.imgPickerToggleText" href="" class="right"></a>
    <div class="clearfix"></div>
    <div class="row left">
        <span class="radius secondary label" data-bind="text: 'Path:' + SkinEditor.subFolder()"></span>
    </div>
    <div class="row images-container">
        <!-- FOLDERS -->
        <div data-bind="foreach: SkinEditor.folders" class="small-4 columns">
            <a data-bind="text: display, click: $root.SkinEditor.openImageFolder.bind($data, display, path)" data-nohijack="true" href=""></a> <br/>
        </div>
        <!-- THUMBNAILS -->
        <div class="small-8 columns">
            <div data-bind="foreach: SkinEditor.images">
                <img class="thumbnail"
                     data-bind="attr: { src : imgPath }, css: { 'selected': selected, 'not-selected': !selected }, click: $root.SkinEditor.selectedThumbnail">
            </div>
        </div>
    </div>
    <div class="row" style="display: none" data-bind="visible: SkinEditor.canUpload()">
        <!-- UPLOAD -->
        <div class="small-2 columns">
            <input type="button" data-bind="click: SkinEditor.submitForm" value="Upload"/>
        </div>
        <div class="small-10 columns">
            <form id="ui-uploadForm" action="/admindata.php" method="post" enctype="multipart/form-data">
                <input name="userImage" id="userImage" type="file" class="inputFile always-editable" />
                <input name="f" type="hidden" value="uploadimage"/>
                <input name="path" type="hidden" data-bind="value: SkinEditor.imagePath"/>
            </form>
        </div>
    </div>
    <div class="row">
        <input type="button" data-bind="click: SkinEditor.selectedImage" value="OK" class="right"/>
        <input type="button" data-bind="click: SkinEditor.closeImgPicker" value="Cancel" class="right" />
    </div>
</div>

<!-- /IMAGE DIALOG -->