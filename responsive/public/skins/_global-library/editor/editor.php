<?php

    include 'paths.php';
    include "config/config.php";

    // if not in Edit Mode - EXIT!

    if(config("Edit") != 1)
	{
		header("HTTP/1.0 404 Not Found"); 
		exit;
	}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/_global-library/css/foundation.min.css" />
    <link rel="stylesheet" type="text/css" href="/_global-library/css/editor.css"/>
    <link rel="stylesheet" type="text/css" href="/_global-library/css/jquery-ui/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="/_global-library/css/datatables.min.css"/>

    <script src="/_global-library/js/bower_components/modernizr/modernizr.js"></script>
    <script src="/_global-library/js/app/helpers/add-modules.js"></script>
    <script type="application/javascript">
        var skinModules = [], skinPlugins = [];
        skinModules.push({ id: "SkinEditor" });
        skinModules.push({ id: "Notifier" });
    </script>
    <style type="text/css">
        .text-warning {
            color: #a94442;
            font-size: 14px; 
        }
    </style>
</head>
<body>


<?php if (isLoggedInSkinEditor()): ?>

    <?php include "partials/modals.php" ?>

<?php endif; ?>


<?php include "partials/navigation.php" ?>


<?php if (isLoggedInSkinEditor()): ?>
    <!-- CONTAINER -->
    <div class="container">

        <?php include "partials/users.php" ?>

        <?php include "partials/edit-content.php" ?>

        <?php include "partials/edit-seo.php" ?>

        <?php include "partials/sub-pages.php" ?>

        <?php include "partials/check-images.php" ?>

        <?php include "partials/tcs-pp-editor.php" ?>

        <?php include "partials/diff.php" ?>

    </div>
    <!-- /CONTAINER -->

    <?php include "partials/image-dialog.php" ?>
<?php endif; ?>

    <?php include "partials/login.php" ?>

    <!--<p>DEBUG</p>-->
    <!--<pre data-bind="text: ko.toJSON($data.SkinEditor.editableContent, null, 2)"></pre>-->


    <?php
        include "_global-library/partials/common/app-js.php";
    ?>

    <script src="/_global-library/js/bower_components/tinymce/tinymce.min.js"></script>
    <script src="/_global-library/js/bower_components/tinymce/jquery.tinymce.min.js"></script>
</body>
</html>