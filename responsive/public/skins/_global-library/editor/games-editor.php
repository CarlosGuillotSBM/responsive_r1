<?php

    include 'paths.php';
    include "config/config.php";

    // if not in Edit Mode - EXIT!

    if(config("Edit") != 1)
	{
		header("HTTP/1.0 404 Not Found"); 
		exit;
	}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/_global-library/css/foundation.min.css" />
    <link rel="stylesheet" type="text/css" href="/_global-library/css/games-editor.css"/>
    <link rel="stylesheet" type="text/css" href="/_global-library/css/jquery-ui/jquery-ui.min.css"/>
    <script src="/_global-library/js/bower_components/modernizr/modernizr.js"></script>
    <script src="/_global-library/js/app/helpers/add-modules.js"></script>
    <script type="application/javascript">
        var skinModules = [], skinPlugins = [];
        skinModules.push({ id: "GamesEditor" });
        skinModules.push({ id: "Notifier" });
    </script>
</head>
<body>

<!-- loding modal -->

<div data-bind="visible: Notifier.loading"
     style="display: block; position: fixed; top: 50%; left: 50%;
     width: 200px; height: 200px; margin-top: -100px; margin-left: -100px;
     z-index: 999999;background-color: #FFFFFF">
    <img src="/_global-library/images/loading.gif">
</div>

<!-- /loding modal -->

<!-- notification -->

<div data-bind="visible: Notifier.notification() && Notifier.system() === 'notification',
                css: { 'success': Notifier.severity() ===  'success',
                       'alert': Notifier.severity() ===  'error',
                       'info': Notifier.severity() ===  'info',
                       'warning': Notifier.severity() ===  'warning' }"
     style="display: none; position: fixed; top: 0px; z-index: 999999; width: 100%;"
     class="alert-box radius">
    <span data-bind="text: Notifier.notification"></span>
    <a data-bind="click: Notifier.clearNotification" href="#" class="close">&times;</a>
</div>

<!-- /notification -->

<!-- NAVIGATION -->

<nav class="top-bar" data-topbar>
    <section class="top-bar-section">

        <?php if (isLoggedInSkinEditor()): ?>
        <!--  Left Nav Section -->
        <ul class="left">
            <li class="has-dropdown">

                <a href="/_global-library/editor/games-editor.php">GAMES EDITOR</a>
                <ul class="dropdown">
                    <li><a href="/_global-library/editor/editor.php">CONTENT EDITOR</a></li>
                </ul>
            </li>
        </ul>
        <?php endif; ?>

        <span data-bind="text: GamesEditor.skinName" class="skin-colour"></span>


        <?php if (isLoggedInSkinEditor()): ?>
        <!-- Right Nav Section -->
        <ul class="right">

            <?php if (isDaubKGroupSkinEditor()): ?>
            <li class="active"
                data-bind="css: { active: GamesEditor.addingGames , inactive: !GamesEditor.addingGames() }">
                <a data-bind="click: GamesEditor.showSection.bind($data, 'add-games')" href="#">Add Games</a>
            </li>
            <?php endif; ?>

            <li data-bind="css: { active: GamesEditor.orderingGames, inactive: !GamesEditor.orderingGames() }">
                <a data-bind="click: GamesEditor.showSection.bind($data, 'order-games')" href="#">Order Games</a>
            </li>
            <li class="has-dropdown">
                <a href="#">More...</a>
                <ul class="dropdown">
                    <li>
                        <a data-bind="click: GamesEditor.flushCache" href="#">Flush Cache</a>
                    </li>
                </ul>
            </li>
        </ul>
        <?php endif; ?>

    </section>
</nav>
<!-- /NAVIGATION -->

<!-- LOGIN DIALOG -->

<div data-bind="visible: GamesEditor.loginVisible()" class="login-panel" style="display: none">
    <input data-bind="value: GamesEditor.username,
                          valueUpdate: ['input', 'keypress'],
                          event:{keypress: GamesEditor.checkForEnterKey}" type="text" placeholder="Username">
    <input data-bind="value: GamesEditor.password,
                          valueUpdate: ['input', 'keypress'],
                          event:{keypress: GamesEditor.checkForEnterKey}" type="password" placeholder="password">
    <input data-bind="click: GamesEditor.doLogin" type="button" value="Login">
</div>

<!-- /LOGIN DIALOG -->

<?php if (isLoggedInSkinEditor()): ?>
<!-- CONTAINER -->
<div class="container" data-bind="visible: GamesEditor.validSession" style="display: none">


    <?php if (isDaubKGroupSkinEditor()): ?>
    <!-- Adding Games Container -->
    <div data-bind="visible: GamesEditor.addingGames" class="adding-games">

        <!-- Add game -->
        <div class="row">
            <div class="small-2 columns">
                <label>Add New Game</label>
            </div>
            <div class="small-5 columns">
                <input data-bind="value: GamesEditor.searchGameId,
                                  valueUpdate: ['input', 'keypress']"
                       type="text" placeholder="Game ID">
            </div>

            <div class="small-5 columns">
                <input class="ui-game-search" type="text" placeholder="Title">
            </div>
        </div>

        <!-- /Add game -->

        <br/><br/>
        <div class="row">
            <div data-bind="visible: GamesEditor.gameToBeAdded().title" style="display: none">
                <span data-bind="text: 'You are adding ' + GamesEditor.gameToBeAdded().title" class="success label"> </span>
            </div>
            <div data-bind="visible: GamesEditor.gameToBeAdded().noData" style="display: none">
                <span data-bind="text: GamesEditor.gameToBeAdded().noData" class="error label"> </span>
            </div>
            <br/><br/>
        </div>

        <div class="row">

            <input data-bind="click: GamesEditor.addGame, visible: GamesEditor.categories().length" type="button" value="Update Game">
            <br/>
            <br/>
            <table id="ui-add-game-to-skins" data-bind="visible: GamesEditor.categories().length">
                <thead>
                <tr data-bind="foreach: GamesEditor.gameCategorySkins()">
                    <th data-bind="text: name, attr: { 'data-skinid' : id }" width="120"></th>
                </tr>
                <tr data-bind="foreach: GamesEditor.devicesRow()">
                    <td data-bind="text: label" width="120"></td>
                </tr>
                </thead>
                <tbody data-bind="foreach: GamesEditor.categories">
                    <tr data-bind="css: 'row' + depth,
                                   visible: depth === 0,
                                   attr: { 'data-depth': depth }">
                        <td data-bind="text: parent ? '+ ' + name : name, click: $root.GamesEditor.expandParentRow, attr: { 'data-catid' : ID }, css: { 'parent-row': parent }"></td>

                    </tr>
                </tbody>
            </table>
            <input data-bind="click: GamesEditor.addGame, visible: GamesEditor.categories().length" type="button" value="Update Game">
        </div>

    </div>
    <!-- /Adding Games Container -->
    <?php endif; ?>

    <!-- Ordering Games -->
    <div data-bind="visible: GamesEditor.orderingGames" class="ordering-games" style="display: none">

        <!-- Common operations -->
        <input data-bind="click: GamesEditor.syncALL" type="button" value="Sync ALL to Live">
        <input data-bind="click: GamesEditor.revertALL" type="button" value="Revert ALL from Live">
        <br/>
        <br/>

        <!-- Filters -->
        <div class="row">
            <div class="small-4 columns">
                <select data-bind="value: GamesEditor.selectedSkin">
                    <option>Select Skin</option>
                    <?php if (isDaubKGroupSkinEditor()): ?>
                    <option value="1">Spin And Win</option>
                    <option value="2">Kitty Bingo</option>
                    <option value="3">Lucky Pants</option>
                    <option value="4">Direct Slot</option>
                    <option value="5">Magical Vegas</option>
                    <option value="6">Bingo Extra</option>
                    <option value="11">Kingjack Casino</option>
                    <option value="12">Aspers Casino</option>
                    <?php endif; ?>
                    <option value="8">Lucky VIP</option>
                    <option value="9">Give Back Bingo</option>
                    <option value="10">Regal Wins</option>
                </select>
            </div>
            <div class="small-3 columns">
                <select data-bind="options: GamesEditor.returnedCategories,
                                   optionsCaption: 'Select Category',
                                   optionsValue: 'id',
                                   optionsText: 'name',
                                   value: GamesEditor.selectedCategory"
                    class="textField">
                </select>

            </div>
            <div class="small-3 columns">
                <select data-bind="value: GamesEditor.selectedDevice">
                    <option value="">Select Device</option>
                    <option value="1">Web</option>
                    <option value="2">Mobile</option>
                </select>
            </div>
            <div class="small-2 columns">
                <input data-bind="click: GamesEditor.getGamesForCategory" type="button" value="Get Games">
            </div>
        </div>
        <!-- /Filters -->

        <!-- Game list -->
        <div class="row" data-bind="visible: GamesEditor.gamesToOrder().length">
            <input data-bind="click: GamesEditor.updateGamesOrder" type="button" value="Update Order">
            <input data-bind="click: GamesEditor.removeSelected" type="button" value="Remove Selected">
            <input data-bind="click: GamesEditor.syncToLive" type="button" value="Sync to Live">
            <input data-bind="click: GamesEditor.revertFromLive" type="button" value="Revert from Live">
        </div>
        <div class="row">
            <div class="small-12 columns">
                <ul data-bind="foreach: GamesEditor.gamesToOrder" id="ui-games-to-order" class="games-list inline-list" >
                    <li data-bind="attr: { 'data-gameid' : GameID }, click: $root.GamesEditor.selectedGameThumbnail">
                        <img class="thumbnail" data-bind="attr: { src: Image, 'data-gameid' : GameID }"><br/>
                        <span data-bind="Title"></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row" data-bind="visible: GamesEditor.gamesToOrder().length">
            <input data-bind="click: GamesEditor.updateGamesOrder" type="button" value="Update Order">
            <input data-bind="click: GamesEditor.removeSelected" type="button" value="Remove Selected">
            <input data-bind="click: GamesEditor.syncToLive" type="button" value="Sync to Live">
            <input data-bind="click: GamesEditor.revertFromLive" type="button" value="Revert from Live">
        </div>
        <!-- /Game list -->

    </div>
    <!-- /Ordering Games -->
</div>
<!-- /CONTAINER -->
<?php endif; ?>

<!--<p>DEBUG</p>-->
<!--<pre data-bind="text: ko.toJSON($data.GamesEditor, null, 2)"></pre>-->

<?php
include "_global-library/partials/common/app-js.php";
?>

<script src="/_global-library/js/bower_components/tinymce/tinymce.min.js"></script>
<script src="/_global-library/js/bower_components/tinymce/jquery.tinymce.min.js"></script>

</body>
</html>