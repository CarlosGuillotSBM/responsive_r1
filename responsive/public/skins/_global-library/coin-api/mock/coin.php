<?php

require "Config.php";
require "MockData.php";
require "MockWorldpay.php";
require "Helpers.php";

// Some initial configuration

Config::$activeAccounts = "1"; // 0 or 1
Config::$balance = "0.00"; // player's balance
Config::$withdrawalsPending = "0.00"; // if !=0 will show the reversal screen
Config::$CardExpired = "1"; // if 1 will show option to change expiry date
Config::$tokenUrl = "http://$_SERVER[HTTP_HOST]/_global-library/coin-api/mock/coin.php"; // worldpay token url
Config::$server = "DEV"; // DEV or STAGING
Config::$Device = "Web"; // Phone Tablet Web
Config::$Source = "Cas"; // Cas PRD BCO BFS

// Store SkinID for redirecting again to the website

if (isset($_GET["SkinID"])) {
    setCookie("SkinID", $_GET["SkinID"], null, '/');
}

// Coin requests

if (isset($_GET["Function"])) {

    $mock = new MockData($_GET["Function"]);
    $mockData = $mock->getData();

    Helpers::outputResponse($mockData);

}

// Worldpay requests

if(isset($_POST["Action"])) {

    $mock = new MockWorldpay($_POST["Action"]);
    $mock->RedirectToCashier();

}