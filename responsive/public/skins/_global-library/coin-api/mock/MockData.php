<?php

class MockData {

    private $function;

    function __construct($function) {
        $this->function = $function;
    }

    public function getData () {

        if ($this->function == "Start") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "PlayerID" => "11244",
                "Function" => "Start",
                "ssid" => "064A7717-51A0-40D2-AFD9-30DAE5C7701A",
                "CustomerID" => "350",
                "Firstname" => "Nuno",
                "Lastname" => "Oliveira",
                "Verified" => "1",
                "LiveChatURL" => "",
                "MaxAccounts" => "5",
                "ActiveAccounts" => Config::$activeAccounts,
                "Balance" => Config::$balance,
                "WithdrawalsPending" => Config::$withdrawalsPending,
                "ForceReversalOfPendingWithdrawal" => "1",
                "SelfExclude" => "",
                "SecsSinceReg" => "106",
                "Deposited" => "0",
                "ShowWireDetails" => "1"
            );

        } elseif ($this->function == "GetAccountTypes") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "Function" => "GetAccountTypes",
                "AccountTypes" => array(
                    array(
                        "AccountTypeCode" => "A_CC",
                        "Description" => "Debit and Credit Cards",
                        "CVN" => "1",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Amounts" => array(
                            array(
                                "Amount" => "5"
                            ),
                            array(
                                "Amount" => "10"
                            ),
                            array(
                                "Amount" => "20"
                            ),
                            array(
                                "Amount" => "30"
                            ),
                            array(
                                "Amount" => "50"
                            ),
                            array(
                                "Amount" => "75"
                            ),
                            array(
                                "Amount" => "100"
                            ),
                            array(
                                "Amount" => "150"
                            ),
                            array(
                                "Amount" => "200"
                            )
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "2817",
                                "Amount" => "5",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "30",
                                "MaxBonus" => "",
                                "Total" => "40",
                                "Description" => "10 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2817",
                                "Amount" => "10",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "30",
                                "MaxBonus" => "",
                                "Total" => "40",
                                "Description" => "10 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2818",
                                "Amount" => "20",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "60",
                                "MaxBonus" => "",
                                "Total" => "80",
                                "Description" => "20 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2819",
                                "Amount" => "30",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "90",
                                "MaxBonus" => "",
                                "Total" => "120",
                                "Description" => "30 Free Spins!"
                            )
                        ),
                    )
                ,
                    array(
                        "AccountTypeCode" => "A_NETELLER",
                        "Description" => "Neteller",
                        "CVN" => "0",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "2817",
                                "Amount" => "10",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "30",
                                "MaxBonus" => "",
                                "Total" => "40",
                                "Description" => "10 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2818",
                                "Amount" => "20",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "60",
                                "MaxBonus" => "",
                                "Total" => "80",
                                "Description" => "20 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2819",
                                "Amount" => "30",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "90",
                                "MaxBonus" => "",
                                "Total" => "120",
                                "Description" => "30 Free Spins!"
                            )
                        )
                    ),
                    array(
                        "AccountTypeCode" => "A_PAYPAL",
                        "Description" => "PayPal",
                        "CVN" => "0",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "2817",
                                "Amount" => "10",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "30",
                                "MaxBonus" => "",
                                "Total" => "40",
                                "Description" => "10 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2818",
                                "Amount" => "20",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "60",
                                "MaxBonus" => "",
                                "Total" => "80",
                                "Description" => "20 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2819",
                                "Amount" => "30",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "90",
                                "MaxBonus" => "",
                                "Total" => "120",
                                "Description" => "30 Free Spins!"
                            )
                        )
                    ),
                    array(
                        "AccountTypeCode" => "A_PAYSAFECARD",
                        "Description" => "PaySafeCard",
                        "CVN" => "0",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "2817",
                                "Amount" => "10",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "30",
                                "MaxBonus" => "",
                                "Total" => "40",
                                "Description" => "10 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2818",
                                "Amount" => "20",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "60",
                                "MaxBonus" => "",
                                "Total" => "80",
                                "Description" => "20 Free Spins!"
                            ),
                            array(
                                "PromoID" => "2819",
                                "Amount" => "30",
                                "Percent" => "300%",
                                "CashDrop" => "0",
                                "Bonus" => "90",
                                "MaxBonus" => "",
                                "Total" => "120",
                                "Description" => "30 Free Spins!"
                            )
                        )
                    )
                )
            );

        } elseif ($this->function == "GetCustomerAccounts") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "Function" => "GetCustomerAccounts",
                "Accounts" => array(
                    array(
                        "AccountID" => "2107",
                        "AccountTypeCode" => "A_VISA_CREDIT",
                        "Description" => "VISA Credit",
                        "AccountNumber" => "*6625",
                        "ExpiryDate" => "01/2015",
                        "ExpireStatus" => Config::$CardExpired,
                        "CVN" => "1",
                        "LimitReached" => "0",
                        "DefaultAccount" => "1",
                        "DefaultAmtIndex" => "1",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Amounts" => array(
                            array(
                                "Amount" => "5"
                            ),
                            array(
                                "Amount" => "10"
                            ),
                            array(
                                "Amount" => "20"
                            ),
                            array(
                                "Amount" => "30"
                            ),
                            array(
                                "Amount" => "50"
                            ),
                            array(
                                "Amount" => "75"
                            ),
                            array(
                                "Amount" => "100"
                            ),
                            array(
                                "Amount" => "150"
                            ),
                            array(
                                "Amount" => "200"
                            )
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "0",
                                "Amount" => "5",
                                "Percent" => "",
                                "CashDrop" => "",
                                "Bonus" => "",
                                "MaxBonus" => "",
                                "Total" => "5",
                                "Description" => "No bonus available"
                            ),
                            array(
                                "PromoID" => "2825",
                                "Amount" => "10",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "2.50",
                                "MaxBonus" => "",
                                "Total" => "12.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2826",
                                "Amount" => "20",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "5",
                                "MaxBonus" => "",
                                "Total" => "25",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2827",
                                "Amount" => "30",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "15",
                                "MaxBonus" => "",
                                "Total" => "45",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2828",
                                "Amount" => "50",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "25",
                                "MaxBonus" => "",
                                "Total" => "75",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2860",
                                "Amount" => "75",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "37.50",
                                "MaxBonus" => "",
                                "Total" => "112.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2829",
                                "Amount" => "100",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "50",
                                "MaxBonus" => "",
                                "Total" => "150",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2861",
                                "Amount" => "150",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "75",
                                "MaxBonus" => "",
                                "Total" => "225",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2862",
                                "Amount" => "200",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "100",
                                "MaxBonus" => "",
                                "Total" => "300",
                                "Description" => "Re-Deposit Bonus!"
                            )
                        )
                    ),
                    array(
                        "AccountID" => "2108",
                        "AccountTypeCode" => "A_PAYSAFECARD",
                        "Description" => "PaySafeCard",
                        "AccountNumber" => "",
                        "ExpiryDate" => "",
                        "ExpireStatus" => Config::$CardExpired,
                        "CVN" => "0",
                        "LimitReached" => "0",
                        "DefaultAccount" => "0",
                        "DefaultAmtIndex" => "2",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "0",
                                "Amount" => "5",
                                "Percent" => "",
                                "CashDrop" => "",
                                "Bonus" => "",
                                "MaxBonus" => "",
                                "Total" => "5",
                                "Description" => "No bonus available"
                            ),
                            array(
                                "PromoID" => "2825",
                                "Amount" => "10",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "2.50",
                                "MaxBonus" => "",
                                "Total" => "12.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2826",
                                "Amount" => "20",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "5",
                                "MaxBonus" => "",
                                "Total" => "25",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2827",
                                "Amount" => "30",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "15",
                                "MaxBonus" => "",
                                "Total" => "45",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2828",
                                "Amount" => "50",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "25",
                                "MaxBonus" => "",
                                "Total" => "75",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2860",
                                "Amount" => "75",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "37.50",
                                "MaxBonus" => "",
                                "Total" => "112.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2829",
                                "Amount" => "100",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "50",
                                "MaxBonus" => "",
                                "Total" => "150",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2861",
                                "Amount" => "150",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "75",
                                "MaxBonus" => "",
                                "Total" => "225",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2862",
                                "Amount" => "200",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "100",
                                "MaxBonus" => "",
                                "Total" => "300",
                                "Description" => "Re-Deposit Bonus!"
                            )
                        )
                    ),
                    array(
                        "AccountID" => "2109",
                        "AccountTypeCode" => "A_PAYPAL",
                        "Description" => "PayPal",
                        "AccountNumber" => "",
                        "ExpiryDate" => "",
                        "ExpireStatus" => Config::$CardExpired,
                        "CVN" => "0",
                        "LimitReached" => "0",
                        "DefaultAccount" => "0",
                        "DefaultAmtIndex" => "1",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "0",
                                "Amount" => "5",
                                "Percent" => "",
                                "CashDrop" => "",
                                "Bonus" => "",
                                "MaxBonus" => "",
                                "Total" => "5",
                                "Description" => "No bonus available"
                            ),
                            array(
                                "PromoID" => "2825",
                                "Amount" => "10",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "2.50",
                                "MaxBonus" => "",
                                "Total" => "12.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2826",
                                "Amount" => "20",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "5",
                                "MaxBonus" => "",
                                "Total" => "25",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2827",
                                "Amount" => "30",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "15",
                                "MaxBonus" => "",
                                "Total" => "45",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2828",
                                "Amount" => "50",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "25",
                                "MaxBonus" => "",
                                "Total" => "75",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2860",
                                "Amount" => "75",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "37.50",
                                "MaxBonus" => "",
                                "Total" => "112.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2829",
                                "Amount" => "100",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "50",
                                "MaxBonus" => "",
                                "Total" => "150",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2861",
                                "Amount" => "150",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "75",
                                "MaxBonus" => "",
                                "Total" => "225",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2862",
                                "Amount" => "200",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "100",
                                "MaxBonus" => "",
                                "Total" => "300",
                                "Description" => "Re-Deposit Bonus!"
                            )
                        )
                    ),
                    array(
                        "AccountID" => "2108",
                        "AccountTypeCode" => "A_NETELLER",
                        "Description" => "Neteller",
                        "AccountNumber" => "*9998",
                        "ExpiryDate" => "",
                        "ExpireStatus" => Config::$CardExpired,
                        "CVN" => "0",
                        "LimitReached" => "0",
                        "DefaultAccount" => "0",
                        "DefaultAmtIndex" => "1",
                        "Promos" => array(
                            array(
                                "PromoID" => "2829",
                                "DepositMin" => "10",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2858",
                                "DepositMin" => "20",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                            array(
                                "PromoID" => "2897",
                                "DepositMin" => "50",
                                "Description" => "Get 100% welcome bonus + 20 free spins <br> Minimum deposit £10 <br> Maximum bonus amount £100 <br> Wagering 25 x deposit & bonus amount"
                            ),
                        ),
                        "Deposit" => array(
                            array(
                                "PromoID" => "0",
                                "Amount" => "5",
                                "Percent" => "",
                                "CashDrop" => "",
                                "Bonus" => "",
                                "MaxBonus" => "",
                                "Total" => "5",
                                "Description" => "No bonus available"
                            ),
                            array(
                                "PromoID" => "2825",
                                "Amount" => "10",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "2.50",
                                "MaxBonus" => "",
                                "Total" => "12.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2826",
                                "Amount" => "20",
                                "Percent" => "25%",
                                "CashDrop" => "0",
                                "Bonus" => "5",
                                "MaxBonus" => "",
                                "Total" => "25",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2827",
                                "Amount" => "30",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "15",
                                "MaxBonus" => "",
                                "Total" => "45",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2828",
                                "Amount" => "50",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "25",
                                "MaxBonus" => "",
                                "Total" => "75",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2860",
                                "Amount" => "75",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "37.50",
                                "MaxBonus" => "",
                                "Total" => "112.50",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2829",
                                "Amount" => "100",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "50",
                                "MaxBonus" => "",
                                "Total" => "150",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2861",
                                "Amount" => "150",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "75",
                                "MaxBonus" => "",
                                "Total" => "225",
                                "Description" => "Re-Deposit Bonus!"
                            ),
                            array(
                                "PromoID" => "2862",
                                "Amount" => "200",
                                "Percent" => "50%",
                                "CashDrop" => "0",
                                "Bonus" => "100",
                                "MaxBonus" => "",
                                "Total" => "300",
                                "Description" => "Re-Deposit Bonus!"
                            )
                        )
                    )
                )
            );

        } elseif ($this->function == "NewTokenAdd") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "Function" => "NewTokenAdd",
                "CardHolder" => "Nuno Oliveira",
                "TxAccountTokenID" => "687",
                "Token1" => "7428177D3064433AAA80A56CCAAC2504A2E190626E294DECBBEECA33585360E8",
                "URL1" => Config::$tokenUrl,
                "Token2" => "",
                "URL2" => ""
            );
        } elseif ($this->function == "NewTokenCVN") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "Function" => "NewTokenCVN"
            );

        } elseif ($this->function == "NewTokenUpdate") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "Function" => "NewTokenUpdate",
                "CardHolder" => "",
                "TxAccountTokenID" => "693",
                "Token1" => "E5764A3A20374F3E861ADF731D0A8520962D441BBBB44F3CB235CA0FD85A8AB3",
                "URL1" => Config::$tokenUrl,
                "Token2" => "",
                "URL2" => ""
            );

        } elseif ($this->function == "Deposit") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Your deposit was successful.",
                "Function" => "Deposit",
                "CoinRef" => "4599",
                "Amount" => "5.00",
                "Bonus" => "0.00",
                "FreeSpins" => "0",
                "Balance" => "10.00",
                "FirstDeposit" => "0",
                "SubscriptionID" => "0",
                "SubscriptionExcessAmount" => "0.00"
            );

        } elseif ($this->function == "GetWithdrawDetail") {

            return array(
                "Status" => "1",
                "StatusMsg" => "",
                "Function" => "GetWithdrawDetail",
                "RealBalance" => "10.00",
                "AvailableToWithdraw" => "10.00",
                "BonusToBeForfeited" => "0.00",
                "MinWithdraw" => "10.00",
                "MinWithdrawFee" => "5.00",
                "WithdrawalsPending" => "0.00",
                "WithdrawHoursPending" => "12"
            );

        } elseif ($this->function == "Withdraw") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Your withdrawal was successful.",
                "Function" => "Withdraw",
                "TxWithdrawID" => "725",
                "Amount" => "10.00",
                "Balance" => "0.00",
                "BonusForfeited" => "0.00",
                "WithdrawalsPending" => "10.00",
                "WithdrawHoursPending" => "12",
                "MinWithdraw" => "10.00",
                "MinWithdrawFee" => "5.00"
            );

        } elseif ($this->function == "GetReverseDetail") {

            return array(
                "Status" => "-1",
                "StatusMsg" => "",
                "Function" => "GetReverseDetail",
                "AvailableToReverse" => "10.00",
                "MinReverse" => "0.01",
                "MinWithdraw" => "10.00",
                "MinWithdrawFee" => "5.00"
            );

        } elseif ($this->function == "Reverse") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Your withdrawal has been successfully reversed.",
                "Function" => "Reverse",
                "TxReverseBatchID" => "640",
                "Amount" => "10.00",
                "Balance" => "10.00",
                "WithdrawalsPending" => "0.00",
                "MinWithdraw" => "10.00",
                "MinWithdrawFee" => "5.00"
            );

        } elseif ($this->function == "GetWireDetails") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "Function" => "GetWireDetails",
                "BankName" => "LLOYDS bank",
                "AccountName" => "Nuno Oliveira",
                "AccountNumber" => "5151677 27272 8788",
                "BranchCode" => "West 111",
                "SwiftCode" => "LLOWQ11",
                "Address" => "Street 112 Lloyds",
                "City" => "London",
                "State" => "Kent",
                "Country" => "UNITED KINGDOM ",
                "PostCode" => "NW51TL",
                "CorrespondingBankName" => "",
                "WireStatus" => "0"
            );

        } elseif ($this->function == "SetWireDetails") {

            return array(
                "Status" => "1",
                "StatusMsg" => "Success",
                "Function" => "SetWireDetails"
            );

        } else
            return array(
               "Status" => "-3",
                "StatusMsg" => "Unexpected error",
                "Function" => "Unknown"
            );
        }

}