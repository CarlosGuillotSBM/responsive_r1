<?php

class MockWorldpay {

    private $action;

    function __construct($action) {
        $this->action = $action;
    }

    public function RedirectToCashier () {

        // configuration

        $returnedFromWorldpay = true;
        $device = Config::$Device;
        $source = Config::$Source;

        // set response with query params

        if ($this->action == "Add") {

            $WebsiteReturn = "Status=1&StatusMsg=Your%20deposit%20was%20successful.&Function=Deposit&PlayerID=11244" .
                             "&ssid=91889FFE-52E0-43A6-B944-726C329D6FBF&CustomerID=350&Source=$source&Device=$device" .
                             "&CoinRef=4596&Amount=5.00&Bonus=0.00&FreeSpins=0&FirstDeposit=1&Balance=5.00" .
                             "&SubscriptionID=0&SubscriptionExcessAmount=0.00";

        } else {

            $WebsiteReturn = "Status=1&StatusMsg=Your%20expiry%20date%20has%20been%20successfully%20modified." .
                             "&Function=NewTokenUpdate&PlayerID=11244&ssid=B82B07C9-15BC-436A-AFB1-86BC314F737E" .
                             "&CustomerID=350&Source=$source&Device=$device";

        }

        if (Config::$server == "DEV") {

            switch ($_COOKIE["SkinID"]) {
                case 1:
                case 999:
                    if (Config::$Device == "Phone" || Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://spinandwin.dagacubedev.net/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://spinandwin.dagacubedev.net/mobile/banking?$WebsiteReturn");
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://spinandwin.dagacubedev.net/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://spinandwin.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
                case 2:
                    if (Config::$Device == "Phone") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/banking?$WebsiteReturn");
                    } else if (Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/banking?$WebsiteReturn");
                        if (Config::$Source == "BCO") {
                            if($returnedFromWorldpay)
                                header("Location: https://kittybingo2.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                            else
                                header("Location: https://kittybingo2.dagacubedev.net/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                        }
                        if (Config::$Source == "BFS") {
                            if($returnedFromWorldpay) {
                                header("Location: https://kittybingo2.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                            } else {
                                header("Location: https://kittybingo2.dagacubedev.net/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                            }
                        }
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.dagacubedev.net/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
                case 3:
                    if (Config::$Device == "Phone") {
                        if (Config::$Source == "PRD")
                            header("Location: https://luckypantsbingo2.dagacubedev.net/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://luckypantsbingo2.dagacubedev.net/mobile/banking?$WebsiteReturn");
                    } else if (Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://luckypantsbingo2.dagacubedev.net/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://luckypantsbingo2.dagacubedev.net/mobile/banking?$WebsiteReturn");
                        if (Config::$Source == "BCO") {
//                            if($returnedFromWorldpay)
//                                header("Location: https://luckypantsbingo2.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
//                            else
                                header("Location: https://luckypantsbingo2.dagacubedev.net/html5/bingo/pants/tablet?hh=1&$WebsiteReturn");
                        }
                        if (Config::$Source == "BFS")
                            header("Location: https://luckypantsbingo2.dagacubedev.net/html5/bingo/pants/tablet?hh=1&$WebsiteReturn");
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://luckypantsbingo.dagacubedev.net/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://luckypantsbingo.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
                case 4:
                    if (Config::$Source == "PRD")
                        header("Location: https://directslot.dagacubedev.net/welcome?$WebsiteReturn");
                    if (Config::$Source == "Cas")
                        header("Location: https://directslot.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                    break;
                case 5:
                    if (Config::$Source == "PRD")
                        header("Location: http://responsive.magicalvegas.dev/welcome/?$WebsiteReturn");
                    if (Config::$Source == "Cas")
                        header("Location: http://responsive.magicalvegas.dev/cashier/?$WebsiteReturn");
                    break;
                default:
                    if (Config::$Device == "Phone") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/banking?$WebsiteReturn");
                    } else if (Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo2.dagacubedev.net/mobile/banking?$WebsiteReturn");
                        if (Config::$Source == "BCO") {
                            if($returnedFromWorldpay)
                                header("Location: https://kittybingo2.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                            else
                                header("Location: https://kittybingo2.dagacubedev.net/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                        }
                        if (Config::$Source == "BFS")
                            header("Location: https://kittybingo2.dagacubedev.net/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.dagacubedev.net/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.dagacubedev.net/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
            }

        } else {

            switch ($_COOKIE["SkinID"]) {
                case 1:
                case 999:
                    if (Config::$Device == "Phone" || Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://spinandwin.com:8433/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://spinandwin.com:8433/mobile/banking?$WebsiteReturn");
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://spinandwin.com:8433/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://spinandwin.com:8433/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
                case 2:
                    if (Config::$Device == "Phone") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.com:8433/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.com:8433/mobile/banking?$WebsiteReturn");
                    } else if (Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.com:8433/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.com:8433/mobile/banking?$WebsiteReturn");
                        if (Config::$Source == "BCO") {
                            if($returnedFromWorldpay)
                                header("Location: https://kittybingo.com:8433/cashier?hh=1&$WebsiteReturn");
                            else
                                header("Location: https://kittybingo.com:8433/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                        }
                        if (Config::$Source == "BFS") {
                            if($returnedFromWorldpay) {
                                header("Location: https://kittybingo.com:8433/cashier?hh=1&$WebsiteReturn");
                            } else {
                                header("Location: https://kittybingo.com:8433/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                            }
                        }
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.com:8433/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.com:8433/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
                case 3:
                    if (Config::$Device == "Phone") {
                        if (Config::$Source == "PRD")
                            header("Location: https://luckypantsbingo.com:8433/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://luckypantsbingo.com:8433/mobile/banking?$WebsiteReturn");
                    } else if (Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://luckypantsbingo.com:8433/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://luckypantsbingo.com:8433/mobile/banking?$WebsiteReturn");
                        if (Config::$Source == "BCO") {
                            if($returnedFromWorldpay)
                                header("Location: https://luckypantsbingo.com:8433/cashier?hh=1&$WebsiteReturn");
                            else
                                header("Location: https://luckypantsbingo.com:8433/html5/bingo/pants/tablet?hh=1&$WebsiteReturn");
                        }
                        if (Config::$Source == "BFS")
                            header("Location: https://luckypantsbingo.com:8433/html5/bingo/pants/tablet?hh=1&$WebsiteReturn");
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://luckypantsbingo.com:8433/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://luckypantsbingo.com:8433/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
                case 4:
                    if (Config::$Source == "PRD")
                        header("Location: https://directslot.gr:8433/welcome?$WebsiteReturn");
                    if (Config::$Source == "Cas")
                        header("Location: https://directslot.gr:8433/cashier?hh=1&$WebsiteReturn");
                    break;
                default:
                    if (Config::$Device == "Phone") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.com:8433/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.com:8433/mobile/banking?$WebsiteReturn");
                    } else if (Config::$Device == "Tablet") {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.com:8433/mobile/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.com:8433/mobile/banking?$WebsiteReturn");
                        if (Config::$Source == "BCO") {
                            if($returnedFromWorldpay)
                                header("Location: https://kittybingo.com:8433/cashier?hh=1&$WebsiteReturn");
                            else
                                header("Location: https://kittybingo.com:8433/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                        }
                        if (Config::$Source == "BFS")
                            header("Location: https://kittybingo.com:8433/html5/bingo/kitty/tablet?hh=1&$WebsiteReturn");
                    } else {
                        if (Config::$Source == "PRD")
                            header("Location: https://kittybingo.com:8433/welcome?$WebsiteReturn");
                        if (Config::$Source == "Cas")
                            header("Location: https://kittybingo.com:8433/cashier?hh=1&$WebsiteReturn");
                    }
                    break;
            }

        }
    }

}