<h1><?php echo $row['Title']; ?></h1>


<div class="youtube-video-banner">
	<div class="youtube-video-banner__video-inner-wrap">
		<img class="youtube-video-banner__video-inner-wrap__img" src="/_images/youtube-widget/tv-background.png">
		<div class="youtube-video-banner__video-inner-wrap__video">
			<a href="#" data-reveal-id="videoModal"><img src="<?php echo $row['Text2']?>" width="100%"></a>
		</div>
	</div>
<div id="videoModal" class="reveal-modal large" data-reveal="" style="display: none; ">
<h2><?php echo $row['Title']; ?></h2>
<div class="flex-video widescreen vimeo" style="display: block;">
<iframe width="230" height="172" src="https://www.youtube.com/embed/<?php echo $row['Text1']?>?&showinfo=0&controls=0&rel=0" frameborder="0" allowfullscreen></iframe>
</div>
<a class="close-reveal-modal">&#215;</a>
</div>