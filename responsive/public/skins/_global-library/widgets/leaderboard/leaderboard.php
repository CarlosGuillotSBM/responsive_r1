<style type="text/css">
  
.rwd-table {
  /*margin: 1em 0 !important;*/
  min-width: 300px !important;
  max-width: 500px !important;
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
}

.tr-header {
  background: #ea6153 !important;
}

table tr.even, table tr.alt, table tr:nth-of-type(even) {
  background: #F9ECEC;
}
.rwd-table tr {
  /*border-top: 1px solid #ddd !important; */
  /*border-bottom: 1px solid #ddd !important;*/
  /*background: #f6f6f6 !important;*/
}

.rwd-table tr:nth-of-type(odd) {
  background: #F5D3D3;
}

.rwd-table th {
  display: none !important;
  border: none !important;
}
.rwd-table td {
  display: block !important;
  color: #3b3b3b !important;
  border: none !important;
  height: 30px !important;   
}

.rwd-table td:first-child {
  padding-top: .5em !important;
}
.rwd-table td:last-child {
  padding-bottom: .5em !important;
}
.rwd-table td:before {
  content: attr(data-th) ": ";
  font-weight: bold !important; 
  width: 6.5em !important;
  display: inline-block !important;
}
@media (min-width: 480px) {
  .rwd-table td:before {
    display: none !important;
  }
}
.rwd-table th, .rwd-table td {
  text-align: center !important;
}
@media (min-width: 480px) {
  .rwd-table th, .rwd-table td {
    display: table-cell !important;
    padding: .25em .5em !important;
  }
  .rwd-table th:first-child, .rwd-table td:first-child {
    padding-left: 0 !important;
  }
  .rwd-table th:last-child, .rwd-table td:last-child {
    padding-right: 0 !important;
  }
}

.rwd-table {
  /*background: #34495E !important;*/
  color: #3b3b3b !important;
  border-radius: .4em !important;
  overflow: hidden !important;
}
.rwd-table tr {
  border-color: #46637f !important;
}
.rwd-table th, .rwd-table td {
  margin: .3em .3em !important;
}
@media (min-width: 480px) {
  .rwd-table th, .rwd-table td {
    padding: .3em !important;

  }
}
.rwd-table th, .rwd-table td:before {
  color: #fff !important;
  font-size: 1.3em;
}
@media (max-width: 480px) {
  .rwd-table td:before {
    color: #881B1B !important;
  }
}

</style>

<?php
  /*
  //we can't use this leaderboard since playngo doesn't maintain it anymore
   $count = 0; 
   $record = simplexml_load_file('https://wexa.dagacube.net/playngo-leaderboard.php');   
   echo '<table class="rwd-table">
  <tr class="tr-header">
    <th>Rank</th>
    <th>Nickname</th>
    <th>Score</th>
  </tr>';
   foreach ($record as $records):
       $rank=$records->Rank;
       $playerid=$records->PlayerID;
       $nickname=$records->Nickname;
       $score=$records->Score;
       if ($count==30) break;
       $count++;
       echo '
   <tr>
    <td data-th="Rank">',$rank,'</td>
    <td data-th="Nickname">',$nickname,'</td>
    <td data-th="Score">',$score,'</td>
  </tr>';
   endforeach;
   echo '</table>';
   */
?>

