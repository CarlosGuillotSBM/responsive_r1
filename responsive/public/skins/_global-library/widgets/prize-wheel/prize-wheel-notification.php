<?php
$playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;
$sessionID =  (isset($_COOKIE["SessionID"])) ? $_COOKIE["SessionID"] : '';
?>
<div class="prize-wheel__notification" data-bind="visible: PrizeWheel.displayPrizeWheel()" style="display: none;">
    <div class="prize-wheel__notification__counter" data-bind="text: PrizeWheel.spinNumber"></div>
    <a class="prize-wheel__notification__icon hide-for-large-up" style="width: 100%" href="<?=config("PrizeWheelURL")."?SkinID=".config("SkinID")."&uid=".$playerID."&guid=".$sessionID."&gameid=531&HomeURL=".config("URL")?>" target="prizeWheel">
        <div class="prize-wheel__icon"  style="width: 100%"></div>
    </a>
    <a class="prize-wheel__notification__icon show-for-large-up" data-bind="click: GameLauncher.openGame" data-gameid="gam-prize-wheel" target="prizeWheel">
        <div class="prize-wheel__icon"></div>
    </a>
</div>