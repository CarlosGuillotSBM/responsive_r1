<?php
/* If this element hasn't been rendered generate the layout */
if(!in_array(@$row['Text16'],$adaptive_banner) && config("SkinID")!="8" && config("SkinID")!="10"):
    $adaptive_banner[]=@$row['Text16'];
    $adaptive_banner_name=@$row['Text16'];
    include "_partials/banner/homepage-adaptive-hero-banner.php";
endif;
if(config("SkinID") == "8" || config("SkinID") == "10"){
    include "_partials/banner/homepage-adaptive-hero-banner.php";
}

$content = array(
    'line-1' =>  $row['Text1'],
    'line-2' =>  $row['Text2'],
    'line-3' =>  $row['Text3'],
    'cta-text' =>  $row['Text4'],
    'cta-url' =>  $row['Text5'],
    't&c-text' =>  $row['Text6'],
    't&c-url' =>  $row['Text7'],
    'extra-copy' =>  $row['Text8'],
    'image-desktop' =>  $row['Text9'],
    'image-desktop-retina' =>  $row['Text10'],
    'image-tablet' =>  $row['Text11'],
    'image-tablet-retina' =>  $row['Text12'],
    'image-mobile' =>  $row['Text13'],
    'image-mobile-retina' =>  $row['Text14'],
    'restriction' =>  $row['Text15'],
    'element-name' =>  $row['Text16'],
);
?>
<span data-adaptive-element='<?=@$content['element-name']?>' 
data-adaptive-line1='<?=(@$content['line-1']!=''? $content['line-1'] : '')?>' data-adaptive-line2='<?=(@$content['line-2']!=''? $content['line-2'] : '')?>' data-adaptive-line3='<?=(@$content['line-3']!=''? $content['line-3'] : '')?>'
data-adaptive-image-desktop='<?=@$content['image-desktop']?>' data-adaptive-image-desktop-retina='<?=@$content['image-desktop-retina']?>'
 data-adaptive-image-tablet='<?=@$content['image-tablet']?>' data-adaptive-image-tablet-retina='<?=@$content['image-tablet-retina']?>'
 data-adaptive-image-mobile='<?=@$content['image-mobile']?>' data-adaptive-image-mobile-retina='<?=@$content['image-mobile-retina']?>'
data-adaptive-restriction='<?=@$content['restriction']?>' 
data-adaptive-cta-link='<?=(@$content['cta-url']!=''? $content['cta-url'] : '')?>' data-adaptive-cta-text='<?=(@$content['cta-text']!=''? $content['cta-text'] : '')?>' 
data-adaptive-tc='<?=(@$content['t&c-text']!=''? $content['t&c-text'] : '')?>' data-adaptive-tc-link='<?=(@$content['t&c-url']!=''? $content['t&c-url'] : '/')?>'
data-adaptive-copy='<?=@$content['extra-copy']?>'></span>