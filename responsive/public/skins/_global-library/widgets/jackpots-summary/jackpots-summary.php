<article class="jackpots-summary">
  <h1>Bingo Jackpots</h1>
  <!-- jackpot -->
  <aside class="jackpots-summary__jackpot">
    <hgroup>
      <h1 class="jackpots-summary__jackpot__title">Kitty Cash</h1>
      <h2 class="jackpots-summary__jackpot__amount">
        <span data-bind="jackpot: 60050">&nbsp;</span>
        <div class="clearfix"></div>
      </h2>
    </hgroup>
  </aside>
  <!-- jackpot -->
  <aside class="jackpots-summary__jackpot">
    <hgroup>
      <h1 class="jackpots-summary__jackpot__title">Cookie Dash</h1>
      <h2 class="jackpots-summary__jackpot__amount">
        <span data-bind="jackpot: 40050">&nbsp;</span>
        <div class="clearfix"></div>
      </h2>
    </hgroup>
  </aside>
  <!-- jackpot -->
  <aside class="jackpots-summary__jackpot">
    <hgroup>
      <h1 class="jackpots-summary__jackpot__title">Splish Splash</h1>
      <h2 class="jackpots-summary__jackpot__amount">
        <span data-bind="jackpot: 30050">&nbsp;</span>
        <div class="clearfix"></div>
      </h2>
    </hgroup>
  </aside>
</article>