<!DOCTYPE html>
<html>
<head>
	<title>FB feed</title>
	<style type="text/css">
		html, body, div, span, applet, object, iframe,
		h1, h2, h3, h4, h5, h6, p, blockquote, pre,
		a, abbr, acronym, address, big, cite, code,
		del, dfn, em, img, ins, kbd, q, s, samp,
		small, strike, strong, sub, sup, tt, var,
		b, u, i, center,
		dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td,
		article, aside, canvas, details, embed,
		figure, figcaption, footer, header, hgroup,
		menu, nav, output, ruby, section, summary,
		time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			font: inherit;
			vertical-align: baseline;
		}
	</style>
</head>
<body>

<div id="fb-root"></div>
<script type="text/javascript" src="//connect.facebook.net/en_GB/sdk.js"></script>
<script type="text/javascript">

	(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

</script>

<?php
	$id = $_REQUEST["id"];
?>

	<div class="fb-like-box" data-href="https://www.facebook.com/<?php echo $id; ?>"
	 data-width="230" data-height="390" data-colorscheme="light" data-show-faces="false" data-header="false"
	 data-stream="true" data-show-border="false">
	</div>

</body>
</html>