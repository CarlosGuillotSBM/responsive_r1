<div class="breadcrumbs__row">
	<ul class="breadcrumbs" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<?php foreach($this->breadcrumbs as $key => $value){ ?>
		<li>
			<a href="<?php echo $value;?>" data-hijack="true" itemprop="url">
				<span itemprop="title"><?php echo $key;?></span>
			</a>
		</li>
		<?php }?>
	</ul>
	<!-- Share button only for desktop -->
	<?php echo '<!-- device'.config("Device").'-->'; if (config("Device") == 1) { ?>
	<div class="share-button right"></div>
	<?php } ?>
	<!-- /Share button only for desktop -->
</div>

<div class="clearfix"></div>
<!-- end BREADCRUMPS -->