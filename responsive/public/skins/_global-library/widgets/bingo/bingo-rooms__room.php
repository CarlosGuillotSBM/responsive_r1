    <!-- slot -->
    <article class="bingo-room small-4 large-2 columns">
      <h1> <a href="/bingo/bingo-room"> <?php echo $this->row["Description"];?> </a> </h1>
      <div class="small-12 bingo-thumb">
        <img src="/_global-library/images/bingo/<?php echo $this->row["GameIcon"];?>">
        <div class="small-12 bingo-room__time"></div>
      </div>
      <div class="play-now-button small-12">
        <span class="tiny button"><?php echo $this->row["ID"];?></span>
      </div>
    </article>
	
	