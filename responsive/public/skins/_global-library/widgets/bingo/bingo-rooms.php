  <!-- SLOTS FULL PANEL AREA -->
  <section class="small-12 bingo-rooms">
    <header>
      <h1 class="bingo-rooms__title">Bingo Rooms</h1>
    
      <!-- display mode -->
      <div class="large-2 right columns bingo-rooms__display-buttons">
        <div class="small-6 columns">
          <img src="../_images/icons/display-view__grid.png">
        </div>
        <div class="small-6 columns">
          <img src="../_images/icons/display-view__list.png">
        </div>
      </div>
      <!-- end display mode -->
      
      <div class="clearfix"></div>
    </header>

    <!-- slots grid -->
    <div class="small-12 small-block-grid-5 bingo-rooms__grid"> 
  
  
  
      <?php $this->repeatData($this->bingo,'_global-library/bingo/bingo-rooms__room.php'); ?>
      
      <div class="clearfix"></div>
    
    </div>
    <!-- end slots grid -->
  </section>
    <!-- END SLOTS FULL PANEL AREA -->