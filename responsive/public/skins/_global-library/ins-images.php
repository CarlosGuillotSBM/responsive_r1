<?php

/*======================================
=            Top cache part            =
======================================*/
// $skinid = htmlspecialchars($_GET["skin"]) ;
// $url = $_SERVER["SCRIPT_NAME"];
// $break = Explode('/', $url);
// $file = $break[count($break) - 1];
// $cachefile = $skinid.'-cached-'.substr_replace($file ,"",-4).'.html';
// $cachetime = 18000;

// // Serve from the cache if it is younger than $cachetime
// if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)) {
//     echo "<!-- Cached copy, generated ".date('H:i', filemtime($cachefile))." -->\n";
//     include($cachefile);
//     exit;
// }
// ob_start(); // Start the output buffer

/*-----  End top cache  ------*/

# To prevent browser error output
header('Content-Type: text/javascript; charset=UTF-8');
# Path to image folder
$imageFolder = htmlspecialchars($_GET["path"]) ;
# Show only these file types from the image folder
$imageTypes = '{*.jpg,*.JPG,*.jpeg,*.JPEG,*.png,*.PNG,*.gif,*.GIF}';
# Set to true if you prefer sorting images by name
# If set to false, images will be sorted by date
$sortByImageName = false;
# Set to false if you want the oldest images to appear first
# This is only used if images are sorted by date (see above)
$newestImagesFirst = true;
# The rest of the code is technical
# Add images to array
$images = glob($imageFolder . $imageTypes, GLOB_BRACE);
# Sort images
if ($sortByImageName) {
    $sortedImages = $images;
    natsort($sortedImages);
} else {
    # Sort the images based on its 'last modified' time stamp
    $sortedImages = array();
    $count = count($images);
    for ($i = 0; $i < $count; $i++) {
        $sortedImages[date('YmdHis', filemtime($images[$i])) . $i] = $images[$i];
    }
    # Sort images in array
    if ($newestImagesFirst) {
        krsort($sortedImages);
    } else {
        ksort($sortedImages);
    }
}
# Generate the HTML output
writeHtml('<ul class="list">');
foreach ($sortedImages as $image) {
    # Get the name of the image, stripped from image folder path and file type extension
    $name = substr($image, strlen($imageFolder));
    list($width, $height) = getimagesize($image);
    $actual_link = "http://$_SERVER[HTTP_HOST]/";
    # Get the 'last modified' time stamp, make it human readable
    $lastModified = '(last modified: ' . date('F d Y H:i:s', filemtime($image)) . ')';
    # Begin adding
    $modlink = preg_replace("/..\/.+?\//i", "", $image, 1);
    writeHtml('<li class="ins-imgs-li">');
    writeHtml('<div class="ins-imgs-img"><a name="' . $image . '" href="#' . $image . '">');
    writeHtml('<img src="test.png" data-src="' . $actual_link . $modlink . '" alt="' . $name . '" title="' . $name . '">');
    writeHtml('</a></div>');
    //writeHtml('<div class="ins-imgs-label">' . $name . ' ' . $lastModified . $width .' x '. $height .'</div>');
    writeHtml('<div class="ins-imgs-label">' . $name . ' <div class="ins-imgs-size">' . $width .' x '. $height .'</div></div>');
    writeHtml('</li>');
}
writeHtml('</ul>');
//writeHtml('<link rel="stylesheet" type="text/css" href="ins-imgs.css">');
# Convert HTML to JS
function writeHtml($html) {
    echo "document.write('" . $html . "');\n";
}

/*======================================
=            Bottom cache part            =
======================================*/

// $cached = fopen($cachefile, 'w');
// fwrite($cached, ob_get_contents());
// fclose($cached);
// ob_end_flush(); // Send the output to the browser

/*-----  End bottom cache  ------*/

?>