<?php
$cmaFriendly = @$this->row['CMAFriendly'] ? $this->row['CMAFriendly'] : 0;
$gameProvider = @$this->row['GameProviderID'] ? $this->row['GameProviderID'] : 0;
?>
<!--
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$$ SEO MICRO DATA FOR THIS PARTIAL
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
Name: itemprop="name"
Image: itemprop="image" 
URL: itemprop="url"
Screenshot: itemprop="screenshot"
-->
<script type="application/javascript">

    var sbm = sbm || {};
    sbm.games = sbm.games || [];
    sbm.games.push({
        "id"    : <?php echo json_encode(@$this->row['DetailsURL']);?>,
        "icon"  : "<?php echo @$this->row['Image'];?>",
        "bg"    : <?php echo json_encode(@$this->row['Text1']);?>,
        "title" : <?php echo json_encode(@$this->row['Title']);?>,
        "type"  : "game",
        "desc"  : <?php echo json_encode(@$this->row['Intro']);?>,
        "thumb" : "<?php echo @$this->row['GameScreen'];?>",
		"cma": <?php echo json_encode($cmaFriendly);?>,
		"provider": <?php echo json_encode($gameProvider);?>,
        "detUrl": <?php echo json_encode(@$this->row['Path']);?>
    });

</script>

<div class="game-details-page" itemscope itemtype="http://schema.org/SoftwareApplication">
    <h1 class="game-detail-wrap__title-bar" itemprop="name"><?php echo @$this->row["Title"];?></h1>
    <div class="game-detail-content">
       
        <div class="game-detail-content__left">
            <div class="overlay-holder">
                <span <?php if(@$this->row["Status"]==1) echo 'data-bind="click: GameLauncher.openGame"';?> data-infopage="true" data-gameid="<?php echo @$this->row["DetailsURL"];?>" data-gametitle="<?php echo @$this->row["Title"] ?>" data-icon="<?php echo @$this->row["Image"];?>" data-nohijack="true" class="games-info__try">
                    <div style="display: none; cursor: pointer" data-bind="visible: validSession() && <?php echo @$this->row["Status"];?>" class="overlay">
                        <img alt="Play Now" src="/_images/common/play-icons/play-now.png" class="play-now-button">
                    </div>
                    <img alt="<?php echo @$this->row["Title"] . " " . $this->controller;?>" itemprop="screenshot" src="<?php echo @$this->row["GameScreen"];?>" />
                </span>
            </div>

            <div class="game-detail-content__aquisition__cta" data-bind="visible: !validSession()" style="display:none;">
                <div class="cta-login" data-bind="click: GameLauncher.openGame" data-gameid="<?php echo @$this->row["DetailsURL"];?>" data-nohijack="true"><a>LOGIN</a></div>
                <div class="cta-join" onclick="location.href=/register/" ><a href="/register/">JOIN NOW</a></div>   
            </div>
            
        </div>
        
        <div class="game-icon-wrap">
            <img alt="<?php echo @$this->row["Title"];?>" itemprop="image" src="<?php echo @$this->row["Image"];?>" />
            
        </div>
        <div class="game-detail-content__right">
            <aside class="game-detail__wrap">
                
                <h3>Game Details</h3>
                <ul class="game-detail-content__right__game-table">
                    <li><span class='game-detail-content__right__game-table__title'>Publisher</span><span itemprop="publisher" class="game-detail-content__right__game-table__provider"><?php echo @$this->row["ProviderName"];?></span></li>
                    <li><span class='game-detail-content__right__game-table__title'>Paylines</span><?php echo @$this->row["PayLines"];?></li>
                    <li><span class='game-detail-content__right__game-table__title'>Feature</span><?php echo @$this->row["Feature"];?></li>
                    <li><span class='game-detail-content__right__game-table__title'>Minimum Bet</span><?php echo @$this->row["MinBet"];?></li>
                    <li><span class='game-detail-content__right__game-table__title'>Progressive</span><?php echo @$this->row["Progressive"];?></li>
					
                    <li><span class='game-detail-content__right__game-table__title'>RTP</span><?php echo @$this->row["RTP"];?></li>
				
                </ul>
            </aside>
        </div>
    </div>
</div>