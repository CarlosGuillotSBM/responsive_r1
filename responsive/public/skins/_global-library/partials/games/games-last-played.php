<script type="application/javascript">
    skinModules.push({
        id: "LastPlayed",
        options: {
            global: true
        }
    });
</script>



<script id="ui-lastplayed-tmpl" type="text/tmpl">
    <div data-bind="click: $root.LastPlayed.launch.bind(id)" class="last-played-game" style="float: left; margin: 3px;">
        <span style="display: none" class="last-played-title" data-bind="text: title"></span>
        <a><img data-bind="attr: {src: icon }"></a>
    </div>
</script>

<!-- Last played container   -->
<div style="display: none" data-bind="visible: LastPlayed.lastGames().length && !LastPlayed.minimized()">
    <div  class="bottom-last-played show-for-large-up">
        <div class="last-played-container-title">Last Played</div>
        <div class="last-played" id="ui-last-played-container" style="display: none;" data-bind="foreach: LastPlayed.lastGames, visible: LastPlayed.lastGames().length">
            <!-- content from ui-lastplayed-tmpl will go here-->
        </div>
    </div>
</div>

<a data-bind="text: LastPlayed.toggleText,
              visible: LastPlayed.lastGames().length && sbm.serverconfig.device === 1,
              click: LastPlayed.toggle" class="last-played-toggle" style="display: none"></a>

<!-- /Last played container -->