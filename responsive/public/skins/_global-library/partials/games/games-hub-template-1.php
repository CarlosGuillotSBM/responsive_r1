<section>

	<?php edit($this->controller,'games-hub'); ?>
	<div class="hub__grid hub__grid__6x3x3">

		<div class="hub__grid__column">						
			<?php $this->getPartial($this->content['games-hub'],0); ?>
		</div>

		<div class="hub__grid__column">
			<!-- promo 1 -->
			<div class="hub__grid__column__child">
				<?php $this->getPartial($this->content['games-hub'],1); ?>
			</div>
			<!-- end promo 1 -->
			<!-- promo 2 -->
			<div class="hub__grid__column__child">
				<?php $this->getPartial($this->content['games-hub'],2); ?>
			</div>
			<!-- end promo 2 -->
		</div>
		<div class="hub__grid__column">
			<?php $this->getPartial($this->content['games-hub'],3); ?>
		</div>
		<div class="slot-promo__border"></div>
		<div class="clearfix"></div>

	</div>


</section>