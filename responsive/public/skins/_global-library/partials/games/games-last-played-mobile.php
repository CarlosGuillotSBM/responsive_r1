<script type="application/javascript">
    skinModules.push({
        id: "LastPlayed",
        options: {
            global: true
        }
    });
</script>

<script id="ui-lastplayed-mobile-tmpl" type="text/tmpl">

    <li class="search-result" data-bind="click: $root.GamesMenuSearch.launchGame">
        <a>
            <div class="search-result__thumb">
                <img class="search-result__thumb__img" itemprop="game-icon"
                     data-bind="attr: { src: icon }">
            </div>
            <div class="search-result__name">
                <div class="search-result__name__inner"
                     data-bind="text: title"></div>
            </div>
            <div class="search-result__cta">
                <?php include "_partials/common/play-icon-svg.php"; ?>
            </div>
        </a>
    </li>

</script>

<ul id="ui-last-played-mobile-container" class="search-results ripple" data-bind="foreach: LastPlayed.lastGames">

</ul>