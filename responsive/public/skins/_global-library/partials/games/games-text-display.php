<table class="nonresponsive ui-text-only-table">
    <thead>
    <td class="sortable"><a data-bind="click: GamesDisplay.orderBy.bind(null, 0, 'STRING')">Name</a></td>
    <td class="sortable hide-for-small-only"><a data-bind="click: GamesDisplay.orderBy.bind(null, 1, 'INT')">Paylines</a></td>
            <?php 
            if(config("SkinID")!=9):?>
    <td></td>
            <?php endif;?>
    <td></td>
    </thead>
    <tbody>
    <?php $i = 1; foreach($this->games as $row){ ?>
        <tr>
            <td><?php echo $row["Title"];?></td>
            <td class="hide-for-small-only" width="100px"><?php echo $row["PayLines"];?></td>
            <?php if(config("SkinID")!=9):?>
            <td width="100px"><a class="cta-more-info" data-hijack="true" href="<?php echo $row["Path"];?>/">Info</a></td>
            <?php endif;?>
            <td width="100px">
                <a class="cta-play"

                    data-bind="click: GameLauncher.openGame"
                   data-gameid="<?php echo $row["DetailsURL"];?>"
                   data-gametitle="<?php echo $row["Title"];?>"
                   data-gamedescription="<?php echo $row["Intro"];?>"
                   data-details-url="<?php echo $row["Path"];?>"
                   data-icon="<?php echo $row["Image1"];?>"
                   data-thumbnail="<?php echo $row["Image"];?>"
                   data-cma-friendly="<?php echo $row["CMAFriendly"];?>"
                   data-demoplay="<?php echo $row["DemoPlay"];?>">Play</a>
            </td>
        </tr>
        <?php $i++; }?>
    </tbody>
</table>