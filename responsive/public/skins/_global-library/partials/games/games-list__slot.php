<div class="games-list__slot">

	<div class="games-list__slot__wrapper">
		<div class="games-list__slot__image">
			<img src="<?php echo $row["Image"];?>">
			<div class="clearfix"></div>
		</div>
 
		<div class="games-list__favourite">
			<div class="games-list__favourite__content">
				<div class="games-list__favourite__content__description">
					<a data-nohijack="true" href="<?php echo $this->getGameHref().$row["DetailsURL"];?>">
					<span> <?php echo $row["Title"];?> </span>
					</a>
				</div>
				<div class="games-list__favourite__content__img">
					<img data-bind="favourite: <?php echo $row["ID"];?>, click: Favourites.toggleFavourite" alt="<?php echo $row["Title"];?>" data-favgameid="<?php echo $row["ID"];?>" data-gamedescription="<?php echo $row["Intro"];?>">
				</div>
			</div>
		</div>
		<div class="games-list__slot-overlay" data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row["ID"];?>"
    data-gametitle="<?php echo $row["Title"];?>"
    data-gamedescription="<?php echo $row["Intro"];?>"
    data-icon="<?php echo $row["Image"];?>">

			<div class="games-list__slot-overlay__info">
				<img src="/_images/common/play-icon-48x48.png">
			</div>

		</div>

	</div>

</div>

