
<div class="game-detail-modal">

     <div class="game-detail-wrap__title-bar">
          <h1 data-bind="text: GameLauncher.gameTitle" class="ui-game-title"></h1>
     </div>
     <div class="game-detail-content">
          <div class="game-detail-content__left">
               <span data-bind="attr: { 'data-gameid': GameLauncher.gameId, 'data-demoplay': GameLauncher.demoPlay }" data-nohijack="true" class="games-info__try">
                    <img data-bind="attr: { src: GameLauncher.icon }">
               </span>


          </div>
          <div class="game-detail-content__right">
               <article  class="game-detail__description">
                    <h3>Game Description</h3>
                    <p data-bind="html: GameLauncher.gameDescription"></p>
                    <a data-bind="attr: { href: GameLauncher.detailsUrl() + '/' }">Read More</a>
               </article>

               <?php include'_partials/login/login-box.php' ?>
          </div>


     </div>

</div>
