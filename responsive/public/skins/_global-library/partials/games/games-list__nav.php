<!-- SLOTS FULL PANEL AREA -->
<nav class="games-list__nav">
  <h1 class="games-list__nav__title"><?php echo $this->panelTitle;?></h1>
  <!-- filters -->
  <div class="games-list__nav__filter">
    <a href="#" data-dropdown="drop1" class="games-list__nav__filter__button">Filter</a> 
    <ul id="drop1" class="f-dropdown" data-dropdown-content>
     <?php $this->repeatData($this->gameCategories,'_partials/common/menu-item.php'); ?>
   </ul>
 </div>
 <!-- end filters -->


 <!-- search -->
 <div class="games-list__nav__search">
  <div class="games-list__nav__search-wrapper">
    <div class="games-list__nav__search-wrapper__left">
      <input placeholder="Search slot" type="text">
    </div>
    <div class="games-list__nav__search-wrapper__right">
      <span class="postfix">find</span>
    </div>
  </div>        
</div>
<!-- end search -->

<!-- display mode -->
<div class="games-list__nav__display-mode">
  <div><a>G</a></div>
  <div><a>L</a></div>
</div>
<!-- end display mode -->

<div class="clearfix"></div>

</nav>
    <!-- END SLOTS FULL PANEL AREA -->