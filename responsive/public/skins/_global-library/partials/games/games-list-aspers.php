<script type="application/javascript">
	skinModules.push({
		id: "GamesDisplay",
		options: {
			defaultMode: "icons"
		}
	});
</script>

<?php
do {
	if(count((array)$this->games) > 0) {
?>

<?php if (config("RealDevice") == 1 || config("SkinID") == 1 || config("SkinID") == 5 || config("SkinID") == 8  || config("SkinID") == 12 || $this->action !== "slots" ) { ?>
	<section class="<?php echo @$this->category["Class"];?> games-list middle-content__box">
		<div class="section__header hideme">
	            <h1 class="section__title">
					<?php echo $this->getCategoryHref();?><span class="section__header__viewall"><?php echo $this->getCategoryHref('View All');?></span>
				</h1>

				<div class="float-right">
					<div class="cat-search-bar">
						<div class="cat-search-bar__categories ui-game-filter-btn games-filter-menu"><?php include "_global-library/partials/game-filter-menu/game-filter-menu-aspers.php";?>
						</div>
					</div>
				</div>
				
	        </div>

		<!-- ICON VIEW -->

		<div data-bind="visible: GamesDisplay.displayMode() === 'icons'" class="games-list__grid">
	        <?php $i = 1; foreach($this->games as $row){ ?>
				<!-- item -->
				<div class="games-list__grid__child ui-parent-game-icon" data-position="<?php echo $i ?>" data-containerkey="<?php echo $this->category['Key']; ?>">
					<?php include '_global-library/_editor-partials/game-icon.php'; ?>
				</div>
				<!-- /item -->
			<?php $i++; }?>
		</div>

		<!-- TEXT VIEW-->
		<div style="display: none" data-bind="visible: GamesDisplay.displayMode() === 'text'" class="games-list__grid">
			<?php include '_global-library/partials/games/games-text-display.php' ?>
		</div>

	</section>
 <?php } else {

	$row['Title'] = $this->category['Description'];
	$row['Url'] = "/" . $this->category['Category'] . "/" . $this->category['Key'];
	$row['Games'] = $this->games;

	include '_global-library/_editor-partials/category-carousel.php';
 }

}} while ($this->GetNextCategory());?>
