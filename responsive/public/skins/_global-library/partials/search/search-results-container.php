<script type="text/template" id="ui-desktop-search-results-tmpl">

	<li class="search-result" data-bind="click: $root.GamesMenuSearch.launchGame, attr:{'data-cma-friendly': cma}" >
		<a>
			<div class="search-result__thumb">
				<img data-bind="attr: { src: icon }"
					 class="search-result__thumb__img" itemprop="game-icon">
			</div>
			<div class="search-result__name">
				<div data-bind="text: title"
					 class="search-result__name__inner"></div>
			</div>
		</a>
	</li>

</script>

<ul style="display: none" class="desktop-search-results-container ui-desktop-search-results-container"
	data-bind="visible: GamesMenuSearch.gamesFound().length, foreach: GamesMenuSearch.gamesFound">



</ul>