<script type="application/javascript">

	skinModules.push({
		id: "GamesMenuSearch",
		options: {
			global: true
		}
	});

</script>

<div class="search-input">
	<input type="text" placeholder="Search Games" class="autocomplete ui-search-input"
		   data-bind="value: GamesMenuSearch.searchTerm, valueUpdate:'input'">
	<i class="search-input__icon"></i>

	<?php include "_global-library/partials/search/search-results-container.php"; ?>

</div>