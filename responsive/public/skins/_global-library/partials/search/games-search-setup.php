<script type="text/tmpl" id="ui-desktop-search-item">
    <li class='search-result'>
        <a>
            <div class='search-result__thumb'>
                <img class='search-result__thumb__img' itemprop='game-icon' src='<%= icon %>'>
            </div>
            <div class='search-result__name'>
                <div class='search-result__name__inner'><%= label %></div>
            </div>
        </a>
    </li>
</script>

<style type="text/css">
    .ui-autocomplete {
        overflow: hidden;
    }
    .search-result__thumb__img {
        width: 50px;
        margin: 0px;
        padding: 0px;
        float: left;
    }
    .search-result {
        font-size: .8em;
    }
    .ui-menu-item {
        height: 44px;
    }
</style>
