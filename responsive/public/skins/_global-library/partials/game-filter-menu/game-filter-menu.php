<script type="application/javascript">
skinPlugins.push({
	id: "magicalMenu",
	options: {
		global: true
	}
});
</script>
<div  class="games-filter-menu ui-game-filter-btn games-filter-menu">
	<!-- game menu button -->
	<div class="game-filter-btn__wrapper ">
		<a class="game-filter-btn ui-game-filter-btn btn btn--dropdown">
			Slots by type
			<span class="btn--dropdown__icon">&blacktriangledown;</span>
		</a>
	</div>

	<!-- game menu container -->
	<div class="games-filter-menu__popover-wrap">

		<div class="games-filter-menu__popover game-filter-menu ui-game-filter-menu" align="center">
			<span class="nub"></span>
				<?php include "_global-library/partials/game-filter-menu/game-filter-categories.php" ?>
			</div>
		</div>
	</div>
	<!-- on click of #ui-game-filter-btn needs .games-filter-menu needs class toggling "selected " -->