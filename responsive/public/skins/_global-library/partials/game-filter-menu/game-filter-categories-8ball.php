<!-- Used by LVIP and RW -->
<dl class="tabs ui-filter-tabs" data-tab>
	<dd id="ui-features" class="active">
		<a href="#panel2">Features</a>
	</dd>
	<dd id="ui-paylines">
		<a href="#panel3">Paylines</a>
	</dd>
	<dd id="ui-themes">
		<a href="#panel1">Themes</a>
	</dd>

	<?php if (config("SkinID") != 2 && config("SkinID") != 3 && config("SkinID") != 6 && config("SkinID") != 9 ) : ?>
	<dd id="ui-providers">
		<a href="#panel4">Publisher</a>
	</dd>
	<?php endif; ?>
</dl>

<div class="tabs-content">
	<div class="content" id="panel1">
		<ul>
			<li>
				<a class="menu-link" id="ui-magic-myth" data-gtag="Game Menu,Themes+Mystical and Magic" href="/slots/mystical-slots/" data-hijack="true">
					Mystical and Magic
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-animals-nature" data-gtag="Game Menu,Themes+Animals Lovers" href="/slots/animal-lovers/" data-hijack="true">
					Animal Lovers
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-movie-adventure" data-gtag="Game Menu,Themes+Action Packed Slots" href="/slots/action-packed-slots/" data-hijack="true">
					Action Packed Slots
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-egyptian" data-gtag="Game Menu,Themes+Eygptian" href="/slots/egyptian/" data-hijack="true">
					Egyptian
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-hungry-for-slots" data-gtag="Game Menu,Themes+Hungry for Slots" href="/slots/hungry-for-slots/" data-hijack="true">
					Hungry for Slots
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-pirate-treasures" data-gtag="Game Menu,Themes+Pirate and Adventure" href="/slots/pirate-and-adventure/" data-hijack="true">
					Pirate and Adventure
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-exclusive" data-gtag="Game Menu,Themes+Exclusive Games" href="/slots/exclusive-games/" data-hijack="true">
					Exclusive<br>Games
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-new" data-gtag="Game Menu,Themes+Brand New Slots" href="/slots/brand-new-slots/" data-hijack="true">
					Brand New<br>Slots
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-featured" data-gtag="Game Menu,Themes+Featured Games" href="/slots/featured-games/" data-hijack="true">
					Featured<br>Games
				</a>
			</li>
		</ul>
	</div>
	<div class="content active" id="panel2">
		<ul>
			<li>
				<a class="menu-link" id="ui-all-slots" data-gtag="Game Menu,Features+All Slots" href="/slots/all-slots/" data-hijack="true">
					All<br>Slots
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-jackpot-slots" data-gtag="Game Menu,Features+MEGA Jackpot" href="/slots/mega-jackpots/" data-hijack="true">
					MEGA Jackpot
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-free-spins" data-gtag="Game Menu,Features+Free Spins Features" href="/slots/free-spins-features/" data-hijack="true">
					Free Spins Features
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-bonus-round" data-gtag="Game Menu,Features+Bonus Round Features" href="/slots/bonus-round-features/" data-hijack="true">
					Bonus Round Features
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-multiways-extra" data-gtag="Game Menu,Features+Multiways Extra Features" href="/slots/multiways-extra-features/" data-hijack="true">
					Multiways Extra Features
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-stacked-wilds" data-gtag="Game Menu,Features+Stacked Wilds Features" href="/slots/stacked-wilds-features/" data-hijack="true">
					Stacked Wilds Features
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-tumbling-reels" data-gtag="Game Menu,Features+Tumbling Reels Features" href="/slots/tumbling-reels-features/" data-hijack="true">
					Tumbling Reels Features
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-click-me" data-gtag="Game Menu,Features+Click Me Features" href="/slots/click-me-features/" data-hijack="true">
					Click Me Features
				</a>
			</li>
		</ul>
	</div>
	<div class="content" id="panel3">
		<ul>
			<li>
				<a class="menu-link" id="ui-10-less" data-gtag="Game Menu,Paylines+10 & Less" href="/slots/10-and-less-line-slots/" data-hijack="true">
					10 & Less<br>Lines
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-15" data-gtag="Game Menu,Paylines+15" href="/slots/15-line-slots/" data-hijack="true">
					15<br>Lines
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-20" data-gtag="Game Menu,Paylines+20" href="/slots/20-line-slots/" data-hijack="true">
					20<br>Lines</a>
			</li>
			<li>
				<a class="menu-link" id="ui-25" data-gtag="Game Menu,Paylines+25" href="/slots/25-line-slots/" data-hijack="true">
					25<br>Lines
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-30" data-gtag="Game Menu,Paylines+30" href="/slots/30-line-slots/" data-hijack="true">
					30<br>Lines
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-40" data-gtag="Game Menu,Paylines+40" href="/slots/40-line-slots/" data-hijack="true">
					40<br>Lines
				</a>
			</li>
			<li>
				<a class="menu-link" id="ui-50-over" data-gtag="Game Menu,Paylines+50 & Over" href="/slots/50-and-over-slots/" data-hijack="true">
					50 & Over <br>Lines
				</a>
			</li>
		</ul>
	</div>
	<div class="content" id="panel4">
		<ul>
			<!-- <li>
				<a class="menu-link" id="ui-novomatic" data-gtag="Game Menu,Providers+Novomatic Games" href="/slots/novomatic/" data-hijack="true">
					Novomatic
				</a>
			</li> -->
			<!-- <li><a class="menu-link" id="ui-eyecon" data-gtag="Game Menu,Publisher+Eyecon" href="/slots/eyecon/" data-hijack="true">Eyecon<br></a></li>
									<li><a class="menu-link" id="ui-daub" data-gtag="Game Menu,Providers+Daub Games" href="/slots/dagacube/" data-hijack="true">Daub<br>Games</a></li>
									<li><a class="menu-link" id="ui-igt" data-gtag="Game Menu,Providers+IGT" href="/slots/igt/" data-hijack="true">IGT</a></li> -->
			<!-- <li><a class="menu-link" id="ui-evolution" data-gtag="Game Menu,Providers+Evolution Games" href="/slots/evolution/" data-hijack="true">Evolution<br>Games</a></li> -->
			<!-- <li><a class="menu-link" id="ui-netent" data-gtag="Game Menu,Providers+Netent" href="/slots/netent/" data-hijack="true">NetEnt</a></li>
									<li><a class="menu-link" id="ui-microgaming" data-gtag="Game Menu,Providers+Microgaming" href="/slots/microgaming/" data-hijack="true">Microgaming</a></li>
									<li><a class="menu-link" id="ui-scientific" data-gtag="Game Menu,Providers+Scientific Games" href="/scientific-games/" data-hijack="true">Scientific<br>Games</a></li>
									<li><a class="menu-link" id="ui-play-and-go" data-gtag="Game Menu,Providers+Play'n Go" href="/slots/playngo/" data-hijack="true">Play'n GO</a></li>
									<li><a class="menu-link" id="ui-realistic" data-gtag="Game Menu,Providers+Realistic Games" href="/slots/realistic/" data-hijack="true">Realistic<br>Games</a></li> -->
		</ul>
	</div>
</div>