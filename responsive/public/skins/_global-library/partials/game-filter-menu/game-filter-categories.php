<dl class="tabs ui-filter-tabs" data-tab>
	<dd id="ui-features" class="tab active">
		<a class="tab__item" href="#game-filter-panel2">Features</a>
	</dd>
	<dd id="ui-paylines" class="tab">
		<a class="tab__item" href="#game-filter-panel3">Paylines</a>
	</dd>
	<dd id="ui-themes" class="tab">
		<a class="tab__item" href="#game-filter-panel1">Themes</a>
	</dd>

	<?php if (config("SkinID") != 2 && config("SkinID") != 3 && config("SkinID") != 6 && config("SkinID") != 9 ) : ?>
		<dd id="ui-providers" class="tab">
			<a class="tab__item" href="#game-filter-panel4">Publisher</a>
		</dd>
	<?php endif; ?>
</dl>
<div class="tabs-content">
	<div class="content" id="game-filter-panel1">
		<ul>
			<li><a class="menu-link" id="ui-magic-myth" data-gtag="Game Menu,Themes+Magicand Myth" href="/slots/magic-and-myth/" data-hijack="true">Magic and Myth</a></li>
			<li><a class="menu-link" id="ui-animals-nature" data-gtag="Game Menu,Themes+Animals and Nature" href="/slots/animals-and-nature/" data-hijack="true">Animals and Nature</a></li>
			<li><a class="menu-link" id="ui-movie-adventure" data-gtag="Game Menu,Themes+Movie and Adventure" href="/slots/movie-and-adventure/" data-hijack="true">Movie and Adventure</a></li>
			<li><a class="menu-link" id="ui-egyptian" data-gtag="Game Menu,Themes+Egyptian" href="/slots/egyptian/" data-hijack="true">Egyptian<br>Slots</a></li>
			<li><a class="menu-link" id="ui-food-and-fruit" data-gtag="Game Menu,Themes+Food and Fruits" href="/slots/food-and-fruit/" data-hijack="true">Food and Fruits</a></li>
			<li><a class="menu-link" id="ui-pirate-treasures" data-gtag="Game Menu,Themes+Pirates and Treasures" href="/slots/pirates-and-treasure/" data-hijack="true">Pirates and Treasure</a></li>
			<li><a class="menu-link" id="ui-exclusive" data-gtag="Game Menu,Themes+Exclusive" href="/slots/exclusive/" data-hijack="true">Exclusive<br>Slots</a></li>
			<li><a class="menu-link" id="ui-new" data-gtag="Game Menu,Themes+New" href="/slots/new/" data-hijack="true">New<br>Slots</a></li>
			<li><a class="menu-link" id="ui-featured" data-gtag="Game Menu,Themes+Featured" href="/slots/featured/" data-hijack="true">Featured<br>Slots</a></li>
		</ul>
	</div>
	<div class="content active" id="game-filter-panel2">
		<ul>
			<li><a class="menu-link" id="ui-all-slots" data-gtag="Game Menu,Features+All Slots" href="/slots/all/" data-hijack="true">All <br>Slots</a></li>
			<li><a class="menu-link" id="ui-jackpot-slots" data-gtag="Game Menu,Features+Jackpot Slots" href="/slots/jackpot-slots/" data-hijack="true">Jackpot <br>Slots</a></li>
			<li><a class="menu-link" id="ui-free-spins" data-gtag="Game Menu,Features+Free Spins" href="/slots/free-spins/" data-hijack="true">Free <br>Spins</a></li>
			<li><a class="menu-link" id="ui-bonus-round" data-gtag="Game Menu,Features+Bonus Round" href="/slots/bonus-round/" data-hijack="true">Bonus <br>Round</a></li>
			<li><a class="menu-link" id="ui-multiways-extra" data-gtag="Game Menu,Features+Multiways Extra" href="/slots/multiways-extra/" data-hijack="true">Multiways <br>Extra</a></li>
			<li><a class="menu-link" id="ui-stacked-wilds" data-gtag="Game Menu,Features+Stacked Wilds" href="/slots/stacked-wilds/" data-hijack="true">Stacked <br>Wilds</a></li>
			<li><a class="menu-link" id="ui-tumbling-reels" data-gtag="Game Menu,Features+Tumbling Reels" href="/slots/tumbling-reels/" data-hijack="true">Tumbling <br>Reels</a></li>
			<li><a class="menu-link" id="ui-click-me" data-gtag="Game Menu,Features+Click Me" href="/slots/click-me/" data-hijack="true">Click Me<br>Slots</a></li>
		</ul>
	</div>
	<div class="content" id="game-filter-panel3">
		<ul>
			<li><a class="menu-link" id="ui-10-less" data-gtag="Game Menu,Paylines+10 & Less" href="/slots/10-and-less-line-slots/" data-hijack="true">10 & Less<br>Lines</a></li>
			<li><a class="menu-link" id="ui-15" data-gtag="Game Menu,Paylines+15" href="/slots/15-line-slots/" data-hijack="true">15<br>Lines</a></li>
			<li><a class="menu-link" id="ui-20" data-gtag="Game Menu,Paylines+20" href="/slots/20-line-slots/" data-hijack="true">20<br>Lines</a></li>
			<li><a class="menu-link" id="ui-25" data-gtag="Game Menu,Paylines+25" href="/slots/25-line-slots/" data-hijack="true">25<br>Lines</a></li>
			<li><a class="menu-link" id="ui-30" data-gtag="Game Menu,Paylines+30" href="/slots/30-line-slots/" data-hijack="true">30<br>Lines</a></li>
			<li><a class="menu-link" id="ui-40" data-gtag="Game Menu,Paylines+40" href="/slots/40-line-slots/" data-hijack="true">40<br>Lines</a></li>
			<li><a class="menu-link" id="ui-50-over" data-gtag="Game Menu,Paylines+50 & Over" href="/slots/50-and-more-line-slots/" data-hijack="true">50 & Over<br>Lines</a></li>
		</ul>
	</div>
	<div class="content" id="game-filter-panel4">
		<ul>
		<?php
			if( config("SkinID") === 12):
		?>
			<li><a class="menu-link" id="ui-novomatic" data-gtag="Game Menu,Publisher+Novomatic" href="/slots/novomatic/" data-hijack="true">Novomatic<br></a></li>
			<?php
			endif;
			?>
			<!-- <li><a class="menu-link" id="ui-eyecon" data-gtag="Game Menu,Publisher+Eyecon" href="/slots/eyecon/" data-hijack="true">Eyecon<br></a></li>
			<li><a class="menu-link" id="ui-daub" data-gtag="Game Menu,Publisher+Daub Games" href="/slots/dagacube/" data-hijack="true">Daub<br>Games</a></li>
			<li><a class="menu-link" id="ui-igt" data-gtag="Game Menu,Publisher+IGT" href="/slots/igt/" data-hijack="true">IGT</a></li> -->
			<!-- <li><a class="menu-link" id="ui-evolution" data-gtag="Game Menu,Providers+Evolution Games" href="/slots/evolution/" data-hijack="true">Evolution<br>Games</a></li> -->
			<!-- <li><a class="menu-link" id="ui-netent" data-gtag="Game Menu,Publisher+Netent" href="/slots/netent/" data-hijack="true">NetEnt</a></li>
			<li><a class="menu-link" id="ui-microgaming" data-gtag="Game Menu,Publisher+Microgaming" href="/slots/microgaming/" data-hijack="true">Microgaming</a></li>
			<li><a class="menu-link" id="ui-scientific" data-gtag="Game Menu,Publisher+Scientific Games" href="/scientific-games/" data-hijack="true">Scientific<br>Games</a></li>
			<li><a class="menu-link" id="ui-play-and-go" data-gtag="Game Menu,Publisher+Play'n Go" href="/slots/playngo/" data-hijack="true">Play'n GO</a></li>
			<li><a class="menu-link" id="ui-realistic" data-gtag="Game Menu,Publisher+Realistic Games" href="/slots/realistic/" data-hijack="true">Realistic<br>Games</a></li> -->
		</ul>
	</div>
</div>