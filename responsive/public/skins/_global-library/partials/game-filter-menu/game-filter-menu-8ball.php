<script type="application/javascript">
skinPlugins.push({
	id: "magicalMenu",
	options: {
		global: true
<?php 	if (isset($customMenuId)) {
			echo ",menuId: '$customMenuId',element:'$customMenuId'";
		}
?>		
	}
});
</script>
<div  class="ui-games-filter-menu<?php if (isset($customMenuId)) {echo "-$customMenuId";}?> games-filter-menu">
	<!-- game menu button -->
	<div class="game-filter-btn__wrapper ">
		<a class="game-filter-btn ui-game-filter-btn<?php if (isset($customMenuId)) {echo "-$customMenuId";}?>"><span>Slots by type &blacktriangledown;</span></a>
	</div>
	
	<!-- game menu container -->
	<div class="games-filter-menu__popover-wrap">
		
		<div class="games-filter-menu__popover game-filter-menu ui-game-filter-menu<?php if (isset($customMenuId)) {echo "-$customMenuId";}?>" align="center">
			<span class="nub"></span>
				<?php include "_global-library/partials/game-filter-menu/game-filter-categories-8ball.php" ?>
			</div>
		</div>
	</div>
	<!-- on click of #ui-game-filter-btn needs .games-filter-menu needs class toggling "selected " -->