<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE Bingo 2
$$$$$$$$$$$$$$$$$$$$$$
2 Rows x 6 colums
4 big
6 small
2x2  2x2  2x2  2x2  2x2
1x1 1x1 1x1 1x1 1x1 1x1
-->

<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">
	<?php edit($this->controller,$hub); ?>
	<div class="hub-grid">
		<!-- col 1-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],1); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 1-->
		<!-- col 2-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],2); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 2-->
		<!-- col 3-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="3" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],3); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 3-->


				<!-- col 4-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],4); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 4-->

	
	
		
		<!-- Beginning of second row  -->

	
			<!-- col 5-->
			<div class="hub-grid__col">
				<!-- item -->
				<div class="hub-grid__child ui-parent-game-icon" data-position="5" data-containerkey="<?php echo $hub ?>">
					<?php @$this->getPartial($this->content[$hub],5); ?>
				</div>
				<!-- /item -->
			</div>
			<!-- /col 5-->
			<!-- col 6-->
			<div class="hub-grid__col">
				<!-- item -->
				<div class="hub-grid__child ui-parent-game-icon" data-position="6" data-containerkey="<?php echo $hub ?>">
					<?php @$this->getPartial($this->content[$hub],6); ?>
				</div>
				<!-- /item -->
			</div>
			<!-- /col 6-->
			<!-- col 7-->
			<div class="hub-grid__col">
				<!-- item -->
				<div class="hub-grid__child ui-parent-game-icon" data-position="7" data-containerkey="<?php echo $hub ?>">
					<?php @$this->getPartial($this->content[$hub],7); ?>
				</div>
				<!-- /item -->
			</div>
			<!-- /col 7-->
			<!-- col 8-->
			<div class="hub-grid__col">
				<!-- item -->
				<div class="hub-grid__child ui-parent-game-icon" data-position="8" data-containerkey="<?php echo $hub ?>">
					<?php @$this->getPartial($this->content[$hub],8); ?>
				</div>
				<!-- /item -->
			</div>
			<!-- /col 8-->
			<!-- col 9-->
			<div class="hub-grid__col">
				<!-- item -->
				<div class="hub-grid__child ui-parent-game-icon" data-position="9" data-containerkey="<?php echo $hub ?>">
					<?php @$this->getPartial($this->content[$hub],9); ?>
				</div>
				<!-- /item -->
			</div>
			<!-- /col 9-->
			<!-- col 10-->
			<div class="hub-grid__col">
				<!-- item -->
				<div class="hub-grid__child ui-parent-game-icon" data-position="10" data-containerkey="<?php echo $hub ?>">
					<?php @$this->getPartial($this->content[$hub],10); ?>
				</div>
				<!-- /item -->
			</div>
			<!-- /col 10-->
			<!-- col 11-->

		</div>
		
	</div>