<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 8
$$$$$$$$$$$$$$$$$$$$$$
6 columns x 2 Rows
6 small icon 
1x1 1x1 1x1 1x1 1x1
1x1 1x1 1x1 1x1 1x1


-->
<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">
<?php edit($this->controller,$hub); ?>
	<div class="hub-grid">
		
		<!-- col 1-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],1); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 1-->
		<!-- col 2-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],2); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 2-->
		<!-- col 3-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="3" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],3); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 3-->
		<!-- col 4-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],4); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 4-->
		<!-- col 5-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="5" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],5); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 5-->
		<!-- col 6-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="6" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],6); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 6-->
	</div>

	<div class="hub-grid">
		
		<!-- col 1-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="7" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],7); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 1-->
		<!-- col 2-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="8" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],8); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 2-->
		<!-- col 3-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="9" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],9); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 3-->
		<!-- col 4-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="10" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],10); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 4-->
		<!-- col 5-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="11" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],11); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 5-->
		<!-- col 6-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="12" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],12); ?>
			</div>
			<!-- /item -->
		</div>
		<!-- /col 6-->
	</div>


	
</div>