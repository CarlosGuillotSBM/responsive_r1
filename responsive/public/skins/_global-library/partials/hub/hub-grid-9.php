<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 9
$$$$$$$$$$$$$$$$$$$$$$
3 LARGE
3 columns x 1 Rows
3 big icon
2x2 2x2 2x2

-->
<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">

	<div class="hub-grid">
		
		<!-- col 1-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="7" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],1); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 1-->
		<!-- col 2-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="8" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],2); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 2-->
		<!-- col 3-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="9" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],3); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 3-->
		
	</div>
	
</div>


