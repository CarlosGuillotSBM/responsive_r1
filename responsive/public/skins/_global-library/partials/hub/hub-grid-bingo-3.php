<div class="hub">
    <?php edit($this->controller,$hub);?>
    <div class="hub-grid">
        <?php for ($i = 1; $i <= count($this->content[$hub]) && $i <= 8; $i++):?>
            <!-- col <?php echo $i; ?>-->
            <div class="hub-grid__col-2x">
                <!-- row -->
                <div class="hub-grid__col-2x__row">
                    <!-- item -->
                    <div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
                        <!-- BIG CONTAINER -->
                        <?php @$this->getPartial($this->content[$hub], $i); ?>
                        <!-- BIG CONTAINER -->
                    </div>
                    <!-- /item -->
                </div>
                <!-- row -->
            </div>
            <!-- /col  <?php echo $i; ?>-->
        <?php endfor; ?>
	</div>
</div>
