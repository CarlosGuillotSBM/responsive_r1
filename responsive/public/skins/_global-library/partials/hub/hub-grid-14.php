<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 14 MOBILE
$$$$$$$$$$$$$$$$$$$$$$
6 columns x 2 Rows
2 icons x 1 widget
4 icons

1x1 1x1 2x2 ===
1x1 1x1 1x1 1x1 

-->

<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">
<?php edit($this->controller,$hub); ?>

	<!-- row /////////////////////////////  -->
	<div class="hub-grid">
		<!-- col 1-->
		<div class="hub-grid__col">
			<div class="hub-grid__child ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],1); ?>
			</div>
		</div>

		<!-- col 2-->
		<div class="hub-grid__col">
			<div class="hub-grid__child ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],2); ?>
			</div>
		</div>

		<!-- col 3-->
		<!-- Aquisition Banner -->
		<div class="hub-grid__col-2x">
			<div class="hub-grid__col-2x__row">
				<div class="hub-grid__child ui-parent-game-icon" data-position="9" data-containerkey="<?php echo $hub ?>">
					<?php @$this->getPartial($this->content[$hub],9); ?>
				</div>
			</div>
		</div>
	</div>


	<!-- row /////////////////////////////  -->
	<div class="hub-grid">
		<!-- col 1-->
		<div class="hub-grid__col">
			<div class="hub-grid__child ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],4); ?>
			</div>
		</div>

		<!-- col 2-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="5" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],5); ?>
			</div>
			<!-- /item -->
		</div>

		<!-- col 3-->
		<div class="hub-grid__col">
			<div class="hub-grid__child ui-parent-game-icon" data-position="6" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],6); ?>
			</div>
		</div>

		<!-- col 4-->
		<div class="hub-grid__col">
			<div class="hub-grid__child ui-parent-game-icon" data-position="7" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],7); ?>
			</div>
		</div>
	</div>


	
</div>