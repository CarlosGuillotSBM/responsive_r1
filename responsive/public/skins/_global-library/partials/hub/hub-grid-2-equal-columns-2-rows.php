<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 
$$$$$$$$$$$$$$$$$$$$$$
2 columns x 2 Rows
1x1 1x1 
1x1 1x1 
-->

<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">
	
	<div class="hub-grid">
		
		<!-- col 1-->
		<div class="hub-grid__2columns">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],1); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="3" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],3); ?>
			</div>
			<!-- /item -->
	
		</div>
		<!-- /col 1-->
		<!-- col 5-->
		<div class="hub-grid__2columns">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],2); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],4); ?>
			</div>
			<!-- /item -->

		</div>
		<!-- /col 2-->
	</div>
</div>