<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 12
$$$$$$$$$$$$$$$$$$$$$$
6 columns x 2 Rows
8 GAME ICONS x 1 LARGE
1x1 1x1 1x1 1x1 1x1 2x2
1x1 1x1 1x1 1x1 1x1 ===

used on magical vegas top games hub
-->


<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">
  
  <div class="hub-grid">


      <!-- col 1-->
    <div class="hub-grid__col-2x">
      <!-- row -->
      <div class="hub-grid__col-2x__row">
        <!-- item -->
        <div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="9" data-containerkey="<?php echo $hub ?>">
          <!-- BIG CONTAINER -->
          <?php @$this->getPartial($this->content[$hub],1); ?>
          <!-- BIG CONTAINER -->
        </div>
        <!-- /item -->
      </div>
      <!-- row -->
    </div>
    <!-- /col 1-->
    
    <!-- col 2-->
    <div class="hub-grid__col">
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],2); ?>
      </div>
      <!-- /item -->
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],3); ?>
      </div>
      <!-- /item -->
    </div>
    <!-- /col 2-->
    <!-- col 2-->
    <div class="hub-grid__col">
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="3" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],4); ?>
      </div>
      <!-- /item -->
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],5); ?>
      </div>
      <!-- /item -->
    </div>
    <!-- /col 2-->
    <!-- col 3-->
    <div class="hub-grid__col">
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="5" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],6); ?>
      </div>
      <!-- /item -->
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="6" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],7); ?>
      </div>
      <!-- /item -->

    </div>
    <!-- /col 3-->

    
    <!-- col 4-->
    <div class="hub-grid__col">
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="7" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],8); ?>
      </div>
      <!-- /item -->
      <!-- item -->
      <div class="hub-grid__child ui-parent-game-icon" data-position="8" data-containerkey="<?php echo $hub ?>">
        <?php @$this->getPartial($this->content[$hub],9); ?>
      </div>
      <!-- /item -->

    </div>
    <!-- /col 4-->




  </div>
</div>