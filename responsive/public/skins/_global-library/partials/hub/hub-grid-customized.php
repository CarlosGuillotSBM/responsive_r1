<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 1
$$$$$$$$$$$$$$$$$$$$$$
6 columns x 3 Rows
2 big icon x 1 widget
1x1 1x1 1x1 1x1 1x1 1x1
1x1 1x1 1x1 1x1 1x1 1x1
1x1 1x1 1x1 1x1 1x1 1x1
-->
<?php
$configName = $_SERVER['SPACEBAR_CONFIG'] ?? '';
$config = Config::loadConfig( $configName);


?>
<section class="section middle-content__box games-list home-games">

    <?php
    $hub = $row['Games'];
    $hub = (isset($hub)) ? $hub : 'hub';



    if( isset( $row["Text2"] ) ){

    $x = (int)ceil($row["Text2"] / 5);
    $y = (int)5;
    $total_games_selected = $row["Text2"];

    ?>
    <div class="hub-outer-wrapper">
        <div class="hub-title"><?php echo $row['Title']; ?></div>
        <div class="hub">
            <div class="hub-grid">
              <?php
                for ($i = 1; $i <= $x; $i++) {
                    for ($j = 1; $j <= $y; $j++) {
                        if($total_games_selected >0) {
                        ?>
                            <div class="hub-grid__col">
                                <div class="hub-grid__child ui-parent-game-icon"
                                     data-position="<?php echo $i * $j; ?>"
                                     data-containerkey="<?php echo $hub ?>">
                                    <?php
                                    --$total_games_selected;
                                    $row = $hub[$total_games_selected];
                                    include "_global-library/_editor-partials/game-icon.php";
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
?> 

                </div>
            </div>
        </div>
        <?php
    }
    ?>


</section>

