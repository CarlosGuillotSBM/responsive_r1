<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 1
$$$$$$$$$$$$$$$$$$$$$$
6 columns x 3 Rows
2 big icon x 1 widget
1x1 1x1 1x1 1x1 1x1 1x1
1x1 1x1 1x1 1x1 1x1 1x1
1x1 1x1 1x1 1x1 1x1 1x1
-->

<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">
	
	<div class="hub-grid">
		
		<!-- col 1-->
		<div class="hub-grid__4columns">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],1); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="5" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],5); ?>
			</div>
			<!-- /item -->
	
		</div>
		<!-- /col 1-->
		<!-- col 5-->
		<div class="hub-grid__4columns">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],2); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="6" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],6); ?>
			</div>
			<!-- /item -->

		</div>
		<!-- /col 2-->
		<!-- col 3-->
		<div class="hub-grid__4columns">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="3" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],3); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="7" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],7); ?>
			</div>
			<!-- /item -->

		</div>
		<!-- /col 3-->
		<!-- col 4-->
		<div class="hub-grid__4columns">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],4); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="8" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],8); ?>
			</div>
			<!-- /item -->

		</div>
		<!-- /col 4-->
	</div>
</div>