<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 
$$$$$$$$$$$$$$$$$$$$$$
5 colums x 2 rows
-->
<?php $hub = (isset($hub)) ? $hub :'grid-item';?>
			<!-- item -->
			<div class="grid-item animated delay-start fadeInLeft" data-position="1" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],1,"_global-library/_editor-partials/game-icon-with-overlay.php"); ?>

			</div>

			<!-- /item -->
			<!-- item -->
			<?php 
			// Iterate from second to ninth item to display the partial for that game
			for ($i=2; $i<=9; $i++): 
				//Create an array that contains the type of animations we can get
				$animatedDelays=["delay-start","delay-05","delay-25","delay-35","delay-45","delay-55"];
				// Add fadeInLeft class to elements 2 and 6
				$animFadeClass= ($i == 2 || $i == 6 ? "fadeInLeft" : "fadeInRight");
				// Get a random class from the $animatedDelays Array
				$animatedDelayClass= $animatedDelays[rand(0,sizeof($animatedDelays)-1)];
			?>
			<div class="grid-item animated <?="$animFadeClass $animatedDelayClass"?>" data-position="1" data-containerkey="<?php echo $hub ?>">
				<?php $this->getPartial($this->content[$hub],$i, "_global-library/_editor-partials/game-icon-with-overlay.php"); ?>
			</div>
			<?php endfor;?>