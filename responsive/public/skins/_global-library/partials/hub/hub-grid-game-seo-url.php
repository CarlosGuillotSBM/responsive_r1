<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 7
$$$$$$$$$$$$$$$$$$$$$$
5 columns x 2 Rows
6 big icon 
2x2 2x2 2x2 2x2 2x2
2x2 2x2 2x2 2x2 2x2


-->
<div class="hub">
		<?php edit($this->controller,$hub); ?>
		<div class="hub-grid">		
		<!-- col 1-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],1); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 1-->
		<!-- col 2-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],2); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 2-->
		<!-- col 3-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="3" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],3); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 3-->

				<!-- col 4-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],4); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 4-->
	</div>


	<!-- row 2 -->

	<div class="hub-grid">
		
		<!-- col 5-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="5" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],5); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 5-->
		<!-- col 6-->
		<div class="hub-grid__col-2x">
			<!-- row -->
			<div class="hub-grid__col-2x__row">
				<!-- item -->
				<div class="hub-grid__child hub-grid__child-featured ui-parent-game-icon" data-position="6" data-containerkey="<?php echo $hub ?>">
					<!-- BIG CONTAINER -->
					<?php @$this->getPartial($this->content[$hub],6); ?>
					<!-- BIG CONTAINER -->
				</div>
				<!-- /item -->
			</div>
			<!-- row -->
		</div>
		<!-- /col 6-->




	</div>
	
</div>