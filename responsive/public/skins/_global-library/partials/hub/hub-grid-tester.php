<!--
$$$$$$$$$$$$$$$$$$$$$$
$$$$ HUB TEMPLATE 1
$$$$$$$$$$$$$$$$$$$$$$
6 columns x 3 Rows
2 big icon x 1 widget
1x1 1x1 1x1 1x1 1x1 1x1
1x1 1x1 1x1 1x1 1x1 1x1
1x1 1x1 1x1 1x1 1x1 1x1
-->

<?php $hub = (isset($hub)) ? $hub : 'hub';?>
<div class="hub">
	
	<div class="hub-grid">
		
		<!-- col 1-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="1" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],1); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="2" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],2); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="3" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],3); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="4" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],4); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="5" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],5); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="6" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],6); ?>
			</div>
			<!-- /item -->			
		</div>
		<!-- /col 1-->
		<!-- col 2-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="7" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],7); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="8" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],8); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="9" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],9); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="10" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],10); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="11" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],11); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="12" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],12); ?>
			</div>
			<!-- /item -->			
		</div>
		<!-- /col 2-->
		<!-- col 3-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="13" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],13); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="14" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],14); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="15" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],15); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="16" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],16); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="17" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],17); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="18" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],18); ?>
			</div>
			<!-- /item -->			
		</div>
		<!-- /col 3-->
		<!-- col 4-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="19" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],19); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="20" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],20); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="21" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],21); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="22" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],22); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="23" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],23); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="24" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],24); ?>
			</div>
			<!-- /item -->			
		</div>
		<!-- /col 4-->
		<!-- col 5-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="25" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],25); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="26" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],26); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="27" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],27); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="28" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],28); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="29" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],29); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="30" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],30); ?>
			</div>
			<!-- /item -->			
		</div>
		<!-- /col 5-->
		<!-- col 6-->
		<div class="hub-grid__col">
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="31" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],31); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="32" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],32); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="33" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],33); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="34" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],34); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="35" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],35); ?>
			</div>
			<!-- /item -->
			<!-- item -->
			<div class="hub-grid__child ui-parent-game-icon" data-position="36" data-containerkey="<?php echo $hub ?>">
				<?php @$this->getPartial($this->content[$hub],36); ?>
			</div>
			<!-- /item -->			
		</div>
		<!-- /col 6-->
		

	</div>
</div>