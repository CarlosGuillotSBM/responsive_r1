<!-- TITLE -->
<div class="promotion-eng__title__wrapper">
    <!-- CONTENT -->
    <div class="promotion-eng__freebie">
        <div class="promotion-eng__freebie__content ui-promotion-eng__freebie__content-<?php echo $row["DetailsURL"]?>">
            Title
        </div>
        <div class="promotion-eng__freebie__progress--title">
            Your progress
        </div>
        <div class="promotion-eng__freebie__progress--value ui-promotion-eng__freebie__progress--value-<?php echo $row["DetailsURL"]?>">
            0%
        </div>
        <div class="promotion-eng__freebie__progress--wrap">
            <div class="promotion-eng__freebie__progress--bar ui-promotion-eng__freebie__progress--bar-<?php echo $row["DetailsURL"]?>">&nbsp;</div>
        </div>

        <div class="ui-freebie-confirmation ui-confirmation-message-<?php echo $row["DetailsURL"]?>" style="display: none">
            Congratulations! You've qualified.
        </div>

    </div>
</div>
