<script type="application/javascript">
    skinModules.push({
        id: "OptIn",
        options: {
            modalSelectorWithoutDetails: "#custom-modal"
        }
    });

</script>
<?php
/* R10-845 -Renamed variables for clarity */
$pFilter=$row["Text5"];
$pTerms=$row["Text6"];
$pCTALoggedIn=$row["Text2"];
$pCTALoggedOut=$row["Text3"];
$pCTAUrl=$row["Text4"];
?>
<!-- PROMO WRAP -->
<div class="promotion-eng" data-filterclass="promotion-eng" data-playerfilter="<?php echo $pFilter?>">
    <script type="application/javascript">
        skinModules.push({
            id: "RadFilter"
        });
    </script>
    <div class="promotion-eng__wrapper">
        <!-- PROMO IMAGE -->
        <div class="promotion-eng__image">
            <img src="<?php echo $row["Image"]?>" alt="<?php echo $row["Alt"]?>" onclick="location.href='/promotions/<?php echo $row["DetailsURL"]?>/'">
        </div>

        <!-- T&C'S -->
        <div class="promotion__text_terms-wrapper">
          <span class="promotion__text_terms"><?php echo $pTerms; ?></span><a class="promotion-eng__terms" data-reveal-id="promo-terms-and-conditions-modal-<?php echo $row["ID"]; echo (isset($WhichTab)? '-'.$WhichTab : '');?>"> T&amp;Cs apply &#10095;</a>
        </div>


        <!-- TABS NAV -->
        <dl class="tabs tab-without-competition ui-promo-tab-<?php echo $row["DetailsURL"]; ?>" data-tab>
          <dd class="tab-title active"><a href="#panel<?php echo $row["ID"]; ?>1">Promo Details</a></dd>
          <dd class="tab-title prize-draw-<?php echo $row["DetailsURL"]; ?>"><a class="ui-prize-draw-<?php echo $row["DetailsURL"]; ?>" href="#panel<?php echo $row["ID"]; ?>2">Prize Draw</a></dd>
          <dd class="tab-title leaderboard-<?php echo $row["DetailsURL"]; ?>"><a href="#panel<?php echo $row["ID"]; ?>3">Leader Board</a></dd>  
          <dd class="tab-title freebie-<?php echo $row["DetailsURL"]; ?>"><a href="#panel<?php echo $row["ID"]; ?>4">Freebie Progress</a></dd>
        </dl>

        <!-- CONTENT WRAP -->
        <div class="promotion-eng__content">

          <!-- TABS CONTENT -->
          <div class="tabs-content">
            <!-- TAB 1 -->
            <div class="content active" id="panel<?php echo $row["ID"]; ?>1">
              <?php include "_global-library/partials/promotion/promotion-tab-details.php" ?>
            </div>
            <!-- TAB 2 -->
            <div class="content" id="panel<?php echo $row["ID"]; ?>2">
              <?php include "_global-library/partials/promotion/promotion-tab-price-draw.php" ?>
            </div>
            <!-- TAB 3 -->
            <div class="content" id="panel<?php echo $row["ID"]; ?>3">
              <?php include "_global-library/partials/promotion/promotion-tab-leader-board.php" ?>
            </div>
            <!-- TAB 4 -->
            <div class="content" id="panel<?php echo $row["ID"]; ?>4">
              <?php include "_global-library/partials/promotion/promotion-tab-freebie.php" ?>
            </div>
          </div>
          <!-- / TABS CONTENT -->

        </div>

        <!-- FOOTER -->
        <!-- FOOTER -->
        <div class="promotion-eng__footer">
          <!-- READ MORE -->
          <span class="promotion-eng__read-more" onclick="location.href='/promotions/<?php echo $row["DetailsURL"]?>/'">Read More</span>
          <!-- CLAIM PRIZE -->
          <a id="claim-now-<?php echo $row['DetailsURL']?>" data-gtag="Promo Card,Claim Prize" class="promotion-eng__optin" data-bind="click: OptIn.claimPrizePromotion.bind($data,'<?php echo $row['DetailsURL']?>')" style="display: none" >CLAIM PRIZE</a>
          <!-- OPT-IN -->
          <a data-gtag="Promo Card,Opt In" class="promotion-eng__optin" style="display: none" data-title="<?php echo $row['Title'] ?>" data-tournament="<?php echo $row['DetailsURL'] ?>">OPT-IN</a>
          <?php if($pCTAUrl === '') {?>
    		    <!-- PLAY NOW -->
            <span class="promotion-eng__play-now cta-full-width" data-bind="click: playBingo">
              <span style="display:none" data-bind="visible: validSession()"><?php if($pCTALoggedIn === '') echo 'Play Now'; else echo $pCTALoggedIn; ?></span><span data-bind="visible: !validSession()"><?php if($row['Text3'] === '') echo 'Play Now'; else echo $row['Text3']; ?></span>
          </span>
            <?php } 
            else { 
              /* Launch 3 Radical from a promo CTA */
              if (strpos( $row["Text4"],"my-account/?code" ) !== false){ ?>
              <span class="promotion-eng__play-now cta-full-width" data-bind="click: claimCodeCTA.bind($data,'<?php echo $row['Text4']; ?>')" ><span style="display:none" data-bind="visible: validSession()">
              <?php if($row['Text2'] === '') echo 'Play Now'; else echo $row['Text2']; ?></span><span data-bind="visible: !validSession()"><?php if($row['Text3'] === '') echo 'Play Now'; else echo $row['Text3']; 
              ?></span></span>
              <?php
              }
              /* Launch 3 Radical from a promo CTA */
              else if (strtolower( $pCTAUrl ) == "3rad"){
              ?>
              <span class="promotion-eng__play-now cta-full-width" data-bind="click: threeRadicalCTA.bind($data)">
                <!-- Show span for logged in -->
                <span style="display:none" data-bind="visible: validSession()"><?php if($pCTALoggedIn === '') echo 'Play Now'; else echo $pCTALoggedIn; ?></span>
                <!-- Show span for logged out -->
                <span data-bind="visible: !validSession()"><?php if($pCTALoggedOut === '') echo 'Play Now'; else echo $pCTALoggedOut; 
                ?></span>
              </span>
              <?php
              }
              else {
              ?>
              <!-- PLAY NOW -->
              <span class="promotion-eng__play-now cta-full-width" data-bind="click: navigateOnLogin.bind($data,'<?php echo $pCTAUrl; ?>')" ><span style="display:none" data-bind="visible: validSession()"><?php if($pCTALoggedIn === '') echo 'Play Now'; else echo $pCTALoggedIn; ?></span><span data-bind="visible: !validSession()"><?php if($row['Text3'] === '') echo 'Play Now'; else echo $row['Text3']; 
              ?></span></span>
          <?php };
          } ?> 
        </div>        
        <!-- TERMS & CONDITIONS MODAL -->
        <div id="promo-terms-and-conditions-modal-<?php echo $row["ID"]; echo (isset($WhichTab)? '-'.$WhichTab : '');?>" class="reveal-modal" data-reveal>
            <h4 class="promotion__title">Terms &amp; Conditions</h4>
            <?php echo $row["Terms"]; ?>
            <span class="close-reveal-modal">&#215;</span>
        </div>

    </div>
</div>
<!-- / PROMO WRAP -->