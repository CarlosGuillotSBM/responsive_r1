<?php

    include 'paths.php';
    include 'config/config.php';
    include 'libs/application.php';
    include 'libs/controller.php';

    // get the request
    $req_dump = print_r($_REQUEST, TRUE);

    // get mobile number from the XML
    $xml = simplexml_load_string($_REQUEST["xml"]);
    $mobileNumber = (string)$xml->originator;

    // if a number has been sent
    if (strlen($mobileNumber) > 0) {

        // send request to database to update the player's record
        $controller = new Controller();

        $params =   array("MobilePhone" => array($mobileNumber, "str", 15),
            "SkinID"        => array(config("SkinID"), "int", 0),
            "SendSMS" => array(0, "int", 0)
        );

        $row = queryDB($controller->db,"DataSetPlayerSMS",$params);

        // if success
        if($row["Code"] == 0) {
            writelog("DataSetPlayerSMS: player's phone updated (" . $mobileNumber .")");
        } else {
            writelog("DataSetPlayerSMS: couldn't update the player's phone (" . $mobileNumber .")");
        }

    } else {
        writelog("DataSetPlayerSMS: skipped the call to SP - mobile number was empty");
    }