<script type="text/javascript">
skinModules.push({
id: "RealityCheck",
options: {
global: true
}
});
</script>

<div id="ui-reality-check-container" class="reality-check__container" data-bind="visible: RealityCheck.warningVisible" style="display: none">
    <div class="reality-check__content">
        
        <!--  Warning Title -->
        <h2>You've been playing for <span data-bind="text: RealityCheck.totalTimeDisplay"></span>.</h2>
        
        <!--  CTAS -->
        <div class="reality-check__buttons">
            <button data-bind="click: RealityCheck.resumeCheck" class="continue-cta">Keep playing <br>and remind me in <span data-bind="text: RealityCheck.frequencyDisplay"></span></button>
            <button data-bind="click: RealityCheck.stopReminders" class="stop-cta">Keep Playing <br>and don't disturb</button>
        </div>

        <!--  Warning Info -->
        <p>
            Or alternatively view your <a data-bind="click: RealityCheck.goToAccountActivity">Account Activity.</a>
        </p>

        <a data-bind="click: RealityCheck.goHome" class="home-cta">Go to the Homepage</a>

    </div>
</div>