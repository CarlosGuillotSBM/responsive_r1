    <script type="text/javascript">
        skinModules.push( {
        id: "RealityCheck",
        options: {
        global: true
        }
    });
    </script>
    <article class="text-partial" data-bind="visible: RealityCheck.active" style="display: none">
        <h1>Game Session Reminders Frequency</h1>

        <div class="text-partial-content">

            <?php edit($this->controller,'reality-check-top');  ?>
            <?php  @$this->repeatData($this->content['reality-check-top']);?>

            <!-- CONTAINER -->
            <div class="reality-check-settings__container" style="width: 100%">
                <select style="width: 45%" data-bind="value: RealityCheck.frequencyId,
                    options: RealityCheck.frequencyOptions,
                    optionsValue: 'ID',
                    optionsText: 'Description'" class="textField">
                </select>
                <div >
                    <a data-bind="click: RealityCheck.setSettings" href="" class="button expand">Update Frequency</a>
                </div>
            </div>
            <br>
            <!-- /CONTAINER -->

            <?php edit($this->controller,'reality-check-bottom');  ?>
            <?php  @$this->repeatData($this->content['reality-check-bottom']);?>

        </div>
    </article>