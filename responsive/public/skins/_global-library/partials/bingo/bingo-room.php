<ul class="bingo-schedule__row__items"
    data-bind="click: $root.BingoScheduler.openRoom,
               css: { alt: $index() % 2 !== 0 , 'funded' : playText === 'Funded Only',
                      active: secondsLive() < 60 }" >
    <li class="bingo-schedule__row__item room-name-col">
        <span class="icon" data-bind="text: displayGameType"></span>
        <span data-bind="html: name"></span>
    </li>
<!--    <li class="bingo-schedule__row__item feature-col"><img src="/_images/room-feature-icons/1tgfc.png"-->
<!--                                                           alt="one to go"></li>-->
    <li class="bingo-schedule__row__item jackpot-col" data-bind="text: displayJackpot"></li>
    <li class="bingo-schedule__row__item price-col" data-bind="text: cardPrice"></li>
    <li class="bingo-schedule__row__item starts-col"><span class="starts-for-mobile"> Starts:</span> <span data-bind="text: displaySecondsLive"></span></li>
    <li class="bingo-schedule__row__item play-col" data-bind="css: { 'funded' : playText === 'Funded Only' }">
        <a class="bingo-schedule__row__items_play" data-bind="text: playText"></a>
          <a class="bingo-schedule__row__items_play--small" data-bind="text: playText"></a>
    </li>
</ul>