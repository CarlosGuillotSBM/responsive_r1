<?php
do {
?>
<section class="<?php echo @$this->category["Class"];?> bingo-list middle-content__box">
	<div class="section__header">
		<h1 class="section__title"><?php echo $this->getCategoryHref();?></h1>
	</div>
	
	<div class="bingo-list__grid">
		<?php $i = 1; foreach($this->games as $row){ ?>
		<!-- item -->
		<div class="bingo-list__grid__child ui-parent-game-icon" data-position="<?php echo $i ?>" data-containerkey="<?php echo $this->category['Key']; ?>">		
			<?php include '_global-library/_editor-partials/bingo-room.php'; ?>
		</div>
		<!-- /item -->
		<?php $i++; }?>
	</div>
</section>
<?php
} while ($this->GetNextCategory());?>