<script type="application/javascript">
skinModules.push({id: "BingoScheduler"});
</script>

<div class="bingo-schedule-holder">
  <!-- content holder title -->
  <!-- content holder content area -->
  <div class="content-holder__content">
    <!-- content holder content full width inner holder-->
    <!-- <div class="content-holder__content_full-width"></div> -->
    
    <!-- content holder content left inner holder-->
    <div class="content-holder__content_full-width">
      <div class="content-holder__title">
        <h1>BINGO SCHEDULE</h1>
        <!-- content holder title right link -->
        <span class="content-holder__title__link">
          <div class="mobile-bingo-menu">
            <button data-bind="text: BingoScheduler.filterApplied"
            data-dropdown="drop1" aria-controls="drop1"
            aria-expanded="false" class="button dropdown">All Rooms</button><br>
            <ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true">
              <li><a data-bind="click: BingoScheduler.setFilter.bind($data,'ALL')">All Rooms</a></li>
              <li><a data-bind="click: BingoScheduler.setFilter.bind($data,'90')">90 Ball Bingo</a></li>
              <li><a data-bind="click: BingoScheduler.setFilter.bind($data,'75')">75 Ball Bingo</a></li>
              <li><a data-bind="click: BingoScheduler.setFilter.bind($data,'5L')">5L Ball Bingo</a></li>
              <li><a data-bind="click: BingoScheduler.setFilter.bind($data,'PRE')">Prebuys</a></li>
            </ul>
          </div>
          <?php  include'_global-library/partials/bingo/bingo-play-now-text.php'; ?>
          
        </div>
        
        <div class="content-holder__inner">
          <!-- Bingo Scedule  -->
          <div class="bingo-schedule">
            
            <!-- Bingo Category Tabs -->
            <div class="bingo-schedule__tabs">
              <dl class="tabs">
                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'ALL'), css: { active: BingoScheduler.filter() == 'ALL'}"><a >All</a></dd>
                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'90') , css: { active: BingoScheduler.filter() == '90'}"><a>Bingo 90</a></dd>
                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'75') , css: { active: BingoScheduler.filter() == '75'}"><a>Bingo 75</a></dd>
                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'5L') , css: { active: BingoScheduler.filter() == '5L'}"><a>Bingo 5L</a></dd>
                <dd data-bind="click: BingoScheduler.setFilter.bind($data,'PRE'), css: { active: BingoScheduler.filter() == 'PRE'}"><a>Prebuys</a></dd>
              </dl>
            </div>
            
            <!-- End Bingo Category Tabs -->
            <ul class="bingo-schedule__header">
              <li class="bingo-schedule__header__title room-name-col">Room</li>
              <li class="bingo-schedule__header__title jackpot-col">Jackpot</li>
              <li class="bingo-schedule__header__title price-col">Price</li>
              <li class="bingo-schedule__header__title starts-col">Starts In</li>
              <li class="bingo-schedule__header__title play-col"></li>
            </ul>
            
            <div class="bingo-schedule__rows">
              
              <!--  Bingo Schedule Single Row -->
              <div class="tabs-content">
                
                <!-- ALL -->
                <div style="display: none" data-bind="foreach: BingoScheduler.openedRooms, visible: BingoScheduler.filter() == 'ALL'">
                    <?php include "_global-library/partials/bingo/bingo-room.php"; ?>
                  </div>
                  
                  <!-- 90 -->
                  <div style="display: none" data-bind="foreach: BingoScheduler.rooms90, visible: BingoScheduler.filter() == '90'">
                      <?php include "_global-library/partials/bingo/bingo-room.php"; ?>
                    </div>
                    
                    <!-- 75 -->
                    <div style="display: none" data-bind="foreach: BingoScheduler.rooms75, visible: BingoScheduler.filter() == '75'">
                        <?php include "_global-library/partials/bingo/bingo-room.php"; ?>
                      </div>
                      
                      <!-- 5L -->
                      <div style="display: none" data-bind="foreach: BingoScheduler.rooms5L, visible: BingoScheduler.filter() == '5L'">
                          <?php include "_global-library/partials/bingo/bingo-room.php"; ?>
                        </div>
                        
                        <!-- PRE -->
                        <div style="display: none" data-bind="foreach: BingoScheduler.roomsPre, visible: BingoScheduler.filter() == 'PRE'">
                            <?php include "_global-library/partials/bingo/bingo-room.php"; ?>
                          </div>
                          
                        </div>
                        
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <!-- end Bingo Scedule  -->
                    
                  </div>
        <!-- content holder content right inner holder-->
      </div>
    </div>
  </div>
</div>
<!-- /main content -->