<script type="application/javascript">
    skinModules.push({
        id: "BingoRoomDetails",
        options: {
            global: true
        }
    });
</script>

<div class="room-detail-modal">


    <h1 class="room-detail__title-bar" data-bind="text: BingoRoomDetails.name()"></h1>

    <div class="room-detail-content">
    <div class="room-detail-content__left"><img alt="" itemprop="screenshot" data-bind="attr: { src: BingoRoomDetails.image() }"/></div>
        
        <div class="room-detail-content__right">
        <h3 class="room-detail-content__right__title">Room Details</h3>
              <ul class="room-detail-content__right__game-table">
                <li><span class='room-detail-content__game-table__title' >Jackpot Amount</span><span itemprop="publisher" data-bind="text: BingoRoomDetails.jackpot()"></span></li>
                <li><span class='room-detail-content__game-table__title'>Game Type</span><span itemprop="publisher" data-bind="text: BingoRoomDetails.gameType()"></span></li>
                <li><span class='room-detail-content__game-table__title'>Starting in</span><span itemprop="publisher" data-bind="text: BingoRoomDetails.secondsDisplay()"></span></li>
                <li><span class='room-detail-content__game-table__title'>Players</span><span itemprop="publisher" data-bind="text: BingoRoomDetails.players()"></span></li>
                <li><span class='room-detail-content__game-table__title'>Card Price</span><span itemprop="publisher" data-bind="text: BingoRoomDetails.cardPrice()"></span></li>
               <!--  <li>
                  <ul class="room-detail-content__progressive-icons">
                    <li> <img src="/_images/common/4x3-bg.png"> </li>
                    <li> <img src="/_images/common/4x3-bg.png"> </li>
                    <li> <img src="/_images/common/4x3-bg.png"> </li>
                    <li> <img src="/_images/common/4x3-bg.png"> </li>
                  </ul>
                </li> -->
              </ul>
                  <?php include'_partials/login/login-box.php' ?>
              </div>
    </div>

</div>