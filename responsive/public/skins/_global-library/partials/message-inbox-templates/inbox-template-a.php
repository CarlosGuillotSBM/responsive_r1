
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Template Inbox - A</title>
</head>

<style type="text/css">
	
.inbox-message-main {
	width: 96%;
    margin: 2%;
	height: auto;
}

.inbox-message {
    float: left;
    width: 100%;
}


.inbox-message h1{
    font-weight: bold;
    font-size: 14px;
    color: #4E4783;
    font-family: 'Helvetica', Arial, sans-serif;
    float: left;
    width: 100%;
    text-transform: capitalize;
}

.inbox-message h2{
    font-weight: bold;
    font-size: 18px;
    color: #4E4783;
    font-family: 'Helvetica', Arial, sans-serif;
    margin-top: 10px;
}


.inbox-message p{
    font-size: 14px;
    font-family: 'Helvetica', Arial, sans-serif;
    color: #494949;
    font-weight: normal;
    text-align: justify;
    line-height: 1.3;
}

.inbox-message__copy a{
    color: #D050D0;
    font-weight: normal;
    text-decoration: none;
}

.inbox-message__copy a:hover{
    color: #B746B7;
    font-weight: normal;
    text-decoration: none;
}

.inbox-message__image {
    height: auto;
    margin: 0 auto; 
}

.inbox-message__image img{
    width: 100%;
    height: auto;
}

.inbox-message__cta-container {
   	height: auto;
    margin: 0 auto; 
}

.inbox-message__cta {
	margin: 0 auto; 
	margin-top: 20px;
	width: 50%;
    height: 9%;
    border-radius: 50px;
    border: 1px solid #20C9DE;
    background-color: #04cde8;
    color: #ffffff;
    font-size: 0.8em;
    padding: 1em;
    display: block;
    text-align: center;
    font-family: "uni_sans_bold", Helvetica, Arial, Verdana, sans-serif;
}


.inbox-message__cta a{
    color: #011558;
    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.35);
    text-decoration: none;
    font-weight: bold;
    width: 80%;
}

.inbox-message__cta a:hover{
    background-color: #06BAD2;
}
</style>

<body>
<!-- MESSAGE TEMPLATE A -->
<div class="inbox-message-main">
<div class="inbox-message">
<h1>TITLE MESSAGE<h1>
<h2>Hi %%Alias%%</h2>
</div>
<div class="inbox-message__image"><img src="http://mailers.communicatormail.com/magicalvegas/images/enjoy-100-600x150.jpg" style="max-width: 100%;" alt=""></div>
<div class="inbox-message"><p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. <a data-bind="visible: validSession()" href="#">Vivamus</a> elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p></div>
<div class="inbox-message-cta-container">
	<button class="inbox-message__cta"><a data-bind="visible: validSession()" href="/start/">PLAY NOW</a></button>
</div>
</div>
</body>
</html>