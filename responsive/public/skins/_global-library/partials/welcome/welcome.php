<div class="welcome-post-reg__modal">

    <!-- TOP HEADER CLOSE MODAL -->
    <a href="/start/" data-gtag="Welcome Start" target="_top" >
      <div class="header-wrap">
        <div class="header-container">
          <div class="logo"></div>
            <p class="header-username">Welcome <span data-bind="text: username">...</span></p>
        </div>
      </div>
    </a>

    <!-- MIDDLE CONTENT -->
    <div class="main-content welcome-post-reg__content">
      <!--     <div class="inline_logo"></div> -->
    <div class="post-reg-welcome">
      <?php   
              include "_global-library/partials/welcome/welcome-bonus-offers.php";
              include "_global-library/partials/welcome/welcome-deposit-offers.php";
      ?>
    </div>
     <?php   
            /*   if (isset($action) && $action == 'cashier') {
                include '_global-library/partials/cashier/cashier.php';
              } */
      ?>     

              
        <?php /*if(isset($action) && $action == 'cashier') : ?>
        <!-- MIDDLE CONTENT -->
          <div class="middle-content">
            <h2 id="welcome-cashier" style="color:white; text-align:center; margin-bottom:20px;">AND VISIT THE CASHIER RIGHT HERE:</h2>
            <div class="middle-content__box">
              <section class="section" style="position:relative;">
                <h1 class="section__title">Cashier</h1>
                <div style="position:absolute !important;" class="cashier__secure-payment"><img src="/_images/cashier/secure-payment.png"></div>
                <!-- CONTENT -->
                <div class="content-template">
                  
                  <!-- PRD CASHIER -->
                  <script type="application/javascript">
                  skinModules.push( { id: "Cashier", options: { source: "Cas" } } );
                  </script>
                  <?php include '_global-library/partials/cashier/cashier.php'; ?>
                  <!-- /PRD CASHIER -->
                </div>
              </section>
            </div>
            <!-- /CONTENT BOX-->
          </div>
          <!-- END MIDDLE CONTENT -->
      <?php else : ?>
              <a href="/cashier/" data-gtag="Welcome Footer Cashier Link" class="no-bonus-link">You can also choose to play without taking a bonus. Find this option in the <span>cashier.</span></a>
      <?php endif; */?>

    <!-- END MIDDLE CONTENT -->
    

  </div>
  <div class="cashier__welcome-wrapper">
    <div class="middle-content">
      <?php edit($this->controller,'welcome-content',@$this->action,'margin-top:300px;margin-left:15%'); ?>
          <?php 
          @$this->getPartial($this->content['welcome-content'],$this->action); ?> 
    </div>
  </div>
  
