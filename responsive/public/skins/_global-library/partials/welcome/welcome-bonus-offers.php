<!-- BONUSES -->
<?php
$bonusCount = isset($this->content['welcome-offers']) ? count($this->content['welcome-offers']) : 0;
?>

<section class="bonus">

    <?php if ($bonusCount > 0): ?>

        <h2>
	        <?php edit($this->controller,'welcome-bonus-offers-title',@$this->action, 'margin-top:-35px;'); ?>
	        <?php @$this->repeatData($this->content['welcome-bonus-offers-title']); ?>
        </h2>

    <?php endif; ?>

    <ul>
        <?php edit($this->controller,'welcome-offers',@$this->action); ?>
        <?php @$this->repeatData($this->content['welcome-offers']); ?>
    </ul>

</section>
