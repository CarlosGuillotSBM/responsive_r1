
<!-- welcome post reg modal -->
<?php // include "_global-library/partials/welcome/welcome.php"; ?>
<!-- /welcome post reg modal -->


<div class="welcome-post-reg__modal__wrap">

	<div class="welcome-post-reg__modal single-offer">

			<!-- TOP HEADER CLOSE MODAL -->
			<a href="/start/" data-gtag="Welcome Start" target="_top" >
				<div class="header-wrap">
					<div class="header-container">
						<div class="logo"></div>
						<!-- <p class="header-username">Welcome <span data-bind="text: username">...</span>, thanks for joining!</p> -->
						<img class="close" src="/_images/common/icons/welcome-close.svg" alt="">
					</div>
				</div>
			</a>

			<!-- MIDDLE CONTENT -->
			<div class="main-content welcome-post-reg__content">
				<!-- BONUSES -->
				<?php
				$bonusCount = isset($this->content['welcome-offers']) ? count($this->content['welcome-offers']) : 0;
				?>

				<section class="bonus">

				    <h2 class="header-username">Welcome <span data-bind="text: username">...</span>, thanks for joining!</h2>
				    <?php if ($bonusCount > 0): ?>
				            <h4>        <?php edit($this->controller,'text',@$this->action); ?>
				        <?php @$this->repeatData($this->content['text']); ?></h4>

				    <?php endif; ?>

				    <ul>
				        <?php edit($this->controller,'welcome-offers',@$this->action); ?>
				        <?php @$this->repeatData($this->content['welcome-offers']); ?>
				    </ul>

				</section>

				<a class="welcome-post-reg__modal__continue" href="/cashier/">You can also choose to play without taking a bonus. Find this option in the cashier.</a>
				
			</div>

			

		<!-- END MIDDLE CONTENT -->
	</div>

</div>

