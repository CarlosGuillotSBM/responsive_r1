
<!-- welcome post reg modal -->
<?php // include "_global-library/partials/welcome/welcome.php"; ?>
<!-- /welcome post reg modal -->

<?php
	$bonusCount = isset($this->content['welcome-offers']) ? count($this->content['welcome-offers']) : 0;
?>

<div class="welcome-post-reg__modal__wrap" onLoad="">
	<div class="welcome-post-reg__modal">
		<!-- TOP HEADER CLOSE MODAL -->
		<a href="/start/" data-gtag="Welcome Start" target="_top" >
			<div class="header-wrap">
				<div class="header-container">
					<div class="logo"></div>
					<p class="header-username">Welcome <span data-bind="text: username">...</span>!</p>
					<img class="close" src="/_images/common/icons/welcome-close.svg" alt="">
				</div>
			</div>
		</a>

		<!-- MIDDLE CONTENT -->
		<div class="main-content welcome-post-reg__content">
			<!-- BONUSES -->
			<section class="bonus">
				<?php if ($bonusCount > 0): 
					edit($this->controller,'welcome-bonus-offers-title', @$this->action); 
					?>
					<h2>
						<?php @$this->repeatData($this->content['welcome-bonus-offers-title']); ?>
					</h2>
				<?php endif; ?>

				<ul>
					<?php edit($this->controller,'welcome-offers',@$this->action); ?>
					<?php @$this->repeatData($this->content['welcome-offers'], $this->action); ?>
				</ul>
			</section>

			<script type="application/javascript">
				skinModules.push( { id: "CashierPRD", options: { source: "PRD"} });
			</script>
			<!-- OFFERS -->
			<section class="deposit" data-bind="visible: CashierPRD.visible" style="display: none">
				<h2>
					<?php edit($this->controller,'welcome-deposit-offers-title',@$this->action); ?>
					<?php @$this->repeatData($this->content['welcome-deposit-offers-title'],$this->action); ?>
				</h2>
				<ul data-bind="foreach: CashierPRD.promos">
					<!-- 1st offer -->
					<li>
						<div class="claim-wrap">
							<div class="claim-container">
								<span data-bind="text: Description"></span>
							</div>
							<a href="/cashier/" class="deposit-cta" data-gtag="Welcome Left Deposit">DEPOSIT TO CLAIM</a>
						</div>
						<div class="icon">
							<span class="betty">
								<img src="/_images/welcome/deposit-offer-icon.png" alt="">
							</span>
						</div>
					</li>
				</ul>
			</section>
			<!-- Please don't ask me why this is required -->
			&nbsp; 
		</div>

		<div class="cashier__welcome-wrapper">
   	<div class="middle-content">
			<?php edit($this->controller,'welcome-content',@$this->action,'margin-top:120px;'); ?>
					<?php @$this->repeatData($this->content['welcome-content']); ?>
    </div>
   </div>
	</div>	
</div>
