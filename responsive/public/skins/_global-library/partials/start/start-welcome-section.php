<!-- Welcome Box -->
<div class="lobby-wrap__content-box">
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header hideme">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-account.php'; ?>
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text">WELCOME <span data-bind="text: username"></span></div>

            <div class="title__link"></div>

            <!-- Mobile Message Inbox -->
            <?php if(config("inbox-msg-on") && config("SkinID") !== 8): ?>
            <a data-hijack="true" href="/start/" title="Message Inbox" class="title__message-inbox" data-bind="visible: MailInbox.unreadMessages() > 0">
                <span class="title__message-inbox__icon"></span>
                <span class="title__message-inbox__value" data-bind="text: MailInbox.unreadMessages()"></span>
            </a>
            <?php endif; ?>
        </div>
    </div>
    <!-- end header bar -->
    <!-- Mobile version of cashback -->
    <?php include'start-claim-cashback--mobile.php'; ?>
    <?php include'start-claim-upgrade-bonus--mobile.php'; ?>
    <div class="lobby-wrap__content-box__content">
        <div class="small-play-wrap">
            <div class="lobby-play-button"> <a href="/games/">PLAY GAMES</a></div>
        </div>
        <div class="small-deposit-wrap">
            <div class="lobby-deposit-button"> <a href="/cashier/">DEPOSIT NOW</a></div>
        </div>

        <div class="lobby-wrap__content-box__content__innerwrap--border">
            <!--  make this content a partial -->
            <!-- welcome area -->

            <div class="welcome-area">
                <div class="welcome-area__top">

                    <div class="welcome-area__top__right">
                        <!-- VIP level slider not being used yet-->
                        <?php // include'_global-library/partials/start/start-current-vip-slider.php'; ?>
                        <!-- VIP current level -->
                        <?php include'_partials/common/current-vip-level.php'; ?>


                    </div>
                    <div class="welcome-area__top__left ui-scheduled-content-container">

                        <?php edit($this->controller,'start-intro-text'); ?>
                        <article class="welcome-intro">
                            <?php @$this->repeatData($this->content['start-intro-text']);?>
                        </article>

                    </div>

                </div>
                <div class="welcome-area__bottom  ui-scheduled-content-container">
                    <!-- email banner image goes here -->
                    <!--     <img class="welcome-banner" src="http://placehold.it/693x142?text=Offer+Image"> -->
                    <!-- Lobby banner -->
                    <?php edit($this->controller,'start-banner'); ?>
                    <?php @$this->repeatData($this->content['start-banner']);?>
                    <!-- /Lobby banner -->

                </div>

            </div>

            <!-- end welcome -->
            <!-- end make this content a partial -->
        </div>
    </div>
</div>
<!-- End Welcome Box -->
