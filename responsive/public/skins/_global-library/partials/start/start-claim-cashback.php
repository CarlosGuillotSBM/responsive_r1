<div style="display:none" data-bind="visible: ClaimCashback.cbID() != 0">
	<div  class="lobby-claim-cashback" data-bind="visible: ClaimCashback.cbAmount() > 0">
		<div class="upgrade-info">
            <a href="#" data-bind="click: ClaimCashback.showInfo.bind($data, playerClass())">T&amp;Cs apply</a>
        </div>
		<p class="upgrade-text">
			<!-- ASPERS -->
			<?php if (config("SkinID") == 12 ) : ?>
				You have a
					<span data-bind="text: ClaimCashback.currency"></span>
					<span data-bind="text: ClaimCashback.cbAmount"></span>
					Loyalty Reward waiting.
			<?php endif;?>

			<!-- GBB, RW & LVIP -->
			<?php if (config("SkinID") == 9 || config("SkinID") == 10 || config("SkinID") == 8 ) : ?>
				You have a
					<span data-bind="text: ClaimCashback.currency"></span>
					<span data-bind="text: ClaimCashback.cbAmount"></span>
					<span style="display: none" data-bind="visible: validSession() && (playerClass() == 2 || playerClass() == 3 || playerClass() == 4 || playerClass() == 5)">Loyalty Reward waiting.</span>
			<?php endif;?>
			
			<!-- LP, BE & KB -->
			<?php if (config("SkinID") == 3 || config("SkinID") == 6 || config("SkinID") == 2 ) : ?>
				You have a
					<span data-bind="text: ClaimCashback.currency"></span>
					<span data-bind="text: ClaimCashback.cbAmount"></span>
					<span style="display: none" data-bind="visible: validSession() && playerClass() == 3">Loyalty Reward waiting.</span>
					<span style="display: none" data-bind="visible: validSession() && (playerClass() == 4 || playerClass() == 5)">Loyalty Reward waiting.</span>
			<?php endif;?>
			
			<!--MV & SAW-->
			<?php if (config("SkinID") == 5 || config("SkinID") == 1 ) : ?>
				You have a
					<span data-bind="text: ClaimCashback.currency"></span>
					<span data-bind="text: ClaimCashback.cbAmount"></span>	
					<span style="display: none" data-bind="visible: validSession() && (playerClass() == 3 || playerClass() == 4 || playerClass() == 5 || playerClass() == 6)">Loyalty Reward waiting.</span>
			<?php endif;?>
		</p>
		



        <p class="upgrade-expiry" data-bind="visible: ClaimCashback.lastDays">
			Expires on <span data-bind="text: ClaimCashback.cbDate"></span>.
		</p>

        <a>
			<div class="lobby-claim-cashback-button" data-bind="click: ClaimCashback.claimPlayerCashback"> CLAIM NOW</div>
		</a>
    </div>

	<!--Message displayed after cashback/bonusback is redeemed-->
	<div class="lobby-claim-cashback alert-box secondary" data-bind="visible: ClaimCashback.cbClaimAmount() > 0">
		<a href="#" class="close" data-bind="click: ClaimCashback.hidePanel">&times;</a>
		<!--  ASPERS  -->
		<?php if (config("SkinID") == 12 ) : ?>
			<h1>Loyalty Reward Redeemed</h1>
		<?php endif;?>
		
		<!-- GBB, RW & LVIP -->
		<?php if (config("SkinID") == 9 || config("SkinID") == 10 || config("SkinID") == 8 ) : ?>
			<h1 style="display: none" data-bind="visible: validSession() && (playerClass() == 2 || playerClass() == 3 || playerClass() == 4 || playerClass() == 5)">Reward Redeemed!</h1>
		<?php endif;?>
		
		<!-- LP, BE & KB -->
		<?php if (config("SkinID") == 3 || config("SkinID") == 6 || config("SkinID") == 2 ) : ?>
			<h1 class="test desktop" style="display: none" data-bind="visible: validSession() && playerClass() == 3">Reward Redeemed!</h1>
			<h1 style="display: none" data-bind="visible: validSession() && (playerClass() == 4 || playerClass() == 5)">Reward Redeemed!</h1>
		<?php endif;?>
		
		<!--MV & SAW-->
		<?php if (config("SkinID") == 5 || config("SkinID") == 1 ) : ?>
			<h1 style="display: none" data-bind="visible: validSession() && (playerClass() == 3 || playerClass() == 4 || playerClass() == 5 || playerClass() == 6)">Reward Redeemed!</h1>
		<?php endif;?>
    </div>
</div>