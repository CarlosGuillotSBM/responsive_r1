<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 19.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns"
	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-273 396.9 48 48"
	 style="enable-background:new -273 396.9 48 48;" xml:space="preserve"
	 width="40px" height="40px">
<style type="text/css">
	.st0{fill:none;stroke:#333333;stroke-width:2;stroke-linecap:round;}
</style>
<title>close</title>
<desc>Created with Sketch.</desc>
<g id="close" transform="translate(229.000000, 24.000000) rotate(-270.000000) translate(-229.000000, -24.000000) translate(205.000000, 0.000000)" sketch:type="MSShapeGroup">
	<path id="line" class="st0" d="M429.9,510.4l-18-18.4"/>
	<path id="line_1_" class="st0" d="M411.9,510.4l18-18.4"/>
</g>
</svg>
