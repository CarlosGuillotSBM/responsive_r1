<!-- Loyalty-slider disable at moment until required-->
<div class="vip-level">

    <div class="vip-level__current">Your Vip Level</div>

    <div class="vip-level__progress-bar-outer">
        <div class="vip-level__progress-bar-inner" data-bind="style: {width: PlayerAccount.vipLevelWidth}, css: PlayerAccount.vipLevel" ></div>
    </div>
    <div class="vip-level__next"><span data-bind="visible: PlayerAccount.nextVipLevel() !=='', text: 'Next: ' + PlayerAccount.nextVipLevel()"></span></div>


</div>

