<!-- make class level-3 dynamic and the word gold -->
<div class="vip-level-area">
	<span class="vip-text">Your VIP Level:</span>
	
	<a href="/vip/" data-hijack="true"><span class="vip-shape" data-bind="text: vipLevel(), css: 'level-' + playerClass()"></span>
	<span class="vip-level-area-icon"></span></a>
</div>