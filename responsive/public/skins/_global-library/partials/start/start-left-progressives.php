<?php
$titleHeaderHeight = 25;
$slideHeight = 95;
$slidesToShow = 3;
?>
<script type="application/javascript">
skinPlugins.push({
id: "ProgressiveSlider",
options: {
slidesToShow: <?php echo $slidesToShow ?>,
mode: "vertical"
}
});
</script>
<!-- prog jackpots -->
<div class="progressive-jackpots__wrapper slider">
    <img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
    <article class="progressive-jackpots" style="height: <?php echo ($slidesToShow * $slideHeight + $titleHeaderHeight). 'px' ?> ; overflow: hidden">
        
        <div class="progressive-jackpots__title">
            <h1>Progressive Jackpots</h1>
        </div>
        <?php edit($this->controller,'progressives'); ?>
        <?php @$this->repeatData($this->content['progressives']);?>
    </article>
</div>
<!-- / prog jackpots -->