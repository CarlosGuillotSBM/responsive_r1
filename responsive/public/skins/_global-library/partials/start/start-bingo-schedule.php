<div class="lobby-wrap__content-box">
    
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <?php include 'icon-bingo.php'; ?>
            </div>
            <div class="title__text">BINGO SCHEDULE</div>
            <div class="title__link">
               <?php  include'_global-library/partials/bingo/bingo-play-now-text.php'; ?>
            </div>
        </div>
    </div>

    <!-- bingo schedule -->
    <div class="lobby-wrap__content-box__content lobby-wrap__content-box__content__bingo-schedule">
        <?php  include'_partials/bingo-schedule/bingo-schedule.php'; ?>
    </div>

</div>
