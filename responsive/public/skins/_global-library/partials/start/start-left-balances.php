<ul class="balanceboxes__wrapper">
    <!-- balances -->
    <?php
    // TODO : refactor this
    $realMoney = "Real Money";
    $bonusMoney = "Bonus Money";
    $freeSpins = "Free Spins";
    $freeSpinsLink = "/slots/exclusive/";
    $points = "Points";
    $loyaltyPoints = "/lucky-club/";
    if (config("SkinID") == 6) {
        $realMoney = "Real Balance";
        $bonusMoney = "Bonus Balance";
        $freeSpins = "Free Spins Balance";
        $points = "Loyalty Points";
    } else if (config("SkinID") == 5) {
        $points = "Moolahs";
    } else if (config("SkinID") == Skin::GiveBackBingo) {
        $freeSpinsLink = "/slot-games/exclusive-games/";
        $loyaltyPoints = "/vip-club/";
    }
    else if (config("SkinID")==2){
        $loyaltyPoints = "/kitty-club/";
    }
    ?>

    <li class="free-cards">Free Cards Total<a data-bind="click: playBingo"><span data-bind="text: PlayerAccount.cards" class="bonuswins_amount ui-lbBonusSpins">...</span></a></li>
    <li class="free-spins">Free Spins Total<a href="<?php echo $freeSpinsLink?>" data-hijack="true"><span data-bind="text: bonusSpins" class="bonuswins_amount ui-lbBonusSpins">...</span></a></li>
    <li class="loyalty-bonus">Loyalty Points<a href="<?php echo $loyaltyPoints?>" data-hijack="true"><span data-bind="text: PlayerAccount.points" class="bonuswins_amount ui-lbBonusSpins">...</span></a></li>
    <li class="my-cashback" style="display:none" data-bind="visible: isCashbackVisible()">My Cashback<span data-bind="text: PlayerAccount.cashback" class="balance_amount ui-lbBalance">...</span></li>

</ul>

<div class="lobby-wrap__left__terms">
    <p>
        <a class="promotional-tcs" href="/terms-and-conditions/general-promotional-terms-and-conditions/">
            Freebie T&amp;Cs apply
        </a>
    </p>
</div>
<!-- Fix for IE9 balanceboxes bug -->
<!--[if IE 9]>
<link rel="stylesheet" type="text/css" href="../_css/ie9fix.css" />
<![endif]-->