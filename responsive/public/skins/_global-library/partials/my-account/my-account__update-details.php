<!-- MAIN CONTENT AREA -->
<div id="content" class="update-details">

	<!-- TOP CONTENT -->
	<div class="row">
		<!-- BREADCRUMPS -->
		<div class="large-9 columns">
			<?php include '_global-library/widgets/breadcrumbs.php'; ?>
		</div>
		<!-- END BREADCRUMPS -->
		<!-- SOCIAL -->
		<div class="large-3 columns">
			<?php include '_global-library/social-share-links.php'; ?>
		</div>
		<!-- END SOCIAL -->
	</div>
	<!-- END TOP CONENT -->

	<!-- MY-ACCOUNT BODY -->
	<div class="update-details__body three-columns-container">

		<!-- LEFT COLUMN -->
		<div class="three-columns-container__left">
			<!-- MY ACCOUNT NAV -->
			<?php include '_global-library/partials/my-account/my-account__side-nav.php'; ?>
			<!-- END MY ACCOUNT NAV -->
		</div>
		<!-- END LEFT COLUMN -->

		<!-- MIDDLE COLUMN -->
		<div class="three-columns-container__middle">
			<?php include '_global-library/partials/my-account/my-account__update-details__form.php'; ?>
		</div>
		<!-- END MIDDLE COLUMN -->

		<!-- RIGHT COLUMN -->
		<div class="three-columns-container__right">
			<!-- Banner -->
			<?php include '_global-library/banners/my-account__banner-1.php' ?>
			<!-- end banner -->
		</div>
		<!-- END RIGHT COLUMN -->

	</div>
	<!-- END MY-ACCOUNT BODY -->

	<!-- END MAIN CONTENT AREA -->
</div>