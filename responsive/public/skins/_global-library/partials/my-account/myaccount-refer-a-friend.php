
<!--
<div>
    <div class="myaccount-details__column">
        <div>
            <label>Wager limit for 1 session</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.wagerPerSession" type="text" maxlength="50"></select>
        </div>
        <div>
            <label>Wager limit for a day</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.wagerPerDay" type="text" maxlength="50"></select>
        </div>
        <div>
            <label>Wager limit for a week</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.wagerPerWeek" type="text" maxlength="100"></select>
        </div>
    </div>

    <div class="myaccount-details__column">
        <div>
            <label>Wager limit for a month</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.wagerPerMonth" type="text" maxlength="50"></select>
        </div>
        <div>
            <label>Loss limit for a session</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.lossPerSession" type="text" maxlength="50"></select>
        </div>
        <div>
            <label>Loss limit for a day</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.lossPerDay" type="text" maxlength="100"></select>
        </div>
    </div>

    <div class="myaccount-details__column">
        <div>
            <label>Loss limit for a week</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.lossPerWeek" type="text" maxlength="50"></select>
        </div>
        <div>
            <label>Loss limit for a month</label>
            <select data-bind="options:PlayerLimits.values, value: PlayerLimits.lossPerMonth" type="text" maxlength="50"></select>
        </div>
        <a data-bind="click: PlayerLimits.updateLimits" href="" class="button expand">Update Limits</a>
        <p> <label>Please note: You can only increase your limits once per day.</label></p>
    </div>

   
</div>
-->
<div class="refer-a-friend">

        <div>
            <label>Write the email of the friend you want to invite</label>
            <input data-bind="value: PlayerReferAFriend.emails" type="text" maxlength="100">
        </div>
</div>

<hr>
		<div class="myaccount-limits__details"> 
			<a data-bind="click: PlayerReferAFriend.sendInvitations" href="" class="button expand">Send Mail</a>
		</div>


        <div>
            <label><strong>Copy this link wherever you want to invite your friends:</strong> <p></p><p data-bind="text: PlayerReferAFriend.playerUrl"></p>

            </label>
        </div>

		<div class="myaccount-limits__note">
			<label><strong>Please note:</strong> Refer as many friends as you can.

			</label>
                            <label>Total Registered: </label> <span data-bind="text: PlayerReferAFriend.totalRegistered"></span> 
                            <label>Total Completed: </label> <span data-bind="text: PlayerReferAFriend.totalCompleted"> Yes</span>
                            <label>Earned So Far: </label> <span data-bind="text: PlayerReferAFriend.earnedSoFar"></span> 
                            <label>Potential Earnings: </label> <span data-bind="text: PlayerReferAFriend.potentialEarnings"></span> 
		</div>

         <!-- Referrals -->

        <div class="myaccount-transactions__promotions" >

            <ul data-bind="foreach: PlayerReferAFriend.friends">
                <li class="myaccount-transactions__promotions__item">
                    <div class="myaccount-transactions__promotions__item__content">
                        <div>
                            <label>Name: </label> <span data-bind="text: Name"></span> 
                            <label>Deposited: </label> <span data-bind="text: Deposited"> Yes</span>
                            <label>Wagered: </label> <span data-bind="text: WageredSoFar"></span> 
                            <label>Prize: </label> <span data-bind="text: Prize"></span> 
                        </div>
                    </div>
                </li>
            </ul>

        </div>
        <!-- /Referrals -->


         <!-- References -->

        <div class="myaccount-transactions__promotions" >
            <label>Referees (by mail): </label>
            <ul data-bind="foreach: PlayerReferAFriend.referrals">
                <li class="myaccount-transactions__promotions__item">
                    <div class="myaccount-transactions__promotions__item__content">
                        <div>
                            <label>Email: </label> <span data-bind="text: Email"></span> 
                            <label>Date Sent: </label> <span data-bind="text: SentDate"> Yes</span>
                            <label>Registered with you?: </label> <span data-bind="text: Status"></span> 
                            <label>Can send again: </label> <span data-bind="text: CanSendAgain"></span> 
                        </div>
                    </div>
                </li>
            </ul>

        </div>
        <!-- /References -->








<!-- 
<div>
    <div class="myaccount-details__column">

        <div>
            <label>Wager limit for 1 session</label>
            <input data-bind="value: PlayerLimits.wagerPerSession" type="text" maxlength="50">
        </div>
        <div>
            <label>Wager limit for a day</label>
            <input data-bind="value: PlayerLimits.wagerPerDay" type="text" maxlength="50">
        </div>
        <div>
            <label>Wager limit for a week</label>
            <input data-bind="value: PlayerLimits.wagerPerWeek" type="text" maxlength="100">
        </div>
    </div>

    <div class="myaccount-details__column">
        <div>
            <label>Wager limit for a month</label>
            <input data-bind="value: PlayerLimits.wagerPerMonth" type="text" maxlength="50">
        </div>
        <div>
            <label>Loss limit for a session</label>
            <input data-bind="value: PlayerLimits.lossPerSession" type="text" maxlength="50">
        </div>
        <div>
            <label>Loss limit for a day</label>
            <input data-bind="value: PlayerLimits.lossPerDay" type="text" maxlength="100">
        </div>
    </div>

    <div class="myaccount-details__column">
        <div>
            <label>Loss limit for a week</label>
            <input data-bind="value: PlayerLimits.lossPerWeek" type="text" maxlength="50">
        </div>
        <div>
            <label>Loss limit for a month</label>
            <input data-bind="value: PlayerLimits.lossPerMonth" type="text" maxlength="50">
        </div>
        <a data-bind="click: PlayerLimits.updateLimits" href="" class="button expand">Update Limits</a>
        <p> <label>Please note: You can only increase your limits once per day.</label></p>
    </div>

   
</div> -->

