<div class='myaccount-transactions columns small-12'>
<h3 class="myaccount-transactions__header" style="display:none">Your Account Transactions</h3>
    <ul class='block-grid-small-4' id="activity_options">
        <li> <a data-bind="click: AccountActivity.showGame, css: { 'current' : AccountActivity.filter() == 'games' }"
        href="" data-nohijack="true"> Game </a> </li>
        <li> <a id="bankingTab" data-bind="click: AccountActivity.showBanking, css: { 'current' : AccountActivity.filter() == 'cashier' }"
        href="" data-nohijack="true"> Banking </a> </li>
        <li> <a data-bind="click: AccountActivity.showPromo, css: { 'current' : AccountActivity.filter() == 'promos' }" href="" data-nohijack="true"> Promo </a> </li>
    </ul>

    <div id="details-container">
    
         <!-- Tabs within Banking tab (My transactions and My deposits) -->
        <div data-bind= "visible: AccountActivity.bankingVisible() || AccountActivity.depositVisible()">
            <ul class='block-grid-small-4 second-tabs' id="activity_options">
                <li> <a class="banking-tab" data-bind="click: AccountActivity.showBanking"
                href="" data-nohijack="true"> My transactions </a> </li>
                <li> <a class="banking-tab" data-bind="click: AccountActivity.showDeposit" href="" data-nohijack="true"> My deposit</a> </li>
            </ul>
        </div>
        <a class="green-cta" data-bind="click: AccountActivity.refresh,
            visible: AccountActivity.gameVisible() || AccountActivity.promoVisible()"
        href="" data-nohijack="true"> Refresh </a>

        <!-- filters for game and banking activity -->
        <div data-bind="visible: AccountActivity.gameVisible() || AccountActivity.bankingVisible()" class="myaccount-date-filter">

            <!--Datepicker Start date-->
            <p class="date-selector_left">
                <strong>Date from:</strong>
                <input id="fromDate" data-bind="value: AccountActivity.filterDate" placeholder="Select a start date" class="textField ui-date-picker" type="text">
            </p>
            <!--Datepicker End date-->
            <p class="date-selector_right">
                <strong>Date to:</strong>
                <input id="toDate" data-bind="value: AccountActivity.filterEndDate" placeholder="Select an end date" class="textField ui-date-picker" type="text" disabled>
            </p>
            <p style="float: left; width: 100%;">
                <strong>Filter:</strong>
                <input data-bind="value: AccountActivity.filterString, valueUpdate: ['input', 'afterkeydown']" placeholder="Filter the records from the table" class="textField" type="text">
            </p>
        </div>
        <!-- Game -->
        <div data-bind="visible: AccountActivity.gameVisible">
         <table class="responsive">
                <thead>
                    <td>Date</td>
                    <td>Type</td>
                    <td>Details</td>
                    <td>Amount</td>
                    <td>Tx ID</td>
                    <td>TxReference</td>
                </thead>
                <tbody data-bind="foreach: AccountActivity.gameRecords">
                    <tr>
                        <td data-bind="text: Date"></td>
                        <td data-bind="text: txType"></td>
                        <td data-bind="text: TxDetails"></td>
                        <td data-bind="text: Amount"></td>
                        <td data-bind="text: txPlayerBalanceID"></td>
                        <td data-bind="text: Ref"></td>
                    </tr>
                </tbody>
            </table>
           
           <!-- PlayerTransactionsSummary - GAMES -->
           <div data-bind="visible: AccountActivity.shouldShowTransactions"> 
               <div class="info-results">
               <ul class="activity-results">
                    <li>Total Wagers:<span data-bind="text: AccountActivity.gameTotalWagers"></span></li> 
                    <li>Total Wins: <span data-bind="text: AccountActivity.gameTotalWins"></span></li>
                    <li>Net Wins/Loss: <span data-bind="text: AccountActivity.gameTotalWinsLoss"></span></li>
                </ul>
           </div>

    
            <div class="disclaimer-results">
                <small class="results-info">* Please note, it can take up to 1 hour for these figures to update.</small>
            </div>
           </div>
           <!-- PlayerTransactionsSummary - GAMES -->
           
             
            <span data-bind="text: AccountActivity.noRecords"></span>
            <!-- Pagination -->
            <ul class="pagination" data-bind="visible: !AccountActivity.noRecords()">
                <li data-bind="click: AccountActivity.getData.bind($data, 'first'),
                    css: { 'unavailable': !AccountActivity.canGoToFirst() }" >
                    <a href="" data-nohijack="true">|<</a>
                </li>
                <li data-bind="click: AccountActivity.getData.bind($data, 'previous'),
                    css: { 'unavailable': !AccountActivity.canGoToPrevious() }">
                    <a href="" data-nohijack="true"><</a>
                </li>
                <li data-bind="click: AccountActivity.getData.bind($data, 'next'),
                    css: { 'unavailable': !AccountActivity.canGoToNext() }">
                    <a href="" data-nohijack="true">></a>
                </li>
                <li data-bind="click: AccountActivity.getData.bind($data, 'last'),
                    css: { 'unavailable': !AccountActivity.canGoToLast() }">
                    <a href="" data-nohijack="true">>|</a>
                </li>
            </ul>
            <small><span data-bind="text: 'Showing page ' + AccountActivity.currentPage() + ' of ' + AccountActivity.totalPages(),
            visible: !AccountActivity.noRecords()"></span></small>
            <!-- /pagination -->
        </div>
        <!-- /Game -->

        <!-- Banking -->
        <div data-bind="visible: AccountActivity.bankingVisible">
        <table class="responsive">
                <thead>
                    <td>Date</td>
                    <td>Type</td>
                    <td>Details</td>
                    <td>Amount</td>
                    <td>Tx ID</td>
                </thead>
                <tbody data-bind="foreach: AccountActivity.bankingRecords">
                    <tr>
                        <td data-bind="text: Date"></td>
                        <td data-bind="text: txType"></td>
                        <td data-bind="text: TxDetails"></td>
                        <td data-bind="text: Amount"></td>
                        <td data-bind="text: txPlayerBalanceID"></td>
                    </tr>
                </tbody>
            </table>
            
            <!-- PlayerTransactionsSummary - BANKING -->
            <div data-bind="visible: AccountActivity.shouldShowTransactions">
                <div class="info-results">
                   <ul class="activity-results">
                        <li class="banking-totals">Total Deposits: <span data-bind="text: AccountActivity.bankingTotalDeposits"></span></li><!--data-bind="text: AccountActivity.bankingDeposits"-->
                        <li class="banking-totals">Total Withdrawals: <span data-bind="text: AccountActivity.bankingTotalWithdrawals"></span></li> <!--data-bind="text: AccountActivity.bankingWithdrawals"-->
                        <li class="banking-net-total">Net Total: <span data-bind="text: AccountActivity.bankingNetTotal"></span></li> <!--data-bind="text: AccountActivity.bankingNetTotal"-->
                    </ul>
               </div>

                <div class="disclaimer-results">
                    <small class="results-info">* Please note, it can take up to 1 hour for these figures to update.</small>
                </div>
            </div>
            <!-- PlayerTransactionsSummary - BANKING -->
            
 
            <span data-bind="text: AccountActivity.noRecords"></span>

            <!-- Pagination -->
            <ul class="pagination" data-bind="visible: !AccountActivity.noRecords()">
                <li data-bind="click: AccountActivity.getData.bind($data, 'first'),
                    css: { 'unavailable': !AccountActivity.canGoToFirst() }" >
                    <a href="" data-nohijack="true">|<</a>
                </li>
                <li data-bind="click: AccountActivity.getData.bind($data, 'previous'),
                    css: { 'unavailable': !AccountActivity.canGoToPrevious() }">
                    <a href="" data-nohijack="true"><</a>
                </li>
                <li data-bind="click: AccountActivity.getData.bind($data, 'next'),
                    css: { 'unavailable': !AccountActivity.canGoToNext() }">
                    <a href="" data-nohijack="true">></a>
                </li>
                <li data-bind="click: AccountActivity.getData.bind($data, 'last'),
                    css: { 'unavailable': !AccountActivity.canGoToLast() }">
                    <a href="" data-nohijack="true">>|</a>
                </li>
            </ul>
        </div>

        <!--Test Markup-->
        <div data-bind="visible: AccountActivity.depositVisible()" class="my-net-deposits">
            <div class="netDepositsWrapper" data-bind="visible: AccountActivity.shouldShowMessage">
                <p class="view-net-deposit">You can view your net deposit amount here.</p>
                <a class="button showResults" href="" data-nohijack="true" data-bind="click: AccountActivity.showNetDeposit">Calculate my Net Deposit</a>
            </div>

            <div class="netDepositsResultWrapper" data-bind="visible: AccountActivity.showResults">
                <p class="view-net-deposit">Your total net deposit amount is:</p>
                <a class="button btn-reveal__deposit inactiveLink" href="" data-bind="text: AccountActivity.netDeposit" data-nohijack="true"></a>
            </div>

            <p class="my-net-deposit__info">The net deposit amount displayed is based on your total deposit minus any processed withdrawals from 1st April 2018 onward.</p>
        </div>
        <!--Test Markup-->

        
        <!-- MY TRANSACTIONS -->
        <div data-bind="visible: AccountActivity.bankingVisible()" class="banking-tab">
            <!-- filters for banking activity -->

            <div data-bind="visible: AccountActivity.bankingVisible">
             
                <small><span data-bind="text: 'Showing page ' + AccountActivity.currentPage() + ' of ' + AccountActivity.totalPages(),
                visible: !AccountActivity.noRecords()"></span></small>
            </div>
        </div>
         <!-- /MY TRANSACTIONS -->
      
            <!-- Promo -->
            <div class="myaccount-transactions__promotions" data-bind="visible: AccountActivity.promoVisible">

                <ul data-bind="foreach: AccountActivity.promoRecords">
                    <li class="myaccount-transactions__promotions__item">
                        <div class="myaccount-transactions__promotions__item__content">
                            <div class="PromoDescription" data-bind="html: PromoDescription">
                            </div>
                            <a class="expandarrow" data-bind="click: $root.AccountActivity.getPromoFullDetail.bind($data, ID)">Details &#9662;</a>
                            <a class="voidbonus" data-bind="visible: Active, click: $root.AccountActivity.voidBonus.bind($data, ID)">CANCEL BONUS</a>
                        </div>
                        
                        <div class="myaccount-transactions__promotions__item__details" data-bind="visible: $root.AccountActivity.promoDetailToShow() == ID ">
                            <div class="panel">
                                <br>
                                <b>Status: </b> <span data-bind="text: Status"></span><br/>
                                <b>Date Given: </b> <span data-bind="text: DateGiven"></span><br/>
                                <b>Date Activated: </b> <span data-bind="text: DateActivated"></span><br/>
                                <b>Date Expires: </b> <span data-bind="text: DateExpires"></span><br/>
                                <b>Date Completed: </b> <span data-bind="text: DateCompleted"></span><br/>
                                <b>Bonus Given: </b> <span data-bind="text: BonusGiven"></span><br/>
                                <b>Wagers Required: </b> <span data-bind="text: WagersRequired"></span><br/>
                                <b>Wagers Applied: </b> <span data-bind="text: WagersApplied"></span><br/>
                                <b>Wagers To Go: </b> <span data-bind="text: WagersToGo"></span><br/>
                                <b>Complete Percent: </b> <span data-bind="text: DisplayPercent"></span><br/>
                                <div data-bind="visible: CompletePercent != '0%'" class="progress [small-# large-#] [secondary alert success] [radius round]">
                                    <span data-bind="attr: { 'style' : 'width: ' + CompletePercent }" class="meter" ></span>
                                </div>
                                <b>Convertible To Real: </b> <span data-bind="text: ConvertibleToReal"></span><br/>
                                <b>Games: </b> <span data-bind="text: Games"></span>
                                <span data-bind="text: MoreGames" style="display: none;"></span>
                                <a data-bind="visible: MoreGames, css: 'ui-show-more-' + ID" class="cta-join">SHOW MORE</a>
                            </div>
                         </div> 
                    </li>
                </ul>

            </div>
        <!-- /Promo -->
    </div>
</div>
<script>
    const bankingTab = document.getElementById('bankingTab');

    bankingTab.addEventListener('click', setTab);

    function setTab() {
        const tabs = document.getElementsByClassName('banking-tab');

        for (i = 0; i < tabs.length; i++) {
            // set first tab to active on load
            if (i == 0) {
                tabs[i].classList.add('active');
            }

            tabs[i].addEventListener('click', function (e) {
                e.preventDefault();

                // remove all active classes from tabs
                removeElementClass('banking-tab', 'active');

                // set clicked tab to active
                this.classList.toggle('active');
            });
        }
    }

    function removeElementClass(elementName, className) {
        const theElements = document.getElementsByClassName(elementName);

        for (i = 0; i < theElements.length; i++) {
            theElements[i].classList.remove(className);
        }
    }
</script>