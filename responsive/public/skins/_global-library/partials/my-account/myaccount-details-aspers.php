<!-- <div class="loader"><img src="../_imgages/loader/ajax-loader.gif"></div> -->
<div id="playerdetails">
    <h3 class="playerdetails__header">Personal Details</h3>

    <div class="myaccount-details__column">
        <div class="aspers__name--wrapper" style="float: left;">
            <label class="aspers__name">First name</label>
            <input data-bind="value: PersonalDetails.firstName" type="text" maxlength="100" disabled="disabled" class="aspers__email--input">
        </div>
        <div class="aspers__name--wrapper" style="float: left;">
            <label class="aspers__name">Last name</label>
            <input data-bind="value: PersonalDetails.lastName" type="text" maxlength="100" disabled="disabled" class="aspers__email--input">
        </div>
        <div class="aspers__email--wrapper">
            <label class="aspers__email">E-mail</label>
            <input data-bind="value: PersonalDetails.email" type="text" maxlength="100" disabled="disabled" class="aspers__email--input">
        </div>
    </div>
    
    <div class="myaccount-details__column">
        <div class="aspers__username--wrapper">
            <label class="aspers__username">Username</label>
            <!-- <p class="aspers__username--input" data-bind="text: username">...</p> -->
            <input data-bind="value: username" type="text" maxlength="100" disabled="disabled" class="aspers__username--input">
        </div>
         <div class="aspers__gender--wrapper">
            <label class="aspers__gender">Gender</label>
            <input data-bind="value: PersonalDetails.title" type="text" maxlength="50" disabled="disabled" class="aspers__gender--input">
        </div>
    </div>
    
    <div class="myaccount-details__column">
        <div class="row large-collapse">
            <div class="address__fields">
                <label class="aspers__address1">Address</label>
                <input data-bind="value: PersonalDetails.address1" type="text" maxlength="100" disabled="disabled" class="aspers__address1--input">
                <input data-bind="value: PersonalDetails.address2" type="text" maxlength="100" disabled="disabled" class="aspers__address2--input">
            </div>

            <div class="address__fields">
                <label class="aspers__address1">Postcode</label>
                <input data-bind="value: PersonalDetails.postcode" type="text" maxlength="15" disabled="disabled" class="aspers__address3--input">
            </div>

            <div class="address__fields">
                <label class="aspers__address1">County/State</label>
                <input data-bind="value: PersonalDetails.state" type="text" maxlength="50" disabled="disabled" class="aspers__address4--input">
            </div> 
        </div>
    </div>

    <div class="myaccount-details__column">
        <div class="row large-collapse">
            <div class="address__fields">
                <label class="aspers__address1">City</label>
                <input data-bind="value: PersonalDetails.city" type="text" maxlength="50" disabled="disabled" class="aspers__address5--input">
            </div>

            <div class="address__fields">
                <label class="aspers__address1">Country</label>
                <input data-bind="value: PersonalDetails.countryName" type="text" maxlength="50" disabled="disabled" class="aspers__address6--input">
            </div>

             <div class="support__copy">If you require any changes to the info above please contact <a href="/support/" class="support__copy--link">support</a></div>
        </div>
    </div>
   
    <!-- Password Reset -->
    <div class="myaccount-details__column aspers__password--wrapper">
        <div class="row large-collapse">
            <div class="small-12 medium-6 large-4 columns">
                <label class="aspers__newpassword">New Password</label>
                <input data-bind="value: PersonalDetails.newPassword" type="password" maxlength="15" class="aspers__newpassword--input">
                <span data-bind="visible: PersonalDetails.passwordsDontMatch" class="error">Passwords do not match</span>
                <span data-bind="visible: PersonalDetails.invalidNewPassword" class="error">Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces</span>
                <span data-bind="visible: PersonalDetails.UsernameAndPassTheSame" class="error">Username and Password cannot be the same</span>
            </div>
            <div class="small-12 medium-6 large-4 columns aspers__repeatnewpassword--wrapper">
                <label class="aspers__repeatnewpassword">Repeat New Password</label>
                <input data-bind="value: PersonalDetails.repeatPassword" type="password" maxlength="15" class="aspers__repeatnewpassword--input">
                <span data-bind="visible: PersonalDetails.passwordsDontMatch" class="error">Passwords do not match</span>
            </div>
            <div class="small-0 medium-0 large-4 columns"></div>
        </div>
    </div>
    <!-- / Password Reset -->

        <div class="row large-collapse">
            <div class="small-12 medium-6 large-4 columns">
                <label class="aspers__phone">Landline Phone</label>
                <input data-bind="value: PersonalDetails.landPhone" type="text" maxlength="50" class="aspers__phone--input">
                <span data-bind="visible: PersonalDetails.invalidLandPhone" class="error">Please specify a valid phone number</span>
            </div>
            <div class="small-12 medium-6 large-4 columns aspers__landiline--wrapper">
                <label class="aspers__phoneext">Landline ext</label>
                <input data-bind="value: PersonalDetails.landPhoneExt" type="text" maxlength="10" class="aspers__phoneext--input">
                <span data-bind="visible: PersonalDetails.invalidLandExtPhone" class="error">Please specify a valid phone number</span>
            </div>
            <div class="small-0 medium-0 large-4 columns"></div>
        </div>
      
            <div class="row large-collapse">
                <div class="small-12 medium-6 large-4 columns">
                    <label class="aspers__mobilephone">Mobile Phone</label>
                    <input id='ui-phone' data-bind="value: PersonalDetails.mobilePhone" type="text" maxlength="50" class="aspers__mobilephone--input">
                    <span data-bind="visible: PersonalDetails.invalidUkMobile" class="error">Mobile phone is 11 digits long, begins with 071 to 075 inclusive or 077 to 079 inclusive.</span>
                    <span data-bind="visible: PersonalDetails.invalidMobile" class="error">Please specify a valid phone number</span>
                </div>
                <div class="small-12 medium-6 large-4 columns">
                </div>
                <div class="small-0 medium-0 large-4 columns"></div>
                <div class="small-0 medium-0 large-4 columns"></div>
            </div>

            <!-- Preferences  -->
            <div class="myaccount-details__column">
                <div class="aspers__preferences_comm">
                    <div class="preferences__header">Preferences</div>
                    <?php edit($this->controller,'communications'); ?>
                    <?php @$this->repeatData($this->content['communications']);?>
                    <ul class="communications">
                       <!--  <li>
                            <label data-bind="css: {ticked: PersonalDetails.allComms}" for="switch__start--allcomms" class="switch__start--label2">
                                <input id="switch__start--allcomms" type="checkbox" data-bind="checked: PersonalDetails.allComms" id="allCommsCheck">
                                <span class="switch__start--label"></span>All Communications
                            </label>
                        </li> -->
                         <li>
                            <label data-bind="css: {ticked: PersonalDetails.allComms}" for="allCommsCheck">
                              <input data-bind="checked: PersonalDetails.allComms" type="checkbox" id="allCommsCheck">
                              <span class="custom checkbox"></span>
                              All Communications
                            </label>
                          </li>
                        <li style="margin-left: 20px;">
                           <label data-bind="css: {ticked: PersonalDetails.wantsEmail}" for="cbEmail">
                                <input data-bind="checked: PersonalDetails.wantsEmail" type="checkbox" id="cbEmail">
                                <span class="custom checkbox"></span>Email
                           </label>
                        </li>
                        <li style="margin-left: 20px;">
                            <label data-bind="css: {ticked: PersonalDetails.wantsSMS}" for="cbSms">
                                <input data-bind="checked: PersonalDetails.wantsSMS" type="checkbox" id="cbSms">
                                <span class="custom checkbox"></span>SMS
                            </label>
                        </li>
                        <li style="margin-left: 20px;">
                            <label data-bind="css: {ticked: PersonalDetails.wantsPhone}" for="cbPhone">
                                <input data-bind="checked: PersonalDetails.wantsPhone" type="checkbox" id="cbPhone">
                                <span class="custom checkbox"></span>Phone
                            </label>
                        </li>
                        <li style="margin-left: 20px;">
                            <label data-bind="css: {ticked: PersonalDetails.wantsMail}" for="cbPostMail">
                                <input data-bind="checked: PersonalDetails.wantsMail" type="checkbox" id="cbPostMail">
                                <span class="custom checkbox"></span>Post
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
        
            <div class="myaccount-details__column">
                 <div class="aspers__preferences_data">
                    <?php edit($this->controller,'data'); ?>
                    <?php @$this->repeatData($this->content['data']);?>

                    <div class="radio-selection">
                        <div>
                            <label for="aspersDataYes" class="radio-label">
                            <input id="aspersDataYes" name="yes" type="radio" value="Yes" data-bind="checked: PersonalDetails.dataAspers">Yes</label>
                        </div>

                        <div>
                            <label for="aspersDataNo" style="margin-left: 10px;" class="radio-label">
                            <input id="aspersDataNo" name="no" type="radio" value="No" data-bind="checked: PersonalDetails.dataAspers">No</label>
                        </div>
                    </div>
                    
                    <div data-bind="visible: PersonalDetails.showAspers">
                        <ul class="communications">
                            <li>
                               <label data-bind="css: {ticked: PersonalDetails.thirdPartyEmail}" for="thirdPartyEmail">
                                    <input data-bind="checked: PersonalDetails.thirdPartyEmail" type="checkbox" id="thirdPartyEmail">
                                    <span class="custom checkbox"></span>Email
                               </label>
                            </li>
                            <li>
                                <label data-bind="css: {ticked: PersonalDetails.thirdPartySMS}" for="thirdPartySMS">
                                    <input data-bind="checked: PersonalDetails.thirdPartySMS" type="checkbox" id="thirdPartySMS">
                                    <span class="custom checkbox"></span>SMS
                                </label>
                            </li>
                            <li>
                                <label data-bind="css: {ticked: PersonalDetails.thirdPartyPhone}" for="thirdPartyPhone">
                                    <input data-bind="checked: PersonalDetails.thirdPartyPhone" type="checkbox" id="thirdPartyPhone">
                                    <span class="custom checkbox"></span>Phone
                                </label>
                            </li>
                        </ul>
                    </div>
                 
                </div>
            </div>

            <!-- Provide Password and Update your details -->
            <div class="row large-collapse">
                <div class="small-12 medium-6 large-4 columns">
                    <label class="aspers__passlabel">Please provide your current password</label>
                    <input data-bind="value: PersonalDetails.currentPassword" type="password" maxlength="15">
                    <span data-bind="visible: PersonalDetails.invalidCurrentPassword" class="error">This field is required</span>
                    <a data-bind="click: PersonalDetails.updateDetails" href="" class="button expand aspersbutton__updatedetails">Update Details</a>
                </div>
            </div>
            

    <!--     </div>
    </div> -->
</div> <!-- Player Details -->
