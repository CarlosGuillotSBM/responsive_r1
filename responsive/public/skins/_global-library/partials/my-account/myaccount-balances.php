
<h3 class="myaccount-balances__header" style="display:none">Your Account Balances</h3>
<ul class="balanceboxes">
	<!-- convert points panel -->
	<div data-bind="visible: ConvertPoints.panelVisible" class="convertpanel" style="display: none">
		<a class="convertpanel__header" href="" style="display: none" data-bind="click: ConvertPoints.hidePanel">
			<span>Convert Loyalty Points</span>
		</a>
		<div class="convertpanel__content">
			<!-- Input -->
			<span data-bind="text: 'How many ' + ConvertPoints.pointsName() + ' do you want to convert?'"></span>
			<input data-bind="value: ConvertPoints.pointsToConvert, valueUpdate: ['input', 'keypress']" type="tel" placeholder="Minimum 1000"
			autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
			<span data-bind="visible: ConvertPoints.errorMsg, text: ConvertPoints.errorMsg" class="error"></span>
			<a data-bind="click: ConvertPoints.convert" href="#" class="convertpointsbtn">Convert</a>
			<!-- Close button -->
			<div class="ui-toggle-points-panel">
				<p> <a data-bind="click: ConvertPoints.hidePanel" href="" class="balancesbtn" >SHOW BALANCES</a></p>
			</div>
		</div>
	</div>

	<!-- balances -->

	<?php
	// TODO : refactor this
	$realMoney = "Real Money";
	$bonusMoney = "Bonus Money";
	$bonusWins = "Bonus Wins";
	$freeSpins = "Free Spins";
	$freeSpinsLink = "/slots/exclusive/";
	$points = "Points";
	$bonusBalance = "Bonus Balance";
	if (config("SkinID") == 6) {
		$realMoney = "Real Balance";
		// $bonusMoney = "Bonus Balance";
		$freeSpins = "Free Spins Balance";
		$points = "Loyalty Points";
	} else if (config("SkinID") == 5) {
		$points = "Moolahs";
	} else if (config("SkinID") == Skin::GiveBackBingo) {
		$freeSpinsLink = "/slot-games/exclusive-games";
	} else if (config("SkinID") == Skin::LuckyVIP) {
		$freeSpinsLink = "/slot-games/exclusive-games";
	} else if (config("SkinID") == Skin::RegalWins) {
		$freeSpinsLink = "/slot-games/exclusive-games";
	}  else if (config("SkinID") == Skin::KingJack) {
		$bonusWins = "Bonus Wins";
	}

	?>

	<li class="balancebar">Total Balance<span data-bind="text: PlayerAccount.currencySymbol() + ' ' + PlayerAccount.balance()" class="balance_amount ui-lbBalance">...</span></li>
	<li class="balancebar"><?php echo $realMoney;?><span data-bind="text: PlayerAccount.currencySymbol() + ' ' + PlayerAccount.real()" class="realmoney_amount ui-lbReal">...</span></li>
	<li class="balancebar"><?php echo $bonusMoney;?><span data-bind="text: PlayerAccount.currencySymbol() + ' ' + PlayerAccount.bonus()" class="bonusmoney_amount ui-lbBonus">...</span></li>
	<?php //if(!in_array(config("SkinID"), array(Skin::LuckyVIP, Skin::GiveBackBingo, Skin::RegalWins))):?>
		<li class="balancebar"><?php echo $bonusWins;?><span data-bind="text: PlayerAccount.currencySymbol() + ' ' + PlayerAccount.bonusWins()" class="bonuswins_amount ui-lbBonusWins">...</span></li>
	<?php //endif; ?>
	<li class="balancebar"><a href="<?php echo $freeSpinsLink;?>" data-hijack="true"><?php echo $freeSpins;?><span data-bind="text: PlayerAccount.spins" class="bonuswins_amount ui-lbBonusSpins">...</span></a></li>
	<li class="balancebar balance">
		<?php echo $points;?>
			<a data-bind="visible: ConvertPoints.canConvertPoints, click: ConvertPoints.showPanel, text: 'Convert ' + ConvertPoints.pointsName()" style="display: none" href="" class="convertpointsbtn">
				Convert
			</a>
			<a data-hijack="true" data-bind="attr: { href: ConvertPoints.pointsInfoLink }" class="convertpointsbtn__info">Info</a>
			<span data-bind="text: PlayerAccount.points" class="points_amount ui-lbPoints">...</span>
	</li>
	<a href="/cashier/" data-hijack="true" id="button_cashier">Cashier</a>
	<a href="/games/" data-hijack="true" id="playoverview">Games</a>
</ul>

<div>
    <p>
        <a class="promotional-tcs" href="/terms-and-conditions/general-promotional-terms-and-conditions/">
            Freebie T&amp;Cs apply
        </a>
    </p>
</div>

			<!-- Fix for IE9 balanceboxes bug -->
			<!--[if IE 9]>
			<link rel="stylesheet" type="text/css" href="../_css/ie9fix.css" />
			<![endif]-->
