<!-- <div class="loader"><img src="../_imgages/loader/ajax-loader.gif"></div> -->


            <div id="playerdetails">

            <h3 class="playerdetails__header" style="display:none">Update your Personal Details</h3>
            
            <div class="myaccount-details__column">

                <div>
                    <label>Gender</label>
                    <input data-bind="value: PersonalDetails.title" type="text" maxlength="50" disabled="disabled">
                </div>

                <!-- <div>
                    <label class="aspers__title">Title</label>
                    <select data-bind="value: PersonalDetails.title" disabled="disabled" class="aspers__title--input">
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                        <option value="Miss">Ms</option>
                    </select>
                </div> -->

                <!-- ko if: PersonalDetails.separatedNames -->
                <div>
                    <label >First Name</label>
                    <input data-bind="value: PersonalDetails.firstName" type="text" maxlength="50" disabled="disabled">
                </div>
                <div >
                    <label >Last Name</label>
                    <input data-bind="value: PersonalDetails.lastName" type="text" maxlength="50" disabled="disabled">
                </div>
                <!-- /ko -->

                <!-- ko ifnot: PersonalDetails.separatedNames -->
                <div class="aspers__name--wrapper">
                    <label class="aspers__name">Last Name</label>
                    <input data-bind="value: PersonalDetails.firstName() + ' ' + PersonalDetails.lastName()" type="text" maxlength="50" disabled="disabled" class="aspers__name--input">
                </div>
                <!-- /ko -->

                <div>
                    <label class="aspers__email">Email</label>
                    <input data-bind="value: PersonalDetails.email" type="text" maxlength="100" disabled="disabled" class="aspers__email--input">
                </div>
                <div>
                    <label class="aspers__newpassword">New Password</label>
                    <input data-bind="value: PersonalDetails.newPassword" type="password" maxlength="15" class="aspers__newpassword--input">
                    <span data-bind="visible: PersonalDetails.passwordsDontMatch" class="error" style="display:none">Passwords do not match</span>
                    <span data-bind="visible: PersonalDetails.invalidNewPassword" class="error" style="display:none">Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces</span>
                    <span data-bind="visible: PersonalDetails.UsernameAndPassTheSame" class="error" style="display:none">Username and Password cannot be the same</span>
                </div>
                <div>
                    <label class="aspers__repeatnewpassword">Repeat New Password</label>
                    <input data-bind="value: PersonalDetails.repeatPassword" type="password" maxlength="15" class="aspers__repeatnewpassword--input">
                    <span data-bind="visible: PersonalDetails.passwordsDontMatch" class="error" style="display:none">Passwords do not match</span>
                </div>

            </div>


            <div class="myaccount-details__column">
                <div>
                    <label class="aspers__address1">Address Line 1</label>
                    <input data-bind="value: PersonalDetails.address1" type="text" maxlength="100" disabled="disabled" class="aspers__address1--input">
                </div>
                <div>
                    <label class="aspers__address2">Address Line 2</label>
                    <input data-bind="value: PersonalDetails.address2" type="text" maxlength="100" disabled="disabled" class="aspers__address2--input">
                </div>
                <div>
                    <label class="aspers__address3">City</label>
                    <input data-bind="value: PersonalDetails.city" type="text" maxlength="50" disabled="disabled" class="aspers__address3--input">
                </div>
                <div>
                    <label class="aspers__address4">County/State</label>
                    <input data-bind="value: PersonalDetails.state" type="text" maxlength="50" disabled="disabled" class="aspers__address4--input">
                </div>
                <div>
                    <label class="aspers__address5">Post Code</label>
                    <input data-bind="value: PersonalDetails.postcode" type="text" maxlength="15" disabled="disabled" class="aspers__address5--input">
                </div>
                <div>
                    <label class="aspers__address6">Country</label>
                    <input data-bind="value: PersonalDetails.countryName" type="text" disabled="disabled" class="aspers__address6--input">
                </div>

                
            </div>


            <div class="myaccount-details__column">
                <div>
                    <label class="aspers__phone">Landline Phone</label>
                    <input data-bind="value: PersonalDetails.landPhone" type="text" maxlength="50" class="aspers__phone--input">
                    <span data-bind="visible: PersonalDetails.invalidLandPhone" class="error" style="display:none">Please specify a valid phone number</span>
                </div>
                <div>
                    <label class="aspers__phoneext">Landline ext</label>
                    <input data-bind="value: PersonalDetails.landPhoneExt" type="text" maxlength="10" class="aspers__phoneext--input">
                    <span data-bind="visible: PersonalDetails.invalidLandExtPhone" class="error" style="display:none">Please specify a valid phone number</span>
                </div>
                <div>
                    <label class="aspers__mobilephone">Mobile Phone</label>
                    <input id='ui-phone' data-bind="value: PersonalDetails.mobilePhone" type="text" maxlength="50" class="aspers__mobilephone--input">

                    <span data-bind="visible: PersonalDetails.invalidUkMobile" class="error" style="display:none">Mobile phone is 11 digits long, begins with 071 to 075 inclusive or 077 to 079 inclusive.</span>
                    <span data-bind="visible: PersonalDetails.invalidMobile" class="error" style="display:none">Please specify a valid phone number</span>
                </div>

            <div class="receive-bonus">
            <?php edit($this->controller,'contact-pref-title');?>
            <h4><?php @$this->repeatData($this->content['contact-pref-title']);?></h4>


            <?php edit($this->controller,'contact-pref-offer');?>
            <span><?php @$this->getPartial($this->content['contact-pref-offer'],1, "_global-library/_editor-partials/text-plain.php"); ?></span>

            </div>
            
            <br />
                <!--DAUB preferences -->
                <div class="checkboxes">
                    <input data-bind="checked: PersonalDetails.allComms" type="checkbox" id="allCommsCheck">
                    <label style="display:inline;">All communications</label>
                </div>

                <div class="daub-preferences">
                   <div class="checkboxes indent">
                        <input data-bind="checked: PersonalDetails.wantsEmail" type="checkbox">
                        <label style="display:inline;">Send me email news and offers</label>
                   </div>
                   <div class="checkboxes indent">
                       <input data-bind="checked: PersonalDetails.wantsSMS" type="checkbox">
                       <label style="display:inline;">Send me news and offers by SMS</label>
                   </div>
                   <div class="checkboxes indent">
                       <input data-bind="checked: PersonalDetails.wantsPhone" type="checkbox">
                       <label style="display:inline;">Contact me about news and offers by phone</label>
                   </div>
                    <div class="checkboxes indent">
                        <input data-bind="checked: PersonalDetails.wantsMail" type="checkbox">
                        <label style="display:inline;">Send me news and offers by post</label>
                    </div>
                </div>
                
        </div>
        
        
        <div class="trusted-partners">
            <div class="info">

                <?php edit($this->controller,'trusted-partners-title');?>
                <h4><?php @$this->repeatData($this->content['trusted-partners-title']);?></h4>


                <?php edit($this->controller,'trusted-partners-offer');?>
                <span><?php @$this->getPartial($this->content['trusted-partners-offer'],1, "_global-library/_editor-partials/text-plain.php"); ?></span>


                <div class="checkboxes">

              <!--  <div  class="checkboxes" style="margin-top: 25px;">
                   <input data-bind="checked: PersonalDetails.thirdPartyAllComms" type="checkbox" id="thirdPartyAllCommsCheck">

                    <label style="display:inline;">All communications</label>
                </div>-->

                <div class="checkboxes">
                    <input type="checkbox" data-bind="checked: PersonalDetails.thirdPartyEmail" name="ThirdPartyEmail">
                    <label style="display:inline;">Email</label>
                </div>

                <div class="checkboxes">
                    <input type="checkbox" data-bind="checked: PersonalDetails.thirdPartySMS" name="ThirdPartySMS">
                    <label style="display:inline;">SMS</label>
                </div>

               <div class="checkboxes">
                    <input type="checkbox" data-bind="checked: PersonalDetails.thirdPartyPhone" name="ThirdPartyPhone">
                    <label style="display:inline;">Phone</label>
                </div>
                </div> 
                <!--<span>I would like to receive great offers and news from other <a href="https://www.stridegaming.com/trustedbrandoffers/">brands</a> within the group via:</span>-->
            </div>

            <!--Trusted Partners preferences-->
           
            <div class="update-personal--details">
                <div style="margin-top: 20px;">
                    <label class="passlabel">Please provide your current password</label>
                    <input data-bind="value: PersonalDetails.currentPassword" type="password" maxlength="15">
                    <span data-bind="visible: PersonalDetails.invalidCurrentPassword" class="error" style="display:none">This field is required</span>
                </div>

                <span data-bind="visible: PersonalDetails.invalidCurrentPassword()" class="error" style="display:none">Please provide the required fields</span>
                <a data-bind="click: PersonalDetails.updateDetails" href="" class="button expand">Update Details</a>
            </div>
        </div>
    </div>

    