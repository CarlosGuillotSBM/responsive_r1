<section class="cookie-policy js-cookie-policy" id="">
	<div class="container">
		<p class="cookie-policy__message">
			We use cookies on our site to provide you with the best user experience. By using our site and services you consent to use cookies in accordance with our
			<a class="cookie-policy__link" href="/privacy-policy/">cookie policy.</a>
		</p>
		<button class="cookie-policy__button js-cookie-close-button" id="">Accept &amp; Close</button>
	</div>
</section>

<script type="text/javascript">
	var cookieContainers = null;
	var closeButtons = null;
	var body = null;
	var heroDesktop = null;

	function hasClass(el, className) {
		if (el.classList)
			return el.classList.contains(className)
		else
			return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
	}
	function addClass(el, className) {
		if (el.classList)
			el.classList.add(className)
		else if (!hasClass(el, className)) el.className += " " + className
	}
function removeClass(el, className) {
	if (el.classList)
		el.classList.remove(className)
	else if (hasClass(el, className)) {
		var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
		el.className=el.className.replace(reg, ' ')
	}
}
window.onload = function( event ) {
	cookieContainers = document.getElementsByClassName('js-cookie-policy');
	closeButtons = document.getElementsByClassName('js-cookie-close-button');
	body = document.body;
	heroDesktop = document.getElementById("ajax-wrapper");
	var isCookiePolicyAccepted = window.localStorage.getItem('cookiePolicyRead');
	if ( !isCookiePolicyAccepted ) {
		body.classList.add('cookie-policy-active');
		for (var i = 0; i < cookieContainers.length; i++) {
			var cookieContainer = cookieContainers[i];
			cookieContainer.classList.add('active');
			if (!hasClass(heroDesktop, 'cookie-active')) {
				addClass(heroDesktop, 'cookie-active')
			}
		}
	}
	for (var i = 0; i < closeButtons.length; i++) {
		var closeButton = closeButtons[i];
		closeButton.addEventListener('click', closeCookiePolicy );
	}
}
function closeCookiePolicy( event ) {
	event.preventDefault();
	window.localStorage.setItem('cookiePolicyRead', true);
	body.classList.remove('cookie-policy-active');
	for (var i = 0; i < cookieContainers.length; i++) {
		var cookieContainer = cookieContainers[i];
		cookieContainer.classList.remove('active');
		if (hasClass(heroDesktop, 'cookie-active')) {
			removeClass(heroDesktop, 'cookie-active')
		}
	}

}
</script>
