<script type="text/javascript">
  
  skinModules.push({ 
    id: "DontMissOut",
    options: {
        global: true
    }
  });

</script>

<!-- "Don't miss out" POPUP -->
<div class="third-party-consent--modal" style="display:none;" data-bind="visible: DontMissOut.showThirdPartyPopup">
    <div class="third-party-consent--popup">
        <h1>Don't miss out...</h1>
        
        <!-- if aspers -->
        <?php if (config("SkinID") === 12): ?>

          <p style="text-align: left; margin-bottom: 0px">Following the introduction of <b>General Data Protection Regulation</b> (GDPR), a new law that gives you extra rights around how your data is used, we are pleased to introduce our wider family of trusted partners – you can get offers from them too!</p>

          <div class="title"> 
            <p>We are "Aspers Online":</p>
          </div>

          <p style="text-align: left; margin-bottom: 0px">You will continue to receive the same great offers and news as before – no need to do anything. Plus, you can update or change your preferences at any time – just head to "My Account" and click "Personal Details".</p>

          <div class="title"> 
            <p>Our Trusted Partners include:</p>
          </div>

          <div class="trusted-parties">
            <ul>
              <li>8 Ball Games</li>
              <li>Daub</li>
              <li>888</li>
              <li>Aspers Group</li>
            </ul>
          </div>

        <?php endif; ?>
      

        <!-- If NOT aspers -->
        <?php if (config("SkinID") !== 12): ?>


            <div class="title"> 
                <p>Our family is bigger than you think! Our trusted partners include:</p>
            </div>

            <div class="trusted-parties">
                <ul>
                  <li>8-Ball</li>
                  <li>888 Casino</li>
                  <li>Aspers Online</li>
                  <li>Netboost</li>
                </ul>
            </div>

          
            <div class="title"> 
              <p>Can they send you exclusive offers and exciting competitions as well?</p>
            </div>


        <?php endif; ?>


        <div class="copy">
          <p style="margin-top: 16px">Please select your trusted partners communication preferences:</p>
        </div>

        <div class="checks-all">
          <label class="checkbox-trusted-parties">All Communications
              <input data-bind="checked: DontMissOut.thirdPartyAllComms" type="checkbox" id="thirdPartyAllCommsCheck">
              <span class="checkmark-trusted-parties"></span>
          </label>
        </div>

        <ul class="trusted-parties-list">
          <li>
            <label class="checkbox-trusted-parties">Email
              <input data-bind="checked: DontMissOut.thirdPartyEmail" type="checkbox">
              <span class="checkmark-trusted-parties"></span>
            </label>
          </li>
          <li>
            <label class="checkbox-trusted-parties">SMS
              <input data-bind="checked: DontMissOut.thirdPartySMS" type="checkbox">
              <span class="checkmark-trusted-parties"></span>
            </label>
          </li>
          <li>
            <label class="checkbox-trusted-parties">Phone
              <input data-bind="checked: DontMissOut.thirdPartyPhone" type="checkbox">
              <span class="checkmark-trusted-parties"></span>
            </label>
          </li>
        </ul>
        
        <div class="info">
          <p>You can update or change these choices at any time – just head to <strong>"My Account"</strong> and click <strong>"Personal Details"</strong>.</p>
        </div>
        
        <div class="btn-continue">
            <a data-bind="click: DontMissOut.updateDetails">Continue</a>
        </div>
    </div>
  </div> 