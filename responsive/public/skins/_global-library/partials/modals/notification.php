<!-- notification -->

<div data-bind="visible: Notifier.notification() && Notifier.system() === 'notification',
                css: { 'success': Notifier.severity() ===  'success',
                       'alert': Notifier.severity() ===  'error',
                       'info': Notifier.severity() ===  'info',
                       'warning': Notifier.severity() ===  'warning' }"
     style="display: none; position: fixed; top: 0px; z-index: 999999999; width: 100%;"
     class="alert-box radius">
    <span data-bind="text: Notifier.notification"></span>
    <a data-bind="click: Notifier.clearNotification" href="#" class="close">&times;</a>
</div>

<!-- /notification -->