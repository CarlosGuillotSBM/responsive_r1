<?php
$siteName = config("Name");
$playerID = (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;
$sessionID = (isset($_COOKIE["SessionID"])) ? $_COOKIE["SessionID"] : '';
?>
<div style="display: none;" class="alert-background" data-bind="visible: Notifier.notification() && Notifier.system() === 'alert'">
    <!-- alert modal -->
    <div class="alertbox">
        <h3 data-bind="text: Notifier.title"></h3>
        <p data-bind="html: Notifier.notification"></p>
        <div data-bind="visible: Notifier.severity() ===  'loading'" class="alertbox__loader">
            <div class="rad__loader-rectangle">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
                <div class="bar4"></div>
                <div class="bar5"></div>
            </div>
        </div>
        <!-- Loading animation -->
        <div class="alertbox__cta" style="display:none;" data-bind="visible: Notifier.severity() !==  'confirm' && Notifier.severity()!= '3Rad'  && Notifier.severity()!= 'prizeWheel'&& Notifier.severity()!= 'loading'">
            <a data-bind="click: Notifier.callbackOnConfirmed" href="#" class="ok-cta">OK</a>
        </div>
        <div class="alertbox__cta" style="display:none;" data-bind="visible: Notifier.severity() ==  '3Rad'">
            <a data-bind="click: Notifier.callbackOnCancelled" href="#" class="cancel-cta">Use later</a>
            <a data-bind="click: Notifier.callbackOnConfirmed" href="#" class="ok-cta">Use now</a>
        </div>
        <div class="alertbox__cta alertbox__cta__prizeWheel" style="display:none;" data-bind="visible: Notifier.severity() ==  'prizeWheel'">
            <a data-bind="click: Notifier.callbackOnCancelled" href="#" class="alertbox__cta__prizeWheel-use-later cancel-cta">Use later</a>
            <a class="ok-cta alertbox__cta__prizeWheel-mobile" data-bind="click: Notifier.callbackOnCancelled" href="<?= config("PrizeWheelURL") . "?SkinID=" . config("SkinID") . "&uid=" . $playerID . "&guid=" . $sessionID . "&gameid=531" ?>" target="prizeWheel">
                Use now
            </a>
            <a class="ok-cta alertbox__cta__prizeWheel-desktop" data-bind="click: GameLauncher.openGame" data-gameid="gam-prize-wheel" target="prizeWheel">
                Use now
            </a>
        </div>
        <!--  Confirm dialog buttons  -->
        <div data-bind="visible: Notifier.severity() ===  'confirm'" class="alertbox__cta">
            <a data-bind="visible: Notifier.showCancelOnConfirmation, click: Notifier.callbackOnCancelled" style="display: none" class="cancel-cta">Cancel</a>
            <a data-bind="click: Notifier.callbackOnConfirmed" href="#" class="ok-cta">OK</a>
            <div class="clearfix"></div>
        </div>


        <div class="clearfix"></div>
    </div>
</div>
<!-- Loading notification START -->
<div style="display: none;" class="alert-background" data-bind="visible: Notifier.notification() && Notifier.system() === 'loading'">
    <!-- alert modal -->
    <div class="alertbox alertbox--white">
        <div class="alertbox--icon">
            <span class="alertbox--icon--warning-icon">!</span>
        </div>
        <h3 class="alertbox--white__title" data-bind="text: Notifier.title"></h3>
        <p class="alertbox--white__message" data-bind="html: Notifier.notification"></p>
        <div data-bind="visible: Notifier.severity() ===  'loading'" class="alertbox__loader">
            <div class="rad__loader-rectangle">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
                <div class="bar4"></div>
                <div class="bar5"></div>
            </div>
        </div>
        <!-- Loading animation -->
        <div class="clearfix"></div>
    </div>
</div>
<!-- Loading Notification END -->
<!-- Result Notification START -->
<div style="display: none;" class="alert-background" data-bind="visible: Notifier.notification() && Notifier.system() === 'result'">
    <!-- alert modal -->
    <div class="alertbox alertbox--white">
        <div class="alertbox--icon alertbox--icon__success" data-bind="visible: Notifier.severity() === 'success'">
            <span class="alertbox--icon--success-icon">&check;</span>
        </div>
        <div class="alertbox--icon alertbox--icon__danger" data-bind="visible: Notifier.severity() === 'fail'">
            <span class="alertbox--icon--failed-icon">&times;</span>
        </div>
        <h3 class="alertbox--white__title" data-bind="text: Notifier.title"></h3>
        <p class="alertbox--white__message" data-bind="html: Notifier.notification"></p>
        <!-- If verification fails" -->
        <div data-bind="visible: Notifier.severity() ===  'fail'" class="alertbox__cta">
            <a data-bind="click: Notifier.switchToURUUpload" href="#" class="ok-cta">Continue</a>
            <!--
                R10-1181
            <a data-bind="click: Notifier.URUUploadDoLater" class="cancel-cta">I will do it later</a>
            -->
            <div class="clearfix"></div>
        </div>
        <!-- If verification succeeeds  -->
        <div data-bind="visible: Notifier.severity() ===  'success'" class="alertbox__cta">
            <a data-bind="click: Notifier.URUSuccessful()" class="ok-cta">Deposit now</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Result Notification END -->
<!-- URU Check Notification START -->
<div style="display: none;" class="alert-background" data-bind="visible: Notifier.notification() && Notifier.system() === 'uru-check'">
    <!-- alert modal -->
    <div class="alertbox alertbox--white">
        <div class="alertbox--icon alertbox--icon__warning" data-bind="visible: Notifier.severity() === 'pending-documents'">
            <span class="alertbox--icon--warning-icon">?</span>
        </div>
        <div class="alertbox--icon alertbox--icon__warning" data-bind="visible: Notifier.severity() === 'awaiting-response'">
            <span class="alertbox--icon--warning-icon">?</span>
        </div>
        <h3 class="alertbox--white__title" data-bind="text: Notifier.title"></h3>
        <p class="alertbox--white__message" data-bind="html: Notifier.notification"></p>
        <!-- If verification is pending" -->
        <div data-bind="visible: Notifier.severity() ===  'not-verified'" class="alertbox__cta">
            <a href="/cashier" data-hijack="false" class="ok-cta">Get started</a>
            <div class="clearfix"></div>
        </div>
        <!-- If verification is pending (welcome page)" -->
        <div data-bind="visible: Notifier.severity() ===  'not-verified-w'" class="alertbox__cta">
            <a href="#" data-bind="click: Notifier.clearNotification" class="ok-cta">Get started</a>
            <div class="clearfix"></div>
        </div>
        <!-- If we are waiting on documents" -->
        <div data-bind="visible: Notifier.severity() ===  'pending-documents'" class="alertbox__cta">
            <a href="/cashier" data-hijack="true" class="ok-cta">Continue</a>
            <!--
                R10-1181
            <a data-bind="click: Notifier.URUUploadDoLater" class="cancel-cta">I will do it later</a>
            -->
            <div class="clearfix"></div>
        </div>
        <!-- If we are reviewing documents" -->
        <div data-bind="visible: Notifier.severity() ===  'awaiting-response'" class="alertbox__cta">
            <a data-bind="click: Notifier.URUUploadDoLater" class="ok-cta">Ok</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Result Notification END -->
<!-- Are you sure style notification -->
<div style="display: none;" class="alert-background" data-bind="visible: Notifier.notification() && Notifier.severity() === 'are-you-sure'">
    <!-- alert modal -->
    <div class="alertbox alertbox--white">
        <div class="alertbox--icon alertbox--icon__warning" data-bind="visible: Notifier.severity() === 'pending-documents'">
            <span class="alertbox--icon--warning-icon">?</span>
        </div>
        <div class="alertbox--icon alertbox--icon__warning" data-bind="visible: Notifier.severity() === 'awaiting-response'">
            <span class="alertbox--icon--warning-icon">?</span>
        </div>
        <h3 class="alertbox--white__title" data-bind="text: Notifier.title"></h3>
        <p class="alertbox--white__message" data-bind="html: Notifier.notification"></p>
        <a data-bind="visible: Notifier.showCancelOnConfirmation, click: Notifier.callbackOnCancelled" style="display: none" class="cancel-cta">Cancel</a>
        <a data-bind="click: Notifier.callbackOnConfirmed" href="#" class="ok-cta">OK</a>
    </div>
    <div class="clearfix"></div>
</div>
</div>

<style>
    /* SAW */
    .cta-verify-1 {
        color: #108B87;
        text-decoration: underline;
    }

    a.cta-verify-1:hover {
        color: #108B87 !important;
    }

    .heading-verify-1 {
        color: #108B87;
    }

    .subheading-verify-1 {
        color: #108B87;
    }

    .icon-verify-1 {
        fill: #108B87;
    }

    /* KB */
    .cta-verify-2 {
        text-decoration: underline;
    }

    a.cta-verify-2:hover {
        color: #1D6FBA !important;
    }

    .icon-verify-2 {
        fill: #1D4775;
    }

    /* LP */
    .cta-verify-3 {
        text-decoration: underline;
        color: #8240AE;
    }

    a.cta-verify-3:hover {
        color: #8240AE !important;

    }

    .heading-verify-3 {
        color: #8240AE;
    }

    .subheading-verify-3 {
        color: #8240AE;
    }

    .icon-verify-3 {
        fill: #8240AE;
    }

    /* MV */
    .cta-verify-5 {
        text-decoration: underline;
        color: #011558;
    }

    a.cta-verify-5:hover {
        color: #011558 !important;

    }

    /* BE */
    .cta-verify-6 {
        text-decoration: underline;
    }

    .icon-verify-6 {
        fill: #0F506D;
    }

    .heading-verify-6 {
        font-family: "expo-sans-pro-bold", Helvetica, Arial, sans-serif;
    }

    .subheading-verify-6 {
        font-family: "expo-sans-pro-bold", Helvetica, Arial, sans-serif;
    }

    .message-verify-6 {
        color: #0F506D !important;
    }

    .cta-verify-6 {
        color: #0F506D;
    }

    .cta-contact-6 {
        border: 1px solid #0F506D !important;
        color: #0F506D !important;
    }


    /* LVIP */
    .box-verify-8 {
        border: 1px solid rgba(255, 255, 255, .5);
    }

    .cta-verify-8 {
        text-decoration: underline;
        color: #fff !important;
    }

    a.cta-verify-8:hover {
        color: #fff !important;
        background-color: transparent;
    }

    .heading-verify-8 {
        color: #fff;
    }

    .subheading-verify-8 {
        color: #fff;
    }

    .icon-verify-8 {
        fill: #fff;
    }

    /* GBB */
    .cta-verify-9 {
        text-decoration: underline;
    }

    .heading-verify-9 {
        color: #1F80D5;
    }

    .subheading-verify-9 {
        color: #1F80D5;
    }

    .icon-verify-9 {
        fill: #1F80D5;
    }

    /* KJ */
    .box-verify-11 {
        border: 1px solid rgba(255, 255, 255, .5);
        background-color: #411972;
    }

    .heading-verify-11 {
        color: #ffffff;
    }

    .subheading-verify-11 {
        color: #ffffff;
    }

    .message-verify-11 {
        color: #ffffff !important;
    }

    .icon-verify-11 {
        fill: #ffffff;
    }

    .cta-contact-11 {
        border: 1px solid #FED500 !important;
        color: #FED500 !important;
    }

    .cta-verify-11 {
        text-decoration: underline;
        color: #ffffff;
    }

    a.cta-verify-11:hover {
        color: #ffffff !important;
        text-decoration: underline;
    }

    /* ASP */
    .cta-verify-12 {
        text-decoration: underline;
        color: #000000;
    }

    .radio-selection .selecter {
        max-width: 50%;
        display: inline-block;
        margin-right: 10px;
    }

    .radio-selection input {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background-color: #ffffff !important;
        border-radius: 100% !important;
        border: 2px solid #000000 !important;
        display: inline-block;
        min-width: 25px !important;
        min-height: 25px !important;
        transition: 0.2s all linear;
        outline: none;
        position: relative;
        top: 14px;
    }

    .radio-selection input:checked {
        background-color: #000000 !important;
        border: 2px solid #000000 !important;
    }

    .radio-label {
        display: inline-block;
        color: #000000;
        font-family: Helvetica;
        font-size: 16px;
        letter-spacing: 0px;
        line-height: 25px;
        float: left;
        background-color: transparent;
    }

    .radio-label--text {
        margin-left: 5px;
        top: -10px;
        position: relative;
    }
</style>
<!-- Source of Funds - Locked account alert modal -->
<div style="display: none;" class="alert-background" data-bind="visible: LoginBox.sofRequired">
    <!-- <div class="alert-background"> -->
    <!-- alert modal -->
    <div class="alertbox box-verify-<?php echo config('SkinID'); ?>" style="padding-top: 25px;">

        <div style="float: right">
            <svg class="icon-verify-<?php echo config('SkinID'); ?>" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path d="M18 10v-4c0-3.313-2.687-6-6-6s-6 2.687-6 6v4h-3v14h18v-14h-3zm-10-4c0-2.206 1.794-4 4-4 2.205 0 4 1.794 4 4v4h-8v-4zm3.408 14l-2.842-2.756 1.172-1.173 1.67 1.583 3.564-3.654 1.174 1.173-4.738 4.827z" />
            </svg>
        </div>

        <h6 style="margin-bottom: 15px; text-transform: uppercase;" class="heading-verify-<?php echo config('SkinID'); ?>">Account
            Temporarily Closed:</h6>
        <h6 style="text-transform: uppercase;" class="subheading-verify-<?php echo config('SkinID'); ?>">Verification
            Required</h6>
        <p class="message-verify-<?php echo config('SkinID'); ?>">
            Please speak to our Customer Service team to verify your account now.
        </p>
        <div class="alertbox__cta" style="margin-bottom:30px;">
            <a href="/support/" class="ok-cta cta-contact-<?php echo config('SkinID'); ?>">CONTACT US</a>
        </div>
        <p>
            <a class="cta-verify-<?php echo config('SkinID'); ?>" href="/account-verification/" style="border: none; margin-top: 10px;">more
                info</a>
        </p>

        <div class="clearfix"></div>
    </div>
    <!-- /alert modal -->
</div>

<!-- ALERT for players with their accounts enabled but still need to upload documents for source of funds -->

<div class="account-verify-modal" style="display: none;" data-bind="visible: showVerifyPopUp">
    <div class="account-verify-popup">

        <img src="/_global-library/images/cashier/account-verification/icon-padlock.svg" alt="" />

        <p class="title">Account Verification</p>

        <p class="copy">We need you to verify your account. Select <b>'Continue'</b> to be taken to a secure area
            where you can quickly and safely upload your documents.</p>

        <div class="btn-holder">
            <div class="btn-cancel" data-bind="click: closeVerifyPopUp">
                <p>I will do it later</p>

            </div>

            <div data-bind="click: accountVerificationCashier" class="btn-continue">
                <p>Continue</p>
            </div>

        </div>

        <a data-bind="click: accountVerificationMoreInfo" class="more-info">More info</a>
    </div>
</div>


<script type="application/javascript">
    skinModules.push({
        id: "ResponsibleGaming",
        options: {
            global: true
        }
    });

    skinModules.push({
        id: "PersonalDetails",
        options: {
            global: true
        }
    });

    skinModules.push({
        id: "TermsService",
        options: {
            global: true
        }
    });
</script>

<div class="responsible-gaming-modal" style="display: none;" data-bind="visible: ResponsibleGaming.showResGamingPopUp">
    <div class="responsible-gaming-popup">
        <div class="title">
            <h1>responsible gaming</h1>
        </div>

        <div class="copy">
            <p>As a responsible gaming site,
                <?php echo $siteName; ?> would like to ensure you are happy with your current level of gaming activity.
                We're here to help you with any concerns or questions you may have about your gameplay.
                Therefore, please select one of the following options:</p>
        </div>

        <div class="box-holder">
            <div class="box-review" data-bind="click: ResponsibleGaming.setLimitResGaming">
                <img src="/_global-library/images/responsible_gaming-modal/icon-settings.svg" />
                <h3>review settings</h3>
                <p>Adjust my deposit limit</p>
            </div>

            <div class="box-support" data-bind="click: ResponsibleGaming.liveChatResGaming">
                <img src="/_global-library/images/responsible_gaming-modal/icon-chat.svg" />
                <h3>Chat to Support</h3>
                <p>About my gaming activity</p>
            </div>

            <div class="box-happy" data-bind="click: ResponsibleGaming.continueResGaming">
                <img src="/_global-library/images/responsible_gaming-modal/icon-smiling-face.svg" />
                <h3>I'm happy with my gaming </h3>
                <p>Continue and play</p>
            </div>
        </div>

        <div class="terms">
            <p>For more information visit <a href="#" data-bind="click: ResponsibleGaming.responsibleURL">Responsible
                    Gaming.</a></p>
        </div>

    </div>
</div>


<div style="display: none" class="tc-privacy-modal" data-bind="visible: TermsService.popupVisible">
    <!-- <div class="tc-privacy-modal"> -->
    <div class="tc-privacy-popup">

        <?php edit($this->controller, 'tcspp-main-title'); ?>

        <div class="title">
            <h1>
                <?php @$this->getPartial($this->content['tcspp-main-title'], 1); ?>
            </h1>
        </div>


        <div class="tc-copy" data-bind="visible: TermsService.hasTCs" style="display: none">
            <div data-bind="html: TermsService.tcs"></div>
        </div>

        <div class="privacy-copy" data-bind="visible: TermsService.hasPP" style="display: none;">
            <div data-bind="html: TermsService.pp"></div>
        </div>
        <div class="checkbox-align" style="text-align:left;padding-left:1em">
            <div class="checkbox-align" data-bind="visible: TermsService.hasTCs" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreeTcs" id="cbAgreeTcs">
                I agree to the updated <a href="/terms-and-conditions/" target="Terms" style="color: blue;text-decoration: underline;">terms and conditions</a>
            </div>
            <div class="checkbox-align" data-bind="visible: TermsService.hasPP" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreePP" id="agreePP">
                I have read and understood the <a href="/privacy-policy" target="Terms" style="color: blue;text-decoration: underline;">privacy policy</a>
            </div>
        </div>


        <div class="com-copy" data-bind="visible: TermsService.hasCOM" style="display: none; text-align:left; margin:1em">
            <h5>Third party communications</h5>
            <p>
                I would like to receive fantastic offers and news from Aspers Online and brands within the&#160;<a title="Privacy Policy" href="/privacy-policy" target="_blank" style="font-weight: bold; color: black;">Daub Group</a>&#160;by:
            </p>
            <div class="checkbox-align" data-bind="visible: TermsService.hasCOM" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreeEmail" id="cbAgreeEmail">
                <label style="display:inline;" for="cbAgreeEmail">Email</label>
            </div>
            <div class="checkbox-align" data-bind="visible: TermsService.hasCOM" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreeSMS" id="cbAgreeSMS">
                <label style="display:inline;" for="cbAgreeSMS">SMS</label>
            </div>

            <div class="checkbox-align" data-bind="visible: TermsService.hasCOM" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreePhone" id="cbAgreePhone">
                <label style="display:inline;" for="cbAgreePhone">Phone</label>
            </div>


            <h5>Data</h5>
            Would you like to share your data with <a href="https://www.aspers.co.uk/" style="font-weight: bold; color: black;">Aspers Group</a> (Offline casino) for compliance and marketing
            reasons?
            </p>
            <div class="field-wrap radio-selection">
                <div class="selecter">
                    <label for="radio-5" class="radio-label">
                        <input id="radio-5" name="yes" type="radio" value="1" data-bind="checked: TermsService.agreeData">
                        <span class="radio-label--text">Yes</span>
                    </label>
                </div>
                <div class="selecter">
                    <label for="radio-6" style="margin-left: 10px;" class="radio-label">
                        <input id="radio-6" name="no" type="radio" value="0" data-bind="checked: TermsService.agreeData" class="border-green">
                        <span class="radio-label--text">No</span>
                    </label>
                </div>
            </div>
            <div class="checkbox-align" data-bind="visible: TermsService.agreeData() == true" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreeTPEmail" id="cbAgreeTPEmail">
                <label style="display:inline;" for="cbAgreeTPEmail">Email</label>
            </div>
            <div class="checkbox-align" data-bind="visible: TermsService.agreeData() == true" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreeTPSMS" id="cbAgreeTPSMS">
                <label style="display:inline;" for="cbAgreeTPSMS">SMS</label>
            </div>
            <div class="checkbox-align" data-bind="visible: TermsService.agreeData() == true" style="display: none;">
                <input type="checkbox" data-bind="checked: TermsService.agreeTPPhone" id="cbAgreeTPPhone">
                <label style="display:inline;" for="cbAgreeTPPhone">Phone</label>
            </div>
            <p class="preference-support-text">You can change your preferences at any time in the 'personal details'
                section
                of "My Account"</p>
            <p>

        </div>

        <div class="btn-holder">
            <!--  This was removed for now  -->
            <!--  <div class="btn tc-btn not-agree" data-bind="click: TermsService.doNotAgree">
              <h5>I do not agree</h5>
           </div> -->
            <div class="btn tc-btn next disabled" data-bind="click: TermsService.continue, css: { disabled: !TermsService.canContinue() } ">
                <h5>I accept</h5>
            </div>
        </div>


    </div>
</div>