<!-- Title Bar -->
<div class="single-message-modal__title-bar">

  <span class="icon-message">
    <?php include '_global-library/partials/start/icon-message-read.php'; ?>
  </span>

  <p class="subject-message" data-bind="text: MailInbox.messageSubject"></p>

  <span class="icon-close close-reveal-modal" data-bind="click: MailInbox.closeMessage">
    <?php include '_global-library/partials/start/icon-message-modal-close.php'; ?>
  </span>

</div>

<!-- Message Content -->
<div class="single-message-modal__content-wrap">

  <iframe id="ui-frame-msg" class="single-message-modal__content" data-bind="bindIframe: MailInbox">

    <!-- message content goes here -->

  </iframe>   

</div>
