<!-- slider -->
<div class="range-slider__wrapper">
  <div class="range-slider__limit">£0</div>
  <div class="range-slider__slider">
    <div class="range-slider radius" data-slider data-options="step: 20;">
      <span class="range-slider-handle" role="slider" tabindex="0"></span>
      <span class="range-slider-active-segment"></span>
      <input type="hidden">
    </div>
  </div>
  <div class="range-slider__limit">£100</div>
</div>
<!-- /slider -->