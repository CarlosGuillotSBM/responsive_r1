<script type="text/html" id="notificationTemplate">
    <div class="cma-notification">
        <div class="row">
            <div class="columns small-2 cma-notification--logo">
                <img src="/_global-library/images/logos/2.png" onerror="this.style.display='none'">
            </div>
            <div class="columns small-8 large-10 cma-notification--mesage" data-bind="text: message"></div>
            <div class="small-2 columns cma-notification--close" data-bind="click: $parent.CMANotifications.removeNotification.bind($data)">
                <div class="cma-notification--counter countdown">
                    <div class="countdown-number">&times;</div>
                    <svg>
                        <circle r="18" cx="20" cy="20"></circle>
                    </svg>
                </div>
            </div>
        </div>
        <!-- Start expandable content -->
        <div data-bind="visible: expand">
            <div class="column small-12 notification--expand-control" data-bind="visible: !readMore(), click: $parent.CMANotifications.expand.bind(this)">
                Read more &#9660;
            </div>
            <div class="notification--expandable" data-bind="visible: readMore()">
            <div>
                <div data-bind="foreach: bonusItems" class="notificationSlides" >
                    <article>
                        <div class="row">
                            <div class="small-5 columns">Promotion name</div>
                            <div class="small-7 columns" data-bind="text: pName"></div>
                        </div>
                        <div class="row">
                        <div class="small-5 columns">Promotion ID</div>
                        <div class="small-7 columns" data-bind="text: pId"></div>
                        </div>
                        <div class="row">
                        <div class="small-5 columns">Wagers required</div>
                        <div class="small-7 columns" data-bind="text: pWager"></div>
                        </div>
                        <div class="row">
                        <div class="small-5 columns">Max convertible to real</div>
                        <div class="small-7 columns" data-bind="text: pMax"></div>
                        </div>
                        <div class="row">
                        <div class="small-5 columns">Date expires</div>
                        <div class="small-7 columns" data-bind="text: pDate"></div>
                        </div>
                    </article>
                </div>
            </div>
        <!-- End expandable content -->
    </div>
</script>