<?php
$fileExtension = (array_search(config("SkinID"),[1,2,3,5,6,9]) > -1 ? ".png" : ".svg");
$logo="/_global-library/images/logos/".config("SkinID").$fileExtension;
?>
<script type="text/html" id="notificationTemplate">
    <div class="cma-notification">
        <div class="cma-notification--wrapper">
            <div class="cma-notification--logo">
                <img src="<?=$logo?>" onerror="this.style.display='none'">
            </div>
            <div class="cma-notification--mesage" data-bind="text: message"></div>
            <div data-bind="visible: expand" class="cma-notification--controls">
                <div class="column small-12 notification--expand-control" data-bind="visible: !readMore(), click: $parent.CMANotifications.expand.bind(this)">
                    More Info &#9660;
                </div>
                <div class="column small-12 notification--expand-control" data-bind="visible: readMore(), click: $parent.CMANotifications.collapse.bind(this)">
                    More Info &#9650;
                </div>
            </div>
            <div class="cma-notification--close" data-bind="click: $parent.CMANotifications.removeNotification.bind($data)">
                <div class="cma-notification--counter countdown">
                    <div class="countdown-number">&times;</div>
                    <svg class="cma-notification--svg">
                        <circle  r="11.5" cx="12.5" cy="12.5"></circle>
                    </svg>
                </div>
            </div>
        </div>
            <hr  data-bind="visible: expand && readMore()">
        <!-- Start expandable content -->
        <div data-bind="visible: expand">
            <div class="notification--expandable" data-bind="visible: readMore()">
            <div>
                <div data-bind="foreach: bonusItems" class="notificationSlides" >
                    <article>
                        <div class="row">
                            <div class="small-6 columns">Promotion name</div>
                            <div class="small-6 column text-right" data-bind="text: pName"></div>
                        </div>
                        <div class="row">
                        <div class="small-6 columns">Promotion ID</div>
                        <div class="small-6 column text-right" data-bind="text: pId"></div>
                        </div>
                        <div class="row">
                        <div class="small-6 columns">Wagers required</div>
                        <div class="small-6 column text-right" data-bind="text: pWager"></div>
                        </div>
                        <div class="row">
                        <div class="small-6 columns">Max convertible to real</div>
                        <div class="small-6 column text-right" data-bind="text: pMax"></div>
                        </div>
                        <div class="row">
                        <div class="small-6 columns">Date expires</div>
                        <div class="small-6 column text-right" data-bind="text: pDate"></div>
                        </div>
                    </article>
                </div>
            </div>
        <!-- End expandable content -->
    </div>
</script>