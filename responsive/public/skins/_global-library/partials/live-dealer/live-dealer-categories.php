<div class="filter-tabs">
  <dl class="filter-tabs__heading ui-filter-tabs" data-tab>
    <dd class="filter-tabs__heading__item active">
      <a class="filter-tabs__heading__link" href="#live-dealer-panel1">Type</a>
    </dd>
    <dd class="filter-tabs__heading__item">
      <a class="filter-tabs__heading__link" href="#live-dealer-panel2">Publisher</a>
    </dd>
  </dl>
  <div class="filter-tabs__content">
    <div class="filter-tabs__content__item active" id="live-dealer-panel1">
      <ul>
        <li>
          <a class="filter-tabs__content__link" id="ui-featured" data-gtag="Game Menu,Themes+Featured" href="/live-dealer/live-featured/" data-hijack="true">
            <span>Featured</span>
          </a>
        </li>
        <li>
          <a class="filter-tabs__content__link" id="ui-live-roulette" data-gtag="Game Menu,Themes+Live Roulette" href="/live-dealer/live-roulette/" data-hijack="true">
            <span>Live Roulette</span>
          </a>
        </li>
        <li>
          <a class="filter-tabs__content__link" id="ui-live-blackjack" data-gtag="Game Menu,Themes+Live Blackjack" href="/live-dealer/live-blackjack/" data-hijack="true">
            <span>Live Blackjack</span>
          </a>
        </li>
        <li>
          <a class="filter-tabs__content__link" id="ui-live-baccarat" data-gtag="Game Menu,Themes+Live Baccarat" href="/live-dealer/live-baccarat/" data-hijack="true">
            <span>Live Baccarat</span>
          </a>
        </li>
        <li>
          <a class="filter-tabs__content__link" id="ui-live-poker" data-gtag="Game Menu,Themes+Live Poker" href="/live-dealer/live-poker/" data-hijack="true">
            <span>Live Poker</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="filter-tabs__content__item" id="live-dealer-panel2">
      <ul>
        <?php if (config("SkinID") != Skin::LuckyVIP && config("SkinID") != Skin::MagicalVegas && config("SkinID") != Skin::KingJack) : ?>
            <li>
                <a class="filter-tabs__content__link" id="ui-test" data-gtag="Game Menu,Features+Evolution" href="/live-dealer/evolution/" data-hijack="true">
                    <span>Evolution</span>
                </a>
            </li>
        <?php endif; ?>
        <li>
            <a class="filter-tabs__content__link" id="ui-test" data-gtag="Game Menu,Features+Netent" href="/live-dealer/netent/" data-hijack="true">
                <span>Netent</span>
            </a>
        </li>
      </ul>
    </div>
  </div>
</div>