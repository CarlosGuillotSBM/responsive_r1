<nav data-bind="visible: !Cashier.cashierNotAvailable()" class="cashier__main-nav">
    <ul>
        <li data-bind="visible: Cashier.showDepositMenu(), css: {'current': Cashier.currentAction() === 'deposit'}" style="display: none;">
            <a class="cashier__main-nav__deposit" href="" data-bind="click: Cashier.openDeposit.bind($data, true)"> <?php echo i18n::getTranslation("deposit") ?> </a>
        </li>
        <li data-bind="visible: Cashier.showWithdrawMenu(), css: {'current': Cashier.currentAction() === 'withdraw'}" style="display: none;">
            <a class="cashier__main-nav__withdraw" href="" data-bind="click: Cashier.openWithdraw"> <?php echo i18n::getTranslation("withdraw") ?> </a>
        </li>
        <li data-bind="visible: Cashier.showWithdrawMenu(), css: {'current': Cashier.currentAction() === 'reverse'}" style="display: none;">
            <a class="cashier__main-nav__reverse" href="" data-bind="click: Cashier.openReverse"> <?php echo i18n::getTranslation("reverse") ?> </a>
        </li>
        <li data-bind="visible: Cashier.needsToVerifyAccount, css: {'current': Cashier.currentAction() === 'account-verification'}" style="display: none">
            <a class="cashier__main-nav__account-verification" href="" data-bind="click: Cashier.openAccountVerification"> Account Verification </a>
        </li> 
        <li data-bind="css: {'current': Cashier.currentAction() === 'bank-details'},
            visible: Cashier.cashierData.canEditBankDetails" style="display: none">
            <a class="cashier__main-nav__details" href="" data-bind="click: Cashier.openBankDetails"> <?php echo i18n::getTranslation("myDetails") ?> </a>
        </li>
    </ul>
</nav>