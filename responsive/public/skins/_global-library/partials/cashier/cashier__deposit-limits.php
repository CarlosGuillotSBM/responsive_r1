<?php
$isAspers = (config("SkinID") === 12);
/*
    R10-1005: Load module AspersAppTracking in order to track Aspers app
    Date : 04/04/2019
    Dev  : Teddy Bakali
*/
    if($isAspers){
        ?>
            <script type="application/javascript">
                skinModules.push({ id:"AspersAppTracking"});
             </script>
        <?php
    }
?>

<!-- Deposit Limits -->

<div data-bind="visible: Cashier.activePanel() === 'DEPOSITLIMITS'" style="display: none" class="cashier__section--limits">

    <div data-bind="visible: Cashier.playerLimits.currentAction() === 'pending'" style="display: none" class="cashier__section">
        <?php require_once "cashier__deposit-limits-pending.php" ?>
    </div>

    <div data-bind="visible: Cashier.playerLimits.currentAction() === 'disclaimer'" style="display: none" class="cashier__section--limits-inner">
        <?php require_once "cashier__deposit-limits-disclaimer.php" ?>
    </div>

    <div data-bind="visible: Cashier.playerLimits.currentAction() === 'responsible'" style="display: none" class="cashier__section--limits-inner">
        <?php require_once "cashier__deposit-limits-responsible.php" ?>
    </div>

    <div data-bind="visible: Cashier.playerLimits.currentAction() === 'setup' || Cashier.playerLimits.currentAction() === 'add-limit'" style="display: none" class="cashier__section--limits-inner">
        <?php require_once "cashier__deposit-limits-setup.php" ?>
    </div>

    <div data-bind="visible: Cashier.playerLimits.currentAction() === 'password'" style="display: none" class="cashier__section--limits-inner">
        <?php require_once "cashier__deposit-limits-password.php" ?>
    </div>

    <div data-bind="visible: Cashier.playerLimits.currentAction() === 'no-limits'" style="display: none" class="cashier__section--limits-inner">
        <?php require_once "cashier__deposit-limits-no-limits.php" ?>
    </div>

</div>

