<section data-bind="visible: Cashier.activePanel() === 'METHODS'" style="display: none" class="cashier__section">
	<h1 class="cashier__section__title"><?php echo i18n::getTranslation("choosePaymentMethod") ?></h1>
	<div class="cashier__list">
		<ul data-bind="foreach: Cashier.addingNewMethod.availableMethods">
			<li data-bind="click: $root.Cashier.selectedNewMethod">
				<div class="cashier__list__wrapper new__methods">
					<img data-bind="attr: { src : '/_global-library/images/cashier/' + icon }">
				</div>
			</li>
		</ul>
	</div>
    <div class="cashier__cta">
        <div class="cashier__cta__wrapper">
          <button data-bind="visible: Cashier.backNeeded, click: Cashier.openDeposit.bind($data, true)" class="secondary radius"> <?php echo i18n::getTranslation("back") ?> </button>
        </div>
    </div>
</section>	 