<section data-bind="visible: Cashier.activePanel() === 'URUPENDINGRESPONSE'" class="cashier__section"
    style="display: none" xmlns="http://www.w3.org/1999/html">

    <!--  DETAILS  -->


    <div class="account-verification verification">
        <h1 class="cashier__section__title">Identity Verification</h1>
        <p class=".cashier__section__description">Our team is still checking your documents to verify your identity.Please bear with us. <a href="/account-verification/" class="help-link"> More info.</a></p>
    </div>
    <!--  /DETAILS  -->

</section>