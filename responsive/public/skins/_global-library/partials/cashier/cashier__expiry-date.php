<article data-bind="visible: Cashier.activePanel() === 'EXPIRY'" class="cashier__withdraw" style="display: none"
         xmlns="http://www.w3.org/1999/html">

    <!--  DETAILS  -->

    <h1 class="cashier__section__title"> <?php echo i18n::getTranslation("changeExpiryDate") ?> </h1>

    <!-- Month -->
    <div class="">
        <label> <?php echo i18n::getTranslation("month") ?> </label>
        <select data-bind="value: Cashier.cardTokenUpdate.expiryMonth">
            <option disabled="" value="" selected=""> <?php echo i18n::getTranslation("month") ?> </option>
            <option value="01"> <?php echo i18n::getTranslation("jan") ?> </option>
            <option value="02"> <?php echo i18n::getTranslation("feb") ?> </option>
            <option value="03"> <?php echo i18n::getTranslation("mar") ?> </option>
            <option value="04"> <?php echo i18n::getTranslation("apr") ?> </option>
            <option value="05"> <?php echo i18n::getTranslation("may") ?> </option>
            <option value="06"> <?php echo i18n::getTranslation("jun") ?> </option>
            <option value="07"> <?php echo i18n::getTranslation("jul") ?> </option>
            <option value="08"> <?php echo i18n::getTranslation("aug") ?> </option>
            <option value="09"> <?php echo i18n::getTranslation("sep") ?> </option>
            <option value="10"> <?php echo i18n::getTranslation("oct") ?> </option>
            <option value="11"> <?php echo i18n::getTranslation("nov") ?> </option>
            <option value="12"> <?php echo i18n::getTranslation("dec") ?> </option>
        </select>
    </div>
    <!-- year -->
    <div class="">
        <label> <?php echo i18n::getTranslation("year") ?> </label>
        <select
            data-bind="options: Cashier.expiryYears,
                                optionsCaption: 'Year', optionsValue: 'val',
                                optionsText: 'desc', value: Cashier.cardTokenUpdate.expiryYear"
            class="textField">
        </select>

    </div>

    <div class="cashier__cta">
    <div class="cashier__cta__wrapper">
        <button data-bind="click: Cashier.saveExpiry" class="success radius">
            <?php echo i18n::getTranslation("save") ?>
        </button>
    </div>
     </div>

    <!--  /DETAILS  -->

</article>