<?php
    // TODO: this should go to each skin - they might have different languages!
    include_once("_global-library/i18n/cashier-en.php");
?>



<script type="application/javascript">
    skinModules.push( {
        id: "QuickDeposit",
        options: {
            source: "Qik",
            global: true
        }
    } );
</script>

<!-- Quick Deposit Container -->

<script id="ui-quick-deposit-promos-tmpl" type="text/tmpl">
    <div>
        <input type="radio" name="promoGroup" data-bind="attr: {value: PromoID}, checked: $root.QuickDeposit.selectedPromo" />
        <span data-bind="html: Description, attr: { title: tooltip }"></span>
    </div>
</script>

<div id="ui-quick-deposit-container" class="quick-deposit-container" data-bind="visible: QuickDeposit.opened" style="display: none">

    <!-- Loading -->
    <div class="quick-deposit-container__loader" data-bind="visible: QuickDeposit.loading">
        <img src="/_global-library/images/loading.gif">
    </div>
    <!-- /Loading -->

    <!--  Quick Deposit Panel -->
    <div data-bind="visible: !QuickDeposit.showError() && !QuickDeposit.loading() && !QuickDeposit.success()">


                <div class="quick-deposit-container__header">
                    <div class="quick-deposit-container__header__icons">
                        Quick Deposit
                    </div>


                     <div class="quick-deposit-container__header__close">
                        <img data-bind="attr: { src : '/_global-library/images/cashier/qd-close.svg'}, click: QuickDeposit.cancel" data-gtag="Closed Quick Deposit Without Depositing">
                          <!-- <img data-bind="attr: { src : '/_global-library/images/cashier/cashier-info.svg'}"> -->
                     </div>

                     <div class="quick-deposit-container__header__card">

                     <div class="select-card">
                          <select data-bind="options: QuickDeposit.methods, optionsValue: 'AccountID', optionsText: 'accountDescription', value: QuickDeposit.selectedMethod" class="textField"></select>
                     </div>
                         <input style="display: none" type="text" class="cvn" placeholder="CVN"
                           data-bind="visible: QuickDeposit.needsCvn,
                                      value: QuickDeposit.cvn,
                                      valueUpdate: ['input', 'afterkeydown']"></div>

                     <div style="display: none" class="error-cvn error" data-bind="visible: QuickDeposit.cvnInvalid">CVN is invalid</div>


                </div>



                <div style="display: none" class="quick-deposit-container__bonustext" data-bind="visible: QuickDeposit.promotions().length > 1">
                    <div id="ui-quick-deposit-promos" class="quick-deposit-container__bonustext__container" data-bind="foreach: QuickDeposit.promotions()">
                            <!-- content from ui-quick-deposit-promos-tmpl will go here -->
                    </div>

                        <!-- <span class="tcs-text"><a href="/terms-and-conditions/general-promotional-terms-and-conditions/" target="_blank">T&amp;C's apply</a></span> -->
                </div>

                <div class="quick-deposit-container__slider">
                    <div class="quick-deposit-container__slider__limits">
                         <div data-bind="text: QuickDeposit.currencySymbol + QuickDeposit.minAmount()" class="qd-range-slider__limit"></div>
                        <div data-bind="text: QuickDeposit.currencySymbol + QuickDeposit.maxAmount()" class="qd-range-slider__limit"></div>
                    </div>
                    <!-- slider -->

                    <div class="qd-range-slider__wrapper">

                        <div class="range-slider__slider">
                            <div id="ui-withdrawal-slider"></div>
                        </div>

                    </div>
                    <!-- /slider -->
                </div>

                <div style="padding: 8px" data-bind="visible: QuickDeposit.showSecureId">
                    <input data-bind="value: QuickDeposit.secureId" class="textField" type="text" placeholder="<?php echo i18n::getTranslation("quickNetellerSecId") ?>">
                    <div style="display: none; color: #FFFFFF" class="error-cvn error" data-bind="visible: QuickDeposit.secureIdInvalid">Neteller Secure Id is invalid</div>
                </div>

                <div class="quick-deposit-container__buttons">
                    <button class="radius success" data-bind="text: 'Deposit ' + QuickDeposit.currencySymbol + QuickDeposit.selectedAmount(),
                                       click: QuickDeposit.makeDeposit"></button>
                    <div class="quick-deposit-container__buttons__promoinfo radius" data-bind="text: QuickDeposit.depositPromoInfo"></div>
                </div>

    </div>
    <!--  /Quick Deposit Panel -->

    <!-- Quick Deposit Error Panel -->

    <div class="inside" data-bind="visible: QuickDeposit.error().code()">
         <h1 class="quick-deposit-container__header">Sorry</h1>
         <!-- Unrecoverable error -->
         <p class="quick-deposit-container_error" data-bind="html: QuickDeposit.error().message"></p>

         <a class="cancel-button radius" href="/cashier/">Visit Cashier</a>

         <a href="#" class="quick-deposit-container__close-link" data-bind="click: QuickDeposit.cancel">Close</a>
    </div>

    <!-- /Quick Deposit Error Panel -->

    <!-- Quick deposit Success -->

    <div style="display: none" data-bind="visible: QuickDeposit.success">
        <h1 class="quick-deposit-container__header"> <?php echo i18n::getTranslation("thankYou") ?> </h1>
        <span class="deposit-successful"> <?php echo i18n::getTranslation("yourDepositSuccessful") ?> </span>

        <!-- Info table -->

        <table>
            <tr>
                <td> <?php echo i18n::getTranslation("depositAmount") ?>:</td>
                <td>
                    <span data-bind="text: QuickDeposit.currencySymbol + QuickDeposit.amountDeposited()"></span>
                </td>
            </tr>
            <tr>
                <td> <?php echo i18n::getTranslation("bonusReceived") ?>:</td>
                <td>
                    <span data-bind="text: QuickDeposit.currencySymbol + QuickDeposit.bonusReceived()"></span>
                </td>
            </tr>
            <tr>
                <td> <?php echo i18n::getTranslation("currentBalance") ?>:</td>
                <td>
                    <span data-bind="text: QuickDeposit.currencySymbol + QuickDeposit.balance()"></span>
                </td>
            </tr>
        </table>


        <button class="cancel-button radius secondary" data-bind="click: QuickDeposit.cancel">Close</button>

    </div>

    <!-- /Quick deposit Success -->

</div>

<!-- /Quick Deposit Container -->
