<?php
$adyen_encrypted_form_expiry_generationtime = date("c");
?>

<form id="adyenform" name="adyenform" style="display: none">
    <input type="text" data-bind="value: Cashier.addingNewMethod.card.number" data-encrypted-name="number" />
    <input type="text" data-bind="value: Cashier.addingNewMethod.card.firstName() + ' ' + Cashier.addingNewMethod.card.lastName()" data-encrypted-name="holderName" />
    <input type="text" data-bind="value: Cashier.addingNewMethod.card.expiryMonth" data-encrypted-name="expiryMonth" />
    <input type="text" data-bind="value: Cashier.addingNewMethod.card.expiryYear" data-encrypted-name="expiryYear" />
    <input type="text" data-bind="value: Cashier.addingNewMethod.card.cvn" data-encrypted-name="cvc" />
    <input type="hidden" value="<?php echo $adyen_encrypted_form_expiry_generationtime;?>" data-encrypted-name="generationtime" />
    <input type="submit" id="btnAdyenformSubmit"/>
</form>

<!-- we need this for the adyen encryption -->
<script type="text/javascript" src="<?php echo config("adyen");?>"></script>