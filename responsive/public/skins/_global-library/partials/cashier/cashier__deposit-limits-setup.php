<section>

    <h1 class="cashier__section__title">Set Your Limits</h1>

    <p>
     You can choose to set a deposit limit here, using the dropdown menu below. Please note, any decrease of limits will happen at midnight, but any increase in limits will have to be re-confirmed after 24 hours.
    </p>

</section>


<section class="limits-setup-form">
    <div class="row">
        <!--  Period  -->
        <select class="large-4 medium-4 small-12 columns" style="display: none" data-bind="visible: !Cashier.playerLimits.allowOnlyMonthlyLimit(),
        value: Cashier.playerLimits.selectedPeriod, enable: Cashier.playerLimits.editMode() !== 'edit'">
            <option value="monthly">Monthly</option>
            <option value="weekly">Weekly</option>
            <option value="daily">Daily</option>
        </select>
    </div>
    <div class="row">
        <!--  Monthly  -->
        <select class="large-4 medium-4 small-12 columns" style="display: none" type="text" maxlength="50"
                data-bind="options:Cashier.playerLimits.monthlyLimits,
        value: Cashier.playerLimits.selectedMonthlyLimit,
        optionsValue: 'amount',
        optionsText: 'amountToDisplay',
        visible: Cashier.playerLimits.selectedPeriod() === 'monthly'"></select>
    </div>

    <div class="row">
        <!--  Weekly  -->
        <select class="large-4 medium-4 small-12 columns" style="display: none" type="text" maxlength="50"
                data-bind="options:Cashier.playerLimits.weeklyLimits,
        value: Cashier.playerLimits.selectedWeeklyLimit,
        optionsValue: 'amount',
        optionsText: 'amountToDisplay',
        visible: Cashier.playerLimits.selectedPeriod() === 'weekly'"></select>
    </div>

    <div class="row">
        <!--  Daily  -->
        <select class="large-4 medium-4 small-12 columns" style="display: none" type="text" maxlength="50"
                data-bind="options:Cashier.playerLimits.dailyLimits,
        value: Cashier.playerLimits.selectedDailyLimit,
        optionsValue: 'amount',
        optionsText: 'amountToDisplay',
        visible: Cashier.playerLimits.selectedPeriod() === 'daily'"></select>
    </div>

    <div class="row">or</div>

    <div class="row">
        <div class="large-4 medium-4 small-12 columns enter-amount">
            <input  type="text" placeholder="Enter Amount" data-bind="value: Cashier.playerLimits.freeAmountEntered">
        </div>
    </div>

    <p>
        <span style="display: none" data-bind="visible: Cashier.playerLimits.invalidAmount, text: Cashier.playerLimits.invalidAmount" class="error"></span>
    </p>

</section>

<button class="button small success radius" data-bind="click: Cashier.playerLimits.cancelSetup">Cancel
</button>
<button class="button secondary small radius" data-bind="click: Cashier.playerLimits.validatesLimit">Ok
</button>