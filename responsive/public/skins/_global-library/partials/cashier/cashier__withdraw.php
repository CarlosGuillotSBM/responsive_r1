<section data-bind="visible: Cashier.activePanel() === 'WITHDRAW'" class="cashier__section" style="display: none">
    <!--  WITHDRAW PANEL  -->
    <div data-bind="visible: Cashier.withdrawal.operation() === 'WITHDRAW'">
        <h1 class="cashier__section__title"><?php echo i18n::getTranslation("withdrawalDetails") ?></h1>
        <div class=''>
            <div class='cashier__section__summary'>
                <div class='cashier__section__summary__title'> <?php echo i18n::getTranslation("totalBalance") ?>:</div>
                <div class='cashier__section__summary__amount'>
                    <span data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.totalBalance()"></span>
                </div>
                <div data-bind="visible: Cashier.withdrawal.bonusToBeForfeited() > 0" class="extra-info"> ( <?php echo i18n::getTranslation("includesBonusFunds") ?> )</div>
            </div>
            <div class='cashier__section__summary'>
                <div class='cashier__section__summary__title'> <?php echo i18n::getTranslation("availableToWithdraw") ?>:</div>
                <div class='cashier__section__summary__amount'>
                    <span data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.availableToWithdraw()"></span>
                </div>
                <div class="extra-info" data-bind="visible: Cashier.withdrawal.realBalance() > Cashier.withdrawal.availableToWithdraw()"> ( <?php echo i18n::getTranslation("realFundsIncludingWLimits") ?> )</div>
                <div data-bind="visible: parseFloat(Cashier.withdrawal.withdrawalsPending()) > 0 && parseFloat(Cashier.withdrawal.withdrawalsPending()) < 10" class="extra-info">
                    <?php echo i18n::getTranslation("yourCurrPendingWithdrawals") ?> <span data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.withdrawalsPending()"></span>
                </div>
            </div>
        </div>
        <div class="cashier__section__withdraw-amount">
            <!-- slider -->
            <label data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.amountToWithdraw()"> </label>
            <div class="range-slider__wrapper">
                <div data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.minWithdraw()" class="range-slider__limit"></div>
                <div class="range-slider__slider">
                    <div id="ui-withdrawal-slider"></div>
                </div>
                <div data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.availableToWithdraw()" class="range-slider__limit"></div>
            </div>
            <!-- /slider -->
            <div class="clearfix"></div>
            <!-- CTA -->
            <div class="cashier__cta--alone">
                <button data-bind="click: Cashier.doWithdrawal,
                enable: Cashier.withdrawal.buttonActive,
                text: 'Withdraw ' + Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.amountToWithdraw()" class="success radius">
                    <?php echo i18n::getTranslation("withdraw") ?>
                </button>
            </div>
            <!-- /CTA -->
        </div>
        <div class="clearfix"></div>
        <br>
        <!-- WARNING -->
        <div class='cashier__section__description'>

            <p>Please note that there is a £5 charge on withdrawal requests of less than £10.</p>
            <!-- <p data-bind="visible: Cashier.withdrawal.realBalance() > Cashier.withdrawal.availableToWithdraw()">
                <?php echo i18n::getTranslation("emailEnquiry1") ?> <span data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.availableToWithdraw()"></span>
                <?php echo i18n::getTranslation("emailEnquiry2") ?> <a data-bind="text: Cashier.withdrawal.enquiryEmail, attr: { 'href': 'mailto:' + Cashier.withdrawal.enquiryEmail() }"></a>

                <a data-bind="text: Cashier.withdrawal.securityEmail, attr: { 'href': 'mailto:' + Cashier.withdrawal.securityEmail() }"></a>
            </p>-->
            <div data-bind="visible: Cashier.withdrawal.bonusToBeForfeited() > 0">
                <h5><span class="warning-title"><?php echo i18n::getTranslation("warning") ?></span></h5>
                <p>
                    <?php echo i18n::getTranslation("youCurrentlyHave") ?>
                    <strong data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.bonusToBeForfeited()"></strong>
                    <?php echo i18n::getTranslation("fundsInBonus") ?>
                    <strong data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.bonusToBeForfeited()"></strong>
                    <?php echo i18n::getTranslation("fundsInBonus2") ?>
                </p>
            </div>
        </div>
        <!-- /WARNING -->
    </div>
    <!--  /WITHDRAW PANEL  -->
    <!--  RESUME PANEL  -->
    <div data-bind="visible: Cashier.withdrawal.operation() === 'RESUME'" class="cashier__section__container">
        <h1 class="cashier__section__title"> <?php echo i18n::getTranslation("withdrawalSuccessful") ?> </h1>
        <p class="withdrawal-response-1">
            <?php echo i18n::getTranslation("withdrawRequest1") ?>
            <strong data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.amountToWithdraw()"></strong>
            <?php echo i18n::getTranslation("withdrawRequest2") ?>
            <strong data-bind="text: Cashier.withdrawal.txWithdrawID"></strong>.</p>
        <p class="withdrawal-response-2">
            <?php echo i18n::getTranslation("withdrawRequest3") ?>
            <strong data-bind="text: Cashier.withdrawal.withdrawHoursPending()"></strong>
            <?php echo i18n::getTranslation("withdrawRequest4") ?>.
            <p class="withdrawal-response-2">
                <?php echo i18n::getTranslation("withdrawRequest5") ?>
                <strong data-bind="text: Cashier.withdrawal.transactionEntity"></strong> <?php echo i18n::getTranslation("withdrawRequest6") ?>.
            </p>
            <ul data-bind="visible: Cashier.withdrawal.bonusToBeForfeited() >0 || Cashier.bonusSpins() > 0 || Cashier.prizeWheelSpins() > 0 || Cashier.bingoCards() > 0">
                <p class="withdrawal-response-2"><?php echo i18n::getTranslation("youForfeited") ?></p>
                <li data-bind="visible: Cashier.withdrawal.bonusToBeForfeited() > 0">
                    <strong data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.bonusToBeForfeited()"></strong>
                    <?php echo i18n::getTranslation("ofBonusFunds") ?>.
                </li>
                <li data-bind="visible: Cashier.lostSpins() > 0">
                    <strong data-bind="text: Cashier.lostSpins"></strong> Free Spins.
                </li>
                <li data-bind="visible: Cashier.lostPrizeWheel() > 0">
                    <strong data-bind="text: Cashier.lostPrizeWheel"></strong> Prize Wheels.
                </li>
                <li data-bind="visible: Cashier.bingoCards() > 0">
                    <strong data-bind="text: Cashier.bingoCards"></strong> Bingo Cards.
                </li>
            </ul>
            <div class=''>
                <div class='cashier__section__summary'>
                    <div class='cashier__section__summary__title'> <?php echo i18n::getTranslation("currPendingWithdrawals") ?>:</div>
                    <div class='cashier__section__summary__amount'>
                        <strong data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.withdrawalsPending()"></strong>
                        <span data-bind="visible: Cashier.withdrawal.withdrawalsPending() > 0 && Cashier.withdrawal.withdrawalsPending() < 10">*</span>
                    </div>
                </div>
                <div class='cashier__section__summary'>
                    <div class='cashier__section__summary__title'> <?php echo i18n::getTranslation("yourCurrBalance") ?>:</div>
                    <div class='cashier__section__summary__amount'><strong data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.balance()"></strong></div>
                </div>
                <p data-bind="visible: Cashier.withdrawal.withdrawalsPending() > 0 && Cashier.withdrawal.withdrawalsPending() < 10" class="withdrawal-response-2">
                    * <?php echo i18n::getTranslation("ifCurrPendingWithLess1") ?>
                    <strong data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.minWithdraw()"></strong>.
                </p>
            </div>
    </div>
    <!--  /RESUME PANEL  -->
    <!--  RETRY PANEL  -->
    <div data-bind="visible: Cashier.withdrawal.operation() === 'RETRY'">
        <span data-bind="text: Cashier.withdrawal.error"></span>
        <div class="cashier__cta">
            <button data-bind="click: Cashier.openWithdraw" class="success radius">
                <?php echo i18n::getTranslation("retry") ?>
            </button>
        </div>

    </div>
    <!--  /RETRY PANEL  -->

    <!-- Player Is Unverified -->
    <div class='cashier__section__description' data-bind="visible: Cashier.withdrawal.operation() === 'UNVERIFIED' && Cashier.withdrawal.playerIsUnverified()">

        <h4><span class="warning-title">You may only withdraw after your account has been verified</span></h4>
        <br>
        <p>
            We need to get your account verified to be eligible for a withdrawal.
            Unfortunately we have been unable to verify your account from the personal details submitted at registration.
            This may have happened for several reasons: there may have been errors on the registered details or you have recently changed your name and/or address etc.
        </p>
        <p>
            In order to assist us in verifying your account please simply email us:
            <ul style="list-style-type: square; padding: 8px">
                <li>A copy of your Driving Licence, Passport or a copy of your Birth Certificate showing your name and date of birth.</li>
                <li>Proof of address (e.g. utility bill or card/bank statement) reflecting your name and address, and dated not older than 3 months.
                    We are only required to see your name, full address, the date it was sent and the letterhead. Please block out all other details.</li>
            </ul>
        </p>
        <p>
            The Documents can be scanned and emailed to: <a data-bind="text: Cashier.withdrawal.securityEmail, attr: { 'href': 'mailto:' + Cashier.withdrawal.securityEmail() }"></a> or faxed quoting your login user name to: +44 (0)203-100-5244 Or,
            if you own a digital camera or mobile phone, you can take a picture of your documents
            - making sure that the details are legible - and email it to: <a data-bind="text: Cashier.withdrawal.securityEmail, attr: { 'href': 'mailto:' + Cashier.withdrawal.securityEmail() }"></a>
        </p>
    </div>
    <!-- /Player Is Unverified -->

</section>