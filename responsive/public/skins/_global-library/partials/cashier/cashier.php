<?php
// TODO: this should go to each skin - they might have different languages!
include_once("_global-library/i18n/cashier-en.php");
?>
<?php if(isset($action) && $action == 'cashier') : ?>
        <div class="cashier">
    <?php else : ?>
    <div class="<?php echo $this->controller; ?>">
<?php endif; ?>

    <div class="cashier-balance-bar">
        <span class="cashier-title-left">Cashier</span>
        <a data-bind="click: Cashier.openLimitsScreen.bind($data,false, true), visible: Cashier.cashierData.pctLimitWarning">
            <span style="text-decoration: underline" class="limit-status" data-bind="text: Cashier.cashierData.pctLimitWarning"></span>
        </a>

        <span data-bind="text: '<?php echo i18n::getTranslation("yourBalance") ?>: ' + balance()"></span>
    </div>
    <!-- NAVIGATION -->
    <div class="row">
        <?php require_once("_global-library/partials/cashier/cashier__main-nav.php") ?>
        <!-- /NAVIGATION -->
        <!-- Cashier content wrapper -->
        <div class="cashier__content-wrapper">
            <!-- ACCOUNTS -->
            <?php require_once("_global-library/partials/cashier/cashier__accounts.php") ?>
            <!-- /ACCOUNTS -->
            <!-- METHODS -->
            <?php require_once("_global-library/partials/cashier/cashier__methods.php") ?>
            <!-- /METHODS -->
            <!-- ENTER DETAILS -->
            <?php require_once("_global-library/partials/cashier/cashier__new-method.php") ?>
            <!-- /ENTER DETAILS -->
            <!-- DEPOSIT -->
            <?php require_once("_global-library/partials/cashier/cashier__deposit.php") ?>
            <?php if (config("adyen") != "") require_once("_global-library/partials/cashier/cashier__barclaycard.php"); ?>
            <!-- /DEPOSIT -->
            <!-- WITHDRAW -->
            <?php require_once("_global-library/partials/cashier/cashier__withdraw.php") ?>
            <!-- /WITHDRAW -->
            <!-- REVERSE -->
            <?php require_once("_global-library/partials/cashier/cashier__reverse.php") ?>
            <!-- /REVERSE -->
            <!-- ACCOUNT VERIFICATION -->
            <?php require_once("_global-library/partials/cashier/cashier__account-verification.php") ?>
            <!-- /ACCOUNTVERIFICATION -->
            <!-- RETRY CVN -->
            <?php require_once("_global-library/partials/cashier/cashier__retry-cvn.php") ?>
            <!-- /RETRY CVN -->
            <!-- EXPIRY DATE -->
            <?php require_once("_global-library/partials/cashier/cashier__expiry-date.php") ?>
            <!-- /EXPIRY DATE -->
            <!-- BANK DETAILS -->
            <?php require_once("_global-library/partials/cashier/cashier__bank-details.php") ?>
            <!-- /BANK DETAILS -->
            <!-- DEPOSIT LIMITS -->
            <?php require_once("_global-library/partials/cashier/cashier__deposit-limits.php") ?>
            <!-- /DEPOSIT LIMITS-->
            <!-- URU CHECK DOCUMENT UPLOAD -->
            <?php require_once("_global-library/partials/cashier/cashier__uru-documents.php") ?>
            <?php require_once("_global-library/partials/cashier/cashier__uru-pending.php") ?>
            <!-- /URU CHECK DOCUMENT UPLOAD -->
            <!-- ERROR DISPLAY -->
            <?php require_once("_global-library/partials/cashier/cashier__error.php") ?>
            <!-- /ERROR DISPLAY -->
            <!--        <p>DEBUG</p>-->
            <!--        <pre data-bind="text: ko.toJSON($data.Cashier.deposit, null, 2)"></pre>-->
            
        </div>
    </div>
    <div class="clearfix"></div>
    <?php include("_global-library/partials/cashier/cashier__footer.php") ?>
    <!-- /Cashier content wrapper -->
</div>