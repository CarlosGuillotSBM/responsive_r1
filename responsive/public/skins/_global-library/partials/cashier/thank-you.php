<!-- MIDDLE CONTENT -->
<div class="middle-content__box cashier__thank-you--header">

  <section class="section <?php echo $this->controller; ?>">
    <h1 class="section__title"><?php echo $this->controller; ?></h1>
    <div class="cashier__secure-payment"><img src="/_global-library/images/cashier/secure-payment.png"></div>
    <!-- CONTENT -->
    <!-- PAGE SPECIFIC CONTENT -->
      <?php require_once("_global-library/partials/cashier/cashier__thank-you.php"); ?>
      <!-- END PAGE SPECIFIC CONTENT -->
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->
