<!-- RETRY CVN -->

<div data-bind="visible: Cashier.activePanel() === 'RETRYCVN'" style="display: none">

<!-- Retry CVN form -->
<form>

<section>

    <fieldset class="">
        <label> <?php echo i18n::getTranslation("retryCvn") ?> </label>

        <input data-bind="value: Cashier.correctCVN.cvn" placeholder="CVN" type="tel">
        <div class="cashier__cta">
        	<button data-bind="click: Cashier.retryCVN" class="success radius" ><?php echo i18n::getTranslation("retry") ?></button>
        	<button data-bind="click: Cashier.cancelDeposit" class="secondary radius"><?php echo i18n::getTranslation("cancel") ?></button>
        </div>

    </fieldset>

</section>

</div>