<section>
    <h1 class="cashier__section__title">Deposit limits</h1>

    <div class="panel radius">
        <h6>You are choosing not to set a limit for now.</h6>
       <p>You can set a Deposit Limit at any time later should you wish to.</p>

        <p>By clicking OK I agree to set no limits for now.</p>
        <button class="button small radius success" data-bind="click: Cashier.playerLimits.requestLimitChange">OK</button>

</section>