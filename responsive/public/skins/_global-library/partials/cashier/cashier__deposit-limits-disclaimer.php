<section id="disclaimer">

    <fieldset class="">
        <h1 class="cashier__section__title">Disclaimer</h1>

        <p>
           When you set a deposit limit, you are choosing to cap the amount you can deposit over a day, a week or a month. This puts you in full control of your money and spending limits. Please note that once you have reached your maximum deposit limit, you will not able to add funds to your account or take advantage of selected promotions for the remainder of the time period you have set.

            In line with regulations, any decrease of limits will happen at midnight, but any increase in limits will have
            to be re-confirmed after 24 hours. Limits are calculated from the beginning of each session, Midnight each day,
            Midnight Sunday and Midnight on the last day of the month depending on the period selected.</p>

        <button class="button small success radius" data-bind="click: Cashier.playerLimits.cancelLimits">Cancel
        </button>
        <button class="button secondary small radius" data-bind="click: Cashier.playerLimits.proceedDisclaimer">Continue
        </button>

</section>

