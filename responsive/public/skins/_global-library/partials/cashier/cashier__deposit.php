<!-- Deposit amount -->
<section data-bind="visible: Cashier.activePanel() === 'PROMOTIONS'" style="display: block" class="cashier__section">
    <div class="cashier__section__extra-info">
        <span class="cashier__section__extra-info__lock"><img src="/_global-library/images/cashier/lock-circle-green.svg"></span>
         <img data-bind="attr: {src: '/_global-library/images/cashier/' + Cashier.deposit.methodIcon() }">
    </div>
    <h1 class="cashier__section__title cashier__deposit__offer"><?php echo i18n::getTranslation("chooseOfferBelow") ?></h1>
    <span class="solid-border-how-deposit"></span>
    <!-- Promotions -->
    <div id="promo-container" class="cashier__section__container">
        <div class="cashier__no-bonus">
            <div class="cashier__grid__cell">
                <div style="padding-top: 11px;" data-bind="click: Cashier.deposit.iDoNotWantBonus" class="cashier__grid__cell__content">Deposit without a bonus</div>
            </div>
        </div>
        
        <h1 class="cashier__section__title__bonus" style="padding-left: 10px;"><?php echo i18n::getTranslation("chooseYourBonus") ?></h1>
        <div data-bind="foreach: Cashier.deposit.promos" class="cashier__bonus-choices">
            <div data-bind="click: $root.Cashier.selectedPromo" class="cashier__grid__cell radius">
                <div data-bind="html: Description,
                        css: { 'active-state--main' : $root.Cashier.deposit.promoId() == PromoID && !$root.Cashier.deposit.iDoNotWantBonus() }"
                    class="cashier__grid__cell__content">   
                </div>
            </div>
        </div>
        <div data-bind="visible: Cashier.deposit.promoCodeAvailable()"  class="cashier__promo-code">
            <div class="row">      
            <?php 
            if(strpos($_SERVER["REQUEST_URI"],"welcome")): ?>
            <div class="large-12 columns">
                <h1 class="cashier__section__title__bonus"><?php echo i18n::getTranslation("promoCodeTitle") ?></h1>
                    <div class="row">
                        <div class="small-12 columns cashier__enterpromo-field">
                            <input type="text" class="cashier__promo__input" placeholder="<?php echo i18n::getTranslation("enterPromoCode") ?>"
                            data-bind="
                            value: Cashier.deposit.promoCode,
                            hasfocus: Cashier.deposit.iHavePromoCode() && Cashier.activePanel() === 'PROMOTIONS',
                            valueUpdate:'input',
                            css: { 'promo-is-valid': Cashier.deposit.isValidPromo() !== '' && Cashier.deposit.isValidPromo(), 'promo-is-invalid': Cashier.deposit.isValidPromo() !== '' && !Cashier.deposit.isValidPromo() }">
                        </div>
                <?php else: ;?>
                <div class="large-12 columns" style="margin-top: 30px;">
                    <h1 class="cashier__section__title__bonus"><?php echo i18n::getTranslation("promoCodeTitle") ?></h1>
                    <div class="row">
                        <div class="large-3 medium-6 small-6 cashier__enterpromo-field">
                            <input type="text" class="cashier__promo__input" placeholder="<?php echo i18n::getTranslation("enterPromoCode") ?>"
                            data-bind="value: Cashier.deposit.promoCode, hasfocus: Cashier.deposit.iHavePromoCode() && Cashier.activePanel() === 'PROMOTIONS', valueUpdate:'input',
                            css: { 'promo-is-valid': Cashier.deposit.isValidPromo() !== '' && Cashier.deposit.isValidPromo(), 'promo-is-invalid': Cashier.deposit.isValidPromo() !== '' && !Cashier.deposit.isValidPromo() }">
                        </div>
                <?php endif;?>
                        <div class="large-9 medium-6 small-6 cashier__enterpromo__cta">
                            <a class="button cashier__validation radius" data-bind="click: Cashier.checkPromoCode"><?php echo i18n::getTranslation("validateCode");?></a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- offer -->
        </div> <!-- END row -->
            <!-- /offer -->

        <div class="cashier__cta">
        <div class="cashier__cta__wrapper cta-deposit">
            <button data-bind="click: Cashier.goBack" class="secondary radius"> Back </button>
            <button data-bind="click: Cashier.showPanel.bind($data, 'AMOUNTS'), visible: Cashier.deposit.isValidPromo()" class="success cashier__next radius"><?php echo i18n::getTranslation("next") ?></button>
        </div>
    </div>
    <!-- /Promotions -->
</section>
<section data-bind="visible: Cashier.activePanel() === 'AMOUNTS'" style="display: none" class="cashier__section">
    <div class="cashier__section__extra-info">
        <span class="cashier__section__extra-info__lock"><img src="/_global-library/images/cashier/lock-circle-green.svg"></span>
    </div>
        <!-- Amounts -->
    <div class="cashier__section__container">
        <h1 class="cashier__section__title cashier__amounts"><?php echo i18n::getTranslation("selectAmount") ?></h1>
        <span class="solid-border-amounts"></span>
        <div class="row">
            <div class="cashier__method-description large-4 medium-12 small-12">
                <p class="cashier__payment-method">Payment method selected:</p>
                <div>
                    <span><img data-bind="attr: {src: '/_global-library/images/cashier/' + Cashier.deposit.methodIcon() }"><span class="card-description" data-bind="text: Cashier.accountDescription()"></span></span>
                </div>
            </div>
            <div data-bind="foreach: Cashier.deposit.depositAmounts" class="cashier__section__container-amounts large-8">
            <!-- amount -->
                <div class="cashier__grid__cell--small">
                    <div data-bind="text: $root.Cashier.cashierData.currencySymbol + Amount,
                        click: $root.Cashier.selectedAmount,
                        css: { 'inactive' : $root.Cashier.deposit.promoDepositMin() > Amount && !$root.Cashier.deposit.iDoNotWantBonus(),
                        'active-state--main' : $root.Cashier.deposit.amount() == Amount}"
                    class="cashier__grid__cell__content"></div>
                </div>
            <!-- /amount -->
            </div>
        </div>
        <!-- free amount -->
        <div class="cashier__free-amount">
            <div>
                <label><?php echo i18n::getTranslation("enterOtherAmount") ?></label>
                <input data-bind="value: $root.Cashier.deposit.freeAmount, valueUpdate:'input',
                enable: $root.Cashier.deposit.promoDepositMin() < $root.Cashier.deposit.maxDeposit || $root.Cashier.deposit.iDoNotWantBonus()" placeholder="Enter amount" type="text">
                <div class="clearfix"></div>
            </div>
            <span class="error" data-bind="visible: Cashier.deposit.minDepositError(), text: Cashier.deposit.minDepositError()" style="display: none"></span>
        </div>
        <!-- /free amount -->

        <!-- AMOUNT page PROMO CODE -->
        <div style="display: none" data-bind="visible: !Cashier.deposit.promos().length" class="cashier__promo-code">
            <div class="row">
                <div class="large-8 columns">
                    <h1 class="cashier__section__title__bonus">Do you have a promo code?</h1>
                    <span class="solid-border-amount-promo"></span>
                    <div class="row">
                        <div class="cashier__radio-selection">
                            <div class="cashier__radio">
                                <label for="radio-1" class="radio-label">
                                    <input id="radio-1" name="showPromo" type="radio" value="Yes" data-bind=" checked: Cashier.showPromoCode">Yes
                                </label>
                            </div>

                            <div class="cashier__radio">
                                <label for="radio-2" style="margin-left: 10px;" class="radio-label">
                                    <input id="radio-2" name="showPromo" type="radio" value="No" data-bind=" checked: Cashier.showPromoCode" checked>No
                                </label>
                            </div>
                        </div>
                        <div data-bind= "visible: Cashier.showPromoCode() == 'Yes'">
                            <div class="small-12 columns cashier__enterpromo-field">
                                <input type="text" class="cashier__promo__input" placeholder="<?php echo i18n::getTranslation("enterPromoCode") ?>"
                                data-bind="
                                value: Cashier.deposit.promoCode,
                                hasfocus: Cashier.deposit.iHavePromoCode() && Cashier.activePanel() === 'PROMOTIONS',
                                valueUpdate:'input',
                                css: { 'promo-is-valid': Cashier.deposit.isValidPromo() !== '' && Cashier.deposit.isValidPromo(), 'promo-is-invalid': Cashier.deposit.isValidPromo() !== '' && !Cashier.deposit.isValidPromo() }">
                            </div>
                            <div class="large-9 medium-6 small-6 cashier__enterpromo__cta">
                                <a class="button cashier__validation radius" data-bind="click: Cashier.checkPromoCode"><?php echo i18n::getTranslation("validateCode");?></a>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
         <!-- Neteller Secure Id -->
        <div style="display: none;" class="cashier__free-amount" data-bind="visible: Cashier.deposit.showNetellerSecureId">
            <div>
                <h1 class="cashier__section__title__neteller">Neteller Secure ID or Authentication Code</h1>
                <input type="text" placeholder="<?php echo i18n::getTranslation("enterSecureId") ?>" data-bind="value: Cashier.deposit.netellerSecureId">
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- /Neteller Secure Id -->
        <div class="clearfix"></div>
    </div>
    <!-- /Amounts -->
    <div class="cashier__section__container">
        <div data-bind="visible: Cashier.deposit.needsCvn" class="cashier__cta">
            <h1 class="cashier__section__title__bonus">Enter CVN to proceed</h1>
            <span class="solid-border-amount-cvn"></span>
            <div class="cashier__cvn">
                <div class="large-12">
                    <input style="float: left;" data-bind="value: Cashier.deposit.cvn" placeholder="<?php echo i18n::getTranslation("cvn") ?>" type="tel">
                    <img style="float: left;" src="/_global-library/images/cashier/cvn-logo.svg" alt="Enter CVN">
                </div>
                <div class="large-12" style="float: left; width: 100%;">
                    <span>Last 3 digits on the back of your card.</span>
                </div>
            </div>
        </div>
    </div>
    <!-- SUBMIT BUTTON -->
    <div class="cashier__cta">
        <div style="display: none;" data-bind="visible: Cashier.deposit.showAcknowledged">
            <input data-bind="checked: Cashier.deposit.acknowledged" type="checkbox" id="cbUkgcAcknowledge">
            <label for="cbUkgcAcknowledge"><?php echo i18n::getTranslation("UKGCAcknowledge"); ?>
                <a class="cbUkgcAcknowledge-readmore" target="_blank" href="http://www.gamblingcommission.gov.uk/Consumers/Protection-of-customer-funds.aspx"> <?php echo i18n::getTranslation("readMore"); ?></a>
            </label>
            
        </div>
            
        <div class="cashier__cta">
            <div class="cashier__cta__wrapper">
                <!-- TODO: data-bind -->
                <button data-bind="click: Cashier.makeDeposit,
                enable: Cashier.canMakeDeposit,
                text: Cashier.deposit.amount() ? '<?php echo i18n::getTranslation("deposit") ?> ' + Cashier.cashierData.currencySymbol + Cashier.deposit.amount() + Cashier.deposit.depositPromoInfo() : '<?php echo i18n::getTranslation("deposit") ?>'"
                class="success cashier__cta-deposit radius">
                </button>
                <button data-bind="click: Cashier.goBack" class="secondary radius"><?php echo i18n::getTranslation("back") ?></button>
              
            </div>
        </div>
    </div>
        <!-- /SUBMIT BUTTON -->
</section>