<section>

    <h1 class="cashier__section__title">Enter Password</h1>

    <p>
    Please re-enter your account password here to confirm your request.
    </p>

</section>



<div class="row">
<section class=" limits-password">
    <input type="password" placeholder="Enter Your Password" data-bind="value: Cashier.playerLimits.password">
    <span style="display: none" data-bind="visible: Cashier.playerLimits.passwordError, text: Cashier.playerLimits.passwordError" class="error"></span>
</section>
</div>

<div class="row">
<section class="limits-password-buttons">
<button class="button small success radius" data-bind="click: Cashier.openDeposit.bind($data, true)">Cancel
</button>
<button class="button secondary small radius" data-bind="click: Cashier.playerLimits.validatesPassword">Ok
</button>

</section>
</div>