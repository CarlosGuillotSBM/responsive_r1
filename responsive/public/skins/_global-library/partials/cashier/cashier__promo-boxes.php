<div class="cashier__promo-boxes">

	<div class="cashier__promo-box" data-amount="5" data-promoid="0">


		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £5</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark" style="display: none"> Bonus amount </span>

		</div>


	</div>


	<div class="cashier__promo-box" data-amount="10" data-promoid="0">


		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £10</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark" style="display: none"> Bonus amount </span>

		</div>

	</div>

	<div class="cashier__promo-box" data-amount="20" data-promoid="0">


		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £20</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark" style="display: none"> Bonus amount </span>

		</div>

	</div>


	<div class="cashier__promo-box" data-amount="30" data-promoid="0">


		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £30</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark"> Bonus amount </span>

		</div>


	</div>


	<div class="cashier__promo-box" data-amount="50" data-promoid="0">


		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £50</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark"> Bonus amount </span>

		</div>


	</div>


	<div class="cashier__promo-box" data-amount="75" data-promoid="0">


		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £75</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark"> Bonus amount </span>

		</div>

	</div>


	<div class="cashier__promo-box" data-amount="100" data-promoid="0">


		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £100</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark"> Bonus amount </span>

		</div>


	</div>

	<div class="cashier__promo-box" data-amount="200" data-promoid="0">



		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £200</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark"> Bonus amount </span>

		</div>


	</div>


	<div class="cashier__promo-box" data-amount="500" data-promoid="0">

		<div class="deposit-col amount-col">
			<span class="ui-amount-td"> Deposit  &nbsp; £500</span>

		</div>

		<div class="bonus-col">
			<span class="ui-question-mark"> Bonus amount </span>

		</div>


	</div>

</div>