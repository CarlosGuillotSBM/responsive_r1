<?php

$csymbol = isset($_COOKIE["CurrencySymbol"]) ? $_COOKIE["CurrencySymbol"] : "";
$amount = isset($_REQUEST["Amount"]) ? $_REQUEST["Amount"] : "";
$bonus = isset($_REQUEST["Bonus"]) ? $_REQUEST["Bonus"] : "";
$freeSpins = isset($_REQUEST["FreeSpins"]) ? $_REQUEST["FreeSpins"] : "";
$freeCards = isset($_REQUEST["FreeCards"]) ? $_REQUEST["FreeCards"] : "";
$sessionID =  (isset($_COOKIE["SessionID"])) ? $_COOKIE["SessionID"] : '';
$threeRadicalPrize1Name = isset($_REQUEST["ThreeRadPrize1Name"]) ? $_REQUEST["ThreeRadPrize1Name"] : "";
$threeRadicalPrize1Amount = isset($_REQUEST["ThreeRadPrize1Amount"]) ? $_REQUEST["ThreeRadPrize1Amount"] : "";
$threeRadicalPrize2Name = isset($_REQUEST["ThreeRadPrize2Name"]) ? $_REQUEST["ThreeRadPrize2Name"] : "";
$threeRadicalPrize2Amount = isset($_REQUEST["ThreeRadPrize2Amount"]) ? $_REQUEST["ThreeRadPrize2Amount"] : "";
$freeGamesPrize1Name = isset($_REQUEST["FreeGamesPrize1Name"]) ? $_REQUEST["FreeGamesPrize1Name"] : "";
$freeGamesPrize1Amount = isset($_REQUEST["FreeGamesPrize1Amount"]) ? $_REQUEST["FreeGamesPrize1Amount"] : "";
$freeGamesPrize2Name = isset($_REQUEST["FreeGamesPrize2Name"]) ? $_REQUEST["FreeGamesPrize2Name"] : "";
$freeGamesPrize2Amount = isset($_REQUEST["FreeGamesPrize2Amount"]) ? $_REQUEST["FreeGamesPrize2Amount"] : "";

$playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;

$balance = isset($_REQUEST["Balance"]) ? $_REQUEST["Balance"] : "";
$source = isset($_REQUEST["Source"]) ? $_REQUEST["Source"] : "";
$depositCount = isset($_REQUEST["DepositSuccessCount"]) ? $_REQUEST["DepositSuccessCount"] : "";
$cardDescriptor = config("cardDescriptor");

if ($amount == "") {
    exit();
}

if (config("lang") == "el") {
    require_once("_global-library/i18n/cashier-el.php");
} else {
    require_once("_global-library/i18n/cashier-en.php");
}

?>
<section class="cashier__section">





    <div class="cashier__content-wrapper--alone">
        <h1 class="cashier__section__title"> <?php echo i18n::getTranslation("thankYou") ?> </h1>
        <span class="cashier__section__notice"> <?php echo i18n::getTranslation("yourDepositSuccessful") ?> </span>
 <div class="clearfix"></div>
        <!-- Info table -->
        <table class="cashier__section__table nonresponsive" style="width:100% !important">
            <tr>
                <td> <?php echo i18n::getTranslation("depositAmount") ?>:</td>
                <td><?php echo $csymbol . " " . $amount; ?></td>
            </tr>
            <tr>
                <td> <?php echo i18n::getTranslation("bonusReceived") ?>:</td>
                <td><?php echo $csymbol . " " . $bonus; ?></td>
            </tr>
            <?php
            if(($threeRadicalPrize1Name!="" && $threeRadicalPrize1Amount!="")) : 
                    $threeRadicalText=$threeRadicalPrize1Name;
                if($threeRadicalPrize2Name!="" && $threeRadicalPrize2Amount!="") : 
                    $threeRadicalText=$threeRadicalText." + ".$threeRadicalPrize2Name;
                endif;
            ?>
            <tr>
                <td> Daily Play Prize:</td>
                <td><?php echo $threeRadicalText; ?> 
                <button class="cashier__3Radical-cta" data-bind="click: ThreeRadical.requestToken">Use now</button></td>
            </tr>
            <?php endif; ?>
            <?php
            if(($freeGamesPrize1Name!="" && $freeGamesPrize1Amount!="")) : 
                    $freeGamesText=$freeGamesPrize1Amount." ".$freeGamesPrize1Name;
                if($freeGamesPrize2Name!="" && $freeGamesPrize2Amount!="") : 
                    $freeGamesText=$freeGamesText." + ".$freeGamesPrize2Name;
                endif;
            ?>
            <tr>
                <td> Free Game:</td>
                <td><?php echo $freeGamesText; ?> 
                <a class="prize-wheel__notification__icon hide-for-large-up" style="width: 100%" href="<?=config("PrizeWheelURL")."?SkinID=".config("SkinID")."&uid=".$playerID."&guid=".$sessionID."&gameid=531"?>" target="prizeWheel">
        <div class="prize-wheel__icon"  style="width: 100%"></div>
    </a>
    <a class="button cashier__prizeWheel-cta-mobile" href="<?=config("PrizeWheelURL")."?SkinID=".config("SkinID")."&uid=".$playerID."&guid=".$sessionID."&gameid=531"?>" target="prizeWheel">
    Use now
            </a>
                <button class="cashier__prizeWheel-cta-desktop" data-bind="click: GameLauncher.openGame" data-gameid="gam-prize-wheel">Use now</button></td>
            </tr>
            <?php endif; ?>
            <tr>
                <td> <?php echo i18n::getTranslation("freeSpins") ?>:</td>
                <td><?php echo $freeSpins; ?></td>
            </tr>
            <tr>
                <td> <?php echo i18n::getTranslation("freeCards") ?>:</td>
                <td><?php echo $freeCards; ?></td>
            </tr>
            <tr>
                <td> <?php echo i18n::getTranslation("currentBalance") ?>:</td>
                <td><?php echo $csymbol . " " . $balance; ?></td>
            </tr>
        </table>
 <div class="clearfix"></div>
        <div class="deposit-thankyou-msg">
            <div class="title"><?php echo i18n::getTranslation("transactionAppearAs") ?> <strong><?php echo $cardDescriptor; ?></strong> <?php echo i18n::getTranslation("onBankStatement") ?>.</div>
        </div>

    </div>

    <div class="clearfix"></div>
    <div class="cashier__thank-you-cta">

        <?php if (isset($_COOKIE['bingoclient'])) : ?>

            <button class="success radius" onClick="window.close()">
                <a><?php echo i18n::getTranslation("goToLobbyButton" . config("SkinID")); ?>   </a>
            </button>

        <?php else : ?>

            <a href="<?php echo getHomeURL(); ?>" data-hijack="true">
                <button class="success radius">
                        <?php echo i18n::getTranslation("goToLobbyButton" . config("SkinID")); ?>
                </button>
            </a>

        <?php endif; ?>

    </div>

</section>

<script type="application/javascript">
    skinModules.push({ id: "DepositGoogleManager",
        options: {
            source: "<?php echo $source; ?>",
            amount: <?php echo $amount; ?>,
            depositCount: <?php echo $depositCount; ?>
        }
    });
    window._vis_opt_queue = window._vis_opt_queue || [];
    window._vis_opt_queue.push(function() {_vis_opt_revenue_conversion("<?php echo $amount; ?>");});
</script>
