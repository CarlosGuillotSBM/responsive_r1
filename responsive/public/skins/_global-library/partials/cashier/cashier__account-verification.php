<!-- ACCOUNT VERIFICATION UI -->
<section data-bind="visible: Cashier.activePanel() === 'ACCOUNTVERIFICATION'" style="display: none;">
            
            <div class="account-verification verification">
                <h1 class="cashier__section__title">Account Verification</h1> 
                <p class=".cashier__section__description">To ensure that your transactions are processed swiftly, we need to fully verify your account. <a href="/account-verification/" class="help-link"> More info.</a></p>

                <section class="cashier__section-main">
                    <!-- UPLOAD SCREEN -->
                    <div class="cashier__section-upload" data-bind="visible: Cashier.accountVerification.currentAction() === 'account-verification-upload'" style="display: none">
                        <div class="cashier__section-upload-files">
                            <img src="/_global-library/images/cashier/account-verification/icon-padlock.svg" alt="" style="float:left; padding-right: 20px;" />
                            <p style="padding-left: 50px;">                            
                                <span data-bind="text: Cashier.accountVerification.mandatoryType"><!--Please upload the following document(s) via our secure portal below:--></span><br/>
                                <!-- <span data-bind="text: Cashier.accountVerification.filesToUpload"></span>. -->
                                <ul data-bind="foreach: Cashier.accountVerification.filesToUploadSOF">
                                    <li data-bind="html: filename" style="list-style: disc"></li>
                                </ul>
                            </p>
                             <p>    
                                 <span data-bind="text: Cashier.accountVerification.mandatoryTypeKYC"><!--Please upload the following document(s) via our secure portal below:--></span><br/>
                                <!-- <span data-bind="text: Cashier.accountVerification.filesToUpload"></span>. -->
                                <ul data-bind="foreach: Cashier.accountVerification.filesToUploadKYC">
                                    <li data-bind="html: filename" style="list-style: disc"></li>
                                </ul>
                            </p>
                            
                        </div>

                        <!-- Choose file CTA -->
                        <div class="cashier__section-cta" style="margin-top: 20px;">
                            <label for="ui-file-uploads">Choose files</label>
                            <input type="file" id="ui-file-uploads" name="ui-file-uploads" accept=".jpg, .jpeg, .png, .pdf" multiple="" style="opacity: 0;">
                        </div>
                        
                        <!--Uploaded files-->
                        
                        <div data-bind="foreach: Cashier.accountVerification.uploadedFiles, visible: Cashier.accountVerification.uploadedFiles().length > 0" style="display: none">
                            <div class="uploaded-files-success">
                                <img src="/_global-library/images/cashier/account-verification/icon-doc-success.svg" alt=""/>
                                <p><span class="truncate" data-bind="text: name"></span><img data-bind="click: $root.Cashier.accountVerification.removeFile.bind($data)" class="uploaded_files-delete" src="/_global-library/images/cashier/account-verification/icon-close.svg" alt=""/></p>
                            </div>
                        </div>
                        
                         <!--Send CTA-->
                        <div class="cashier__section-cta">
                            <button class="send-file disabled" data-bind="click: Cashier.accountVerification.upload, css: {disabled: Cashier.accountVerification.uploadedFiles().length === 0 }">Send</button>
                        </div>
                    </div>
                    <!--UPLOAD SCREEN END-->

                    <!--Please Note-->
                    <div class="cashier__section-notes">
                    <div class="notes-border"></div>
                        <span>Please note:</span>
                        <ul>
                            <li>If more than one document is required, you will need to upload them all before you can press ‘Send’</li>
                            <li>Total file size cannot exceed 10MB</li>
                            <li>If you do not provide your documents, some restrictions may be imposed on your account, and on other linked accounts on our sister sites</li>
                        </ul>
                    </div>

                    <!-- LOADING SCREEN -->
                    <div class="cashier__section-loading" data-bind="visible: Cashier.accountVerification.currentAction() === 'account-verification-loading'" style="display: none">
                        <div class="loading-icon">
                            <img src="/_global-library/images/cashier/account-verification/icon-loading.svg" alt=""/>
                        </div>
                        <p class="sending-message">Sending</p>
                    </div>
                    <!-- LOADING SCREEN -->

                    <!-- SUCCESSFUL SCREEN -->
                    <div class="cashier__section-status" data-bind="visible: Cashier.accountVerification.currentAction() === 'account-verification-success'" style="display: none">
                        <div class="success-icon">
                            <img src="/_global-library/images/cashier/account-verification/icon-security-success.svg" alt=""/>
                        </div>
                        <h4 class="success-heading">Submission successful</h4>
                        <p class="success-message">Thank you. We will be in touch to let you know whether your documents have been successfully validated.</p>
                    </div>
                    <!-- SUCCESSFUL SCREEN -->

                    <!-- FAILED SCREEN -->
                    <div class="cashier__section-status" data-bind="visible: Cashier.accountVerification.currentAction() === 'account-verification-failed'" style="display: none">
                        <div class="failed-icon">
                            <img src="/_global-library/images/cashier/account-verification/icon-security-error.svg" alt=""/>
                        </div>
                        <h4 class="failed-heading">Submission unsuccessful</h4>
                        <a href ="#" class="failed-message">Try again</a>
                    </div>
                    <!-- FAILED SCREEN -->
                </section> <!--End of main section-->

                <div class="cashier__support-team">
                    <p>Need some help? Please contact our <a href="/help/" class="help-link">Support Team</a>, available 24/7.</p>
                </div>
            </div>

</section>