<section data-bind="visible: Cashier.activePanel() === 'BANKDETAILS'" class="cashier__section" style="display: none"
xmlns="http://www.w3.org/1999/html">

<!--  DETAILS  -->

<h1 class="cashier__section__title"> <?php echo i18n::getTranslation("bankDetails") ?> </h1>

<div class='' id="ui-wire-status-info">

    <div data-bind="visible: Cashier.bankDetails.wireStatus() === 0" class='small-12 summary'>
        <?php echo i18n::getTranslation("withdrawalsWillNotBeProcessed") ?>.
    </div>

    <div data-bind="visible: Cashier.bankDetails.wireStatus() === 1" class='small-12 summary'>
        <?php echo i18n::getTranslation("withdrawalsWillBeProcessed") ?>.
    </div>

</div>

<div class='superclear'>&nbsp;</div>

<div class="cashier__section__form">

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("bankName") ?> * </label>
        <input data-bind="value: Cashier.bankDetails.bankName" type="text" placeholder="<?php echo i18n::getTranslation("bankName") ?>">
    </div>
    
    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("accountName") ?> * </label>
        <input data-bind="value: Cashier.bankDetails.accountName" type="text" placeholder="<?php echo i18n::getTranslation("accountName") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("accountNumber") ?> * </label>
        <input data-bind="value: Cashier.bankDetails.accountNumber" type="text" placeholder="<?php echo i18n::getTranslation("accountNumber") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("branchCode") ?> </label>
        <input data-bind="value: Cashier.bankDetails.branchCode" type="text" placeholder="<?php echo i18n::getTranslation("branchCode") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("swiftCode") ?> * </label>
        <input data-bind="value: Cashier.bankDetails.swiftCode" type="text" placeholder="<?php echo i18n::getTranslation("swiftCode") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("country") ?> </label>
        <input data-bind="value: Cashier.bankDetails.country" type="text" placeholder="<?php echo i18n::getTranslation("country") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("address") ?> * </label>
        <input data-bind="value: Cashier.bankDetails.address" type="text" placeholder="<?php echo i18n::getTranslation("address") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("city") ?> * </label>
        <input data-bind="value: Cashier.bankDetails.city" type="text" placeholder="<?php echo i18n::getTranslation("city") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("state") ?> </label>
        <input data-bind="value: Cashier.bankDetails.state" type="text" placeholder="<?php echo i18n::getTranslation("state") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("postcode") ?> </label>
        <input data-bind="value: Cashier.bankDetails.postCode" type="text" placeholder="<?php echo i18n::getTranslation("postcode") ?>">
    </div>

    <div class="cashier__form-group">
        <label> <?php echo i18n::getTranslation("correspondingBankName") ?> </label>
        <input data-bind="value: Cashier.bankDetails.correspondingBankName" type="text" placeholder="<?php echo i18n::getTranslation("correspondingBankName") ?>">
    </div>

    <div class="clearfix"></div>

    <div class=''>
        <input data-bind="checked: Cashier.bankDetails.wireStatus" type="checkbox">
        <?php echo i18n::getTranslation("iWantWireTransfers") ?>
    </div>

</div>
<div class="cashier__cta">
<div class="cashier__cta__wrapper">
    <button data-bind="click: Cashier.saveBankDetails" class="success radius" >
        <?php echo i18n::getTranslation("saveDetails") ?>
    </button>
    </div>
</div>


<!--  /DETAILS  -->

</section>