<div class="cashier-widget">
    <h1 id="welcome-cashier">
        <?php echo $row["Title"];?>
    </h1>
    <section class="section" style="position:relative;">
        <div class="cashier-widget--header">
            <div class="cashier__secure-payment">
                <img src="/_images/cashier/secure-payment.png">
            </div>
        </div>
        <!-- CONTENT -->
        <div class="content-template">

            <!-- PRD CASHIER -->
            <script type="application/javascript">
                skinModules.push({
                    id: "Cashier",
                    options: {
                        source: "Cas"
                    }
                });
            </script>
            <?php include '_global-library/partials/cashier/cashier.php'; ?>
            <!-- /PRD CASHIER -->
        </div>
    </section>
</div>