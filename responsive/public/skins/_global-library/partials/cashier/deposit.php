
<!-- MAIN CONTENT AREA -->
<div id="content">

	<!-- TOP CONTENT -->

    <!-- END TOP CONTENT -->

    <!-- CASHIER BODY -->
    <div class="cashier__body">
		<!-- cashier navigation -->
		<?php require_once("_global-library/partials/cashier/cashier__main-nav.php") ?>
		<!-- end cashier navigation -->

		<!-- CONTENT WRAPPER -->
		<div class="cashier__content-wrapper">
			
			<!-- PAGE SPECIFIC CONTENT -->
			<!-- PROMO BOXES -->
			<?php require_once("_global-library/partials/cashier/cashier__promo-boxes.php"); ?>
			<!-- END PROMO BOXES -->

			<!-- NEW METHOD -->
			<?php require_once("_global-library/partials/cashier/cashier__new-method.php"); ?>
			<!-- END NEW METHOD -->
			<!-- END PAGE SPECIFIC CONTENT -->

		</div>
		<!-- END CONTENT WRAPPER -->
	</div>
    <!-- END CASHIER BODY -->

  <!-- END MAIN CONTENT AREA -->
</div>