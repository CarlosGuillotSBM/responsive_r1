<div style="display: none;" class="cashier__footer" data-bind="visible: Cashier.activePanel() === 'ACCOUNTSLIST' || Cashier.activePanel() === 'METHODS'">
	<div class="cashier__footer__inner">
		<ul class="cashier__footer__links ">
			<li class="cashier__footer__link">
				<a href="/about-us/" target="_blank">About</a>
			</li>
			<li class="cashier__footer__link" data-bind="visible: Cashier.playerLimits.showDepositLimitsLink" style="display: none">
				<a href="#" data-bind="click: Cashier.openLimitsScreen.bind($data,true)">Deposit Limits</a>
			</li>
			<li class="cashier__footer__link" data-bind="visible: !Cashier.playerLimits.showDepositLimitsLink()" style="display: none">
				<a href="/banking/" target="_blank">Banking</a>
			</li>
			<li class="cashier__footer__link">
				<a href="" data-bind="click: Cashier.openLimitsScreen.bind($data,false)">Responsible Gaming</a>
			</li>
			<li class="cashier__footer__link">
				<a href="/faq/" target="_blank">FAQ</a>
			</li>
		</ul>
	</div>
	
</div>