<section data-bind="visible: Cashier.activePanel() === 'REVERSE'" class="cashier__section" style="display: none">

    <!--  REVERSE  -->
    <div data-bind="visible: Cashier.reversal.operation() === 'REVERSE'">

        <div>

            <h1 class="cashier__section__title"> <?php echo i18n::getTranslation("reversalDetails") ?> </h1>

            <div class="cashier__section__summary--alone">
                <div class="cashier__section__summary__title"> <?php echo i18n::getTranslation("pendingWithdrawals") ?>: </div>
                <div class="cashier__section__summary__amount">
                    <strong data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.availableToReverse()"></strong>
                </div>

                <div data-bind="visible: parseFloat(Cashier.reversal.minReverse()) > 0.01">
                    <div class="cashier__section__summary__title"> <?php echo i18n::getTranslation("minReversalAmount") ?>:</div>
                    <div class="cashier__section__summary__amount"><strong data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.minReverse() + '.'"></strong></div>
                </div>

                <div data-bind="visible: Cashier.reversal.availableToReverse() > 0 && Cashier.reversal.availableToReverse() < Cashier.reversal.minWithdraw()">
                    <div class='small-12 reversal-description-info'>
                        <p>
                            *  <?php echo i18n::getTranslation("ifCurrPendingWithLess1") ?>
                            <strong data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.minWithdraw()"></strong>
                            <?php echo i18n::getTranslation("ifCurrPendingWithLess2") ?>.
                        </p>
                    </div>

                </div>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="cashier__section__withdraw-amount">
            <label> <?php echo i18n::getTranslation("amountToReverse") ?> </label>
            <!-- slider -->
            <label data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.amountToReverse()"> </label>
            <div class="range-slider__wrapper">
                <div data-bind="text: Cashier.reversal.currencySymbol() + '0.00'" class="range-slider__limit"></div>
                <div class="range-slider__slider">
                    <div id="ui-reversal-slider"></div>
                </div>
                <div data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.availableToReverse()" class="range-slider__limit"></div>
            </div>
            <!-- /slider -->
            <div class="clearfix"></div>
            
        </div>
        <div class="cashier__cta--alone">
            <button data-bind="click: Cashier.doReversal,
                               enable: Cashier.reversal.buttonActive,
                               text: 'Reverse ' + Cashier.reversal.currencySymbol() + Cashier.reversal.amountToReverse()"
                    class="success radius" ><?php echo i18n::getTranslation("reverse") ?></button>
        </div>
        

    </div>
    <!--  /REVERSE  -->

    <!--  RESUME  -->
    <div data-bind="visible: Cashier.reversal.operation() === 'RESUME'">

        <div class="">
            <h1 class="cashier__section__title"><?php echo i18n::getTranslation("reversalSuccessful") ?></h1>
            <p class="withdrawal-response-1">
                <?php echo i18n::getTranslation("YourReversalOf1") ?> <strong data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.amountToReverse()"></strong>
                <?php echo i18n::getTranslation("YourReversalOf2") ?>
                <strong data-bind="text: Cashier.reversal.txReverseBatchID"></strong> .</p>
        </div>

        <div class=''>
            <div class='cashier__section__summary'>
                <div class='cashier__section__summary__title'> <?php echo i18n::getTranslation("yourCurrPendingWithdrawals") ?>:</div>
                <div class='cashier__section__summary__amount'>
                    <strong data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.withdrawalsPending()"></strong>
                    <span data-bind="visible: Cashier.reversal.withdrawalsPending() > 0 && Cashier.reversal.withdrawalsPending() < 10"> * </span>
                </div>
            </div>

            <div class='cashier__section__summary'>
                <div class='cashier__section__summary__title'> <?php echo i18n::getTranslation("yourCurrBalance") ?>:</div>
                <div class='cashier__section__summary__amount'>
                    <strong data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.balance()"></strong>
                </div>
            </div>


            <div data-bind="visible: Cashier.reversal.withdrawalsPending() > 0 && Cashier.reversal.withdrawalsPending() < 10" class='small-12 reversal-description-info'>
                <p>
                    * <?php echo i18n::getTranslation("ifCurrPendingWithLess1") ?>
                    <strong data-bind="text: Cashier.reversal.currencySymbol() + Cashier.reversal.minWithdraw()"></strong>
                    <?php echo i18n::getTranslation("ifCurrPendingWithLess2") ?>.
                </p>
            </div>


        </div>

    </div>
    <!--  /RESUME  -->

    <!--  RETRY  -->
    <div data-bind="visible: Cashier.reversal.operation() === 'RETRY'">
        <p data-bind="text: Cashier.reversal.error" class="response-msg"></p>
        <button data-bind="click: Cashier.openReverse" class="success radius" >
            <?php echo i18n::getTranslation("retry") ?>
        </button>
    </div>
    <!--  /RETRY  -->
</section>