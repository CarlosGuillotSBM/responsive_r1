<!-- Deposit Limits -->
<div>

    <h1 class="cashier__section__title"><?php echo i18n::getTranslation("depositLimits") ?></h1>

    <p data-bind="text: Cashier.playerLimits.depositLimitMsg"></p>

    <table class="nonresponsive">
        <thead>
        <td style="display: none" data-bind="visible: Cashier.playerLimits.pendingDailyLimit() !== '0'"><?php echo i18n::getTranslation("day") ?></td>
        <td style="display: none" data-bind="visible: Cashier.playerLimits.pendingWeeklyLimit() !== '0'"><?php echo i18n::getTranslation("week") ?></td>
        <td style="display: none" data-bind="visible: Cashier.playerLimits.pendingMonthlyLimit() !== '0'"><?php echo i18n::getTranslation("month") ?></td>
        </thead>
        <tr>
            <td style="display: none" data-bind="visible: Cashier.playerLimits.pendingDailyLimit() !== '0', text: Cashier.playerLimits.pendingDailyLimit"></td>
            <td style="display: none" data-bind="visible: Cashier.playerLimits.pendingWeeklyLimit() !== '0', text: Cashier.playerLimits.pendingWeeklyLimit"></td>
            <td style="display: none" data-bind="visible: Cashier.playerLimits.pendingMonthlyLimit() !== '0', text: Cashier.playerLimits.pendingMonthlyLimit"></td>
        </tr>
    </table>

    <p><?php echo i18n::getTranslation("currLimitsAre") ?>:</p>

    <table class="nonresponsive">
        <thead>
        <td data-bind="visible: Cashier.playerLimits.dailyLimitSetBy() !== ''"><?php echo i18n::getTranslation("day") ?></td>
        <td data-bind="visible: Cashier.playerLimits.weeklyLimitSetBy() !== ''"><?php echo i18n::getTranslation("week") ?></td>
        <td data-bind="visible: Cashier.playerLimits.monthlyLimitSetBy() !== ''"><?php echo i18n::getTranslation("month") ?></td>
        </thead>
        <tr>
            <td data-bind="visible: Cashier.playerLimits.dailyLimitSetBy() !== '', text: Cashier.playerLimits.currentDailyLimitDisplay"></td>
            <td data-bind="visible: Cashier.playerLimits.weeklyLimitSetBy() !== '', text: Cashier.playerLimits.currentWeeklyLimitDisplay"></td>
            <td data-bind="visible: Cashier.playerLimits.monthlyLimitSetBy() !== '', text: Cashier.playerLimits.currentMonthlyLimitDisplay"></td>
        </tr>
    </table>

    <div class="cashier__cta__wrapper">

        <button style="display: none"
            data-bind="visible: !Cashier.playerLimits.pendingConfirmation(),
                click: Cashier.openDeposit.bind($data, true)" class="secondary radius">OK
        </button>

        <button style="display: none"
                data-bind="visible: Cashier.playerLimits.pendingConfirmation,
                click: Cashier.playerLimits.setPlayerLimits" class="secondary radius">Confirm
        </button>

         <button  class=" success radius" data-bind="click: Cashier.playerLimits.cancelRequest">Cancel
         </button>
       
        </a>
    </div>

</div>
