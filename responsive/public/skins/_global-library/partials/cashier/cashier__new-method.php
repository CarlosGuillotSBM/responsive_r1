<!-- NEW METHOD -->
<div data-bind="visible: Cashier.activePanel() === 'ENTERDETAILS'" class="cashier__new-method" style="display: none">
    <!-- New card form -->
    <form>
        <!-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -->
        <!-- card Payment details -->
        <!-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -->
        <section data-bind="visible: Cashier.addingNewMethod.accountType() === 'A_CC'" class="cashier__section" style="display: none">
            <div class="cashier__section__extra-info">
                <span class="cashier__section__extra-info__lock"><img src="/_global-library/images/cashier/lock-circle-green.svg"></span>
                <img data-bind="attr: {src: '/_global-library/images/cashier/' + Cashier.addingNewMethod.card.creditCardIcon() }, visible: Cashier.addingNewMethod.card.creditCardIcon()!=''">
            </div>
            <h1 class="cashier__section__title"><?php echo i18n::getTranslation("enterYourDetails") ?></h1>
            <fieldset class="">
                
                <div class="">
                    <!-- Name -->
                    <div class="cashier__form-group">
                        <label> <?php echo i18n::getTranslation("firstName") ?> </label>
                        <input data-bind="value: Cashier.addingNewMethod.card.firstName" placeholder="<?php echo i18n::getTranslation("firstName") ?>" autocomplete="sbm-cashier-firstname" type="text">
                    </div>
                    <!-- Last Name -->
                    <div class="cashier__form-group">
                        <label> <?php echo i18n::getTranslation("lastName") ?> </label>
                        <input data-bind="value: Cashier.addingNewMethod.card.lastName" placeholder="<?php echo i18n::getTranslation("lastName") ?>" autocomplete="sbm-cashier-lastname" type="text">
                    </div>
                    <!--Card number-->
                    <div class="cashier__form-group">
                        <label><?php echo i18n::getTranslation("cardNumber") ?></label>
                        <input data-bind="value: Cashier.addingNewMethod.card.number, valueUpdate: ['input', 'afterkeydown']" placeholder="Card Number"  autocomplete="sbm-cashier-cardnumber" type="tel">
                    </div>
                    <!-- Expire date area -->
                    <div class="cashier__form-group">
                        <label class=""> <?php echo i18n::getTranslation("validTo") ?> </label>
                        <!-- Month -->
                        <div class="cashier__form-group__container">
                            <!-- <label> Month </label> -->
                            <select data-bind="value: Cashier.addingNewMethod.card.expiryMonth">
                                <option disabled="" value="" selected=""> <?php echo i18n::getTranslation("month") ?> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <!-- year -->
                        <div class="cashier__form-group__container">
                            <!-- <label> Year </label> -->
                            <select
                                data-bind="options: Cashier.expiryYears,
                                optionsCaption: 'Year', optionsValue: 'val',
                                optionsText: 'desc', value: Cashier.addingNewMethod.card.expiryYear"
                                class="textField">
                            </select>
                        </div>
                    </div>
                    <!-- End Expire date area -->
                    <div class="cvn">
                        <h1 class="cashier__section__title__bonus">Enter CVN to proceed</h1>
                        <span class="solid-border-amount-cvn"></span>
                        <input data-bind="value: Cashier.addingNewMethod.card.cvn, valueUpdate: ['input', 'afterkeydown']" placeholder="CVN" autocomplete="off" type="tel">
                        <div class="cvn__icon">
                            <img class="" src="/_global-library/images/cashier/cvn-logo.svg">
                        </div>

                        <div class="large-12" style="float: left; width: 100%;margin-top: 10px;">
                            <span class="cvn__info">Last 3 digits on the back of your card.</span>
                        </div>
                    </div>
                </div>
            </fieldset>
        </section>
        <!-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -->
        <!-- Neteller Payment details -->
        <!-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -->
          <section data-bind="visible: Cashier.addingNewMethod.accountType() === 'A_NETELLER'" class="cashier__section" style="display: none">
            <div class="cashier__section__extra-info">
                <span class="cashier__section__extra-info__lock"><img src="/_global-library/images/cashier/lock-circle-green.svg"></span>
                <img data-bind="attr: {src: '/_global-library/images/cashier/' + Cashier.deposit.methodIcon() }">
            </div>
            <h1 class="cashier__section__title"><?php echo i18n::getTranslation("enterYourDetails") ?></h1>
            <fieldset>
                
                <!-- Number -->
                <div class="large-6">
                    <div class="form-group neteller-number">
                        <label> <?php echo i18n::getTranslation("netellerAccountID") ?> </label>
                        <input data-bind="value: Cashier.addingNewMethod.neteller.number" type="text" placeholder="<?php echo i18n::getTranslation("netellerAccountID") ?>">
                    </div>
                </div>
                <!-- ID -->
                <div class="large-6">
                    <div class="form-group neteller-id">
                        <label><?php echo i18n::getTranslation("netellerSecId") ?></label>
                        <input data-bind="value: Cashier.addingNewMethod.neteller.secureId" type="text" placeholder="<?php echo i18n::getTranslation("netellerSecId") ?>">
                    </div>
                </div>
            </fieldset>
        </section>
        <!-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -->
        <!-- PAYSAFE Payment details -->
        <!-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ -->
        <section data-bind="visible: Cashier.addingNewMethod.accountType() === 'A_PAYSAFECARD'" class="cashier__section" style="display: none">
            <div class="cashier__section__extra-info">
                <img data-bind="attr: {src: '/_global-library/images/cashier/' + Cashier.deposit.methodIcon() }">
                <span class="cashier__section__extra-info__lock"><img src="/_global-library/images/cashier/lock-circle-green.svg"></span>
            </div>
            <h1 class="cashier__section__title"><?php echo i18n::getTranslation("enterYourDetails") ?></h1>
            <fieldset>
                
                <div class="">
                    <!-- Name -->
                    <div class="form-group name">
                        <label> <?php echo i18n::getTranslation("firstName") ?> </label>
                        <input data-bind="value: Cashier.addingNewMethod.paysafe.firstName" type="text" placeholder="First Name">
                    </div>
                    <!-- Last Name -->
                    <div class="form-group last-name">
                        <label> <?php echo i18n::getTranslation("lastName") ?> </label>
                        <input data-bind="value: Cashier.addingNewMethod.paysafe.lastName" type="text" placeholder="Last Name">
                    </div>
                    <!-- email -->
                    <div class="form-group card-number">
                        <label><?php echo i18n::getTranslation("email") ?></label>
                        <input data-bind="value: Cashier.addingNewMethod.paysafe.email" type="email" placeholder="Email">
                    </div>
                    <!-- Date of Birth -->
                    <div class="form-group date-of-birth">
                        <label class=""> <?php echo i18n::getTranslation("dateOfBirth") ?> </label>
                        <!-- Day -->
                        <div>
                            <label> <?php echo i18n::getTranslation("day") ?> </label>
                            <select data-bind="value: Cashier.addingNewMethod.paysafe.dayOfBirth">
                                <option disabled selected><?php echo i18n::getTranslation("day") ?></option>
                                <option value="01"> 1 </option>
                                <option value="02"> 2 </option>
                                <option value="03"> 3 </option>
                                <option value="04"> 4 </option>
                                <option value="05"> 5 </option>
                                <option value="06"> 6 </option>
                                <option value="07"> 7 </option>
                                <option value="08"> 8 </option>
                                <option value="09"> 9 </option>
                                <option value="10"> 10 </option>
                                <option value="11"> 11 </option>
                                <option value="12"> 12 </option>
                                <option value="13"> 13 </option>
                                <option value="14"> 14 </option>
                                <option value="15"> 15 </option>
                                <option value="16"> 16 </option>
                                <option value="17"> 17 </option>
                                <option value="18"> 18 </option>
                                <option value="19"> 19 </option>
                                <option value="20"> 20 </option>
                                <option value="21"> 21 </option>
                                <option value="22"> 22 </option>
                                <option value="23"> 23 </option>
                                <option value="24"> 24 </option>
                                <option value="25"> 25 </option>
                                <option value="26"> 26 </option>
                                <option value="27"> 27 </option>
                                <option value="28"> 28 </option>
                                <option value="29"> 29 </option>
                                <option value="30"> 30 </option>
                                <option value="31"> 31 </option>
                            </select>
                        </div>
                        <!-- Month -->
                        <div class="">
                            <label><?php echo i18n::getTranslation("month") ?></label>
                            <select data-bind="value: Cashier.addingNewMethod.paysafe.monthOfBirth">
                                <option disabled selected> <?php echo i18n::getTranslation("month") ?> </option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <!-- year -->
                        <div class="">
                            <label> <?php echo i18n::getTranslation("year") ?> </label>
                            <select
                                data-bind="options: Cashier.dobYears, optionsCaption: 'Year',
                                optionsValue: 'val', optionsText: 'desc',
                                value: Cashier.addingNewMethod.paysafe.yearOfBirth"
                                class="textField">
                            </select>
                        </div>
                    </div>
                    <!-- End Date of Birth date area -->
                </div>
            </fieldset>
        </section>
    </form>
    <!-- end new card form -->
    <div class="cashier__cta">
        <div class="cashier__cta__wrapper">
          <button data-bind="click: Cashier.goBack" class="secondary radius"> <?php echo i18n::getTranslation("back") ?> </button>
            <button data-bind="click: Cashier.showPanel.bind($data, 'PROMOTIONS')" class="success radius save__details"> <?php echo i18n::getTranslation("saveMethodDetails") ?> </button>
        </div>
    </div>
    
</div>
<!-- END NEW METHOD -->