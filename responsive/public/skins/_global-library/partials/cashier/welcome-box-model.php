<section data-bind="visible: CashierPrototype.methodsScreenActive" class="cashier__section">
    <div class="cashier__section__container">
        <!-- payment methods -->
        <nav class="cashier__ftd__nav" >
            <span class="cashier__ftd__nav__msg"><strong>Well Done!</strong></span>
            <span class="cashier__ftd__nav__msg">You have been <strong>awarded £10</strong> on our exclusive slots. </span>
            <span class="cashier__ftd__nav__msg">Redeem your <strong>cashier deposit bonus</strong> in the next <strong>5 minutes.</strong></span>
            <h5 class="cashier__ftd__nav__title">Choose Payment Method</h5>
            <div class="cashier__ftd__nav__payment">
                <div data-bind="click: CashierPrototype.showDetailsScreen">Card</div>
            </div>
            <div class="cashier__ftd__nav__payment">
                <div data-bind="click: CashierPrototype.showDetailsScreen">Paypal</div>
            </div>
            <div class="cashier__ftd__nav__payment">
                <div data-bind="click: CashierPrototype.showDetailsScreen">Neteller</div>
            </div>
            <div class="clearfix"></div>
        </nav>
        <!-- end payment methods -->
    </div>
</section>
<!-- FTD PAYMENT INFO -->
<section style="display: none" data-bind="visible: CashierPrototype.detailsScreenActive" class="cashier__section" id="payment-details-container">
    <div class="cashier__section__container">
        <h5>Payment details</h5>
        <!-- payment forms -->
        <div class="cashier__ftd_form">
            <!-- card form -->
            <div class="card-form">
                <!-- name -->
                <div class="card-form__name">
                    <input name="credit-card-number" type="text" placeholder="Name" value="Nuno Oliveira">
                </div>
                <!-- /name -->
                <!-- card number -->
                <div class="card-form__name">
                    <input name="credit-card-number" type="text" placeholder="Card Number">
                </div>
                <!-- /card number -->
                <!-- expiry date -->
                <div class="card-form__expiry-date">
                    <!-- month -->
                    <div>
                        <select name="expiry-month">
                            <option disabled="" selected="">Month</option>
                            <option value="01">Jan</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Apr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Aug</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dec</option>
                        </select>
                    </div>
                    <!-- /end month -->
                    <!-- year -->
                    <div>
                        <select name="expiry-year">
                            <option disabled="" selected="">Year</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                        </select>
                    </div>
                    <!-- end year -->
                </div>
                <!-- /expiry date -->
                <!-- CVN -->
                <div class="card-form__cvn">
                    <input id="cvn" name="cvn" type="text" placeholder="CVN">
                    <div class="card-form__cvn__check-box">
                        <input type="checkbox"> I prefer NOT to enter my CVN again for future deposits.
                    </div>
                </div>
                <!-- /CVN -->
            </div>
            <!-- /card form -->
            <div style="display: block" class="cashier__ftd_form__buttons">
                <button class="secondary radius" data-bind="click: CashierPrototype.showMethodsScreen"> Cancel </button>
                <button class="success radius" data-bind="click: CashierPrototype.showDepositScreen"> Next </button>
            </div>
            
        </div>
        <!-- /payment forms -->
        <div class="clearfix"></div>
    </div>
</section>
<!-- /FTD PAYMENT INFO -->
<!-- cashier GRID -->
<div style="display: none" data-bind="visible: CashierPrototype.depositScreenActive">
    <!-- /cashier GRID -->
    <div class="clearfix"></div>
    <!-- Deposit amount -->
    <div>
        <section class="cashier__section">
            <div class="cashier__section__container">
                <h5>Select Amount</h5>
                <div class="cashier__section__container-amounts">
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£10</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£20</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£50</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£100</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£150</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£200</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£250</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£300</div>
                    </div>
                    <!-- /amount -->
                    <!-- amount -->
                    <div class="cashier__grid__cell--small">
                        <div data-bind="click: CashierPrototype.selectAmount" class="cashier__grid__cell__content">£500</div>
                    </div>
                    <!-- /amount -->
                    <div class="clearfix"></div>
                </div>
                <div class="cashier__free-amount">
                    <div>
                        <label>Or enter a free amount</label>
                        <input type="text">
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
    <section class="cashier__section ">
        <div id="promo-container" class="cashier__section__container">
            <h5>Select bonus</h5>
            <div class="cashier__no-bonus">
                <input type="checkbox"><label>I don't want bonus</label>
            </div>
            <div class="cashier__promo-code">
                <input type="checkbox"><label>I have a promo code</label><input type="text" placeholder="promo code" disabled="">
            </div>
            <!-- offer -->
            <div  class="cashier__grid__cell" style="opacity: 0.2" data-bind="style: { 'opacity': CashierPrototype.promo1Activate()  ? '1' : '0.2' }">
                <div data-bind="click: CashierPrototype.selectPromo" class="cashier__grid__cell__content">Get 100% + 100 free spins</div>
            </div>
            <!-- /offer -->
            <!-- offer -->
            <div  class="cashier__grid__cell" style="opacity: 0.2" data-bind="style: { 'opacity': CashierPrototype.promo2Activate()  ? '1' : '0.2' }">
                <div data-bind="click: CashierPrototype.selectPromo" class="cashier__grid__cell__content">Get 200% + 200 free spins and 10 mulas</div>
            </div>
            <!-- /offer -->
            <!-- offer -->
            <div  class="cashier__grid__cell" style="opacity: 0.2" data-bind="style: { 'opacity': CashierPrototype.promo3Activate()  ? '1' : '0.2' }">
                <div data-bind="click: CashierPrototype.selectPromo" class="cashier__grid__cell__content">Get 300% + 200 free spins and 20 mulas</div>
            </div>
            <!-- /offer -->
            <div class="clearfix"></div>
        </div>
        <!-- SUBMIT BUTTON -->
        <div class="cashier__cta">
            <div class="cashier__cta__wrapper">
                <button data-bind="click: CashierPrototype.showMethodsScreen" class="secondary radius">
                Cancel
                </button>
                <button class="success radius" >
                Deposit
                </button>
            </div>
        </div>
        <!-- /SUBMIT BUTTON -->
    </section>
</div>
<!-- /Deposit amount -->
<div class="clearfix"></div>