<section>
<h1 class="cashier__section__title">About Responsible Gaming</h1>

<div class="panel callout radius">
  <h6>What is Responsible Gaming?</h6>
  <p>
Responsible Gaming means staying in control of how much time and money you spend on your gameplay.
 Visit <a href="http://www.gamcare.org.uk/" target="blank">Gamcare</a>, <a href="http://www.gamblingcontrol.org/" target="blank">Gambling Control</a> or <a href="http://www.gamblingcommission.gov.uk" target="blank">Gambling Commission</a> for more information on responsible gaming.
  </p>
<div class="row gaming-logos">
  <a href="http://www.gamcare.org.uk/" target="blank"><img class="gaming-logo" src="/_global-library/images/cashier/gamcare.png"></a>
     <a href="http://www.gamblingcontrol.org/" target="blank"><img class="gaming-logo" src="/_global-library/images/cashier/alderney.png"></a>
       <a href="http://www.gamblingcommission.gov.uk" target="blank"><img class="gaming-logo" src="/_global-library/images/cashier/gambling-commision.png"></a>
</div>
</div>

<div class="responsible-limits-box panel secondary radius">
    <p>We recommend Responsible Gaming so we give you the option to choose limits that suit you. You can set your deposit limits any time you like.</p>

    <!--  responsible gaming  -->
    <div style="display: none" data-bind="visible: !Cashier.playerLimits.allowOnlyMonthlyLimit()">
        <ul>
            <li data-bind="visible: Cashier.playerLimits.dailyLimitSetBy() === '' &&
                                    Cashier.playerLimits.weeklyLimitSetBy() === '' &&
                                    Cashier.playerLimits.monthlyLimitSetBy() === ''">
                <p>You can set your <a data-bind="click: Cashier.playerLimits.openLimitsSetup">monthly limits</a> now or anytime you want.</p>
            </li>
            <li data-bind="visible: Cashier.playerLimits.monthlyLimitSetBy() !== ''">
                <p>
                    <span data-bind="text: Cashier.playerLimits.displayLimits.monthly"></span>
                    <a data-bind="click: Cashier.playerLimits.editLimit.bind($data,'monthly')">Edit</a>
                </p>
            </li>
            <li data-bind="visible: Cashier.playerLimits.weeklyLimitSetBy() !== ''">
                <p>
                    <span data-bind="text: Cashier.playerLimits.displayLimits.weekly"></span>
                    <a data-bind="click: Cashier.playerLimits.editLimit.bind($data,'weekly')">Edit</a>
                </p>
            </li>
            <li data-bind="visible: Cashier.playerLimits.dailyLimitSetBy() !== ''">
                <p>
                    <span data-bind="text: Cashier.playerLimits.displayLimits.daily"></span>
                    <a data-bind="click: Cashier.playerLimits.editLimit.bind($data,'daily')">Edit</a>
                </p>
            </li>
            <li style="display: none"
                data-bind="visible: Cashier.playerLimits.dailyLimitSetBy() === '' ||
                    Cashier.playerLimits.weeklyLimitSetBy() === '' ||
                    Cashier.playerLimits.monthlyLimitSetBy() === ''">
                <p>
                    <a data-bind="click: Cashier.playerLimits.openLimitsSetup">Add</a>
                </p>
            </li>
        </ul>

    </div>
    <!--  /responsible gaming  -->

    <!--  deposit limits  -->
    <div style="display: none" data-bind="visible: Cashier.playerLimits.allowOnlyMonthlyLimit">

        <div style="display: none" data-bind="visible: !Cashier.playerLimits.limitsToSave.monthly()">
            <p>You can set your <a data-bind="click: Cashier.playerLimits.openLimitsSetup">monthly limits</a> now or anytime you want.</p>
        </div>

        <div style="display: none" data-bind="visible: Cashier.playerLimits.limitsToSave.monthly">

            <div style="display: none" data-bind="visible: Cashier.playerLimits.limitsToSave.monthly() === -1">
                <p>You are opting for no limits for now</p>
            </div>

            <div style="display: none" data-bind="visible: Cashier.playerLimits.limitsToSave.monthly() !== -1">
                <p>Only allow me to make monthly deposits up to <span data-bind="text: Cashier.currencySymbol() + Cashier.playerLimits.limitsToSave.monthly()"></span></p>
            </div>

        </div>
    </div>
    <!--  /deposit limits  -->


</div>
    <button class="button small radius success" data-bind="click: Cashier.playerLimits.cancelLimits">Cancel</button>
    <button style="display: none" class="button secondary small radius" data-bind="click: Cashier.playerLimits.removeLimits, visible: Cashier.playerLimits.canRemoveLimits">Remove Limits</button>
    <button class="button secondary small radius"
            data-bind="click: Cashier.playerLimits.requestLimitChange,
            visible: Cashier.playerLimits.modified">Save Deposit Limits</button>


</section>

