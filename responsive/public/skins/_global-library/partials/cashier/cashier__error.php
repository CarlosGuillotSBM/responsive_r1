<article data-bind="visible: Cashier.activePanel() === 'ERROR'" class="cashier__withdraw" style="display: none"
         xmlns="http://www.w3.org/1999/html">
    <section class="cashier__section">
        <!-- Unrecoverable error -->
        <div data-bind="visible: Cashier.error().code() === '-1' || Cashier.error().code() === '47'">
            <p data-bind="html: Cashier.error().message"></p>
            <p><?php echo i18n::getTranslation("ifProbPersists") ?>.</p>
        </div>
        <!-- Cashier is not available for the Player / Skin -->
        <div data-bind="visible: Cashier.error().code() === '-2'">
            <p data-bind="html: Cashier.error().message"></p>
        </div>
        <!-- Data input or other recoverable error -->
        <div data-bind="visible: Cashier.error().code() === '-3'">
            <p data-bind="html: Cashier.error().message"></p>

    
            <table data-bind="visible: Cashier.withdrawal.displayLimits() && Cashier.currentAction() === 'withdraw'">
                <thead>
                    <td></td>
                    <td>Limit</td>
                    <td>Used</td>
                    <td>Available</td>
                </thead>
                <tr>
                    <td> <?php echo i18n::getTranslation("MinWithdraw"); ?> </td>
                    <td data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.minWithdraw()"> </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td> <?php echo i18n::getTranslation("WithdrawLimitCountPerDay"); ?> </td>
                    <td data-bind="text: Cashier.withdrawal.withdrawLimitCountPerDay()"> </td>
                    <td data-bind="text: Cashier.withdrawal.withdrawCountThisDay"> </td>
                    <td data-bind="text: Cashier.withdrawal.withdrawLimitCountPerDay() - Cashier.withdrawal.withdrawCountThisDay()"> </td>
                </tr>
                <tr>
                    <td> <?php echo i18n::getTranslation("WithdrawLimitPerDay"); ?> </td>
                    <td data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.withdrawLimitPerDay()"> </td>
                    <td data-bind="text: Cashier.withdrawal.withdrawalsThisDay"> </td>
                    <td data-bind="text: Cashier.withdrawal.currencySymbol() + (Cashier.withdrawal.withdrawLimitPerDay() - Cashier.withdrawal.withdrawalsThisDay())"> </td>
                </tr>
                <tr>
                    <td> <?php echo i18n::getTranslation("WithdrawLimitPerWeek"); ?> </td>
                    <td data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.withdrawLimitPerWeek()"> </td>
                    <td data-bind="text: Cashier.withdrawal.withdrawalsThisWeek"> </td>
                    <td data-bind="text: Cashier.withdrawal.currencySymbol() + (Cashier.withdrawal.withdrawLimitPerWeek() - Cashier.withdrawal.withdrawalsThisWeek())"> </td>
                </tr>
                <tr>
                    <td> <?php echo i18n::getTranslation("WithdrawLimitPerMonth"); ?> </td>
                    <td data-bind="text: Cashier.withdrawal.currencySymbol() + Cashier.withdrawal.withdrawLimitPerMonth()"> </td>
                    <td data-bind="text: Cashier.withdrawal.withdrawalsThisMonth"> </td>
                    <td data-bind="text: Cashier.withdrawal.currencySymbol() + (Cashier.withdrawal.withdrawLimitPerMonth() - Cashier.withdrawal.withdrawalsThisMonth())"> </td>
                </tr>
            </table>
        </div>
        <!-- Error that requires player to go back to main deposit screen -->
        <div data-bind="visible: Cashier.error().code() === '-4'">
            <p data-bind="html: Cashier.error().message"></p>
            <p>Please contact <a href="/support/">support</a> should you need any assistance.</p>
        </div>
        <!-- Where Source=PRD and the deposit is declined or Paypal cancellation -->
        <div data-bind="visible: Cashier.error().code() === '-5'">
            <p>
                <?php echo i18n::getTranslation("depositNotCompleted") ?>.
                <?php echo i18n::getTranslation("takeAdvantage") ?> <a href="/cashier/"><?php echo i18n::getTranslation("cashier") ?></a>
            </p>
        </div>
        <!-- Cashier is closed  -->
        <div data-bind="visible: Cashier.error().code() === '2999'">
            <p data-bind="html: Cashier.error().message"></p>
            <p>
                <?php echo i18n::getTranslation("openTime") ?>: <span data-bind="text: Cashier.error().openTime"></span>
            </p>
        </div>
    </section>
</article>