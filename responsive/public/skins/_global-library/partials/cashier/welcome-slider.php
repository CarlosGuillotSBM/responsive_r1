<!-- cashier GRID -->
<section class="cashier__section">

  <div class="cashier__section__container">
    <!-- offer -->
    <div class="cashier__grid__cell">
      <div class="cashier__grid__cell__content">Deposit between 10 and 20 and get 300% + 100 free spins</div>
    </div>
    <!-- /offer -->

    <!-- offer -->
    <div class="cashier__grid__cell">
      <div class="cashier__grid__cell__content">Deposit between 21 and 50 and get 300% + 100 free spins and 10 mulas</div>
    </div>
    <!-- /offer -->

    <!-- offer -->
    <div class="cashier__grid__cell">
      <div class="cashier__grid__cell__content">Deposit between 21 and 50 and get 300% + 100 free spins and 10 mulas</div>
    </div>
    <!-- /offer -->
    <div class="clearfix"></div>
  </div>
  
</section>
<!-- /cashier GRID -->
<div class="clearfix"></div>
<!-- Deposit amount -->
<section class="cashier__section">

  <div class="cashier__section__container">
    <div  class="cashier__deposit-amount__amount-text">
      <h5>Deposit amount:</h5>
      <span>£400</span>
    </div>
    <div class="cashier__deposit-amount__range">
      <ul>
        <li>£10</li>
        <li><input id="test" type="range"/></li>
        <li>£50</li>
      </ul>
      
    </div>
    <div class="cashier__deposit-amount__free-amount">
      <input id="test" type="text" placeholder="£400"/>
    </div>
    &nbsp;
  </div>
  
</section>
<!-- /Deposit amount -->
<div class="clearfix"></div>
<!-- FTD PAYMENT INFO -->
<section class="cashier__section">
  <div class="cashier__section__container">
    <h5>Payment details</h5>

    <!-- payment methods -->
    <nav class="cashier__ftd__nav">
      <div class="cashier__ftd__nav__payment">
        <div>Card</div>
      </div>
      <div class="cashier__ftd__nav__payment">
        <div>Paypal</div>
      </div>
      <div class="cashier__ftd__nav__payment">
        <div>Neteller</div>
      </div>
      <div class="clearfix"></div>
    </nav>
    <!-- end payment methods -->

    <!-- payment forms -->
    <div class="cashier__ftd_form">

      <!-- card form -->
      <div class="card-form">

        <!-- name -->
        <div class="card-form__name">
          <input name="credit-card-number" type="text" placeholder="Name">
        </div>
        <!-- /name -->

        <!-- expiry date -->
        <div class="card-form__expiry-date">
          <!-- month -->
          <div>
            <label>Month</label>
            <select name="expiry-month">
              <option disabled="" selected="">Month</option>
              <option value="01">Jan</option>
              <option value="02">Feb</option>
              <option value="03">Mar</option>
              <option value="04">Apr</option>
              <option value="05">May</option>
              <option value="06">Jun</option>
              <option value="07">Jul</option>
              <option value="08">Aug</option>
              <option value="09">Sep</option>
              <option value="10">Oct</option>
              <option value="11">Nov</option>
              <option value="12">Dec</option>
            </select>
          </div>
          <!-- /end month -->

          <!-- year -->
          <div>
            <label>Year</label>
            <select name="expiry-year">
              <option disabled="">Year</option>
              <option value="2014">2014</option>
              <option value="2015">2015</option>
              <option value="2016">2016</option>
              <option value="2017">2017</option>
              <option value="2018">2018</option>
              <option value="2019">2019</option>
            </select>
          </div>
          <!-- end year -->
        </div>
        <!-- /expiry date -->

        <!-- CVN -->
        <div class="card-form__cvn">
          <label>CVN</label><img src="/_global-library/images/cashier/help-24x24.png">
          <input id="cvn" name="cvn" type="text" placeholder="Card number">
          <div class="card-form__cvn__check-box">
            <input type="checkbox"> I prefer NOT to enter my CVN again for future deposits.
          </div>
        </div>
        <!-- /CVN -->

      </div>
      <!-- /card form -->

      <!-- SUBMIT BUTTON -->
      <div class="cashier__ftd_form__deposit-button">
        <button class="success radius">
          Deposit
        </button>
      </div>
      
      <!-- /SUBMIT BUTTON -->

    </div>
    <!-- /payment forms -->
    <div class="clearfix"></div>
  </div>
</section>
    <!-- /FTD PAYMENT INFO -->