<section data-bind="visible: Cashier.activePanel() === 'ACCOUNTSLIST'" style="display: none" class="cashier__section">
    <h1 class="cashier__section__title"><?php echo i18n::getTranslation("choosePaymentMethod") ?>
    <span class="secure-payment-icon">
       <img src="/_global-library/images/cashier/secure-payment.svg" alt="Secure Payment">
    </span>
    </h1>
    <span class="solid-border"></span>
  <div class="cashier__list" >
    <ul data-bind="foreach: Cashier.customerAccounts">
      <li data-bind="css: { 'active-state' : DefaultAccount == '1' },click: $root.Cashier.selectedAccount">
        <div class="cashier__list__wrapper">
          <img class="payment-icon" data-bind="attr: { src : '/_global-library/images/cashier/' + icon }">
          <div style="float: right;">
            <span class="cashier__list__description" data-bind="text: listDescription"></span>
            <span class="cashier__account-arrow"><img src="/_global-library/images/cashier/arrow-right.svg" alt=""></span>
          </div>
        </div>
         <div data-bind="visible: LimitReached === 'd'" class="limit-reached">You have reached your cashier daily deposit limit</div>
         <div data-bind="visible: LimitReached === 'w'" class="limit-reached">You have reached your cashier weekly deposit limit</div>
         <div data-bind="visible: LimitReached === 'm'" class="limit-reached">You have reached your cashier monthly deposit limit</div>
      </li>
      <div class="clearfix"></div>
     
      <div class="cashier__list__expire" data-bind="visible: AccountTypeCode == 'A_VISA_CREDIT' && (ExpireStatus == 1 || ExpireStatus == 2), click: $root.Cashier.openExpiry">
        <div><img src="/_global-library/images/cashier/alert.png"></div>
        <div><?php echo i18n::getTranslation("changeExpiry") ?></div>
      </div>
      
    </ul>
    <div class="cashier__cta">
    <div class="cashier__cta__wrapper">
      <button data-bind="visible: !Cashier.maxAccountsReached(), click: Cashier.AddNewMethod" class="success radius add-method" >
          <?php echo i18n::getTranslation("addNewMethod") ?>
      </button>
    </div>
      </div>
     
  </div>
</section>