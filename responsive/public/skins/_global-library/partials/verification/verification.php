<div class="middle-content__box">
    <div class="cashier verification" style="margin-bottom: 25px;">
        <div class="account-verification" style="padding-top:30px;">
            <?php edit($this->controller,'account-verification-simplified'); ?>
            <div class="content-template" style="margin-top:20px;">
                <!-- repeatable content -->
                <?php @$this->repeatData($this->content['account-verification-simplified'],"_global-library/partials/verification/verification-intro.php");?>
                <!-- /repeatable content -->
            </div>

            <section class="cashier__section-main">
                <!-- UPLOAD SCREEN -->
                <div class="cashier__section-upload">
                    <h2>Account Verification</h2>
                    <div class="upload-secure">
                        <img src="/_global-library/images/cashier/account-verification/icon-padlock.svg" alt="secure upload icon"/>
                    </div>
                    <div class="cashier__section-upload-files">
                        <p>Upload your documents to verify your account quickly and easily. <a href="/account-verification/">Find out more.</a></p>
                        <div class="secure-info">
                            <p class="find-out-more">Please  provide at least <u>one</u> of the following:</p>
                            <ul>
                                <li>A copy of a recent bank statement (from the last 3 months), <b>OR</b></li>
                                <li>A copy of a recent pay slip/pension statement (from the last 3 months), <b>OR</b></li>
                                <li>Other clear evidence that would support ‘affordability’ in relation to playing with us</li>
                            </ul>
                            <p style="margin-top: 35px;">You can upload your documents via the secure form below:</p>
                        </div>
                    </div>

                    <!-- Choose file CTA -->
                    <div class="cashier__section-cta" style="margin-top: 20px;">
                        <label for="ui-file-uploads">Choose files</label>
                        <input type="file" id="ui-file-uploads" name="ui-file-uploads" accept=".jpg, .jpeg, .png, .pdf" multiple="" style="opacity: 0;">
                    </div>
                    <!-- Choose file CTA -->

                    <!--Uploaded files-->
                    <div data-bind="foreach: AccountVerificationSimple.uploadedFiles, visible: AccountVerificationSimple.uploadedFiles().length > 0" style="display: none">
                        <div class="uploaded-files-success">
                            <img src="/_global-library/images/cashier/account-verification/icon-doc-success.svg" alt=""/>
                            <p><span class="truncate" data-bind="text: name"></span> <img data-bind="click: $root.AccountVerificationSimple.removeFile.bind($data)" class="uploaded_files-delete" src="/_global-library/images/cashier/account-verification/icon-close.svg" alt=""/></p>
                        </div>
                    </div>
                    <!--Uploaded files-->

                    <!--Send CTA-->
                    <div class="cashier__section-cta">
                        <button class="send-file disabled" data-bind="click: AccountVerificationSimple.upload, css: {disabled: AccountVerificationSimple.uploadedFiles().length === 0 }">Send</button>
                    </div>
                    <!-- Send CTA -->
                    
                    <!--Please Note-->
                    <div class="cashier__section-notes">
                        <span>Please note:</span>
                        <ul>
                            <li>If more than one document is required, you will need to upload them all before you can press ‘Send’</li>
                            <li>Total file size cannot exceed 10MB</li>
                            <li>If you do not provide your documents some restrictions may be imposed on your account, and on other linked accounts on our sister sites</li>
                        </ul>
                    </div>
                    <!--Please Note-->
                </div>
                <!-- UPLOAD SCREEN -->
                <!--Support Team-->
                <div class="cashier__support-team">
                    <p style="color:#4C4C4C;">Need some help? Please contact our <a href="/help/" class="help-link">Support Team</a>, available 24/7.</p>
                </div>
                <!--Support Team-->
            </section>

        </div> <!--END Account verification-->
    </div> <!--End cashier verification-->
</div>

<script type="text/javascript">
    skinModules.push({
        id: "AccountVerificationSimple"
    });
</script>