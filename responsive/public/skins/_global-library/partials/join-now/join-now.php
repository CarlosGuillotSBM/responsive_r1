<?php

$currentUrl = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$isAspers = (config("SkinID") === 12);

$isChinesePage = ($isAspers && (preg_match("#\/chinese\/$#", $currentUrl) == 1 || preg_match("#\/newyear\/$#", $currentUrl) == 1));
//$titleTemplate = ($isChinesePage ? "_global-library/_editor-partials/text-only.php" : "_global-library/_editor-partials/text.php");
$termsAndConditionText = ($isChinesePage ? "条款和条件" : "T&Cs");
$privacyPolicyText = ($isChinesePage ? "隐私政策" : "Privacy Policy");
$urlArray = explode('/', rtrim($currentUrl, '/'));
$end = end($urlArray);
?>
<script type="application/javascript">
    skinModules.push({ id:"JoinNow",
        options: {
            registrationSteps: 3
        }
    });
</script>


<?php
    if($isAspers == "1"){
        ?>
            <script type="application/javascript">
                skinModules.push({ id:"AspersAppTracking"});
            </script>
        <?php
    }
?>



<div class="main-content registration">
    <!-- REGISTRATION 3STEP MODAL WRAP -->
    <div class="registration-3step">

        <!-- CONTENT -->
        <div class="content">
            <!-- NAV STICKY
              http://foundation.zurb.com/sites/docs/v/5.5.3/components/topbar.html
              Should be fixed on scroll when div container nav touches top page
            -->
            <div>

                <!-- BANNER -->
                <div class="banner">

                    <?php $jnSkin = config("SkinID") ?>

                    <!-- spin and win / kitty -->

                    <?php if ($jnSkin == 1 || $jnSkin == 2): ?>

                        <?php edit('registration','registration-banner'); ?>
                        <?php @$this->repeatData($this->content['registration-banner']);?>

                    <?php endif; ?>


                    <!-- Lucky pants / bingo extra / give back bingo-->
                    <?php if ($jnSkin == 3 || $jnSkin == 6 || $jnSkin == 9): ?>

                        <?php edit('registration','title',@$this->action); ?>
                        <?php $this->getPartial(@$this->content['title'],1); ?>

                    <?php endif; ?>

                    <!-- magical vegas / luckyvip / regal wins / king jack / aspers -->
                    <?php if ($jnSkin == 5 || $jnSkin == 8 || $jnSkin == 10 || $jnSkin == 11 || $jnSkin == 12): ?>

                        <?php edit('registration','registration-banner',$this->action); ?>
                        <?php @$this->repeatData($this->content['registration-banner']);?>

                    <?php endif; ?>

                </div>

                <!--  Step 1 to 4 Edit point - To go between steps without validating  -->
                <?php if(config("Edit")):?>
                    <div class="editor-buttons">
                        <a data-dropdown="drop2" aria-controls="drop2" aria-expanded="false" class="options-menu">Options</a>
                        <ul id="drop2" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li class="options-menu__buttons"><input type='button' data-bind="click: function(){ JoinNow.goToStepNoValidation(1)}" value="Step 1"></li>
                            <li class="options-menu__buttons"><input type='button' data-bind="click: function(){ JoinNow.goToStepNoValidation(2)}" value="Step 2"></li>
                            <li class="options-menu__buttons"><input type='button' data-bind="click: function(){ JoinNow.goToStepNoValidation(3)}" value="Step 3"></li>
                        </ul>
                    </div>
                <?php endif;?>
                <!--  Step 1 to 4 Edit point - To go between steps without validating  -->

                <nav role="navigation">
                    <div class="naviaton-inner">
                        <a class="active" href="javascript:void(0)" data-bind="css: {passed: JoinNow.currentStep() > 1, active: JoinNow.currentStep() == 1}, click: function(){ JoinNow.goToStep(1)}"><span>Personal</span><div class='dotted_line'></div></a>
                        <a class="" href="javascript:void(0)" data-bind="css: {passed: JoinNow.currentStep() > 2, active: JoinNow.currentStep() == 2}, click: function(){ JoinNow.goToStep(2)}"><span>Contact</span><div class='dotted_line'></div></a>
                        <a class="current" href="javascript:void(0)" data-bind="css: {active: JoinNow.currentStep() == 3, current: JoinNow.currentStep() != 3}, click: function(){ JoinNow.goToStep(3)}"><span>Account</span></a>
                    </div>
                </nav>


            </div>

            <!-- FORM -->

            <!-- 3 STEP REGISTRATION FORM -->
            <div class="registration-3step-form">

                <form id="registration-3step-form">

                    <!-- PERSONAL -->
                    <fieldset class="step" data-bind="visible: JoinNow.currentStep() == 1">
                        <!-- <div class="steps-title">Personal Details</div> -->

                        <!-- TITLE -->
                        <div class="field-wrap radio-selection">
                            <span class="field-wrap__label">Title</span>

                            <div>
                                <label for="radio-1" class="radio-label">
                                    <input data-gtag="JoinNowTracking,Step1+Field1-Title-Mr" id="radio-1" name="title" type="radio" value="mr" data-bind="checked: JoinNow.title">
                                    Mr</label>
                            </div>

                            <div>
                                <label  for="radio-2" style="margin-left: 10px;" class="radio-label">
                                    <input data-gtag="JoinNowTracking,Step1+Field1-Title-Mrs" id="radio-2" name="title" type="radio" value="mrs" data-bind="checked: JoinNow.title">
                                    Mrs</label>
                            </div>

                            <div>
                                <label  for="radio-3" style="margin-left: 10px;" class="radio-label">
                                    <input data-gtag="JoinNowTracking,Step1+Field1-Title-Ms" id="radio-3" name="title" type="radio" value="ms" data-bind="checked: JoinNow.title">
                                    Ms</label>
                            </div>

                            <div>
                                <label  for="radio-4" style="margin-left: 10px;" class="radio-label">
                                    <input data-gtag="JoinNowTracking,Step1+Field1-Title-Other" id="radio-4" name="title" type="radio" value="other" data-bind="checked: JoinNow.title">
                                    Other</label>
                            </div>
                            <span class="error" style="display: none; float: left; width: 100%;" data-bind="visible: JoinNow.titleRequired">This field is required.</span>
                        </div>
                        <!-- / TITLE -->

                        <!-- FIRST NAME -->
                        <div class="field-wrap">
                            <span class="field-wrap__label">First Name</span>
                            <div id="icon"></div>
                            <input data-gtag="JoinNowTracking,Step1+Field2-FirstName" id="ui-firstname" class="textField field-wrap__field" data-bind="value: JoinNow.firstName" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Enter your first name">
                        </div>
                        <!-- / FIRST NAME -->

                        <!-- LAST NAME -->
                        <div class="field-wrap">
                            <span class="field-wrap__label">Last Name</span>
                            <div id="icon"></div>
                            <input data-gtag="JoinNowTracking,Step1+Field3-LastName" class="textField field-wrap__field" data-bind="value: JoinNow.lastName"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Enter your last name">
                        </div>
                        <!-- / LAST NAME -->

                        <!--DATE OF BIRTH-->
                        <div class="field-wrap">
                            <span class="field-wrap__label">Date of Birth <small style="float: right; font-size: 12px; padding-right: 5px;">e.g. 19/03/1979</small></span>
                            <!--  <span>e.g. 19/03/1979</span> -->
                            <div class="date">

                                <!-- tel used to bring up friendlier number input keyboard -->
                                <input data-gtag="JoinNowTracking,Step1+Field4-DateofBirth-Day" type="number" pattern="\d*" id="dayOfBirth" name="dayOfBirth" class="fdatepicker" placeholder="DD" maxlength="2" data-bind="value: JoinNow.dayOfBirth, valueUpdate: 'afterkeydown'" min="0">

                                <input data-gtag="JoinNowTracking,Step1+Field4-DateofBirth-Month" type="number" pattern="\d*" id="monthOfBirth" name="monthOfBirth" class="fdatepicker" placeholder="MM" maxlength="2" data-bind="value: JoinNow.monthOfBirth, valueUpdate: 'afterkeydown'" min="0">

                                <div id="icon"></div>
                                <input data-gtag="JoinNowTracking,Step1+Field4-DateofBirth-Year" type="number" pattern="\d*" id="yearOfBirth" name="selectedYearOfBirth" class="fdatepicker" placeholder="YYYY" maxlength="4" pattern="\d{4}" data-bind="value: JoinNow.selectedYearOfBirth, valueUpdate: 'afterkeydown'" min="0">

                                <span class="error" style=" width: 100%; float: left;" data-bind="validationMessage: JoinNow.DOB"></span>
                            </div>
                            <p class="field-support-text">Your date of birth is used to verify that you are over 18 years old</p>
                        </div>
                        <!-- http://zurb.com/university/lessons/date-night-with-zurb-foundation-how-to-set-up-a-date-picker -->
                        <!-- / DATE OF BIRTH -->

                        <!-- EMAIL -->
                        <div class="field-wrap">
                            <!-- <label class="error">Error</label> -->
                            <span class="field-wrap__label">E-mail address</span>
                            <div id="icon"></div>
                            <!-- valueUpdate will invoke mail checking just after the user enters the domain name and before the last dot -->
                            <!-- <input data-gtag="JoinNowTracking,Step1+Field5-EmailAddress" id="ui-email" class="textField field-wrap__field" data-bind="value: JoinNow.email"  type="email" placeholder="Enter a valid email address"> -->
                            <input data-gtag="JoinNowTracking,Step1+Field5-EmailAddress" id="ui-email" class="textField field-wrap__field" data-bind="value: JoinNow.email"  type="email" placeholder="Enter a valid email address">

                            <!-- <small class="error">Invalid entry</small> -->
                            <span style="display: none" data-bind="text: JoinNow.emailNotAvailable, visible: JoinNow.emailNotAvailable() !== ''" class="error"></span>
                        </div>
                        <!-- / EMAIL -->
                    </fieldset>
                    <!-- / PERSONAL -->

                    <!-- CONTACT -->
                    <fieldset class="step" style="display: none" data-bind="visible: JoinNow.currentStep() == 2">
                        <!-- <div class="steps-title">Contact Details</div> -->

                        <!-- MOBILE PHONE -->
                        <div class="field-wrap">
                            <span class="field-wrap__label">Mobile</span>
                            <input data-gtag="JoinNowTracking,Step2+Field1-Mobile" id="ui-phone" class="phone field-wrap__field" data-bind="value: JoinNow.mobilePhone" type="tel" placeholder="Enter a valid mobile number" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                        </div>
                        <!-- / MOBILE PHONE -->

                        <!-- COUNTRY OF RESIDENCE -->
                        <div class="field-wrap">
                            <span class="field-wrap__label">Country of Residence</span>
                            <div id="icon"></div>
                            <select data-gtag="JoinNowTracking,Step2+Field2-Country" class="textField field-wrap__field" data-bind="options: JoinNow.countries, optionsCaption: 'Choose Country', optionsValue: 'Code', optionsText: 'Desc', value: JoinNow.selectedCountry">
                                <option  class="country" value="GBR">United Kingdom</option>
                            </select>
                        </div>
                        <!-- / COUNTRY OF RESIDENCE -->


                        <!-- POST CODE -->
                        <div class="field-wrap">
                            <span class="field-wrap__label">Post Code</span>
                            <input style="width: 51%;"

                                   data-gtag="JoinNowTracking,Step2+Field3-PostCode"
                                   class="post-code field-wrap__field"
                                   data-bind="value: JoinNow.postcode, valueUpdate: ['input', 'afterkeydown']"
                                   type="text" autocomplete="off"
                                   autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Enter your postcode"/>
                            <a href="#" data-nohijack="true" data-bind="click: JoinNow.postcodeLookup, css: {disabled: JoinNow.postcodeIsValid() == false}"  class="disabled button postfix findaddress" >Find Address</a>
                            <span class="error" style="display: none; float: left; width: 100%;" data-bind="visible: JoinNow.addressFieldsRequired">You should select your address.</span>                            




                        </div>
                        <!-- / POST CODE -->

                        <!-- ADDRESS - Show this section if postcode is valid -->
                        <!-- Show this section if postcode is valid -->

                        <select  data-bind="visible: JoinNow.addresses().length > 0,
        options: JoinNow.addresses,
        optionsCaption: 'Please select',
        value: JoinNow.selectedAddress1" class="textField border-important" id="selectAdd" color="color: white">
                        </select>


                        <div data-bind="visible: JoinNow.showAddressFields">

                            <div class="field-wrap">
                                <span class="field-wrap__label">Address Line 1</span>
                                <div id="icon"></div>
                                <input data-gtag="JoinNowTracking,Step2+Field4-AddressLine1" class="field-wrap__field" data-bind="value: JoinNow.address1" class="textField" type="text" id="addressLine1"
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                            </div>

                            <div class="field-wrap">
                                <span class="field-wrap__label">Address Line 2</span>
                                <div id="icon"></div>
                                <input data-gtag="JoinNowTracking,Step2+Field5-AddressLine2" class="field-wrap__field" data-bind="value: JoinNow.address2" class="textField" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" id="addressLine2">
                            </div>

                            <div class="field-wrap">
                                <span class="field-wrap__label">City</span>
                                <div id="icon"></div>
                                <input data-gtag="JoinNowTracking,Step2+Field6-City" class="field-wrap__field" data-bind="value: JoinNow.city" class="textField" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" id="city">
                            </div>

                            <div class="field-wrap"  id="state_wrap">
                                <span class="field-wrap__label">County/State</span>
                                <div id="icon"></div>
                                <input data-gtag="JoinNowTracking,Step2+Field7-County/State" class="field-wrap__field" data-bind="value: JoinNow.state" class="textField" type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" id="county">
                            </div>
                        </div>
                        <div data-bind="visible: JoinNow.enterAddressManually, click: JoinNow.enterAddressManually" class="enter-address-manually">Enter address manually</div>
                        <span class="error" style="display: none; float: left; width: 100%;" data-bind="visible: JoinNow.dataAspersRequired">This field is required.</span>
                        <!-- / ADDRESS -->

                        <!-- ASPERS ONLY PREFERENCES - Communications and Data -->
                        <?php if (config("SkinID") == 12): ?>
                            <ul class="mailing-opt communication-channel">
                                <div class="preferences-title">Preferences</div>

                                <!-- COMMUNICATIONS -->
                                <span class="communications">Communications!</span>
                                <?php edit('registration','mailing-opt',$this->action); ?>
                                <?php @$this->repeatData($this->content['mailing-opt']);?>
                                <div class="field-wrap">
                                    <input type="checkbox" data-bind="checked: JoinNow.dataAspersCommunications"
                                           style="radio-radius:0px;width:25px;vertical-align:middle;"/>
                                    All communications:
                                </div>


                                <div style="width: 100%; overflow: hidden;">
                                    <div style="width:50%; float: left;">
                                        <label data-bind="css: {ticked: JoinNow.wantsEmail}" for="cbEmail">
                                            <input data-gtag="JoinNowTracking,Step2+Field8-Email"
                                                   data-bind="checked: JoinNow.wantsEmail" type="checkbox" id="cbEmail">
                                            <span class="custom checkbox"></span>
                                            Email
                                        </label>
                                        <label data-bind="css: {ticked: JoinNow.wantsSMS}" for="cbSms">
                                            <input data-gtag="JoinNowTracking,Step2+Field9-SMS"
                                                   data-bind="checked: JoinNow.wantsSMS" type="checkbox" id="cbSms">
                                            <span class="custom checkbox"></span>
                                            SMS
                                        </label>

                                    </div>
                                    <div style="margin-left: 50%;">
                                        <label data-bind="css: {ticked: JoinNow.wantsMail}" for="cbPostMail">
                                            <input data-gtag="JoinNowTracking,Step2+Field11-wantsMail"
                                                   data-bind="checked: JoinNow.wantsMail" type="checkbox" id="cbPostMail">
                                            <span class="custom checkbox"></span>
                                            Post
                                        </label>

                                        <label data-bind="css: {ticked: JoinNow.wantsPhone}" for="cbPhone">
                                            <input data-gtag="JoinNowTracking,Step2+Field10-Phone"
                                                   data-bind="checked: JoinNow.wantsPhone" type="checkbox" id="cbPhone">
                                            <span class="custom checkbox"></span>
                                            Phone
                                        </label>
                                    </div>
                                </div>

                                <p class="preference-support-text">You can change your preferences at any time in the ‘personal details’ section of “My Account”</p>
                            </ul>
                            <!-- / COMMUNICATIONS -->

                            <!-- DATA -->
                            <div class="data-aspers">
                                <ul class="mailing-opt-aspers trusted-partners">
                                    <span class="data">Data</span>
                                    <?php edit('registration','trusted-partners',$this->action); ?>
                                    <?php @$this->repeatData($this->content['trusted-partners']);?>
                                </ul>

                                <div class="field-wrap radio-selection">
                                    <div>
                                        <label for="radio-5" class="radio-label">
                                            <input data-gtag="JoinNowTracking,Step2+Field10-DataYes" id="radio-5" name="yes" type="radio" value="Yes" data-bind="checked: JoinNow.dataAspers">
                                            Yes</label>
                                    </div>

                                    <div>
                                        <label for="radio-6" style="margin-left: 10px;" class="radio-label">
                                            <input data-gtag="JoinNowTracking,Step2+Field11-DataNo" id="radio-6" name="no" type="radio" value="No" data-bind="checked: JoinNow.dataAspers">
                                            No</label>
                                    </div>
                                    <span class="error" style="display: none; float: left; width: 100%;" data-bind="visible: JoinNow.dataAspersRequired">This field is required.</span>
                                </div>

                                <div class="data-checkbox" data-bind="visible: JoinNow.showAspers">
                                    <li class="trusted-partners--check">
                                        <label style="margin-top: 10px;" data-bind="css: {ticked: JoinNow.thirdPartyEmail}" for="thirdPartyEmail">
                                            <input data-gtag="JoinNowTracking,Step2+Field12-DataEmail" data-bind="checked: JoinNow.thirdPartyEmail" type="checkbox" id="thirdPartyEmail" checked>
                                            <span class="custom checkbox"></span>
                                            Email
                                        </label>
                                    </li>
                                    <li class="trusted-partners--check">
                                        <label style="margin-bottom: 15px;" data-bind="css: {ticked: JoinNow.thirdPartySMS}" for="thirdPartySMS">
                                            <input data-gtag="JoinNowTracking,Step2+Field13-DataSMS" data-bind="checked: JoinNow.thirdPartySMS" type="checkbox" id="thirdPartySMS">
                                            <span class="custom checkbox"></span>
                                            SMS
                                        </label>
                                    </li>

                                    <p class="preference-support-text">You can change your preferences at any time in the ‘personal details’ section of “My Account”</p>

                                </div>
                            </div>

                            <!-- / DATA -->

                        <?php endif; ?>
                        <!-- / ASPERS ONLY PREFERENCES - Communications and Data -->

                        <!-- ALL SKINS - COMMUNICATION PREFERENCES - EXCEPT ASPERS -->
                        <?php if (config("SkinID") != 12) : ?>
                            <ul class="mailing-opt communication-channel">
                                <div class="preferences-title">Communication Preferences!</div>
                                <?php edit('registration','mailing-opt',$this->action); ?>
                                <?php @$this->repeatData($this->content['mailing-opt']);?>
                                <div class="field-wrap">
                                    <input type="checkbox" data-bind="checked: JoinNow.dataAspersCommunications"
                                           style="radio-radius:0px;width:25px;vertical-align:middle;"/>
                                    All communications:
                                </div>

                                <div style="width: 100%; overflow: hidden;">
                                    <div style="width:50%; float: left;">
                                        <label data-bind="css: {ticked: JoinNow.wantsEmail}" for="cbEmail">
                                            <input data-gtag="JoinNowTracking,Step2+Field8-Email"
                                                   data-bind="checked: JoinNow.wantsEmail" type="checkbox" id="cbEmail">
                                            <span class="custom checkbox"></span>
                                            Email
                                        </label>
                                        <label data-bind="css: {ticked: JoinNow.wantsSMS}" for="cbSms">
                                            <input data-gtag="JoinNowTracking,Step2+Field9-SMS"
                                                   data-bind="checked: JoinNow.wantsSMS" type="checkbox" id="cbSms">
                                            <span class="custom checkbox"></span>
                                            SMS
                                        </label>

                                    </div>
                                    <div style="margin-left: 50%;">
                                        <label data-bind="css: {ticked: JoinNow.wantsMail}" for="cbPostMail">
                                            <input data-gtag="JoinNowTracking,Step2+Field11-wantsMail"
                                                   data-bind="checked: JoinNow.wantsMail" type="checkbox" id="cbPostMail">
                                            <span class="custom checkbox"></span>
                                            Post
                                        </label>

                                        <label data-bind="css: {ticked: JoinNow.wantsPhone}" for="cbPhone">
                                            <input data-gtag="JoinNowTracking,Step2+Field10-Phone"
                                                   data-bind="checked: JoinNow.wantsPhone" type="checkbox" id="cbPhone">
                                            <span class="custom checkbox"></span>
                                            Phone
                                        </label>
                                    </div>
                                </div>


                                <p class="preference-support-text">You can change your preferences at any time in the ‘personal details’ section of “My Account”</p>
                            </ul>
                        <?php endif; ?>
                        <!-- / ALL SKINS - COMMUNICATION PREFERENCES - EXCEPT ASPERS -->

                    </fieldset>
                    <!-- / CONTACT -->

                    <!-- ACCOUNT -->
                    <fieldset class="step" style="display: none" data-bind="visible: JoinNow.currentStep() == 3">
                        <!-- <div class="steps-title">Account Details</div> -->

                        <!-- USERNAME -->
                        <div class="field-wrap">
                            <span class="field-wrap__label" >Username</span>
                            <div id="icon" style="height: 96px"></div>
                            <p class="field-wrap__requirements">Minimum of 3 characters, alphabet letters only</p>
                            <input data-gtag="JoinNowTracking,Step3+Field1-Username" id="ui-username" class="textField field-wrap__field" data-bind="value: JoinNow.username"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Enter a username">
                            <span style="display: none" data-bind="text: JoinNow.usernameNotAvailable, visible: JoinNow.usernameNotAvailable() != ''" class="error"></span>
                        </div>
                        <!-- / USERNAME -->

                        <!-- PASSWORD -->
                        <div class="registration__password">
                            <div class="field-wrap">
                                <span class="field-wrap__label">Password</span>
                                <div id="icon" style="height: 96px"></div>
                                <p class="field-wrap__requirements">Minimum of 5 characters, include numbers</p>
                                <input data-gtag="JoinNowTracking,Step3+Field2-Password" class="textField field-wrap__field registration__password" data-bind="value: JoinNow.password, attr: { type: JoinNow.showPassword() == true ? 'text' : 'password' }" type="password" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Enter a password">
                                <div class="show-password eye-shut" data-bind="click: JoinNow.togglePassword"></div>
                                <!-- <label data-tooltip title="Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces">Password Help</label>-->
                                <!-- <label data-tooltip title="Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces">Password Help</label>-->
                            </div>
                        </div>
                        <!-- / PASSWORD -->

                        <!-- ONLY ASPERS LAND BASED CASINO -->
                        <?php if (config("SkinID") == 12) : ?>
                            <div class="field-wrap">
                                <span class="field-wrap__label">My Offline Casino</span>
                                <div id="icon"></div>

                                <select data-gtag="JoinNowTracking,Step3+Field3-OfflineCasino" data-bind="options: JoinNow.offlineCasinos,
            optionsCaption: 'Please select',
            value: JoinNow.offlineCasino" class="textField border-important">
                                </select>

                            </div>
                        <?php endif; ?>
                        <!-- / ONLY ASPERS LAND BASED CASINO -->

                        <!-- PROMO CODE -->
                        <div class="field-wrap">
                            <span class="field-wrap__label">Promo Code</span>
                            <p class="field-wrap__requirements">Optional</p>
                            <div id="icon"></div>
                            <input data-gtag="JoinNowTracking,Step3+Field4-PromoCode" class="textField field-wrap__field" data-bind="value: JoinNow.promoCode"  type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Enter promo code">
                        </div>
                        <!-- / PROMO CODE -->

                        <!-- ACCOUNT CURRENCY -->
                        <div class="field-wrap radio-selection">
                            <span class="field-wrap__label" style="margin-bottom: -10px; margin-top: 20px;">Choose your account currency</span>
                            <div>
                                <label for="radio-7" class="radio-label">
                                    <input data-gtag="JoinNowTracking,Step3+Field5-Currency-Pounds" id="radio-7" name="currency" type="radio" value="GBP" data-bind="checked: JoinNow.currency">
                                    &pound;</label>
                            </div>

                            <div>
                                <label for="radio-8" style="margin-left: 10px;" class="radio-label">
                                    <input data-gtag="JoinNowTracking,Step3+Field5-Currency-Euros" id="radio-8" name="currency" type="radio" value="EUR" data-bind="checked: JoinNow.currency">
                                    &euro;</label>
                            </div>
                            <span class="error" style="display: none; float: left;
    width: 100%;" data-bind="visible: JoinNow.currencyRequired">This field is required.</span>
                        </div>
                        <!-- / ACCOUNT CURRENCY -->

                        <!-- TC'S AND PRIVACY POLICY CHECKBOXES -->
                        <div class="field-wrap" style="margin-top: 20px;">
                            <span class="field-wrap__label">T&C’s and Privacy Policy</span>
                            <!-- ACCEPT TC'S -->
                            <ul class="mailing-opt">
                                <li>
                                    <label data-bind="css: {ticked: JoinNow.over18}" for="cbOver18" class="terms-over18">
                                        <input data-gtag="JoinNowTracking,Step3+Field6-AcceptTandCs" data-bind="checked: JoinNow.over18" type="checkbox" id="cbOver18" name="cbOver18">
                                        <span class="custom checkbox"></span>
                                        I accept the <a data-nohijack="true" class="register-3step-tc" href="#" data-reveal-id="TcModal" target="terms">T&amp;Cs</a> and confirm I am over 18
                                    </label>
                                </li>
                                <p data-bind="validationMessage: JoinNow.over18"></p>
                                <span class="error" data-bind=" validationMessage: JoinNow.DOBvOver18"></span>
                                <li>
                                    <label data-bind="css: {ticked: JoinNow.policy}" for="cbpolicy" class="terms-over18">I read and understood the <a data-nohijack="true" class="register-3step-tc" href="#" data-reveal-id="PrivacyModal" target="terms">Privacy Policy</a></label>
                                    <input data-gtag="JoinNowTracking,Step3+Field7-AcceptPrivacyPolicy" style="display: none;" data-bind="checked: JoinNow.policy" type="checkbox" id="cbpolicy" name="cbpolicy">
                                    <span class="custom checkbox"></span>
                                </li>
                            </ul>
                        </div>
                        <!-- / TC'S AND PRIVACY POLICY CHECKBOXES -->

                        <!-- TC'S POPUP -->
                        <div id="TcModal" class="reveal-modal" data-reveal aria-hidden="true" role="dialog" style="height: 500px !important;">
                            <div class="modal-content">
                                <?php edit('registration','4step-tcs-popup',$this->action); ?>
                                <?php @$this->repeatData($this->content['4step-tcs-popup']);?>
                                <a class="close-reveal-modal" aria-label="Close" style="color: black;text-decoration: none;">&#215;</a>
                            </div>

                            <div class="footer-bar">
                                <a style="position: relative;" data-bind="click: JoinNow.over18Modal">I accept the T&Cs</a>
                            </div>

                        </div>
                        <!-- / TC'S POPUP-->

                        <!-- PRIVACY POLICY POPUP-->
                        <div id="PrivacyModal" class="reveal-modal" data-reveal aria-hidden="true" role="dialog" style="height: 500px !important;">
                            <div class="modal-content">
                                <?php edit('registration','4step-privacy-popup',$this->action); ?>
                                <?php @$this->repeatData($this->content['4step-privacy-popup']);?>
                                <a class="close-reveal-modal" aria-label="Close" style="color: black;text-decoration: none;">&#215;</a>
                            </div>

                            <div class="footer-bar">
                                <a style="position: relative;" data-bind="click: JoinNow.policyModal">I read and understood the privacy policy</a>
                            </div>

                        </div>
                        <!-- / PRIVACY POLICY POPUP-->

                    </fieldset>
                    <!-- ACCOUNT -->

                    <!-- HIDDEN INPUT ComingFrom -->
                    <input type="hidden" name="ComingFrom" id="ComingFrom" value="<?= $end ?>">
                    <!-- REGISTER CTAS -->
                    <a  class="button register-3step-cta next create-account-cta" data-nohijack="true" data-bind="click: JoinNow.doRegistration, visible: JoinNow.totalSteps() == JoinNow.currentStep()">
                        <!-- CREATE ACCOUNT -->
                        <span data-bind="visible: !JoinNow.loading()">CREATE ACCOUNT</span>
                        <div class="loader" style="display: none" data-bind="visible: JoinNow.loading"></div>
                    </a>
                    <!-- NEXT -->
                    <a data-gtag="JoinNowTracking,NextButton" class="registerNextCta" data-bind="click: JoinNow.nextStep, visible:  JoinNow.totalSteps() > 0 && JoinNow.totalSteps() > JoinNow.currentStep() ">NEXT</a>

                    <!-- Error message: Please resolve the above errors before continuing -->
                    <div style="display: none;" class="step-contains-error" data-bind="visible: JoinNow.stepContainsErrors">
                        <span >Please resolve the above errors before continuing</span>
                    </div>

                    <!-- PREVIOUS -->
                    <div class="step-previous" style="display: none" data-bind="visible: JoinNow.currentStep() > 1">
                        <a data-gtag="JoinNowTracking,PreviousButton" href="#" data-bind="click: JoinNow.previousStep">Previous</a>
                    </div>
                    <!-- / REGISTER CTAS -->
                </form>

            </div>
            <!-- /3 STEP REGISTRATION FORM -->
            <!-- TERMS AND CONDITIONS -->
            <!-- <div class="resitration-terms-section">
      <div id="ui-registration-terms-toggle" style="cursor: pointer; text-decoration: underline;">Terms and Conditions</div>
      <?php edit('registration','Terms-and-coditions-panel');?>
      <div id="ui-registration-terms-display">
        <?php @$this->getPartial($this->content['Terms-and-coditions-panel'],1); ?>
      </div>
    </div> -->

            <!--TERMS AND Conditions / Privacy Policy Tabs-->
            <div class="join-now-terms-section">
                <!--  <div class="terms__section-tabs">
                   <button id="terms" class="terms__tabs-tcs" onclick="openTerms('Tcs')">T&Cs</button>
                   <button id="policy" class="terms__tabs-policy" onclick="openTerms('PrivacyPolicy')">Privacy Policy</button>
                 </div> -->

                <dl class="tabs ui-filter-tabs terms__section-tabs" data-tab="">
                    <dd id="terms" class="terms__tabs-tcs active" onclick="openTCs()"><a href="#panel2"> <?php echo $termsAndConditionText; ?> </a></dd>
                    <dd id="policy" class="terms__tabs-policy" onclick="openPP()"><a href="#panel3"> <?php echo $privacyPolicyText; ?> </a></dd>
                </dl>

                <div id="Tcs" class="terms terms-content tcs1">
                    <?php edit('registration','Terms-and-coditions-panel');?>
                    <?php @$this->getPartial($this->content['Terms-and-coditions-panel'],1); ?>
                </div>

                <div id="PrivacyPolicy" class="terms terms-content policy-content pp1" style="display:none">
                    <?php edit('registration','4step-privacy-policy',$this->action); ?>
                    <?php @$this->repeatData($this->content['4step-privacy-policy']);?>
                </div>
                <div class="back-to-top">
                    <button onclick="topFunction()" id="myBtn" title="Go to top">Back to Top</button>
                </div>

                <script>
                    function openTerms(termsName) {
                        var i;
                        var x = document.getElementsByClassName("terms");
                        for (i = 0; i < x.length; i++) {
                            x[i].style.display = "none";
                        }
                        document.getElementById(termsName).style.display = "block";
                    }

                    function openTCs() {

                        document.getElementsByClassName("tcs1")[0].style.display = "block";
                        document.getElementsByClassName("pp1")[0].style.display = "none";
                    }

                    function openPP() {

                        document.getElementsByClassName("tcs1")[0].style.display = "none";
                        document.getElementsByClassName("pp1")[0].style.display = "block";
                    }

                    window.onscroll = function() {scrollFunction()};

                    function scrollFunction() {
                        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                            document.getElementById("myBtn").style.display = "block";
                        } else {
                            document.getElementById("myBtn").style.display = "none";
                        }
                    }

                    // When the user clicks on the button, scroll to the top of the document
                    function topFunction() {
                        document.body.scrollTop = 0;
                        document.documentElement.scrollTop = 0;
                    }

                </script>

            </div>

        </div>
        <!-- /CONTENT -->

    </div>
    <!-- /REGISTRATION 3STEP MODAL WRAP -->
</div>


