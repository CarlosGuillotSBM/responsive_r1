
<div class="login-box__inner" >
<!-- LOGIN BOX -->
    <article class="login-box" style="max-width: 90%; width: 30rem; margin: 0 auto;">

        <h3>Please log in to continue</h3>
        <!-- FORM -->
        <?php include '_partials/login/login-box.php' ?>
        <!-- / FORM -->
    </article>
</div>