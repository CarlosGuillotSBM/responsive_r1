<?php
$playerClass = (isset($_COOKIE["Class"])) ? $_COOKIE["Class"] : -1;
$res = $this->token;
/* If we have an actual request (player logged in ) */
if($res != -1){
    if ($res['httpStatus'] >= 200 and $res['httpStatus'] < 300) {
        $response["Code"] = 0;
        $response["hmac"] = $res["body"];
        $response["playerID"] = (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;
        $url = config("threeRadicalBoardURL") . "?externalId=" . $response["playerID"] . "&signature=" . $response["hmac"]["signature"] . "&nonce=" . $response["hmac"]["nonce"];
    } else {
        $response["Code"] = 1;
        $response["Msg"] = "Oops! Something unexpected happened. Please contact support and quote 'R06'";
        $response["Debug"] = $res;
        $url = "/start";
    }
}
/* If we don't have an actual request (player logged out ) */
else{
    $url = "/";
}
$status = $response["Code"] ?? -1;
$message = $response["Msg"] ?? "";
?>
<script type="text/javascript">
window.onload = function () {
    var playerClass = <?php echo $playerClass ?>;
    /* Code to check if we got response from the Microservices */
    var funded = window.localStorage.getItem("funded");
    var url = "<?php echo $url ?>";
    var code = <?php echo $status; ?>;
    /* If the player is logged in and has received a response */
    if(code == "0"){
        if(playerClass != -1 && funded == "true"){
            window.location.replace(url);
        }
        else{
            alert("Sorry, but you do not qualify for this promotion. If you would like more details please contact customer service.");
            window.location.replace("/promotions/");
        }
    }
    /* If we have a microservice error, return to start */
    else if(code == "1"){
        alert("<?php echo $message?>");
        window.location.replace(url);
    }
    /* If the player is logged out, return to homepage */
    else{
        window.location.replace(url);
    }
};
</script>