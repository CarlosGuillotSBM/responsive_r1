<script src="/_global-library/js/bower_components/ua-parser-js\dist\ua-parser.min.js"></script>
<script type="text/javascript">

     window.onload = checkPathName();

     function checkPathName() {
          path = window.location.pathname;
          if ( path === '/browser-not-supported-safari/' || path === '/browser-not-supported-chrome/') {
               return;
          }
          return sniffClientUserAgent();
     }

     function sniffClientUserAgent() {
          var clientAppSpecificationsString = window.navigator.appVersion;
          var UserAGentSniffer = new UAParser( clientAppSpecificationsString );
          var clientSpecifications = {
               browser: UserAGentSniffer.getBrowser().name,
               browserVersion: parseInt( UserAGentSniffer.getBrowser().major ),
               os: UserAGentSniffer.getOS().name
          }
          checkRedirect( clientSpecifications );
     }

     function checkRedirect( client ) {
          if ( client.browser === 'IE' && client.browserVersion < 11 ) {
               return redirectTo('chrome');
          }
     }

     function redirectTo( browser ) {
          window.location.href = window.location.href + '/browser-not-supported-' + browser;
     }

     function isFacebookApp() {
         var ua = navigator.userAgent || navigator.vendor || window.opera;
         return ( ua.indexOf( "FBAN" ) > -1 ) || ( ua.indexOf( "FBAV" ) > -1);
     }

</script>
