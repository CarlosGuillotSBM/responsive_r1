
<script src="https://www.google.com/recaptcha/api.js?render=<?=config("recaptcha3SiteKey")?>&render=explicit"></script>
<script>
var sbm = sbm || {};
function recaptchaResponseFunc(){
    grecaptcha.ready(function () {
        grecaptcha.execute("<?=config("recaptcha3SiteKey")?>", { action: 'login' }).then(function (token) {
            sbm.recaptchaToken = token;
        });
        sbm.core.publish("recaptcha-ready");
    });
}
$( document ).ready(function() {
    recaptchaResponseFunc();
});
</script>
<style>
.grecaptcha-badge{
    bottom: 85px !important;
    visibility: hidden;
    display: none;
    z-index: 1000000;
}
</style>