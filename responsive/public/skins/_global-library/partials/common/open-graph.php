<?php 
	// getting the Open Graph details
	$ogProtocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https' ? 'https://' : 'http://';
	$ogDomain = $_SERVER["SERVER_NAME"];
	$ogTitle = isset($this->content["Title"]) ? @$this->content["Title"] : "";
	$ogImage = isset($this->content["Image"]) ? @$this->content["Image"] : "";

	$ogFullImagePath = $ogProtocol . $ogDomain . $ogImage;
?>

<?php if(($this->controller == "promotions") ):?>
    <meta property="og:title" content="<?php echo $ogTitle;?>" />
    <meta property="og:image" content="<?php echo $ogFullImagePath;?>" />   
<?php endif;?>