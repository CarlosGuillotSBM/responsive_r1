<!-- Accordion -->
<li class="accordion-navigation">
    <a data-hijack="false" class="ui-accordion">
        <h1><?php echo $row['Title'];?></h1>
    </a>
    <div class="content" style="display: none">
            <?php echo $row['Body']; ?>
    </div>
</li>
<!-- /Accordion -->