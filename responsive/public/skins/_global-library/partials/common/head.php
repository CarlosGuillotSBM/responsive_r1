<!doctype html>
<html class="no-js" lang="en">
<head>
    <link rel="icon" type="image/ico" href="/_images/common/favicon.ico" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="stylesheet" href="/_css/main.css<?php echo "?v=" . config("Version"); ?>" />
    <script type="application/javascript">
        // sbm initialization
        var skinModules = [], skinPlugins = [];
        skinModules.push( {
            id: "RealityCheck",
            options: {
                global: true,
                gameId: "<?php echo isset($_GET["gameid"]) ? $_GET["gameid"] : (isset($_GET["GameID"]) ? $_GET["GameID"] : 2); ?>"

            }
        });
    </script>
</head>
<body>
