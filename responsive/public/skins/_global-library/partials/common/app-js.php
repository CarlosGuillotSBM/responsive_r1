<?php
    $jsV = config("Version");
    $jsSkin = config("SkinID");
    $jsDebug = config("jsDebug");
?>

<script src="/config.php?<?php echo $jsV?>"></script>

<?php if ($jsDebug): ?>

    <script src="/_global-library/js/bower_components/jquery/dist/jquery.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/jquery.cookie.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/jquery.count.to.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/jquery.easing.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/jquery-ui.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/jquery.ui.touch-punch.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/bower_components/jquery-slimscroll/jquery.slimscroll.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/jquery.lazyload.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/jquery.sort-elements.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/bower_components/foundation/js/foundation.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/bower_components/slick-carousel/slick/slick.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/bower_components/share-button-master/build/share.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/underscore.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/knockout/knockout.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/knockout/knockout.validation.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/knockout/knockout.config.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/moment.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/datatables.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/otherlevels-countdown.js?<?php echo $jsV?>"></script>

    <?php if ($jsSkin !== Skin::GiveBackBingo): ?>
        <script src="/_global-library/js/vendor/fastclick.js?<?php echo $jsV?>"></script>
    <?php endif; ?>
    <script src="/_global-library/js/vendor/jquery.bxslider/jquery.bxslider.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/intlTelInput/js/intlTelInput.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/intltelinput/js/utils.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/dropdown.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/sandbox.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/core.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/router.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/storage.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/dataservice.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/dataservice-web.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/dataservice-admin.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/dataservice-cashier.js?<?php echo $jsV?>"></script>
    <!-- THREE RADICAL MODULES -->
    <script src="/_global-library/js/app/services/dataservice-threeradical.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/dataservice-sockets.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/three-radical.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cma-notifications.js?<?php echo $jsV?>"></script>
    <!-- THREE RADICAL MODULES -->
    <!-- R10-851 PRIZE WHEEL MODULES -->
    <script src="/_global-library/js/app/services/dataservice-prizewheel.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/prize-wheel.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/google-manager.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/logger.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/native.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/helpers/add-modules.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/helpers/i18n.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/vm-main.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/notifier.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/login-box.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/responsible-gaming.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/dont-miss-out.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/terms-service.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/join-now.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/game-launcher.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/last-played.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/games-display.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/game-window.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/favourites.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/games-menu-search.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/reality-check.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/bingo/models/bingo-room.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/bingo/bingo-launcher.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/bingo/bingo-room-details.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/bingo/bingo-room-details-page.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/bingo/helpers/bingo-rules.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/bingo/helpers/bingo-permissions.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/bingo/bingo-scheduler.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/editors/skin-editor.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/editors/games-editor.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/account-activity.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/personal-details.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/personal-details-new.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/player-account.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/convert-points.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/redeem-promos.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/mail-inbox.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/player-refer-a-friend.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/card-token.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/cashier-data.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/credit-card.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/deposit.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/neteller.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/paysafe.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/withdrawal.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/reversal.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/bank-details.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/card-token-update.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/retry-cvn.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/card-validator.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/player-limits.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/cashier.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/quick-deposit.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/welcome.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/forgot-password.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/deposit-google-manager.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/games/games-menu-search.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/opt-in.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/preview.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/claim-cashback.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/my-account/claim-upgrade-bonus.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/track-navigation.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/content-scheduler.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/rad-switch.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/rad-filter.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/potential-players.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/vip-landing.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/owl.carousel.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/vendor/readmore.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/services/dataservice-player-management.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/account-verification.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/account-verification-simple.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/adaptive-banners.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/share.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/aspers-app-tracking.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/external-to-room.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/cashier/models/uru-document-upload.js?<?php echo $jsV?>"></script>

<?php else: ?>

    <script src="/_global-library/js/build/sbm.vendor.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/build/sbm.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 1 && $jsDebug): ?>

    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 1 && !$jsDebug): ?>

    <script src="/_global-library/js/build/sbm.spinandwin.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>


<?php if ($jsSkin == 2 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>

<?php endif; ?>


<?php if ($jsSkin == 2 && !$jsDebug): ?>

    <script src="/_global-library/js/build/sbm.kittybingo.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 3 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 3 && !$jsDebug): ?>

    <script src="/_global-library/js/build/sbm.luckypantsbingo.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 5 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/promotions.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
        <!-- WOW JS Animation https://codepen.io/TimLamber/pen/dLxbF -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/0.1.12/wow.min.js"></script>
    <!-- JS -->
    <!-- <script src="./media/ppc/get-free-spins-now/js/default.js"></script> -->
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 5 && !$jsDebug): ?>

    <script src="/_global-library/js/build/sbm.magicalvegas.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>


<?php if ($jsSkin == 6 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 6 && !$jsDebug): ?>

<script src="/_global-library/js/build/sbm.bingoextra.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>


<?php if ($jsSkin == 7 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 8 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 8 && !$jsDebug): ?>

<script src="/_global-library/js/build/sbm.luckyvip.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 9 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 9 && !$jsDebug): ?>

<script src="/_global-library/js/build/sbm.givebackbingo.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 10 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 10 && !$jsDebug): ?>

    <script src="/_global-library/js/build/sbm.regalwins.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 11 && $jsDebug): ?>
    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 11 && !$jsDebug): ?>

    <script src="/_global-library/js/build/sbm.kingjack.min.js?<?php echo $jsV?>"></script>

<?php endif; ?>

<?php if ($jsSkin == 12 && $jsDebug): ?>

    <script src="/_js/plugins/global.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/tables.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/magical-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/rad_dotdotdot.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/home-main-promo.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/twitter.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/facebook.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/slick.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/progressive-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/winners-slider.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/mobile-menu.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/featured-games.js?<?php echo $jsV?>"></script>
    <script src="/_js/plugins/owl-carousel.js?<?php echo $jsV?>"></script>


<?php endif; ?>

<?php if ($jsSkin == 12 && !$jsDebug): ?>

    <script src="/_global-library/js/build/sbm.aspers.min.js?<?php echo $jsV?>"></script>
    <script src="/_global-library/js/app/modules/aspers-app-tracking.js?<?php echo $jsV?>"></script>


<?php endif; ?>

<?php if ($jsDebug): ?>
    <script src="/_global-library/js/app.js?<?php echo $jsV?>"></script>
<?php else: ?>
    <script src="/_global-library/js/app.min.js?<?php echo $jsV?>"></script>
<?php endif; ?>
