<?php

$_GET_lower = array_change_key_case($_GET, CASE_LOWER);

if (isset($_GET["Alias"])) {
    $alias = @$_GET["Alias"];
    $roomName = @$_GET["RoomName"];
    $playerID = @$_COOKIE["PlayerID"];
    $sessionID = @$_COOKIE["SessionID"];
    $alias = @$_GET["Alias"];
    $path = config("URL") . config("BingoLegacyURL") . "?RoomName=$roomName&PlayerID=$playerID&SessionID=$sessionID&Alias=$alias&frame=true";
} else {
    $skinId = @$_GET["skinID"];
    $playerId = @$_COOKIE["PlayerID"];
    $sessionId = @$_COOKIE["SessionID"];
    $gameId = @$_GET_lower["gameid"];
    $fun = @$_GET["fun"];
    $source = @$_GET["GameLaunchSourceID"];
    $myAccountURL = config("URL").config("accountActivityURL");
    $deviceId = config("Device");

    $path = config("GameURL") . "?skinID=$skinId&PlayerID=$playerId&SessionID=$sessionId&GameID=$gameId&fun=$fun&GameLaunchSourceID=$source&DeviceId=$deviceId&MyAccountURL=$myAccountURL&frame=true";
}
?>

<?php include '_global-library/partials/reality-check/games-reality-check.php'; ?>

<iframe id="ui-game-frame" src="<?php echo $path ?>"
        style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:0px; height:0px; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
    Your browser doesn't support iframes
</iframe>

<script type="text/javascript">

    var frame = {

        screen: {
            w: 0,
            h: 0
        },

        getScreenDimensions: function () {
            var w = window,
                d = document,
                e = d.documentElement,
                g = d.getElementsByTagName('body')[0],
                w = w.innerWidth || e.clientWidth || g.clientWidth,
                h = w.innerHeight|| e.clientHeight|| g.clientHeight;

            return { w: w, h: h};
        },

        resize: function () {
            var d = this.getScreenDimensions();
            document.getElementById("ui-game-frame").style.height = d.h + "px";
            document.getElementById("ui-game-frame").style.width = d.w + "px";
        },

        setup: function () {

            this.resize();

            // Listen for resize changes
            window.addEventListener("resize", function () { this.resize(); }.bind(this), false);
        }

    };

    window.onload = function () {
        setTimeout(function () {
            frame.setup();
        }, 1000);
    };



</script>