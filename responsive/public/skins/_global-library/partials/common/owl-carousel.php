
    <?php 

    /* parameters:

     $editorBlock
     $slideTemplate*/?>


     <?php edit($this->controller,$editorBlock); ?>
      <?php if(isset($this->content[$editorBlock])) { ?>
    <ul class="slots-carousel ui-owl-carousel">
      <?php if(isset($this->content[$editorBlock][0])) {
        $row = $this->content[$editorBlock][0];
        ?>   

      <li class="ui-owl-loading-slide slide">
      <?php include $slideTemplate; ?>              
      </li>        
      <?php }?>  
      <?php foreach($this->content[$editorBlock] as $row) {?>      
      <li class="ui-owl-slide slide" style="display: none">
      <?php include $slideTemplate; ?>              
      </li>
      <?php }?>  
    </ul>
      
      <?php }?> 