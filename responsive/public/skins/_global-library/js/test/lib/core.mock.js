var sbm = sbm || {};

sbm.CoreMock = function ($, ko, _, serverconfig) {

    var self = this;

    self.$ = $;
    self.ko = ko;
    self._ = _;
    self.o = $({});
    self.logger = new sbm.Logger();
    self.serverconfig = serverconfig;
    self.storage = new sbm.Storage(self);

    /**
     * subscribe event
     */
    self.subscribe = function () {
        self.o.on.apply(self.o, arguments);
    };

    /**
     * unsubscribe event
     */
    self.unsubscribe = function () {
        self.o.off.apply(self.o, arguments);
    };

    /**
     * publish event
     */
    self.publish = function () {
        self.o.trigger.apply(self.o, arguments);
    };

    /**
     * Get i18n term
     * @param key the term key
     * @param params array with values to be replaced on the term
     * @returns {string}
     */
    self.getTranslation = function (key, params) {
        return sbm.i18n.get(key, params);
    };

    return self;

};