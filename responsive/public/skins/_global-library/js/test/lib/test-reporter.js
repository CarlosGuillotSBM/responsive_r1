var reporter = {

    failedSpecs: [],

    specDone: function (result) {

        if (result.failedExpectations.length) {
            this.failedSpecs.push({
                spec: result.description,
                exceptions: result.failedExpectations
            });
        }
    },

    jasmineDone: function () {

        var message = "",
            report = "",
            subject = "RAD unit test passed";

        if (this.failedSpecs.length) {

            subject = "RAD unit test fail";

            var i, j;

            for (i = 0; i < this.failedSpecs.length; i++) {

                message = "Spec <b>" + this.failedSpecs[i].spec + "</b> has failed";
                report += "<br/>" + message;

                for (j = 0; j < this.failedSpecs[i].exceptions.length; j++) {

                    console && console.log(this.failedSpecs[i].exceptions[j].stack);
                }
            }
            this.failedSpecs = [];

        } else {
            report = "Great!! All tests passed!!";
        }

        if (sbm.jasmine && sbm.jasmine.sendReport) {
            this.sendReport(subject, report);
        }
    },

    sendReport: function (subject, report) {
        $.ajax(
            {
                "url": "http://responsive.magicalvegas.dev/admindata.php",
                "dataType": "json",
                "data": {
                    f: "sendtestreport",
                    s: subject,
                    c: report
                },
                type: "POST"
            }
        );
    }
};

jasmine.getEnv().addReporter(reporter);