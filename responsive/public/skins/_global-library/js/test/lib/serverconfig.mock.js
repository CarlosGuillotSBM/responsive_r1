var sbm = sbm || {};

sbm.serverconfig = {
    cashierUrl: "https://coin.dagacubedev.net",
    debug: true,
    device: 1,
    editMode: true,
    gameUrl: "https://game.dagacubedev.net/radgame.php",
    ip: "127.0.0.1",
    skinId: 5,
    version: "1.0.6"
};
