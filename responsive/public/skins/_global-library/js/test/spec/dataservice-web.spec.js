/**
 * Tests for all the AJAX requests related to website data
 * Uses Jasmin as testing suite
 */

(function () {

    "use strict";

    /**
     * Initialization
     */
    var username = "nuno4",
        password = "qqqaaa111",
        email = "nuno4@yopmail.com",
        dataservice,
        dataserviceAdmin,
        core,
        sb,
        adminUrl = sbm.jasmine ? sbm.jasmine.adminApiUrl : "",
        webUrl = sbm.jasmine ? sbm.jasmine.webApiUrl : "";

    core = new sbm.CoreMock($, ko, _, sbm.serverconfig);
    sb = new sbm.Sandbox(core);

    dataserviceAdmin = new sbm.DataserviceAdmin(core, adminUrl);
    dataserviceAdmin.init();

    dataservice = new sbm.DataserviceWeb(core, webUrl);
    dataservice.init();

    /**
     * Login on the admin area
     */

    describe("Login on Admin Area", function () {

        it("Should login as nunoo", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                done();
            };

            sb.listen("editor-done-login", callback);
            sb.notify("editor-do-login", ["nunoo", "qqq111"]);
        });

    });

    /**
     * Registration
     */

    describe("Registration", function () {

        var groupId, regSessionId;

        it("Should get a registration session", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.RegSessionID).toBeDefined();
                expect(data.Currencies).toBeDefined();
                expect(data.Countries).toBeDefined();
                expect(data.Countries).toBeDefined();
                expect(data.GroupID).toBeDefined();

                groupId = data.GroupID;
                regSessionId = data.RegSessionID;

                done();
            };

            sb.listen("got-register-session", callback);
            sb.notify("get-register-session");
        });

        it("Should check that username nuno666 is available", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Suggest).not.toBeDefined();
                sb.ignore("checked-register-username");
                done();
            };

            sb.listen("checked-register-username", callback);
            sb.notify("check-register-username", ["nuno666", groupId, regSessionId]);
        });

        it("Should check that username nuno4 is not available", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(1);
                expect(data.Suggest).toBeDefined();
                done();
            };

            sb.listen("checked-register-username", callback);
            sb.notify("check-register-username", ["nuno4", groupId, regSessionId]);
        });

        it("Should check that email nuno666@yopmail.com is available", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Suggest).not.toBeDefined();
                sb.ignore("checked-register-email");
                done();
            };

            sb.listen("checked-register-email", callback);
            sb.notify("check-register-email", ["nuno666@yopmail.com", groupId, regSessionId]);
        });

        // note: on dev we are not validating the email
        xit("Should check that email nuno4@yopmail.com is not available", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(1);
                expect(data.Suggest).toBeDefined();
                done();
            };

            sb.listen("checked-register-email", callback);
            sb.notify("check-register-email", ["nuno4@yopmail.com", groupId, regSessionId]);
        });

        xit("Should get list of addresses for postcode NW51TL", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Address).toBeDefined();
                expect(data.Town).toBeDefined();
                expect(data.County).toBeDefined();
                expect(data.Postcode).toBeDefined();
                expect(data.Addresses).toBeDefined();
                done();
            };

            sb.listen("got-postcode-lookup", callback);
            sb.notify("get-postcode-lookup", ["NW51TL", regSessionId]);
        });

        it("Should register a new player", function (done) {

            var tail = Date.now() / 1000 | 0;
            var playerDetails = {
                username: "nuno" + tail,
                password: "qqq111",
                email: "nuno" + tail + "@yopmail.com",
                currency: "GBP",
                promoCode: "",
                title: "Mr",
                firstName: "Nuno",
                lastName: "Daga",
                dob: "1990-01-01",
                country: "GBR",
                address1: "Spacebar Media Ltd Highgate Studios",
                address2: "53-79 Highgate Road",
                city: "London",
                state: "Greater London",
                postcode: "NW5 1TL",
                landline: "08779879798",
                mobile: "08977987987",
                wantsEmail: 0,
                wantsSMS: 0,
                raf: "",
                src: "",
                landPage: ""
            };

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);

                done();
            };

            sb.listen("registered-player", callback);
            sb.notify("register-player", [playerDetails, groupId, regSessionId]);
        });

    });

    /**
     * Login
     */

    describe("Login", function () {

        it("Should login as " + username + ":" + password, function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.PlayerID).toBeDefined();
                expect(data.SessionID).toBeDefined();
                expect(data.Username).toBeDefined();
                expect(data.Favourites).toBeDefined();
                done();
            };

            sb.listen("done-login", callback);
            sb.notify("do-login", [username, password]);
        });

    });

    /**
     * Get balance
     */

    describe("Balance", function () {

        it("Should get balance", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Balance).toBeDefined();
                expect(data.Real).toBeDefined();
                expect(data.Bonus).toBeDefined();
                expect(data.BonusWins).toBeDefined();
                expect(data.Points).toBeDefined();
                done();
            };

            sb.listen("got-balance", callback);
            sb.notify("get-balance");
        });

    });

    /**
     * Favourites
     */

    describe("Favourites", function () {

        it("Should allow to add Wimbledon to the favourites", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Favourites).toBeDefined();

                var game = _.find(data.Favourites, function (game) {
                    return game.Game === "winbledon";
                });

                expect(game).toBeTruthy();
                sb.ignore("finish-set-favourite-game");
                done();
            };

            sb.listen("finish-set-favourite-game", callback);
            sb.notify("set-favourite-game", ["winbledon", true]);
        });

        it("Should allow to remove Wimbledon from the favourites", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Favourites).toBeDefined();

                var game = _.find(data.Favourites, function (game) {
                    return game.Game === "winbledon";
                });

                expect(game).toBeUndefined();
                done();
            };

            sb.listen("finish-set-favourite-game", callback);
            sb.notify("set-favourite-game", ["winbledon", false]);
        });
    });

    /**
     * My Account
     */

    describe("My Account", function () {

        var currentPoints;

        it("Should get player account", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Balance).toBeDefined();
                expect(data.Real).toBeDefined();
                expect(data.Bonus).toBeDefined();
                expect(data.BonusWins).toBeDefined();
                expect(data.Points).toBeDefined();
                expect(data.Funded).toBeDefined();
                expect(data.Class).toBeDefined();
                expect(data.Spins).toBeDefined();
                expect(data.Cards).toBeDefined();
                expect(data.Cashback).toBeDefined();

                currentPoints = data.Points;

                done();
            };

            sb.listen("got-player-account", callback);
            sb.notify("get-player-account");
        });

        it("Should get banking activity", function (done) {

            var data = {
                date: "20141212",
                start: 0,
                length: 2,
                sort: 0,
                dir: "asc",
                search: "",
                types: "cashier"
            };

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Returned).toEqual(2);
                expect(data.Returned).toEqual(2);
                expect(data.Data).toBeDefined();
                expect(data.Data.length).toEqual(2);
                expect(data.Data[0].row).toBeDefined();
                expect(data.Data[0].txPlayerBalanceID).toBeDefined();
                expect(data.Data[0].txType).toBeDefined();
                expect(data.Data[0].txTypeID).toBeDefined();
                expect(data.Data[0].TxDetails).toBeDefined();
                expect(data.Data[0].Date).toBeDefined();
                expect(data.Data[0].Amount).toBeDefined();
                done();
            };

            sb.listen("got-banking-activity", callback);
            sb.notify("get-banking-activity", [data]);
        });

        it("Should get promo activity", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Data).toBeDefined();
                expect(data.Data[0].ID).toBeDefined();
                expect(data.Data[0].PromoState).toBeDefined();
                expect(data.Data[0].Status).toBeDefined();
                expect(data.Data[0].PromoDescription).toBeDefined();
                expect(data.Data[0].DateGiven).toBeDefined();
                expect(data.Data[0].DateActivated).toBeDefined();
                expect(data.Data[0].DateExpires).toBeDefined();
                expect(data.Data[0].DateCompleted).toBeDefined();
                expect(data.Data[0].BonusGiven).toBeDefined();
                expect(data.Data[0].WagersRequired).toBeDefined();
                expect(data.Data[0].WagersApplied).toBeDefined();
                expect(data.Data[0].WagersToGo).toBeDefined();
                expect(data.Data[0].DisplayPercent).toBeDefined();
                expect(data.Data[0].CompletePercent).toBeDefined();
                expect(data.Data[0].ConvertibleToReal).toBeDefined();
                expect(data.Data[0].Games).toBeDefined();
                done();
            };

            sb.listen("got-promo-activity", callback);
            sb.notify("get-promo-activity");
        });

        // not getting any data on DEV
        xit("Should get games activity", function (done) {

            var data = {
                date: "20141212",
                start: 0,
                length: 2,
                sort: 0,
                dir: "asc",
                search: "",
                types: "games"
            };

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                done();
            };

            sb.listen("got-banking-activity", callback);
            sb.notify("get-banking-activity", [data]);
        });

        it("Should get player personal details", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Email).toBeDefined();
                expect(data.Title).toBeDefined();
                expect(data.FirstName).toBeDefined();
                expect(data.LastName).toBeDefined();
                expect(data.CurrencyCode).toBeDefined();
                expect(data.CountryCode).toBeDefined();
                expect(data.CountryName).toBeDefined();
                expect(data.Address1).toBeDefined();
                expect(data.Address2).toBeDefined();
                expect(data.City).toBeDefined();
                expect(data.State).toBeDefined();
                expect(data.Postcode).toBeDefined();
                expect(data.LandPhone).toBeDefined();
                expect(data.MobilePhone).toBeDefined();
                expect(data.SecurityQuestionID).toBeDefined();
                expect(data.SecurityQuestion).toBeDefined();
                expect(data.SecurityAnswer).toBeDefined();
                expect(data.WantsEmail).toBeDefined();
                expect(data.SendSMS).toBeDefined();
                expect(data.LastLoginDate).toBeDefined();
                expect(data.Security).toBeDefined();
                done();
            };

            sb.listen("got-personal-details", callback);
            sb.notify("get-personal-details");

        });

        it("Should save player personal details", function (done) {

            var data = {
                currentPassword: password,
                newPassword: "",
                landPhone: "",
                landPhoneExt: "",
                mobilePhone: Math.floor(Math.random() * 78454789),
                securityQuestion: 1,
                securityAnswer: "xxx",
                wantsEmail: false,
                wantsSMS: false,
            };

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                done();
            };

            sb.listen("saved-personal-details", callback);
            sb.notify("save-personal-details", [data]);

        });

        it("Should allow player to redeem points", function (done) {

            var pointsToConvert = 1;

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Points).toEqual(currentPoints - pointsToConvert);
                done();
            };

            sb.listen("converted-points", callback);
            sb.notify("convert-points", [pointsToConvert, 1]);
        });
    });

    /**
     * Search games
     */
    describe("Search for games", function () {

        it("Should allow a player to search for a game", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.Games).toBeDefined();
                done();
            };

            sb.listen("searched-games", callback);
            sb.notify("search-games", ["grease"]);
        });

    });

    /**
     * Winners feed
     */
    describe("Winners feed", function () {

        it("Should get the list of winners", function (done) {

            var callback = function (e, data) {
                expect(data).toBeDefined();
                if (data.length > 0) {
                    expect(data[0].WinType).toBeDefined();
                    expect(data[0].WinAmount).toBeDefined();
                    expect(data[0].Username).toBeDefined();
                    expect(data[0].GameName).toBeDefined();
                    expect(data[0].Country).toBeDefined();
                    expect(data[0].Avatar).toBeDefined();
                }
                done();
            };

            sb.listen("got-winners-feed", callback);
            sb.notify("get-winners-feed");
        });

    });

    /**
     * Forgot Password
     */

    describe("Forgot password", function () {

        it("Should allow a player to request an email to reset the password", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                done();
            };

            sb.listen("forgot-password-finished", callback);
            sb.notify("forgot-password", email);
        });
    });

    /**
     * Players current limits
     */

    describe("Player limits", function () {

        var limits = {
            wls : (100 + Math.random() * 10).toFixed(2),
            wld : (200 + Math.random() * 10).toFixed(2),
            wlw : (300 + Math.random() * 10).toFixed(2),
            wlm : (400 + Math.random() * 10).toFixed(2),
            lls : (500 + Math.random() * 10).toFixed(2),
            lld : (600 + Math.random() * 10).toFixed(2),
            llw : (700 + Math.random() * 10).toFixed(2),
            llm : (800 + Math.random() * 10).toFixed(2)
        };

        it("Should get player limits", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.WagerLimitSession).toBeDefined(0);
                expect(data.WagerLimitDay).toBeDefined();
                expect(data.WagerLimitWeek).toBeDefined();
                expect(data.WagerLimitMonth).toBeDefined();
                expect(data.LossLimitSession).toBeDefined();
                expect(data.LossLimitDay).toBeDefined();
                expect(data.LossLimitWeek).toBeDefined();
                expect(data.LossLimitMonth).toBeDefined();
                done();
            };

            sb.notify("get-player-limits");
            sb.listen("got-player-limits", callback);
        });

        xit("Should set player limits", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                done();
            };

            sb.notify("set-player-limits", [limits]);
            sb.listen("finished-set-player-limits", callback);
        });

        xit("Should have changed the limits", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.WagerLimitSession).toEqual(limits.wls);
                expect(data.WagerLimitDay).toEqual(limits.wld);
                expect(data.WagerLimitWeek).toEqual(limits.wlw);
                expect(data.WagerLimitMonth).toEqual(limits.wlm);
                expect(data.LossLimitSession).toEqual(limits.lls);
                expect(data.LossLimitDay).toEqual(limits.lld);
                expect(data.LossLimitWeek).toEqual(limits.llw);
                expect(data.LossLimitMonth).toEqual(limits.llm);
                done();
            };

            sb.notify("get-player-limits");
            sb.listen("got-player-limits", callback);
        });
    });

    /**
     * Logout
     */

    describe("Logout", function () {

        it("Should logout player", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                done();
            };

            sb.listen("done-logout", callback);
            sb.notify("do-logout");
        });

        it("Should get session has expired", function (done) {

            var callback = function () {
                expect(1).toEqual(1);
                done();
            };

            sb.listen("session-has-expired", callback);
            sb.notify("get-balance");
        });
    });

}());