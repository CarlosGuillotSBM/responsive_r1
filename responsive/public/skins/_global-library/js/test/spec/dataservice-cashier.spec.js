/**
 * Tests for all the AJAX requests related to Cashier
 * Uses Jasmin as testing suite
 */

(function () {

    "use strict";

    /**
     * Initialization
     */
    var username = "nuno888",
        password = "qqq111",
        source = "Cas",
        playerId,
        dataserviceWeb,
        dataserviceCashier,
        dataserviceAdmin,
        core,
        sb,
        cashierData,
        availableAccountTypes,
        customerAccounts,
        withdrawDetail,
        reverseDetail,
        adminUrl = sbm.jasmine ? sbm.jasmine.adminApiUrl : "",
        webUrl = sbm.jasmine ? sbm.jasmine.webApiUrl : "";

    core = new sbm.CoreMock($, ko, _, sbm.serverconfig);
    sb = new sbm.Sandbox(core);

    dataserviceAdmin = new sbm.DataserviceAdmin(core, adminUrl);
    dataserviceAdmin.init();

    dataserviceWeb = new sbm.DataserviceWeb(core, webUrl);
    dataserviceWeb.init();

    dataserviceCashier = new sbm.DataserviceCashier(core, sbm.serverconfig.cashierUrl);
    dataserviceCashier.init();

    /**
     * Login on the admin area
     */

    describe("Login on Admin Area", function () {

        it("Should login as nunoo", function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                done();
            };

            sb.listen("editor-done-login", callback);
            sb.notify("editor-do-login", ["nunoo", "qqq111"]);
        });

    });

    /**
     * Login on the Website
     */

    describe("Login on Website", function () {

        it("Should login as " + username + ":" + password, function (done) {

            var callback = function (e, data) {
                expect(data.Code).toEqual(0);
                expect(data.SessionID).toBeDefined();
                expect(data.PlayerID).toBeDefined();

                // save data on storage
                sb.saveOnSessionStorage("playerId", data.PlayerID);
                sb.saveOnSessionStorage("sessionId", data.SessionID);
                // create cashier data
                cashierData = new sbm.CashierData(sb, source);

                done();
            };

            sb.listen("done-login", callback);
            sb.notify("do-login", [username, password]);
        });

    });

    /**
     * Start
     */

    describe("Cashier start", function () {

        it("Start the cashier", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                expect(data.CustomerID).toBeDefined();
                expect(data.Firstname).toBeDefined();
                expect(data.Lastname).toBeDefined();
                expect(data.Verified).toBeDefined();
                expect(data.MaxAccounts).toBeDefined();
                expect(data.ActiveAccounts).toBeDefined();
                expect(data.WithdrawalsPending).toBeDefined();
                expect(data.ForceReversalOfPendingWithdrawal).toBeDefined();
                expect(data.SelfExclude).toBeDefined();
                expect(data.SecsSinceReg).toBeDefined();
                expect(data.Deposited).toBeDefined();
                expect(data.ShowWireDetails).toBeDefined();
                done();
            };

            sb.listen("cashier-started", callback);
            sb.notify("cashier-start", [cashierData]);
        });

    });

    /**
     * Get Account Types
     */

    describe("Cashier get account types", function () {

        it("Should get available account types", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                expect(data.AccountTypes).toBeDefined();
                expect(data.AccountTypes.length).toBeGreaterThan(0);

                availableAccountTypes = data.AccountTypes;
                done();
            };

            sb.listen("cashier-got-account-types", callback);
            sb.notify("cashier-get-account-types");
        });

    });

    /**
     * Get Customer Accounts
     */

    describe("Cashier get customer accounts", function () {

        it("Should get player accounts", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                expect(data.Accounts).toBeDefined();
                expect(data.Accounts.length).toBeGreaterThan(0);

                customerAccounts = data.Accounts;
                done();
            };

            sb.listen("cashier-got-customer-accounts", callback);
            sb.notify("cashier-get-customer-accounts");
        });

    });

    /**
     * Deposit
     */

    describe("Cashier deposit", function () {

        it("Should allow player to deposit with his credit card", function (done) {

            var ccAccount = core._.find(customerAccounts, function (account) {
                return account.AccountTypeCode === "A_VISA_CREDIT";
            });

            expect(ccAccount).toBeDefined();

            var deposit = {
                promoId: sb.ko.observable(0),
                promoCode: sb.ko.observable(0),
                iDoNotWantBonus: sb.ko.observable(0),
                needsCvn: sb.ko.observable(1),
                cvnOption: sb.ko.observable(0),
                accountId: ccAccount.AccountID,
                accountType: sb.ko.observable("A_CC"),
                amount: sb.ko.observable(ccAccount.Amounts[0].Amount),
                affCredit: "",
                cvn: sb.ko.observable("111")
            };

            var addingNewMethod = {
                card: {
                    expiryYear: sb.ko.observable(""),
                    expiryMonth: sb.ko.observable(""),
                    cvn: sb.ko.observable(""),
                    firstName: sb.ko.observable(""),
                    lastName: sb.ko.observable("")
                },
                neteller: {
                    number: sb.ko.observable(""),
                    secureId: sb.ko.observable("")
                },
                paysafe: {
                    firstName: sb.ko.observable(""),
                    lastName: sb.ko.observable(""),
                    email: sb.ko.observable(""),
                    dob: sb.ko.observable("")
                }
            };

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                expect(data.CoinRef).toBeDefined();
                expect(data.Amount).toBeDefined();
                expect(data.Bonus).toBeDefined();
                expect(data.FreeSpins).toBeDefined();
                expect(data.Balance).toBeDefined();
                expect(data.FirstDeposit).toBeDefined();
                expect(data.DepositSuccessCount).toBeDefined();

                sb.ignore("cashier-deposited");
                done();
            };

            sb.listen("cashier-deposited", callback);
            sb.notify("cashier-deposit", [addingNewMethod, deposit]);
        });

        it("Should allow player to retry cvn", function (done) {

            var ccAccount = core._.find(customerAccounts, function (account) {
                return account.AccountTypeCode === "A_VISA_CREDIT";
            });

            expect(ccAccount).toBeDefined();

            var deposit = {
                promoId: sb.ko.observable(0),
                promoCode: sb.ko.observable(0),
                iDoNotWantBonus: sb.ko.observable(0),
                needsCvn: sb.ko.observable(1),
                cvnOption: sb.ko.observable(0),
                accountId: ccAccount.AccountID,
                accountType: sb.ko.observable("A_CC"),
                amount: sb.ko.observable(ccAccount.Amounts[0].Amount),
                affCredit: "",
                cvn: sb.ko.observable("666")
            };

            var addingNewMethod = {
                card: {
                    expiryYear: sb.ko.observable(""),
                    expiryMonth: sb.ko.observable(""),
                    cvn: sb.ko.observable(""),
                    firstName: sb.ko.observable(""),
                    lastName: sb.ko.observable("")
                },
                neteller: {
                    number: sb.ko.observable(""),
                    secureId: sb.ko.observable("")
                },
                paysafe: {
                    firstName: sb.ko.observable(""),
                    lastName: sb.ko.observable(""),
                    email: sb.ko.observable(""),
                    dob: sb.ko.observable("")
                }
            };

            var callback = function (e, data) {
                expect(data.Status).toEqual("46");
                expect(data.TxDepositID).toBeDefined();
                expect(data.dsid).toBeDefined();

                // retry request
                var retried = function (e, data) {
                    expect(data.Status).toEqual("1");
                    done();
                };

                var params = {
                    cvn: sb.ko.observable("111"),
                    txDepositId: data.TxDepositID,
                    dsid: data.dsid
                };

                sb.listen("cashier-deposit-retried-CVN", retried);
                sb.notify("cashier-deposit-retry-CVN", [params]);
            };

            sb.listen("cashier-deposited", callback);
            sb.notify("cashier-deposit", [addingNewMethod, deposit]);
        });

    });

    /**
     * Withdraw
     */

    describe("Cashier Withdraw", function () {

        it("Should get withdrawal detail", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                expect(data.RealBalance).toBeDefined();
                expect(data.AvailableToWithdraw).toBeDefined();
                expect(data.BonusToBeForfeited).toBeDefined();
                expect(data.MinWithdraw).toBeDefined();
                expect(data.WithdrawalsPending).toBeDefined();
                expect(data.WithdrawHoursPending).toBeDefined();

                withdrawDetail = data;
                done();
            };

            sb.listen("cashier-got-withdraw-detail", callback);
            sb.notify("cashier-get-withdraw-detail");

        });

        it("Should allow a player to withdraw money", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                done();
            };

            sb.listen("cashier-done-withdrawal", callback);
            sb.notify("cashier-do-withdrawal", withdrawDetail.AvailableToWithdraw);
        });
    });

    /**
     * Reverse
     */

    describe("Cashier Reverse", function () {

        it("Should get reversal detail", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                expect(data.AvailableToReverse).toBeDefined();
                expect(data.MinReverse).toBeDefined();
                expect(data.MinWithdraw).toBeDefined();

                reverseDetail = data;
                done();
            };

            sb.listen("cashier-got-reverse-detail", callback);
            sb.notify("cashier-get-reverse-detail");

        });

        it("Should allow a player to reverse a withdrawal", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                done();
            };

            sb.listen("cashier-done-reverse", callback);
            sb.notify("cashier-do-reversal", reverseDetail.AvailableToReverse);
        });
    });

    /**
     * Wire Details
     */

    describe("Cashier Wire Details", function () {

        it("Should get wire details", function (done) {

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");
                expect(data.BankName).toBeDefined();
                expect(data.AccountName).toBeDefined();
                expect(data.AccountNumber).toBeDefined();
                expect(data.BranchCode).toBeDefined();
                expect(data.SwiftCode).toBeDefined();
                expect(data.Address).toBeDefined();
                expect(data.City).toBeDefined();
                expect(data.State).toBeDefined();
                expect(data.Country).toBeDefined();
                expect(data.PostCode).toBeDefined();
                expect(data.CorrespondingBankName).toBeDefined();
                expect(data.WireStatus).toBeDefined();

                sb.ignore("cashier-got-wire-details");
                done();
            };

            sb.listen("cashier-got-wire-details", callback);
            sb.notify("cashier-get-wire-details");

        });

        it("Should allow a player to set the wire details", function (done) {

            var tail = Math.floor(Math.random() * 99);

            var newDetails = {
                bankName: "BANK OF LONDON" + tail,
                accountName: "NUNO OLIVEIRA",
                accountNumber: "123456789" + tail,
                branchCode: "BRANCH" + tail,
                swiftCode: "SWFTCODE" + tail,
                address: "KENTISH TOWN" + tail,
                city: "LONDON",
                state: "GREATER LONDON",
                postcode: "NW51TL",
                correspondingBankName: "BK" + tail,
                wireStatus: 0
            };

            var callback = function (e, data) {
                expect(data.Status).toEqual("1");

                // get details again and check if they were saved
                var gotDetailsAgain = function (e, data) {
                    expect(data.Status).toEqual("1");
                    expect(data.BankName).toEqual(newDetails.bankName);
                    expect(data.AccountName).toEqual(newDetails.accountName);
                    expect(data.AccountNumber).toEqual(newDetails.accountNumber);
                    done();
                };

                sb.listen("cashier-got-wire-details", gotDetailsAgain);
                sb.notify("cashier-get-wire-details");

            };

            sb.listen("cashier-saved-bank-details", callback);
            sb.notify("cashier-save-bank-details", newDetails);
        });
    });

}());