var sbm = sbm || {};

/**
 * The Sandbox
 * Hides the Core implementation
 * Exposes a simple API to the modules
 * Should be very stable without big changes
 *
 * @param core
 * @returns {Sandbox}
 * @constructor
 */
sbm.Sandbox = function(core) {
  "use strict";

  var self = this;

  /**
   * Ignore a subscribed event message
   * @param e Event
   */
  self.ignore = function(e) {
    core.unsubscribe(e);
  };

  /**
   * Subscribe an event message
   * @param e Event
   * @param data Data
   */
  self.listen = function(e, data) {
    core.subscribe(e, data);
  };

  /**
   * Logs an error
   * @param msg Error message
   */
  self.logError = function(msg) {
    core.logger.log(msg, core.logger.severity.ERROR);
  };

  /**
   * Logs information
   * @param msg Information message
   */
  self.logInfo = function(msg) {
    core.logger.log(msg, core.logger.severity.INFO);
  };

  /**
   * Logs a warning
   * @param msg Warning message
   */
  self.logWarning = function(msg) {
    core.logger.log(msg, core.logger.severity.WARNING);
  };

  /**
   * Publish an event
   * @param e Event
   * @param data Data
   *
   * If not get-balance event then show loading animation
   */
  self.notify = function(e, data) {
    if (
      e !== "get-balance" &&
      e !== "add-last-game" &&
      e !== "launch-game" &&
      e !== "get-bingo-rooms" &&
      e !== "messages-get-inbox" &&
      e !== "set-redirect-URL"
    ) {
      core.publish("show-notification-loading");
    }
    core.publish(e, data);
  };

  /**
   * Show success dialog
   * @param msg
   */
  self.showSuccess = function(msg, onConfirm) {
    msg && core.publish("show-notification-success", [msg, onConfirm, "alert"]);
  };

  /**
   * Show error dialog
   * @param msg
   */
  self.showError = function(msg, onConfirm) {
    msg && core.publish("show-notification-error", [msg, onConfirm, "alert"]);
  };

  /**
   * Show information dialog
   * @param msg
   */
  self.showInfo = function(msg) {
    msg && core.publish("show-notification-info", [msg, "alert"]);
  };

  /**
   * Show warning dialog
   * @param msg
   */
  self.showWarning = function(msg) {
    msg && core.publish("show-notification-warning", [msg, "alert"]);
  };

  /**
   * Show confirmation dialog
   * @param msg
   * @onConfirm Callback to be invoked if player confirms
   * @onCancelled Callback to be invoked if player cancels
   */
  self.showConfirm = function(
    msg,
    onConfirm,
    onCancelled,
    title,
    hideAfterConfirmation = true
  ) {
    msg &&
      core.publish("show-notification-confirm", [
        msg,
        onConfirm,
        onCancelled,
        title,
        hideAfterConfirmation
      ]);
  };
  self.showAreYouSure = function(msg, onConfirm, onCancelled, title) {
    msg &&
      core.publish("show-notification-are-you-sure", [
        msg,
        onConfirm,
        onCancelled,
        title
      ]);
  };
  /**
   * Show ThreeRadical dialog
   * @param msg
   * @function onConfirm
   */
  self.showTreeRadicalBox = function(msg, onConfirm) {
    msg &&
      core.publish("show-notification-threeRadical", [
        msg.replace("3 Radical -", ""),
        onConfirm,
        "alert"
      ]);
  };
  /**
   * Show ThreeRadical dialog
   * @param msg
   */
  self.showPrizeWheelBox = function(msg) {
    msg && core.publish("show-notification-prizeWheel", [msg, "alert"]);
  };
  /**
   * Show ThreeRadical dialog
   * @param msg
   */
  self.showPleaseWaitBox = function(title, msg) {
    msg &&
      core.publish("show-notification-please-wait", [title, msg, "loading"]);
  };

  /**
   * Show success notification
   * @param msg
   */
  self.notifySuccess = function(msg) {
    msg && core.publish("show-notification-success", [msg, "notification"]);
  };

  /**
   * Show error notification
   * @param msg
   */
  self.notifyError = function(msg) {
    msg && core.publish("show-notification-error", [msg, "notification"]);
  };

  /**
   * Show information notification
   * @param msg
   */
  self.notifyInfo = function(msg) {
    msg && core.publish("show-notification-info", [msg, "notification"]);
  };

  /**
   * Show warning notification
   * @param msg
   */
  self.notifyWarning = function(msg) {
    msg && core.publish("show-notification-warning", [msg, "notification"]);
  };

  self.showThreeRadicalBox = function(msg, onConfirm) {
    msg &&
      core.publish("show-notification-threeRadical", [msg, onConfirm, "alert"]);
  };

  self.showURUDialog = function(type, title, message) {
    msg && core.publish("show-notification-uru-check", [type, title, message]);
  };
  /**
   * Initializes a route
   * @param path The pathname for the route
   */
  self.initRoute = function(path) {
    core.router.startRoute(path);
  };

  /**
   * Get value from storage
   * @param key The key's name
   * @returns {*} Object
   */
  self.getFromStorage = function(key) {
    return core.storage.get(key);
  };

  /**
   * Save on local storage
   * @param key
   * @param value
   */
  self.saveOnStorage = function(key, value) {
    core.storage.set(key, value);
  };

  /**
   * Save on session storage
   * @param key
   * @param value
   */
  self.saveOnSessionStorage = function(key, value) {
    core.storage.set(key, value, true);
  };

  /**
   * Remove from storage
   * @param key
   */
  self.removeFromStorage = function(key) {
    core.storage.remove(key);
  };

  /**
   * Save on a cookie
   * @param key
   * @param value
   */
  self.setCookie = function(key, value) {
    core.storage.setCookie(key, value);
  };

  /**
   * Get cookie value
   * @param key
   * @returns {*} Object
   */
  self.getCookie = function(key) {
    return core.storage.getCookie(key);
  };

  /**
   * Remove cookies
   * @param cookies Array with the cookies
   */
  self.removeCookies = function(cookies) {
    core.storage.clearCookies(cookies);
  };

  /**
   * Get query parameter from URL
   * @param parameter
   * @returns {string|*}
   */
  self.getUrlParameter = function(parameter) {
    return core.getUrlParameterByName(parameter);
  };

  /**
   * Show loading animation
   */
  self.showLoadingAnimation = function() {
    core.publish("show-notification-loading");
  };

  /**
   * Hide loading animation
   */
  self.hideLoadingAnimation = function() {
    core.publish("hide-notification-loading");
  };

  /**
   * Post to URL
   * @param path
   * @param params
   */
  self.postToUrl = function(path, params) {
    core.postToUrl(path, params);
  };

  /**
   * Get current device
   * @returns {*|string}
   */
  self.getDevice = function() {
    return core.getDevice();
  };

  /**
   * Get translation for a given term
   * @param key Term
   * @param params array with values to be replaced on the term
   * @returns {string}
   */
  self.i18n = function(key, params) {
    return core.getTranslation(key, params);
  };

  /**
   * Track Navigation
   * @action the action of the player
   * @label the extra info which you want to pass through
   */
  self.trackNavigation = function(action, label) {
    self.notify("track-navigation", [
      location.protocol + "//" + location.host + location.pathname,
      action,
      label
    ]);
  };

  /**
   * Send data to Google Tag Manager
   * @param category
   * @param label
   */
  self.sendDataToGoogle = function(category, label) {
    core.sendToGoogleManager(category + "," + label);
  };

  /**
   * Send a variable to the GTM
   * @param gtag
   */
  self.sendVariableToGoogleManager = function(key, value) {
    core.sendVariableToGoogleManager(key, value);
  };

  /**
   * Send a set of variable to the GTM
   * @param gtag
   */
  self.sendSetOfVariablesToGoogleManager = function(variables) {
    core.sendSetOfVariablesToGoogleManager(variables);
  };

  self.validSession = function() {
    return self.getFromStorage("sessionId") || false;
  };

  /**
   * Given a playerClass it returns the translated name of the class
   * @param category
   * @param label
   */
  self.getPlayerClassName = function(playerClassId) {
    return core.getTranslation("level" + playerClassId, []);
  };

  /**
   * Checks if a game/room is favourite
   * @param game Game Id or Room Id
   * @returns {*|boolean} True if is favourite otherwise false
   */
  self.isFavourite = function(game) {
    return core.isFavourite(game);
  };

  /** Navigate on login */

  self.navigateOnLogin = function(url) {
    return core.router.vmMain.navigateOnLogin(url);
  };

  self.ko = core.ko;
  self.$ = core.$;
  self._ = core._;
  self.serverconfig = core.serverconfig;

  return self;
};
