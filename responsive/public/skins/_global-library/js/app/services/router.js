var sbm = sbm || {};

/**
* Module responsible for the initialization of each route ("/slots/", "/register/" etc)
* It will check on each route, the modules and plugins that are necessary to run
* @param core
* @param options
* @constructor
*/
sbm.Router = function (core, options) {
    
    "use strict";
    
    var self = this;
    var isFromNewPage = true;
    
    // the most top object of the view model - all the other modules will be added to this object
    self.vmMain = null;
    
    // array with the registered modules on the page - these are defined throughout the html
    self.registeredModules = [];
    
    /**
    * module initialization
    */
    self.init = function () {
        
        // show the splash?
        if (sbm.serverconfig.showSplash && !core.$.cookie("splash")) {
            
            if (document.location.pathname === "/") {
                if (!document.referrer) {
                    self.tvAddsOn();
                    return;
                }
            } else {
                core.$.cookie("splash", 1, { expires: 365, path: '/' });
            }
            
        }
        if(isFromNewPage){
            sbm.core.publish("prize-wheel-check-for-prizes");
            isFromNewPage = false;
        }
        
        // on navigating on history (back and forward on browser)
        window.onpopstate = function (e) {
            var path = window.location.pathname;
            var state = e.state;
            
            if (path === "/cashier/" && state) {
                core.publish("cashier-navigation", state);
            }
            
            // add some exclusions
            var excluded = path.indexOf("/cashier/") > -1 || path.indexOf("/forgot-password/") > -1;
            
            // start route if was not excluded
            if (!excluded) {
                self.startRoute(path, true);
            }
        };
        
        self.reset();
        
        self.setModulesAndPlugins(window.skinModules, window.skinPlugins);
        
        // foundation initialization
        core.$(document).foundation();
        
        self.startModules(sbm.modules);
        self.startPlugins(sbm.plugins);
        
        self.resetGlobalArrays();
        
        self.checkForValidSession();
        
        core.googleManager.init();
        
        // apply ko bindings
        try {
            core.ko.applyBindings(self.vmMain, window.document.body);
        } catch (e) {
            alert("Sorry there was an error while processing bindings. \n If the error persists contact support.");
            console && console.error(e);
        }
        
        // force modal to be on top 0px
        //core.$(".reveal-modal").css({ top: '0px' });
        
        
        // images lazy loading
        
        core.$("img.lazy").lazyload({
            threshold : 100,
            failure_limit : 8,
            effect : "fadeIn"
        });
        
        core.$("div.lazy").lazyload();
        
        // hijack links
        if (sbm.serverconfig.contentSwap) {
            self.hijack();
        }
        
        self.checkCookies();
        
        if (sbm.serverconfig.skinId !== 9) {
            FastClick.attach(document.body);
        }
        
        core.publish("hide-notification-loading");
        
        // After navigation check if we should hide or show recaptcha accordingly
        var url = window.location.pathname;
        if( url.indexOf("/login/") > -1 ){
            core.publish("show-recaptcha");
        }
        else{
            core.publish("hide-recaptcha");
        }
        core.publish("prize-wheel-get-player-prizes",[]);
        
        // opens a tab by default
        var tab = core.getUrlParameterByName("tab");
        tab && self.openTab(tab);
        
    };
    
    /**
    * Resets a route
    * Removes ko bindings
    * Destroy previous modules
    * Creates and initializes the top most model view
    */
    self.reset = function () {
        
        core.ko.cleanNode(window.document.body);
        core.$(document).off();
        self.destroyModules();
        
        if (self.vmMain) {
            self.vmMain.destroy();
        }
        
        self.vmMain = new sbm.VmMain(new sbm.Sandbox(core), options);
        self.vmMain.init();
    };
    
    /**
    * Adds the modules and plugins specified on DOM to the local arrays
    * @param modules
    * @param plugins
    */
    self.setModulesAndPlugins = function (modules, plugins) {
        
        // adding modules
        if (modules && modules.length) {
            _.each(modules, function (module) {
                sbm.addModule(module);
            });
        }
        
        // adding plugins
        if (plugins && plugins.length) {
            _.each(plugins, function (plugin) {
                sbm.addPlugin(plugin);
            });
        }
        
    };
    
    /**
    * Start all modules added to the page
    * @param modules array
    */
    self.startModules = function (modules) {
        
        if (typeof sbm.Sandbox === "function") {
            
            var i = 0,
            l = modules ? modules.length : 0;
            
            var moduleId, moduleOptions, Module, vm;
            
            for (i; i < l; i++) {
                
                moduleId = modules[i].id;
                moduleOptions = modules[i].options;
                Module = sbm[moduleId];
                
                if (typeof Module === "function") {
                    
                    vm = new Module(new sbm.Sandbox(core), moduleOptions);
                    if (typeof vm.init === "function") {
                        vm.init();
                        core.logger.log("Module " + moduleId + " init", core.logger.severity.INFO);
                    } else {
                        core.logger.log("No init function defined on module " + moduleId, core.logger.severity.WARNING);
                    }
                    self.registeredModules.push(moduleId);
                    self.vmMain[moduleId] = vm;
                    
                } else {
                    core.logger.log("Could not apply bindings to view: module " + moduleId + " not found", core.logger.severity.FATAL);
                }
                
            }
            
        } else {
            core.logger.log("Could not start module: Sandbox is not defined");
        }
        
    };
    
    /**
    * Destroy all registered modules
    */
    self.destroyModules = function () {
        
        if (self.registeredModules.length > 0) {
            var i;
            for (i = 0; i < self.registeredModules.length; i++) {
                if (typeof self.vmMain[self.registeredModules[i]].destroy === "function") {
                    self.vmMain[self.registeredModules[i]].destroy();
                    core.logger.log("Module " + self.registeredModules[i] + " was destroyed", core.logger.severity.INFO);
                } else {
                    core.logger.log("No destroy function defined on module " + self.registeredModules[i], core.logger.severity.WARNING);
                }
            }
            self.registeredModules = [];
        }
        
    };
    
    /**
    * Starts plugins
    * @param plugins array
    */
    self.startPlugins = function (plugins) {
        
        var i = 0,
        l = plugins ? plugins.length : 0;
        
        for (i; i < l; i++) {
            
            var pluginName = plugins[i].id;
            var pluginOptions = plugins[i].options;
            var Plugin = sbm[pluginName];
            
            if (typeof Plugin === "function") {
                if (pluginOptions && Array.isArray(pluginOptions)) {
                    var m = pluginOptions.length, n = 0;
                    for (n; n < m; n++) {
                        var p = new Plugin(pluginOptions[n]);
                        if (typeof p.run === "function") {
                            if (pluginName === "Slick") {
                                // needs the sandbox
                                p.run(new sbm.Sandbox(core), self.vmMain);
                            } else {
                                p.run();
                            }
                            core.logger.log("Plugin " + pluginName + " init", core.logger.severity.INFO);
                        } else {
                            core.logger.log("Could not run Plugin " + pluginName + ": function run not found", core.logger.severity.ERROR);
                            return;
                        }
                    }
                    
                } else {
                    var p = new Plugin(pluginOptions);
                    if (typeof p.run === "function") {
                        p.run();
                        core.logger.log("Plugin " + pluginName + " init", core.logger.severity.INFO);
                    } else {
                        core.logger.log("Could not run Plugin " + pluginName + ": function run not found", core.logger.severity.ERROR);
                        return;
                    }
                }
                
                
            } else {
                core.logger.log("Plugin " + pluginName + " not found", core.logger.severity.ERROR);
                return;
            }
            
        }
        
    };
    
    /**
    * Loads the content for a given route
    * The content will be retrieved via AJAX request to server
    * @param path the route path
    * @param callback function to call when succeeded
    */
    self.loadContent = function (path, callback) {
        
        core.$.getJSON("/content.php", {cid: path, format: "json"}, function (json) {
            
            // store page key on hidden field
            core.$("#ui-cache-key").val(json.key);
            
            // set html content
            core.$.each(json, function (key, value) {
                core.$(key).html(value);
            });
            
            if (json.title) document.title = json.title;
            
            callback();
            
        }).fail(function () {
            window.location = "/error/";
        });
        
    };
    
    /**
    * Link hijack
    * Get the href of the link and starts that route
    */
    self.hijack = function () {
        
        if (!history.pushState) {
            return;
        }
        
        core.$(document).on("click", "a[data-hijack=true]", function (e) {
            
            e.preventDefault();
            core.publish("show-notification-loading");
            var href = core.$(this).attr("href");
            
            if (href) {
                // adding leading slash
                if (href.charAt(0) !== "/") {
                    href = "/" + href;
                }
                // adding trailing slash
                if (href.slice(-1) !== "/") {
                    href += "/";
                }
            }
            self.startRoute(href);
        });
        
    };
    
    /**
    * Starts a specific route
    * @param route path
    * @param nopush boolean if set to true we will push the history
    */
    self.startRoute = function (route, nopush) {
        if (sbm.serverconfig.contentSwap) {
            self.startRouteWithAjax(route, nopush);
        } else {
            self.startRouteFullLoad(route);
        }
    };
    
    /**
    * Loads new page redirecting the player
    * @param route
    */
    self.startRouteFullLoad = function (route) {
        window.location.pathname = route;
    };
    
    /**
    * Loads new page with content swap
    * @param route
    * @param nopush
    */
    self.startRouteWithAjax = function (route, nopush) {
        if (route) {
            
            // if route is home and has valid session - go to start page
            if (route === "/") {
                if (self.checkForValidSession()) {
                    route = "/start/";
                }
            }
            
            // close any reveal modal that might be opened
            var $allModals = core.$(".reveal-modal");
            if ($allModals.length > 0) {
                $allModals.foundation("reveal", "close");
                // make sure the calendar close in the activity pages
                $("#ui-datepicker-div").hide();
            }
            
            var afterLoadContent = function () {
                $(window).trigger("newContentLoaded");
                window.scrollTo(0, 0);
                // init route again
                self.init(route.split("/")[1]);
                // clear unnecessary cookies
                core.$.cookie("SkinID") && core.storage.init();
                // init tooltips
                // self.initTooltips("#ajax-wrapper");
            };
            
            self.loadContent(route, afterLoadContent);
            
            if (history.pushState) {
                !nopush && history.pushState("", route, route);
            }
            
        }
    };
    
    /**
    * Checks if there is a valid session
    * @returns bolean
    */
    self.checkForValidSession = function () {
        var hasValidSession = core.storage.get("sessionId") || false;
        self.vmMain.validSession(hasValidSession);
        return hasValidSession;
    };
    
    /**
    * check if we got the needed cookies
    * if not remove cookie LanguageCode - this will force to get them again
    */
    self.checkCookies = function () {
        
        if (!core.$.cookie("CurrencySymbol")) {
            core.$.removeCookie("LanguageCode", { path: '/' });
        }
    };
    
    /**
    * Replace the actual page with the contents from the /splash/ page
    * Removes any unwanted elements, navigation bars, initial loading panel etc
    */
    self.tvAddsOn = function () {
        
        core.$.cookie("splash", 1, { expires: 365, path: '/' });
        
        core.$.getJSON("/content.php", {cid: "/splash/", format: "json"}, function (json) {
            
            core.$(".header-mobile").remove();
            core.$(".nav-for-mobile").remove();
            core.$(".header-desktop").remove();
            core.$("#ajax-wrapper").css("margin-top", "0px");
            // store page key on hidden field
            core.$("#ui-cache-key").val(json.key);
            
            // set html content
            core.$.each(json, function (key, value) {
                core.$(key).html(value);
            });
            
            core.$("#ui-first-loader").remove();
            
        }).fail(function () {
            alert("Something went wrong, try to reload the page again please.");
        });
        
        core.$(".loading").hide();
    };
    
    /**
    * Clear non global modules and plugins
    */
    self.resetGlobalArrays = function () {
        
        window.skinModules = core._.reject(window.skinModules, function (m) {
            if (m.options && Array.isArray(m.options)) {
                m.options =  core._.reject(m.options, function (w) {
                    return (!w.options || !w.options.global);
                });
            } else {
                return (!m.options || !m.options.global);
            }   
        });
        
        window.skinPlugins = core._.reject(window.skinPlugins, function (p){
            if (p.options && Array.isArray(p.options)) {
                p.options =  core._.reject(p.options, function (y) {
                    return (!y.options || !y.options.global);
                });
            } else {
                return (!p.options || !p.options.global);
            }   
        });
        
        sbm.resetModules();
        sbm.resetPlugins();
        
    };
    
    /**
    * Initializes jquery ui tooltips
    */
    self.initTooltips = function (selector) {
        
        if (selector) {
            
            core.$(selector).tooltip({
                content: function () {
                    return $(this).attr("title");
                }
            });
            
        } else {
            
            core.$(document).tooltip({
                content: function () {
                    return $(this).attr("title");
                }
            });
            
        }
        
    };
    
    /**
    * Opens a tab on the page
    * @param tabId - id attribute of the tab DOM element
    */
    self.openTab = function (tabId) {
        var $tab = core.$("#" + tabId);
        $tab.click();
        $tab.closest(".mobile-tabs-menu__dropdown").removeClass("open");
    };
};
