var sbm = sbm || {};

/**
 * Class responsible for all the AJAX requests for the Skin Editor
 * @param core Instance of the core class
 * @param url Url to the API
 * @returns {DataserviceAdmin}
 * @constructor
 */
sbm.DataserviceAdmin = function (core, url) {
    "use strict";

    var self = this;

    // setting url
    url = url || "/admindata.php";
    self.url = url;

    // extends from base Dataservice class
    core._.extend(self, new sbm.Dataservice(core, self.url, "json", 60000));

    /**
     * initialization
     */
    self.init = function () {

        core.logger.log("Admin Dataservice init", core.logger.severity.INFO);

        core.subscribe("get-category-list", self.getCategoryList);
        core.subscribe("get-category-list-all", self.getCategoryListAll);
        core.subscribe("get-editor-users-list-all", self.getEditorUserListAll);
        core.subscribe("get-editable-content", self.getEditableContent);
        core.subscribe("save-editable-content", self.saveEditableContent);
        core.subscribe("add-new-user", self.addNewUser);
        core.subscribe("save-editable-user", self.saveEditableUser);
        core.subscribe("set-content-order", self.setOrder);
        core.subscribe("get-seo-content", self.getSEOContent);
        core.subscribe("save-seo-content", self.saveSEOContent);
        core.subscribe("get-content-images", self.getContentImages);
        core.subscribe("get-content-images-by-search", self.getContentImagesBySearch);
        core.subscribe("sync-live", self.setContentLive);
        core.subscribe("revert-all-content", self.revertAllContent);
        core.subscribe("editor-do-login", self.doLogin);
        core.subscribe("editor-do-logout", self.doLogout);
        core.subscribe("editor-delete-item", self.deleteItem);
        core.subscribe("editor-cancel-editing", self.cancelEditing);
        core.subscribe("editor-get-games", self.getGames);
        core.subscribe("editor-get-game-category-skins", self.getGameCategorySkins);
        core.subscribe("editor-get-game-info", self.getGameInfo);
        core.subscribe("editor-add-game-to-skins", self.addGameToSkins);
        core.subscribe("editor-get-categories-for-skin", self.getCategoriesForSkin);
        core.subscribe("editor-get-games-for-category", self.getGamesForCategory);
        core.subscribe("editor-update-games-order", self.updateGamesOrder);
        core.subscribe("editor-set-all-games-live", self.setAllGamesLive);
        core.subscribe("editor-set-games-live", self.setGamesLive);
        core.subscribe("editor-revert-all-games", self.revertAllGames);
        core.subscribe("editor-revert-games", self.revertGames);
        core.subscribe("editor-remove-games", self.removeGames);
        core.subscribe("editor-flush-cache", self.flushCache);
        core.subscribe("editor-check-skin-images", self.checkSkinImages);
        core.subscribe("editor-compare-live-staging", self.compareLiveAndStaging);
        core.subscribe("editor-publish-tcs", self.publishTcs);
        core.subscribe("editor-publish-pp", self.publishPP);
        core.subscribe("show-latest-versionsTP", self.getLatestVersionsTP);
        core.subscribe("show-all-versionsTP", self.getAllVersionsTP);
        core.subscribe("save-draft-tcs", self.saveTCDraft);
        core.subscribe("save-draft-pp", self.savePPDraft);
        core.subscribe("save-draft-no-version", self.saveDraftNoVersion);

    };

    /**
     * Get the list of categories
     * @param e
     * @param page
     * @param key
     * @param subKey
     * @param confirmCreation
     */
    self.getCategoryList = function (e, page, key, subKey, confirmCreation) {
        if (!page || !key) {
            core.logger.log("DataserviceAdmin/getCategoryList: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "categorylist",
            p: page,
            k: key,
            skey: subKey,
            c: confirmCreation || 0
        };

        self.ajaxRequest(p, function (data) {
            core.publish("got-category-list", data);
        });
    };

    /**
     * Get a list of all the content items for a page / key and all subkeys
     * @param e
     * @param page
     * @param key
     */
    self.getCategoryListAll = function (e, page, key) {
        if (!page || !key) {
            core.logger.log("DataserviceAdmin/getCategoryListAll: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "categorylistall",
            p: page,
            k: key
        };

        self.ajaxRequest(p, function (data) {
            core.publish("got-category-list-all", data);
        });
    };

    /**
     * Get a list of all the content items for a page / key and all subkeys
     * @param e
     * @param page
     * @param key
     */
    self.getEditorUserListAll = function (e) {
        var p = {
            f: "editoruserslistall"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("got-editor-users-list-all", data);
        });
    };

    /**
     * Get the content of a record
     * @param e
     * @param id
     * @param defaultContentCategoryId
     */
    self.getEditableContent = function (e, id, defaultContentCategoryId, device) {
        if (isNaN(id)) {
            core.logger.log("DataserviceAdmin/getEditableContent: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "content",
            id: id,
            cct: defaultContentCategoryId,
            d: device
        };

        self.ajaxRequest(p, function (data) {
            core.publish("got-editable-content", data);
        });
    };

    /**
     * Get SEO content for a page
     * @param e
     * @param page
     */
    self.getSEOContent = function (e, page) {
        if (!page) {
            core.logger.log("DataserviceAdmin/getSEOContent: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getseocontent",
            p: page
        };

        self.ajaxRequest(p, function (data) {
            core.publish("got-seo-content", data);
        });
    };

    /**
     * Save the content
     * @param e
     * @param content
     */
    self.saveEditableContent = function (e, content) {
        if (!content) {
            core.logger.log("DataserviceAdmin/saveEditableContent: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "setcontent",
            id: content.id,
            ccid: content.contentCategoryID,
            cctid: content.contentCategoryTypeID(),
            n: content.name,
            t: content.title,
            img: content.image() || "",
            imga: content.imageAlt || "",
            img1: content.image1() || "",
            imga1: content.imageAlt1 || "",
            d: content.detailUrl || "",
            i: content.intro || "",
            b: content.body || "",
            tm: content.terms || "",
            df: content.dateFrom || "",
            dt: content.dateTo || "",
            dc: content.devices,
            days: content.days,
            s: content.status,
            o: content.offerId,
            top: content.top || 0,
            t1: content.text1 || "",
            t2: content.text2 || "",
            t3: content.text3 || "",
            t4: content.text4 || "",
            t5: content.text5 || "",
            t6: content.text6 || "",
            t7: content.text7 || "",
            t8: content.text8 || "",
            t9: content.text9 || "",
            t10: content.text10 || "",
            t11: content.text11 || "",
            t12: content.text12 || "",
            t13: content.text13 || "",
            t14: content.text14 || "",
            t15: content.text15 || "",
            t16: content.text16 || "",
            t17: content.text17 || "",
            t18: content.text18 || ""
        };

        self.ajaxRequest(p, function (data) {
            core.publish("saved-editable-content", data);
        });
    };

    /**
     * Save the user
     *
     * @param e
     * @param content
     */
    self.addNewUser = function (e, user) {
        if (!user) {
            core.logger.log("DataserviceAdmin/addNewUser: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "newuser",
            user: btoa(JSON.stringify(user))
        };

        self.ajaxRequest(p, function (data) {
            core.publish("added-new-user", data);
        });
    };

    /**
     * Save the user
     *
     * @param e
     * @param content
     */
    self.saveEditableUser = function (e, user) {
        if (!user) {
            core.logger.log("DataserviceAdmin/saveEditableUser: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "updateuser",
            user: btoa(JSON.stringify(user))
        };

        self.ajaxRequest(p, function (data) {
            core.publish("saved-editable-user", data);
        });
    };

    /**
     * Save the SEO content
     * @param e
     * @param content
     */
    self.saveSEOContent = function (e, content) {

        var p = {
            f: "setseocontent",
            p: content.cacheKey,
            c: content.seoContent
        };

        self.ajaxRequest(p, function (data) {
            core.publish("saved-seo-content", data);
        });

    };

    /**
     * Set the order on the grid of the categories
     * @param e
     * @param contentCategoryId
     * @param order
     */
    self.setOrder = function (e, contentCategoryId, order) {
        if (!contentCategoryId || !order) {
            core.logger.log("DataserviceAdmin/setOrder: parameters missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "setcontentorder",
            ccid: contentCategoryId,
            o: JSON.stringify(order)
        };

        self.ajaxRequest(p, function (data) {
            core.publish("finished-set-content-order", data);
        });
    };

    /**
     * Get images for the content
     * @param e
     * @param contentCategoryId
     * @param subfolders
     */
    self.getContentImages = function (e, contentCategoryId, subfolders) {
        self.__getContentImages(e, contentCategoryId, subfolders, null, "getcontentimages");
    };

    /**
     * Get images for the content
     * @param e
     * @param contentCategoryId
     * @param subfolders
     */
    self.getContentImagesBySearch = function (e, contentCategoryId, subfolders, search) {
        self.__getContentImages(e, contentCategoryId, subfolders, search, "getcontentimagesbysearch");
    };

    self.__getContentImages = function (e, contentCategoryId, subfolders, search, func) {
        if (!contentCategoryId) {
            core.logger.log("DataserviceAdmin/revertAllContent: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p  = {
            f: func,
            s: search || null,
            cctid: contentCategoryId,
            d: subfolders || null
        };

        self.ajaxRequest(p, function (data) {
            core.publish("got-content-images", data);
        });
    };

    /**
     * Deploy content to LIVE server
     * @param e
     * @param categoryId
     */
    self.setContentLive = function (e, categoryId, subKey, contentIds) {
        if (!categoryId) {
            core.logger.log("DataserviceAdmin/setContentLive: parameter missing", core.logger.severity.ERROR);
            return;
        }
        if (!contentIds) {
            contentIds = '';
        }

        var p  = {
            f: "setcontentlive",
            ccid: categoryId,
            skey: subKey,
            cids: contentIds
        };

        self.ajaxRequest(p, function (data) {
            core.publish("synced-live", data);
        });
    };

    /**
     * Revert content from LIVE server
     * @param e
     * @param contentCategoryId
     */
    self.revertAllContent = function (e, contentCategoryId) {
        if (!contentCategoryId) {
            core.logger.log("DataserviceAdmin/revertAllContent: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "revertallcontent",
            ccid: contentCategoryId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("reverted-all-content", data);
        });
    };

    /**
     * Login
     * @param e
     * @param username
     * @param password
     */
    self.doLogin = function (e, username, password) {
        if (!username || !password) {
            core.logger.log("DataserviceAdmin/doLogin: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "login",
            u: username,
            p: password
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-done-login", data);
        });
    };

    /**
     * Login
     * @param e
     * @param username
     * @param password
     */
    self.doLogout = function (e) {
        var p = {
            f: "logout"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-done-logout", data);
        });
    };

    /**
     * Delete record
     * @param e
     * @param id
     */
    self.deleteItem = function (e, id) {
        if (!id) {
            core.logger.log("DataserviceAdmin/deleteItem: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "deletecontent",
            id: id
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-deleted-item", data);
        });
    };

    /**
     * Cancel editing of a record
     * @param e
     * @param id
     */
    self.cancelEditing = function (e, id) {
        if (!id) {
            core.logger.log("DataserviceAdmin/cancelEditing: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "canceledit",
            id: id
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-canceled-editing", data);
        });
    };

    /**
     * Get games based on the search
     * @param e
     * @param id
     * @param title
     */
    self.getGames = function (e, id, title) {
        if (!id && !title) {
            core.logger.log("DataserviceAdmin/searchGames: parameters missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "searchgames",
            id: id,
            name: title
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-got-games", data);
        });
    };

    /**
     * Get game categories
     * @param e
     * @param id
     */
    self.getGameCategorySkins = function (e, id) {
        if (!id) {
            core.logger.log("DataserviceAdmin/getGameCategorySkins: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getgamecategoryskins",
            id: id
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-got-game-category-skins", data);
        });
    };

    /**
     * Retrieve games information from server
     * @param e
     * @param gameId
     */
    self.getGameInfo = function (e, gameId) {
        if (!gameId) {
            core.logger.log("DataserviceAdmin/getGameInfo: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getgameinfo",
            id: gameId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-got-game-info", data);
        });
    };

    /**
     * Add games to skin
     * @param e
     * @param gameId
     * @param skinCategories
     */
    self.addGameToSkins = function (e, gameId, skinCategories) {
        if (!gameId || !skinCategories) {
            core.logger.log("DataserviceAdmin/addGameToSkins: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "addgames",
            id: gameId,
            json: JSON.stringify(skinCategories)
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-added-game-to-skins", data);
        });
    };

    /**
     * Get all categories for a skin
     * @param e
     * @param skinId
     */
    self.getCategoriesForSkin = function (e, skinId) {
        if (!skinId) {
            core.logger.log("DataserviceAdmin/getCategoriesForSkin: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "getgamecategorylist",
            sid: skinId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-got-categories-for-skin", data);
        });
    };

    /**
     * Get games for a category
     * @param e
     * @param categoryId
     * @param device
     * @param skinId
     */
    self.getGamesForCategory = function (e, categoryId, device, skinId) {
        if (!categoryId || !device || !skinId) {
            core.logger.log("DataserviceAdmin/getGamesForCategory: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "getgamesincategory",
            id: categoryId,
            did: device,
            sid: skinId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-got-games-for-category", data);
        });
    };

    /**
     * Update game order
     * @param e
     * @param categoryId
     * @param device
     * @param skinId
     * @param order
     */
    self.updateGamesOrder = function (e, categoryId, device, skinId, order) {
        if (!categoryId || !device || !order) {
            core.logger.log("DataserviceAdmin/updateGamesOrder: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "setgameorder",
            id: categoryId,
            did: device,
            sid: skinId,
            o: JSON.stringify(order)
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-updated-games-order", data);
        });
    };

    /**
     * Set all games to LIVE
     * @param e
     * @param skinId
     */
    self.setAllGamesLive = function (e, skinId) {
        if (!skinId) {
            core.logger.log("DataserviceAdmin/setAllGamesLive: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "setgameslive",
            gc: 0,
            dc: 0,
            sid: skinId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-set-all-games-live-finished", data);
        });
    };

    /**
     * Set games to LIVE
     * @param e
     * @param gameCategoryId
     * @param deviceCategoryId
     * @param skinId
     */
    self.setGamesLive = function (e, gameCategoryId, deviceCategoryId, skinId) {
        if (!skinId) {
            core.logger.log("DataserviceAdmin/setGamesLive: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "setgameslive",
            gc: gameCategoryId,
            dc: deviceCategoryId,
            sid: skinId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-set-games-live-finished", data);
        });
    };

    /**
     * Revert all games from LIVE
     * @param e
     * @param skinId
     */
    self.revertAllGames = function (e, skinId) {
        if (!skinId) {
            core.logger.log("DataserviceAdmin/revertAllGames: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "revertgames",
            gc: 0,
            dc: 0,
            sid: skinId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-reverted-all-games", data);
        });
    };

    /**
     * Revert games from LIVE
     * @param e
     * @param gameCategoryId
     * @param deviceCategoryId
     * @param skinId
     */
    self.revertGames = function (e, gameCategoryId, deviceCategoryId, skinId) {
        if (!skinId) {
            core.logger.log("DataserviceAdmin/revertGames: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "revertgames",
            gc: gameCategoryId,
            dc: deviceCategoryId,
            sid: skinId
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-reverted-games", data);
        });
    };

    /**
     * Remove games
     * @param e
     * @param gameCategoryId
     * @param deviceCategoryId
     * @param skinId
     * @param games
     */
    self.removeGames = function (e, gameCategoryId, deviceCategoryId, skinId, games) {
        if (!skinId) {
            core.logger.log("DataserviceAdmin/removeGames: parameter missing", core.logger.severity.ERROR);
            return;
        }

        var p = {
            f: "removegames",
            gc: gameCategoryId,
            dc: deviceCategoryId,
            sid: skinId,
            json: JSON.stringify(games)
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-removed-games", data);
        });
    };

    /**
     * Flush website cache
     */
    self.flushCache = function () {
        var p = {
            f: "flush"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-flushed-cache", data);
        });
    };

    /**
     * Get all the edit points in the skin, for a given device, and all the images they reference
     */
    self.checkSkinImages = function (e, environment, device) {
        var p = {
            f: "checkskinimages",
            l: environment,
            d: device
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-checked-skin-images", data);
        });
    };

    /**
     * Compare Staging environment against Live
     */
    self.compareLiveAndStaging = function () {
        var p = {
            f: "comparestagingtolive"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("editor-compared-live-staging", data);
        });
    };


    /**
     * Get latest versions of PP and TCs
     */
    self.getLatestVersionsTP = function () {

        var p = {
            f: "showLatestVersionsTP"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("showed-latest-versionsTP", data);
        });
    };

    /**
     * Get all status of PP and TCs
     */
    self.getAllVersionsTP = function () {

        var p = {
            f: "showAllVersionsTP"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("showed-all-versionsTP", data);
        });
    };
    

    /**
     * Publish Terms and Conditions
     */
    self.publishTcs = function (e, content) {

        // save as draft
        var save = function (content) {

            var p = {
                f: "savetcs",
                c: content
            };

            self.ajaxRequest(p, function (data) {
                
                // publish
                if (data.Code === 0) {
                    publish(data.Body.id);
                }
            });
        };

        // publish the terms
        var publish = function (id) {

            var p = {
                f: "publishtcs",
                i: id
            };

            self.ajaxRequest(p, function (data) {
                core.publish("published-tcs", data);
            });

        };


        // publish contents
        save(content);
    };


    /**
     * Publish Privacy Policy
     */
    self.publishPP = function (e, content) {

        // save as draft
        var save = function (content) {

            var p = {
                f: "savepp",
                c: content
            };

            self.ajaxRequest(p, function (data) {
                
                // publish
                if (data.Code === 0) {
                    publish(data.Body.id);
                }
            });
        };

        // publish the terms
        var publish = function (id) {

            var p = {
                f: "publishpp",
                i: id
            };

            self.ajaxRequest(p, function (data) {
                core.publish("published-pp", data);
            });

        };
            // publish contents
        save(content);
    };

    /**
     * Save Draft of TCs with a version
     */
    self.saveTCDraft = function (e, contentTC) {

       
        var p = {
            f: "savetcs",
            c: contentTC
       };

    
        self.ajaxRequest(p, function (data) {
            core.publish("saved-tc-draft", data);
        });

    };

     /**
     * Save Draft of PP with a version
     */
    self.savePPDraft = function (e, contentPP) {

       
        var p = {
            f: "savepp",
            c: contentPP
       };

    
        self.ajaxRequest(p, function (data) {
            core.publish("saved-pp-draft", data);
        });

    };

    /**
     * Save TCs and PP with no Version
     */
    self.saveDraftNoVersion = function (e, id, content) {
       
            var p = {
                f: "updateTPNoVersion",
                i: id,
                c: content

            };

            self.ajaxRequest(p, function (data) {
                core.publish("saved-draft-no-version", data);
            });

    };



    return self;
};