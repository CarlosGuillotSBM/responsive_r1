var sbm = sbm || {};

/**
 * Class responsible for all the AJAX requests to Coin API
 * @param core Instance of the core class
 * @param url Url to the API
 * @returns {DataserviceCashier}
 * @constructor
 */
sbm.DataserviceCashier = function(core, url) {
  "use strict";

  var self = this;
  self.commonParams = {};

  core._.extend(self, new sbm.Dataservice(core, url, "jsonp"));

  /**
   * Initialization
   */
  self.init = function() {
    core.logger.log("Cashier Dataservice init", core.logger.severity.INFO);

    core.subscribe("cashier-start", self.start);
    core.subscribe("cashier-get-account-types", self.getAccountTypes);
    core.subscribe("cashier-deposit", self.deposit);
    core.subscribe("cashier-deposit-retry-CVN", self.depositRetryCVN);
    core.subscribe("cashier-get-customer-accounts", self.customerAccounts);
    core.subscribe(
      "quick-cashier-get-customer-accounts",
      self.quickCashierCustomerAccounts
    );
    core.subscribe("cashier-get-token-update", self.getNewTokenUpdate);
    core.subscribe("cashier-get-withdraw-detail", self.getWithdrawDetail);
    core.subscribe("cashier-do-withdrawal", self.withdraw);
    core.subscribe("cashier-get-reverse-detail", self.getReverseDetail);
    core.subscribe("cashier-do-reversal", self.reverse);
    core.subscribe("cashier-get-wire-details", self.getWireDetails);
    core.subscribe("cashier-save-bank-details", self.setWireDetails);
    core.subscribe("cashier-get-new-token", self.getNewToken);
    core.subscribe("cashier-set-new-token-cvn", self.newTokenCVN);
    core.subscribe("quick-cashier-start", self.startQuick);
    core.subscribe(
      "quick-deposit-get-customer-accounts",
      self.quickDepositGetCustomerAccounts
    );
    core.subscribe("quick-deposit-deposit", self.quickDeposit);
    core.subscribe("get-limit-values", self.getLimitValues);
    core.subscribe(
      "request-deposit-limit-change",
      self.requestDepositLimitChange
    );
    core.subscribe("cancel-deposit-limits", self.cancelDepositLimits);
    core.subscribe("set-deposit-limits", self.setDepositLimits);
    core.subscribe("reset-deposit-limits", self.resetLimits);
    core.subscribe("age-id-verification-run-uru-check", self.runUruCheck);
    core.subscribe(
      "age-id-verification-check-finished",
      self.onAgeIdVerificationCheckFinished
    );
    core.subscribe(
      "player-verification-cas-finished",
      self.onPlayerVerificationCasFinished
    );
  };

  /**
   * Start a new cashier session
   * @param e
   * @param cashierData
   */
  self.start = function(e, cashierData) {
    self.startCashier(cashierData, "cashier-started");
  };

  /**
   * Start a quick cashier session
   * @param e
   * @param cashierData
   */
  self.startQuick = function(e, cashierData) {
    self.startCashier(cashierData, "quick-cashier-started");
  };

  /**
   * Starts cashier session
   * @param cashierData
   * @param callbackEvent The event to be emitted when finished
   */
  self.startCashier = function(cashierData, callbackEvent) {
    if (!cashierData) {
      return;
    }

    self.commonParams = {
      Source: cashierData.source(),
      SkinID: cashierData.skinId,
      IP: cashierData.ip,
      PlayerID: cashierData.playerId,
      ssid: cashierData.ssid,
      Device: cashierData.device
    };

    var p = {
      Function: "Start"
    };

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      if (data.Status === "1") {
        self.commonParams.CustomerID = data.CustomerID;
      }
      core.publish(callbackEvent, data);
    });
  };

  /**
   * Get list of available account types
   */
  self.getAccountTypes = function() {
    var p = {
      Function: "GetAccountTypes"
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-got-account-types", data);
    });
  };

  /**
   * makes a deposit
   * @param e
   * @param addingNewMethod
   * @param deposit
   */
  self.deposit = function(e, addingNewMethod, deposit) {
    var promoId = deposit.promoId();
    if (deposit.promoCode()) {
      promoId = -1;
    }
    if (deposit.iDoNotWantBonus()) {
      promoId = 0;
    }

    var cvnOption = "";
    if (deposit.needsCvn()) {
      cvnOption = deposit.cvnOption() ? "1" : "0";
    }

    var p = {
      Function: "Deposit",
      AccountID: deposit.accountId || 0,
      AccountType: deposit.accountType(),
      Amount: deposit.amount(),
      PromoID: promoId,
      PromoCode: deposit.promoCode(),
      AffCredit: deposit.affCredit,
      TxDepositID: deposit.txDepositId || "",
      dsid: deposit.dsid || "",
      UKGCAck: deposit.acknowledged() ? "1" : "",
      ExpiryDate: addingNewMethod.card.expiryYear()
        ? addingNewMethod.card.expiryYear() + addingNewMethod.card.expiryMonth()
        : "",
      CVN: addingNewMethod.card.cvn() || deposit.cvn(),
      CVNOption: cvnOption,
      AccountNumber: addingNewMethod.neteller.number(),
      AccountData:
        deposit.accountId && deposit.accountType() === "A_NETELLER"
          ? deposit.netellerSecureId()
          : addingNewMethod.neteller.secureId(),
      Firstname:
        addingNewMethod.card.firstName() || addingNewMethod.paysafe.firstName(),
      Lastname:
        addingNewMethod.card.lastName() || addingNewMethod.paysafe.lastName(),
      Email: addingNewMethod.paysafe.email(),
      DOB: addingNewMethod.paysafe.dob()
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(
      p,
      function(data) {
        core.publish("cashier-deposited", data);
      },
      true
    );
  };
  /**
   * makes a quick deposit
   * @param e
   * @param deposit
   */
  self.quickDeposit = function(e, deposit, cashierData) {
    var p = {
      Function: "Deposit",
      AccountID: deposit.accountId,
      Amount: deposit.amount,
      PromoID: deposit.promoId,
      CVN: deposit.cvn,
      CVNOption: deposit.cvnOption,
      CustomerID: deposit.customerId,
      AccountType: deposit.accountType,
      AccountData: deposit.secureId || ""
    };

    var commonParams = {
      Source: cashierData.source(),
      SkinID: cashierData.skinId,
      IP: cashierData.ip,
      PlayerID: cashierData.playerId,
      ssid: cashierData.ssid,
      Device: cashierData.device
    };

    core._.extend(p, commonParams);

    self.ajaxRequest(
      p,
      function(data) {
        core.publish("quick-deposit-deposited", data);
      },
      true
    );
  };
  /**
   * Retry CVN
   * @param e
   * @param cvnData The deposit data
   */
  self.depositRetryCVN = function(e, cvnData) {
    var p = {
      Function: "DepositRetryCVN",
      CVN: cvnData.cvn(),
      TxDepositID: cvnData.txDepositId || "",
      dsid: cvnData.dsid || ""
    };

    // if failed on adding new card self.commonParams are empty
    if (_.isEmpty(self.commonParams)) {
      // get the common params
      self.commonParams = {
        Source: cvnData.source,
        SkinID: cvnData.skinId,
        IP: cvnData.ip,
        PlayerID: cvnData.playerId,
        ssid: cvnData.ssid,
        Device: cvnData.device,
        CustomerID: cvnData.customerId
      };
    }

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-deposit-retried-CVN", data);
    });
  };
  /**
   * Get list of customer accounts
   */
  self.customerAccounts = function() {
    self.getCustomerAccounts("cashier-got-customer-accounts");
  };
  /**
   * Get list of customer accounts
   */
  self.quickCashierCustomerAccounts = function() {
    self.getCustomerAccounts("quick-cashier-got-customer-accounts");
  };
  /**
   * Get customer accounts
   * @param callbackEvent The event to be emitted
   */
  self.getCustomerAccounts = function(callbackEvent) {
    var p = {
      Function: "GetCustomerAccounts"
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish(callbackEvent, data);
    });
  };
  /**
   * Get Withdraw Detail
   * Gets a customers withdrawal details before a withdrawal request can be made.
   * Only display the withdrawal amount prompt if this function returns a success Status (1)
   * @source cashier source
   * @session cashier global session
   * @cashierData cashier global data
   * @playerData player global data
   * @callback function to call after finish
   */
  self.getWithdrawDetail = function() {
    var p = {
      Function: "GetWithdrawDetail"
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-got-withdraw-detail", data);
    });
  };
  /**
   * Withdraw
   * Submit a players requested withdrawal amount.
   * @param e
   * @param amount
   */
  self.withdraw = function(e, amount) {
    var p = {
      Function: "Withdraw",
      Amount: amount
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-done-withdrawal", data);
    });
  };
  /**
   * Get Reverse Detail
   * Gets a customers pending withdrawal details that may be reversed before a reverse request can be made.
   * Only display the reversal amount prompt if this function returns a success Status (1)
   */
  self.getReverseDetail = function() {
    var p = {
      Function: "GetReverseDetail"
    };
    core._.extend(p, self.commonParams);
    self.ajaxRequest(p, function(data) {
      core.publish("cashier-got-reverse-detail", data);
    });
  };
  /**
   * Reverse
   * Submit a players requested reversal amount.
   * @param e
   * @param amount
   */
  self.reverse = function(e, amount) {
    var p = {
      Function: "Reverse",
      Amount: amount
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-done-reverse", data);
    });
  };
  /**
   * Get New Token
   */
  self.getNewToken = function() {
    var p = {
      Function: "NewTokenAdd"
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-got-new-token", data);
    });
  };
  /**
   * Get wire details
   */
  self.getWireDetails = function() {
    var p = {
      Function: "GetWireDetails"
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-got-wire-details", data);
    });
  };
  /**
   * Set wire details
   * @param e
   * @param bankData
   */
  self.setWireDetails = function(e, bankData) {
    var p = {
      Function: "SetWireDetails",
      BankName: bankData.bankName,
      AccountName: bankData.accountName,
      AccountNumber: bankData.accountNumber,
      BranchCode: bankData.branchCode,
      SwiftCode: bankData.swiftCode,
      Address: bankData.address,
      City: bankData.city,
      State: bankData.state,
      Postcode: bankData.postcode,
      CorrespondingBankName: bankData.correspondingBankName,
      Status: bankData.wireStatus ? "1" : "0"
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("cashier-saved-bank-details", data);
    });
  };
  /**
   * Send CVN for the new ACC card
   * @param e
   * @param card
   * @param deposit
   */
  self.newTokenCVN = function(e, card, deposit, aed) {
    var p = {
      Function: "NewTokenCVN",
      CVN: card.cvn(),
      CVNOption: card.cvnOption() ? 1 : 0,
      Amount: deposit.amount(),
      PromoID: deposit.promoCode() ? "-1" : deposit.promoId(),
      PromoCode: deposit.promoCode() || "",
      SubscriptionID: deposit.subscriptionId || 0,
      TxAccountTokenID: card.token.txAccountTokenId,
      First6: card.first6 || "",
      aed: aed || ""
    };
    core._.extend(p, self.commonParams);

    self.ajaxRequest(
      p,
      function(data) {
        core.publish("cashier-finished-set-new-token-cvn", data);
      },
      true
    );
  };
  /**
   * Get a new token for a card about to expire
   * @param e
   * @param accountId
   */
  self.getNewTokenUpdate = function(e, accountId) {
    var p = {
      Function: "NewTokenUpdate",
      AccountID: accountId
    };
    core._.extend(p, self.commonParams);
    self.ajaxRequest(p, function(data) {
      core.publish("cashier-got-token-update", data);
    });
  };

  /**
   * Get customer accounts for the Quick Deposit
   * Quick Deposit doesn't call the start function so the commonParams don't work here
   * @param e
   * @param sessionId
   * @param playerId
   * @param device
   */
  self.quickDepositGetCustomerAccounts = function(
    e,
    sessionId,
    playerId,
    device
  ) {
    var p = {
      Function: "GetCustomerAccountsQikDeposit",
      Source: "Qik",
      SkinID: core.serverconfig.skinId,
      IP: core.serverconfig.ip,
      PlayerID: playerId,
      ssid: sessionId,
      Device: device
    };
    self.ajaxRequest(p, function(data) {
      core.publish("quick-deposit-got-customer-accounts", data);
    });
  };

  /**
   * Get player limits
   */
  self.getLimitValues = function(e, action) {
    var p = {
      Function: "GetDepositLimits"
    };

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      if (action === "change-request") data.action = "change-request";
      core.publish("got-limit-values", data);
    });
  };

  /**
   * Request deposit limits change
   * @param e
   * @param limits json object with the limits per day, week and month
   */
  self.requestDepositLimitChange = function(e, limits) {
    var p = {
      Function: "RequestDepositLimitChange",
      DepositLimitPerDay: limits.daily,
      DepositLimitPerWeek: limits.weekly,
      DepositLimitPerMonth: limits.monthly,
      Password: limits.password
    };

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("request-deposit-limit-change-finished", data);
    });
  };

  /**
   * Cancel a pending limits change request
   */
  self.cancelDepositLimits = function() {
    var p = {
      Function: "CancelDepositLimits"
    };

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("canceled-deposit-limits", data);
    });
  };

  self.setDepositLimits = function() {
    var p = {
      Function: "SetDepositLimits"
    };

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("set-deposit-limits-finished", data);
    });
  };

  self.resetLimits = function() {
    var p = {
      Function: "RequestDepositLimitChange",
      DepositLimitPerDay: -1,
      DepositLimitPerWeek: -1,
      DepositLimitPerMonth: -1
    };

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("reset-deposit-limits-finished", data);
    });
  };

  // URU Verification Check

  self.runUruCheck = function() {
    core._.extend(p, self.commonParams);

    var p = {
      Function: "CheckAndVerifyCustomer"
    };

    core._.extend(p, self.commonParams);

    self.ajaxRequest(p, function(data) {
      core.publish("age-id-verification-check-finished", [data]);
    });
  };

  self.onPlayerVerificationCasFinished = function(e, data) {
    var dataVer = JSON.parse(data.Body);
    if (dataVer != null) {
      core.storage.setCookie("URUVerified", 2);
      var URUVerified = core.storage.getCookie("URUVerified");
      core.publish("age-id-verification-check-if-verified", [
        URUVerified,
        {},
        false
      ]);
    } else {
      core.storage.setCookie("URUVerified", 3);
    }
  };

  /* Run after verification */
  self.onAgeIdVerificationCheckFinished = function(e, data) {
    var Msg;
    var Title;
    var Status;
    if (data.CountryCode != "GBR") {
      core.publish("player-verification-cas", [
        core.storage.getCookie("PlayerID"),
        core.storage.getCookie("SessionID")
      ]);
    } else {
      if (data.VerificationStatus == "1") {
        Msg =
          "<p>Your identity has been successfully verified.</p><p>Enjoy your gameplay!</p>";
        Title = "Check successful";
        Status = "success";
        core.storage.setCookie("URUVerified", 1);
        core.publish("show-notification-result", [Status, Title, Msg]);
      } else {
        core.storage.setCookie("URUVerified", 2);
        core.publish("player-verification-cas", [
          core.storage.getCookie("PlayerID"),
          core.storage.getCookie("SessionID")
        ]);
      }
    }
  };

  return self;
};
