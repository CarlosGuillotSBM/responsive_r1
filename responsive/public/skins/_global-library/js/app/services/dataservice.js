var sbm = sbm || {};

/**
 * Base class for performing AJAX requests
 * @param core Instance of the core class
 * @param url The url for the request
 * @param dataType The data type (json/jsonp)
 * @returns {Dataservice}
 * @constructor
 */
sbm.Dataservice = function (core, url, dataType, timeout) {
    "use strict";

    var self = this;
    url = url || "/data.php";

    /**
     * Performs the AJAX request
     * Handles success and error callback
     * @param data Data to send to the server
     * @param callback Callback function to call after success
     * @param doNotStopAnimation Boolean - if true does not stop the loading animation; if false/undefined stops animation
     * @param async Boolean - if true the request will be made asynchronously else will be synchronously
     */
    self.ajaxRequest = function (data, callback, doNotStopAnimation, async) {

        var asyncOption = typeof async === "undefined";

        core.$.ajax(
            {
                "url": url,
                "dataType": dataType || "json",
                "timeout" : timeout || 30000,
                "data": data,
                type: "POST",
                async : asyncOption
            }
        )
            .done(function (data) {
                if (data.Code === -1) {
                    if (url === "/admindata.php") {
                        core.publish("admin-session-has-expired");
                    } else {
                        core.publish("session-has-expired");
                    }
                } else {
                    callback(data);
                }
                !doNotStopAnimation && core.publish("hide-notification-loading");
            })
            .fail(function (jqXHR, textStatus) {
                core.publish("show-notification-error", ["Sorry there is a system error, if the error persists contact support", "alert"]);
                core.publish("hide-notification-loading");
                core.logger.log("Dataservice Error: " + textStatus, core.logger.severity.ERROR);
            });

    };


    /**
     * Performs the REST request
     * Handles success and error callback
     * @param url the request URL
     * @param httpMethod (GET/POST etc)
     * @param data Data to send to the server
     * @param callback Callback function to call after success
     */
    self.restRequest = function (url, httpMethod, data, callback) {

        core.$.ajax(
            {
                "url": url,
                "dataType": "json",
                "timeout" : 10000,
                "data": data,
                "type": httpMethod
            }
        )
            .done(function (data) {
                callback(data);
            })
            .fail(function (jqXHR, textStatus) {
                core.logger.log("Dataservice Error: " + textStatus, core.logger.severity.ERROR);
            });

    };

    return self;

};