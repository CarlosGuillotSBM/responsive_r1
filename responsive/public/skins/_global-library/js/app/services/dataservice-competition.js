var sbm = sbm || {};

/**
 * Class responsible for all the AJAX requests to Competition API
 * @param core Instance of the core class
 * @param url Url to the API
 * @returns {DataserviceCompetition}
 * @constructor
 */
sbm.DataserviceCompetition = function (core, url) {
    "use strict";

    var self = this;
    self.commonParams = {};

    core._.extend(self, new sbm.Dataservice(core, url, "jsonp"));

    /**
     * Initialization
     */
    self.init = function () {
        core.logger.log("Competition Dataservice init", core.logger.severity.INFO);

        core.subscribe("rest-opt-in", self.optIn);

    };

    /**
     * Get the list of categories
     * @param e
     * @param playerId
     * @param competitionId
     */
    self.optIn = function (e, playerId, competitionId) {
        if (!playerId || !competitionId) {
            core.logger.log("DataserviceCompetition/optIn: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            path: 'competitions/optin',
            playerId: playerId,
            competitionId: competitionId
        };
        self.ajaxRequest(p, function (data) {
            core.publish("got-rest-opt-in", data);
        }, false, true,  function (data) {
            core.publish("error-on-got-rest-opt-in", data);
        });
    };

    return self;

};