var sbm = sbm || {};

/**
* Class responsible for all the AJAX requests to website backend
* @param core Instance of the core class
* @param url Url to the Data Requests (If needed)
* @returns {DataserviceSockets}
* @constructor
*/
sbm.DataserviceSockets = function (core, url) {
    "use strict";
    
    var self = this;
    this.wsUri = core.serverconfig.socketURL;
    this.playerID = core.storage.getCookie("PlayerID");
    this.sessionID = core.storage.getCookie("SessionID");
    
    core._.extend(self, new sbm.Dataservice(core, url));
    
    /**
    * Initialization
    */
    self.init = function () {
        
        core.logger.log("Web Dataservice for Sockets init", core.logger.severity.INFO);
        /* Start listening after logging in */
        core.subscribe("done-login", self.onDoneLogin);
        core.subscribe("got-cma-notification", self.onGotCMANotification);
        self.initSocket();
    };
    
    /**
    * Login
    * @param e
    * @param username
    * @param password
    * @param async
    */
    self.onDoneLogin = function(e){
        // Connect to Web Sockets
        self.initSocket();
    };
    
    self.initSocket = function(e){
        if(core.serverconfig.socketsEnabled == 1  && core.storage.getCookie("SessionID")){
            this.playerID = core.storage.getCookie("PlayerID");
            this.sessionID = core.storage.getCookie("SessionID");
            // Let us open a web socket
            this.socketURL = this.wsUri + "?playerId=" + this.playerID + "&sessionId=" + this.sessionID;
           // this.socketURL = "ws://localhost:8080";
            var ws = new WebSocket(this.socketURL);
            ws.onopen = self.onOpen;
            ws.onmessage = self.onGotEventFromSocket;
            
            ws.onclose = self.onClose;
        }
    };
    /* After opening the websockets, what should we do? */
    self.onOpen = function(evt)
    {
        core.logger.log("Websocket connection opened", core.logger.severity.INFO);
    }
    
    /* Run this after websockets are disconnected */
    self.onClose = function(evt)
    {
        core.logger.log("Websocket connection closed", core.logger.severity.INFO);
    }
    
    /* Send information to the websockets */
    self.doSend = function(message)
    {
        core.logger.log("Websocket connection sending: ", core.logger.severity.INFO);
        self.websocket.send(message);
    }
    
    /* Debugger in case of an error in connection */
    self.onError = function(evt)
    {
        writeToScreen("DEBU ERROR: " + evt.data);
    }
    
    
    
    self.onGotEventFromSocket = function (event) {
        core.logger.log("Websocket connection received: ", core.logger.severity.INFO);
        console.log("DEBU ",event.data);
        core.publish("got-cma-notification", [event.data]);
    };
    
    return self;
    
};