var sbm = sbm || {};

/**
 * Class responsible for all the AJAX requests to Core Services Player Management API
 * @param core Instance of the core class
 * @param url Url to the API
 * @returns {DataserviceCompetition}
 * @constructor
 *
 * API documentation: http://192.168.205.180:9000/swagger-ui.html
 */
sbm.DataservicePlayerManagement = function (core, url) {
    "use strict";

    var self = this;
    self.apiURL = url || "/data.php";;
    core._.extend(self, new sbm.Dataservice(core, self.apiURL));

    /**
     * Initialization
     */
    self.init = function () {
        core.logger.log("Player Management Dataservice init", core.logger.severity.INFO);
        core.subscribe("player-verification", self.playerVerification);
        core.subscribe("player-verification-cas", self.playerVerificationCas);
        core.subscribe("upload-documents", self.uploadDocuments);
        core.subscribe("get-documents", self.getDocuments);
        core.subscribe("responsible-gaming-action-continue", self.socialResponsiblePopupContinue);
        core.subscribe("responsible-gaming-action-set-limit", self.socialResponsiblePopupSetLimit);
        core.subscribe("responsible-gaming-action-live-chat", self.socialResponsiblePopupLiveChat);
    };

    /**
     * Check if player needs verification
     * @param e
     * @param playerId
     * @param sessionId
     */
    self.playerVerification = function (e, playerId, sessionId) {
        if (!playerId || !sessionId) {
            core.logger.log("DataservicePlayerManagement/playerVerification: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getPlayerAccountVerification"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("player-verification-finished", data);
        });

    };
    self.playerVerificationCas = function (e, playerId, sessionId) {
        if (!playerId || !sessionId) {
            core.logger.log("DataservicePlayerManagement/playerVerification: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getPlayerAccountVerification"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("player-verification-cas-finished", data);
        });

    };

    /**
     * Gets the list of all documents
     * @param e
     * @param playerId
     * @param sessionId
     */
    self.getDocuments = function () {
        var p = {
            f: "getDocuments"
        };

        self.ajaxRequest(p, function (data) {
            core.publish("got-documents", data);
        });

    };

    /**
     * Uploads the documents
     */
    self.uploadDocuments = function (e, files) {

        var form = new FormData();

        for (var i = 0, file; file = files[i]; ++i) {

            form.append("document-" + i, file);
        }

        form.append("f", "uploadFilesPlayerAccountVerification");

        var settings = {
          "async": true,
          "url": "/data.php",
          "method": "POST",
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (data) {
            core.publish("hide-notification-loading");
            core.publish("upload-documents-finished", JSON.parse(data));
        });

    };

   
    /**
     * Responsible Gaming user check continue option
     */

    self.socialResponsiblePopupContinue = function (e, action) {
        if (!action) {
            core.logger.log("DataservicePlayerManagement/socialResponsiblePopupContinue: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getSocialResponsiblePopupAction",
            a: action
        };

        self.ajaxRequest(p, function (data) {
            core.publish("responsible-gaming-action-continue-finished", data);
        });

    };

    /**
     * Responsible Gaming user check set limit option
     */

    self.socialResponsiblePopupSetLimit = function (e, action) {
        if (!action) {
            core.logger.log("DataservicePlayerManagement/socialResponsiblePopupSetLimit: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getSocialResponsiblePopupAction",
            a: action
        };

        self.ajaxRequest(p, function (data) {
            core.publish("responsible-gaming-action-set-limit-finished", data);
        });

    }; 

    /**
     * Responsible Gaming user check set limit option
     */

    self.socialResponsiblePopupLiveChat = function (e, action) {
        if (!action) {
            core.logger.log("DataservicePlayerManagement/socialResponsiblePopupLiveChat: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            f: "getSocialResponsiblePopupAction",
            a: action
        };

        self.ajaxRequest(p, function (data) {
            core.publish("responsible-gaming-action-live-chat-finished", data);
        });

    };  

    return self;

};