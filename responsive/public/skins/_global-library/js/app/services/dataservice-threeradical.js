var sbm = sbm || {};

/**
 * Class responsible for all the AJAX requests to ThreeRadical API
 * @param core Instance of the core class
 * @param url Url to the API
 * @returns {DataserviceThreeRadical}
 * @constructor
 */
sbm.DataserviceThreeRadical = function (core, url) {
    "use strict";

    var self = this;
    self.commonParams = {};

    core._.extend(self, new sbm.Dataservice(core, url, "jsonp"));

    /**
     * Initialization
     */
    self.init = function () {
        /* Log that the service is up and running */
        core.logger.log("ThreeRadical Dataservice init", core.logger.severity.INFO);
        /* When notified, generate a new token */
        core.subscribe("threeRadical-request-token", self.generateToken);
    };

    /**
     * Get a new HMAC token
     * @param e
     * @param playerId
     * @param sessionId 
     */
    self.generateToken = function (e) {
        var p = {
            f: "getThreeRadicalHMACToken",
        };
        
        self.ajaxRequest(p, function (data) {
            /* We should notify the system that we have a new token */
            core.publish("threeRadical-generated-token", data);
        });
    };


    return self;

};