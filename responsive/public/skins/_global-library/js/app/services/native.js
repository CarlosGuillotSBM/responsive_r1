var sbm = sbm || {}; 

/**
 * Service to handle the website when is using the wrapper gonative to create the hybrid app
 * https://gonative.io
 */
sbm.native = function () {

    var self = this;
    
    /**
     * url to point to the website
     */
    self.url = sbm.serverconfig.debug ? "https://responsivemagical.dagacubedev.net" : "https://www.magicalvegas.com";

    /**
     * the sbm core
     */
    self.core = null;

    /**
     * native menu links
     * logged out and logged in versions
     */
    self.menu = {
        loggedOut: [
            {
                "label": "Home",
                "url": self.url,
                "icon": "fa-home",
                "subLinks": []
            },
            {
                "label": "Login",
                "url": self.url + "/login/",
                "icon": "fa-play",
                "subLinks": []
            },
            {
                "label": "New Games",
                "url": self.url + "/slots/new/",
                "icon": "fa-fire",
                "subLinks": []
            },
            {
                "label": "Slots by Feature",
                "isGrouping": true,
                "icon": "fa-star",
                "subLinks": [
                {
                    "label": "All Slots",
                    "url": self.url + "/slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Jackpot Slots",
                    "url": self.url + "/slots/jackpot-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Free Spins",
                    "url": self.url + "/slots/free-spins/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Bonus Rounds",
                    "url": self.url + "/slots/bonus-round/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Multiways Extra",
                    "url": self.url + "/slots/multiways-extra/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Stacked Wilds",
                    "url": self.url + "/slots/stacked-wilds/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Tumbling Reels",
                    "url": self.url + "/slots/tumbling-reels/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Click Me Slots",
                    "url": self.url + "/slots/click-me/",
                    "icon": "fa-star-o"
                }
                ]
            },
            {
                "label": "Slots by Paylines",
                "isGrouping": true,
                "icon": "fa-star",
                "subLinks": [
                {
                    "label": "10 & Less Lines",
                    "url": self.url + "/slots/10-and-less-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "15 Lines",
                    "url": self.url + "/slots/15-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "20 Lines",
                    "url": self.url + "/slots/20-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "25 Lines",
                    "url": self.url + "/slots/25-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "30 Lines",
                    "url": self.url + "/slots/30-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "40 Lines",
                    "url": self.url + "/slots/40-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "50 & Over Lines",
                    "url": self.url + "/slots/50-and-more-line-slots/",
                    "icon": "fa-star-o"
                }
                ]
            },
            {
                "label": "Slots by Themes",
                "isGrouping": true,
                "icon": "fa-star",
                "subLinks": [
                {
                    "label": "Magic and Myth",
                    "grouping": "[grouping]",
                    "isGrouping": true,
                    "isSubmenu": false,
                    "subLinks": [],
                    "url": self.url + "/slots/magic-and-myth/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Animals and Nature",
                    "url": self.url + "/slots/animals-and-nature/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Movie and Adventure",
                    "url": self.url + "/slots/movie-and-adventure/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Egyptian Slots",
                    "url": self.url + "/slots/egyptian/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Food and Fruits",
                    "url": self.url + "/slots/food-and-fruit/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Pirates and Treasure",
                    "url": self.url + "/slots/pirates-and-treasure/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Exclusive Slots",
                    "url": self.url + "/slots/exclusive/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "New Slots",
                    "url": self.url + "/slots/new/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Featured Slots",
                    "url": self.url + "/slots/featured/",
                    "icon": "fa-star-o"
                }
                ]
            },
            {
                "label": "Live Casino",
                "url": self.url + "/live-casino/",
                "icon": "fa-female",
                "subLinks": []
            },
            {
                "label": "Table & Card",
                "url": self.url + "/table-card/",
                "icon": "fa-th-large",
                "subLinks": []
            },
            {
                "label": "Roulette",
                "url": self.url + "/roulette/",
                "icon": "fa-cog",
                "subLinks": []
            },
            {
                "label": "Scratch & Arcade",
                "url": self.url + "/scratch-and-arcade/",
                "icon": "fa-list-alt",
                "subLinks": []
            },
            {
                "label": "All Games",
                "url": self.url + "/games/",
                "icon": "fa-sort-alpha-asc",
                "subLinks": []
            },
            {
                "label": "Promotions",
                "url": self.url + "/promotions/",
                "icon": "fa-dropbox",
                "subLinks": []
            },
            {
                "label": "VIP & Loyalty",
                "url": self.url + "/loyalty/vip-club/",
                "icon": "fa-bookmark",
                "subLinks": []
            },
            {
                "label": "Help",
                "grouping": "[grouping]",
                "isGrouping": true,
                "isSubmenu": false,
                "subLinks": [
                {
                    "label": "FAQ",
                    "url": self.url + "/faq/",
                    "icon": "fa-qrcode",
                    "subLinks": []
                },
                {
                    "label": "Support",
                    "url": self.url + "/support/",
                    "icon": "fa-comments",
                    "subLinks": []
                },
                {
                    "label": "About Us",
                    "url": self.url + "/about-us/",
                    "icon": "fa-asterisk",
                    "subLinks": []
                },
                {
                    "label": "Privacy Policy",
                    "url": self.url + "/privacy-policy/",
                    "icon": "fa-stack-exchange",
                    "subLinks": []
                }
                ],
                "icon": "fa-question"
            },
            {
                "label": "Responsible Gaming",
                "url": self.url + "/responsible-gaming/",
                "icon": "fa-thumbs-o-up",
                "subLinks": []
            },
            {
                "label": "Terms & Conditions",
                "url": self.url + "/terms-and-conditions/",
                "icon": "fa-shield",
                "subLinks": []
            }
        ],
        loggedIn: [
            {
                "label": "Home",
                "url": self.url + "/start/",
                "icon": "fa-home",
                "subLinks": []
            },  
            {
                "label": "Cashier",
                "url": self.url + "/cashier/",
                "icon": "fa-gbp",
                "subLinks": []
            },
            {
                "label": "My Account",
                "url": self.url + "/my-account/",
                "icon": "fa-user",
                "subLinks": []
            },
            {
                "label": "My Favourites",
                "url": self.url + "/my-favourites/",
                "icon": "fa-heart",
                "subLinks": []
            },
            {
                "label": "New Games",
                "url": self.url + "/slots/new/",
                "icon": "fa-fire",
                "subLinks": []
            },
            {
                "label": "Slots by Feature",
                "isGrouping": true,
                "icon": "fa-star",
                "subLinks": [
                {
                    "label": "All Slots",
                    "url": self.url + "/slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Jackpot Slots",
                    "url": self.url + "/slots/jackpot-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Free Spins",
                    "url": self.url + "/slots/free-spins/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Bonus Rounds",
                    "url": self.url + "/slots/bonus-round/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Multiways Extra",
                    "url": self.url + "/slots/multiways-extra/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Stacked Wilds",
                    "url": self.url + "/slots/stacked-wilds/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Tumbling Reels",
                    "url": self.url + "/slots/tumbling-reels/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Click Me Slots",
                    "url": self.url + "/slots/click-me/",
                    "icon": "fa-star-o"
                }
                ]
            },
            {
                "label": "Slots by Paylines",
                "isGrouping": true,
                "icon": "fa-star",
                "subLinks": [
                {
                    "label": "10 & Less Lines",
                    "url": self.url + "/slots/10-and-less-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "15 Lines",
                    "url": self.url + "/slots/15-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "20 Lines",
                    "url": self.url + "/slots/20-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "25 Lines",
                    "url": self.url + "/slots/25-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "30 Lines",
                    "url": self.url + "/slots/30-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "40 Lines",
                    "url": self.url + "/slots/40-line-slots/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "50 & Over Lines",
                    "url": self.url + "/slots/50-and-more-line-slots/",
                    "icon": "fa-star-o"
                }
                ]
            },
            {
                "label": "Slots by Themes",
                "isGrouping": true,
                "icon": "fa-star",
                "subLinks": [
                {
                    "label": "Magic and Myth",
                    "grouping": "[grouping]",
                    "isGrouping": true,
                    "isSubmenu": false,
                    "subLinks": [],
                    "url": self.url + "/slots/magic-and-myth/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Animals and Nature",
                    "url": self.url + "/slots/animals-and-nature/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Movie and Adventure",
                    "url": self.url + "/slots/movie-and-adventure/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Egyptian Slots",
                    "url": self.url + "/slots/egyptian/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Food and Fruits",
                    "url": self.url + "/slots/food-and-fruit/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Pirates and Treasure",
                    "url": self.url + "/slots/pirates-and-treasure/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Exclusive Slots",
                    "url": self.url + "/slots/exclusive/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "New Slots",
                    "url": self.url + "/slots/new/",
                    "icon": "fa-star-o"
                },
                {
                    "label": "Featured Slots",
                    "url": self.url + "/slots/featured/",
                    "icon": "fa-star-o"
                }
                ]
            },
            {
                "label": "Live Casino",
                "url": self.url + "/live-casino/",
                "icon": "fa-female",
                "subLinks": []
            },
            {
                "label": "Table & Card",
                "url": self.url + "/table-card/",
                "icon": "fa-th-large",
                "subLinks": []
            },
            {
                "label": "Roulette",
                "url": self.url + "/roulette/",
                "icon": "fa-cog",
                "subLinks": []
            },
            {
                "label": "Scratch & Arcade",
                "url": self.url + "/scratch-and-arcade/",
                "icon": "fa-list-alt",
                "subLinks": []
            },
            {
                "label": "All Games",
                "url": self.url + "/games/",
                "icon": "fa-sort-alpha-asc",
                "subLinks": []
            },
            {
                "label": "Promotions",
                "url": self.url + "/promotions/",
                "icon": "fa-dropbox",
                "subLinks": []
            },
            {
                "label": "VIP & Loyalty",
                "url": self.url + "/loyalty/vip-club/",
                "icon": "fa-bookmark",
                "subLinks": []
            },
            {
                "label": "Help",
                "grouping": "[grouping]",
                "isGrouping": true,
                "isSubmenu": false,
                "subLinks": [
                {
                    "label": "FAQ",
                    "url": self.url + "/faq/",
                    "icon": "fa-qrcode",
                    "subLinks": []
                },
                {
                    "label": "Support",
                    "url": self.url + "/support/",
                    "icon": "fa-comments",
                    "subLinks": []
                },
                {
                    "label": "About Us",
                    "url": self.url + "/about-us/",
                    "icon": "fa-asterisk",
                    "subLinks": []
                },
                {
                    "label": "Privacy Policy",
                    "url": self.url + "/privacy-policy/",
                    "icon": "fa-stack-exchange",
                    "subLinks": []
                }
                ],
                "icon": "fa-question"
            },
            {
                "label": "Player Settings",
                "url": self.url + "/cashier/?settings=1/",
                "icon": "fa-list-alt"
            },
            {
                "label": "Responsible Gaming",
                "url": self.url + "/responsible-gaming/",
                "icon": "fa-thumbs-o-up",
                "subLinks": []
            },
            {
                "label": "Terms & Conditions",
                "url": self.url + "/terms-and-conditions/",
                "icon": "fa-shield",
                "subLinks": []
            },
            {
                "label": "Log Out",
                "url": "javascript:sbm.native.logout();",
                "icon": "fa-share",
                "subLinks": []
            }
        ]
    }

    /**
     * initialization
     * @param {*} core 
     */
    function init (core) {
        
        self.core = core;

        // event subscription

        self.core.subscribe("done-login", onLogin);
        self.core.subscribe("done-logout", onLogout);

        // menu configuration

        setTimeout(function () {
            setMenu(!!sbm.core.storage.get("sessionId"));
        },10000);
        
    }

    /*
    * Logout from the skin
    */
    function logout () {
        self.core.publish("do-logout");
        var json = JSON.stringify(self.menu.loggedOut);
        window.location.href = "gonative://sidebar/setItems?items=" + encodeURIComponent(json);
    }

    /**
     * 
     * @param {*} selector 
     */
    function removeHeader (selector) {
        self.core.$(selector).remove();
    }

    /**
     * configures the native menu depending if player is logged in or not
     * This should be done when app is started and when player logs in or logs out
     * @param {*} loggedIn: boolean true if logged in false if logged out
     */
    function setMenu (loggedIn) {

        var json,
            session;

        // get menu configuration depending on the session value

        json = loggedIn ? JSON.stringify(self.menu.loggedIn) : JSON.stringify(self.menu.loggedOut);
        
        // request to native wrapper to change the menu
        
        window.location.href = "gonative://sidebar/setItems?items=" + encodeURIComponent(json); 
    }

    /**
     * on login it should switch to the logged in menu
     */
    function onLogin () {
        setMenu(true);
    }

    /**
     * on logout it should switch to the logged out menu
     */
    function onLogout () {
        setMenu(false);
    }

    /**
     * searches for games
     * @param {*} term The serch term
     */
    function search (term) {
        self.core.publish("search-games", [term]);
    }

    /**
     * everything we want to expose to global scope
     */
    return {
        init: init,
        logout: logout,
        removeHeader: removeHeader
    };

};
sbm.native = sbm.native();