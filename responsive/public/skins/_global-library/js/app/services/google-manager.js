var sbm = sbm || {};

/**
 * Handles all actions for Google Tag Manager
 * @param core
 * @param dataLayer
 * @constructor
 */
sbm.GoogleManager = function (core, dataLayer) {
    "use strict";

    /**
     * Module initialization
     */
    this.init = function () {

        core.$("[data-gtag]").on("click", this.clicked);
        core.$(".left-off-canvas-menu > ul > li > a").on("click", this.leftMenuClicked);
        core.$(".right-off-canvas-menu > ul > li > a").on("click", this.rightMenuClicked);
    };

    /**
     * Handles clicks on elements which have the tag defined
     */
    this.clicked = function (e) {

        var gtag = core.$(e.currentTarget).data("gtag");
        this.pushToDataLayer(gtag);

    }.bind(this);

    /**
     * Handles clicks on mobile left menu
     */
    this.leftMenuClicked = function (e) {
        var href = $(e.currentTarget).attr("href");
        this.pushToDataLayer("Left Menu Click," + href || "");
        if (!sbm.serverconfig.contentSwap) {
            window.location.pathname = href;
        }
    }.bind(this);

    /**
     * Handles clicks on mobile right menu
     */
    this.rightMenuClicked = function (e) {
        var href = $(e.currentTarget).attr("href");
        this.pushToDataLayer("Right Menu Click," + href || "");
        if (!sbm.serverconfig.contentSwap) {
            window.location.pathname = href;
        }
    }.bind(this);

    /**
    * pushes the data to the google's data layer
    * tag should be defined as "category, label"
    */
    this.pushToDataLayer = function (gtag) {

        if (!gtag) {
            core.logger.log("GTag empty", core.logger.severity.ERROR);
            return;
        }

        gtag = gtag.split(",");

        var category = gtag[0];
        var label =  (gtag[1] && gtag[1] !== "undefined") ? gtag[1] : "";
        var action = window.location.pathname;

        if (window.dataLayer) {

            window.dataLayer.push({
                "event": "GAevent",
                "eventCategory": category,
                "eventAction": action,
                "eventLabel": label
            });

            core.logger.log("GTag  C: " + category + ", A:" + action + ", L:" + label, core.logger.severity.INFO);
        }

    }.bind(this);

    /**
    * Pushes variable the data to the google's data layer
    */
    this.pushVariableToDataLayer = function (key, value) {
        if (!key) {
            core.logger.log("Variable Empty", core.logger.severity.ERROR);
            return;
        }

        if (window.dataLayer) {
            var variables = {};
            variables["event"] = "OLEvent";
            variables[key] = value;
            this.resetDataLayerGTM();
            window.dataLayer.push(variables);

            if (sbm.serverconfig.debug == true) {
                core.logger.log("Variable Sent: " + key, core.logger.severity.INFO);
            }
        }
    }.bind(this);

    /**
    * Pushes array of variables to the GTM
    */
    this.pushSetOfVariablesToDataLayer = function (variables) {
        if (!variables || Object.keys(variables).length === 0) {
            core.logger.log("Variables Empty", core.logger.severity.ERROR);
            return;
        }
        if (window.dataLayer) {

            if (!('event' in variables))
                variables.event = "OLEvent";

            this.resetDataLayerGTM();
            window.dataLayer.push(variables);
            if (sbm.serverconfig.debug == true) {
                core.logger.log("Variables Sent: " + Object.keys(variables), core.logger.severity.INFO);
            }
        }
    }.bind(this);

    this.resetDataLayerGTM = function() {
        var gtmId = sbm.serverconfig.gtmId;
        if (gtmId !== "" && window.google_tag_manager !== undefined && window.google_tag_manager[gtmId] !== undefined && window.google_tag_manager[gtmId].dataLayer !== undefined ) {
            window.google_tag_manager[gtmId].dataLayer.reset();
        }
    }
};
