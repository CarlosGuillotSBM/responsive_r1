var sbm = sbm || {};

/**
 * Logs messages
 * @returns {sbm.Logger}
 * @constructor
 */
sbm.Logger = function () {

    "use strict";

    var self = this;

    self.severity = {};
    self.severity.INFO = "INFO";
    self.severity.WARNING = "WARNING";
    self.severity.ERROR = "ERROR";
    self.severity.FATAL = "FATAL";

    /**
     * Log a message on the console
     * @param msg
     * @param severity
     */
    self.log = function (msg, severity) {

        if (!window.console || !sbm.serverconfig.debug) {
            return;
        }

        msg = "[SBM LOGGER] " + msg;

        if (severity === self.severity.INFO) {

            console && console.info(msg);

        } else if (severity === self.severity.WARNING) {

            console && console.info(msg);

        } else if (severity === self.severity.ERROR) {

            console && console.error(msg);

        } else {

            throw msg;

        }

    };

    return self;
}