var sbm = sbm || {};

/**
* Class responsible for all the AJAX requests to website backend
* @param core Instance of the core class
* @param url Url to the API
* @returns {DataserviceWeb}
* @constructor
*/
sbm.DataserviceWeb = function (core, url) {
    "use strict";
    
    var self = this;
    
    url = url || "/data.php";
    
    core._.extend(self, new sbm.Dataservice(core, url));
    
    /**
    * Initialization
    */
    self.init = function () {
        
        core.logger.log("Web Dataservice init", core.logger.severity.INFO);
        
        core.subscribe("do-login", self.login);
        core.subscribe("do-login-after-registration", self.loginAfterRegistration);
        core.subscribe("get-register-session", self.getRegisterSession);
        core.subscribe("check-register-username", self.checkRegisterUsername);
        core.subscribe("check-register-email", self.checkRegisterEmail);
        core.subscribe("get-postcode-lookup", self.getPostCodeLookup);
        core.subscribe("get-address-details", self.getAddressDetails);
        core.subscribe("register-player", self.registerPlayer);
        core.subscribe("set-favourite-game", self.setFavouriteGame);
        core.subscribe("get-favourite-list", self.getFavouriteList);
        core.subscribe("do-logout", self.logout);
        core.subscribe("get-balance", self.getBalance);
        core.subscribe("get-banking-activity", self.getBankingActivity);
        core.subscribe("get-games-activity", self.getGamesActivity);
        core.subscribe("get-promo-activity", self.getPromoActivity);
        core.subscribe("get-personal-details", self.getPersonalDetails);
        core.subscribe("save-personal-details", self.setPersonalDetails);
        core.subscribe("get-player-account", self.getPlayerAccount);
        core.subscribe("convert-points", self.convertPoints);
        core.subscribe("search-games", self.searchGames);
        core.subscribe("get-winners-feed", self.getWinnersFeed);
        core.subscribe("forgot-password", self.forgotPassword);
        core.subscribe("reset-password", self.resetPassword);
        core.subscribe("accept-tc", self.acceptTC);
        core.subscribe("get-bingo-rooms", self.getBingoRooms);
        core.subscribe("get-bingo-room-detail", self.getBingoRoomDetail);
        core.subscribe("get-player-limits", self.getPlayerLimits);
        core.subscribe("set-player-limits", self.setPlayerLimits);
        core.subscribe("accept-player-limits", self.acceptLimits);
        core.subscribe("cancel-player-limits", self.cancelLimits);
        core.subscribe("check-promo-code", self.checkPromoCode);
        core.subscribe("redeem-promo", self.redeemPromo);
        core.subscribe("resend-verification-code", self.resendVerificationCode);
        core.subscribe("verify-mobile", self.verifyMobile);
        core.subscribe("get-competition-list", self.getCompetitionList);
        core.subscribe("get-leaderboard-by-competitionId", self.getLeaderboardByCompetitionId);
        core.subscribe("opt-in", self.optIn);
        core.subscribe("void-bonus", self.voidBonus);
        core.subscribe("get-player-raf-info", self.getPlayerRafInfo);
        core.subscribe("send-raf-emails", self.sendRafEmails);
        core.subscribe("message-update-status", self.updateMessageStatus);
        core.subscribe("message-get-preview", self.messageGetPreview);
        core.subscribe("messages-get-inbox", self.messagesGetInbox);
        core.subscribe("cashier-first-deposit", self.firstDeposit);
        core.subscribe("get-progressives", self.getProgressives);
        core.subscribe("get-player-cashback", self.getPlayerCashback);
        core.subscribe("claim-player-cashback", self.claimPlayerCashback);
        core.subscribe("get-player-upgrade-bonus", self.getPlayerUpgradeBonus);
        core.subscribe("claim-player-upgrade-bonus", self.claimPlayerUpgradeBonus);
        core.subscribe("get-game-recommendations", self.getGameRecommendations);
        core.subscribe("track-navigation", self.trackNavigation);
        core.subscribe("reality-get-settings", self.realityGetSettings);
        core.subscribe("reality-set-settings", self.realitySetSettings);
        core.subscribe("reality-stop-game", self.realityStopGame);
        core.subscribe("reality-start-game",self.realityStartGame);
        core.subscribe("get-game-info",self.getGameInfoLauncher);
        core.subscribe("claim-prize",self.claimPrize);
        core.subscribe("submit-potential-player",self.submitPotentialPlayer);
        core.subscribe("get-customer-number",self.getCustomerNumber);
        core.subscribe("get-customer",self.getCustomer);
        core.subscribe("get-net-deposit",self.getNetDeposit);
        core.subscribe("get-transaction-summary", self.getTransactionSummary);
        core.subscribe("get-tcs-pp", self.getTcsPP);
        core.subscribe("tcs-pp-accept", self.tcsPPAccept);
        core.subscribe("tcs-register-accept", self.tcsRegisterAcept);
        core.subscribe("get-player-entity", self.getPlayerEntity);
        core.subscribe("dont-miss-out-update", self.dontMissOutUpdate);
        core.subscribe("get-aspers-tc-pp", self.getAspersTCPP);
        core.subscribe("aspers-do-first-login", self.getAspersDoFirstLogin);
        core.subscribe("age-id-verification-check-if-verified", self.onAgeIdVerificationCheckIfVerified);
        core.subscribe("recaptcha-ready", self.onRecaptchaReady);
    };
    
    /**
    * Login
    * @param e
    * @param username
    * @param password
    * @param async
    */
    self.login = function (e, username, password, async, redirectURL, loginWithSession) {
        
        if (!username || !password) {
            core.logger.log("Dataservice.login Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        redirectURL = (typeof redirectURL !== 'undefined') ? redirectURL : '';
        loginWithSession = (typeof loginWithSession !== 'undefined') ? loginWithSession : 0;
        
        var screenSize = window.screen.width + "x" + window.screen.height;
        
        var p = {
            f: "login",
            u: username,
            p: password,
            s: screenSize,
            r: redirectURL,
            l: loginWithSession,
            t: sbm.recaptchaToken
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("done-login", data);
        }, false, async);
        
    };
    
    /**
    * Login after player registration
    * @param e
    * @param username
    * @param password
    */
    self.loginAfterRegistration = function (e, username, password) {
        
        if (!username || !password) {
            core.logger.log("Dataservice.loginAfterRegistration Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var screenSize = window.screen.width + "x" + window.screen.height;
        var p = {
            f: "login",
            u: username,
            p: password,
            s: screenSize,
            afterRegistration: true
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("done-login-after-registration", data);
        }, false);
        
    };
    
    /**
    * Get Transaction Summary
    */
    self.getTransactionSummary = function (e, request) {
        
        if (!request.startDate || !request.endDate) {
            core.logger.log("Dataservice.getTransactionSummary Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
            
        }
        
        var p = { 
            f: "getPlayerTransactionsSummary",
            startDate: request.startDate,
            endDate: request.endDate
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-transaction-summary", data);
        }, false);
        
    };
    
    
    /**
    * Get register session
    * @param e
    */
    self.getRegisterSession = function (e) {
        
        var p = {
            f: "regsession"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-register-session", data);
        });
        
    };
    /**
    * Check if username if available for registration
    * @param e
    * @param username
    * @param groupId
    * @param regSessionId
    */
    self.checkRegisterUsername = function (e, username, groupId, regSessionId) {
        
        if (!username || !groupId || !regSessionId) {
            core.logger.log("Dataservice.checkRegisterUsername Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "regcheckusername",
            u: username,
            g: groupId,
            rs: regSessionId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("checked-register-username", data);
        });
        
    };
    /**
    * Check if email is available for registration
    * @param e
    * @param email
    * @param groupId
    * @param regSessionId
    */
    self.checkRegisterEmail = function (e, email, groupId, regSessionId) {
        
        if (!email || !groupId || !regSessionId) {
            core.logger.log("Dataservice.checkRegisterEmail Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "regcheckemail",
            e: email,
            g: groupId,
            rs: regSessionId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("checked-register-email", data);
        });
        
    };
    /**
    * Get addresses for the postcode
    * @param e
    * @param postcode
    * @param regSessionId
    */
    self.getPostCodeLookup = function (e, postcode, regSessionId) {
        
        if (!postcode || postcode.length < 4 || !regSessionId) {
            core.logger.log("Dataservice.getPostCodeLookup Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f : "regcheckpostcode",
            p : postcode,
            rs: regSessionId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-postcode-lookup", data);
        });
        
    };
    /**
    * Get address details for the address Id
    * @param e
    * @param addressId
    */
    self.getAddressDetails = function (e, addressId) {
        
        if (!addressId) {
            core.logger.log("Dataservice.getAddressDetails Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f : "getAddressDetails",
            i : addressId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-address-details", data);
        });
        
    };
    /**
    * Register a new player
    * @param e
    * @param playerDetails
    * @param groupId
    * @param regSessionId
    */
    self.registerPlayer = function (e, playerDetails, groupId, regSessionId) {
        
        if (!playerDetails || !groupId || !regSessionId) {
            core.logger.log("Dataservice.registerPlayer Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        if (playerDetails.title === "mr") playerDetails.gender = 1;
        else if (playerDetails.title === "mrs" || playerDetails.title === "ms") playerDetails.gender = 0;
        else playerDetails.gender = 2;
        
        var p = {
            f: "register",
            rs: regSessionId,
            g: groupId,
            u: playerDetails.username,
            p: playerDetails.password,
            e: playerDetails.email,
            c: playerDetails.currency,
            pc: playerDetails.promoCode || "",
            t: playerDetails.title,
            fn: playerDetails.firstName,
            ln: playerDetails.lastName,
            dob: playerDetails.dob,
            cc: playerDetails.country,
            a1: playerDetails.address1  ?  playerDetails.address1.toString(): playerDetails.address1.toString(),
            a2: playerDetails.address2  ?  playerDetails.address2.toString():playerDetails.address2.toString(),
            cty: playerDetails.city     ?  playerDetails.city.toString()    :playerDetails.city.toString(),
            st: playerDetails.state,
            pst: playerDetails.postcode,
            lp: playerDetails.landline,
            mp: playerDetails.mobile,
            we: playerDetails.wantsEmail ? 1 : 0,
            ws: playerDetails.wantsSMS ? 1 : 0,
            wp: playerDetails.wantsPhone ? 1 : 0,
            wpm: playerDetails.wantsMail ? 1 : 0,
            src: playerDetails.src || "",
            l: playerDetails.landPage || "",
            ref: playerDetails.ref,
            oc: playerDetails.offlineCasino || "",
            ac: playerDetails.aspersCustomer,
            tpemails: playerDetails.wantsThirdPartyEmails,
            tpsms: playerDetails.wantsThirdPartySMS,
            tpc: playerDetails.wantsThirdPartyCalls,
            gender: playerDetails.gender
        };
        self.ajaxRequest(p, function (data) {
            core.publish("registered-player", data);
        });
    };
    /**
    * Set/Remove game from favourites list
    * @param e
    * @param gameId
    * @param isFavourite
    */
    self.setFavouriteGame = function (e, gameId, isFavourite) {
        
        if (!gameId) {
            core.logger.log("Dataservice.setFavouriteGame Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "setplayerfavouritegame",
            g: gameId,
            a: isFavourite ? 1 : 0
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("finish-set-favourite-game", data);
        });
    };
    
    /**
    * Get favourites game list
    *
    * @param e
    * @param gameId
    * @param isFavourite
    */
    self.getFavouriteList = function (e) {
        
        var p = {
            f: "getplayerfavouritegames"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("finish-get-favourite-list", data);
        });
    };
    
    /**
    * Logout player from the website
    */
    self.logout = function () {
        
        var p = {
            f: "logout"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("done-logout", data);
        });
    };
    /**
    * Get player's balance
    */
    self.getBalance = function () {
        
        var p = {
            f: "balance"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-balance", data);
        });
        
    };
    /**
    * Get player's activity on the website
    * @param data
    * @param notification
    */
    self.getActivity = function (data, notification) {
        
        if (!data || !notification) {
            core.logger.log("Dataservice.getActivity Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "getplayertx",
            date: data.date,
            endDate: data.endDate,
            start: data.start,
            length: data.length,
            sort: data.sort,
            dir: data.dir,
            search: data.search,
            types: data.types
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish(notification, data);
        });
    };
    /**
    * Get player's banking activity
    * @param e
    * @param data
    */
    self.getBankingActivity = function (e, data) {
        self.getActivity(data, "got-banking-activity");
    };
    /**
    * Get player's game activity
    * @param e
    * @param data
    */
    self.getGamesActivity = function (e, data) {
        self.getActivity(data, "got-games-activity");
    };
    /**
    * Get player's promo activity
    */
    self.getPromoActivity = function () {
        var p = {
            f: "getplayerbonustx"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-promo-activity", data);
        });
    };
    
    /**
    * Net Deposits
    */
    self.getNetDeposit = function (e, playerId) {
        
        if (!playerId) {
            core.logger.log("Dataservice.getNetDeposit Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "getnetdeposits",
            id: playerId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-net-deposit", data);
        });
        
    };
    
    /**
    * Get player's personal details
    */
    self.getPersonalDetails = function () {
        var p = {
            f: "getplayerdetails"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-personal-details", data);
        });
    };
    /**
    * Get skin's progressives
    */
    self.getProgressives = function () {
        var p = {
            f: "getprogressives"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-progressives", data);
        });
    };    
    /**
    * Save player's personal details
    * @param e
    * @param data
    */
    self.setPersonalDetails = function (e, data) {
        
        if (!data) {
            core.logger.log("Dataservice.setPersonalDetails Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "setplayerdetails",
            opwd: data.currentPassword,
            pwd: data.newPassword,
            lp: data.landPhone,
            lpe: data.landPhoneExt,
            mp: data.mobilePhone,
            sqid: data.securityQuestion,
            sa: data.securityAnswer,
            email: data.wantsEmail,
            sms: data.wantsSMS,
            phone: data.wantsPhoneCalls,
            mailpost: data.wantsMail,
            tpemails: data.wantsThirdPartyEmails,
            tpsms: data.wantsThirdPartySMS,
            tpc: data.wantsThirdPartyCalls,
            acn: data.aspersCustomerNumber
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("saved-personal-details", data);
        });
    };
    /**
    * Get player's account
    */
    self.getPlayerAccount = function () {
        var p = {
            f: "getplayeraccount"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-player-account", data);
        });
    };
    
    /**
    * Convert points
    * @param e
    * @param points
    * @param type
    */
    self.convertPoints = function (e, points, type) {
        
        if (!points || !type) {
            core.logger.log("Dataservice.convertPoints Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "redeempoints",
            p: points,
            t: type
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("converted-points", data);
        });
    };
    /**
    * Search for games
    * @param e
    * @param title
    */
    self.searchGames = function (e, title) {
        
        var p = {
            f: "searchgames",
            t: title
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("searched-games", data);
        });
    };
    /**
    * Get winners
    */
    self.getWinnersFeed = function () {
        
        var p = {
            f: "winners"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-winners-feed", data);
        });
    };
    /**
    * Request a new password
    * @param e
    * @param email
    */
    self.forgotPassword = function (e, email) {
        
        if (!email) {
            core.logger.log("Dataservice.forgotPassword Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "playerforgotpassword",
            e: email
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("forgot-password-finished", data);
        });
        
    };
    /**
    * Set a new password
    * @param e
    * @param playerId
    * @param GUID
    * @param newPassword
    */
    self.resetPassword = function (e, playerId, GUID, newPassword) {
        
        if (!playerId || !GUID || !newPassword) {
            core.logger.log("Dataservice.resetPassword Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "playerforgotpasswordupdate",
            id: playerId,
            guid: GUID,
            p: newPassword
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("reset-password-finished", data);
        });
        
    };
    /**
    * Accept terms and conditions
    * @param e
    * @param playerId
    */
    self.acceptTC = function (e, playerId) {
        
        if (!playerId) {
            core.logger.log("Dataservice.acceptTC Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            f: "accepttandc",
            id: playerId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("accepted-tc", data);
        });
        
    };
    
    /**
    * Get bingo rooms
    */
    self.getBingoRooms = function () {
        
        var p = {
            f: "getbingorooms"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-bingo-rooms", data);
        });
        
    };
    
    /**
    * Get bingo rooms
    */
    self.getBingoRoomDetail = function () {
        
        var p = {
            f: "getbingorooms"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-bingo-room-detail", data);
        });
        
    };
    
    /**
    * Get player limits
    */
    self.getPlayerLimits = function () {
        
        var p = {
            f: "getlimits"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-player-limits", data);
        });
    };
    
    /**
    * Set player limits
    */
    self.setPlayerLimits = function (e, limits) {
        
        var p = {
            f: "setlimits",
            wlp : limits.wlp,
            wla : limits.wla,
            llp : limits.llp,
            lla : limits.lla,
            dlp : limits.dlp,
            dla : limits.dla
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("finished-set-player-limits", data);
        });
    };
    
    /**
    * Accept player limits
    * @param e
    */
    self.acceptLimits = function (e) {
        
        var p = {
            f: "acceptplayerlimits",
            a: 1
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-acceptance-limits", data);
        });
        
    };
    
    /**
    * Cancel player limits
    * @param e
    */
    self.cancelLimits = function (e) {
        
        var p = {
            f: "acceptplayerlimits",
            a: 0
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-acceptance-limits", data);
        });
        
    };
    
    /**
    * Check if a promo code is valid
    */
    self.checkPromoCode = function (e, code) {
        
        var p = {
            "f": "checkpromocode",
            "p": code
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("checked-promo-code", data);
        });
    };
    
    /**
    * Redeem promo code
    */
    self.redeemPromo = function (e, code) {
        
        var p = {
            "f": "playerredeempromocode",
            "p": code
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("redeemed-promo", data);
        });
    };
    
    /**
    * Re-send verification code to mobile phone
    */
    self.resendVerificationCode = function () {
        
        var p = {
            "f": "resendphoneverificationcode"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("resend-verification-code-finished", data);
        });
    };
    
    /**
    * Verify mobile phone
    */
    self.verifyMobile = function (e, code) {
        
        var p = {
            "f": "playerverifymobile",
            "p": code
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("verify-mobile-finished", data);
        });
    };
    
    /**
    * Get competion list
    */
    self.getCompetitionList = function () {
        
        var p = {
            "f": "competitionpromolist"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-competition-list", data);
        });
    };
    
    /**
    * Get competion list
    */
    self.getLeaderboardByCompetitionId = function (e, compId, promoName) {
        
        var p = {
            "f": "leaderboardbycompetition",
            "cId": compId
        };
        
        self.ajaxRequest(p, function (data) {
            data["PromoName"] = promoName;
            core.publish("got-leaderboard-for-competition", data);
        });
    };
    
    /**
    * Opt in player on a tournament
    */
    self.optIn = function (e, tournament, playerId) {
        
        if (!tournament || !playerId) {
            core.logger.log("Dataservice.optIn Fatal Error: invalid parameters passed", core.logger.severity.ERROR);
            return;
        }
        
        var p = {
            "f": "optin",
            "p": playerId,
            "c": tournament
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("opt-in-finished", data);
        });
    };
    
    /**
    * Player voids bonus using txPromoID
    */
    self.voidBonus = function (e,txPromoId) {
        
        var p = {
            "f": "playertxpromocancel",
            "p": txPromoId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-voided-bonus", data);
        });
    };
    
    
    /**
    * Get player's raf info
    */
    self.getPlayerRafInfo = function () {
        var p = {
            f: "getplayerrafinfo"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-player-raf-info", data);
        });
    };
    
    /*
    * Player sends refer a friend emails
    * @param e
    * @param emailList: semicolon list of emails to send a refer a friend email
    */
    self.sendRafEmails = function(e,emailList) {
        var p = {
            "f": "rafrefer",
            "el": emailList
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("sent-raf-emails", data);
        });
    };
    
    
    
    /*
    * We update the status of the Message
    * @param action: the status modification in the message 'read', 'unread', 'deleted'
    * @param messageId: the id of the message to update its status
    */
    self.updateMessageStatus = function(e, action, messageId) {
        var p = {
            "f": "messageUpdateStatus",
            "a": action,
            "m": messageId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("message-status-updated", data);
        });
    };
    
    
    
    /*
    * Retrieves a renderization of the message so the player can see it
    * @param messageId messageId to render
    */
    
    self.messageGetPreview = function(e,messageId) {
        var p = {
            "f": "messageGetPreview",
            "m": messageId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("message-got-preview", data);
        });
    };
    
    
    /*
    * Retrieves a renderization of the message so the player can see it
    * @param messageId messageId to render
    */
    
    self.messagesGetInbox = function(e,templateId) {
        var p = {
            "f": "getPlayerMessages"
        };
        
        if (templateId != 0) {
            p["t"] = templateId;
        }
        
        self.ajaxRequest(p, function (data) {
            core.publish("messages-got-inbox", data);
        });
    };
    
    /**
    * Keep the affiliate trail after the first time deposit
    */
    self.firstDeposit = function () {
        
        var p = {
            "f": "firstDeposit"
        };
        
        core._.extend(p, self.commonParams);
        
        self.ajaxRequest(p, function () {}); //no need to notify
    };
    
    /**
    * Return the cashback of the active player if any
    */
    self.getPlayerCashback = function () {
        
        var p = {
            "f": "getPlayerCashback"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-player-cashback", data);
        });
    };
    
    /**
    * Claims the cashback of the player
    * @param cashbackId: the id of the cashback item to be claimed
    */
    self.claimPlayerCashback = function (e,cashbackId) {
        var p = {
            "f": "claimCashback",
            "c": cashbackId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("claimed-player-cashback", data);
        });
    };
    
    /**
    * Return the upgrade bonus of the active player if any
    */
    self.getPlayerUpgradeBonus = function () {
        
        var p = {
            "f": "getPlayerUpgradeBonus"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-player-upgrade-bonus", data);
        });
    };
    
    /**
    * Claims the upgrade bonus of the player
    * @param upgradeBonusId: the id of the upgrade bonus item to be claimed
    */
    self.claimPlayerUpgradeBonus = function (e, upgradeBonusId) {
        var p = {
            "f": "claimUpgradeBonus",
            "c": upgradeBonusId 
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("claimed-player-upgrade-bonus", data);
        });
    };
    
    
    /**
    * Get player recommendations
    */
    self.getGameRecommendations = function () {
        var p = {
            "f": "gamerecommendations"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-game-recommendations", data);
        });
    };
    
    /**
    * Tracks the navigated page so far
    */
    self.trackNavigation = function (e, navigationPage, action, label) {
        var p = {
            "f": "tracknavigation",
            "np": navigationPage,
            "a": action,
            "l": label
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("tracked-navigation", data);
        });
    };
    
    self.realityGetSettings = function () {
        var p = {
            "f": "realityGet"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-reality-settings", data);
        });
    };
    
    self.realitySetSettings = function (e,realityCheckTypeId) {
        var p = {
            "f": "realitySet",
            "rc": realityCheckTypeId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("on-set-reality-settings", data);
        });
    };
    
    self.realityStopGame = function (e,gameId) {
        var p = {
            "f": "realityStop",
            "g": gameId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("on-reality-stop-game", data);
        });
    };
    
    self.realityStartGame = function (e,gameId) {
        var p = {
            "f": "realityStart",
            "g": gameId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("on-reality-start-game", data);
        });
    };
    
    /**
    * Retrieves games information from server
    * @param e
    * @param gameId
    */
    self.getGameInfoLauncher = function (e, gameId, fun, extraParams) {
        if (!gameId) {
            core.logger.log("getGameInfo: parameter missing", core.logger.severity.ERROR);
            return;
        }
        var p = {
            "f": "getgameinfo",
            "id": gameId
        };
        
        self.ajaxRequest(p, function (data) {
            data["Id"] = gameId;
            data["Fun"] = fun;
            data["ExtraParams"] = extraParams;
            core.publish("got-game-info", data);
        });
    };
    
    /**
    * Claims the prize in our system, (it's actually a prizePlayerId)
    * @param e
    * @param prizeId    
    */
    
    self.claimPrize = function (e,prizeId) {
        var p = {
            "f": "claimPrize",
            "p": prizeId
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("on-claimed-prize", data);
        });
    };    
    
    /**
    * Submits a potential player data to be stored
    * @para e
    * @param email
    */
    
    self.submitPotentialPlayer = function (e, email, wantsEmail) {
        var p = {
            "f": "submitPotentialPlayer",
            "e": email,
            "we": wantsEmail
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("on-potential-player-submited", data);
        });
    };
    
    /**
    * Gets Aspers Customer Number of the VIP player
    * @para e
    * @param promoCode the promo code given to player on the DM
    * @param dateOfBirth the players date of birth
    */
    
    self.getCustomerNumber = function (e, promoCode, dateOfBirth) {
        var p = {
            "f": "getCustomerNumber",
            "p": promoCode,
            "d": dateOfBirth
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-customer-number", data);
        });
    };
    
    /**
    * Gets Aspers player data from IG
    * @para e
    * @param customer ID the players Aspers Customer Number
    */
    
    self.getCustomer = function (e, customerNumber) {
        var p = {
            "f": "getCustomer",
            "n": customerNumber
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-customer", data);
        });
    };
    
    // get terms and conditions / privacy policy
    self.getTcsPP = function (e, session) {
        var p = {
            "f": "getAllTermsForPlayer",
            "s": session
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-tcs-pp", data);
        });
    };
    
    // player accepts terms and conditions / privacy policy
    self.tcsPPAccept = function (e, tcs, pp) {
        var p = {
            "f": "playerAcceptedTerms", 
            "c": ""
        };
        
        if (tcs) p.c = tcs;
        if (pp) p.c = p.c ? p.c + "," + pp : pp;
        
        self.ajaxRequest(p, function (data) {
            core.publish("tcs-pp-accepted", data);
        });
    };
    
    // player accepts terms and conditions upon registration
    self.tcsRegisterAcept = function (e, playerId, tcs) {
        var p = {
            "f": "playerAcceptedTermsUponRegistration", 
            "p": playerId,
            "c": tcs
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("tcs-register-accepted", data);
        });
    };
    
    // get player details from microservice
    self.getPlayerEntity = function () {
        var p = {
            "f": "getPlayerEntity"
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("got-player-entity", data);
        });
    };
    
    self.dontMissOutUpdate = function (e, wantsEmail, wantsSms, wantsPhoneCalls) {
        var p = {
            "f": "updatePlayerCommsFromPopUp",
            "tpemails": wantsEmail,
            "tpsms": wantsSms,
            "tpc": wantsPhoneCalls
        };
        
        self.ajaxRequest(p, function (data) {
            core.publish("dont-miss-out-updated", data);
        });  
    };
    
    self.getAspersTCPP = function (e, tSession, playerID) {
        var p = {
            f: "getAspersTermsAndConditions"
        };
        
        self.ajaxRequest(p, function (data) {
            data.ts=tSession;
            data.tp=playerID;
            core.publish("got-aspers-tc-pp", data);
            core.publish("show-terms-popup", [tSession, playerID, true]);
        }, false);
        
    };
    
    self.getAspersDoFirstLogin = function (
        e,
        playerID,
        wantsEmail,
        wantsSMS,
        wantsPhone,
        wantsMail,
        wantsData,
        wantsTPEmail,
        wantsTPSMS,
        wantsTPPhone,
        aspersCustomerNumber
        ) {
            var p = {
                f: "doAspersFirstLogin",
                
                we: wantsEmail,
                ws: wantsSMS,
                wp: wantsPhone,
                wpm: wantsMail,
                
                wte: wantsTPEmail,
                wts: wantsTPSMS,
                wtp: wantsTPPhone,
                
                pID: playerID,
                acn: aspersCustomerNumber,
                
                wd: wantsData,
            };
            
            
            
            self.ajaxRequest(p, function (data) {
            }, false);
            
        };
        /* AGE ID Verification start */
        /* Check if the player is verified */
        self.onAgeIdVerificationCheckIfVerified = function (e, verificationStatus, data, login){
            var type = "";
            var title = "";
            var message = "";
            switch(verificationStatus){
                case "0" : 
                type = "not-verified";
                title = "Identity Verification";
                message = "<p>As required by UK regulations we need to verify your identity before you can play our games. Please select any payment method on the next page to start the verification.</p>";
                break;
                case "2" : 
                type = "pending-documents";
                title = "Identity Verification";
                message = "<p>Sorry we have been unable to verify your identity.</p><p>Select ‘Continue’ to be taken to a secure area where you can quickly and safely upload the following documents:</p><ul><li>Passport and/or Identity card</li><li>AND a utility bill</li></ul>";
                break;
                case "3" : 
                type = "awaiting-response";
                title = "Identity Verification";
                message = "<p>To access our games and cashier, we need to verify your identity.</p><p>Please bear with us</p>";
                break;
            };
            if(login){
                core.publish("process-successful-login",data);
                // If status == 3 we have to check documents
                if(verificationStatus == 3){   
                    core.publish("player-verification-cas",[core.storage.getCookie("PlayerID"), core.storage.getCookie("SessionID")]);
                }
            }
            else{
                core.publish("show-notification-uru-check",[type, title, message]);
            }
        }
        // When we first load the page we need to show or hide recaptcha
        this.onRecaptchaReady = function(){
            
            // Check route and show or hide recaptcha badge accordingly
            var url = window.location.pathname;
            if( url.indexOf("/login/") > -1 ){
                core.$(".grecaptcha-badge").css("visibility","visible");
            }
            else{
                core.$(".grecaptcha-badge").css("visibility","hidden");
            }
        }
        
        return self;
        
    };