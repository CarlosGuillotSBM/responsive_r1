var sbm = sbm || {};

/**
 * Storage manager
 * @param core
 * @returns {sbm.Storage}
 * @constructor
 */
sbm.Storage = function (core) {
    "use strict";

    var self = this;
    var SESSION = "SessionID";
    var PLAYER = "PlayerID";
    var CURRSYMBOL = "CurrencySymbol";
    var REGISTERED = "Registered";
    var COOKIETRACKGUID = "CookieTrackGUID";
    var secureCookies = sbm.serverconfig.ip === "127.0.0.1" ? false : true;

    /**
     * Initialization
     */
    self.init = function () {

        if (!core.$.removeCookie) {
            core.$.removeCookie = function (key, options) {
                if (core.$.cookie(key) !== undefined) {
                    // Must not alter options, thus extending a fresh object...
                    core.$.cookie(key, '', core.$.extend({}, options, { expires: -1 }));
                    return true;
                }
                return false;
            };
        }

        if (!window.localStorage) {

            core.publish("show-notification-error", ["Your browser is not supported, please update it to a newer version", "alert"]);
            return;

        }

        if (self.get("languageCode") === null || self.get("currencySymbol") === null) {

            self.clearLocalStorageExcludingOtherLevelInfo();
            window.sessionStorage.clear();
            self.storeCookies();
            core.logger.log("Cookies were stored in local storage", core.logger.severity.INFO);
        }

    };

    self.clearLocalStorageExcludingOtherLevelInfo = function() {
        var arr = []; // Array to hold the keys
        for (var i = 0; i < window.localStorage.length; i++){
            if (window.localStorage.key(i).substring(0,3) != 'OL_') {
                arr.push(window.localStorage.key(i));
            }
        }
        for (var i = 0; i < arr.length; i++) {
            window.localStorage.removeItem(arr[i]);
        }
    };

    /**
     * Save data on cookies
     */
    self.storeCookies = function () {

        self.set("languageCode", core.$.cookie("LanguageCode") || "en");
        self.set("countryCode", core.$.cookie("CountryCode") || "");
        self.set("currencyCode", core.$.cookie("CurrencyCode") || "");
        self.set("currencySymbol", core.$.cookie("CurrencySymbol") || "");
        self.set("centsSymbol", core.$.cookie("CentsSymbol") || "");
        self.set("aff", core.$.cookie("Aff") || "");
        self.set("httpReferer", core.$.cookie("HTTPReferer") || "");

        // clear all cookies except LanguageCode
        self.clearCookies(["CountryCode", "CurrencyCode", "CurrencySymbol",
            "CentsSymbol", "Aff", "HTTPReferer"]);

    };

    /**
     * receives an array with the cookies key
     * @param keys
     */
    self.clearCookies = function (keys) {

        if (!core.$.removeCookie) return;

        core._.each(keys, function (key) {
            core.$.removeCookie(key, { path: '/' });
        });

    };

    /**
     * Save data
     * @param key
     * @param value
     * @param session
     */
    self.set = function (key, value, session) {
        if (session) {
            if (key === "sessionId") {
                core.$.cookie(SESSION, value, {path: '/', secure: secureCookies});
            }
            if (key === "playerId") {
                core.$.cookie(PLAYER, value, {path: '/', secure: secureCookies});
            }
            if (key === "Registered") {
                core.$.cookie(REGISTERED, value, {path: '/', secure: secureCookies});
            }
        } else {
            try {
                window.localStorage.setItem(key, value);
            }catch(e){
                core.googleManager.pushToDataLayer("Safari Incognito Mode," + navigator.userAgent);
            }
            if (key === "currencySymbol") {
                // update the cookie set on server side originally
                core.$.cookie(CURRSYMBOL, value, {path: '/', secure: secureCookies});
            }
        }
    };

    /**
     * Get data
     * @param key
     * @returns {*}
     */
    self.get = function (key) {

        // TODO: remove this as soon we get rid of the old sites
        if (window.globalOldSkin && key === "currencySymbol") {
            return self.getCurrSymbolForOldSkins();
        }

        if (key === "sessionId") {
            return core.$.cookie(SESSION);
        }
        if (key === "playerId") {
            return core.$.cookie(PLAYER);
        }
        if (key === "cookieTrackGuid") {
            core.$.cookie(COOKIETRACKGUID);
        }
        return window.localStorage.getItem(key) || window.sessionStorage.getItem(key);
    };

    /**
     * Remove from storage
     * @param key
     */
    self.remove = function (key) {
        if (key === "sessionId") {
            core.$.removeCookie(SESSION);
        }
        if (key === "playerId") {
            core.$.removeCookie(PLAYER);
        }
        window.localStorage.removeItem(key);
        window.sessionStorage.removeItem(key);
    };

    self.getCurrSymbolForOldSkins = function(){
        var currencySymbol = core.$.cookie('Currency');
        var currSymbol = '';
        switch (currencySymbol) {
            case "GBP":
                currSymbol = '£';
                break;
            case "EUR":
                currSymbol = '€';
                break;
            case "AUD":
            case "CAD":
                currSymbol = '$';
                break;
            default:
                currSymbol = '';
                break;
        }
        return currSymbol;
    };


    /**
    * sets a cookie
    */
    self.setCookie = function (key, value,session) {
        var params = { path: '/', secure: secureCookies};
        if (key === "cookieTrackGuid") {
            core.$.cookie(COOKIETRACKGUID, value, params);
        } else 
        if (!session) {
            params.expires = 20*365;
            core.$.cookie(key, value, params);
        }

    }

    /**
    * return the value of the cookie
    */
    self.getCookie = function (key) {
        var value;            
        if (key === "cookieTrackGuid") {
            value = core.$.cookie(COOKIETRACKGUID);
        } else {
            value = core.$.cookie(key);
        }
        return value;
    }    
    return self;
}