var sbm = sbm || {};

/**
* Class responsible for all the AJAX requests for the Prize Wheel
* @param core Instance of the core class
* @returns {DataservicePrizeWheel}
* @constructor
*/
sbm.DataservicePrizeWheel = function (core) {
    "use strict";
    
    var self = this;
    /**
    * initialization
    */
    // extends from base Dataservice class
    core._.extend(self, new sbm.Dataservice(core, self.url, "json", 60000));
    self.init = function () {
        // subscribe event when player has logged in
        core.logger.log("Prize Wheel Dataservice init", core.logger.severity.INFO);
        core.subscribe("prize-wheel-check-for-prizes", self.checkForPrizes);
        core.subscribe("prize-wheel-get-player-prizes", self.getPlayerPrizes);
    };
    
    /**
    * Check if the player has a prize
    * @param e Event that called this function
    * @param user The user to check
    */
    
    /**
    * Check if the player has a prize
    * @param e Event that called this function
    * @param user The user to check
    */
    self.checkForPrizes = function (e) {
        var Session = core.storage.getCookie("SessionID");
        if(Session !== undefined && Session !== null){
            var p = {
                f: "prizewheel",
            };
            self.ajaxRequest(p, function (data) {
                core.publish("prize-wheel-got-prize-info", [data]);
            });
        }
    };
    
    self.getPlayerPrizes = function(e){
        var Session = core.storage.getCookie("SessionID");
        if(Session !== undefined && Session !== null){
            var playerSpins = core.storage.getCookie("PWSpins");
            var data = {Code: 500, Spins: 0};
            if(typeof(playerSpins) !== "undefined"){
                data.Code = 0;
                data.Spins = parseInt(playerSpins);
                core.publish("prize-wheel-got-prize-info", [data]);
            }
            else{
                core.publish("prize-wheel-check-for-prizes", []);
            }
        }
    }
    
    return self;
};