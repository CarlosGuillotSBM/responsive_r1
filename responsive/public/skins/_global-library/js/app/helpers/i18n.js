/**
 * Internationalization support
 */
var sbm = sbm || {};

sbm.i18n = {
  /**
   * English
   */

  en: {
    conversionSuccessful: "Conversion was successful",
    checkEmailWithInstructions:
      "Thanks, please check your email for further instructions",
    passwordChanged: "Your password was successfully changed!",
    needsToBeLoggedInToSetFav:
      "Sorry you need to be logged in to set the game as favourite",
    updatedRecords: "Thank you, we've updated your records",
    gameNoAvailableOnMobile: "This game is not available for Mobile",
    activeBonus:
      "You have an active bonus worth %s%s. All or part of this may be removed if you submit a withdrawal.",
    reenterDetails:
      "Due to security reasons we require you to re-enter your card details",
    expiryDateChanged: "Your expiry date has been successfully modified",
    invalidAmount: "Invalid Amount",
    invalidCvn: "Invalid CVN",
    invalidPromotion: "Invalid Promotion",
    invalidCvnRetry: "CVN is not valid, please retry",
    detailsChanged: "Your details have been successfully saved",
    invalidExpiryDate: "Expiry date is not valid",
    accountReachedLimit:
      "you have reached your %s limits or house limits, please contact support",
    cardHasExpired:
      "Please update your card expiry date or use a different payment method",
    roomClosed: "%s is closed",
    roomWillOpenSoon: "Room will open soon",
    soon: "Soon",
    quickDepNotAvailable:
      "Quick Deposit does not support PayPal. Please visit the <a href='/cashier/'>cashier</a>.",
    quickDepNoAccount:
      "No registered payment method found. Please visit the cashier.",
    quickDepNoDefaultAccount:
      "Quick Deposit does not support the registered payment method. Please visit the <a href='/cashier/'>cashier</a>.",
    noBonusForMe: "No Bonus For Me",
    ifRequireWithdrawal: "If you require a withdrawal please email",
    invalidNetellerSecureId:
      "Invalid Neteller Secure ID or Authentication Code",
    updatedLimits:
      "Thank you, you will need to confirm your limits in your next login after the next 24h.",
    invalidPromoCode: "Invalid promo code",
    isValidPromoCode: "The promo code is valid",
    redeemSuccess: "You have redeemed successfully your promo code",
    resendSuccess: "The verification code was sent to your mobile phone",
    invalidCode: "Invalid code",
    mobileVerified: "Thank you, your mobile phone has been verified",
    loginToOptIn: "Please log in to Opt-In",
    limitConfirmation: "Wold you like to confirm your new limits?",
    voidBonusConfirmation:
      "The following amounts will be removed from your account:<br/>Bonus:%s<br/>Bonus wins:%s",
    limitConfirmationRemaining:
      "You'll need to confirm your limit changes in %s hours %s minutes %s seconds",
    limitToAccept:
      "You have a limit change pending. Please login again in order to accept or cancel it.",
    "level-1": "",
    level0: "Newbie",
    level1: "Bronze",
    level2: "Silver",
    level3: "Gold",
    level4: "Ruby",
    level5: "Emerald",
    level6: "Elite",
    permission_0: "Any player can Enter",
    permission_1: "Funded Players only",
    permission_2:
      "Just players who have deposited in the last 30 days can enter",
    permission_3:
      "Just players who have deposited in the last 7 days can enter",
    permission_4: "Just player who have deposited today can enter",
    permission_5: "You need to have at least silver VIP level to enter",
    permission_6: "You need to have at least gold VIP level to enter",
    permission_7: "You need to have at least ruby VIP level to enter",
    permission_9:
      "Just players funded in the last 30 days and at least VIP Level Bronze",
    permission_10:
      "Just players funded in the last 30 days and at least VIP Level Silver",
    permission_11:
      "Just players funded in the last 30 days and at least VIP Level Gold",
    permission_12:
      "Just players funded in the last 30 days and at least VIP Level Ruby",
    permission_13:
      "Just players funded in the last 30 days and at least VIP Level Emerald",
    permission_14:
      "Just players funded in the last 7 days and at least VIP Level Bronze",
    permission_15:
      "Just players funded in the last 7 days and at least VIP Level Silver",
    permission_16:
      "Just players funded in the last 7 days and at least VIP Level Gold",
    permission_17:
      "Just players funded in the last 7 days and at least VIP Level Ruby",
    permission_18:
      "Just players funded in the last 7 days and at least VIP Level Emerald",
    permission_19: "Just players funded today and at least VIP Level Bronze",
    permission_20: "Just players funded today and at least VIP Level Silver",
    permission_21: "Just players funded today and at least VIP Level Gold",
    permission_22: "Just players funded today and at least VIP Level Ruby",
    permission_23: "Just players funded today and at least VIP Level Emerald",
    permission_withAmount_9:
      "You must be a Bronze player or above and have deposited at least %s in the last 30 days",
    permission_withAmount_10:
      "You must be a Silver player or above and have deposited at least %s in the last 30 days",
    permission_withAmount_11:
      "You must be a Gold player or above and have deposited at least %s in the last 30 days",
    permission_withAmount_12:
      "You must be a Ruby player or above and have deposited at least %s in the last 30 days",
    permission_withAmount_13:
      "You must be a Emerald player or above and have deposited at least %s in the last 30 days",
    permission_withAmount_14:
      "You must be a Bronze player or above and have deposited at least %s in the last 7 days",
    permission_withAmount_15:
      "You must be a Silver player or above and have deposited at least %s in the last 7 days",
    permission_withAmount_16:
      "You must be a Gold player or above and have deposited at least %s in the last 7 days",
    permission_withAmount_17:
      "You must be a Ruby player or above and have deposited at least %s in the last 7 days",
    permission_withAmount_18:
      "You must be a Emerald player or above and have deposited at least %s in the last 7 days",
    permission_withAmount_19:
      "You must be a Bronze player or above and have deposited at least %s today",
    permission_withAmount_20:
      "You must be a Silver player or above and have deposited at least %s today",
    permission_withAmount_21:
      "You must be a Gold player or above and have deposited at least %s today",
    permission_withAmount_22:
      "You must be a Ruby player or above and have deposited at least %s today",
    permission_withAmount_23:
      "You must be a Emerald player or above and have deposited at least %s today",
    permission_24: "Just funded players and registered in Last 7 days",
    permission_25: "Just funded players and registered in Last 2 days",
    permission_26: "Just players who registered in the last 7 days can enter",
    permission_27: "Just players who registered in the last 2 days can enter",
    permission_99: "Unfunded players only",
    permission_100: "Private Room",
    acknowledgeFirst: "Please acknowledge funds protection to deposit",
    requestLimitChange: "Request Limit Change",
    depositLimitsInvalid: "The limits are not valid",
    requestChangeFinished: "Your limits have been requested successfully",
    confirm: "Confirm",
    deposit: "OK",
    invalidMailReason_1: "You have already invited this friend, thanks!",
    invalidMailReason_2:
      "Sorry, we have been unable to send to this email address. Please try another email address",
    invalidMailReason_3: "Sorry, this email address in unreachable",
    invalidMailReason_5:
      "Please wait 2 weeks before trying to remind your friend you can only remind them once",
    confirmIncreaseNextLimits:
      "You have chosen to set a limit that is higher than one or more of your other chosen limits, eg monthly. If you proceed, your other limit(s) will be increased accordingly",
    addedToFavourites: "ADDED TO FAVOURITES",
    removedFromFavourites: "REMOVED FROM FAVOURITES",
    roomOnlyForFunded:
      "This room is for funded players only. To unlock it, just top up your account in the cashier."
  },

  el: {
    activeBonus:
      "Εχετε ένα ενεργό μπόνους αξίας %s%s. Αυτό θα αφαιρεθεί εαν ζητήσετε ανάληψη.",
    reenterDetails:
      "Για λόγους ασφαλείας θα χρειαστεί να εισάγετε πάλι τα στοιχεία της κάρτας σας",
    invalidCvnRetry: "Το τριψήφιο CVN δεν είναι έγκυρο, δοκιμάστε ξανά",
    expiryDateChanged: "Η ημερομηνία λήξης έχει τροποποιηθεί με επιτυχία",
    invalidAmount: "Μη έγκυρο ποσό",
    invalidCvn: "Μη έγκυρο CVN",
    invalidPromotion: "Μη έγκυρη προσφορά",
    invalidPromoCode: "Μη έγκυρος κωδικός προσφοράς",
    invalidNetellerSecureId:
      "Μη έγκυρο Neteller Secure ID ή Κωδικός Επιβεβαίωσης",
    ifRequireWithdrawal: "Εαν επιθυμείτε ανάληψη παρακαλώ στείλτε email",
    detailsChanged: "Επιτυχής αποθήκευση στοιχείων",
    invalidExpiryDate: "Η ημερομηνία λήξης δεν είναι έγκυρη",
    accountReachedLimit: "Ο λογαριασμός έφτασε στο όριό του",
    cardHasExpired:
      "Παρακαλούμε ανανεώστε την ημερομηνία λήξης ή χρησιμοποιήστε διαφορετική μέθοδο",
    isValidPromoCode: "Ο κωδικός προσφοράς είναι έγκυρος",
    acknowledgeFirst:
      "Παρακαλούμε αποδεχτείτε το επίπεδο προστασίας για να καταθέσετε",
    requestLimitChange: "Αίτημα Αλλαγής Ορίων",
    depositLimitsInvalid: "Τα όρια δεν είναι έγκυρα",
    requestChangeFinished: "Το αίτημα για τα όρια έχει τεθεί επιτυχώς",
    confirm: "Επιβεβαίωση",
    deposit: "OK",
    playerLimits: "Όρια Παίκτη",
    UKGCAcknowledge:
      "Αποδέχομαι πως τα χρήματά που καταθέτω προστατεύονται απο τις ελεγκτικές αρχές στο δεύτερο επίπεδο προστασίας"
  },

  /**
   * Helper method to get translation
   * @param key - the translation key
   * @param params - array with substitutions strings
   * @returns {string}
   */
  get: function(key, params) {
    var str = "";

    var lang = "en";

    if (params) {
      var i = 0;
      str = this[lang][key];
      for (i; i < params.length; i++) {
        str = str.replace("%s", params[i]);
      }
    } else {
      str = this[lang][key];
    }

    return str;
  }
};
