/**
 * Auxiliar code to add modules and plugins to the website
 */

var sbm = sbm || {};

// own properties
sbm.modules = [];
sbm.plugins = [];

/**
 * Adds item to collection
 * Prevents from adding duplicated items
 * @param type "plugins" / "modules"
 * @param item the object to be added
 */
sbm.add = function (type, item) {

    if (item.id !== 'Slick' && item.id !== 'magicalMenu'){
        var i = 0, exists = false;

        for (i; i < sbm[type].length ; i++) {
            if (sbm[type][i].id === item.id ) {
                exists = true;
                if (item.id === "BingoRoomDetailsPage") {
                    // TODO: remove this hack to update the room Id
                    sbm[type][i].options.roomId = item.options.roomId;
                }
            }
        }

        if (!exists) {
            sbm[type].push(item);
        }
    } else {
        
        var i = 0, exists = false, n = 0, needArray = false, found = 0;

        for (i; (i < sbm[type].length && !exists); i++) {
            if (sbm[type][i].id == item.id) {
                //alert(' iteration: '+ sbm[type][i].id + ' item: ' + item.id);
                needArray = true;
                found = i;
               
                if (!sbm[type][i].options || !Array.isArray(sbm[type][i].options)) {
                    if ((!sbm[type][i].options && !item.options) 
                        ||  (!sbm[type][i].options.element && !item.options.element)
                        || sbm[type][i].options.element === item.options.element) {
                            exists = true;
                            if (item.id === "BingoRoomDetailsPage") {
                                // TODO: remove this hack to update the room Id
                                sbm[type][i].options.roomId = item.options.roomId;
                            }
                    }
                } else {
                    needArray = true;
                    n = 0; 
                    for (n; (n < sbm[type][i].options.length && !exists); n++) {
                        if ((!sbm[type][i].options[n] == [] && !item.options) 
                            ||  (!sbm[type][i].options[n].element && !item.options.element)
                            || sbm[type][i].options[n].element === item.options.element) {
                                exists = true;
                                if (item.id === "BingoRoomDetailsPage") {
                                    // TODO: remove this hack to update the room Id
                                    sbm[type][i].options[n].roomId = item.options.roomId;
                                }
                        }
                    }
                }
            }
        }

        if (!exists && !needArray) {
            sbm[type].push(item);
        } else if (!exists) {
            var newOptions = sbm[type][found].options?sbm[type][found].options:[];        
            if (!Array.isArray(newOptions)) {
                sbm[type][found].options = [];
                sbm[type][found].options.push(newOptions);
            } if (!Array.isArray(item.options )){
                sbm[type][found].options.push(item.options);
            }
        }
    }

};

sbm.addModule = function (module) {
    sbm.add("modules", module);
};

sbm.addPlugin = function (plugin) {
    sbm.add("plugins", plugin);
};

sbm.resetModules = function () {
    sbm.modules = [];
};

sbm.resetPlugins = function () {
    sbm.plugins = [];
};