var sbm = sbm || {};

/**
 * Player forgot password
 * @param sb The sanbox
 * @constructor
 */
sbm.ForgotPassword = function (sb) {
    "use strict";

    // fields
    this.forgot = sb.ko.observable(true);
    this.recover = sb.ko.observable(false);
    this.email = sb.ko.observable("");
    this.newPassword = sb.ko.observable("");
    this.repeatPassword = sb.ko.observable("");
    // errors
    this.invalidEmail = sb.ko.observable(false);
    this.passwordsDoNotMatch = sb.ko.observable(false);
    this.invalidNewPassword = sb.ko.observable("");
    this.passwordsAreEmpty = sb.ko.observable("");
    this.passwordsDoNotMatch = sb.ko.observable("");
    // other
    this.GUI = "";
    this.playerId = "";

    /**
     * Module initialization
     */
    this.init = function () {

        sb.listen("forgot-password-finished", this.onForgotPasswordFinished);
        sb.listen("reset-password-finished", this.onResetPasswordFinished);

        var guid = sb.getUrlParameter("GUID");
        var playerId = sb.getUrlParameter("PlayerID");

        if (guid && playerId) {
            this.GUI = guid;
            this.playerId = playerId;
            this.setEnterNewPasswordForm();
        }

    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("forgot-password-finished");
        sb.ignore("reset-password-finished");
    };

    /**
     * Player chose to recover password
     */
    this.recoverPassword = function () {
        if (this.validEmail()) {
            this.invalidEmail(false);
            sb.notify("forgot-password", [this.email]);
        } else {
            this.invalidEmail(true);
        }
    }.bind(this);

    /**
     * Validates email
     * @returns {boolean}
     */
    this.validEmail = function () {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(this.email());
    };

    /**
     * Forgot password request has finished
     * Handles response
     */
    this.onForgotPasswordFinished = function (e, data) {
        if (data.Code === 0) {
            this.email("");
            sb.showConfirm(sb.i18n("checkEmailWithInstructions"), this.goToHomePage, null, " ");
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Recover password
     * Show new password form
     */
    this.setEnterNewPasswordForm = function () {

        history.replaceState({}, 'Forgot Password', window.location.pathname);
        this.forgot(false);
        this.recover(true);

    };

    /**
     * Player selected to reset the password
     * Validates the passwords and request to change it
     */
    this.resetPassword = function () {

        this.passwordsAreEmpty(false);
        this.passwordsDoNotMatch(false);
        this.invalidNewPassword(false);

        var newPassword = this.newPassword();
        var repeatPassword = this.repeatPassword();

        if (newPassword === "" || repeatPassword === "") {
            this.passwordsAreEmpty(true);
            return;
        }

        if (newPassword !== "" && repeatPassword !== "" && newPassword !== repeatPassword) {
            this.passwordsDoNotMatch(true);
            return;
        }

        if (newPassword !== "" && repeatPassword !== "" && !this.validPassword()) {
            this.invalidNewPassword(true);
            return;
        }

        sb.notify("reset-password", [this.playerId, this.GUI, newPassword]);
        this.passwordsAreEmpty(false);
        this.passwordsDoNotMatch(false);
        this.invalidNewPassword(false);

    }.bind(this);

    /**
     * Validates password
     * @returns {boolean}
     */
    this.validPassword = function () {

        var pattern = new RegExp(/^(?=.*?\d)(\w|[!@\^#\$%\&\*])+$/i);
        return pattern.test(this.newPassword());

    };

    /**
     * Reset password request has finished
     * Handles response
     */
    this.onResetPasswordFinished = function (e, data) {

        if (data.Code === 0) {

            this.newPassword("");
            this.repeatPassword("");

            sb.showConfirm(sb.i18n("passwordChanged"), this.goToHomePage, null, " ");
        } else {
            data.Msg && sb.showError(data.Msg);
        }

    }.bind(this);

    /**
     * Go to the home page in the notification pop up
     */
    this.goToHomePage = function () {
        sb.initRoute("/");
    }
};
