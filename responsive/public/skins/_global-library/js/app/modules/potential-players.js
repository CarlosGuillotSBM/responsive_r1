var sbm = sbm || {};

/**
* Get details of potential players
* Validate player information and submits it to be stored on database
* @param sb The Sandbox
* @param options JSON object with any options for the viewmodel
* @returns {PotentialPlayers} viewmodel PotentialPlayers
*/
sbm.PotentialPlayers = function (sb, options) {

	// properties

	this.email = sb.ko.observable("");
	this.invalidEmail = sb.ko.observable(false);
	this.over18 = sb.ko.observable(false);
	this.invalidOver18 = sb.ko.observable(false);
	this.wantsOffers = sb.ko.observable(true);

	// module initialization
	this.init = function () {
		sb.listen("on-potential-player-submited", this.onPlayerSubmited);
	};

	// submit details to database
	this.submit = function () {

		// validation
	
		var validEmail = this.isValidEmail(),
			validOver18 = this.over18(),
			valid = false;

		valid = validEmail && validOver18;

		// if valid - submit information to back-end

		if (valid) sb.notify("submit-potential-player", [this.email(), this.wantsOffers()]);

		// show/hide errors
		this.invalidEmail(!validEmail);
		this.invalidOver18(!validOver18);
		
	}.bind(this);

	// email validation
	this.isValidEmail = function () {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(this.email());
	}

	// response from dataservice
	this.onPlayerSubmited = function (e, data) {

		if (data.Code === 0) {
            alert("Thank you for providing your details, we will inform you when we launch.")
        } else {
            alert("Oops... something went wrong. Please try again later.")
        }

	}.bind(this);
};