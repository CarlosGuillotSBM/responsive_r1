var sbm = sbm || {};

/**
 * Game's editor for the website
 * @param sb The sandbox
 * @constructor
 * Author: Nuno Oliveira
 */
sbm.GamesEditor = function (sb) {
    "use strict";

    /**
     * subscriptions ###################################################################################################
     */

    /**
     * searched game id has changed
     */
    this.onSearchGameIdChanged = function (id) {
        if (id) {
            sb.notify("editor-get-games", [id, ""]);
        } else {
            this.gameToBeAdded("");
        }
    }.bind(this);

    /**
     * selected a skin from the dropdown
     */
    this.onSelectedSkin = function (skinId) {
        // get categories
        skinId > 0 && sb.notify("editor-get-categories-for-skin", [skinId]);
    }.bind(this);

    /**
     * Properties ######################################################################################################
     */

    // update game screen

    this.addingGames = sb.ko.observable(true);
    this.orderingGames = sb.ko.observable(false);
    this.searchGameId = sb.ko.observable("").extend({ rateLimit: 1000 });
    this.searchGameId.subscribe(this.onSearchGameIdChanged);
    this.gameToBeAdded = sb.ko.observable("");
    this.gamesFound = [];
    this.gameCategorySkins = sb.ko.observableArray("");
    this.loginVisible = sb.ko.observable(false);
    this.validSession = sb.ko.observable(false);
    this.username = sb.ko.observable("");
    this.password = sb.ko.observable("");
    this.devicesRow = sb.ko.observableArray([]);
    this.categories = sb.ko.observableArray([]);
    this.skinId = null;
    this.skinName = sb.ko.observable("");

    // order games screen

    this.selectedSkin = sb.ko.observable(-1);
    this.selectedSkin.subscribe(this.onSelectedSkin);
    this.returnedCategories = sb.ko.observableArray([]);
    this.selectedCategory = sb.ko.observable("");
    this.selectedDevice = sb.ko.observable("");
    this.gamesToOrder = sb.ko.observableArray([]);

    /**
     * Functions for Update Game #######################################################################################
     */

    /**
     * Module initialization
     */
    this.init = function () {

        this.setupSearchBox();

        sb.listen("editor-done-login", this.onDoneLogin);
        sb.listen("editor-got-game-category-skins", this.onGotGameCategorySkins);
        sb.listen("admin-session-has-expired", this.clearSession);
        sb.listen("editor-added-game-to-skins", this.onAddedGameToSkins);
        sb.listen("editor-got-categories-for-skin", this.onGotCategoriesForSkin);
        sb.listen("editor-got-games-for-category", this.onGotGamesForCategory);
        sb.listen("editor-updated-games-order", this.onUpdatedGamesOrder);
        sb.listen("editor-got-games", this.OnGotGames);
        sb.listen("editor-set-all-games-live-finished", this.onSetAllGamesLiveFinished);
        sb.listen("editor-set-games-live-finished", this.onSetGamesLiveFinished);
        sb.listen("editor-reverted-all-games", this.onRevertedAllGames);
        sb.listen("editor-reverted-games", this.onRevertedGames);
        sb.listen("editor-removed-games", this.onRemovedGames);
        sb.listen("editor-flushed-cache", this.onFlushedCache);

        if (sb.getCookie("AdminSessionID")) {
            this.validSession(true);
        } else {
            this.validSession(false);
            this.loginVisible(true);
        }

        sb.$(".games-list").sortable().disableSelection();

        this.skinId = sbm.serverconfig.skinId;
        this.skinName(this.getSkinName(this.skinId));
        this.selectedSkin(this.skinId);
    };

    /**
     * Module destroy
     */
    this.destroy = function () {
        sb.ignore("editor-done-login");
        sb.ignore("editor-got-game-category-skins");
        sb.ignore("admin-session-has-expired");
        sb.ignore("editor-added-game-to-skins");
        sb.ignore("editor-added-game-to-skins");
        sb.ignore("editor-got-categories-for-skin");
        sb.ignore("editor-got-games-for-category");
        sb.ignore("editor-updated-games-order");
        sb.ignore("editor-got-games");
        sb.ignore("editor-set-all-games-live-finished");
        sb.ignore("editor-set-games-live-finished");
        sb.ignore("editor-removed-games");
        sb.ignore("editor-flushed-cache");
    };

    /**
     * Search game input - setup autocomplete plugin
     */
    this.setupSearchBox = function () {

        sb.$(".ui-game-search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/admindata.php",
                    dataType: "json",
                    data: {
                        f: "searchgames",
                        id: "",
                        name: request.term
                    },
                    success: function (data) {

                        this.gamesFound.length = 0;

                        if (data.Code === 0) {

                            if (data.Games.length > 0) {

                                sb._.each(data.Games, function (game) {
                                    this.gamesFound.push({
                                        label: game.name,
                                        value: game.name,
                                        gameId: game.ID
                                    });
                                }.bind(this));

                            } else {
                                sb.notifyInfo("No games were found");
                            }

                        } else {
                            data.Msg && sb.showError(data.Msg);
                        }

                        response(this.gamesFound);

                    }.bind(this)
                });
            }.bind(this),
            minLength: 3,
            delay: 500,
            select: function (e, ui) {
                this.selectedGame(ui.item.gameId, ui.item.value);
            }.bind(this)
        });

    };

    /**
     * Clears the session
     */
    this.clearSession = function () {
        this.loginVisible(true);
        this.validSession(false);
        sb.removeFromStorage("sessionId");
    }.bind(this);

    /**
     * Login request
     */
    this.doLogin = function () {
        sb.notify("editor-do-login", [this.username(), this.password()]);
    }.bind(this);

    /**
     * Login response
     */
    this.onDoneLogin = function (e, data) {
        if (data.Code === 0) {

            this.loginVisible(false);
            this.validSession(true);
            this.showSection("add-games");

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Game selection from auto complete
     * @param id - Game Id
     * @param title - Game Title
     */
    this.selectedGame = function (id, title) {
        this.gameToBeAdded({
            id: id,
            title: title
        });
        this.getGameCategorySkins(id);
    };

    /**
     * Get all categories and skins for the selected game
     * @param data - data from AJAX response
     */
    this.onGotGameCategorySkins = function (e, data) {
        if (data.Code === 0) {

            this.rearrangeDataForDisplay(data);

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Rearrange the data for display
     * @param data - all categories and skins for the selected game got previously
     */
    this.rearrangeDataForDisplay = function (data) {

        // add row with caption for devices
        var i, row = [{ label: "Categories" }], checkboxes = [];
        for (i = 0; i < data.Skins.length; i++) {
            row.push({ label: "W M" });
        }

        // add new cell for category column
        data.Skins.unshift({ id: 0, name: "" });

        this.devicesRow(row);

        // mark parent categories
        for (i = 1; i < data.Categories.length; i++) { // starts in the 2nd position

            // check that the current category is a child and the previous category is a parent
            if (data.Categories[i].depth > 0 && data.Categories[i - 1].depth !== data.Categories[i].depth) {
                data.Categories[i - 1].parent = true;
            } else {
                data.Categories[i - 1].parent = false;
            }

            data.Categories[i].parent = false;
        }

        this.categories(data.Categories);
        this.gameCategorySkins(data.Skins);

        // add checkboxes
        var tds = sb.$("#ui-add-game-to-skins tbody tr td");
        sb._.each(tds, function (td) {
            for (i = 0; i < data.Skins.length - 1; i++) {
                sb.$(td).after("<td><input type='checkbox'><input type='checkbox'></td>");
            }
        });

        this.setCheckboxes(data.SkinCat);
    };

    /**
     * Add data to all checkboxes and check/uncheck properly
     * @param skinCategories
     */
    this.setCheckboxes = function (skinCategories) {

        var $row, colIndex, $checkboxes, $checkWeb, $checkMobile;

        sb._.each(skinCategories, function (skinCat) {

            // get row for the category
            $row = sb.$("#ui-add-game-to-skins td[data-catid=" + skinCat.CatID + "]").parent();

            // get column index for the skin
            colIndex = sb.$("#ui-add-game-to-skins th[data-skinid=" + skinCat.SkinID + "]").index();

            // get all 3 slots (web, mobile)
            $checkboxes = $row.find("td:eq(" + colIndex + ")").children();

            // set data attributes with skin and category on each checkbox

            $checkWeb = $checkboxes.eq(0);
            $checkMobile = $checkboxes.eq(1);

            $checkWeb.data("device", "web").data("catid", skinCat.CatID).data("skinid", skinCat.SkinID);
            $checkMobile.data("device", "mobile").data("catid", skinCat.CatID).data("skinid", skinCat.SkinID);

            // checks and sets availability

            if (!skinCat.Web) {
                $checkWeb.addClass("not-available");
            }

            if (!skinCat.Mobile) {
                $checkMobile.addClass("not-available");
            }

            if (skinCat.WebAlready) {
                $checkboxes.first().prop("checked", "checked");
            }

            if (skinCat.MobileAlready) {
                $checkboxes.eq(1).prop("checked", "checked");
            }

        });
    };

    /**
     * Enter key when logging in
     */
    this.checkForEnterKey = function (data, e) {
        if (e.which == 13) {
            this.doLogin();
        } else {
            return true;
        }
    }.bind(this);

    /**
     * Show different sections of the page
     */
    this.showSection = function (section) {
        if (section === "add-games") {
            // change active screen
            this.orderingGames(false);
            this.addingGames(true);
        } else if (section === "order-games") {
            // clear the array of games to order
            this.gamesToOrder([]);
            // change active screen
            this.addingGames(false);
            this.orderingGames(true);
        }
    }.bind(this);

    /**
     * Preview website in the other window
     */
    this.preview = function () {
        if (navigator.appName.indexOf("Microsoft") !== -1) {
            window.editorWindow = window.open("http://responsive.magicalvegas.dev/slots/", "height=720,width=900,top=100,left=100,status=1");
        } else {
            window.editorWindow = window.open("http://responsive.magicalvegas.dev/slots/", "", "height=720,width=900,screenY=100,screenX=100");
        }
    }.bind(this);

    /**
     * Request for getting all categories and skins for the selected game
     * @param gameId
     */
    this.getGameCategorySkins = function (gameId) {
        sb.notify("editor-get-game-category-skins", [gameId]);
    };

    /**
     * Expand/Collapse parent categories
     */
    this.expandParentRow = function (vm, elem) {

        var $currentTarget = sb.$(elem.currentTarget);
        var parentDepth = $currentTarget.parent().data("depth");

        var nextRows = $currentTarget.parent().nextAll("tr");
        if (!nextRows.length) {
            return;
        }

        var depth, $row, i = 0, length = nextRows.length;
        var visibility = !(sb.$(nextRows[0]).is(":visible"));

        var cellText = $currentTarget.text();
        if (visibility) {
            $currentTarget.text(cellText.replace("+", "-", "gi"));
        } else {
            $currentTarget.text(cellText.replace("-", "+", "gi"));
        }

        for (i; i < length; i++) {

            $row = sb.$(nextRows[i]);
            depth = $row.data("depth");

            if (depth === parentDepth + 1) {
                if (visibility) {
                    $row.show();
                } else {
                    $row.hide();
                }
            } else {
                return;
            }
        }

    }.bind(this);

    /**
     * Add the game to skins categories
     */
    this.addGame = function () {

        // get all checked checkboxes
        var $checkboxes = sb.$("input[type=checkbox]:checked");

        // get information about which skins and categories should add the game
        var data = [];

        // get necessary data from each checkbox
        $checkboxes.each(function () {
            var cbData = $(this).data();
            data.push({
                "CatID": cbData.catid,
                "SkinID": cbData.skinid,
                "Web": cbData.device === "web" ? 1 : 0,
                "Mobile": cbData.device === "mobile" ? 1 : 0
            });
        });

        sb.notify("editor-add-game-to-skins", [this.gameToBeAdded().id, data]);

    }.bind(this);

    /**
     * Add games to skin request has ended
     * Handles response
     */
    this.onAddedGameToSkins = function (e, data) {

        if (data.Code === 0) {
            sb.notifySuccess("The game was successfully added");

        } else if (data.Code === 0) {
            sb.notifyWarning("No games to add");

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

    /**
     * Get Categories for skin request has ended
     * Handles response
     */
    this.onGotCategoriesForSkin = function (e, data) {

        if (data.Code === 0) {
            this.returnedCategories(data.Categories);
        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

    /**
     * Functions for Order Game ########################################################################################_
     */

    /**
     * Get the games for the filter selection
     */
    this.getGamesForCategory = function () {

        if (this.selectedCategory() && this.selectedDevice()) {
            sb.notify("editor-get-games-for-category", [this.selectedCategory(), this.selectedDevice(), this.selectedSkin()]);
        } else {
            sb.notifyWarning("Select Category and Device first");
        }

    }.bind(this);

    /**
     * got games from server
     */
    this.onGotGamesForCategory = function (e, data) {

        if (data.Code === 0) {

            this.gamesToOrder(data.Games);

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

    /**
     * update games order
     */
    this.updateGamesOrder = function () {

        var order = [];

        sb.$("#ui-games-to-order li").each(function () {
            order.push(sb.$(this).data("gameid"));
        });

        sb.notify("editor-update-games-order", [this.selectedCategory(), this.selectedDevice(), this.selectedSkin(), order]);

    }.bind(this);

    /**
     * update games order - response from server
     */
    this.onUpdatedGamesOrder = function (e, data) {

        if (data.Code === 0) {

            sb.notifySuccess("Games order updated");

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

    /**
     * Get games request has ended
     * Handles response
     */
    this.OnGotGames = function (e, data) {

        if (data.Code === 0) {

            if (data.Games.length) {

                this.selectedGame(data.Games[0].ID, data.Games[0].name);

            } else {
                sb.notifyInfo("Game not found");
            }

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

    /**
     * Syncs ALL games to Live
     */
    this.syncALL = function () {
        if (confirm("This will sync ALL categories and ALL devices for the selected skin.\n Are you sure you want to do this?")) {
            sb.notify("editor-set-all-games-live", [this.selectedSkin()]);
        }
    }.bind(this);

    /**
     * Syncs only the games from the selected filter
     */
    this.syncToLive = function () {
        sb.notify("editor-set-games-live", [this.selectedCategory(), this.selectedDevice(), this.selectedSkin()]);
    }.bind(this);

    /**
     * Handle response from server after setting all games live
     */
    this.onSetAllGamesLiveFinished = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("All games have been sync to Live");

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Handle response from server after setting the game selection to live
     */
    this.onSetGamesLiveFinished = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("These games have been sync to Live");

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Reverts ALL games from Live
     */
    this.revertALL = function () {
        if (confirm("This will revert ALL categories and ALL devices for the selected skin.\n Are you sure you want to do this?")) {
            sb.notify("editor-revert-all-games", [this.selectedSkin()]);
        }
    }.bind(this);

    /**
     * Reverts from Live only the games from the selected filter
     */
    this.revertFromLive = function () {
        sb.notify("editor-revert-games", [this.selectedCategory(), this.selectedDevice(), this.selectedSkin()]);
    }.bind(this);

    /**
     * Handle response from server after reverting all games from live
     */
    this.onRevertedAllGames = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("All games have been reverted from Live");

            this.getGamesForCategory();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Handle response from server after reverting the game selection from live
     */
    this.onRevertedGames = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("These games have been reverted from Live");

            this.getGamesForCategory();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Get skin name
     * @param skinId
     * @returns {string} skin name
     */
    this.getSkinName = function (skinId) {
        switch (skinId) {
        case 1:
            return "Spin and Win";
        case 2:
            return "Kitty Bingo";
        case 3:
            return "Lucky Pants Bingo";
        case 4:
            return "Direct Slot";
        case 5:
            return "Magical Vegas";
        case 6:
            return "Bingo Extra";
        case 999:
            return "Test";
        }
    };

    /**
     * User selected a game thumbnail
     */
    this.selectedGameThumbnail = function (game, e) {
        var $game = sb.$(e.currentTarget).find("img");
        var selected = $game.hasClass("selected");
        if (selected) {
            $game.removeClass("selected");
        } else {
            $game.addClass("selected");
        }
    }.bind(this);

    /**
     * Remove selected games
     */
    this.removeSelected = function () {
        var $selection = sb.$(".thumbnail.selected");
        if ($selection.length) {

            var id, ids = [];

            $selection.each(function () {
                id = sb.$(this).data("gameid");
                id && ids.push(id);
            });

            sb.notify("editor-remove-games", [this.selectedCategory(), this.selectedDevice(), this.selectedSkin(), ids]);

        } else {
            sb.notifyWarning("No games were selected");
        }
    }.bind(this);

    /**
     * Handle response from server after removing games
     */
    this.onRemovedGames = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("The selected games have been removed");

            this.getGamesForCategory();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Flush website's cache
     */
    this.flushCache = function () {
        sb.notify("editor-flush-cache");
    }.bind(this);

    /**
     * Flush cache request has finished
     * Handles response
     */
    this.onFlushedCache = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("Cache flushed");

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);
};