var sbm = sbm || {};

/**
 * Skin editor
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.SkinEditor = function (sb, options) {
    "use strict";



    

    this.searchingInFolders = function() {
        if (this.searchText().length >= 3) {
            sb.notify("get-content-images-by-search", [this.editableContent().contentCategoryTypeID, this.subFolder(), this.searchText()]);
        } else {
            sb.notify("get-content-images", [this.editableContent().contentCategoryTypeID, this.subFolder()]);
        }
    }.bind(this);

    // properties
    this.testTitle = sb.ko.observable("");

    // this.chineseTest = ko.computed(function() {
    //     return this.testTitle();
    // }.bind(this);

    this.chineseTest = ko.computed(function() {
       return this.testTitle();
    }, this);


    this.page = "";
    this.key = "";
    this.subKey = "";
    this.cacheKey = "";
    this.editingUsers = sb.ko.observable(false);
    this.addingUsers = sb.ko.observable(false);
    this.editingContent = sb.ko.observable(true);
    this.editingSubPages = sb.ko.observable(false);
    this.editingSEO = sb.ko.observable(false);
    this.editingTcsAndPP = sb.ko.observable(false);
    this.checkingSkinImages = sb.ko.observable(false);
    this.checkingModifications = sb.ko.observable(false);
    this.categories = sb.ko.observableArray([]);
    this.subpages = sb.ko.observableArray([]);
    this.users = sb.ko.observableArray([]);
    this.editingContentType = sb.ko.observable("");
    this.editing = sb.ko.observable(false);
    this.editableContent = sb.ko.observable({});
    this.defaultTypeId = sb.ko.observable("");
    this.defaultTypeName = sb.ko.observable("");
    this.listCategoryId = null;
    this.seoContent = sb.ko.observable({});
    this.imgPickerVisible = sb.ko.observable(false);
    this.imagePath = sb.ko.observable("");
    this.folders = sb.ko.observableArray([]);
    this.images = sb.ko.observableArray([]);
    this.selectedImageName = sb.ko.observable("");
    this.subFolder = sb.ko.observable("");
    this.searchText = sb.ko.observable("").extend({ rateLimit: 1000 });
    this.searchText.subscribe(this.searchingInFolders);
    this.loginVisible = sb.ko.observable(false);
    this.validSession = sb.ko.observable(false);
    this.username = sb.ko.observable("");
    this.password = sb.ko.observable("");
    this.displayError = sb.ko.observable("");
    this.imgPickerToggleText = sb.ko.observable("Maximize");
    this.introEnabled = sb.ko.observable(false);
    this.bodyEnabled = sb.ko.observable(false);
    this.termsEnabled = sb.ko.observable(false);
    this.oldCategoryBeforeChange = "";
    this.categoryChangeSubscription = null;
    this.gamesFound = [];
    this.selectedImgPicker = 1;
    this.editingDevice = ""; // 0 - join record; 1 - web only; 2 - mobile only
    this.skinName = sb.ko.observable("");
    this.checkImgEnvironment = sb.ko.observable(-1);
    this.checkImgDevice = sb.ko.observable(-1);
    this.skinPages = sb.ko.observableArray([]);
    this.stagingAndLiveModifications = sb.ko.observableArray([]);
    this.returnedCategories = sb.ko.observableArray([]);

    // Logged User Properties
    this.canUpload = sb.ko.observable(JSON.parse(sb.getFromStorage("canUpload")) || 0);
    this.IsAdmin = sb.ko.observable(JSON.parse(sb.getFromStorage("IsAdmin")) || 0);
    this.UserGroup = sb.ko.observable(JSON.parse(sb.getFromStorage("UserGroup")) || 0);

    // New user
    this.newUsername = sb.ko.observable("");
    this.newPassword = sb.ko.observable("");
    this.newIsAdmin = sb.ko.observable(false);
    this.newUserGroup = sb.ko.observable(1);
    this.newCanUpload = sb.ko.observable(false);

    // Terms and conditions
    this.termsAndConditions = {
        editMode: sb.ko.observable("new")
    };

    // Privacy Policy
    this.privacyPolicy = {
        editMode: sb.ko.observable("new")
    };

    // checkbox to remember user to update the TCs
    this.updatedFullTcs = sb.ko.observable(false);

    /**
     * User canceled edition
     */
    this.cancelEditing = function () {
        $(".list").DataTable().destroy();
        if (this.editableContent().id) {
            sb.notify("editor-cancel-editing", [this.editableContent().id]);
        } else {
            this.editing(false);
            this.closeImgPicker();
        }
    }.bind(this);

    /**
     * Check if user has hit Enter button
     */
    this.checkForEnterKey = function (data, e) {
        if (e.which == 13) {
            this.doLogin();
        } else {
            return true;
        }
    }.bind(this);

    /**
     * Deselects the chosen image
     */
    this.clearImage = function (image) {
        if (image === 1) {
            this.editableContent().image("");
        } else if (image === 2) {
            this.editableContent().image1("");
        } else if (image === 3) {
            this.editableContent().text1("");
        } else if (image === 4) {
            this.editableContent().text2("");
        } else if (image === 5) {
            this.editableContent().text3("");
        } else if (image === 6) {
            this.editableContent().text4("");
        } else if (image === 7) {
            this.editableContent().text5("");
        } else if (image === 8) {
            this.editableContent().text6("");
        } else if (image === 9) {
            this.editableContent().text7("");
        } else if (image === 10) {
            this.editableContent().text8("");
        } else if (image === 11) {
            this.editableContent().text9("");
        } else if (image === 12) {
            this.editableContent().text10("");
        } else if (image === 13) {
            this.editableContent().text11("");
        } else if (image === 14) {
            this.editableContent().text12("");
        } else if (image === 15) {
            this.editableContent().text13("");
        } else if (image === 16) {
            this.editableContent().text14("");
        } else if (image === 17) {
            this.editableContent().text15("");
        } else if (image === 18) {
            this.editableContent().text16("");
        } else if (image === 19) {
            this.editableContent().text17("");
        } else if (image === 20) {
            this.editableContent().text18("");
        } 
    }.bind(this);

    /**
     * Clear session
     */
    this.clearSession = function () {
        this.loginVisible(true);
        this.validSession(false);
        sb.removeFromStorage("sessionId");
    }.bind(this);

    /**
     * Close image picker
     */
    this.closeImgPicker = function () {
        this.imgPickerVisible(false);
        this.subFolder("");
    }.bind(this);

    /**
     * Delete item
     * @param id Item ID
     */
    this.deleteItem = function (id) {
        if (window.confirm("You're about to delete an item. Do you wish to proceed?")) {
            id && sb.notify("editor-delete-item", [id]);
        }
    }.bind(this);

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("editor-got-categories-for-skin");
        sb.ignore("got-category-list");
        sb.ignore("got-editor-users-list-all");
        sb.ignore("got-editable-content");
        sb.ignore("saved-editable-content");
        sb.ignore("saved-editable-user");
        sb.ignore("finished-set-content-order");
        sb.ignore("got-seo-content");
        sb.ignore("saved-seo-content");
        sb.ignore("got-content-images");
        sb.ignore("synced-live");
        sb.ignore("reverted-all-content");
        sb.ignore("editor-done-login");
        sb.ignore("editor-deleted-item");
        sb.ignore("editor-canceled-editing");
        sb.ignore("admin-session-has-expired");
        sb.ignore("editor-got-game-info");
        sb.ignore("editor-flushed-cache");
        sb.ignore("editor-checked-skin-images");
    };

    /**
     * Login request
     */
    this.doLogin = function () {
        sb.notify("editor-do-login", [this.username(), this.password()]);
    }.bind(this);

    /**
     * Edit record
     */
    this.editContent = function (id, defaultContentCategoryId, device) {

        this.hideFields();
        this.editingDevice = device;
        // get content
        id && sb.notify("get-editable-content", [id, defaultContentCategoryId, device]);

    }.bind(this);

    /**
     * Dynamic field configuration
     * @param fields
     */
    this.fieldConfigurator = function (fields) {

        if (fields) {

            var $field, $fieldChars, currentChars, maxChars, currentValue;

            this.hideFields();

            sb._.each(fields, function (field) {

                if (field.FieldName === "Image") {
                    sb.$("#editImageContiner").show();
                }
                if (field.FieldName === "Image1") {
                    sb.$("#editImageContiner2").show();
                    sb.$("#editImageContiner2").find("*").show();
                }
                if (field.FieldName === "GameID") {
                    sb.$("#ui-search-game").show();
                    sb.$("#ui-search-game").find("*").show();
                }

                $field = sb.$("#" + field.FieldName);

                if ($field.length) {
                    // check if is a text field and change label
                    sb.$("label[for='" + field.FieldName + "']").text(field.Label);

                    //enable field
                    $field.show();
                    sb.$("label[for='" + field.FieldName + "']").show();

                    //mandatory fields
                    if (field.Mandatory) {
                        $field.attr("data-mandatory", "true");
                        $field.prev().append("<span class='mandatory-field'>*</span>");
                    }

                    if (sb.$("#" + field.FieldName + "-chars").length === 0) {

                        // if tinyMCE
                        if (field.FieldName === "Intro" || field.FieldName === "Body" || field.FieldName === "Terms") {

                            sb.$("#tab" + field.FieldName).text(field.Label);

                            // append element with information about max length and chars remaining
                            currentChars = field.Length - tinyMCE.get(field.FieldName).getContent().length;
                            $field.after("<small class='right'>Chars left:<span class='charcount' data-maxchars='" + field.Length + "' id='" + field.FieldName + "-chars'>" + currentChars + "</span></small>");

                        } else {

                            // append element with information about max length and chars remaining
                            if (field.Length > 0) {
                                currentChars = field.Length - $field.val().length;
                                $field.after("<div class='right'><small class='counting-chars'>Chars left:<span class='charcount' data-maxchars='" + field.Length + "' id='" + field.FieldName + "-chars'>" + currentChars + "</span></small></div>");
                            }

                            // when user types we will update the chars remaining and also cut any text above max chars
                            $field.on("keyup", function (e) {

                                var $this = sb.$(this);
                                $fieldChars = $this.next().find(".charcount"); // the span with the chars
                                maxChars = parseInt($fieldChars.data("maxchars"), 10);

                                currentValue = $this.val();

                                if (maxChars >= currentValue.length) {
                                    if (!isNaN(maxChars)) {
                                        currentChars = maxChars - currentValue.length;
                                        $fieldChars.text(currentChars);
                                    }
                                } else {
                                    $this.val(currentValue.substring(0, maxChars));
                                }

                            });
                        }

                    }

                }

                this.setupTextField(field);

            }.bind(this));
        }
    };

    /**
     * Setup Text fields of the different type of content
     * @param field
     */
    this.setupTextField = function (field) {

        if (field.FieldName.indexOf("Text") !== -1) {
            if (field.FieldType === 2) {
                sb.$("#edit" + field.FieldName + "Image").show();
                sb.$("#edit" + field.FieldName + "DropdownBox").hide();
            } else if (field.FieldType === 4) {
                sb.$("#edit" + field.FieldName + "DropdownBox").show();
                sb.$("#edit" + field.FieldName + "Image").hide();
                sb.$("#" + field.FieldName).hide();
            } else {
                sb.$("#edit" + field.FieldName + "Image").hide();
                sb.$("#edit" + field.FieldName + "DropdownBox").hide();
            }
        }

    };
    /**
     * Get the list of categories
     * @param page
     * @param key
     * @param subKey
     * @param confirmCreation
     */
    this.getCategoryList = function (page, key, subKey, confirmCreation) {
        if (page && key) {
            sb.notify("get-category-list", [page, key, subKey, confirmCreation]);
        }
    };

    /**
     * Get a list of all the content items for a page / key and all subkeys
     */
    this.getCategoryListAll = function () {
        if (this.page && this.key) {
            sb.notify("get-category-list-all", [this.page, this.key]);
        }
    }.bind(this);

    /**
     * Get a list of all the content items for a page / key and all subkeys
     */
    this.getEditorUsersListAll = function () {
        sb.notify("get-editor-users-list-all");
    }.bind(this);

    /**
     * Get content type name
     * @param id
     * @returns {string}
     */
    this.getContentTypeName = function (id) {

        if (id === 1) {
            return "Text";
        }

        return "";

    };

    /**
     * Get the current device
     * @param forWeb 1
     * @param forMobile 2
     * @returns {string}
     */
    this.getDevicesFormated = function (forWeb, forMobile) {
        var list = "";
        if (forWeb) list = "1";
        if (forMobile) list = list === "" ? "2" : list + ",2";
        return list;
    };

    /**
     * Get game info
     */
    this.getGameInfo = function () {
        this.editableContent().gameId() && sb.notify("editor-get-game-info", [this.editableContent().gameId()]);
        sb.$(".ui-game-search").val("");
    }.bind(this);

    /**
     * Get SEO content
     */
    this.getSEOContent = function () {
        sb.notify("get-seo-content", [this.cacheKey]);
    };

    /**
     * Get state name
     * @param id
     * @param editingUser
     * @returns {*}
     */
    this.getStateName = function (id, editingUser) {

        if (editingUser) return "Editing " + editingUser;
        if (id === 0) return "Hiding";
        if (id === 1) return "Live";
        if (id === 2) return "New Item";
        if (id === 3) return "Modifying";
        if (id === 4) return "Archiving";
        if (id === 5) return "Hidden";
        if (id === 6) return "Order";
        if (id === 7) return "Archived";

        return "";

    };

    /**
     * Get state name
     * @param id
     * @param editingUser
     * @returns {*}
     */
    this.checkedToSync = function (id, editingUser, user) {
        var adminUsername = sb.getCookie("AdminUsername");
        return (adminUsername === editingUser.toLowerCase()) || (sb._.contains([2,3,4,6], id) && user.toLowerCase() === adminUsername);
    };

    /**
     * Get all the different contents on TinyContent fields
     * @returns {{intro: *, body: *, terms: *}}
     */
    this.getTinyContent = function () {
        return {
            intro: tinyMCE.get('Intro').getContent(),
            body: tinyMCE.get('Body').getContent(),
            terms: tinyMCE.get('Terms').getContent()
        };
    };

    /**
     * Hide all fields that are not used on the detail
     */
    this.hideFields = function () {

        var $inputs = sb.$("#ui-editable-form").find("input:not(:button, .always-editable)");
        var $selects = sb.$("#ui-editable-form").find("select:not(.always-editable)");
        // set up the form
        $inputs.hide();
        $selects.hide();
        // remove mandatory attr
        $inputs.removeAttr("data-mandatory");
        $selects.removeAttr("data-mandatory");
        // remove errors
        sb.$(".error").removeClass("error");
        // remove mandatory fields - span with *
        sb.$(".mandatory-field").remove();
        sb.$("label:not(.always-editable)").hide();
        // image container
        sb.$("#editImageContiner").hide();
        sb.$("#editImageContiner2").hide();
        // chars counters
        sb.$(".charcount").parent().remove();
        // search image
        sb.$("#ui-search-game").hide();
        // text fields images
        sb.$(".ui-edit-text-image").hide();
    };

    /**
     * Module initialization
     */
    this.init = function () {

        sb.listen("got-category-list", this.onGotCategoryList);
        sb.listen("got-category-list-all", this.onGotCategoryListAll);
        sb.listen("got-editor-users-list-all", this.onGotEditorUsersListAll);
        sb.listen("got-editable-content", this.onGotEditableContent);
        sb.listen("saved-editable-content", this.onSavedEditableContent);
        sb.listen("added-new-user", this.onaddedNewUser);
        sb.listen("saved-editable-user", this.onSavedEditableUser);
        sb.listen("finished-set-content-order", this.onFinishedSetOrder);
        sb.listen("got-seo-content", this.onGotSEOContent);
        sb.listen("saved-seo-content", this.onSavedSEOContent);
        sb.listen("got-content-images", this.onGotContentImages);
        sb.listen("synced-live", this.onSyncedLive);
        sb.listen("reverted-all-content", this.onRevertedAllContent);
        sb.listen("editor-done-login", this.onDoneLogin);
        sb.listen("editor-done-logout", this.onDoneLogout);
        sb.listen("editor-deleted-item", this.onDeletedITem);
        sb.listen("editor-canceled-editing", this.onCanceledEditing);
        sb.listen("admin-session-has-expired", this.clearSession);
        sb.listen("editor-got-game-info", this.onGotGameInfo);
        sb.listen("editor-flushed-cache", this.onFlushedCache);
        sb.listen("editor-checked-skin-images", this.onCheckedSkinImages);
        sb.listen("editor-compared-live-staging", this.onComparedLiveAndStaging);
        sb.listen("editor-got-categories-for-skin", this.onGotCategoriesForSkin);
        sb.listen("published-tcs", this.onPublishedTCs);
        sb.listen("published-pp", this.onPublishedPP);

        this.page = sb.getUrlParameter("page");
        this.key = sb.getUrlParameter("key");
        this.subKey = sb.getUrlParameter("subkey") || "";
        this.cacheKey = sb.getUrlParameter("cachekey");

        if (sb.getCookie("AdminSessionID")) {
            this.showSection(this.key);
            this.validSession(true);
            sb.notify("editor-get-categories-for-skin", [sbm.serverconfig.skinId]);
        } else {
            this.validSession(false);
            this.loginVisible(true);
        }

        this.setupSearchBox();

        tinymce.init({
            selector: "textarea.tinymce",
            plugins: "paste, link, table, image, media, preview",
            entity_encoding : "numeric",
            width: "100%",
            height: 400,
            autoresize_min_height: 400,
            autoresize_max_height: 600,
            paste_as_text: false,
            menu: {},
            menubar: "insert",
            convert_urls: false,
            toolbar: [
                "bold italic | bullist numlist outdent indent | link image | media | table | formatselect | "
            ],
            setup : function (ed) {
                ed.on("keydown", function (e) {

                    //currentChars = field.Length - tinyMCE.get(field.FieldName).getContent().length;
                    var currLenght = sb.$(ed.getBody()).text().length;
                    var $fieldChars = $("#" + ed.id).next().find(".charcount");
                    var maxChars = parseInt($fieldChars.data("maxchars"), 10);

                    // if is not backspace and not delete
                    if (e.which !== 8 && e.which !== 46) {
                        // if reached max chars
                        if (currLenght > maxChars) {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        }
                    }

                    $fieldChars.text(maxChars - currLenght);
                });

            }
        });

        sb.$("table.list tbody").sortable().disableSelection();
        sb.$("#DateFrom_DST").datepicker({
            dateFormat: "yy-mm-dd"
        });
        sb.$("#DateTo_DST").datepicker({
            dateFormat: "yy-mm-dd"
        });
        var self = this;
        sb.$("#ui-uploadForm").on("submit", function (e) {
            e.preventDefault();
            var imgPath = sb.$("#userImage").val();
            if (imgPath) {
                var filename = imgPath.split("\\").slice(-1);
                $.ajax({
                    url: "/admindata.php",
                    type: "POST",
                    data:  new FormData(this),
                    dataType: "json",
                    cache: false,
                    processData: false,
                    contentType: false,
                    async : false
                }).done(function () {
                    self.imgPickerVisible(false);
                    sb.$("#userImage").val("");
                    self.editableContent().image(self.imagePath() + filename);
                }).fail(function (jqXHR, textStatus) {
                    sb.notifyError("Sorry there is a system error, if the error persists contact support");
                    sb.logError("Uploading image Error: " + textStatus);
                    self.editableContent().image("");
                });
            } else {
                sb.notifyWarning("Please choose a file first");
            }
        });

        this.skinName(this.getSkinName());

    };

    /**
     * Checking if the content is valid
     * @returns {boolean}
     */
    this.isValidContent = function () {
        var mandatoryFields = $("input[data-mandatory]");
        var valid = true, fieldName;      
        sb._.each(mandatoryFields, function (field) {
            var $field = sb.$(field);
            fieldName = $field.attr('id');
            if (sb.$("#" + fieldName).val() === "") {
                $field.addClass("error");
                valid = false;
            } else {
                $field.removeClass("error");
            }
        });
        if (!valid) {
            sb.notifyWarning("Please fill all mandatory fields first");
        }
        return valid;
    };

    /**
     * Cancel editing request has ended
     * Handles response
     */
    this.onCanceledEditing = function (e, data) {
        if (data.Code === 0) {

            this.getCategoryList(this.page, this.key, this.subKey);
            this.editing(false);
            this.closeImgPicker();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * User changed the type of the content
     * Let's save it so it can be reverted later
     * @param oldVal
     */
    this.onCategoryItemBeforeChange = function (oldVal) {
        this.oldCategoryBeforeChange = oldVal;
    };

    /**
     * Change the type of content category
     * Do request to backend
     */
    this.onCategoryItemChanged = function (newVal) {
    
        if (window.confirm("This will make the form to reload and loose all unsaved changes. Do you wish to proceed?")) {
            sb.notify("get-editable-content", [this.editableContent().id, newVal, this.editingDevice]);
        } else {
            // remove subscription or we'll end up on a infinite loop
            this.categoryChangeSubscription.dispose();
            this.editableContent().contentCategoryTypeID(this.oldCategoryBeforeChange);
            // add subscription
            this.categoryChangeSubscription = this.editableContent().contentCategoryTypeID.subscribe(this.onCategoryItemChanged);
        }
    }.bind(this);

    /**
     * Delete item request has ended
     * Handles response
     */
    this.onDeletedITem = function (e, data) {
        if (data.Code === 0) {

            this.getCategoryList(this.page, this.key, this.subKey);
            sb.notifySuccess("Item removed successfully");
            this.editing(false);
            this.closeImgPicker();
            this.reloadWebsite();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Login request has ended
     * Handles response
     */
    this.onDoneLogin = function (e, data) {
        if (data.Code === 0) {

            this.loginVisible(false);
            this.validSession(true);
            this.showSection(this.key);
            this.canUpload(data.CanUpload);
            this.IsAdmin(data.IsAdmin);
            this.UserGroup(data.UserGroup);
            sb.saveOnStorage("canUpload", data.CanUpload);
            sb.saveOnStorage("IsAdmin", data.IsAdmin);
            sb.saveOnStorage("UserGroup", data.UserGroup);
            sb.setCookie("canUpload", data.CanUpload);
            sb.setCookie("IsAdmin", data.IsAdmin);
            sb.setCookie("UserGroup", data.UserGroup);

            this.reloadEditor();
        } else {
            this.username("");
            this.password("");
            this.displayError(data.Msg);
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Finish set order request has ended
     * Handles response
     */
    this.onFinishedSetOrder = function (e, data) {
        if (data.Code === 0) {

            this.getCategoryList(this.page, this.key, this.subKey);
            sb.notifySuccess("Order saved successfully");
            this.reloadWebsite();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Get category list request has ended
     * Handles response
     */
    this.onGotCategoryList = function (e, data) {
        if (data.Code === 0) {

            this.editingSEO(false);
            this.editingSubPages(false);
            this.checkingSkinImages(false);
            this.checkingModifications(false);
            this.editingContent(true);

            this.listCategoryId = data.ID;
            this.defaultTypeId(data.DefaultTypeID);
            this.defaultTypeName(data.DefaultTypeName);

            var list = [];

            var self = this;
            _.each(data.Data, function (category) {

                var temp = {
                    id : category.ID,
                    gridId : category.ID + ",",
                    name : category.Name,
                    title : category.Title,
                    status : self.getStateName(category.StateID, category.EditingUser),
                    toSync : self.checkedToSync(category.StateID, category.EditingUser, category.User),
                    devices : category.Device,
                    date : category.Date,
                    publishDate : category.DateFrom_DST,
                    typeId : category.TypeID,
                    typeName : category.TypeName,
                    order : category.SortOrder,
                    user: category.User,
                    editingUser: category.EditingUser,
                    split: !!category.Split
                };
                list.push(temp);
            });
            this.categories(list);
            $(".list").DataTable({
                "order": [],
                dom: '<"datatab__left" l><"datatab__middle"><"datatab__right"f>ip'
            });
            $(".datatab__middle").html('<label>Status: <select onchange="redraw()" name="status" id="status">'+
                '<option value="">All</option>'+
                '<option value="Live">Live</option>'+
                '<option value="New Item">New Item</option>'+
                '<option value="Modifying">Modifying</option>'+
                '<option value="Editing">Editing</option>'+
                '<option value="Hidden">Hidden</option>'+
                '<option value="Order">Order</option>' +
                '<option value="Archived">Archived</option>'+
            '</select></label>');
        } else {
            if (data.Code === 1) {
                var msg = "This will add a new edit point. \n\n" +
                        "- Page: " + this.page + "\n" +
                        "- Key: " + this.key+ "\n" +
                        "- SubKey: " + this.subKey+ "\n\n" +
                        "Are you sure?";

                if (confirm(msg)) {
                    this.getCategoryList(this.page, this.key, this.subKey, 1);
                } else {
                    window.close();
                }
            }

        }
    }.bind(this);

    /**
     * Got all sub-pages from request
     */
    this.onGotCategoryListAll = function (e, data) {
        if (data.Code === 0) {

            var list = [];

            var self = this;
            _.each(data.Data, function (subpage) {

                var temp = {
                    id: subpage.ID,
                    subkey: subpage.SubKey,
                    url : subpage.DetailsURL,
                    date : subpage.Date,
                    user: subpage.User
                };

                list.push(temp);

            }.bind(this));
            this.subpages(list);

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Got all users from request
     */
    this.onGotEditorUsersListAll = function (e, data) {
        if (data.Code === 0) {

            var list = [];
            _.each(data.Data, function (user) {
                list.push({
                    ID: user.ID,
                    Username: user.Username,
                    IsAdmin: user.IsAdmin,
                    UserGroup: user.UserGroup,
                    CanUpload: user.CanUpload
                });
            }.bind(this));
            this.users(list);
        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Get content images request has ended
     * Handles response
     */
    this.onGotContentImages = function (e, data) {
        if (data.Code === 0) {
            this.imagePath(data.ImagePath);

            if (data.Folders !== false)
                this.folders(data.Folders);

            var images = [];
            _.each(data.Images, function (img) {
                images.push({
                    imgPath: data.ImagePath + img.display,
                    selected: sb.ko.observable(false)
                });
            });

            this.images(images);

            this.imgPickerVisible(true);
        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Get editable request has ended
     * Handles response
     */
    this.onGotEditableContent = function (e, data) {
        sb.$(".ui-game-search").val("");
        if (data.Code === 0) {

            var deviceList = data.DeviceCategoryIDs.split(','); // Staging: 51=web, 52=Tab, 53=Phone

            var days = data.Days.split(",");

            var detailUrl =  data.ID ? data.DetailsURL : this.subKey;

            var content = {
                id: data.ID,
                contentCategoryID: this.listCategoryId,
                contentCategoryTypeID: sb.ko.observable(),
                offers: sb.ko.observable(),
                offerId: data.OfferID,
                DefaultContentCategoryTypeName: data.DefaultContentCategoryTypeName,
                name: sb.ko.observable(data.Name),
                title: sb.ko.observable(data.Title),
                body: data.Body,
                intro: sb.ko.observable(data.Intro),
                order: data.SortOrder,
                forWeb: sb.ko.observable(deviceList.indexOf("1") > -1),
                forMobile: sb.ko.observable(deviceList.indexOf("2") > -1),
                status: data.StatusID,
                detailUrl: sb.ko.observable(detailUrl),
                image: sb.ko.observable(data.Image || ""),
                imageAlt: sb.ko.observable(data.ImageAlt),
                image1: sb.ko.observable(data.Image1 || ""),
                imageAlt1: sb.ko.observable(data.ImageAlt1),
                imageHeight: data.ImageHeight,
                imageWidth: data.ImageWidth,
                dateFrom: data.DateFrom_DST ? data.DateFrom_DST.substring(0,10) : "",
                dateTo: data.DateTo_DST ? data.DateTo_DST.substring(0,10) : "",
                lastUpdate: data.Date,
                user: data.User,
                terms: data.Terms,
                types: data.Type,
                gameId: sb.ko.observable(""),
                partial: data.Partial,
                text1: sb.ko.observable(data.Text1),
                text2: sb.ko.observable(data.Text2),
                text3: sb.ko.observable(data.Text3),
                text4: sb.ko.observable(data.Text4),
                text5: sb.ko.observable(data.Text5),
                text6: sb.ko.observable(data.Text6),
                text7: sb.ko.observable(data.Text7),
                text8: sb.ko.observable(data.Text8),
                text9: sb.ko.observable(data.Text9),
                text10: sb.ko.observable(data.Text10),
                text11: sb.ko.observable(data.Text11),
                text12: sb.ko.observable(data.Text12),
                text13: sb.ko.observable(data.Text13),
                text14: sb.ko.observable(data.Text14),
                text15: sb.ko.observable(data.Text15),
                text16: sb.ko.observable(data.Text16),
                text17: sb.ko.observable(data.Text17),
                text18: sb.ko.observable(data.Text18),
                sunday: days.indexOf("1") > -1,
                monday: days.indexOf("2") > -1,
                tuesday: days.indexOf("3") > -1,
                wednesday: days.indexOf("4") > -1,
                thursday: days.indexOf("5") > -1,
                friday: days.indexOf("6") > -1,
                saturday: days.indexOf("7") > -1,
                selectText1: sb.ko.observable(data.Text1)
            };

            this.editableContent(content);
            // set observable value
            this.editableContent().contentCategoryTypeID(data.ContentCategoryTypeID);
            this.editableContent().offers(data.Offer);
            this.editingContentType(this.getContentTypeName(content.contentCategoryTypeID));
            // set tab visibility
            this.introEnabled(data.Intro_tab);
            this.bodyEnabled(data.Body_tab);
            this.termsEnabled(data.Terms_tab);
            // on change category
            this.editableContent().contentCategoryTypeID.subscribe(this.onCategoryItemBeforeChange, this, "beforeChange");
            this.categoryChangeSubscription = this.editableContent().contentCategoryTypeID.subscribe(this.onCategoryItemChanged);
            // tinymce content
            this.setTinyContent(content.intro(), content.body, content.terms);

            var fields = [];

            if (data.Details_tab) {
                fields = data.Details_tab.concat(data.Intro_tab || []).concat(data.Body_tab || []).concat(data.Terms_tab || []);
                this.fieldConfigurator(fields);
            }

            this.editing(true);

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Get game info request has ended
     * Handles response
     */
    this.onGotGameInfo = function (e, data) {

        var success = data.Code === 0 || data.Code === 2;

        if (data.Code === 2) {
            sb.showInfo(sb.i18n("gameNoAvailableOnMobile"));
            this.editableContent().forMobile(false);
        }

        if (success) {

            if(this.editableContent().contentCategoryTypeID() == 38) {
                this.editableContent().text1(data.DetailsURL); // this is for welcome game offers
                this.editableContent().text3(data.Text1); 
                this.editableContent().text4(data.Image); 
                this.editableContent().image1(data.Image1); 
                this.editableContent().text5(data.Title);
                tinyMCE.get('Intro').setContent(data.Intro);                
            } else {
                this.editableContent().imageAlt(data.Alt);
                this.editableContent().imageAlt1(data.Alt);
                this.editableContent().detailUrl(data.DetailsURL);
                this.editableContent().image(data.Image);
                this.editableContent().image1(data.Image1);
                this.editableContent().name(data.Name);
                this.editableContent().title(data.Title);
                this.editableContent().intro(data.Intro);
                this.editableContent().text1(data.Text1);
                if(this.editableContent().contentCategoryTypeID() == 28) {
                    this.editableContent().text2(data.DetailsURL); // this is for game winner articles
                }
                tinyMCE.get('Intro').setContent(data.Intro);
            }


        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

    /**
     * Setup game search
     * It uses jQuery auto-complete plugin
     */
    this.setupSearchBox = function () {

        sb.$(".ui-game-search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/admindata.php",
                    dataType: "json",
                    data: {
                        f: "searchgames",
                        id: "",
                        name: request.term
                    },
                    success: function (data) {

                        this.gamesFound.length = 0;

                        if (data.Code === 0) {

                            if (data.Games.length > 0) {

                                sb._.each(data.Games, function (game) {
                                    this.gamesFound.push({
                                        label: game.name,
                                        value: game.name,
                                        gameId: game.ID
                                    });
                                }.bind(this));

                            } else {
                                sb.notifyInfo("No games were found");
                            }

                        } else {
                            data.Msg && sb.showError(data.Msg);
                        }

                        response(this.gamesFound);

                    }.bind(this)
                });
            }.bind(this),
            minLength: 3,
            delay: 500,
            select: function (e, ui) {
                this.editableContent().gameId(ui.item.gameId);
            }.bind(this)
        });

    };

    /**
     * Get SEO content request has ended
     * Handles response
     */
    this.onGotSEOContent = function (e, data) {

        if (data && data.Code === 0) {

            this.seoContent({
                cacheKey: data.Page,
                seoContent: data.Content
            });

        } else {
            // new record
            this.seoContent({
                cacheKey: this.cacheKey,
                seoContent: ""
            });
        }

    }.bind(this);

    /**
     * Get images for a specific path
     * Makes request
     */
    this.openImageFolder = function (display, path) {

        if (display === "Up Folder") {
            this.subFolder(path);
        } else {
            this.subFolder(this.subFolder() + display + "/");
        }
        sb.notify("get-content-images", [this.editableContent().contentCategoryTypeID, this.subFolder()]);

    }.bind(this);

    /**
     * Opens games search dialog
     */
    this.openImgPicker = function (imgpicker) {

        this.selectedImgPicker = imgpicker;
        sb.notify("get-content-images", [this.editableContent().contentCategoryTypeID]);

    }.bind(this);

    /**
     * Revert from LIVE all the content
     */
    this.onRevertedAllContent = function (e, data) {
        if (data.Code === 0) {

            this.getCategoryList(this.page, this.key, this.subKey);
            sb.notifySuccess("All content reverted successfully");
            this.reloadWebsite();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Save content request has ended
     * Handles response
     */
    this.onSavedEditableContent = function (e, data) {
        if (data.Code === 0) {

            this.getCategoryList(this.page, this.key, this.subKey);

            this.editing(false);
            this.closeImgPicker();
            sb.notifySuccess("Data saved successfully");
            this.reloadWebsite();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Save content request has ended
     * Handles response
     */
    this.onSavedEditableUser = function (e, data) {
        if (data.Code === 0) {
            sb.showConfirm("Data saved successfully");
        } else {
            data.Msg && sb.notifyError(data.Msg);
            this.getEditorUsersListAll();
        }
    }.bind(this);

    /**
     * Save content request has ended
     * Handles response
     */
    this.onaddedNewUser = function (e, data) {
        if (data.Code === 0) {
            sb.showConfirm("Data saved successfully");
            this.showSection("USERS");
            this.addingUsers(false);
            this.newUsername("");
            this.newPassword("");
            this.newIsAdmin(false);
            this.newUserGroup(1);
            this.newCanUpload(false);
        } else if (data.Code === 2601 || data.Code === 2627) {
            this.displayError("The name " + this.newUsername() + " is already used. Please change it");
        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Save SEO content request has ended
     * Handles response
     */
    this.onSavedSEOContent = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("Data saved successfully");
            this.reloadWebsite();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Sync to live request has ended
     * Handles response
     */
    this.onSyncedLive = function (e, data) {
        if (data.Code === 0) {

            this.getCategoryList(this.page, this.key, this.subKey);
            sb.notifySuccess("Data synced successfully");
            this.reloadWebsite();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Revert content from LIVE
     */
    this.revertAllContent = function () {
        if (confirm("This will override what you are seeing on Staging with what is currently on Live, are you sure?")){
            this.listCategoryId && sb.notify("revert-all-content", [this.listCategoryId]);
        }
    }.bind(this);

    /**
     * Reload website
     * @param url
     * @param isSubpage
     */
    this.reloadWebsite = function (url, isSubpage) {
        if (!window.opener) {
            window.location.reload();
        }

        if (isSubpage) {

            if (url.indexOf("(Default)") === -1) {
                window.opener.location.pathname = url;
            } else {
                window.opener.location.pathname = url.replace(" (Default)", "", "gi");
            }

        } else {

            if (url) {
                window.opener.location.href = url;
            } else {
                window.opener.location.reload();
            }
        }
    };
    /**
     * Reload website
     * @param url
     * @param isSubpage
     */
    this.reloadEditor = function () {
        window.location.reload();
    };

    /**
     * Save content
     * Validates and makes request
     */
    this.saveEditableContent = function (content, top) {
        $(".list").DataTable().destroy();
        if (content) {
            /*In case the dropdown has been selected*/
            if (content.selectText1()) {
                content.text1(content.selectText1());
            }
            if (this.isValidContent()) {


                var save = function (content) {
                    content.devices = this.getDevicesFormated(content.forWeb(), content.forMobile());
                    var tinyContents = this.getTinyContent();
                    content.intro = tinyContents.intro;
                    content.body = tinyContents.body;
                    content.terms = tinyContents.terms;


                    var days = "";
                    if (content.sunday) days += "1";
                    if (content.monday) days += days ? ",2" : "2";
                    if (content.tuesday) days += days ? ",3" : "3";
                    if (content.wednesday) days += days ? ",4" : "4";
                    if (content.thursday) days += days ? ",5" : "5";
                    if (content.friday) days += days ? ",6" : "6";
                    if (content.saturday) days += days ? ",7" : "7";

                    content.days = days;
                    content.subKey = this.subKey;
                    content.top = top || "";

                    sb.notify("save-editable-content", [content]);
                }.bind(this);

                // editing

                var currentDevice = "";
                var otherDevice = "";

                if (this.editingDevice === 1) {
                    currentDevice = "Web";
                    otherDevice = "Mobile";
                } else {
                    currentDevice = "Mobile";
                    otherDevice = "Web";
                }

                var warningMsg = "";

                if ((this.editingDevice === 1 || this.editingDevice === 2) &&
                        (content.forWeb() && content.forMobile())) {

                    // warn of overriding record
                    warningMsg = "You selected Edit " + currentDevice + " but you also checked " + otherDevice + ".\n" +
                                     "If you wish to proceed, you will override the existing " + otherDevice + " data.";
                    if (confirm(warningMsg)) {
                        save(content);
                    }

                } else if ((this.editingDevice === 0) && (!content.forWeb() || !content.forMobile())) {

                    // warn of splitting record
                    warningMsg = "You are editing a join record which means the web and mobile content are the same.\n" +
                                 "If you wish to proceed, it will split the record and the content will be different for both platforms.";
                    if (confirm(warningMsg)) {
                        save(content);
                    }

                } else {
                    save(content);
                }
            }
        }
    }.bind(this);

    /**
     * Save user modifications
     *
     * Validates and makes request
     */
    this.saveEditableUser = function (user) {
        if (user) {
            sb.notify("save-editable-user", [user]);
        }
    }.bind(this);

    /**
     * Save user modifications
     *
     * Validates and makes request
     */
    this.displayNewUserPanel = function () {

        this.editingUsers(false);
        this.addingUsers(true);
    }.bind(this);

    /**
     * Save user modifications
     *
     * Validates and makes request
     */
    this.addNewUser = function () {

        var valid = true;
        if (!this.newUsername()) {
            this.displayError("The username is missing");
            return;
        }
        if (!this.newPassword()) {
            this.displayError("The password is missing");
            return;
        }

        var user = {
            username : this.newUsername(),
            password : this.newPassword(),
            isAdmin : this.newIsAdmin(),
            userGroup : this.newUserGroup(),
            canUpload : this.newCanUpload()
        };

        sb.notify("add-new-user", [user]);
    }.bind(this);

    /**
     * Save user modifications
     *
     * Validates and makes request
     */
    this.backList = function () {
        this.editingUsers(true);
        this.addingUsers(false);
    }.bind(this);

    /**
     * Save order
     */
    this.saveOrder = function () {

        // the id on the grid is "1," or "2," for Id 1 and Id 2
        // we concatenate all the ids then create an array
        var order = sb.$("tr.ui-grid-item").find("td:first").text().split(",");
        // remove last element (is empty)
        order.pop();

        $(".list").DataTable().destroy();
        order.length && sb.notify("set-content-order", [this.listCategoryId, order]);

    }.bind(this);

    /**
     * Save SEO content
     */
    this.saveSEO = function (content) {

        content && sb.notify("save-seo-content", [content]);

    }.bind(this);

    /**
     * User selected a thumbnail from the picker
     */
    this.selectedThumbnail = function (img) {
        _.each(this.images(), function (img) {
            img.selected(false);
        });
        img.selected(true);
    }.bind(this);

    /**
     * User selected an image from the dialog
     */
    this.selectedImage = function () {
        var selectedImg = _.filter(this.images(), function (img) {
            return img.selected();
        });
        if (selectedImg.length === 1) {
            if (this.selectedImgPicker === 1) {
                this.editableContent().image(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 2) {
                this.editableContent().image1(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 3) {
                this.editableContent().text1(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 4) {
                this.editableContent().text2(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 5) {
                this.editableContent().text3(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 6) {
                this.editableContent().text4(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 7) {
                this.editableContent().text5(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 8) {
                this.editableContent().text6(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 9) {
                this.editableContent().text7(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 10) {
                this.editableContent().text8(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 11) {
                this.editableContent().text9(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 12) {
                this.editableContent().text10(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 13) {
                this.editableContent().text11(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 14) {
                this.editableContent().text12(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 15) {
                this.editableContent().text13(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 16) {
                this.editableContent().text14(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 17) {
                this.editableContent().text15(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 18) {
                this.editableContent().text16(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 19) {
                this.editableContent().text17(selectedImg[0].imgPath);
            } else if (this.selectedImgPicker === 20) {
                this.editableContent().text18(selectedImg[0].imgPath);
            } 

            this.imgPickerVisible(false);
            this.subFolder("");
        } else {
            sb.notificatyWarning("Please select an image");
        }

    }.bind(this);

    /**
     * Set content on the tinyMCE editors
     * @param intro
     * @param body
     * @param terms
     */
    this.setTinyContent = function (intro, body, terms) {
        tinyMCE.get('Intro').setContent(intro);
        tinyMCE.get('Body').setContent(body);
        tinyMCE.get('Terms').setContent(terms);
    };

    this.checkedContentIds = function() {
        var contentIds = '';
        sb._.each(this.categories(), function (content) {
            if (content.toSync === true) {
                contentIds += content.id + ",";
            }
        });

        return contentIds.replace(/,+$/,'');
    }.bind(this);

    /**
     * Sync to live request
     */
    this.syncLive = function () {
        $(".list").DataTable().destroy();
        var contentIds = this.checkedContentIds();
        if (contentIds !== '') {
            this.listCategoryId && sb.notify("sync-live", [this.listCategoryId, this.subKey, contentIds]);
        } else {
            sb.showConfirm("No Selected contents to sync");
        }
    }.bind(this);

    /**
     * Show panel
     * @param section Which panel
     */
    this.showSection = function (section) {

        if (section === "USERS") {
            this.getEditorUsersListAll();
            this.unvisibleAllThePanels();
            this.editingUsers(true);
        } else if (section === "SEO") {
            if (!this.seoContent().cacheKey) {
                this.getSEOContent();
            }
            this.unvisibleAllThePanels();
            this.editingSEO(true);
        } else if (section === "SUBPAGES") {
            this.getCategoryListAll(this.page, this.key);
            this.unvisibleAllThePanels();
            this.editingSubPages(true);
        } else if (section === "SKINIMAGES") {
            this.unvisibleAllThePanels();
            this.checkingSkinImages(true);
        } else if (section === "COMPARE") {
            this.unvisibleAllThePanels();
            this.checkingModifications(true);
        } else if (section === "TCSANDPP") {
            this.unvisibleAllThePanels();
            this.editingTcsAndPP(true);
        } else {
            this.getCategoryList(this.page, this.key, this.subKey);
            this.unvisibleAllThePanels();
            this.editingContent(true);
        };
    }.bind(this);

    this.unvisibleAllThePanels = function() {
        this.checkingModifications(false);
        this.addingUsers(false);
        this.checkingSkinImages(false);
        this.editingSEO(false);
        this.editingSubPages(false);
        this.editingContent(false);
        this.editingUsers(false);
        this.editingTcsAndPP(false);
        $(".list").DataTable().destroy();
    }.bind(this);

    /**
     * Upload image form submission
     */
    this.submitForm = function () {
        sb.$("#ui-uploadForm").submit();
    }.bind(this);

    /**
     * Maximize/restore image picker dialog
     */
    this.toggleSize = function (vm, e) {
        if (this.imgPickerToggleText() === "Maximize") {
            sb.$(e.currentTarget).parent().addClass("maximized");
            this.imgPickerToggleText("Restore");
        } else {
            sb.$(e.currentTarget).parent().removeClass("maximized");
            this.imgPickerToggleText("Maximize");
        }
    }.bind(this);

    /**
     * Reloads the editor when user selects a sub-page to be edited
     */
    this.reloadContent = function (subkey, url) {
        this.reloadWebsite(url, true);
        var newUrl = window.location.href.split('?')[0];
        newUrl =  newUrl + "?page=" + this.page + "&key=" + this.key + "&subkey=" + (subkey || "") + "&cachekey=" + this.cacheKey  + "_" + subkey;
        window.location = newUrl;
    }.bind(this);

    /**
     * Get skin name
     * @returns {string}
     */
    this.getSkinName = function () {
        switch (sbm.serverconfig.skinId) {
        case 1:
            return "Spin and Win";
        case 2:
            return "Kitty Bingo";
        case 3:
            return "Lucky Pants Bingo";
        case 4:
            return "Direct Slot";
        case 5:
            return "Magical Vegas";
        case 6:
            return "Bingo Extra";
        case 8:
            return "Lucky VIP";
        case 9:
            return "Give Back Bingo";
        case 10:
            return "Regal Wins";
        }
    };

    /**
     * Flush website cache
     */
    this.flushCache = function () {
        sb.notify("editor-flush-cache");
    }.bind(this);

    /**
     * Flush cache request has ended
     * Handles response
     */
    this.onFlushedCache = function (e, data) {
        if (data.Code === 0) {

            sb.notifySuccess("Cache flushed");
            this.reloadWebsite();

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * User selection for checking skin images
     */
    this.showSkinImages = function () {
        this.showSection("SKINIMAGES");
    }.bind(this);

    /**
     * Check skin images
     */
    this.checkSkinImages = function () {
        sb.notify("editor-check-skin-images", [this.checkImgEnvironment(), this.checkImgDevice()]);
    }.bind(this);

    /**
     * Got response from check skin images
     * Handles response
     */
    this.onCheckedSkinImages = function (e, data) {
        if (data.Code === 0) {

            this.rearrangeSkinImages(data.Data);

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Re-arrange the data got from checkskinimages so it can be visualized on the table
     * @param list - list with all the images got on the response for checkskinimages
     */
    this.rearrangeSkinImages= function (list) {
        var pages = [];

        // record to bind on the table
        var PageKey = function (id, key, subkey, images, contentId, environment) {

            this.id = id;
            this.key = key;
            this.subkey = subkey;
            this.images = [];
            this.contentId = contentId;
            this.environment = environment;

            /**
             * only can edit if the environment is not live
             */
            this.edit = function () {
                if (this.environment === "1") {
                    sb.notifyWarning("You're trying to edit a LIVE record which is not allowed");
                } else {
                    window.location.search = "?page=" + id + "&key=" + key + "&subkey=" + subkey;
                }
            }.bind(this);

            images.Image1 && this.images.push({ file: images.Image1 });
            images.Image2 && this.images.push({ file: images.Image2 });
            images.Image3 && this.images.push({ file: images.Image3 });
            images.Image4 && this.images.push({ file: images.Image4 });
            images.Image5 && this.images.push({ file: images.Image5 });
            images.Image6 && this.images.push({ file: images.Image6 });
            images.Image7 && this.images.push({ file: images.Image7 });
        };

        // each page
        var env = this.checkImgEnvironment();
        sb._.each(list, function (page) {
            sb._.each(page.Images, function (images) {
                if (images.Image1 || images.Image2 || images.Image3 ||
                    images.Image4 || images.Image5 || images.Image6 || images.Image7) {

                    // create new record
                    pages.push(new PageKey(page.Name.Page, page.Name.Key, images.Subkey, images, images.ID, env));
                }
            });
        });

        this.skinPages(pages);

        sb.$('.img-zoom').on("click", function() {

            sb.$(this).toggleClass('transition');

        });
    };

    /**
     * Logout
     */
    this.logout = function () {
        sb.notify("editor-do-logout");
    }.bind(this);

    /**
     * Logout
     */
    this.onDoneLogout = function (data) {
        this.reloadEditor();
    }.bind(this);

    /**
     * User selection for checking for changes between LIVE and STAGING
     */
    this.compareLiveAndStaging = function () {
        this.showSection("COMPARE");
        sb.notify("editor-compare-live-staging");
    }.bind(this);

    /**
     * Got response from checking for changes
     * Handles response
     */
    this.onComparedLiveAndStaging = function (e, data) {
        if (data.Code === 0) {

            this.stagingAndLiveModifications(data.Data);

        } else {
            data.Msg && sb.notifyError(data.Msg);
        }
    }.bind(this);

    /**
     * Get Categories for skin request has ended
     * Handles response
     */
    this.onGotCategoriesForSkin = function (e, data) {
        if (data.Code === 0) {
            this.returnedCategories(data.Categories);
        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

    /**
     * User wants to edit a modified content
     */
    this.editModification = function (data) {
        sb.notify("get-category-list", [data.Page, data.Key, data.DetailsURL]);
    }.bind(this);

    /**
    * TERMS AND PRIVACY POLICY
    */

    /**
    * User selection for adding both Term and Conditions and PP
    */
    this.newTcsAndPP = function () {
        this.showSection("TCSANDPP");
        this.termsAndConditions.editMode("new");
        this.privacyPolicy.editMode("new");
        this.updatedFullTcs(false);
    }.bind(this);

    /* Publish terms and conditions and privacy policy to live */
    this.publishTcsAndPP = function () {
        
        if (this.updatedFullTcs()) {

            if (this.termsAndConditions.editMode() === "new")  {
                var content = tinyMCE.get('ui-terms-conditions').getContent();
                content && sb.notify("editor-publish-tcs", content);
            }

            if (this.privacyPolicy.editMode() === "new")  {
                var content = tinyMCE.get('ui-privacy-policy').getContent();
                content && sb.notify("editor-publish-pp", content);
            }            

        } else {

            sb.showWarning("Please make sure you updated the full T&C's");
        }

    }.bind(this);

    /* callbak for publish terms and conditions to live */
    this.onPublishedTCs = function (e, data) {
        if (data.Code === 0) {
            sb.showSuccess("Content synced to live");
        }
    }.bind(this);

    /* callbak for publish privacy policy to live */
    this.onPublishedPP = function (e, data) {
        if (data.Code === 0) {
            sb.showSuccess("Content synced to live");
        }
    }.bind(this);
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var value = data[6] || ""; // use data for the status
            var filter = ( $("#status").val() != "" ? $("#status").val() : "all");
            if ( value.indexOf(filter) != -1  && filter != "all")
            {
                return true;
            }
            else if (filter == "all"){
                return true;
            }
            else if (typeof(filter) == "undefined"){
                return true;
            }
            return false;
        }
    );    

};

function redraw(){
    $(".list").DataTable().draw();

}