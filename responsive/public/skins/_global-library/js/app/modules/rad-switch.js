var sbm = sbm || {};

/**
 * RadSwitch
 * This module is a helper to swtich between versions of the same skin
 * @param sb The sandbox
 * @constructor
 */
sbm.RadSwitch = function (sb) {
    "use strict";

    this.switchModalName = '.ui-switch-modal';
    this.switchBarName = '.ui-switch-bar';
    this.switch3WeeksModalName = '.switch-3-weeks-modal';
    this.startDate = sb.serverconfig.switchStartDate; //new Date('2016', '06', '27', '12', '34', '09'); // yyyy,mm (month-1!!!), dd,hh,mm,ss
    this.endDate = sb.serverconfig.end3WeeksDate; //new Date('2016', '07', '30', '12', '54', '09');   
    this.lastDaysDate = sb.serverconfig.start3WeeksDate; 

    this.surveyModal = "#survey-monkey";

    /**
     * Module initialization
     */
    this.init = function () {

        var today = new Date(); 
        if (this.endDate.getTime() > today.getTime()) {
            if (this.startDate !== "" && !this.oldPlayer(today)) {
                sb.listen("done-login", this.onDoneLogin);

                if (sb.getFromStorage("sessionId")) {
                    this.checkReminders();
                }
            }
        }

        sb.$(this.surveyModal).on("open", function () {
            $("body").addClass("modal-open");
        });
        sb.$(this.surveyModal).on("close", function () {
            $("body").removeClass("modal-open");
        });
        this.showSurveyModal();

    }.bind(this);

    /**
     * Module destruction
     */
    this.destroy = function destroy() {
        sb.ignore("done-login");
    };

    /**
     * set to true if player is funded and has registered before the launch date
     */
    this.oldPlayer = function (registrationDate) {
        if (typeof registrationDate === "undefined") {
            return false;
        }

        var registration = (registrationDate instanceof Date) ? registrationDate : this.parseDate(registrationDate),
            funded = sb.getFromStorage("funded");

        return (funded === "true" && this.startDate > registration);

    }.bind(this);

    this.parseDate = function (dateString) {

        var theDateTime = dateString.substring(0, 19).split(" ");
        var theDate = theDateTime[0];
        var theTime = theDateTime[1];

        theDate = theDate.split("-");
        theTime = theTime.split(":");

        var dateParsed = new Date(theDate[0], (theDate[1] - 1), theDate[2], theTime[0], theTime[1], theTime[2]);
        return dateParsed;
    };


    this.checkReminders = function () {
        if (sb.getFromStorage("sessionId")) {


            var showReminders = sb.getCookie("noReminders");

            if (this.oldPlayer(sb.getCookie("RegistrationDate"))) {
                if (!sb.getCookie("noReminders")) {

                    this.showBar();
                
                    var now = new Date();
                    var start = this.startDate.getTime();

                    // in case today is over the deadline 
                    if(now.getTime() > this.lastDaysDate) {
                        // we display the modal if we didn't do it before and we keep a cookie to now showing it again

                        var switchModalShowed = sb.getCookie("SwitchModalShowed");
                        var switch3WeeksModalShowed = sb.getCookie("switch3WeeksModalShowed");
                        if(localStorage && !switch3WeeksModalShowed){
                            /*
                            $('.ui-3weeks-date').text(this.endDate.getDate() + '.'+(this.endDate.getMonth()+1)+'.'+this.endDate.getFullYear());
                            this.show3WeeksModal();
                            sb.setCookie("switch3WeeksModalShowed", true);*/
                        }

                        // also we calculate the number of days to display in the countdown and we update the message to display them in the bar
                        var daysToSwitch = Math.floor((this.endDate.getTime()-now.getTime())/(60*60*24*1000));
                        daysToSwitch += 1; // count the current day
                        sb.$('.ui-switch-bar-text').text('Switch to our classic site - only available for '+daysToSwitch  +' more day'+ ((daysToSwitch != 1)? 's':''));

                    }
                }
            }
        }
    }.bind(this);

    /**
     * Login request has ended
     * Handles response
     */
    this.onDoneLogin = function (e, data) {
        if (data.Code === 0) {

            var now = new Date();
            var switchModalShowed = sb.getCookie("SwitchModalShowed");
            sb.setCookie("RegistrationDate", data.RegistrationDate);

            // if old player on a desktop

            if (this.oldPlayer(data.RegistrationDate) && sbm.serverconfig.device === 1 && now.getTime() > this.startDate.getTime() && !(now.getTime() > this.lastDaysDate.getTime())) {     
                this.checkReminders();
                if (!switchModalShowed) {
                    this.showModal();
                }    
            }
        }
    }.bind(this);

    /**
     * Closes switchModal
     */
    this.closeModal = function () {
        var $switchModal = sb.$(this.switchModalName);
        if ($switchModal.length > 0) {
            $switchModal.foundation("reveal", "close");
        }
    }.bind(this);

    /**
     * Opens switchModal
     */
    this.showModal = function () {
        var $switchModal = sb.$(this.switchModalName);
        if ($switchModal.length > 0) {
            $switchModal.foundation("reveal", "open");
            sb.setCookie("SwitchModalShowed", true);
        }
    }.bind(this);

    /**
     * Closes switch3WeeksModal
     */
    this.close3WeeksModal = function () {
        var $switchModal = sb.$(this.switch3WeeksModalName);
        if ($switchModal.length > 0) {
            $switchModal.foundation("reveal", "close");
        }
    }.bind(this);

    /**
     * Opens switch3WeeksModal
     */
    this.show3WeeksModal = function () {
        var $switchModal = sb.$(this.switch3WeeksModalName);
        if ($switchModal.length > 0) {
            $switchModal.foundation("reveal", "open");
        }
    }.bind(this);

    /**
     * Opens switch3WeeksModal
     */
    this.showSurveyModal = function () {
        //REMOVE EVERYTHING ONCE THEY HAVE COLLECTED THE DATA
        var $surveyModal = sb.$(this.surveyModal);
        var src = "";

        /*
         Test accounts
         ID		Username
         13018	joseBP
         13017	joseNP
         13016	joseVIP
         13015	joseOP
         */
        var VIPPlayers    = [];//[418287, 418307, 612597, 1014276, 13016, 979529, 333348, 270804, 1035729, 260123, 940845, 468104, 417497, 490571, 367846, 350510, 585958, 969432, 414284, 466669, 542719, 1030139, 381078, 386179, 621788, 271520, 249703, 866504, 390839, 527771, 278718, 487071, 405792, 228011, 466384, 921902, 226586, 989363, 622785, 362716, 372271, 688458, 382358, 251668, 1029440, 441252, 359484, 641163, 886562, 369044, 314364, 609772, 823594, 942976, 452970, 436847, 491237, 405797, 412936, 796062, 490079, 792052, 381714, 783867, 348379, 412778, 380634, 985906, 334994, 284228, 288141, 710395, 429647, 268076, 1001481, 256867, 883321, 264470, 557228, 454909, 463043, 435043, 409679, 427350, 622568, 771329, 840113, 410983, 521134, 284287, 424134, 243344, 658556, 237699, 432518, 413176, 408273, 397672, 361097, 390786, 489296, 439403, 280163, 512084, 382709, 438730,540956,913993,292473,341429,377646,780147 ];
        var OldPlayers    = [];//[13015, 286242, 366149, 902193, 228400, 875403, 967305, 461815, 498097, 792303, 708735, 998143, 546913, 928721, 1009205, 424209, 438197, 846064, 844293, 532020, 918897, 992505, 407390, 383914, 728099, 985692, 489332, 1005220, 282691, 322584, 979165, 386365, 958020, 922158, 401591, 234167, 990667, 463894, 532118, 1008666, 892442, 1006673, 1012565, 837437, 293891, 533019, 816505, 804346, 399193, 967351, 402294, 872891, 688658, 337481, 342911, 793010, 303009, 701326, 301571, 430255, 317160, 226621, 1004827, 453683, 818558, 993686, 256666, 340968, 933963, 604930, 952520, 987434, 821187, 901159, 385569, 818953, 262404, 816881, 278059, 664483, 453528, 884408, 852303, 464536, 284265, 341709, 440852, 273517, 525886, 228341, 243664, 373255, 669259, 250244, 335116, 859260, 405088, 368709, 989679, 383847, 999901, 433656,869614,344248,786291,330623,839321,461071,850081,973772,242130,644212 ];
        var NewPlayers    = [];//[13017, 1031092, 1035879, 1049438, 1031266, 1035999, 1052148, 1017027, 1023111, 1027178, 1038540, 1017915, 1046964, 1030962, 1039530, 1026505, 1026315, 1014236, 1029680, 1016866, 1031280, 1032378, 1045083, 1027462, 1023510, 1031222, 1022609, 1029400, 1030133, 1052311, 1031613, 1031411, 1040777, 1028505, 1049300, 1023685, 1046784, 1025219, 1038471, 1021445, 1052478, 1024888, 1045120, 1017199, 1034772, 1013695, 1038259, 1023185, 1025223, 1038715, 1051696, 1043659, 1029913, 1021983, 1046922, 1048628, 1052773, 1022027, 1054002, 1049672, 1047062, 1053472, 1029359, 1021272, 1030865, 1033550, 1047657, 1017191, 1037272, 1047028, 1028518, 1019158, 1016617, 1042312, 1021329, 1034827, 1047817, 1027038, 1029841, 1042295, 1035880, 1023362, 1052435, 1049814, 1045394, 1054605, 1035067, 1039121, 1016700, 1020769, 1031173, 1032996, 1019225, 1041773, 1050287, 1043226, 1038769, 1028181, 1023309, 1031611, 1021887, 1081146,986789,1054740,1081264,1072117,1080733,1054474,1079258,1060283,1051208,964751,1041028,1051845,1062286,984552,1048038,621178,1061834, 955270,1029580,928355,1068189,989754,1066528,1080047,1079013 ];
        var BingoPlayers  = [];//[13018, 404599, 230377, 346096, 424157, 369394, 516336, 442232, 423363, 692404, 782105, 226732, 385847, 570024, 462179, 234598, 372400, 295812, 412158, 977052, 273695, 968652, 425589, 252104, 815427, 417179, 1054482, 1041756, 372512, 795923, 1031059, 1051077, 247697, 698898, 258457, 1008704, 1043080, 972766, 1005497, 1025537, 918019, 392439, 506411, 1016852, 1007844, 306967, 1016553, 586465, 392182, 305505, 928169, 356923, 1032975, 653026, 226573, 425623, 988618, 998821, 380720, 965241, 1015612, 247959, 536650, 988199, 463614, 643771, 365145, 388626, 796213, 444317, 886518, 1051085, 1033041, 1016573, 1039686, 1003593, 490016, 876399, 792396, 1034694, 962817, 1048404, 466259, 615019, 1047212, 1022875, 1026055, 289512, 1031070, 936147, 1019231, 483837, 628738, 1013551, 1026368, 1030947, 887142, 1001885, 1032935, 1046007, 644369, 1062311, 1053688 ];
        if (_.contains(VIPPlayers, parseInt(sb.$.cookie("PlayerID")))) {
            src = "https://www.surveymonkey.co.uk/r/D2FZBQV";
        } else if (_.contains(OldPlayers, parseInt(sb.$.cookie("PlayerID")))) {
            src = "https://www.surveymonkey.co.uk/r/DG7NVB3";
        } else if (_.contains(NewPlayers, parseInt(sb.$.cookie("PlayerID")))) {
            src = "https://www.surveymonkey.co.uk/r/DGC63CF";
        } else if (_.contains(BingoPlayers, parseInt(sb.$.cookie("PlayerID")))) {
            src = "https://www.surveymonkey.co.uk/r/DGDLJPY";
        }
     
        //check if the variable is set it up
        if ($surveyModal.length > 0 && src !== "" && sb.getFromStorage("surveyMonkey") !== sb.$.cookie("PlayerID")) {
            $surveyModal.find("iframe").attr("src", src);
            $surveyModal.foundation("reveal", "open");
            sb.saveOnStorage("surveyMonkey", sb.$.cookie("PlayerID"));
        }
    }.bind(this);

    /**
     * Closes the switch bar
     */
    this.closeBar = function () {
        sb.$('.switch-top-message').delay(120).animate({ 'top': '-45px', "margin-bottom": '0' }), 2000, "easeOutBounce";
        sb.$(".main-wrap.top-message").animate({"margin-top": '0px'});
    }.bind(this);

    /**
     * Shows the switch bar
     */
    this.showBar = function () {
        sb.$('.switch-top-message').delay(1500).animate({ 'top': '0', "margin-bottom": '40px' }).css({ 'position': 'fixed' }), 2000, "easein";
        sb.$(".main-wrap.top-message").delay(1550).animate({"margin-top": '45px'});
    }.bind(this);  

    /**
     * Stay in the new site
     */
    this.switchModalStay = function () {
        this.closeModal();
    }.bind(this);

    /**
     * Switch to the old site
     */
    this.switchModalSwitch = function () {
        this.closeModal();
        this.switchToOldSite();
    }.bind(this);

    /**
     * No reminders and stay in the new site
     */
    this.switchBarNoReminders = function () {
        this.closeBar();
        sb.setCookie('noReminders', false);
    }.bind(this);

    /**
     * Switch to the old site from bar
     */
    this.switchBarSwitch = function () {
        this.closeBar();
        this.switchToOldSite();
    }.bind(this);


    this.switchToOldSite = function () {
        sb.setCookie('showOldSite', true);
        window.location.replace(sbm.serverconfig.switchOldSiteURL+"/login/"+sb.$.cookie("PlayerID")+"/"+sb.$.cookie("SessionID")); // here we'll need to add the parameters SessionID and PlayerID
    }

};