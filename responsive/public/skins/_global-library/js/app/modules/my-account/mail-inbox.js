var sbm = sbm || {};

/**
 * Player's account data
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.MailInbox = function (sb, options) {
    "use strict";

    this.newMessage = function (message) {

        return message;
    }

    if (sb.getFromStorage("messages")) {
        var  parsedMessages = JSON.parse(sb.getFromStorage("messages"));
        this.messages = sb.ko.observableArray(ko.utils.arrayMap(parsedMessages, function(m) {
            m.MailStatus = sb.ko.observable(m.MailStatus);
            m.Clicked = sb.ko.observable(m.Clicked); 
            m.MessageHtml = sb.ko.observable("");           
            return m;
        }));
        this.unreadMessages = sb.ko.observable(sb.getFromStorage("unreadMessages"));
    } else {
        this.messages = sb.ko.observableArray([]);
        this.unreadMessages = sb.ko.observable(0);
    }
    this.allChecked = sb.ko.observable(false);
    this.expanded = sb.ko.observable(false);
    this.messageHtml = sb.ko.observable("");
    this.messageSubject = sb.ko.observable("");
    this.selectedMessage = sb.ko.observable("");
    this.messagesTimer = null;
    this.messagesDelay = ((((0*60)+0)*60)+30)*1000+0; // ((((hours*60)+min)*60)+seconds)*1000)+milliseconds
    this.templateId = 0;
    this.playerID = 0;

    /**
     * Module initialization
     */
    this.init = function () {

        // exit if is disabled by configuration
        if (!sbm.serverconfig.messageInboxOn) return;

        sb.listen("message-status-updated", this.onMessageUpdated);
        sb.listen("message-got-preview", this.onMessagePreviewed);
        sb.listen("messages-got-inbox", this.onMessagesGotInbox);
        this.getMessagesInbox(false);       
        sb.listen("done-login", this.getMessagesInbox);
        sb.listen("remove-messages", this.removeMessages);
        this.playerID = sb.getFromStorage("playerId");
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("message-status-updated");
        sb.ignore("message-got-preview");
        sb.ignore("messages-got-inbox");
        sb.ignore("remove-messages");
        sb.ignore("done-login");
        window.clearTimeout(this.messagesTimer);
    };



    this.removeMessages = function () {
        sb.removeFromStorage("messages");
    }.bind(this);


    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("message-status-updated");
        sb.ignore("message-got-preview");
        sb.ignore("messages-got-inbox");
        sb.ignore("remove-messages");
        window.clearTimeout(this.messagesTimer);
    }.bind(this);

    this.onLogin = function () {
        this.getMessagesInbox(true);


    }.bind(this);


    /**
    * Updates message status 
    *
    */
    this.updateMessageStatus = function (action, messageId) {
        if (messageId < 0) {
            _.each(this.messages(), function (message) {
                if (message.Id == messageId) {
                    switch(action) {
                        case 'read': 
                            if (message.MailStatus() == 1) {
                                this.unreadMessages(this.unreadMessages()-1);
                            }
                            break;
                        case 'unread':
                            if (message.MailStatus() == 2) {
                                this.unreadMessages(this.unreadMessages()+1);
                            } 
                            break;  
                        case 'deleted':
                            if (message.MailStatus() == 1) {
                                this.unreadMessages(this.unreadMessages()-1);
                            } 
                            break;
                        default: 

                        break;
                    }
                }
            }.bind(this));            
        }
        sb.notify("message-update-status",[action,messageId]);

    }.bind(this);

    /**
     * Get player message inbox
     */
    this.getMessagesInbox = function (isLogin) {
        var sId = sb.getFromStorage("sessionId");
        if (sId !== undefined) {

            var now = new Date(); 
            var lastTime = new Date(sb.getFromStorage("lastTimeMessagesUpdated"));
            var extraActions = JSON.parse(sb.getFromStorage("extraActions"));
            if (extraActions && extraActions.templateId){
                this.templateId = extraActions.templateId;
                delete extraActions.templateId;
                sb.saveOnStorage("extraActions",JSON.stringify(extraActions));
            }

            if ((this.templateId !== 0 && isLogin) || sb.getFromStorage("messages") === "undefined" ){
                sb.notify("messages-get-inbox",this.templateId);
            }
            if (sb.getFromStorage("lastTimeMessagesUpdated") === "undefined"  || (now - lastTime > this.messagesDelay) ) {
                sb.notify("messages-get-inbox",this.templateId);              
                this.messagesTimer = setInterval(function () {
                    sb.notify("messages-get-inbox",this.templateId); 
                }.bind(this), this.messagesDelay);
            } else {
                this.messagesTimer = setInterval(function () {
                    sb.notify("messages-get-inbox",this.templateId); 
                }.bind(this), this.messagesDelay - (now - lastTime));            
            } 
        }
    }.bind(this);

    /**
    *  Updates the player inbox with the received messages
    *  Handles response
    */
    this.onMessagesGotInbox = function(e, data) {
        if (data.Code === 0 ) {
            // so it adds the click property to the messages to be used by ko
            var checkedVal = this.allChecked();
            var unread = 0;
            var vmSelf = this;
            _.each(data.Messages, function (message) {
                    message["Clicked"] = sb.ko.observable(checkedVal);
                    message.MailStatus = sb.ko.observable(message.MailStatus);
                    if (message.Id !== vmSelf.selectedMessage().Id) {
                        message["MessageHtml"] = sb.ko.observable("");   

                    } else {
                        message["MessageHtml"] = vmSelf.selectedMessage().MessageHtml;
                    }
                    if (message.MailStatus() == 1) {
                        unread ++;
                    }
            });
            this.unreadMessages(unread);
            this.messages(data.Messages);            
            //sb.$('#single-message-modal').show();
            //this.revealSingleMessage(this.selectedMessage().Id);
            this.saveMessages();
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);


    /**
    * We save the messages in the local storage
    *
    */
    this.saveMessages = function(){
            sb.saveOnStorage("messages",sb.ko.toJSON(this.messages()));
            var extraActions = JSON.parse(sb.getFromStorage("extraActions"));
            if (extraActions && extraActions.templateId){
                this.templateId = extraActions.templateId;
            }
                
            // reset time of the timer
            // if the last timeframe was less than the by default, we reset the last TimeMessagesUpdated to use the by default the next time
            var now = new Date();
            var lastTime = new Date(sb.getFromStorage("lastTimeMessagesUpdated"));
            if (sb.getFromStorage("lastTimeMessagesUpdated") === "undefined" || now - lastTime < this.messagesDelay) {
                window.clearInterval(this.messagesTimer);
                this.messagesTimer = setInterval(function () {
                    sb.notify("messages-get-inbox",this.templateId); 
                }.bind(this), this.messagesDelay);
            }

            sb.saveOnStorage("unreadMessages", this.unreadMessages());
            sb.saveOnStorage("lastTimeMessagesUpdated", now);
    }.bind(this);

    /**
    * Updates messages status in a batch 
    *
    */
    this.updateClickedMessageStatuses = function (action) {

        var updateService =  this.updateMessageStatus;
        sb.ko.utils.arrayForEach(this.messages(), function (message) {
            if (message.Clicked()) {
                if (action == "read") {
                    sb.sendDataToGoogle("Message Inbox", "PlayerID: " + this.playerID + " - Subject: " + message.Subject + " - Action: markAsRead");
                } else if (action == "unread") {
                    sb.sendDataToGoogle("Message Inbox", "PlayerID: " + this.playerID + " - Subject: " + message.Subject + " - Action: markAsUnRead");
                } else if (action == "deleted") {
                    sb.sendDataToGoogle("Message Inbox", "PlayerID: " + this.playerID + " - Subject: " + message.Subject + " - Action: markAsDelete");
                }
                updateService(action,message.Id);
            }
        }.bind(this));

    }.bind(this);

    /**
    * Closes the modal window that displays the message 
    *
    */
    this.closeMessage = function () {
        this.selectedMessage("");
        sb.sendDataToGoogle("Message Inbox", "PlayerID: " + this.playerID + " - Subject: " + this.messageSubject() + " - Action: closeMessage");
    }.bind(this);

    /**
    * Messages status update 
    * Handles response
    *
    */
    this.onMessageUpdated = function (e, data) {
        if (data.Code === 0) {
            if (data.newStatus > 2) {
                var match = ko.utils.arrayFirst(this.messages(), function(message) {
                    return data.messageId === message.Id;
                });
                this.messages.remove(match);
            } else {
                this.messages(ko.utils.arrayMap(this.messages(), function (message) {
                    if (message.Id == data.messageId) {
                        message.MailStatus(data.newStatus);
                    } 
                    
                    return message;
                    
                }));
            }
            //we also update the account of the unread messages
            if (data.oldStatus !== data.newStatus) {
                if (data.oldStatus === 1) {
                    this.unreadMessages(parseInt(this.unreadMessages())-1);
                } else if (data.newStatus === 1) {
                    this.unreadMessages(parseInt(this.unreadMessages())+1);
                }                
            }

            this.saveMessages();
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
    * It receives the html version of the message to be displayed and it pops the window up
    * Handles response
    *
    */
    this.onMessagePreviewed = function (e,data) {
        if (data.Code === 0) {

            this.messageSubject(this.selectedMessage().Subject);
            this.messageHtml(data.Html);

            sb.sendDataToGoogle("Message Inbox", "PlayerID: " + this.playerID + " - Subject: " + this.messageSubject() + " - Action: openedMessage");
        } else {
            data.Msg && sb.showError(data.Msg);
        }       
    }.bind(this);

    /**
     * Marks as selected or unselected all the messages
     */
    this.selectAllToggle = function () {

        var allCheckedAux = ((this.allChecked() + 1) % 2)==1;
        this.allChecked(allCheckedAux);
        this.messages(ko.utils.arrayMap(this.messages(), function (message) {
            message.Clicked(allCheckedAux);
            return message;
        }));
        return true;

    }.bind(this); 

    /**
    * Reveal window message and renders email template
    * @messageId to reveal
    */
    this.revealSingleMessage = function(messageId) {
        if (this.selectedMessage() && this.selectedMessage().Id === messageId) {
            this.selectedMessage("");
        } else {
            this.selectedMessage("");
            this.selectedMessage(ko.utils.arrayFirst(this.messages(), function(message) {
                        return messageId === message.Id;
                    }));            
            this.updateMessageStatus('read',this.selectedMessage().Id);
            sb.notify("message-get-preview",this.selectedMessage().Id);
        }
    }.bind(this);


    /**
    * Reveal window message and renders email template
    * @singleMessageId - the div id
    * @email template - the template used to reveal the silverpop template
    */   

    this.check = function(data,event) {

        //event.stopPropagation();
        return true;
    }.bind(this);    


    /**
    * Expands/collaps the email list
    */
    this.expandToggle = function() {
        this.expanded(!this.expanded());
        if (!this.expanded()){
            window.scrollTo(0,0);
            return false;
        } 
        return true;

    }.bind(this);

    /**
     * closes a foundation dropdown
     * @param el - the dropdown element
     */
    this.closeDropDown = function (el) {
        sb.$(el).foundation("dropdown", "close", $("[data-dropdown-content]"));
    };

    /**
     * Update selected messages as read
     */
    this.markAsRead = function (vm, e) {
        this.closeDropDown(e.currentTarget);
        this.updateClickedMessageStatuses("read");
    }.bind(this);

    /**
     * Update selected messages as unread
     */
    this.markAsUnread = function (vm, e) {
        this.closeDropDown(e.currentTarget);
        this.updateClickedMessageStatuses("unread");
    }.bind(this);

    /**
     * Delete selected messages
     */
    this.delete = function (vm, e) {
        this.closeDropDown(e.currentTarget);
        this.updateClickedMessageStatuses("deleted");
    }.bind(this);



};
