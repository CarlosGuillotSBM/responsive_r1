var sbm = sbm || {};

/**
 * Claim player's bonus
 * @param sb The sandbox
 * @constructor
 */
sbm.ClaimUpgradeBonus = function (sb, options) {
    "use strict";

    this.ubID = sb.ko.observable("");
    this.ubDate = sb.ko.observable("");
    this.ubAmount = sb.ko.observable("");
    this.ubBonusAmount = sb.ko.observable("");
    this.ubBonusDate = sb.ko.observable("");
    this.msg = sb.ko.observable("");
    this.lastDays = sb.ko.observable(false);
    this.currency = sb.ko.observable(sb.getFromStorage("currencySymbol"));
    this.fullTermsPath = sb.ko.observable("");;

     /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("got-player-upgrade-bonus", this.gotPlayerUpgradeBonus);
        sb.listen("claimed-player-upgrade-bonus", this.claimedPlayerUpgradeBonus); // this data is requested on PlayerAccount
        if (sb.validSession()) {
            this.getPlayerUpgradeBonus();
        }

        switch (sbm.serverconfig.skinId) {
            case 1: // spin and win
                this.fullTermsPath('/loyalty/');
                break;

            case 2: // kittybingo
                this.fullTermsPath('/kitty-club/');
                break;

            case 3: // luckypantsbingo
                this.fullTermsPath('/lucky-club/');
                break;

            case 6: // bingoextra
                this.fullTermsPath('/extra-club/');
                break;

            case 9: // givebackbingo
                this.fullTermsPath('/vip-club/');
                break;

            case 12: // aspers
                this.fullTermsPath('/loyalty/');
                break;

            default:
                this.fullTermsPath('/loyalty/vip-club/');
                break;
        }
    };

     /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("got-player-upgrade-bonus");
    };

     /**
     * Requests the player's upgrade bonus
     */
    this.getPlayerUpgradeBonus = function () {
        sb.notify("get-player-upgrade-bonus");

    }.bind(this);

    /**
     * Redeems a given player's upgrade bonus
     */
    this.claimUpgradePlayerBonus = function () {
        sb.notify("claim-player-upgrade-bonus",[this.ubID()]);

    }.bind(this);


    /**
     * You receive the upgrade bonus
     * Handles response
     */
    this.gotPlayerUpgradeBonus = function (e, data) {
        if (data.Code === 0) {
           this.ubID(data.UBID);
            this.ubDate(data.UBDate);
            this.ubAmount(data.UBAmount);
            var date1 = new Date();
            var date2 = new Date(data.UBDate);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            this.lastDays(diffDays<2);
        } else {

            // no upgrade bonus
           this.ubID("");
            this.ubDate("");
            this.ubAmount("");
        }
    }.bind(this);

    /**
     * Once that you redeem the upgrade bonus you update the upgrade bonus variables
     * Handles response
     */
    this.claimedPlayerUpgradeBonus = function (e, data) {
        if (data.Code === 0) {
         	this.ubDate("");
            this.ubAmount(0);
            this.ubBonusAmount(data.UBClaimAmount);
            this.ubBonusDate(data.UBClaimDate);
            this.lastDays(false);
        } else {
           this.ubBonusAmount("");
            this.ubBonusDate("");
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

     /**
     * Hide upgrade bonus panel, deleting the upgrade bonus ID
     */
    this.hidePanel = function () {
        this.ubID(0);
        this.ubDate("");
        this.ubAmount(0);
        this.ubBonusAmount(0);
        this.ubBonusDate("");
        this.lastDays(false);
    }.bind(this);

    this.showInfo = function() {
        var msg = 'VIP Upgrade bonus – Gold players receive a slots bonus 25x WR with a max ' +
                  'convertible to real amount of x5  - Ruby and Emerald players receive an ALL ' +
                  'Games Bonus 15x with a max convertible x10. Full T&Cs ' +
                  '<a href="' + this.fullTermsPath() + '">here</a>';

         //TC's specific for Aspers
                  if (sbm.serverconfig.skinId === 12 ) {

                      msg = 'VIP Upgrade bonus – Ruby and Emerald players receive an All Games Bonus with ' +
                      '10x WR with a max convertible to real amount of x10. ' +
                      'Full T&Cs ' +
                      '<a href="' + this.fullTermsPath() + '">here</a>';

                  }

    	sb.showInfo(msg);
    }.bind(this);

};





