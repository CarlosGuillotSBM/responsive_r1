var sbm = sbm || {};

/**
 * Personal details management
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.PersonalDetailsNew = function (sb, options) {
    "use strict";

    this.title = sb.ko.observable("");
    this.firstName = sb.ko.observable("");
    this.lastName = sb.ko.observable("");
    this.email = sb.ko.observable("");
    this.securityAnswer = sb.ko.observable("");
    this.newPassword = sb.ko.observable("");
    this.repeatPassword = sb.ko.observable("");
    this.address1 = sb.ko.observable("");
    this.address2 = sb.ko.observable("");
    this.city = sb.ko.observable("");
    this.state = sb.ko.observable("");
    this.postcode = sb.ko.observable("");
    this.countryName = sb.ko.observable("");
    this.landPhone = sb.ko.observable("");
    this.landPhoneExt = sb.ko.observable("");
    this.mobilePhone = sb.ko.observable("");
    this.wantsEmail = sb.ko.observable("");
    this.wantsEmailBoxVisible = sb.ko.observable("");
    this.wantsSMS = sb.ko.observable("");
    this.wantsSMSBoxVisible = sb.ko.observable("");
    this.wantsPhone = sb.ko.observable("");
    this.currentPassword = sb.ko.observable("");
    this.securityQuestions = sb.ko.observableArray([]);
    this.securityQuestion = sb.ko.observable();
    this.securityQuestionNew = sb.ko.observable();
    this.invalidCurrentPassword = sb.ko.observable(false);
    this.invalidNewPassword = sb.ko.observable(false);
    this.invalidSecurityAnswer = sb.ko.observable(false);
    this.passwordsDontMatch = sb.ko.observable(false);
    this.UsernameAndPassTheSame = sb.ko.observable(false);
    this.invalidMobile = sb.ko.observable(false);
    this.invalidUkMobile = sb.ko.observable(false);
    this.mobileVerified = sb.ko.observable(false);
    this.mobileVerificationVisible = sb.ko.observable(false);
    this.verificationCode = sb.ko.observable("");
    this.receivedPersonalDetails = sb.ko.observable(false);
    this.invalidSecurityQuestion = sb.ko.observable(false);

    //store subscriptions in array
    this.subscriptions = [];

    /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("got-personal-details", this.onGotPersonalDetails);
        sb.listen("saved-personal-details", this.onSavedPersonalDetails);
        sb.listen("resend-verification-code-finished", this.onResendFinished);
        sb.listen("verify-mobile-finished", this.onVerifyFinished);
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("got-personal-details");
        sb.ignore("saved-personal-details");
        sb.ignore("resend-verification-code-finished");
        sb.ignore("verify-mobile-finished");
    };

    /**
     * Get player's personal details
     */
    this.getPersonalDetails = function () {        
        sb.notify("get-personal-details");
    }.bind(this);

    /**
     * Validates the new password
     */
    this.isValidPassword = function () {
        var pwd = this.newPassword();
        if (pwd === "") {
            return true;
        }
        return (/^(?=.*?\d)(\w|[!@\^#\$%\&\*])+$/i).test(pwd);
    };

    /**
     * Validates the security answer
     */
    this.isValidSecurityAnswer = function () {
        var answer = this.securityAnswer();
        return answer !== "";
    };


    /**
     * Validates the security question
     */
    this.isValidSecurityQuestion = function () {
        
        return parseInt(this.securityQuestion().trim()) > 0;
    };
    

    this.validatesMobile = function () {
        var number = this.mobilePhone();
        var country = this.countryName().toUpperCase();

        //remove empty spaces
        country = country.replace(/ /g, "");

        if (country === "UNITEDKINGDOM") {
            this.invalidUkMobile(!number.match(/^(0044|\+44|)07([1-5]|[7-9])\d{8}$/));
        } else {
            this.invalidMobile(!number.match(/^(\+|)\d*$/));
        }
    };




    /**
     * Save player's details
     * Validation and request
     */
    this.updateDetails = function () {

        // validation
        this.invalidCurrentPassword(!this.currentPassword());
        this.invalidNewPassword(!this.isValidPassword());
        this.passwordsDontMatch(this.newPassword() !== this.repeatPassword());
        this.invalidSecurityAnswer(!this.isValidSecurityAnswer());
        this.UsernameAndPassTheSame(sb.getFromStorage("username") === this.newPassword());
        this.invalidSecurityQuestion(!this.isValidSecurityQuestion());
        this.validatesMobile();

        if (this.invalidCurrentPassword() || this.passwordsDontMatch() || this.invalidUkMobile() || this.invalidUkMobile() || this.invalidSecurityAnswer() || this.invalidSecurityQuestion()) {
            return;
        }

        // request

        var request = {
            currentPassword: this.currentPassword(),
            newPassword: this.newPassword(),
            landPhone: this.landPhone(),
            landPhoneExt: this.landPhoneExt(),
            mobilePhone: this.mobilePhone(),
            securityQuestion: this.securityQuestion(),
            securityAnswer: this.securityAnswer(),
            wantsEmail: !!parseInt(this.wantsEmail()),
            wantsSMS: !!parseInt(this.wantsSMS()),
            wantsPhone: !!parseInt(this.wantsPhone())
        };

        sb.notify("save-personal-details", [request]);

    }.bind(this);

    /**
     * Get player's details request has ended
     * Handles response
     */
    this.onGotPersonalDetails = function (e, data) {
        if (device !== "web") {
            for(var i in this.subscriptions) {
                this.subscriptions[i].dispose(); //no longer want notifications
            }
        }
        
        if (data.Code === 0) {
            this.receivedPersonalDetails(true);
            this.title(data.Title);
            this.firstName(data.FirstName);
            this.lastName(data.LastName);
            this.email(data.Email);
            this.securityAnswer(data.SecurityAnswer);
            this.address1(data.Address1);
            this.address2(data.Address2);
            this.securityQuestion(data.SecurityQuestionID);
            this.securityQuestionNew(data.SecurityQuestionID.toString())
            this.city(data.City);
            this.state(data.State);
            this.postcode(data.Postcode);
            this.countryName(data.CountryName);
            this.landPhone(data.LandPhone);
            this.landPhoneExt(data.LandPhoneExt);
            this.mobilePhone(data.MobilePhone);
            this.wantsEmail(data.WantsEmail);
            this.wantsEmailBoxVisible(!!data.WantsEmail);
            this.wantsSMS(data.SendSMS);
            this.wantsSMSBoxVisible(!!data.SendSMS);
            this.securityQuestions(data.Security);
            this.mobileVerified(!!data.MobileVerified);
            this.wantsPhone(data.WantsPhone);

            // check device
            var device = sb.getDevice();
            if (device === "web") {
                this.transformDropdown();
            } else {
                this.subscriptions.push(this.securityQuestion.subscribe(function(newValue){
                    this.securityAnswer("");
                }.bind(this)));
            }

            /*
            sb.$('#cd-dropdown').dropdown( {
                                            gutter : 44,
                                            stack : false,
                                            delay : 100,
                                            slidingIn : 100
                                        } );
*/
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Save player's details has ended
     * Handles response
     */
    this.onSavedPersonalDetails = function (e, data) {
        if (data.Code === 0) {

            sb.showSuccess(sb.i18n("updatedRecords"));

            // get details again
            this.getPersonalDetails();

        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Toggle form mobile verification visibility
     */
    this.showMobileVerification = function () {
        this.mobileVerificationVisible(!this.mobileVerificationVisible());
    }.bind(this);

    /**
     * Send mobile verification code to server to verify player's mobile phone
     */
    this.verifyMobile = function () {
        var code = this.verificationCode();
        if (code) {
            sb.notify("verify-mobile", [code]);
        } else {
            sb.showWarning(sb.i18n("invalidCode"));
        }
    }.bind(this);

    /**
     * Request to send again the verification code to player's mobile phone
     */
    this.resendVerificationCode = function () {
        sb.notify("resend-verification-code");
    }.bind(this);

    /**
     * Re-send verification code request has ended
     * Handles response
     */
    this.onResendFinished = function (e, data) {
        if (data.Code === 0) {
            sb.showSuccess(sb.i18n("resendSuccess"));
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Verification request has ended
     * Handles response
     */
    this.onVerifyFinished = function (e, data) {
        if (data.Code === 0) {
            sb.showSuccess(sb.i18n("mobileVerified"));
            // hide verification form
            this.showMobileVerification(false);
            this.mobileVerified(true);
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);


    /**
    * Transforms the dropdown box using the dropdown jQuery plugin
    *
    */
    this.transformDropdown = function (element) {
        sb.$('#cd-dropdown').dropdown( {
                                        gutter : 0,
                                        stack : false,
                                        delay : 100,
                                        slidingIn : 100,
                                        obj: this,
                                        initialZ: 3000,
                                        onOptionSelect: function(opt) { 
                                                                //this.obj.securityQuestion(opt[0].attributes["data-value"].value);
                                                                //this.obj.securityAnswer("");        
                                                            }
                                    } );
        if (sb.$('#cd-dropdown-email-bonus')) {
            sb.$('#cd-dropdown-email-bonus').dropdown( {
                                            gutter : 0,
                                            stack : false,
                                            delay : 100,
                                            slidingIn : 100,
                                            obj: this,
                                            initialZ: 2000,
                                            onOptionSelect: function(opt) { 
                                                                    //this.obj.wantsEmail(opt[0].attributes["data-value"].value);
                                                                            }
                                        } );  
        }
        if (sb.$('#cd-dropdown-sms-bonus')) {
            sb.$('#cd-dropdown-sms-bonus').dropdown( {
                                            gutter : 0,
                                            stack : false,
                                            delay : 100,
                                            slidingIn : 100,
                                            obj: this,
                                            initialZ: 1000,
                                            onOptionSelect: function(opt) { 
                                                                    //this.obj.wantsSMS(opt[0].attributes["data-value"].value);
                                                                            }
                                        } ); 
        }                                         
    }.bind(this);

    /**
    * Toggles the value of wants SMS
    */
    this.toggleSMS = function () {
        if (this.wantsSMS() == 0){
            this.wantsSMS("1");
        } else {
            this.wantsSMS("0");
        }
    }.bind(this);

    /**
    * Toggles the value of want Email
    */
    this.toggleEmail = function () {
        if (this.wantsEmail() == 0){
            this.wantsEmail("1");
        } else {
            this.wantsEmail("0");
        }
    }.bind(this);


    this.selectOption = function(option, item) {
        var questionId = this.securityQuestionNew();
        if (option.attributes["value"].value.trim() == questionId) {
            var typ = document.createAttribute("selected");
            typ.value = "selected";
            option.attributes.setNamedItem(typ);
        }
       // ko.applyBindingsToNode(option, {disable: item.value == null && !optional}, item);
    }.bind(this);  

};