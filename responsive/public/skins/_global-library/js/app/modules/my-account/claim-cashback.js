var sbm = sbm || {};

/**
 * Claim player's cashback
 * @param sb The sandbox
 * @constructor
 */
sbm.ClaimCashback = function (sb, options) {
    "use strict";

    this.cbID = sb.ko.observable("");
    this.cbDate = sb.ko.observable("");
    this.cbAmount = sb.ko.observable("");
    this.cbClaimAmount = sb.ko.observable("");
    this.cbClaimDate = sb.ko.observable("");
    this.msg = sb.ko.observable("");
    this.lastDays = sb.ko.observable(false);
    this.currency = sb.ko.observable(sb.getFromStorage("currencySymbol"));
    this.fullTermsPath = sb.ko.observable("");;

    /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("got-player-cashback", this.gotPlayerCashback);
        sb.listen("claimed-player-cashback", this.claimedPlayerCashback); // this data is requested on PlayerAccount
        if (sb.validSession()) {
            this.getPlayerCashback();
        }

        switch (sbm.serverconfig.skinId) {
            case 1: // spin and win
                this.fullTermsPath('/loyalty/');
                break;

            case 2: // kittybingo
                this.fullTermsPath('/kitty-club/');
                break;

            case 3: // luckypantsbingo
                this.fullTermsPath('/lucky-club/');
                break;

            case 6: // bingoextra
                this.fullTermsPath('/extra-club/');
                break;

            case 9: // givebackbingo
                this.fullTermsPath('/vip-club/');
                break;

            case 12: // aspers
                this.fullTermsPath('/loyalty/');
                break;

            default:
                this.fullTermsPath('/loyalty/vip-club/');
                break;
        }
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("got-player-cashback");
        sb.ignore("redeemed-player-cashback");
    };

    /**
     * Requests the player's cashback
     */
    this.getPlayerCashback = function () {

        sb.notify("get-player-cashback");

    }.bind(this);

    /**
     * Redeems a given player's cashback
     */
    this.claimPlayerCashback = function () {
        sb.notify("claim-player-cashback",[this.cbID()]);

    }.bind(this);

    /**
     * You receive the cashback
     * Handles response
     */
    this.gotPlayerCashback = function (e, data) {
        if (data.Code === 0) {
            this.cbID(data.CBID);
            this.cbDate(data.CBDate);
            this.cbAmount(data.CBAmount);
            var date1 = new Date();
            var date2 = new Date(data.CBDate);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            this.lastDays(diffDays<2);
        } else {
            // no cashback
            this.cbID("");
            this.cbDate("");
            this.cbAmount("");
        }
    }.bind(this);

    /**
     * Once that you redeem the cashback you update the cashback variables
     * Handles response
     */
    this.claimedPlayerCashback = function (e, data) {
        if (data.Code === 0) {
            this.cbDate("");
            this.cbAmount(0);
            this.cbClaimAmount(data.CBClaimAmount);
            //this.cbAmount(data.CBClaimAmount);
            this.cbClaimDate(data.cbClaimDate);
            this.lastDays(false);
        } else {
            this.cbClaimAmount("");
            this.cbClaimDate("");
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Hide claimCashback panel, deleting the casback ID
     */
    this.hidePanel = function () {
        this.cbID(0);
        this.cbDate("");
        this.cbAmount(0);
        this.cbClaimAmount(0);
        this.cbClaimDate("");
        this.lastDays(false);
    }.bind(this);

    
    this.showInfo = function(playerClass, e) {

      //Loyalty Reward message
      var msg = 'Congratulations! You’ve gained exclusive access to your loyalty reward! ' +
                'Please note wagering requirements may apply. ' +
                'For full T&C\'s click ' +
                '<a href="' + this.fullTermsPath() + '">here</a>';


      switch (Number(playerClass)) {

          //VIP class Ruby
          case 4:

                // Cashback TC's for LP KB and BE
                if (sbm.serverconfig.skinId === 3 || sbm.serverconfig.skinId === 2 || sbm.serverconfig.skinId === 6 ) {

                    msg = 'Congratulations! You’ve gained exclusive access to your loyalty reward! ' +
                          'Please note wagering requirements may apply. ' +
                          'For full T&C\'s click ' +
                          '<a href="' + this.fullTermsPath() + '">here</a>';
                }

                //Kickback TC's specific for Aspers
                if (sbm.serverconfig.skinId === 12 ) {

                    msg = 'Congratulations! You’ve gained exclusive access to your loyalty reward! ' +
                          'Please note wagering requirements may apply. ' +
                          'For full T&C\'s click ' +
                          '<a href="' + this.fullTermsPath() + '">here</a>';
                }
              break;

          //VIP class Emerald
          case 5:

               // Cashback TC's for LP KB and BE
               if (sbm.serverconfig.skinId === 3 || sbm.serverconfig.skinId === 2 || sbm.serverconfig.skinId === 6 ) {

                    msg = 'Congratulations! You’ve gained exclusive access to your loyalty reward! ' +
                          'Please note wagering requirements may apply. ' +
                          'For full T&C\'s click ' +
                          '<a href="' + this.fullTermsPath() + '">here</a>';
                }

                 //Loyalty Reward TC's specific for Aspers 
                 if (sbm.serverconfig.skinId === 12 ) {

                    msg = 'Congratulations! Claim your exclusive daily VIP Loyalty Reward on losses. ' +
                    '10x wagering. Emerald max convertible to real amount of £1000. 18+. ' +
                    'For full T&C\'s click ' +
                    '<a href="' + this.fullTermsPath() + '">here</a>';
                }
              break;
      }

    sb.showInfo(msg);
  }.bind(this);

};