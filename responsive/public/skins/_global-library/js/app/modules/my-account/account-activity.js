var sbm = sbm || {};

/**
 * Player's activity on the website
 * Shows: game, banking and promo activities
 * @param sb The sandbox
 * @param options Any options needed (optional)
 * @constructor
 */
sbm.AccountActivity = function (sb, options) {
    "use strict";

    /**
     * Refresh the tab using the button
     */
    this.refresh = function () {
        if(this.filter() == 'games') {
            this.showGame();
        }
        if(this.filter() == 'cashier') {
            this.showBanking();
        }
        if(this.filter() == 'promos') {
            this.showPromo();
        }
    }.bind(this);

    /**
     * Request data
     * @param goTo - table navigation option (first/previous/next/last)
     */
    this.getData = function (goTo) {

        // pagination validation
        if (goTo === "first" && !this.canGoToFirst()) return;
        if (goTo === "previous" && !this.canGoToPrevious()) return;
        if (goTo === "next" && !this.canGoToNext()) return;
        if (goTo === "last" && !this.canGoToLast()) return;

        // defaults
        this.start(0);

        // set start record
        if (goTo === "first") {
            this.start(0);
        } else if (goTo === "previous") {
            this.start(this.pagination.start - this.pageSize);
        } else if (goTo === "next") {
            this.start(this.pagination.start + this.pageSize);
        } else if (goTo === "last") {
            this.start(Math.floor(this.pagination.total / this.pageSize) * this.pageSize);
        }

        this.currentPage(Math.ceil(this.start() / this.pageSize) + 1);
        // get banking or game activity
        if (this.filterEndDate()) {
            
           var date =  $('#fromDate').val().replace(/-/g, "");
           var endDate =  $('#toDate').val().replace(/-/g, "");


             this.transactionSummary(date,endDate);
            if (date.length !== 8) {
                sb.$("#details-container .ui-date-picker").addClass("error");
            } else {
                sb.$("#details-container .ui-date-picker").removeClass("error");
                var request = {
                    date: date,
                    endDate: endDate,
                    start: this.start(),
                    length: this.pageSize,
                    sort: 0,
                    dir: "desc",
                    search: this.filterString(),
                    types: this.filter()
                };

                if (this.filter() === this.filters.cashier) {
                    sb.notify("get-banking-activity", [request]);
                } else if (this.filter() === this.filters.games) {
                    sb.notify("get-games-activity", [request]);
                } else if (this.filter() === this.filters.promo) {
                    sb.notify("get-promo-activity");
                }
            }
        }

    }.bind(this);

    // Properties

    this.gameVisible = sb.ko.observable(true);
    this.promoVisible = sb.ko.observable(false);
    this.bankingVisible = sb.ko.observable(false);
    this.depositVisible = sb.ko.observable(false);
    this.netDeposit = sb.ko.observable("");
    this.filterDate = sb.ko.observable();
    this.filterEndDate = sb.ko.observable();
    this.filterString = sb.ko.observable("").extend({ rateLimit: 1000 });
    this.filterString.subscribe(this.getData);
    this.filter = sb.ko.observable("games");
    this.filter.subscribe(this.getData);
    this.start = sb.ko.observable(0);
    this.filterEndDate.subscribe(this.getData);
    this.noRecords = sb.ko.observable("");
    this.weeklyFilter = sb.ko.observable("");
    this.gameRecords = sb.ko.observableArray([]);
    this.promoRecords = sb.ko.observableArray([]);
    this.bankingRecords = sb.ko.observableArray([]);
    this.canGoToFirst = sb.ko.observable(false);
    this.canGoToPrevious = sb.ko.observable(false);
    this.canGoToNext = sb.ko.observable(false);
    this.canGoToLast = sb.ko.observable(false);
    this.pagination = {};
    this.currencySymbol = sb.getFromStorage("currencySymbol") || "";
    this.currentPage = sb.ko.observable("");
    this.totalPages = sb.ko.observable("");
    this.pageSize = 10;
    this.filters = {
        all : "all",
        games: "games",
        promo: "promos",
        cashier: "cashier",
        deposit: "deposit"
    };
    this.request = {
        startDate: this.filterDate(),
        endDate: this.filterEndDate(),
        start: this.start(),
        length: 5,
        sort: 0,
        dir: "desc",
        search: this.filterString(),
        types: this.filter()
    };
    this.promoDetailToShow = sb.ko.observable("");
    this.promoIdToVoid = sb.ko.observable("");

    this.shouldShowTransactions = sb.ko.observable(false);
    this.gameTotalWagers = sb.ko.observable(0).extend({ numeric: 2 });
    this.gameTotalWins = sb.ko.observable(0).extend({ numeric: 2 });
    this.gameTotalWinsLoss = sb.ko.observable(0).extend({ numeric: 2 });
    this.bankingTotalDeposits = sb.ko.observable(0).extend({ numeric: 2 });
    this.bankingTotalWithdrawals = sb.ko.observable(0).extend({ numeric: 2 });
    this.bankingNetTotal = sb.ko.observable(0).extend({ numeric: 2 });

    // Functions

    /**
     * Module initialization
     */
    this.init = function () {

        sb.listen("got-banking-activity", this.onGotBankingData);
        sb.listen("got-games-activity", this.onGotGamesData);
        sb.listen("got-promo-activity", this.onGotPromoData);
        sb.listen("got-voided-bonus", this.onVoidedBonus);
        sb.listen("got-net-deposit", this.onNetDeposit);
        sb.listen("got-transaction-summary", this.onTransactionSummaryData);

         // initialize jquery UI date picker
        $("#fromDate").datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function () {
                    var startDate = $(this).datepicker('getDate');
                    var toDate = $('#toDate');

                    /* add 6 days to current selected date */
                    startDate.setDate(startDate.getDate() + 7);

                    /* Gets the start date */
                    var minDate = $(this).datepicker('getDate');

                    /* activate end datepicker after user chose start date */
                    toDate.attr('disabled', false);
                    /* first day which can be selected in toDate is selected date in fromDate */
                    toDate.datepicker('option', 'minDate', minDate);

                    /* sets toDate maxDate to the last day of 6 (7 including current) days window */
                    toDate.datepicker('option', 'maxDate', startDate);

                    /* same for fromDate */
                    $(this).datepicker('option', 'minDate', new Date(2007, 1 - 1, 1));

                    /* Add value to the select an end date field after enabled */
                    $('#toDate').val('Select an end date');

                }
            });

            /* End date format */
            $('#toDate').datepicker({
                dateFormat: "yy-mm-dd"
            });
        
    };

    /**
     * Module destructor
     */
    this.destroy = function () {
        sb.ignore("got-bankingactivity");
        sb.ignore("got-games-activity");
        sb.ignore("got-promo-activity");
        sb.ignore("got-voided-bonus");
        sb.ignore("got-net-deposit");
        sb.ignore("got-transaction-summary");
    };

    this.transactionSummary = function(date, endDate) {
        var request = {
            startDate: date,
            endDate: endDate
        };     
        sb.notify("get-transaction-summary",[request]);
    }.bind(this);

     this.onTransactionSummaryData = function(e, data) {
       this.shouldShowTransactions(true);
       this.gameTotalWagers(data.Wagers);
       this.gameTotalWins(data.Wins);
       this.gameTotalWinsLoss(data.Wagers + data.Wins);
       this.bankingTotalDeposits(data.Deposits);
       this.bankingTotalWithdrawals(data.CompletedWithdrawals);
       this.bankingNetTotal(data.Deposits - data.CompletedWithdrawals);
       
    }.bind(this);
    

    /**
     * Show game activity panel
     */
    this.showGame = function () {
        this.noRecords("");
        this.filterString("");
        this.filter(this.filters.games);
        this.promoVisible(false);
        this.bankingVisible(false);
        this.depositVisible(false);
        this.gameVisible(true);
    }.bind(this);

    /**
     * Show promo activity panel
     */
    this.showPromo = function () {
        this.noRecords("");
        this.filterString("");
        this.filter(this.filters.promo);
        this.gameVisible(false);
        this.bankingVisible(false);
        this.depositVisible(false);
        this.promoVisible(true);
        this.getPromoActivity();
    }.bind(this);

    /**
     * Request promo activity
     */
    this.getPromoActivity = function () {
        sb.notify("get-promo-activity");
    };

    /**
     * Voids the txPromo if possible
     */
    this.voidBonus = function (id) {
        this.promoIdToVoid(id);     
        var promo = this.getPromoById(id);
        sb.showConfirm(sb.i18n("voidBonusConfirmation",[promo.Bonus,promo.BonusWins]),this.onVoidLimitsAccept,this.onVoidLimitsCancel);
    }.bind(this);


    /**
     * Voids the txPromo if possible
     */
    this.onVoidLimitsAccept = function () {
        sb.notify("void-bonus", this.promoIdToVoid());
    }.bind(this);

    /**
     * Voids the txPromo if possible
     */
    this.onVoidLimitsCancel = function () {
    };

    /**
     * Show banking activity panel
     */
    this.showBanking = function () {
        this.noRecords("");
        this.filterString("");
        this.filter(this.filters.cashier);
        this.gameVisible(false);
        this.promoVisible(false);
        this.depositVisible(false);
        this.bankingVisible(true);
    }.bind(this);

    /**
    * Show deposits activity panel
    **/
    this.showDeposit = function () {
        this.gameVisible(false);
        this.promoVisible(false);
        this.bankingVisible(false);
        this.depositVisible(true);
    }.bind(this);


    /**
     * Banking activity request has ended
     * Handles response
     */
    this.onGotBankingData = function (e, data) {

        if (data.Code === 0) {

            this.noRecords("");
            this.bankingRecords(data.Data);
            this.setPagination(data.Start, data.Returned, data.Total);
        
        } else if (data.Code === 1) {

            this.bankingRecords([]);
            this.noRecords(data.Msg);

        } else {
            data.Msg && sb.showError(data.Msg);
        }

    }.bind(this);

    this.shouldShowMessage = ko.observable(true); // Message initially visible
    this.showResults = ko.observable(false); // Message initially visible
    
    this.showNetDeposit = function () {
        this.shouldShowMessage(false);
        this.showResults(true);
        sb.notify("get-net-deposit", sb.getFromStorage("playerId"));
    }.bind(this);


    this.onNetDeposit = function (e,data) {

        if (data.Code === 0) {
            this.netDeposit(this.currencySymbol + data.Amount);
        } else {
            this.netDeposit(data.Msg);
        }
    }.bind(this);

    

    /**
     * Games activity request has ended
     * Handles response
     */
    this.onGotGamesData = function (e, data) {

        if (data.Code === 0) {

            this.noRecords("");
            this.gameRecords(data.Data);
            this.setPagination(data.Start, data.Returned, data.Total);

        } else if (data.Code === 1) {

            this.gameRecords([]);
            this.noRecords(data.Msg);

        } else {
            data.Msg && sb.showError(data.Msg);
        }

    }.bind(this);

    /**
     * Promo activity request has ended
     * Handles response
     */
    this.onGotPromoData = function (e, data) {

        if (data.Code === 0) {
            var promos = [];
            _.each(data.Data, function (promo) {

                promo.PromoDescription = promo.PromoDescription.replace("\\n", "<br/>", "g");
                promo.PromoDescription = promo.PromoDescription.replace("\\n", "<br/>", "g");
                promo.MoreGames = false;
                var games = promo.Games.split(",");
                if (games.length > 40) {
                    promo.Games = games.slice(0, 40).join(", ");
                    promo.MoreGames = games.slice(40).join(", ").trim();
                }
                promos.push(promo);
            });

            this.promoRecords(promos);
            _.each(promos, function(promo) {
                sb.$(".ui-show-more-"+promo.ID).on("click", function(){
                    if ($(this).hasClass('active')) {
                        $(this).html("SHOW MORE");
                        $(this).prev().fadeOut(1000);
                        var scrollUpTo = sb.$(this).prev();
                        if (scrollUpTo) {
                            $("html, body").animate({ scrollTop: scrollUpTo.position().top - 120 }, 1000);
                        }
                        $(this).removeClass('active');
                    } else {
                        $(this).html("SHOW LESS");
                        $(this).prev().fadeIn(1000);
                        $(this).addClass('active');
                    }
                });
            });
        } else if (data.Code === 1) {

            this.promoRecords([]);
            this.noRecords(data.Msg);

        } else {
            data.Msg && sb.showError(data.Msg);
        }

    }.bind(this);


    /**
     * Returns the promo info using the id to filter
     */
    this.getPromoById = function (id) {
        var promos = this.promoRecords();

        var promo = _.find(promos, function (p){
            return p.ID === id;
        });

        if (promo) return promo;

        return -1;
    };

    /**
     * Remove one element from the promos array by ID
     *
     * @param id The element to be removed
     */
    this.removePromoById = function(id) {
        this.promoRecords.remove(this.getPromoById(id));
    };

    this.onVoidedBonus = function (e, data) {
        if (data.Code === 0) {
            data.Msg && sb.showInfo(data.Msg);
            //we should delete or cancel the promo here (visually)
            this.removePromoById(this.promoIdToVoid());
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Show the full data for a particular record of promo history
     * @param id - the record Id
     */
    this.getPromoFullDetail = function (id) {
        if (this.promoDetailToShow() === id) {
            this.promoDetailToShow("");
        } else {
            this.promoDetailToShow(id);
        }
    }.bind(this);

    /**
     * Set table page navigation
     * @param start
     * @param returned
     * @param total
     */
    this.setPagination = function (start, returned, total) {
        var pages = Math.ceil(total / this.pageSize);
        this.totalPages(pages);
        this.pagination = {
            start: start,
            returned: returned,
            total: total
        };
        this.canGoToFirst(start !== 0);
        this.canGoToPrevious(start - returned >= 0);
        this.canGoToNext(start + returned < total);
        this.canGoToLast(start + returned !== total);
    };

};