var sbm = sbm || {};

/**
 * Personal details management
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.PersonalDetails = function (sb, options) {
    "use strict";

    var self = this;

    this.title = sb.ko.observable("");
    this.gender = sb.ko.observable("");
    this.firstName = sb.ko.observable("");
    this.lastName = sb.ko.observable("");
    this.email = sb.ko.observable("");
    this.securityAnswer = sb.ko.observable("");
    this.newPassword = sb.ko.observable("");
    this.repeatPassword = sb.ko.observable("");
    this.address1 = sb.ko.observable("");
    this.address2 = sb.ko.observable("");
    this.city = sb.ko.observable("");
    this.state = sb.ko.observable("");
    this.postcode = sb.ko.observable("");
    this.countryName = sb.ko.observable("");
    this.landPhone = sb.ko.observable("");
    this.landPhoneExt = sb.ko.observable("");


    var onAllCommsChange = function (newVal) {

        if (newVal) {
            // set all cb to true
            this.wantsEmail(true);
            this.wantsSMS(true);
            this.wantsPhone(true);
            this.wantsMail(true);

            this.thirdPartyEmail(true);
            this.thirdPartySMS(true);
            this.thirdPartyPhone(true);
        } else {
            this.wantsEmail(false);
            this.wantsSMS(false);
            this.wantsPhone(false);
            this.wantsMail(false);

            this.thirdPartyEmail(false);
            this.thirdPartySMS(false);
            this.thirdPartyPhone(false);
        }

    }.bind(this);

    var onWantsEmailChange = function (newVal) {
        if (sbm.serverconfig.skinId != 12 ) {
             this.thirdPartyEmail(newVal);
        }
       
        if(newVal) {
            if(this.wantsSMS() && this.wantsPhone()) {
                this.allComms(true);
            }
        } else {
           $("#allCommsCheck").prop("checked", false);
        }

    }.bind(this);

    var onWantsSMSChange = function (newVal) {
        if (sbm.serverconfig.skinId != 12 ) {
             this.thirdPartySMS(newVal);
        }
         
        if(newVal) {
            if(this.wantsEmail() && this.wantsPhone()  && this.wantsMail()) {
                this.allComms(true);
            }
        } else {
             $("#allCommsCheck").prop("checked", false);
        }

    }.bind(this);

    var onWantsPhoneChange = function (newVal) {

        if(newVal) {
            this.thirdPartyPhone(true);
            if(this.wantsSMS() && this.wantsEmail() && this.wantsMail()) {
                this.allComms(newVal);
            }
        } else {
            this.thirdPartyPhone(false)
             $("#allCommsCheck").prop("checked", false);
        }

    }.bind(this);


    var onWantsMailChange = function (newVal){
        this.wantsMail(newVal);

    }.bind(this)

    var onThirdPartyAllCommsChange = function (newVal) {

        if (newVal) {
            // set all cb to true
            this.thirdPartyEmail(true);
            this.thirdPartySMS(true);
            this.thirdPartyPhone(true);
        } else {
            this.thirdPartyEmail(false);
            this.thirdPartySMS(false);
            this.thirdPartyPhone(false);
        }

    }.bind(this);

     var onThirdPartyEmailChange = function (newVal) {

        if(newVal) {
            if(this.thirdPartySMS() && this.thirdPartyPhone()) {
                this.thirdPartyAllComms(true);
            }
        } else {
           $("#thirdPartyAllCommsCheck, #switch__start--thirdpartyallcomms").prop("checked", false);
        }

    }.bind(this);

    var onThirdPartySMSChange = function (newVal) {

        if(newVal) {
            if(this.thirdPartyEmail() && this.thirdPartyPhone()) {
                this.thirdPartyAllComms(true);
            }
        } else {
             $("#thirdPartyAllCommsCheck, #switch__start--thirdpartyallcomms").prop("checked", false);
        }

    }.bind(this);

    var onThirdPartyPhoneChange = function (newVal) {

        if(newVal) {
            if(this.thirdPartySMS() && this.thirdPartyEmail()) {
                this.thirdPartyAllComms(true);
            }
        } else {
             $("#thirdPartyAllCommsCheck, #switch__start--thirdpartyallcomms").prop("checked", false);
        }

    }.bind(this);

    var onDataAspersChanged = function (value){

        if(value == "Yes"){
            this.showAspers(true);

        } else {
            this.showAspers(false);
            this.thirdPartyEmail(false);
            this.thirdPartySMS(false);
        }

    }.bind(this);

    this.allComms = sb.ko.observable(false);
    this.allComms.subscribe(onAllCommsChange);

    this.mobilePhone = sb.ko.observable("");

    this.wantsEmail = sb.ko.observable(false);
    this.wantsEmail.subscribe(onWantsEmailChange);

    this.wantsSMS = sb.ko.observable(false);
    this.wantsSMS.subscribe(onWantsSMSChange);

    this.wantsPhone = sb.ko.observable(true);
    this.wantsPhone.subscribe(onWantsPhoneChange);

    this.wantsMail = sb.ko.observable(true);
    this.wantsMail.subscribe(onWantsMailChange);
    
    this.thirdPartyAllComms = sb.ko.observable(false);
    this.thirdPartyAllComms.subscribe(onThirdPartyAllCommsChange);

    this.thirdPartyEmail = sb.ko.observable(false);
    this.thirdPartyEmail.subscribe(onThirdPartyEmailChange);

    this.thirdPartySMS = sb.ko.observable(false);
    this.thirdPartySMS.subscribe(onThirdPartySMSChange);

    this.thirdPartyPhone = sb.ko.observable(false);
    this.thirdPartyPhone.subscribe(onThirdPartyPhoneChange);
    
    this.currentPassword = sb.ko.observable("");
    this.securityQuestions = sb.ko.observableArray([]);
    this.securityQuestion = sb.ko.observable("");
    this.invalidCurrentPassword = sb.ko.observable(false);
    this.invalidNewPassword = sb.ko.observable(false);
    this.passwordsDontMatch = sb.ko.observable(false);
    this.UsernameAndPassTheSame = sb.ko.observable(false);
    this.invalidMobile = sb.ko.observable(false);
    this.invalidUkMobile = sb.ko.observable(false);
    this.invalidLandPhone = sb.ko.observable(false);
    this.invalidLandExtPhone = sb.ko.observable(false);
    this.mobileVerified = sb.ko.observable(false);
    this.mobileVerificationVisible = sb.ko.observable(false);
    this.verificationCode = sb.ko.observable("");
    this.countryCallCode = sb.ko.observable("");
    this.separatedNames = sb.ko.observable( sbm.serverconfig.separatedNames );
   
    this.dataAspers = sb.ko.observable(false);
    this.dataAspers.subscribe(onDataAspersChanged); 
    this.showAspers = sb.ko.observable(true);
    // this.showAspers.subscribe(onDataAspersSaved);
    this.dataAspersRequired = sb.ko.observable("");
    
    
    this.onMobilePhoneChanged = function(newVal,oldVal) {
       this.validatesMobile();
    }.bind(this);


    /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("got-personal-details", this.onGotPersonalDetails);
        sb.listen("saved-personal-details", this.onSavedPersonalDetails);
        sb.listen("resend-verification-code-finished", this.onResendFinished);
        sb.listen("verify-mobile-finished", this.onVerifyFinished);

        sb.$("#ui-phone").intlTelInput({
                autoPlaceholder: 'aggressive',
                utilsScript: '/_global-library/js/vendor/intltelinput/js/utils.js'
            });

        this.mobilePhone.subscribeChanged(this.onMobilePhoneChanged);
        this.getPersonalDetails();

    }.bind(this);


    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("got-personal-details");
        sb.ignore("saved-personal-details");
        sb.ignore("resend-verification-code-finished");
        sb.ignore("verify-mobile-finished");
    };

    /**
     * Get player's personal details
     */
    this.getPersonalDetails = function () {
        //This initializes with the account page so i check if there is a session to prevent "Your session expired alert bug"
        if($.cookie("SessionID") !== undefined)
            sb.notify("get-personal-details");
    }.bind(this);

    /**
     * Validates the new password
     */
    this.isValidPassword = function () {
        var pwd = this.newPassword();
        if (pwd === "") {
            return true;
        }
        return (/^(?=.*?\d)(\w|[!@\^#\$%\&\*])+$/i).test(pwd);
    };

    this.validatesMobile = function () {
        var number = this.mobilePhone();
        var numberLandline = this.landPhone();
        var numberLandlineExt = this.landPhoneExt();
        var country = this.countryName().toUpperCase();

        //remove empty spaces
        /*
        country = country.replace(/ /g, "");
        if (country === "UNITEDKINGDOM") {
            this.invalidUkMobile(!number.match(/^(0044|\+44|)07([1-5]|[7-9])\d{8}$/));
        } else {
            this.invalidMobile(!number.match(/^(\+|)\d*$/));
        }*/
        this.invalidMobile(!sb.$('#ui-phone').intlTelInput("isValidNumber"));
        this.invalidLandPhone(!numberLandline.match(/^(\+|)\d*$/));
        this.invalidLandExtPhone(!numberLandlineExt.match(/^(\+|)\d*$/));
    };

    
    /**
     * Save player's details
     * Validation and request
     */
    this.updateDetails = function () {

        // validation

        this.invalidCurrentPassword(!this.currentPassword());
        this.invalidNewPassword(!this.isValidPassword());
        this.passwordsDontMatch(this.newPassword() !== this.repeatPassword());
        this.UsernameAndPassTheSame(sb.getFromStorage("username") === this.newPassword());
        this.validatesMobile();

        if (this.invalidCurrentPassword() || this.passwordsDontMatch() || this.invalidUkMobile() || this.invalidMobile() || this.invalidLandPhone() || this.invalidLandExtPhone()) {
            return;
        }

        // request

        var request = {
            currentPassword: this.currentPassword(),
            newPassword: this.newPassword(),
            landPhone: this.landPhone(),
            landPhoneExt: this.landPhoneExt(),
            mobilePhone: $('#ui-phone').intlTelInput("getNumber").substring(1),
            securityQuestion: this.securityQuestion(),
            securityAnswer: this.securityAnswer(),
            wantsEmail: !!this.wantsEmail(),
            wantsSMS: !!this.wantsSMS(),
            wantsPhoneCalls: !!this.wantsPhone(),
            wantsMail: !!this.wantsMail(),
            wantsThirdPartyCalls: !!this.thirdPartyPhone(),
            wantsThirdPartySMS: !!this.thirdPartySMS(),
            wantsThirdPartyEmails: !!this.thirdPartyEmail(),
            aspersCustomerNumber: sb.getFromStorage("aspersCustomerNumber")
        };

        sb.notify("save-personal-details", [request]);

    }.bind(this);

    /**
     * Get player's details request has ended
     * Handles response
     */
    this.onGotPersonalDetails = function (e, data) {

        if (data.Code === 0) {

            // to split the mobile number into callCode and number we do the following
            // we assume that all the numbers from the DB are using the country call code correctly
            /*
            if (typeof data.countryCallCode !== 'undefined') {
                this.countryCallCode(data.countryCallCode);
                var i = data.MobilePhone.indexOf(this.CountryCallCode());  
                var mobilePhone = [s.slice(0,i), s.slice(i+1)];
            } else {
                var mobilePhone = data.MobilePhone;
            }
            */
            if(data.Gender === 0 ) {
                data.Gender = "Female";
            }

            if(data.Gender === 0 ) {
                data.Gender = "Female";
            }

            if(data.Gender === 1 ) {
                data.Gender = "Male";
            }

            if(data.Gender === 2 ) {
                data.Gender = "Other";
            }

            $("#ui-phone").intlTelInput("setNumber", '+'+data.MobilePhone);
            this.mobilePhone($("#ui-phone").intlTelInput("getNumber", intlTelInputUtils.numberFormat.NATIONAL));
            this.title(data.Gender);
            //this.gender(data.Gender);
            this.firstName(data.FirstName);
            this.lastName(data.LastName);
            this.email(data.Email);
            this.securityAnswer(data.SecurityAnswer);
            this.address1(data.Address1);
            this.address2(data.Address2);
            this.city(data.City);
            this.state(data.State);
            this.postcode(data.Postcode);
            this.countryName(data.CountryName);
            this.landPhone(data.LandPhone);
            this.landPhoneExt(data.LandPhoneExt);
            this.wantsEmail(data.WantsEmail);
            this.wantsSMS(data.SendSMS);
            this.wantsPhone(data.WantsPhoneCalls);
            this.wantsMail(data.WantsPostMail);
            this.thirdPartyEmail(data.WantsThirdPartyEmail);
            this.thirdPartySMS(data.WantsThirdPartySMS);
            this.thirdPartyPhone(data.WantsThirdPartyPhoneCalls);
            this.securityQuestions(data.Security);
            this.securityQuestion(data.SecurityQuestionID);
            this.mobileVerified(!!data.MobileVerified);

            if(this.thirdPartyEmail() == true || this.thirdPartySMS() == true) {
                this.showAspers(true);
                $("#aspersDataYes").prop("checked", true);
            } else {
                $("#aspersDataNo").prop("checked", true);
                this.showAspers(false);
            }

        } else {
            data.Msg && sb.showError(data.Msg);
        }

    }.bind(this);

    /**
     * Save player's details has ended
     * Handles response
     */
    this.onSavedPersonalDetails = function (e, data) {
        if (data.Code === 0) {

            sb.showSuccess(sb.i18n("updatedRecords"));

            // get details again
            this.getPersonalDetails();

        } else {
            data.Msg && sb.showError(data.Msg);
        }


    }.bind(this);

    /**
     * Toggle form mobile verification visibility
     */
    this.showMobileVerification = function () {
        this.mobileVerificationVisible(!this.mobileVerificationVisible());
    }.bind(this);

    /**
     * Send mobile verification code to server to verify player's mobile phone
     */
    this.verifyMobile = function () {
        var code = this.verificationCode();
        if (code) {
            sb.notify("verify-mobile", [code]);
        } else {
            sb.showWarning(sb.i18n("invalidCode"));
        }
    }.bind(this);

    /**
     * Request to send again the verification code to player's mobile phone
     */
    this.resendVerificationCode = function () {
        sb.notify("resend-verification-code");
    }.bind(this);

    /**
     * Re-send verification code request has ended
     * Handles response
     */
    this.onResendFinished = function (e, data) {
        if (data.Code === 0) {
            sb.showSuccess(sb.i18n("resendSuccess"));
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Verification request has ended
     * Handles response
     */
    this.onVerifyFinished = function (e, data) {
        if (data.Code === 0) {
            sb.showSuccess(sb.i18n("mobileVerified"));
            // hide verification form
            this.showMobileVerification(false);
            this.mobileVerified(true);
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);
};