var sbm = sbm || {};

/**
 * Player's refer a friend screen
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.PlayerReferAFriend = function (sb, options) {
    "use strict";

    this.totalRegistered = sb.ko.observable("");
    this.totalCompleted = sb.ko.observable("");
    this.earnedSoFar = sb.ko.observable("");
    this.potentialEarnings = sb.ko.observable("");
    this.referrals = sb.ko.observableArray([]);
    this.friends = sb.ko.observableArray([]);
    this.usernameAux = sb.getFromStorage("username");
    if (this.usernameAux === null) {
        this.usernameAux = sb.$.cookie('Username');
    }
    this.playerUrl = "https://"+location.hostname + "/register/" + this.usernameAux;
    this.fbLink = 'https://www.facebook.com/sharer/sharer.php?u='+this.playerUrl;
    this.twitterLink = 'https://twitter.com/home?status=\'' + this.playerUrl+'\'';

    this.emails = sb.ko.observable("");
    this.validEmails = sb.ko.observableArray([]);
    this.invalidEmails = sb.ko.observableArray([]);

    this.emailsToResend = [];
    this.notifierEmail = null;
    this.notifierInitialWidth = null;

    this.allChecked = false;
    this.errorMsg = sb.ko.observable("");
    this.filtersShowed = sb.ko.observable(false);

    this.reminderMsg = sb.ko.observable("Send Reminder");


    this.filterOptions = sb.ko.observable([  
                            {   name: 'All Referrals', 
                                referralsFilterFunction: function (item) { return true; }, 
                                friendsFilterFunction: function (item) { return true; }
                            },
                            {   name: 'Pending', 
                                referralsFilterFunction: function (item) { return false; }, 
                                friendsFilterFunction: function (item) { return (item.Wagered != 1); }
                            },
                            {   name: 'Eligible for Reminder', 
                                referralsFilterFunction: function (item) { return (item.CanSendAgain == 1); }, 
                                friendsFilterFunction: function (item) { return false; }
                            },
                            {   name: 'Completed', 
                                referralsFilterFunction: function (item) { return false; }, 
                                friendsFilterFunction: function (item) { return (item.Wagered == 1); }
                            },
                            {   name: 'Invited', 
                                referralsFilterFunction: function (item) { return true; }, 
                                friendsFilterFunction: function (item) { return false; }
                            }
                            ]);

    this.friendsC = ko.computed(function() {
                       return sb.ko.utils.arrayMap(this.friends(), 
                                function (item) {
                                    if (item["visible"] !== undefined) {
                                        item.visible(this.selectedFilterOption().friendsFilterFunction(item));
                                    } else {
                                        item["visible"] = sb.ko.observable(this.selectedFilterOption().friendsFilterFunction(item));
                                    }                                                                
                                    return item;
                            }.bind(this));                      
                    }.bind(this), this);


    this.referralsC = ko.computed(function() {
                       return ko.utils.arrayMap(this.referrals(), 
                                function (item) {
                                    if (item["visible"] !== undefined) {
                                        item.visible(this.selectedFilterOption().referralsFilterFunction(item));
                                    } else {
                                        item["visible"] = sb.ko.observable(this.selectedFilterOption().referralsFilterFunction(item));
                                    }                                      
                                    return item;
                            }.bind(this));                   
                    }.bind(this), this);

    this.selectedFilterOption = sb.ko.observable();                            


    /**
    * hides and shows the filtering options
    */
    this.showFilters = function (e) {
        this.filtersShowed(!this.filtersShowed());
        if (e.stopPropagation){
            e.stopPropagation();
        }
    }.bind(this);
    /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("got-player-raf-info", this.onGotPlayerRafInfo);
        sb.listen("sent-raf-emails", this.onSentRafEmails);
        this.selectedFilterOption(this.filterOptions()[0]);
        
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("got-player-raf-info");
        sb.ignore("sent-raf-emails");
        
    };

    /*
    * Starts the process to show the player raf tab
    */
    this.getPlayerRafInfo = function () {     
        sb.notify("get-player-raf-info");
    };

    /**
    * selects the filter option
    *
    */
    this.selectFilterOption = function (option,e) {

        this.selectedFilterOption(option);
        this.showFilters(e);
    }.bind(this);

    /**
     * Get player info regarding Refer a Friend
     * Handles response
     */
    this.onGotPlayerRafInfo = function (e, data) {
        if (data.Code === 0) {
            this.totalRegistered(data.TotalRegistered);
            this.totalCompleted(data.TotalCompleted);
            this.earnedSoFar(data.EarnedSoFar);
            this.potentialEarnings(data.PotentialEarnings);
            this.referrals(data.Referrals);
            this.friends(this.updateFriends(data.Friends));
            this.selectedFilterOption(this.filterOptions()[0]);
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
    *   Utility to parse and obtain the necessary fields for displaying the wagered so far circle
    */
    this.updateFriends = function (friends) {
        sb._.each(friends, function (friend) {
            var amounts = friend.WageredSoFar.split("/");
            friend["amountWagered"] = amounts[0];
            friend["toBeWagered"] = amounts[1];
            friend["percentage"] = 'p' + Math.round((parseFloat(friend["amountWagered"]) / parseFloat(friend["toBeWagered"]))*100);
            
        });   
        return friends;     
    }




    /*
    * Sends invitations within the Refer a Friend program to all your friends
    */
    this.sendInvitations = function (vm, e) {        
        this.cleanErrorMsg();
        var el = this.emails();
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if(!re.test(el)) {
            sb.showError("Oops, you need to use a valid email address so you can invite your friends.")
        } else {
            this.notifierInitialWidth = sb.$(e.currentTarget).width();
            sb.$(e.currentTarget).text("Sending...");
            sb.$(e.currentTarget).animate({width: '100%'},'fast',
                                                    function(){   
                                                        sb.notify("send-raf-emails",[el]);}
                                        );
            this.notifierEmail = e.currentTarget;
        }
    }.bind(this);

    this.copyLink = function(elementId) {
        var copyTextarea = document.querySelector(elementId);
        //copyTextarea.select();
        // create a Range object
        var range = document.createRange();
        // set the Node to select the "range"
        range.selectNode(copyTextarea);
        window.getSelection().removeAllRanges();
        // add the Range to the set of window selections
        window.getSelection().addRange(range);
        try {
            document.execCommand('copy');
            return true;
            //window.getSelection().removeAllRanges();
        } catch (err) {
            alert("Sorry but your browser does not support automatic copy. Please copy the link manually");
            return false;
        }
    }

    this.sliderCopyLink = function () {

        var success = this.copyLink("#ui-copyLinkText");

        if (success) {
            var nElement = sb.$("#sliderCopyLink");
            if (!nElement.hasClass("success")){
                var initialWidth = nElement.width();
                nElement.text("Copied to your clipboard!");
                nElement.removeClass("referral-methods__button__icon").addClass("success").animate({width: '100%'},'fast',
                    function(){
                        setTimeout(function(){
                            nElement.animate({width: initialWidth},800, function(){
                                nElement.removeClass("success");
                            }).text("").addClass("referral-methods__button__icon");
                        },1000);
                    }
                );
            }
        }
    }.bind(this);

    this.shareOnFb = function(){
        window.open(this.fbLink, "fbShare", 'width=600, height=400,toolbar=0, resizable=1, location=0');
    }.bind(this);

    this.shareOnTwitter = function(){
        window.open(this.twitterLink, "twitterShare", 'width=600, height=400,toolbar=0, resizable=1, location=0');
    }.bind(this);

    /*
    * Sends invitations within the Refer a Friend program to all your friends
    */
    this.resendInvitations = function () {    
        this.cleanErrorMsg();
        if (this.emailsToResend.length == 0) {
            sb.showWarning("Please select one or more referrals.")
        } else {
            this.reminderMsg("Sending...");
            sb.notify("send-raf-emails",[this.emailsToResend.toString().replace(/,/g,";")]);        
            this.emailsToResend = [];
        }
    }.bind(this);

    this.toggleCheckAll =  function(){   
        if (!this.allChecked) {
            sb.$('input:checkbox').attr('checked','checked');
            this.allChecked = true;
        } else {
            sb.$('input:checkbox').removeAttr('checked');
            this.allChecked = false;
        }
        return true;
    }   

    /*
    * Add email to emailsToResend
    */
    this.toggleEmail = function (email,data) {
        var index = this.emailsToResend.indexOf(email);
        if (index > -1) {
            this.emailsToResend.splice(index, 1);
        } else {
            this.emailsToResend.push(email);
        }
        return true;
    }.bind(this);

    /*
    * Close display Error Emails div
    */
    this.cleanErrorMsg = function () {
        this.errorMsg("");
    }.bind(this);

    this.onSentRafEmails = function(e,data) {
        var nElement = this.notifierEmail;
        var nWidth = this.notifierInitialWidth;
        this.notifierEmail = null;
        this.notifierInitialWidth = null;        
        sb.$('input:checkbox').removeAttr('checked');
        if (data.Code === 0) {  
            if (nElement!==null){  
                this.emails('');
                sb.$(nElement).text("Email sent successfully!");
                setTimeout(function(){
                                sb.$(nElement).text("Send").animate({width: nWidth},800);},1000
                            );
                var e;
                if (data.ValidEmails) {

                    sb._.each(data.ValidEmails.split(";"), function (e) {
                        if (e!==""){
                            this.referrals.unshift({Email: e, Status:0 , CanSendAgain: 4});
                        }
                    }.bind(this));
                }
            } else {
                this.reminderMsg("Sent!");  
                setTimeout(function(){
                    this.reminderMsg("Send Reminder");}.bind(this),1000
                ); 
                if (data.ValidEmails) {
                    
                    sb._.each(data.ValidEmails.split(";"), function(e) {

                        if (e!==""){    
                            var updatedReferral = sb.ko.utils.arrayFirst(this.referrals(), 
                                function(item){ return item.Email == e});   
                            var upRefAux = {Email: e, Status:0 , CanSendAgain: 2};
                            this.referrals.replace(updatedReferral,upRefAux);
                        }

                    }.bind(this));

                }
            }
        } else {
            if (nElement!==null){
                this.emails('');
                if (data.Msg) {
                    sb.$(nElement).text(data.Msg);
                } else {
                    sb.$(nElement).text("Oops, there was an error!");
                }
                setTimeout(function(){
                                sb.$(nElement).text("Send").animate({width: nWidth},800);},1000
                            );

                var i;
                var emails;
                var reasons;
                var eMsg = "";
                if (data.InvalidEmailsReason && data.InvalidEmailsReason !== "") {
                    reasons = data.InvalidEmailsReason.split(";");
                    emails = data.InvalidEmails.split(";");
                    for (i = 0; i < reasons.length; i++){  
                        if (emails[i]!==""){              
                            if (eMsg !== ""){
                                eMsg += "<br><hr>";
                            }
                            eMsg += emails[i]+"<br>"+sb.i18n("invalidMailReason_"+reasons[i]);
                        }
                    }                              
                    eMsg = "Oops, we couldn't send the invitation to your following friends: <br><br>" + eMsg;
                    this.errorMsg(eMsg);
                }
                var e;
                if (data.ValidEmails) {

                    sb._.each(data.ValidEmails.split(";"), function (e) {
                        if (e!==""){
                            var updatedReferral = sb.ko.utils.arrayFirst(this.referrals(),
                                function(item){ return item.Email == e});
                            var upRefAux = {Email: e, Status:0 , CanSendAgain: 2};
                            this.referrals.replace(updatedReferral,upRefAux);
                        }
                    }.bind(this));

                }
            } else {
                this.reminderMsg("Sent!");  
                setTimeout(function(){
                    this.reminderMsg("Send Reminder");}.bind(this),1000
                );  
                if (data.ValidEmails) {

                    sb._.each(data.ValidEmails.split(";"), function (e) {

                        if (e!==""){
                            var updatedReferral = sb.ko.utils.arrayFirst(this.referrals(),
                                function(item){ return item.Email == e});
                            var upRefAux = {Email: e, Status:0 , CanSendAgain: 2};
                            this.referrals.replace(updatedReferral,upRefAux);

                        }
                    }.bind(this));

                }
            }
        }
    }.bind(this);

};