var sbm = sbm || {};

/**
 * Player's account data
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.PlayerAccount = function(sb, options) {
  "use strict";

  this.balance = sb.ko.observable("");
  this.real = sb.ko.observable("");
  this.bonus = sb.ko.observable("");
  this.bonusWins = sb.ko.observable("");
  this.points = sb.ko.observable("");
  this.funded = sb.ko.observable("");
  this.playerClass = sb.ko.observable("");
  this.spins = sb.ko.observable("");
  this.cards = sb.ko.observable("");
  this.cashback = sb.ko.observable("");
  this.currencySymbol = sb.ko.observable("");
  this.messages = sb.ko.observableArray([]);
  this.nextvipLvl;
  //this.username = sb.ko.observable(sb.getFromStorage("username").toUpperCase());
  this.vipLevel = sb.ko.computed(function() {
    var vipLvl = sb.getPlayerClassName(this.playerClass());
    if (vipLvl !== undefined) return vipLvl.toLowerCase();
    return "";
  }, this);
  this.nextVipLevel = ko.computed(function() {
    var nextvipLvl = sb.getPlayerClassName(this.playerClass() + 1);
    if (nextvipLvl !== undefined) return nextvipLvl.toLowerCase();
    return "";
  }, this);
  this.vipLevelWidth = ko.computed(function() {
    var nextvipLvlWidth = (parseInt(this.playerClass()) * 16).toString() + "%";
    return nextvipLvlWidth;
  }, this);
  this.allChecked = sb.ko.observable(false);
  this.expanded = sb.ko.observable(true);
  this.messageHtml = sb.ko.observable("");
  this.selectedMessage = sb.ko.observable("");
  this.bonusBalance = sb.ko.computed(function() {
    var sum = parseInt(this.bonus()) + parseInt(this.bonusWins());
    return sum.toFixed(2);
  }, this);

  /**
   * Module initialization
   */
  this.init = function() {
    if (sb.getFromStorage("sessionId")) {
      sb.listen("got-player-account", this.onGotPlayeAccount);
      sb.listen("message-status-updated", this.onMessageUpdated);
      this.getPlayerData();
    }
  };

  /**
   * Module destruction
   */
  this.destroy = function() {
    sb.ignore("got-player-account");
    sb.ignore("message-status-updated");
  };

  /**
   * Get player data request
   */
  this.getPlayerData = function() {
    sb.notify("get-player-account");
  }.bind(this);

  /**
   * Updates message status
   *
   */
  this.updateMessageStatus = function(action, messageId) {
    sb.notify("message-update-status", [action, messageId]);
  };

  /**
   * Updates messages status in a batch
   *
   */
  this.updateClickedMessageStatuses = function(action) {
    var updateService = this.updateMessageStatus;
    sb.ko.utils.arrayForEach(this.messages(), function(message) {
      if (message.Clicked()) {
        updateService(action, message.Id);
      }
    });
  }.bind(this);

  /**
   * Messages status update
   * Handles response
   *
   */
  this.onMessageUpdated = function(e, data) {
    if (data.Code === 0) {
      if (data.newStatus > 2) {
        var match = ko.utils.arrayFirst(this.messages(), function(message) {
          return data.messageId === message.Id;
        });
        this.messages.remove(match);
      } else {
        this.messages(
          ko.utils.arrayMap(this.messages(), function(message) {
            if (message.Id == data.messageId) {
              message.MailStatus(data.newStatus);
            }

            return message;
          })
        );
      }
    } else {
      data.Msg && sb.showError(data.Msg);
    }
  }.bind(this);

  /**
   * Get player data request has ended
   * Handles response
   */
  this.onGotPlayeAccount = function(e, data) {
    if (data.Code === 0) {
      this.balance(data.Balance);
      this.real(data.Real);
      this.bonus(data.Bonus);
      this.bonusWins(data.BonusWins);
      this.points(data.Points);
      this.funded(data.Funded);
      this.playerClass(data.Class);
      this.spins(data.Spins);
      this.cards(data.Cards);
      this.cashback(data.Cashback + "%");
      sb.notify("cashier-got-player-account", [this.cards()]);
      this.currencySymbol(sb.getFromStorage("currencySymbol") || "");
      // so it adds the click propertie to the messages to be used by ko
      if (this.messages() && this.messages().length == 0) {
        var checkedVal = this.allChecked();
        _.each(data.Messages, function(message) {
          message["Clicked"] = sb.ko.observable(checkedVal);
          message.MailStatus = sb.ko.observable(message.MailStatus);
        });
      }
      this.messages(data.Messages);
    } else {
      data.Msg && sb.showError(data.Msg);
    }
  }.bind(this);

  /**
   * Marks as selected or unselected all the messages
   */
  this.selectAllToggle = function() {
    var allCheckedAux = (this.allChecked() + 1) % 2 == 1;
    this.allChecked(allCheckedAux);
    this.messages(
      ko.utils.arrayMap(this.messages(), function(message) {
        message.Clicked(allCheckedAux);
        return message;
      })
    );
    return true;
  }.bind(this);

  /**
   * Reveal window message and renders email template
   * @messageId to reveal
   */

  this.revealSingleMessage = function(messageId) {
    this.selectedMessage(
      ko.utils.arrayFirst(this.messages(), function(message) {
        return messageId === message.Id;
      })
    );
    this.updateMessageStatus("read", this.selectedMessage().Id);
    sb.notify("message-get-preview", this.selectedMessage().Id);
  }.bind(this);

  /**
   * Reveal window message and renders email template
   * @singleMessageId - the div id
   * @email template - the template used to reveal the silverpop template
   */

  this.check = function(data, event) {
    //event.stopPropagation();
    return true;
  }.bind(this);

  /**
   * Expands/collaps the email list
   */
  this.expandToggle = function() {
    this.expanded(!this.expanded());
    return true;
  }.bind(this);

  /**
   * closes a foundation dropdown
   * @param el - the dropdown element
   */
  this.closeDropDown = function(el) {
    sb.$(el).foundation("dropdown", "close", $("[data-dropdown-content]"));
  };

  /**
   * Update selected messages as read
   */
  this.markAsRead = function(vm, e) {
    this.closeDropDown(e.currentTarget);
    this.updateClickedMessageStatuses("read");
  }.bind(this);

  /**
   * Update selected messages as unread
   */
  this.markAsUnread = function(vm, e) {
    this.closeDropDown(e.currentTarget);
    this.updateClickedMessageStatuses("unread");
  }.bind(this);

  /**
   * Delete selected messages
   */
  this.delete = function(vm, e) {
    this.closeDropDown(e.currentTarget);
    this.updateClickedMessageStatuses("deleted");
  }.bind(this);
};
