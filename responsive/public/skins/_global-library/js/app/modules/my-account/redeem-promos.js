var sbm = sbm || {};

/**
 * Module to redeem any promo codes
 * @param sb sandbox
 * @constructor
 */
sbm.RedeemPromos = function (sb, options) {

    "use strict";

    /**
     * properties
     */

    this.promo = sb.ko.observable("");
    this.error = sb.ko.observable("");

    /**
     * functions
     */

    this.init = function () {
        sb.listen("redeemed-promo", this.onRedeemed);
        // set promo code if it is been sent by url parameter - remove last "/"" if any
        var code = sb.getUrlParameter("claim").replace(/\/$/, "").toUpperCase();
        if (code) this.promo(code);
    };

    this.destroy = function () {
        sb.ignore("redeemed-promo");
    };

    /**
     * Redeem Promo
     */
    this.redeemPromo = function () {

        var promo = this.promo();

        if (promo) {
            sb.notify("redeem-promo", [promo]);
        } else {
            this.error(sb.i18n("invalidPromoCode"));
        }

    }.bind(this);

    /**
     * Redeem Promo has ended
     * Handles response
     */
    this.onRedeemed = function (e, data) {
        if (data.Code === 0) {
            sb.setCookie("ClaimCodeID", this.promo());
            sb.sendVariableToGoogleManager("ClaimCodeID", this.promo());
            
            if (sb.$(".claimcode_modal.redeem").is(":visible")) {
                sb.$(".claimcode_modal.redeem").fadeOut(500, function(){
                    sb.$(".claimcode_modal.success").fadeIn(500);
                });
            } else {
                
                if((data.ThreeRadical=="" || data.ThreeRadical==null) || data.Msg.indexOf("3 Radical -") == -1){
                    if(data.FreeGames != ""){
                        sb.showPrizeWheelBox(data.Msg);
                        // If we are receiving a prize wheel promo, then check again
                        sb.notify("prize-wheel-check-for-prizes", []);
                    }
                    else{
                        sb.showSuccess(data.Msg);
                    }
                }
                else{
                        var callback= function(){sb.notify("threeRadical-request-token")};
                        sb.showTreeRadicalBox(data.ThreeRadical, callback);
                }
                this.promo("");
                this.error("");
            }
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);
};
