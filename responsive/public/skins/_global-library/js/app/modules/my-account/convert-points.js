var sbm = sbm || {};

/**
 * Convert player's points
 * @param sb The sandbox
 * @constructor
 */
sbm.ConvertPoints = function (sb, options) {
    "use strict";

    /**
     * Player changed the amount of points
     * @param points Amount of points to convert
     */
    this.onPointsToConvertChanged = function (points) {
        if (points && !isNaN(points) && points >= 1000) {
            this.errorMsg("");
        }
    }.bind(this);

    this.panelVisible = sb.ko.observable(false);
    this.canConvertPoints = sb.ko.observable(false);
    this.pointsToConvert = sb.ko.observable("").extend({ rateLimit: 500 });
    this.pointsToConvert.subscribe(this.onPointsToConvertChanged);
    this.type = sb.ko.observable("1");
    this.errorMsg = sb.ko.observable("");
    this.pointsName = sb.ko.observable(options.name);
    this.pointsInfoLink = sb.ko.observable(options.url);

    /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("converted-points", this.onConvertedPoints);
        sb.listen("got-player-account", this.onGotPlayeAccount); // this data is requested on PlayerAccount
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("converted-points");
        sb.ignore("got-player-account");
    };

    /**
     * Player choose to convert points
     * If valid request conversion
     */
    this.convert = function () {
        var URUVerification = sb.getCookie("URUVerified");
        sb.notify("age-id-verification-check-if-verified",[URUVerification],false);
        if(URUVerification == 1){
            var points = parseInt(this.pointsToConvert(), 10);
            if (!isNaN(points) && points >= 1000) {
                sb.notify("convert-points", [this.pointsToConvert(), this.type()]);
            } else if (this.pointsToConvert() === "") {
                this.errorMsg("This field is required.");
            } else {
                this.errorMsg("Sorry, the minimum amount to convert is 1000");
            }
        }
    }.bind(this);

    /**
     * Points conversion has finished
     * Handles response
     */
    this.onConvertedPoints = function (e, data) {
        if (data.Code === 0) {
            sb.notify("get-player-account"); // will be processed on PlayerAccount
            sb.showSuccess(sb.i18n("conversionSuccessful"));
            this.panelVisible(false);
            this.pointsToConvert("");
            this.errorMsg("");
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Show convert points panel
     */
    this.showPanel = function () {
        this.panelVisible(true);
    }.bind(this);

    /**
     * Hide convert points panel
     */
    this.hidePanel = function () {
        this.panelVisible(false);
    }.bind(this);

    /**
     * Get player account request has finished
     * Handles response
     */
    this.onGotPlayeAccount = function (e, data) {
        if (data.Code === 0) {
            this.canConvertPoints(data.Points >= 1000);
        } // don't show any error
    }.bind(this);

};