var sbm = sbm || {};

sbm.ThreeRadical = function(sb, options){
    
    var vm=this;
    
    this.token='';
    this.signature='';
    this.playerID=0;
    this.url = sbm.serverconfig.threeRadicalURLBoard;
    
    // Start listening for requests on tokens
    this.init = function () {
        sb.listen("threeRadical-generated-token", this.gotHMACToken);
        sb.listen("threeRadical-open-window", this.openThreeRadicalWindow);
    };
    // When the module is destroyed stop listening for events.
    this.destroy = function () {
        sb.ignore("threeRadical-generated-token");
        sb.ignore("threeRadical-open-window");
    };
    this.requestToken = function(){
        sb.notify("threeRadical-request-token",[]);
    }
    // When we get a token append it to a URL
    this.gotHMACToken = function(e,data){
        /* If we actually receive a code call the 3 Radical Window */
        if(data.Code === 0){
            vm.token=data['hmac']['nonce'];
            vm.signature=data['hmac']['signature'];
            vm.playerID=data['playerID'];
            sb.notify("threeRadical-open-window",[data]);
        }
        else{
            sb.notify("show-notification-warning", [data.Msg, "alert"]);
            sb.notify("hide-notification-loading");
        }
    };
    
    this.openThreeRadicalWindow = function(e,data){
        if(data.playerID!=0){
            var username = username || sb.getFromStorage("username");
            var params={
                "externalId":data.playerID,
                "signature":data.hmac.signature,
                "nonce": data.hmac.nonce
            }
            sb.notify("launch-game-window", [false, null, "3Rad",username,params]);
        }
    };
};