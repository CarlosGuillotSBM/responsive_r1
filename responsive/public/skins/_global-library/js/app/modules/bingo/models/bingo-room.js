var sbm = sbm || {};

/**
 * Class that represents a bingo room
 * @param r The room data
 * @constructor
 */
sbm.BingoRoom = function (sb, r) {

    "use strict";

    /**
     * Formats the remaining time to next game following these rules:
     * if < 0 show "Soon"
     * if time < 1 min show secs
     * if > 12h show days show Open Time
     * if > 1min < 12h show hours:mins:secs
     */
    this.formatSeconds = function (seconds, openTimes) {
        if (this.closed) {
            return openTimes || "";
        }
        if (seconds < 0) {
            return sb.i18n("soon");
        }
        if (seconds < 60) {
            return "00:00:" + (seconds > 9 ? seconds : "0" + seconds);
        }
        if (seconds >= (60 * 60 * 12)) {
            this.cssDisplayTime = "long";
            return openTimes;
        }

        var time = moment.duration(seconds, "seconds");
        var h = time._data.hours,
            m = time._data.minutes,
            s = time._data.seconds;

        return (h > 9 ? h : "0" + h) + ":" + (m > 9 ? m : "0" + m) + ":" + (s > 9 ? s : "0" + s);
    };

    /**
     * Formats the game type to be displayed
     * @param type The game type
     */
    this.formatGameType = function (type) {
        if (type === "75 Ball") return "75";
        if (type === "90 Ball") return "90";
        if (type === "5 Line") return "5L";
        if (type === "KO") return "KO";
        return "";
    };

    /**
     * Get all rules if is a progressive game
     */
    this.getProgressiveRules = function () {
        var rules = "";
        var possibleKeys = ["75P", "90P", "LN", "BL", "MN", "HCUT", "LCUT", "GB"];
        var key, feature, values, i = 0;
        for (i; i < this.featuresRules.length; i++) {
            feature = this.featuresRules[i].split(":");
            key = feature.shift();
            values = feature[1];
            if (possibleKeys.indexOf(key) > -1) {
                rules = rules ? "<br/>" + sbm.bingoRules.get(key, values) : sbm.bingoRules.get(key, values);
            }
        }
        return rules;
    };

    /**
     * Get the text to display if the room is not available
     * @returns {string}
     */
    this.getClosedText = function () {

        // room closed
        if (this.closed) {
            return "CLOSED";
        }

        // room only for funded
        if (this.permissionId === 1 && sb.getFromStorage("funded") === "false" && sb.getFromStorage("sessionId")) {
            this.playText = "Funded Only"
            return "FUNDED";
        }

        // room is available
        this.playText = "Play Now"
        return "";
    };

    /**
     * Properties
     */

    this.id = r.ID;
    this.roomId = r.RoomID;

    this.seconds = r.Seconds;
    this.secondsLive = sb.ko.observable(r.Seconds);
    this.players = r.Players;
    this.statusId = r.StatusID;
    this.openTimes = r.Opentimes;
    this.name = r.Name;
    this.gameType = r.GameType;
    this.features = r.Features;
    this.permissionId = r.GamePermissionID;
    this.permissionAmount = r.GamePermissionAmount;
    this.closed = r.StatusID === 0 ? true : false;
    this.closedText = this.getClosedText();
    this.displayJackpot = sb.getFromStorage("currencySymbol") + this.jackpot;
    this.displaySeconds = this.formatSeconds(parseInt(r.Seconds, 10), r.OpenTimes);
    this.displaySecondsLive = sb.ko.observable(this.formatSeconds(parseInt(r.Seconds, 10), r.OpenTimes));
    this.displayGameType = this.formatGameType(r.GameType);

    // TODO: Mik needs to style the progressive icon and tooltip
    //r.GameFeaturesRules = "90P:0:31|LINK:0:0";
    //this.featuresRules = r.GameFeaturesRules ? r.GameFeaturesRules.split("|") : [];
    this.featuresRules = [];

    this.progressiveRules = this.getProgressiveRules();
    this.displayJackpot = this.closed ? "" : r.Jackpot;
    this.cardPrice = this.closed ? "" : r.Cardprice;
    this.cssDisplayTime = this.closed ? "long" : "short";

    this.image = r.Image;
};
