var sbm = sbm || {};

/**
 * Class that represents a bingo room details
 * @param sb The sandbox
 * @param options
 * @constructor
 */
sbm.BingoRoomDetailsPage = function (sb, options) {

    "use strict";

    this.room = sb.ko.observable({});
    this.refreshTimer = null;

    this.init = function () {

        if (options.roomId) {
            sb.notify("get-bingo-room-detail");
            sb.listen("got-bingo-room-detail", this.onGotBingoRoomDetail);
        }
    };

    this.destroy = function () {
        sb.ignore("got-bingo-room-detail");
    };

    this.onGotBingoRoomDetail = function (e, data) {
        if (data.Code === 0) {

            var room = sb._.find(data.Rooms, function (r) {
                return r.ID === options.roomId;
            });

            if (room) {
                this.room(new sbm.BingoRoom(sb, room));

                if (!this.refreshTimer) {

                    this.refreshTimer = setInterval(function () {
                        this.refreshRoomInfo();
                    }.bind(this), 1000);
                }
            } else {
                this.room({});
            }

        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    this.refreshRoomInfo = function () {
        var room = this.room();
        room.seconds--;
        room.displaySeconds = room.formatSeconds(room.seconds, room.openTimes);
        this.room(room);

        if (room.seconds < -5) {
            clearInterval(this.refreshTimer);
            sb.notify("get-bingo-room-detail");
        }
    }.bind(this);

    this.openRoom = function (roomId, image) {
        roomId && sb.notify("launch-bingo-room", [roomId, sb.getFromStorage("playerId"), sb.getFromStorage("username"), image]);
    }.bind(this);
};
