var sbm = sbm || {};

/**
 * Module responsible for showing the bingo scheduler
 * @param sb The sandbox
 * @param options
 * @constructor
 */
sbm.BingoScheduler = function (sb) {
    "use strict";

    /**
     * subscriptions
     */

    this.onFilterChanged = function (filter) {
        switch (filter) {
        case "90":
            this.filterApplied("Bingo 90");
            break;
        case "75":
            this.filterApplied("Bingo 75");
            break;
        case "5L":
            this.filterApplied("Bingo 5L");
            break;
        case "PRE":
            this.filterApplied("Prebuy");
            break;
        case "ALL":
        default:
            this.filterApplied("All Rooms");
            break;
        }
        $(".dropdown").foundation("dropdown", "close", $("[data-dropdown-content]"));
    }.bind(this);

    /**
     * Properties
     */

    this.allRooms = [];
    this.openedRooms = sb.ko.observableArray([]);
    this.rooms90 = sb.ko.observableArray([]);
    this.rooms75 = sb.ko.observableArray([]);
    this.rooms5L = sb.ko.observableArray([]);
    this.roomsPre = sb.ko.observableArray([]);
    this.timerWeb = null;
    this.intervalWeb = 1000;
    this.filter = sb.ko.observable("ALL");
    this.filterApplied = sb.ko.observable("All Rooms");
    this.filter.subscribe(this.onFilterChanged);

    /**
     * Module initialization
     * Gets bingo rooms information immediately and then with a timer
     */
    this.init = function () {
        sb.listen("got-bingo-rooms", this.onGotBingoRooms);

        this.applyDefaultFilter();
    };

    /**
     * Module destruction
     * Clear events
     */
    this.destroy = function () {
        sb.ignore("got-bingo-rooms");
        clearInterval(this.timerWeb);
    };

    /**
     * Got bingo rooms data response from server
     * Populate the rooms
     */
    this.onGotBingoRooms = function (e, data) {
        if (data.Code === 0) {

            var i = 0,
                length = data.Rooms.length;

            this.allRooms = [];

            for (i; i < length; i++) {
                this.allRooms.push(new sbm.BingoRoom(sb, data.Rooms[i]));
            }

            this.filterRooms();
            this.addGamesToArray(data.Rooms);

            if (!this.timerWeb) {
                this.timerWeb = setInterval(function () {
                    this.refreshRoomsInfo();
                }.bind(this), this.intervalWeb);
            }

            sb.notify("bingo-rooms-updated");

        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Refresh rooms display seconds
     */
    this.refreshRoomsInfo = function () {

        // update seconds - will update on all arrays because is the same object!
        sb._.each(this.openedRooms(), function (r) {
            r.seconds--;

            r.secondsLive(r.seconds);
            r.displaySecondsLive(r.formatSeconds(r.seconds, r.openTimes));
        });

        // remove rooms that reached 0 seconds

        var filter = function (rooms) {
            return sb._.reject(rooms, function (r) {
                return r.seconds <= 0;
            });
        };

        this.openedRooms(filter(this.openedRooms()));
        this.rooms90(filter(this.rooms90()));
        this.rooms75(filter(this.rooms75()));
        this.rooms5L(filter(this.rooms5L()));
        this.roomsPre(filter(this.roomsPre()));

    };

    /**
     * Opens bingo room on iframe if is desktop or same window if is mobile
     * @param path the page path
     * @param params the post parameters to send in the request
     */
    this.openRoom = function (room) {

        this.checkBrowserCompatibility();
        sb.notify("launch-bingo-room", [room.id, null, null, room.image]);

    }.bind(this);

    // TODO: Remove this restriction once the bingo client is supported by IE11
    this.checkBrowserCompatibility = function() {
        if (sbm.serverconfig.skinId === 9 && this.msieversion() !== null && this.msieversion() <= 11) {
            window.location.href = '/browser-not-supported-chrome/'
            throw new Error('Abort');
        }
    }.bind(this);

    this.msieversion = function() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf ("MSIE ");
        var revision = ua.indexOf ("rv:");
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        } else if (revision > 0) {
            return parseInt(ua.substring(revision + 3, ua.indexOf(".", revision)));
        } else {
            return null;
        }
    };

    /**
     * Filters all rooms to the correct array
     */
    this.filterRooms = function () {

        var opened = [],
            rooms90 = [],
            rooms75 = [],
            rooms5L = [],
            roomsPre = [];

        _.each(this.allRooms, function (r) {
            if (!r.closed) opened.push(r);
            if (!r.closed && r.displayGameType === "90") rooms90.push(r);
            if (!r.closed && r.displayGameType === "75") rooms75.push(r);
            if (!r.closed && r.displayGameType === "5L") rooms5L.push(r);
            if (!r.closed && r.features.indexOf("Pre") > -1) roomsPre.push(r);
        });

        this.openedRooms(opened);
        this.rooms90(rooms90);
        this.rooms75(rooms75);
        this.rooms5L(rooms5L);
        this.roomsPre(roomsPre);
    };

    /**
     * Add new rooms to the global array that contains all games and rooms
     * This is needed because some modules use that array to access information regarding the game/room
     * @param rooms
     */
    this.addGamesToArray = function (rooms) {
        var r, i,
            length = rooms.length,
            toAdd = [];

        // find the rooms that are not in the array yet
        for (i = 0; i < length; i++) {
            r = _.find(rooms, function(r){ return r.id = rooms[i].id; });
            if (r) {
                toAdd.push(r);
            }
        }

        // add the rooms to the global array that contains all rooms
        length = toAdd.length;
        for (i = 0; i < length; i++) {
            sbm.games.push(toAdd[i]);
        }
    };

    this.setFilter = function (filter) {
        this.filter(filter);
    }.bind(this);

    /**
     * Checks for the current url and depending on the action it will set a default filter
     */
    this.applyDefaultFilter = function () {
        var filter,
            url = window.location.pathname;

        if (url.indexOf("90-ball") > -1) {
            filter  = "90";
        } else if (url.indexOf("75-ball") > -1) {
            filter = "75";
        } else if (url.indexOf("turbo") > -1) {
            filter = "90";
        } else if (url.indexOf("five-line") > -1) {
            filter = "5L";
        } else if (url.indexOf("free") > -1) {
            filter = "90";
        } else {
            filter = "ALL";
        }

        this.setFilter(filter);

        sb.$(".ui-bingo-schedule").removeClass("active");
        sb.$(".ui-bingo-schedule.ui-bingo-" + filter).addClass("active");
        sb.$(".ui-bingo-filter-" + filter).trigger( "click" );

    }.bind(this);
};