var sbm = sbm || {};

/**
 * Module responsible for getting bingo rooms information and launching them
 * @param sb The sandbox
 * @param options
 * @constructor
 */
sbm.BingoLauncher = function (sb, options) {
    "use strict";

    /**
     * Properties
     */

    this.rooms = sb.ko.observable({});
    this.timerDb = null;
    this.timerWeb = null;
    this.intervalDb = 30000;
    this.intervalWeb = 1000;
    this.$modal = sb.$(options.modalSelector);
    this.playerInfo = null;

    /**
     * Module initialization
     * Gets bingo rooms information immediately and then with a timer
     */
    this.init = function () {
        sb.notify("get-bingo-rooms");

        this.timerDb = setInterval(function () {
            clearInterval(this.timerWeb);
            sb.notify("get-bingo-rooms");
        }, this.intervalDb);

        sb.listen("got-bingo-rooms", this.onGotBingoRooms);
        sb.listen("launch-bingo-room", this.launchRoom);
        sb.listen("open-room-info-modal", this.openRoomInfo);
        sb.listen("got-balance", this.onGotBalance);
        sb.notify("get-balances");

        this.$modal.on("open", function () {
            sb.$("body").addClass("modal-open");
            sb.notify("show-recaptcha");
        });
        this.$modal.on("close", function () {
            sb.$("body").removeClass("modal-open");
            sb.notify("hide-recaptcha");
        });

    };

    /**
     * Module destruction
     * Clear events
     */
    this.destroy = function () {
        this.rooms([]);
        sb.ignore("got-bingo-rooms");
        sb.ignore("launch-bingo-room");
        sb.ignore("open-room-info-modal");
        sb.ignore("got-balance");
        clearInterval(this.timerDb);
        clearInterval(this.timerWeb);
    };

    /**
     * Got bingo rooms data response from server
     * Populate the rooms
     */
    this.onGotBingoRooms = function (e, data) {
        if (data.Code === 0) {

            var i = 0,
                length = data.Rooms.length,
                rooms = {};

            for (i; i < length; i++) {
                rooms[data.Rooms[i].ID] = new sbm.BingoRoom(sb, data.Rooms[i]);
            }

            this.rooms(rooms);

            if (!this.timerWeb) {
                this.timerWeb = setInterval(function () {
                    this.refreshRoomsInfo();
                }.bind(this), this.intervalWeb);
            }

        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);


    /**
     * Got Balance data response from server
     * Updates playerInfo
     */
    this.onGotBalance = function (e, data) {
        if (data.Code === 0) {

            /*Create or update playerInfo*/            
            var newInfo =   {
                daysFromRegistration: data.DaysFromRegistration,
                fundedStatus: data.FundedStatus,
                playerClass: data.PlayerClass,
                depositToday: data.DepositToday,
                depositThisWeek: data.DepositThisWeek,
                depositThisMonth: data.DepositThisMonth
            }
            /*If the player info is different from */                                
            if (newInfo !== this.playerInfo) {
                this.playerInfo = newInfo;
                /*For each room we create a closed text depending on players class if it*/


            }

        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Refresh the start time info on each room
     */
    this.refreshRoomsInfo = function () {

        var rooms = this.rooms();
        sb._.each(rooms, function (room) {
            room.seconds--;
            room.displaySeconds = room.formatSeconds(room.seconds, room.openTimes);
        });
        this.rooms(rooms);
    };

    /**
     * Opens bingo room on iframe if is desktop or same window if is mobile
     * @param path the page path
     * @param params the post parameters to send in the request
     */
    this.openRoom = function (roomId, img, username, e) {

        // broadcast message that a room has been launch
        sb.notify("game-launcher-set-room", [roomId]);

        // if no session - opens bingo info page and exits
        var sessionId = sb.getFromStorage("sessionId");

        if (!sessionId) {
            this.$modal.foundation("reveal", "open");
            sb.notify("show-recaptcha");
            sb.notify("show-room-details", [this.rooms()[roomId], img]);
            window.scrollTo(0, 0);
            sb.hideLoadingAnimation();
            return;
        }

        // check if can launch room

        var msgToDisplay = this.canOpenRoom(roomId);
        if (msgToDisplay) {
            sb.showInfo(msgToDisplay);
            sb.hideLoadingAnimation();
            return;
        }

        // launch the room
        sb.notify("launch-game-window", [false, roomId, "room", username]);
        // adds to last games as default
        sb.notify("add-last-game", [roomId, img, "Bingo", "room"]);

        this.openedGame(roomId, e);

    }.bind(this);

    /**
     * Sends data to google tag manager
     * @param room The room ID
     * @param el The clicked DOM element
     */
    this.openedGame = function (room, el) {
        var parent = sb.$(el).closest(".ui-parent-game-icon");
        sb.sendDataToGoogle("Room Launch", room + "+" + sb.$(parent).data("position") + "+" + sb.$(parent).data("containerkey"));
    };

    this.canOpenRoom = function (roomId) {
        var rooms = this.rooms();

        // bit of hack for "only funded rooms"
        if (rooms[roomId].permissionId === 1 && sb.getFromStorage("funded") === "false") {
            return  sb.i18n("roomOnlyForFunded");
        }

        // if room exists and is closed - show info and exit
        if (sb._.size(rooms) && rooms[roomId] && rooms[roomId].closed) {
            return sb.i18n("roomClosed", [this.rooms()[roomId].name]);
        }
        // if room exists and there is any closed text (means that player cannot open the room)
        if (sb._.size(rooms) && rooms[roomId] && rooms[roomId].closedText) {
            return rooms[roomId].closedText;
        }

        sbm.bingoGamePermission["GamePermissionAmount"] = rooms[roomId].permissionAmount;
        if (sb._.size(rooms) && rooms[roomId] 
                && !sbm.bingoGamePermission['permission_'+rooms[roomId].permissionId](this.playerInfo)) {
            var permisionKeyMessage = (sbm.bingoGamePermission["GamePermissionAmount"] > 0) ? 'permission_withAmount_' : 'permission_';
            return sb.i18n(permisionKeyMessage+rooms[roomId].permissionId, [Number(sbm.bingoGamePermission["GamePermissionAmount"])]);
        }
    };

    /**
     * Launch room if request from another module
     */
    this.launchRoom = function (e, roomId, playerId, username, img) {
        this.openRoom(roomId, img, username);
        sb.hideLoadingAnimation();
    }.bind(this);


    this.openRoomInfo = function (e, roomId, img) {
        var room = this.rooms()[roomId];
        if (room) {
            this.$modal.foundation("reveal", "open");
            sb.notify("show-room-details", [this.rooms()[roomId], img]);
            sb.notify("show-recaptcha");
            window.scrollTo(0, 0);
        } else {
            sb.showWarning("Sorry but this room is not available");
        }
        sb.hideLoadingAnimation();
    }.bind(this);
};