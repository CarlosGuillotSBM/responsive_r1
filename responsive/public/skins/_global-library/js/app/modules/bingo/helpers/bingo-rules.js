var sbm = sbm || {};

sbm.bingoRules = {

    get: function (key, values) {
        var i = 0;
        var str = this[key];
        values = values.split(",");
        for (i; i < values.length; i++) {
            str = str.replace("{" + i + "}", values[i]);
        }
        return str;
    },

    "GAME90": "90 Ball Bingo",
    "GAME75": "75 Ball Bingo",
    "GAME5L": "5 Line Bingo",

    "FRE": "FREE GAME<br/>Cards for this game are free. Max {0} cards per player",
    "GIFT": "PRIZE GAME<br/>The winner of this game receives a {0}",
    "PRE": "PREBUY GAME<br/>This is a prebuy game",
    "FJG": "FIXED JACKPOT GAME",
    "75S": "75 BALL SLIDING JACKPOT<br/>£{0} sliding to £{1}",
    "90S": "90 BALL SLIDING JACKPOT<br/>£{0} sliding to £{1}",
    "SALE": "CARD SALE NOW ON<br/>All Cards {0}% off!",
    "LINK": "LINKED ROOM<br/>Players from other networks also have access to this room.<br/>However chat will be limited to members of Bingo Extra",
    "SPEED2": "FAST BINGO<br/>Games play at a fast speed",
    "SPEED3": "TURBO BINGO<br/>Games play at turbo speed",
    "SPEED4": "HURRICANE BINGO<br/>Games play with the speed of a hurricane",
    "SPEED5": "BLINDING BINGO<br/>Games play at a blindingly fast speed",
    "B1G1F": "BUY 1 GET 1 FREE",
    "B2G1F": "BUY 2 GET 1 FREE",
    "BIG": "PARTY GAME<br/>Game is played across several rooms for a bigger jackpot!",
    "GAMBLE": "DOUBLE OR NOTHING<br/>Choose to gamble your winnings and potentially win double!",

    "75P": "75 BALL PROGRESSIVE JACKPOT<br/>Bingo in under {0} calls to win!",
    "90P": "90 BALL FULL HOUSE PROGRESSIVE<br/>Bingo in under {0} calls to win!",
    "LN": "LUCKY NUMBERS JACKPOT<br/>Daub all 4 of your lucky numbers in under {0} calls on any 1 card to win!",
    "BL": "BINGO LOTTO<br/>Daub all 4 of your lucky numbers in under {0} calls on any 1 card to win!",
    "MN": "MAGIC NUMBER<br/>Bingo on the chosen magic number to win!",
    "HCUT": "HIGH CUT JACKPOT<br/>Bingo on the highest number to win!",
    "LCUT": "LOW CUT JACKPOT<br/>Bingo on the lowest number to win!",
    "GB": "GOODIE BAG<br/>Bingo in under {0} calls to win!",

    "1TGPT": "1TG - WIN EXTRA POINTS!<br/>Got a 1TG on a FH and didn’t have a win? Have {0} bonus points on us!",
    "2TGPT": "2TG - WIN EXTRA POINTS!<br/>Got a 2TG on a FH and didn’t have a win? Have {0} bonus points on us!",
    "3TGPT": "3TG - WIN EXTRA POINTS!<br/>Got a 3TG on a FH and didn’t have a win? Have {0} bonus points on us!",
    "1TGCASH": "1TG - WIN BONUS MONEY!<br/>Got a 1TG on a FH and didn’t have a win? Have {0} bonus money on us!",
    "2TGCASH": "2TG - WIN BONUS MONEY!<br/>Got a 2TG on a FH and didn’t have a win? Have {0} bonus money on us!",
    "3TGCASH": "3TG - WIN BONUS MONEY!<br/>Got a 3TG on a FH and didn’t have a win? Have {0} bonus money on us!",
    "1TGCS": "1TG - SPLIT PRIZE<br/>{0} is split between all players with 1TG cards",
    "FTDFS": "BINGO SURPRISE<br/>Some random lucky players will win {0} FREE spins!",
    "FTDFC": "BINGO SURPRISE<br/>Some random lucky players will win {0} FREE cards!",
    "FTDBOGOF": "BINGO SURPRISE<br/>Some random lucky players will get BOGOF into next game!",

    "1TGFS": "1TG – WIN EXTRA FREE SPINS!<br/>Got a 1TG on a fullhouse and didn’t have a win? Have {0} FREE spins on us!",
    "2TGFS": "2TG – WIN EXTRA FREE SPINS!<br/>Got a 2TG on a fullhouse and didn’t have a win? Have {0} FREE spins on us!",
    "LOSTFS": "RISK FREE BINGO - SCOOP FREE SPINS IF YOU DON'T WIN!<br/>Didn't get a win during the game? Have {0} FREE spins on us!",
    "FHFS": "FULLHOUSE WIN – GET EXTRA FREE SPINS!<br/>Get the fullhouse win and have {0} FREE spins on us!",
    "LFS": "1 LINE WIN – GET EXTRA FREE SPINS!<br/>Get the 1 line win and have {0} FREE spins on us!",
    "2LFS": "2 LINE WIN – GET EXTRA FREE SPINS!<br/>Get the 2 line win and have {0} FREE spins on us!",
    "3LFS": "3 LINE WIN – GET EXTRA FREE SPINS!<br/>Get the 3 line win and have {0} FREE spins on us!",
    "4LFS": "4 LINE WIN – GET EXTRA FREE SPINS!<br/>Get the 4 line win and have {0} FREE spins on us!",

    "1TGFC": "1TG – WIN EXTRA FREE CARDS!<br/>Got a 1TG on a fullhouse and didn’t have a win? Have {0} FREE cards on us!",
    "2TGFC": "2TG – WIN EXTRA FREE CARDS!<br/>Got a 2TG on a fullhouse and didn’t have a win? Have {0} FREE cards on us!",
    "LOSTFC": "RISK FREE BINGO - SCOOP FREE CARDS IF YOU DON'T WIN!<br/>Didn't get a win during the game? Have {0} FREE cards on us!",
    "FHFC": "FULLHOUSE WIN – GET EXTRA FREE CARDS!<br/>Get the fullhouse win and have {0} FREE cards on us!",
    "1LFC": "1 LINE WINNERS GET EXTRA FREE CARDS!<br/>Get the 1 line win and have {0} FREE cards on us!",
    "2LFC": "2 LINE WINNERS GET EXTRA FREE CARDS!<br/>Get the 2 line win and have {0} FREE cards on us!",
    "3LFC": "3 LINE WINNERS GET EXTRA FREE CARDS!<br/>Get the 3 line win and have {0} FREE cards on us!",
    "4LFC": "4 LINE WINNERS GET EXTRA FREE CARDS!<br/>Get the 4 line win and have {0} FREE cards on us!",

    "1TGBOGOF": "1TG WINNERS GET BOGOF INTO NEXT GAME!",
    "2TGBOGOF": "1TG WINNERS GET BOGOF INTO NEXT GAME!",
    "LOSTBOGOF": "RISK FREE BINGO - GET BOGOF INTO NEXT GAME IF YOU DON'T WIN!",
    "FHBOGOF": "FULLHOUSE WIN – GET BOGOF INTO NEXT GAME!",
    "LBOGOF": "1 LINE WIN – GET BOGOF INTO NEXT GAME!",
    "2LBOGOF": "2 LINE WIN – GET BOGOF INTO NEXT GAME!",
    "3LBOGOF": "3 LINE WIN – GET BOGOF INTO NEXT GAME!",
    "4LBOGOF": "4 LINE WIN – GET BOGOF INTO NEXT GAME!",

    "XBALLSFIXED": "EXTRA BINGO BALLS WILL BE CALLED AT THE END OF THE GAME.  EXTRA CHANCES TO WIN!!",
    "XBALLSRAKE": "EXTRA BINGO BALLS WILL BE CALLED AT THE END OF THE GAME.  EXTRA CHANCES TO WIN!!",
    "XWINSFIXED": "EXTRA BINGO BALLS WILL BE CALLED AT THE END OF THE GAME.  EXTRA CHANCES TO WIN!!",
    "XWINSRAKE": "EXTRA BINGO BALLS WILL BE CALLED AT THE END OF THE GAME.  EXTRA CHANCES TO WIN!!"
};