var sbm = sbm || {};

/**
 * Class that represents a bingo room details
 * @param sb The sandbox
 * @constructor
 */
sbm.BingoRoomDetails = function (sb) {

    "use strict";

    this.seconds;
    this.timer;
    this.secondsDisplay = sb.ko.observable("");
    this.name = sb.ko.observable("");
    this.jackpot = sb.ko.observable("");
    this.gameType = sb.ko.observable("");
    this.gameTypeDisplay = sb.ko.observable("");
    this.players = sb.ko.observable("");
    this.cardPrice = sb.ko.observable("");
    this.image = sb.ko.observable("");

    this.init = function () {
        sb.listen("show-room-details", this.onShowRoomDetails);
    };

    this.destroy = function () {
        sb.ignore("show-room-details");
        clearInterval(this.timer);
    };

    this.onShowRoomDetails = function (e, room, img) {

        sb.hideLoadingAnimation();

        if (room) {
            this.timer && clearInterval(this.timer);
            this.name(room.name);
            this.jackpot(room.displayJackpot);
            this.gameType(room.gameType);
            this.players(room.players);
            this.cardPrice(room.cardPrice);
            this.image(img);
            this.seconds = room.seconds;
            this.secondsDisplay(room.formatSeconds(room.seconds));
            this.gameTypeDisplay(room.formatGameType(room.gameType));
            this.timer = setInterval(function () {
                this.secondsDisplay(room.formatSeconds(room.seconds, room.openTimes));
                this.gameTypeDisplay(room.formatGameType(room.gameType));
            }.bind(this), 1000);

        } else {
            this.image(img);
            this.secondsDisplay("Soon");
        }
    }.bind(this);
};
