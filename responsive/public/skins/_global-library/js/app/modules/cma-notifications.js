var sbm = sbm || {};

sbm.CMANotifications = function (sb, options) {
    "use strict";
    this.notifications = sb.ko.observableArray([]);
    this.counter = 0;
    this.timeToDestroy = 10000;
    var vm = this;
    this.previousNot = 0;
    
    this.init = function () {
        sb.listen("got-cma-notification", this.onGotCMANotification);
        sb.listen("game-iframe-closed", this.onGameIframeClosed);
    };

    this.onGameIframeClosed = function (e, data){
        this.previousNot = 0;
    }
    
    this.onGotCMANotification = function (e, data){
        var received = JSON.parse(data);
        if (received.type == "promo-money-info"){
            if(received.promoMoneyInfo !== 'undefined' && received.promoMoneyInfo.length >0){
                if(this.previousNot != received.promoMoneyInfo[0].txPromoId 
                    && received.promoMoneyInfo[0].txPromoId !== undefined
                ){
                    this.addNotification("base", "You are playing with Bonus Money.", true, received);
                    this.previousNot = received.promoMoneyInfo[0].txPromoId;
                }
                else if(received.promoMoneyInfo[0].txPromoId === undefined) {
                    if(this.previousNot != 1){
                        this.addNotification("base", "You are playing with Real Money.", false , "");
                        this.previousNot = 1;
                    }
                }
            }
        }
    }.bind(this);
    
    this.addNotification = function (style, message, expand, received) {
        // Dummy functionality, would need to replace with real data to get the bonus funds the player has available
        if(expand){
            var Promotions=[];
            received.promoMoneyInfo.forEach(function (promotion) {
                var pDate = promotion.expiryDate.substring(0,10).split("-");
                var newDate = pDate[2]+"-"+pDate[1]+"-"+pDate[0];
                var pWager = promotion.wagersRequired == 0 ? 'n/a' : sb.getCookie('CurrencySymbol')+' '+parseFloat(promotion.wagersRequired/100).toFixed(2);
                var pMax = promotion.maxConvertibleToReal == 0 ? 'n/a' : sb.getCookie('CurrencySymbol')+' '+parseFloat(promotion.maxConvertibleToReal/100).toFixed(2);
                Promotions.push({pName: promotion.promoName, pId: promotion.promoId, pWager: pWager, pMax: pMax, pDate: newDate})
            });
        }
        this.notifications.push({
            type: style,
            message: message,
            template: "cma-notification--" + style,
            expand: expand,
            readMore: sb.ko.observable(false),
            id: this.counter,
            bonusItems : sb.ko.observableArray(Promotions),
            onSwipe: function (direction) {
                if (direction == "right") {
                    this.removeNotification(this);
                }
            }
        });
        this.autoClose(this.counter);
        var length=this.notifications().length;
        this.counter++;
    }.bind(this);
    
    
    this.autoClose = function (id) {
        this.timeout = window.setTimeout(
            function () {
                vm.notifications.remove(function (item) {
                    return item.id == id;
                });
            }, this.timeToDestroy);
        }.bind(this);
        
        this.removeNotification = function (notification) {
            this.notifications.remove(notification);
        }.bind(this);
        
        this.expand = function (notification, event) {
            /* I'm SURE there is a better way to do this */
            var animatedElement = $(event.currentTarget).closest('.cma-notification').find('.cma-notification--svg');
            $(animatedElement).hide();
            notification.readMore(true);
            setTimeout(function(){
                $('.notificationSlides').bxSlider({
                    mode: 'horizontal',
                    minSlides: 1,
                    auto: false,
                    pager: true,
                    controls: false,
                    touchEnabled: false
                });
            },50)
            
            clearTimeout(this.timeout);
        }.bind(this);
        
        this.collapse = function (notification, event) {
            notification.readMore(false);
            this.autoClose(notification.id);
        }.bind(this);
    }