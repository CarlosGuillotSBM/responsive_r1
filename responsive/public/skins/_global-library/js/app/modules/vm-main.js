var sbm = sbm || {};

/**
* Top most object on the view model - our Master view model
* This view model is never destroyed
* All the other view models will be added to this one
* @param sb The sandbox
* @param options Options if any
* @constructor
*/
sbm.VmMain = function (sb, options) {
    "use strict";
    
    /**
    * Returns the number of deposits the player has done
    * @returns {Number|*} Number of deposits
    */
    this.getDepositCount = function () {
        var count = sb.getFromStorage("depositCount");
        return count ? parseInt(count, 10) : 0;
    };
    
    this.validSession = sb.ko.observable(false);
    this.desktopView = sb.ko.observable(true);
    this.cashierView = sb.ko.observable(false);
    this.startView = sb.ko.observable(false);
    this.balance = sb.ko.observable("...");
    this.points = sb.ko.observable("...");
    this.bonus = sb.ko.observable("...");
    this.bonusWins = sb.ko.observable("...");
    this.real = sb.ko.observable("...");
    this.bonusSpins = sb.ko.observable("...");
    this.tickets = sb.ko.observable("...");
    this.cashback = sb.ko.observable("...");
    this.cards = sb.ko.observable("...");
    this.bonusBalance = sb.ko.observable("...");
    this.balanceTimer = null;
    this.balanceDelay = 20000;
    this.prizeWheelTimer = null;
    this.editMode = options.editMode;
    this.username = sb.ko.observable(sb.getFromStorage("username"));
    this.depositCount = sb.ko.observable(this.getDepositCount());
    this.device = sb.ko.observable(sb.getDevice());
    this.playerClass = sb.ko.observable("0");
    this.lastLoginDate = sb.ko.observable(sb.getFromStorage("lastLoginDate"));
    this.avatar = sb.ko.observable("");
    this.isFunded = sb.ko.observable("");
    this.displayThreeRadical = sb.ko.observable("");
    this.displayRecaptcha = sb.ko.observable(false);
    
    this.token='';
    this.signature='';
    this.playerID=0;
    this.avatarRoute = sb.ko.computed(function () {
        if (this.avatar() == "") {
            return '/_images/common/kitty-avatar.png';
        } else {
            return '/_images/common/avatars/'+this.avatar();
        }
    }, this);
    this.vipLevel = sb.ko.computed(function () {
        var vipLvl = sb.getPlayerClassName(this.playerClass());
        if (vipLvl !== undefined) return vipLvl.toUpperCase();
        return '';
    }, this);
    this.vipLevelCssClass = sb.ko.computed(function () {
        var vipLvl = sb.getPlayerClassName(this.playerClass());
        if (vipLvl !== undefined) return vipLvl.toLowerCase();
        return '';
    }, this);
    this.showVerifyPopUp = sb.ko.observable(false || (!!sb.getFromStorage("verifyAccountPopUp") && sb.getFromStorage("sessionId")));
    // this.bonusBalance = sb.ko.computed( function ( ) {
    //      var sum = parseInt( this.bonus() ) + parseInt( this.bonusWins() );
    //      return sum.toFixed(2);
    // }, this);
    
    /**
    * Module initialization
    */
    this.init = function () {
        sb.listen("done-logout", this.onLogout);
        sb.listen("done-login", this.onLogin);
        sb.listen("got-balance", this.onGotBalance);
        sb.listen("session-has-expired", this.clearSession);
        sb.listen("refresh-balances", this.getBalances);
        sb.listen("player-has-deposited", this.updatePlayerData);
        sb.listen("got-player-account", this.onGotPlayeAccount);
        sb.listen("player-verification-finished", this.onPlayerVerification);
        sb.listen("show-recaptcha", this.onShowRecaptcha);
        sb.listen("hide-recaptcha", this.onHideRecaptcha);
        
        this.setKindOfView();
        this.checkIfCashierView();
        this.checkIfStartPageView();
        if (sb.getFromStorage("sessionId")) {
            this.refreshBalance();
            sb.notify("get-player-account");
            sb.$("a.ui-go-home").attr("href", "/start/").text("START");
            this.playerClass(sb.getFromStorage("class"));
            this.avatar(sb.getFromStorage("avatar"));
            this.isFunded(sb.getFromStorage("funded"));
            /* Show to all players on KB, MV, Aspers and LP */
            if(sbm.serverconfig.skinId == 2 || sbm.serverconfig.skinId == 5 || sbm.serverconfig.skinId == 12 || sbm.serverconfig.skinId == 3 || sbm.serverconfig.skinId == 1) {
                this.displayThreeRadical(this.isFunded()=="true" && this.playerClass() != -1);
            }
            sb.notify("get-player-account");
            if (!sb.getFromStorage("hideBalance") === null) {
                sb.saveOnStorage("hideBalance", false);
            }
            this.showBalance();
        }
        
    };
    
    
    /**
    * Deposit count has changed
    */
    this.changeDepositCount = function (count) {
        this.depositCount(count);
    }.bind(this);
    
    
    /**
    * Player Class has changed
    */
    this.changePlayerClass = function (classId) {
        this.playerClass(classId);
        sb.saveOnStorage("class", classId);
    }.bind(this);
    
    /**
    * Module destruction
    */
    this.destroy = function () {
        sb.ignore("done-logout");
        sb.ignore("done-login");
        sb.ignore("got-balance");
        sb.ignore("session-has-expired");
        sb.ignore("refresh-balances");
        sb.ignore("player-has-deposited");
        sb.ignore("got-player-account");
        sb.ignore("player-verification-finished");
        sb.ignore("player-verification"); 
        this.balanceTimer && clearInterval(this.balanceTimer);
    };
    
    /**
    * Player selected logout action
    */
    this.logout = function () {
        sb.notify("remove-messages");
        sb.notify("do-logout");
        
        
    }.bind(this);
    
    /**
    * Logout request has ended
    * Handles response
    */
    this.onLogout = function (e, data) {
        
        if (data.Code === 0) {
            this.destroy();
            sb.removeFromStorage("sessionId");
            sb.removeFromStorage("depositCount");
            sb.removeFromStorage("verifyAccount");
            sb.removeCookies(["URUVerified","PWSpins"]);
            window.location = "/";
        } else {
            data.Msg && sb.showError(data.Msg);
        }
        
    }.bind(this);
    
    /**
    * Clear session and redirects to main page
    */
    this.clearSession = function () {
        if (window.location.pathname.split("/")[1] !== "_global-library") {
            this.destroy();
            sb.removeFromStorage("sessionId");
            alert('Your session has expired, please login again');
            window.location = "/";
        }
    }.bind(this);
    
    /**
    * Login request has ended
    * Handles responses
    */
    this.onLogin = function (e, data) {
        if (data.Code === 0) {
            this.validSession(true);
            sb.saveOnStorage("funded", !!data.Funded);
            this.real(data.Real);
            this.bonus(data.Bonus);
            this.avatar(data.Avatar);
            sb.saveOnStorage("avatar", data.Avatar);
            this.lastLoginDate(data.LastLoginDate);
        }
    }.bind(this);
    
    
    this.testFunction = function() {
        
    }
    /**
    * Refresh balance
    * Makes request
    */
    this.refreshBalance = function () {
        if (!this.balanceTimer) {
            sb.notify("get-balance");
            this.balanceTimer = setInterval(function () {
                sb.notify("get-balance");
            }, this.balanceDelay);
        } else {
            // only if we have a session
            sb.getFromStorage("sessionId") && sb.notify("get-balance");
        }
    };
    
    /**
    * Get balance request has ended
    * Handles response
    */
    this.onGotBalance = function (e, data) {
        
        if (data.Code === 0) {
            var currSymbol = sb.getFromStorage("currencySymbol") || "";
            var bonusBalance = (parseInt(data.Bonus) +  parseInt(data.BonusWins)).toFixed(2);
            this.balance(currSymbol + data.Balance);
            this.points(data.Points);
            this.updateLoyaltyRow(data.Points);
            this.bonusSpins( data.BonusSpins);
            this.tickets(data.Tickets);
            this.changePlayerClass(data.PlayerClass);
            this.real(currSymbol + data.Real);
            this.bonus(currSymbol + data.Bonus);
            this.bonusWins(currSymbol + data.BonusWins);
            this.bonusBalance(currSymbol + bonusBalance);
            if (!this.username()) {
                this.username(sb.getFromStorage("username"));
            }
            this.updateBalancePlayerDetails(data);
            this.showBalance();
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);
    
    this.updateBalancePlayerDetails = function (data) {
        sb.saveOnStorage("balance", data.Balance);
        sb.saveOnStorage("points", data.Points);
        sb.saveOnStorage("real", data.Real);
        sb.saveOnStorage("bonus", data.Bonus);
        sb.saveOnStorage("bonusWins", data.BonusWins);
        this.updateGTMVariables(data);
    };
    
    this.updateGTMVariables = function(data){
        var variables = {
            "RealMoneyBalance" : data.Real,
            "BonusMoneyBalance" : data.Bonus,
            "FreeSpinsCount" : data.BonusSpins,
            "LoyaltyLevel" : data.PlayerClass,
            "LoyaltyPoints" : data.Points
        };
        sb.sendSetOfVariablesToGoogleManager(variables);
    };
    
    /**
    * Update the row in LP mobile relying on the points
    * @param points
    */
    this.updateLoyaltyRow = function (points) {
        if (points < 1000) {
            sb.$('.loyalty_points').html('LOYALTY POINTS');
            // Aspers
            if ( $('.loyalty_points--aspers').length > 0 ) {
                sb.$('.loyalty_points-placeholder').html('POINTS');
            }
        } else {
            sb.$('.loyalty_points').html('<a href="/my-account/?tab=ui-my-convert-tab">CONVERT LOYALTY POINTS</a>');
            //Aspers
            if ( $('.loyalty_points--aspers').length > 0 ) {
                sb.$('.loyalty_points-placeholder').html('<a href="/my-account/?tab=ui-convert-points-tab">CONVERT POINTS</a>');
            }
        }
    };
    
    /**
    * get balances - called from other module
    */
    this.getBalances = function () {
        this.refreshBalance();
        this.showBalance();
    }.bind(this);
    
    this.setKindOfView = function() {
        if (sbm.serverconfig.device === 1) {
            this.desktopView(true);
        } else {
            this.desktopView(false);
        }
    }.bind(this);
    
    this.checkIfCashierView = function() {
        if (window.location.href.indexOf("/cashier") > -1) {
            this.cashierView(true);
        } else {
            this.cashierView(false);
        }
    }.bind(this);
    
    this.checkIfStartPageView = function() {
        if (window.location.href.indexOf("/start") > -1) {
            this.startView(true);
        } else {
            this.startView(false);
        }
    }.bind(this);
    
    this.showBalance = function() {
        if (sb.getFromStorage("hideBalance") === "true") {
            this.balance("BALANCES");
            $(".balance, .slider.round").addClass("active");
            if (  $("#switch__mybalance").length > 0 ) {
                $("#switch__mybalance")[0].checked = true;
            }
        } else {
            this.setBalanceWithCurrency(sb.getFromStorage("balance") || this.balance());
            $(".balance, .slider.round").removeClass("active");
            $(".balance .slider.round").removeClass("active");
            if (  $("#switch__mybalance").length > 0 ) {
                $("#switch__mybalance")[0].checked = false;
            }
        }
        
        /**
        * toggle switch on/off balance show/hide
        */
        $("#ui-balance-switch").off("click change touchend");
        $("#ui-balance-switch").on("click change touchend", function () {
            $(".balance, .slider.round").toggleClass("active");
            if ($(".balance").hasClass("active")) {
                this.balance("BALANCES");
                sb.saveOnStorage("hideBalance", true);
                if (  $("#switch__mybalance").length > 0 ) {
                    $("#switch__mybalance")[0].checked = true;
                }
            } else {
                this.setBalanceWithCurrency(sb.getFromStorage("balance") || this.balance());
                sb.saveOnStorage("hideBalance", false);
                if (  $("#switch__mybalance").length > 0 ) {
                    $("#switch__mybalance")[0].checked = false;
                }
            }
        }.bind(this));
    };
    
    /**
    * Set the balance with the currency from the local storage
    *
    * @param balance
    */
    this.setBalanceWithCurrency = function(balance) {
        var currSymbol = sb.getFromStorage("currencySymbol") || "";
        this.balance(currSymbol + balance);
    };
    
    /**
    * Update player's data related to deposit count and player stage
    * @param e
    * @param depositCount
    */
    this.updatePlayerData = function (e, depositCount) {
        sb.saveOnStorage("depositCount", depositCount);
        sb.saveOnStorage("funded", true);
    }.bind(this);
    
    this.playBingo = function () {
        this.checkBrowserCompatibility();
        if (sbm.serverconfig.bingoUrl) {
            if (this.validSession()) {
                sb.notify("launch-game-window", [false, null, "room"]);
                sb.sendVariableToGoogleManager("PlayedBingo", true);
            } else {
                sb.notify("open-custom-modal", [false]);
            }
        } else {
            if (!this.validSession()) {
                sb.notify("open-custom-modal", [false]);
                sb.notify("set-redirect-URL",[sbm.serverconfig.slotPage.slice(1, sbm.serverconfig.slotPage.length - 1)]);
            } else {
                location.pathname = sbm.serverconfig.slotPage;
            }
        }
    }.bind(this);
    
    // TODO: Remove this restriction once the bingo client is supported by IE11
    this.checkBrowserCompatibility = function() {
        if (sbm.serverconfig.skinId === 9 && this.msieversion() !== null && this.msieversion() <= 11) {
            window.location.href = '/browser-not-supported-chrome/'
            throw new Error('Abort');
        }
    }.bind(this);
    
    this.msieversion = function() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf ("MSIE ");
        var revision = ua.indexOf ("rv:");
        if (msie > 0) {
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        } else if (revision > 0) {
            return parseInt(ua.substring(revision + 3, ua.indexOf(".", revision)));
        } else {
            return null;
        }
    }
    
    /**
    * Navigates to a given page if the player is logged in. Otherwise, you need to login.
    * @param redirectURL
    */
    this.navigateOnLogin = function (redirectURL) {
        redirectURL = (typeof redirectURL === 'string') ? redirectURL : '';
        
        var redirectExternal = redirectURL.toLowerCase().indexOf('http');
        /* If the CTA Action contains the word register, send to the page without logging in */
        if(redirectURL.indexOf("register")>-1){
            location.pathname = redirectURL;
        } 
        else{
            if (this.validSession()) {
                /* If the URL we are going to is an internal one then redirect page */
                if (redirectExternal === -1) {                
                    if (redirectURL.indexOf("bingo-room") != -1){
                        var roomId=redirectURL.split("?r=")[1];
                        sb.notify("launch-game-window", [false, roomId, "room"]);
                        sb.notify("player-has-loggedin", []);
                    }
                    else if(redirectURL==="3rad"){ 
                        var playerClass = sb.getCookie("Class");
                        var isFunded = sb.getFromStorage("funded");
                        if(playerClass != -1 && isFunded === "true"){
                            sb.notify("threeRadical-request-token",[]);
                        }
                        else{
                            sb.notify("show-notification-warning", ["Sorry, but you do not qualify for this promotion. If you would like more details please contact customer service.", "alert"]);
                        }
                    }
                    else{
                        location.pathname = redirectURL;
                    }
                }
                else {
                    window.open(redirectURL);
                }
            } else {
                if (redirectExternal === -1) {
                    sb.notify("open-custom-modal", [false]);
                    sb.notify("set-redirect-URL",[redirectURL]);
                } else {
                    window.open(redirectURL);
                }
            }
        }
    }.bind(this);
    
    this.claimCodeCTA = function (redirectURL) {
        redirectURL = (typeof redirectURL === 'string') ? redirectURL : '';
        var redirectExternal = redirectURL.toLowerCase().indexOf('http');
        var code=redirectURL.substring(redirectURL.indexOf("code=")+5).toUpperCase();
        redirectURL="my-account?tab=ui-redeem-tab&claim="+code;
        if (this.validSession()) {
            location.pathname = redirectURL;
        }
        else{
            if (redirectExternal === -1) {
                sb.notify("open-custom-modal", [false]);
                sb.notify("set-redirect-URL",[redirectURL]);
            }
            else {
                location.pathname = redirectURL;
            }
            
        }
    }.bind(this);
    
    this.threeRadicalCTA = function () {
        /* If session is started ask for a token*/
        if (this.validSession()) {
            this.playerClass(sb.getFromStorage("class"));
            this.isFunded(sb.getFromStorage("funded"));
            this.displayThreeRadical(this.isFunded()=="true" && this.playerClass() != -1);
            if(this.displayThreeRadical()){
                sb.notify("threeRadical-request-token",[]);
            }
            else{
                sb.notify("show-notification-warning", ["Sorry, but you do not qualify for this promotion. If you would like more details please contact customer service.", "alert"]);
            }
        }
        /* If session is not started ask for login and send a special code 
        so we can know it's a 3 Radical Request */ 
        else{
            sb.notify("open-custom-modal", [false]);
            sb.notify("set-redirect-URL",["3rad"]);
        }
    }.bind(this);
    
    /**
    * Get player data request has ended
    * Handles response
    */
    this.onGotPlayeAccount = function (e, data) {
        
        if (data.Code === 0) {
            this.cashback(data.Cashback + "%");
            this.cards(data.Cards);
        }
    }.bind(this);
    
    /** 
    * Get the cashback visibility
    */
    this.isCashbackVisible = function () {
        
        var _playerClass = this.playerClass();
        var invisibleClassesId = [-2, -1, 0, 1, 2, ""]; //"" value until we have the vip id
        
        return !invisibleClassesId.some(function(elem){
            if(_playerClass === elem)
            return true
        })
    };
    
    /*
    Player verification response from dataservice
    */
    this.onPlayerVerification = function (e, data) {
        
        if (data.Code === 0) {
            
            if (data.Body !== "null" && data.Body !== "") {
                this.showVerifyPopUp(true);
                sb.saveOnStorage("verifyAccountPopUp", true);
                sb.saveOnStorage("verifyAccount", data.Body);
                sb.initRoute("/");
            }
            
        } else {
            sb.removeFromStorage("verifyAccount");
        }
    }.bind(this);
    
    this.closeVerifyPopUp = function() {
        this.showVerifyPopUp(false);
        sb.removeFromStorage("verifyAccountPopUp");
    }.bind(this);
    
    this.accountVerificationMoreInfo = function() {
        sb.removeFromStorage("verifyAccountPopUp");
        window.open("/account-verification/");
    }.bind(this);
    
    this.accountVerificationCashier = function() {
        this.closeVerifyPopUp();
        window.location.pathname = "/cashier/?verification=1";
    }.bind(this);
    
    if ( $.trim( $("#live-casino-banner-edit").text() ).length == 0 ) {
        sb.$("#live-casino-banner-edit").css("display","none");
    } else {
        sb.$("#live-casino-banner-edit").css("display","block");
    }
    
    if ( $.trim( $("#page-banner-edit").text() ).length == 0 ) {
        sb.$("#page-banner-edit").css("display","none");
    } else {
        sb.$("#page-banner-edit").css("display","block");
    }

    this.onShowRecaptcha = function(){
        if(!sb.getCookie("SessionID")){
            sb.$(".grecaptcha-badge").css("visibility","visible");
            sb.$(".grecaptcha-badge").css("display","block");
        }
        else{
            this.onHideRecaptcha();
        }
        sb.notify("hide-notification-loading");
    }.bind(this);
    
    this.onHideRecaptcha = function(){
        sb.$(".grecaptcha-badge").css("visibility","hidden");
        sb.$(".grecaptcha-badge").css("display","none");
        sb.notify("hide-notification-loading");
    }.bind(this);
    
};