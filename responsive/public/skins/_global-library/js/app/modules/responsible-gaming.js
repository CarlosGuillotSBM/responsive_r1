var sbm = sbm || {};

/**
 * Login Box
 * Responsible for handling the social responsbile popup
 * @param sb The sandbox
 * @constructor
 */
sbm.ResponsibleGaming = function (sb, options) {
    "use strict";

     this.showResGamingPopUp = sb.ko.observable(false);


	this.init = function () {
       sb.listen("social-responsible-popup", this.onSocialVerifiedUser);
       sb.listen("responsible-gaming-action-continue-finished", this.onSocialResponsibleActionContinue);
       sb.listen("responsible-gaming-action-set-limit-finished", this.onSocialResponsibleActionSetLimit);
    };

    /**
     * Module destruction
     */
    this.destroy = function destroy() {
        sb.ignore("social-responsible-popup");
    };

    /*
        show Responsible gaming popup
    */
    
    this.onSocialVerifiedUser = function () {
      this.showResGamingPopUp(true);
       sb.$("#login-modal").hide();
    }.bind(this);

    /*
		Closes popup and takes them to set their limit
    */
    this.setLimitResGaming = function () {

    		sb.notify("responsible-gaming-action-set-limit", [3]);

    }.bind(this)


    /* 
	 function for the user to log in after choosing set limit function
    */
    this.onSocialResponsibleActionSetLimit = function(e, data) {
    	if(data.Code === 0) {
			sb.saveOnSessionStorage("sessionId", data.Body);
    		this.showResGamingPopUp(false);
    		window.location.pathname = "/cashier/?settings=1";
    	}
    }.bind(this);

     /*
        Closes popup and opens live chat
    
    */
    this.liveChatResGaming = function () {
      window.open(sb.serverconfig.liveChatURL, "_blank"); 
       sb.notify("responsible-gaming-action-live-chat", [2]);    
       
    }.bind(this);

    /*
        Closes popup and logs in user as normal
    */
    this.continueResGaming = function () {
     
     	sb.notify("responsible-gaming-action-continue", [1]);
   
    }.bind(this);

    /* 
	 function for the user to log in after accepting continue with gaming
    */
    this.onSocialResponsibleActionContinue = function(e, data) {
   
    	if(data.Code === 0) {
    		sb.saveOnSessionStorage("sessionId", data.Body);
    		this.showResGamingPopUp(false);
    		location.pathname = "/start/";
    	}
    }.bind(this);

    /*
    	takes the user to the responsible gaming page
    */
    this.responsibleURL = function() {
    	location.pathname = "/responsible-gaming/";
    };

};


