var sbm = sbm || {};

/**
 * This module is responsible for sending the deposit data to google tag manager
 * @param sb sandbox
 * @param options the deposit data

  {
    id: "DepositGoogleManager",
        options: {
            source: "Cas",
            amount: 30.00,
            depositCount: 2
        }
    }
  }

 * @constructor
 */
sbm.DepositGoogleManager = function (sb, options) {
    "use strict";

    /**
     * Module initialization
     */
    this.init = function () {

        var depositCount = options ? options.depositCount : "";

        var category;

        if (depositCount === 1) {
            category = "FTD";
        } else if (depositCount === 2) {
            category = "2ndDeposit";
        } else if (depositCount === 3) {
            category = "3rdDeposit";
        } else {
            category = "Deposit";
        }

        sb.notify("player-has-deposited", depositCount);
        sb.sendDataToGoogle(category, sbm.core.storage.get("playerId"));
    };
};
