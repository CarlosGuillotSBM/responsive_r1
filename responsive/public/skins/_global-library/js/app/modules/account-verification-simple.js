var sbm = sbm || {};

/*
	Module used to manage player verification - Source of Funds (DEV-11347)
*/
sbm.AccountVerificationSimple = function (sb) {

	// properties

  this.uiUploadFiles;

  this.uploadedFiles = sb.ko.observableArray([]);

  this.fileTypes = [
    "jpeg",
    "pjpeg",
    "png",
    "pdf"
  ];

  // initialization - must be called when module is created
  this.init = function () {
    
    // listen for events

    sb.listen("upload-documents-finished", this.onUploadFilesFinished);

    // get Dom element
    this.uiUploadFiles = document.getElementById("ui-file-uploads");

    // add event to be triggered when player uploads files
    this.uiUploadFiles.addEventListener("change", this.onFileUpload);
  };

  // destruction - must be called when module is destroyed
  this.destroy = function () {

    sb.ignore("upload-documents-finished");
  };

  // when player uploads files
  this.onFileUpload = function (input) {

    // validation

    var files = input.target.files;

    for (var i = 0; i < files.length; i++) {
      if (!this.validFileType(files[i].type)) {
        sb.showError("File type '" + files[i].type +"' is not valid. Please remove it and try again.");
        return;
      }
    }

    // all good - add files to the list - append if there's any

    var list = this.uploadedFiles();
    for (var i = 0; i < files.length; i++) {
      list.push(files[i]);
    }
    this.uploadedFiles(list);

  }.bind(this);

  // remove file from the list of uploads
  this.removeFile = function (data) {
  
    var filename = data.name;

    // remove file from array
    var newList = sb._.reject(this.uploadedFiles(), function (file) {
      return file.name === filename;
    });

    this.uploadedFiles(newList);

  }.bind(this);

  // validates if file type is correct
  this.validFileType = function (file) {
    for(var i = 0; i < this.fileTypes.length; i++) {
      // we only consider the extension and not the full type
      if(file.indexOf(this.fileTypes[i]) > -1) {
        return true;
      }
    }
  };

  // validates the size of the files
  // returns true if all good otherwise false
  // limit: 10 MB (10485760 bytes) overall
  this.sizeValidation = function () {

    var files = this.uploadedFiles(),
        total = 0;

    for (var i = 0; i < files.length; i++) {
      total += files[i].size;
    }

    if (total < 10485760) return true;

    return false;
  };

  // upload file
  this.upload = function () {

    // size validation
    if (this.sizeValidation()) {

      sb.notify("upload-documents", [this.uploadedFiles(), sb.getFromStorage("playerId"), sb.getFromStorage("sessionId")]);
    } else {

      sb.showError("Upload failed. Total file size exceeds 10MB.");
    }

  }.bind(this);

  // files have been uploaded
  this.onUploadFilesFinished = function (e, data) {

    if (data.Code === 0) {
      
      var c = function () {
        location.pathname = "/start/";
      }
      sb.showSuccess("Submit successful. We will be in touch shortly to inform you whether your documents have been successfully validated.", c);

    } else {

      sb.showError("Upload failed, please try again or contact our support team.");
      this.uploadedFiles([]);
    }
  }.bind(this);
};