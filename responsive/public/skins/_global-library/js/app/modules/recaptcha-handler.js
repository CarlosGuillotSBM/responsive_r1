var sbm = sbm || {};

sbm.RecaptchaHandler = function (sb) {
    "use strict";

    /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("hide-recaptcha", this.hideRecaptcha);
        sb.listen("show-recaptcha", this.showRecaptcha);
        this.hideRecaptcha();
    }.bind(this);

    /**
     * Module destruction
     */
    this.destroy = function destroy() {
        sb.ignore("show-recaptcha");
        sb.ignore("hide-recaptcha");
    };

    this.hideRecaptcha = function(e){
        var $recaptcha = sb.$(".grecaptcha-badge");
        $recaptcha.css();
    }
    
    this.showRecaptcha = function(e){
        var $recaptcha = sb.$(".grecaptcha-badge");
        $recaptcha.css("visibility","visible");
    }

};