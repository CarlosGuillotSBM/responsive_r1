var sbm = sbm || {};

/**
 * Preview
 * Responsible for handling the preview system
 * @param sb The sandbox
 * @param options  
 * @constructor
 */
sbm.Preview = function (sb, options) {
    "use strict";

    /**
     * Module initialization
     */
    this.init = function () {
        this.extraActions(options.extraActions);
        this.doLogin(options.username, options.password, options.redirectURL, options.loginWithSession);
        sb.listen("done-login", this.onDoneLogin);
    };

    /**
     * Module destruction
     */
    this.destroy = function destroy() {
        sb.ignore("done-login");
    };

    /**
    * Try to login
    */
    this.doLogin = function (username, password, redirectURL, loginWithSession) {

        if (username && password && redirectURL) {
            sb.notify("do-login", [username, password, false, redirectURL, loginWithSession]);
        }

    }.bind(this);

    /**
    * It saves the extraActions
    */
    this.extraActions = function (extraActions) {

        sb.saveOnStorage("extraActions", extraActions);

    }

    /**
    * It deletes the extraActions
    */
    this.deleteExtraActions = function () {

        sb.removeFromStorage("extraActions");
    }

};