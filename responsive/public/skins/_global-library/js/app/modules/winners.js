var sbm = sbm || {};

/**
 * Winners
 * @param sb The sandbox
 * @constructor
 */
sbm.Winners = function (sb) {
    "use strict";

    this.winners = sb.ko.observableArray([]);

    /**
     * Module initialization
     */
    this.init = function () {
        sb.listen("got-winners-feed", this.onGotWinnersFeed);
        sb.notify("get-winners-feed");
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("got-winners-feed");
    };

    /**
     * Get winners request has ended
     * Handles response
     */
    this.onGotWinnersFeed = function (e, data) {
        if (data.Code === 0) {

            this.winners(data);

        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);
};