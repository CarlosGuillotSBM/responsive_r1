var sbm = sbm || {};

/**
 * Module Register
 * This module is responsible for sending the user to the bingo room from an external url.
 * @param sb The Sandbox
 * @param options JSON object with any options for the viewmodel
* @constructor
*/
sbm.ExternalToRoom = function (sb, options) {
    "use strict";
    /**
    * Module initialization
    */
    this.init = function(){
        sb.listen("done-login", this.onDoneLogin);
        sb.listen("player-has-loggedin", this.onPlayerHasLoggedin);
    }
    /**
    * Module destruction
    */
    this.destroy = function(){
        sb.ignore("done-login");
        sb.ignore("player-has-loggedin");
    }
    this.onDoneLogin = function(){
        sb.initRoute("/start");
    }
    this.onPlayerHasLoggedin = function(e){
        sb.initRoute("/start");
    }

    this.onAccessFromExternalUrl = function(url){
        if(sb.getCookie("SessionID") != null){
            var roomId = url.split("?r=")[1];
            var username = sb.getFromStorage("username");
            sb.notify("launch-game-window", [false, roomId, "room", username]);
            sb.notify("player-has-loggedin", []);
        }
        else{
            sb.navigateOnLogin(url);
        }
    }.bind(this);
}