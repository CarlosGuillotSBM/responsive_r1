var sbm = sbm || {};

/**
 * Dont miss out popup
 * Responsible for handling the popup
 * @param sb The sandbox
 * @constructor
 */
sbm.DontMissOut = function (sb, options) {
    "use strict";

    var onThirdPartyAllCommsChange = function (newVal) {

        if (newVal) {
            // set all cb to true
            this.thirdPartyEmail(true);
            this.thirdPartySMS(true);
            this.thirdPartyPhone(true);
        } else {
            this.thirdPartyEmail(false);
            this.thirdPartySMS(false);
            this.thirdPartyPhone(false);
        }

    }.bind(this);

     var onThirdPartyEmailChange = function (newVal) {

        if(newVal) {
            if(this.thirdPartySMS() && this.thirdPartyPhone()) {
                this.thirdPartyAllComms(true);
            }
        } else {
           sb.$("#thirdPartyAllCommsCheck").prop("checked", false);
        }

    }.bind(this);

    var onThirdPartySMSChange = function (newVal) {

        if(newVal) {
            if(this.thirdPartyEmail() && this.thirdPartyPhone()) {
                this.thirdPartyAllComms(true);
            }
        } else {
             sb.$("#thirdPartyAllCommsCheck").prop("checked", false);
        }

    }.bind(this);

    var onThirdPartyPhoneChange = function (newVal) {

        if(newVal) {
            if(this.thirdPartySMS() && this.thirdPartyEmail()) {
                this.thirdPartyAllComms(true);
            }
        } else {
             sb.$("#thirdPartyAllCommsCheck").prop("checked", false);
        }

    }.bind(this);

    this.showThirdPartyPopup = sb.ko.observable(false);
    
    this.thirdPartyAllComms = sb.ko.observable(false);
    this.thirdPartyAllComms.subscribe(onThirdPartyAllCommsChange);

    this.thirdPartyEmail = sb.ko.observable(false);
    this.thirdPartyEmail.subscribe(onThirdPartyEmailChange);

    this.thirdPartySMS = sb.ko.observable(false);
    this.thirdPartySMS.subscribe(onThirdPartySMSChange);

    this.thirdPartyPhone = sb.ko.observable(false);
    this.thirdPartyPhone.subscribe(onThirdPartyPhoneChange);

    this.init = function () {
        

        /* 

        !!!!! Can't go LIVE yet !!!!!

        sb.notify("get-player-entity");
        
        sb.listen("got-player-entity", this.onGotPlayerEntity);
        sb.listen("dont-miss-out-updated", this.onUpdated);
        

        */
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("saved-personal-details");
        sb.ignore("show-dont-miss-out-popup");
        sb.ignore("got-player-entity");
    };

    this.updateDetails = function() {

        sb.notify("dont-miss-out-update", [!!this.thirdPartyEmail(), !!this.thirdPartySMS(), !!this.thirdPartyPhone()]);

    }.bind(this);


    // got player details from back end
    this.onGotPlayerEntity = function (e, data) {

        // sucess
        if (data.Code === 0) {

            // we must show the dont miss out popup to players
            // PARTIALLY or FULLY subscribed on DAUB comms (R10-67)

            // but first we need to check if the player already has seen it 
            // it must show only once!

            
            if (!data.Body.gdprThirdPartyCompliant) {

                // needs to show?
                if (data.Body.wantsEmail || data.Body.sendSMS || !data.Body.doNotCall) {

                    // populate players options
                    this.thirdPartyEmail(data.wantsThirdPartyEmail);
                    this.thirdPartySMS(data.wantsThirdPartySMS);
                    this.thirdPartyPhone(data.wantsThirdPartyPhoneCalls);
                   
                    // show popup
                    this.showThirdPartyPopup(true);
                }
            }

        }

    }.bind(this);

    // details updated
    this.onUpdated = function (e, data) {

        // if sucess
        if (data.Code === 0) {

            this.showThirdPartyPopup(false);

        } else {

            sb.showWarning(data.Msg);
        }

    }.bind(this);

};

