var sbm = sbm || {};

/**
 Module for the Opt In functionality
 Players are able to opt in on the available tournaments
 */

sbm.OptIn = function(sb, options) {

    "use strict";

    /**
     * Properties
     */
    this.tournaments = [];
    this.optedInTournament;
    this.units = {
        1: "p",
        2: "points"
    };
    this.validSession = sb.getFromStorage("sessionId") || false;
    this.clickedButton;

    this.$modalWithoutDetails = (options) ? sb.$(options.modalSelectorWithoutDetails) : false;
    this.optedInFromModal = false;

    /**
     * Functions
     */

    this.init = function() {
        sb.notify("get-competition-list");
        sb.listen("got-competition-list", this.onGotTournamentList);
        sb.listen("opt-in-from-modal", this.optInFromModal);
        sb.listen("opt-in-finished", this.onOptInFinished);
        sb.listen("got-leaderboard-for-competition", this.showLeaderBoard);
        sb.listen("on-claimed-prize", this.displayClaimedModal);
        sb.getUrlParameter("showtcs") && this.jumpToTCs();

        this.$modalWithoutDetails && this.$modalWithoutDetails.on("close", function() {
            this.clickedButton && this.clickedButton.on("click", this.optInClick);
        }.bind(this));
    };

    this.destroy = function() {
        sb.ignore("got-competition-list");
        sb.ignore("opt-in-finished");
        sb.ignore("opt-in-from-modal");
        sb.ignore("on-claimed-prize");
    };

    /**
     * Get active competition list request has finished
     * Handles response
     */
    this.onGotTournamentList = function(e, data) {
        if (data.Code === 0) {

            // opt-in automatically
            this.optInWithUrlParams();

            // handle response

            var optIn = this.optInClick;
            this.tournaments = data.Competitions;

            // show any opt-in buttons for the active competitions
            // change button depending if player is opted in or not

            var $btn, $claimNowBtn, $confirmationMessage;

            _.each(this.tournaments, function(c) {
                $btn = sb.$("[data-tournament=" + c.PromoName + "]");
                $claimNowBtn = sb.$("#claim-now-" + c.PromoName);
                $confirmationMessage = sb.$(".ui-confirmation-message-" + c.PromoName);
                if ($btn.length) {

                    if (c.PrizeID && c.PrizeID > 0) {
                        $claimNowBtn.show();
                        $confirmationMessage.show();
                        $btn.hide();
                        $btn.next().removeClass("cta-full-width");
                    } else {
                        if (c.OptedIn) {
                            $claimNowBtn.hide();
                            $confirmationMessage.hide();
                            switch (c.CompEntryType) {
                                case 2:
                                case 4:
                                case 11:
                                    $btn.html("Opted In &#10003;");
                                    $btn.addClass("active");
                                    // make play now smaller
                                    $btn.next().removeClass("cta-full-width");

                                    $btn.show();
                                    break;
                                case 1:
                                    $btn.html("YOU'RE IN &#10003;");
                                    $btn.addClass("active");
                                    // make play now smaller
                                    $btn.next().removeClass("cta-full-width");

                                    $btn.show();
                                    break;
                                case 3:
                                    $btn.html("Invited &#10003;");
                                    $btn.addClass("active");
                                    // make play now smaller
                                    $btn.next().removeClass("cta-full-width");

                                    $btn.show();
                                    break;
                            }

                        } else {
                            switch (c.CompEntryType) {
                                case 2:
                                case 4:
                                case 11:
                                    if (!this.validSession) {
                                        // make button bigger
                                        $btn.addClass("cta-full-width");
                                        $btn.text("OPT-IN");
                                        // remove play now button
                                        //if (typeof (options) == 'undefined' || typeof (options.details) == 'undefined' ) {
                                        $btn.next().remove();
                                        //}
                                    } else {
                                        // make play now smaller
                                        $btn.next().removeClass("cta-full-width");
                                    }

                                    $btn.on("click", optIn);

                                    $btn.show();
                                    break;
                                case 1:
                                    if (this.validSession) {
                                        $btn.html("YOU'RE IN &#10003;");
                                        $btn.addClass("active");
                                        // make play now smaller
                                        $btn.next().removeClass("cta-full-width");

                                        $btn.show();
                                    }
                                    break;
                                case 3:
                                    if (this.validSession) {
                                        $btn.html("INVITE ONLY");
                                        $btn.addClass("active cta-full-width");
                                        // make play now smaller
                                        var $next = $btn.next();
                                        if ($btn.next().hasClass("cta-full-width")) {
                                            $btn.next().hide();
                                            $btn.next().removeClass("cta-full-width");
                                            // and disabled
                                            $btn.next().prop('disabled', true);
                                        }

                                        $btn.show();
                                    }
                                    break;
                            }

                        } //end else not opted in
                    }// end else prizeID
                } // end if button


                // remove class from tab so it will show full width
                // this makes it to display the Prize Details tab - CURRENTLY ONLY FOR RAFFLES NOW
                if (c.CompType === 8 || c.CompType === 3 || c.CompType === 2) {
                    sb.$(".ui-promo-tab-" + c.PromoName).removeClass("tab-without-competition");
                }

                this.showCompPrizeDrawDetails(c);
                this.showBannerDetails(c);
                if (c.CompType === 3 || c.CompType === 2) {
                    sb.notify("get-leaderboard-by-competitionId", [c.ID, c.PromoName]);
                } else {
                    sb.$(".tab-title.leaderboard-" + c.PromoName).hide();
                }
                this.showFreebieProgress(c);
            }.bind(this));

        } else {
            data.Msg && sb.showError(data.Msg);
        }

    }.bind(this);

    /**
    * Set the progress bar percentage for Single Player Achievement (type 1) and Single Player Achievement Round (type 9)
    * SPA can have over 100% in that case we need to translate back to 100% (ex: 1.25 will be 25%)
    * SPAR never gets above 100% so we just need to translate to percentage the points/treshold
    */
    this.showFreebieProgress = function(c) {
        if (this.validSession && (c.CompType === 1 || c.CompType === 9)) {
            sb.$(".ui-promo-tab-" + c.PromoName).removeClass("tab-without-competition");
            sb.$(".tab-title.prize-draw-" + c.PromoName).hide();
            sb.$(".tab-title.leaderboard-" + c.PromoName).hide();

            var progress = c.Progress;

            sb.$(".ui-promotion-eng__freebie__content-" + c.PromoName).html(this.getPrizeDetailDescription(c));
            sb.$(".ui-promotion-eng__freebie__progress--value-" + c.PromoName).html(progress + "%");
            sb.$(".ui-promotion-eng__freebie__progress--bar-" + c.PromoName).css({ width: progress + "%" });
        } else {
            sb.$(".tab-title.freebie-" + c.PromoName).hide();
        }
    }.bind(this);

    /**
     * Handles click on opt-in button
     */
    this.showLeaderBoard = function(e, data) {

        if (data !== undefined) {
            var playerID = sb.getFromStorage("playerId");
            var $container = sb.$(".ui-leaderboard-" + data.PromoName);
            sb.$(".ui-prize-draw-" + data.PromoName).text("Tourney");
            sb.$(".ui-prize-draw-name-" + data.PromoName).text("Tourney ends");
            sb.$(".ui-prize-draw-players-" + data.PromoName).text("Number of players");
            sb.$(".ui-comp-entries" + data.PromoName).text(data.NumOfPlayers);

            var $htmlLeaderBoard = "";
            var position;
            _.each(data.Leaderboard, function(c) {
                if (playerID == c.PlayerID) {
                    position = c.Position
                }
                $htmlLeaderBoard += ((playerID == c.PlayerID) ? "<li class='ui-leader-position__user'>" : "<li>") +
                    "<div class='ui-leader-position'>" + c.Position + "</div>" +
                    "<div class='ui-leader-name'>" + c.Username + "</div>" +
                    "<div class='ui-leader-points'>" + c.Points + " pts </div>" +
                    "</li>";
            });
            sb.$(".ui-confirmation-message" + data.PromoName).text("Congratulations you came " + position);
            sb.$(".ui-confirmation-message-" + data.PromoName).removeClass("ui-draw-confirmation");
            sb.$(".ui-confirmation-message-" + data.PromoName).addClass("ui-tournament-confirmation");
            $container.html($htmlLeaderBoard);
        }

    }.bind(this);

    /**
     * Handles click on opt-in button
     */
    this.optInClick = function(e) {
        
        var $btn = sb.$(e.currentTarget);
        e.stopPropagation();
        $btn.off("click");
        
        
        var player = sb.getFromStorage("playerId");
        
        if (player) {
            var URUVerified = sb.getCookie("URUVerified");
            if(URUVerified == "1"){
                this.optIn(this.getTournamentId($btn.data("tournament")), player);
            }
            else{
                sb.notify("age-id-verification-check-if-verified",[URUVerified, {}, false]);   
            }
        } else {
            //sb.showWarning(sb.i18n("loginToOptIn"));
            this.clickedButton = $btn;
            
            sb.notify("open-custom-modal", [true]);
        }

    }.bind(this);

    /**
     * Opens game info modal
     * @param game Object containing all the necessary data for the info panel
     */
    this.openLoginModalWithoutDetails = function(e) {
        this.$modalWithoutDetails.foundation("reveal", "open");
        window.scrollTo(0, 0);
        sb.hideLoadingAnimation();
        sb.$("#custom-modal").addClass("ui-opt-in");
    }.bind(this);

    /**
     * Opens game info modal
     * @param game Object containing all the necessary data for the info panel
     */
    this.displayClaimedModal = function(e, data) {
        if (data.Code == 0) {
            var amount = "";

            switch (data.Type){
                /*bonus or real money, we add the currency symbol*/
                case 1:
                case 2:
                    amount += sb.getFromStorage("currencySymbol");
                    break;
                default:
                    break;
            }

            amount += data.ClaimAmount;

            switch (data.Type){
                case 1:
                    amount += " cash";
                    break;

                case 2:
                    amount += " bonus";
                    break;
                case 3:
                    amount += " points";
                    break;
                default:
                    break;
            }
            sb.$(".competition-amount").html(amount);
            sb.$(".competition-name").html(data.CompName);
            sb.$("#claimed-prize-modal").foundation("reveal", "open");
            window.scrollTo(0, 0);
            sb.hideLoadingAnimation();
            sb.notify("get-competition-list");
        }
    }.bind(this);

    /**
     * Opt-In request
     * @param tournamentId The tournament Id
     * @param playerId The player Id
     */
    this.optIn = function(tournamentId, playerId) {
        this.optedInTournament = parseInt(tournamentId, 10);
        sb.notify("opt-in", [tournamentId, playerId]);
    };

    /**
     * Opt-In request
     * @param tournamentId The tournament Id
     * @param playerId The player Id
     */
    this.optInFromModal = function(e, playerId) {
        var tournamentId = this.getTournamentId(this.clickedButton.data("tournament"));
        var title = this.clickedButton.data("title");
        this.optedInTournament = parseInt(tournamentId, 10);
        sb.showConfirm("To take part in this promotion, click the OK button below.", function() {
            sb.notify("opt-in", [tournamentId, playerId]);
            this.optedInFromModal = true;
        }.bind(this),
        function() {
            sb.initRoute("/promotions/");
        },
        title);
    }.bind(this);



    /**
     * Returns the tournament Id based on the promo name
     */
    this.getTournamentId = function(promo) {

        var tournament = _.find(this.tournaments, function(t) {
            return t.PromoName.toString() === promo.toString();
        });

        if (tournament) return tournament.ID;

        return -1;
    };

    /**
     * Returns the promo name based on the tournament id
     */
    this.getPromoName = function(tournamentId) {

        var tournament = _.find(this.tournaments, function(t) {
            return t.ID === tournamentId;
        });

        if (tournament) return tournament.PromoName;

        return -1;
    };

    /**
     * Opt-in request has finished
     * Handles response
     */
    this.onOptInFinished = function(e, data) {
        var goToPromotions = function() {
            sb.initRoute("/promotions/");
        };
        var $btn;
        if (data.Code == 0 || data.Code == -2 ) {
            
            
            if (data.Code == 0){
                sb.showSuccess(data.Msg, (this.optedInFromModal ) ? goToPromotions : function(){});
            } else {
                sb.showError(data.Msg, (this.optedInFromModal) ? goToPromotions : function() {});
            }

            // get promo name
            var promo = this.getPromoName(this.optedInTournament);
            $btn = sb.$("[data-tournament=" + promo + "]");

            // change button
            $btn.html("Opted In &#10003;");
            $btn.addClass("active");
            sb.sendDataToGoogle("Opt-In", sbm.serverconfig.device);
            sb.sendVariableToGoogleManager("OptinCompetitionID", this.optedInTournament);
        } else {
            var promo = this.getPromoName(this.optedInTournament);
            $btn = sb.$("[data-tournament=" + promo + "]");
            
            $btn.on("click", this.optInClick);

            data.Msg && sb.showError(data.Msg, (this.optedInFromModal) ? goToPromotions : function() {});
        }

    }.bind(this);

    /**
     * Check if there is a request to opt-in automatically
     */
    this.optInWithUrlParams = function() {

        // get params
        var competitionID = sb.getUrlParameter("c");
        var playerId = sb.getUrlParameter("p");

        // clear query parameters from url
        window.history.replaceState("", "", window.location.pathname);

        // opt-in
        if (competitionID && playerId) {
            this.optIn(competitionID, playerId);
        }
    };

    /**
     * Jumps to the T&Cs section and expands the container
     */
    this.jumpToTCs = function() {
        sb.$("#ui-promo-tcs").removeClass("accordion")[0].scrollIntoView(false);
    };

    /**
     * Gets the cost of the entry
     * Returns a string with the cost
     * @param cost
     * @param unit
     */
    this.getCostOfEntry = function(cost, unit) {

        if (cost === 0) {
            return "FREE";
        } else if (cost === -1) {
            return "Invite Only";
        } else if (!cost) {
            return "FREE";
        }
        return cost + this.units[unit];
    };

    /**
     * Sets the countdown for the competition end
     * @param end competition end date
     * @param selector jQuery selector for the element to bind
     */
    this.competitionEndCountDown = function(end, selector) {

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;
        var $el = $(selector);

        if (!$el.length) return;

        // parsing the date

        var theDateTime = end.split(" ");
        var theDate = theDateTime[0];
        var theTime = theDateTime[1];

        theDate = theDate.split("-");
        theTime = theTime.split(":");

        var dateParsed = new Date(Date.UTC(theDate[0], (theDate[1] - 1), theDate[2], theTime[0], theTime[1], theTime[2]));

        // countdown

        function showRemaining() {
            var now = new Date();
            var distance = dateParsed - now;
            if (distance < 0) {

                clearInterval(timer);
                return 'EXPIRED!';

            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);


            $el.html(days + " days " + ((hours < 10) ? "0" : "") + hours + ":" + ((minutes < 10) ? "0" : "") + minutes + ":" + ((seconds < 10) ? "0" : "") + seconds);

        }

        timer = setInterval(showRemaining, 1000);
    };

    /**
     * Retrieves the right description to be displayed on the Prize Details tab
     * @param competition the competition object
     */
    this.getPrizeDetailDescription = function(competition) {

        // no session
        if (!this.validSession) {
            return competition.LoggedOutDesc;
        }
        if (competition.OptedIn) {
            // opted in
            return competition.OptedInDesc;
        } else {
            // not opted in
            return competition.NotOptedInDesc;
        }
    };

    /**
     * Binds the values onto the tab Prize Draw Details
     * @param c competition
     */
    this.showCompPrizeDrawDetails = function(c) {

        // blurb description
        sb.$(".ui-comp-blurb" + c.PromoName).html(this.getPrizeDetailDescription(c));

        // competition end
        this.competitionEndCountDown(c.ValidToDate, ".ui-comp-end" + c.PromoName);

        // competition entries
        sb.$(".ui-comp-entries" + c.PromoName).html(c.NumTickets);

        // competition cost
        sb.$(".ui-comp-cost" + c.PromoName).html(this.getCostOfEntry(c.Cost, c.Unit));

    }.bind(this);

    /**
     * Binds the values onto the competition banner Details
     * @param c competition
     */
    this.showBannerDetails = function(c) {

        // competition end
        this.competitionEndCountDown(c.ValidToDate, ".ui-comp-end" + c.PromoName);

        // competition entries
        sb.$(".ui-comp-entries" + c.PromoName).text(c.NumTickets);

        // set visibility
        // if opted in
        if (c.OptedIn) {
            sb.$(".ui-comp-banner-in" + c.PromoName).show();
        } else {
            sb.$(".ui-comp-banner-out" + c.PromoName).show();
        }

    }.bind(this);

    /**
     * It claims the prize linked to the promotion
     *
     *
     */
    this.claimPrizePromotion = function(promotion) {
        var c;
        var $claimNowBtn;
        c = _.find(this.tournaments, function(item) {
            return item.PromoName === promotion
        });

        if (c && typeof c.PromoName !== 'undefined') {
            $claimNowBtn = sb.$("#claim-now-" + c.PromoName);
        }

        if (c && typeof c.PrizeID !== 'undefined' && $claimNowBtn.length && c.PrizeID > 0) {
            switch (c.PrizeTypeID) {
                case 1: // real money
                case 2: // points
                case 4: // Bonus Money
                case 5: // Prize
                    sb.notify("claim-prize", [c.PrizeID]);
                    break;
                case 3: // Scratchcard
                    var extraParams = { "prizePlayerId": c.PrizeID }
                    sb.notify("get-game-info", [c.LaunchGameID, false, extraParams]);
                    break;
                    break;
                    break;
                case 15: // Bonus Spin
                    break;
                case 16: // Free Cards
                    break;

            }
        }
    }.bind(this);
};
