var sbm = sbm || {};

/**
 * Module to filter DOM elemens
 * Each DOM element will have a type of filter and the rules
 *
 * Player filter:
 *
 *  - Hides DOM elements by player class and deposit count
 *  - DOM element should have a data-playerfilter attribute with the rules
 *  - Rules should be for example "VIPSILVER,DEP1,DEP2+"
 *
 * @param sb
 * @constructor
 */
sbm.RadFilter = function (sb, options) {
    "use strict";

    this.scheduledContent = sb.ko.observableArray([]);
    this.days = ["SUN","MON","TUE","WED","THU","FRI","SAT","SUN"];
    this.today = new Date();
    this.day = this.days[this.today.getDay()];
    this.hour = this.today.getHours();
    this.playerClassId = sb.getFromStorage("class");
    this.depositCount = sb.getFromStorage("depositCount");
    this.visibleElements = [];
    this.playerLoggedIn = false;
    // we could create a validation array here of "Validated" objects if in the future this grows
    this.classChecked = false;
    this.classValidated = true;
    this.timeChecked = false;
    this.timeValidated = true;
    this.depositsChecked = false;
    this.depositsValidated = true;
    this.maxElements = [];
    this.count = [];
    if (options && options.max) {
        sb._.each(options.max, function (e) {
            this.maxElements[e.elementClass] = e.count;
            this.count[e.elementClass] = 0;
        }.bind(this));
    }


    var playersTypeMap = new Map();
    playersTypeMap.set(-1, 'RED');
    playersTypeMap.set(1, 'VIPBRONZE');
    playersTypeMap.set(2, 'VIPSILVER');
    playersTypeMap.set(3, 'VIPGOLD');
    playersTypeMap.set(4, 'VIPRUBY');
    playersTypeMap.set(5, 'VIPEMERALD');
    playersTypeMap.set(6, 'VIPELITEDIAMOND');
    playersTypeMap.set(7, 'NEWBIE');


    var depositTypeMap = new Map();
    depositTypeMap.set('DEP0',0);
    depositTypeMap.set('DEP1',1);
    depositTypeMap.set('DEP2',2);
    depositTypeMap.set('DEP2+',3);



    this.init = function () {

        // apply filters

        // we just filter in logged in view. If we want to change this approach we'll need to change the code a little bit
        //if (sb.getFromStorage("sessionId")) {
        this.filterByPlayer(sb.$("[data-playerfilter]"));
        //}

    };


    this.destroy = function () {};




    this.checkDayTime = function (dateTime) {

        this.timeChecked = true;
        return this.checkDay(dateTime.substring(0,3)) && this.checkTime(dateTime.substring(3));

    }.bind(this);

    this.checkDay = function(today) {

        return today.toUpperCase() === this.day;

    }.bind(this);

    this.checkTime = function(time) {

        var hours = time.replace(/[\(\)]/g,"").split("-");

        return (time == "" ? true : (hours[0] ? (hours[0] <= this.hour && hours[1] > this.hour) : false));

    }.bind(this);

    /**
     /* Checks the number of deposits of the player. example DEP2 (just with 2 deposits) DEP2+ (two or more deposits)
     */
    this.checkDeposits = function(deposits) {
        this.depositsChecked = true;
        // we split the string using + so depAux[0] will be always 0
        var depAux = deposits.substring(3).split("+");
        if (depAux.length > 1) {
            // if exists then it means there was a + so we compare using >=
            return parseInt(this.depositCount) >= parseInt(depAux[0]);
        } else {
            return parseInt(this.depositCount) == parseInt(depAux[0]);
        }

    }.bind(this);

    /**
     /* Checks the player class of the player to decide whether to display the banner or not. Example VIPSILVER, VIPGOLD
     */
    this.checkPlayerClass = function(playerClass) {

        this.classChecked = true;
        return playerClass.substring(3).toUpperCase() === sb.getPlayerClassName(this.playerClassId).toUpperCase();


    }.bind(this);

    this.isContentVisible = function(rulesString) {
        /*
            18,-1,Red
            19,1,Bronze
            20,2,Silver
            21,3,Gold
            22,4,Ruby
            23,5,Emerald
            24,6,Elite Diamond
            85,0,Newbie

            VIPBRONZE, VIPSILVER, VIPGOLD, VIPRUBY, VIPEMERALD, VIPELITE
        */

        var sessId = sb.getCookie('SessionID');

        // if rulestring is emphy and not session id or not session id and rulestring is OUT
        if( rulesString == "" && !sessId || rulesString.includes('OUT') && !sessId ){
           return true
        }
        // if rulestring is emphy and session id or session id and rulestring is OUT
        if( rulesString.includes('OUT') && sessId ){
            return false
        }


        if( rulesString == "" && sessId ){
            return true
        }

        if(rulesString.includes('IN') && !sessId ){
            return false
        }
        
        
        // Ignore restrictions on logged out
        if(rulesString.includes('VIP') && !sessId ){
            return true
        }
        
        // Ignore restrictions on logged out
        if ( rulesString.includes ('DEP')   && !sessId ){
            return true;
        }


        // deposit filtering
        if ( rulesString.includes ('DEP')    ){

            var res = rulesString.split(",");

            var depositType = null;
            var checkedPromo = false;
            
            res.forEach(function(res){
                if( res.includes ('DEP')){
                    if(checkedPromo == false){
                        depositType = res;
                    }
                }
                
                if( res.includes ('VIPBRONZE') || res.includes ('VIPSILVER') || res.includes ('VIPGOLD') || res.includes ('VIPRUBY')
                || res.includes ('VIPEMERALD') || res.includes ('VIPELITE') || res.includes ('NEWBIE') ){
                    playerClassRestrion = res;
                }
            });
            
            if(depositType ){
                checkedPromo = true;
                //debugger;
                var playerDeposits =  sb.getFromStorage('depositCount');
                var promoRestriction = depositTypeMap.get( depositType.trim() );
                // if the promotion is excusive for especific level, dont show, example DEP2+
                // if the promotion count is equal to deposit count then show it
                if (promoRestriction == playerDeposits){
                    return true;
                }
            }
        }
        

        // class filtering
        // deposit filtering
        if (
            rulesString.includes ('VIPBRONZE')  || rulesString.includes ('VIPSILVER') ||
            rulesString.includes ('VIPGOLD')    || rulesString.includes ('VIPRUBY')   ||
            rulesString.includes ('VIPEMERALD') || rulesString.includes ('VIPELITE')  ||
            rulesString.includes ('NEWBIE')
        ) {

            var res = rulesString.split(",");
            var playerClassRestrion = null

            res.forEach(function (res) {

                if (res.includes('VIPBRONZE') || res.includes('VIPSILVER') || res.includes('VIPGOLD') ||
                    res.includes('VIPRUBY') || res.includes('VIPEMERALD') || res.includes('VIPELITE') ||
                    res.includes('NEWBIE')) {
                    playerClassRestrion = res;
                }

            });

            if (playerClassRestrion && sessId ) {
               //console.log("tipo de restriccion " +playerClassRestrion);
               //console.log (playersTypeMap.get(parseInt(sb.getFromStorage('class'))));

                var playerMapped = playersTypeMap.get(parseInt(sb.getFromStorage('class')));

                if (playerClassRestrion ==  playerMapped) {
                    return true;
                }


            }
        }


        // default validation

        // we restart the visibility per every element
        this.classChecked = false;
        this.classValidated = true;
        this.timeChecked = false;
        this.timeValidated = true;
        this.depositsChecked = false;
        this.depositsValidated = true;

        // overwrite validation if logged out state

        if (!sb.getFromStorage("sessionId")) {

            // we restart the visibility per every element
            this.classChecked = true;
            this.depositsChecked = true;
        }

        // First of all, if there's no rules. The banner will be within the banner to be displayed group, no matter what
        if (rulesString == "") return true;

        var rules = rulesString.replace(/\s/g,"").split(",");
        var i = 0;
        var rule = "";
        while (rules[i]){

            rule = rules[i].substring(0,3);

            switch (rule) {
                case "DEP":
                    if (!this.depositsChecked || !this.depositsValidated) {
                        this.depositsValidated = this.checkDeposits(rules[i]);
                    }

                    break;
                case "VIP":
                    if (!this.classChecked || !this.classValidated) {
                        this.classValidated = this.checkPlayerClass(rules[i]);
                    }

                    break;
                case "OUT":

                    if (!this.classChecked || !this.classValidated) {
                        this.classValidated = this.checkPlayerClass(rules[i]);
                    }

                    break;
                case "IN":
                    // doesn't do anything
                    break;
                default:
                    // it might be a day of the week
                    if (!this.timeChecked || !this.timeValidated) {
                        this.timeValidated = this.checkDayTime(rules[i]);
                    }
                    break;
            }


            i++;
        }

        return this.timeValidated && this.classValidated && this.depositsValidated;

    }.bind(this);

    /**
     * Hides DOM elements based on player class and deposit count
     * @param $els array with DOM elements containing the rules
     */
    this.filterByPlayer = function ($els) {

        var rule = "";
        // Get rules and hide if rules apply
        sb._.each($els, function (e) {
            var auxClass = sb.$(e).data("filterclass");

            rule = sb.$(e).data("playerfilter");
            // it checks for content that shouldn't be visible either because it's above the max element count
            // or not visible due to the visibility rules of the element
            if((this.maxElements[auxClass] && this.count[auxClass] >= this.maxElements[auxClass]) || !this.isContentVisible(rule)) {
                e.remove();
            } else {
                this.count[auxClass]++;
            }

        }.bind(this));
        //This inits the promo slider from homepage after all promos are removed 
        /* R10-1215
        if((".bxslider.bxslider-promo-carousel-s").length > 0){
            skinPlugins.push({
                id: "Slick",
                options: {
                    slideWidth: 250,
                    responsive: false,
                    minSlides: 1,
                    maxSlides: 100,
                    slideMargin: 10,
                    auto: false,
                    infinite: true,
                    element: 'bxslider-promo-carousel-s',
                    pager: false
                }
            });
        }
        */
    }.bind(this);
};