var sbm = sbm || {};

/**
 * Module responsible for giving feedbak to player
 * Shows dialogs and notification on header
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.Notifier = function(sb, options) {
  "use strict";

  this.notification = sb.ko.observable("");
  this.severity = sb.ko.observable("info");
  this.title = sb.ko.observable("Information");
  this.system = sb.ko.observable("");
  this.loading = sb.ko.observable(false);
  this.showCancelOnConfirmation = sb.ko.observable(false);

  this.callbackOnConfirmed = function() {
    this.clearNotification();
  }.bind(this);

  this.onSeverityChange = function(newVal) {
    if (newVal === "confirm") {
      this.title("Please Confirm");
    } else if (newVal === "success") {
      this.title("Success");
    } else if (newVal === "error") {
      this.title("Oops");
    } else if (newVal === "warning") {
      this.title("Oops");
    } else if (newVal === "info") {
      this.title("Information");
    } else {
      this.title("Message");
    }
  }.bind(this);

  this.severity.subscribe(this.onSeverityChange);

  /**
   * Module initialization
   */
  this.init = function() {
    sb.listen("show-notification-success", this.onShowSuccess);
    sb.listen("show-notification-error", this.onShowError);
    sb.listen("show-notification-warning", this.onShowWarning);
    sb.listen("show-notification-info", this.onShowInfo);
    sb.listen("show-notification-threeRadical", this.onShowThreeRadical);
    sb.listen(
      "show-notification-prizeWheel",
      this.onShowNotificationPrizeWheel
    );
    sb.listen("show-notification-please-wait", this.onShowPleaseWait);
    sb.listen("show-notification-uru-check", this.onShowURUDialog);
    sb.listen(
      "show-notification-lccp-from-welcome",
      this.onShowLCCPFromWelcome
    );
    sb.listen("show-notification-confirm", this.onShowConfirmation);
    sb.listen("show-notification-loading", this.onShowLoading);
    sb.listen("show-notification-result", this.onShowResult);
    sb.listen("hide-notification-loading", this.onHideLoading);
    sb.listen("hide-notification-dialog", this.clearNotification);
    sb.listen("show-notification-are-you-sure", this.onShowAreYouSure);
  };

  /**
   * Module destruction
   */
  this.destroy = function() {
    sb.ignore("show-notification-success");
    sb.ignore("show-notification-error");
    sb.ignore("show-notification-warning");
    sb.ignore("show-notification-info");
    sb.ignore("show-notification-confirm");
    sb.ignore("show-notification-loading");
    sb.ignore("show-notification-please-wait");
    sb.ignore("hide-notification-loading");
    sb.ignore("hide-notification-dialog");
  };

  /**
   * Show warning
   */
  this.onShowWarning = function(e, msg, system) {
    this.showNotification(msg, "warning", system);
  }.bind(this);

  /**
   * Show info
   */
  this.onShowInfo = function(e, msg, system) {
    this.showNotification(msg, "info", system);
  }.bind(this);

  /**
   * Show notification
   * @param msg The message
   * @param severity The type
   * @param system If uses notification header or a dialog
   */
  this.showNotification = function(msg, severity, system) {
    if (sbm.serverconfig.useAlerts) {
      alert(msg);
      return;
    }

    this.severity(severity);
    this.notification(msg);
    this.system(system);

    // hide notifications after a delay
    if (system === "notification") {
      if (severity === "success" || severity === "info") {
        var delay =
          options && options.delay ? parseInt(options.delay, 10) : 1500;
        setTimeout(
          function() {
            this.notification("");
          }.bind(this),
          delay
        );
      }
    } else {
      scrollTo(0, 0);
    }
  };

  /**
   * Show confirmation dialog
   */
  this.onShowConfirmation = function(
    e,
    msg,
    onConfirm,
    onCancel,
    title,
    hideAfterConfirmation = true
  ) {
    if (sbm.serverconfig.useAlerts) {
      if (confirm(msg)) {
        onConfirm();
      }

      return;
    }

    this.system("alert");
    this.severity("confirm");
    if (title) {
      this.title(title);
    }
    this.notification(msg);
    this.callbackOnConfirmed = function() {
      typeof onConfirm === "function" && onConfirm();
      if (hideAfterConfirmation) {
        this.clearNotification();
      }
    }.bind(this);

    if (typeof onCancel === "function") {
      this.showCancelOnConfirmation(true);
      this.callbackOnCancelled = function() {
        onCancel();
        this.clearNotification();
      }.bind(this);
    }
  }.bind(this);

  this.onShowAreYouSure = function(e, msg, onConfirm, onCancel, title) {
    if (sbm.serverconfig.useAlerts) {
      if (confirm(msg)) {
        onConfirm();
      }

      return;
    }

    this.system("alert");
    this.severity("are-you-sure");
    if (title) {
      this.title(title);
    }
    this.notification(msg);
    this.callbackOnConfirmed = function() {
      typeof onConfirm === "function" && onConfirm();
      this.clearNotification();
    }.bind(this);

    if (typeof onCancel === "function") {
      this.showCancelOnConfirmation(true);
      this.callbackOnCancelled = function() {
        onCancel();
        this.clearNotification();
      }.bind(this);
    }
  }.bind(this);

  /**
   * Show success
   */
  this.onShowSuccess = function(e, msg, onConfirm, system) {
    if (sbm.serverconfig.useAlerts) {
      if (confirm(msg)) {
        onConfirm();
      } else {
        onCancel();
      }

      return;
    }

    this.severity("success");
    this.notification(msg);
    this.system(system);
    this.callbackOnConfirmed = function() {
      typeof onConfirm === "function" && onConfirm();
      this.clearNotification();
    }.bind(this);
  }.bind(this);

  this.onShowThreeRadical = function(e, prize, onConfirm, system) {
    if (sbm.serverconfig.useAlerts) {
      if (confirm(msg)) {
        onConfirm();
      } else {
        this.clearNotification();
      }

      return;
    }
    var imagePath = "/";
    this.severity("3Rad");
    this.title("Success");
    switch (sbm.serverconfig.skinId) {
      case 1: // spin and win
        imagePath = "/_images/common/icons/dice.svg";
        break;

      case 2: // kittybingo
        imagePath = "/_images/common/nav-social-icons/dice.svg";
        break;

      case 3: // luckypantsbingo
        imagePath = "/_images/common/nav-social-icons/dice.svg";
        break;

      case 5: // magicalvegas
        imagePath = "/_images/common/icons/dice-black.svg";
        break;

      case 12: // aspers
        imagePath = "/_images/common/icons/dice-black.svg";
        break;

      default:
        imagePath = "_images/common/nav-social-icons/dice.svg";
        break;
    }
    this.notification(
      "Congrats! Your claim code has been accepted and your " +
        prize +
        "  is credited. Go to <img src='" +
        imagePath +
        "' alt='Extras' style='width:32px'> to use your " +
        prize
    );
    this.system(system);

    this.callbackOnConfirmed = function() {
      typeof onConfirm === "function" && onConfirm();
      this.clearNotification();
    }.bind(this);
    this.callbackOnCancelled = function() {
      this.clearNotification();
    }.bind(this);
  }.bind(this);

  this.onShowNotificationPrizeWheel = function(e, message, system) {
    if (sbm.serverconfig.useAlerts) {
      if (confirm(msg)) {
        onConfirm();
      } else {
        this.clearNotification();
      }

      return;
    }
    var imagePath = "/";
    this.severity("prizeWheel");
    this.title("Success");
    switch (sbm.serverconfig.skinId) {
      case 12: //Aspers
        imagePath = "/_images/prize-wheel/aspers.svg";
        break;

      case 6: //Bingo Extra
        imagePath = "/_images/prize-wheel/be.svg";
        break;

      case 9: //Giveback Bingo
        imagePath = "/_images/prize-wheel/gbb.svg";
        break;

      case 2: // kittybingo
        imagePath = "/_images/prize-wheel/prize-wheel-kb.svg";
        break;

      case 3: //LuckyPants
        imagePath = "/_images/prize-wheel/lp.svg";
        break;

      case 8: //LuckyVip
        imagePath = "/_images/prize-wheel/lvip.svg";
        break;

      case 5: // magicalvegas
        imagePath = "../_images/prize-wheel/prize-wheel-mv.svg";
        break;

      case 10: // regalwins
        imagePath = "../_images/prize-wheel/rw.svg";
        break;

      case 1: // spinandwin
        imagePath = "../_images/prize-wheel/sw.svg";
        break;

      default:
        //KingJack for now (Joao will provide the missing design)
        imagePath = "../_images/prize-wheel/prize-wheel-generic.svg";
        break;
    }
    this.notification(
      message +
        "<br>Go to <img style='max-width: 10%' src='" +
        imagePath +
        "'> to use it."
    );
    this.system(system);

    this.callbackOnConfirmed = function() {
      typeof onConfirm === "function" && onConfirm();
      this.clearNotification();
      return true;
    }.bind(this);
    this.callbackOnCancelled = function() {
      this.clearNotification();
      return true;
    }.bind(this);
  }.bind(this);

  /**
   * Show Error
   */
  this.onShowError = function(e, msg, onConfirm, system) {
    if (sbm.serverconfig.useAlerts) {
      if (confirm(msg)) {
        onConfirm();
      } else {
        onCancel();
      }

      return;
    }
    this.severity("error");
    this.notification(msg);
    this.system(system);

    this.callbackOnConfirmed = function() {
      typeof onConfirm === "function" && onConfirm();
      this.clearNotification();
    }.bind(this);
  }.bind(this);

  /* Three radical claim code */
  this.onShowThreeRadical = function(e, prize, onConfirm, system) {
    if (sbm.serverconfig.useAlerts) {
      if (confirm(msg)) {
        onConfirm();
      } else {
        this.clearNotification();
      }

      return;
    }
    var imagePath = "/";
    this.severity("loading");
    this.title("Success");
    switch (sbm.serverconfig.skinId) {
      case 1: // spin and win
        imagePath = "/_images/common/icons/dice.svg";
        break;

      case 2: // kittybingo
        imagePath = "/_images/common/nav-social-icons/dice.svg";
        break;

      case 3: // luckypantsbingo
        imagePath = "/_images/common/nav-social-icons/dice.svg";
        break;

      case 5: // magicalvegas
        imagePath = "/_images/common/icons/dice-black.svg";
        break;

      case 12: // aspers
        imagePath = "/_images/common/icons/dice-black.svg";
        break;

      default:
        imagePath = "_images/common/nav-social-icons/dice.svg";
        break;
    }
    this.notification(
      "Congrats! Your claim code has been accepted and your " +
        prize +
        "  is credited. Go to <img src='" +
        imagePath +
        "' alt='Extras' style='width:32px'> to use your " +
        prize
    );
    this.system(system);

    this.callbackOnConfirmed = function() {
      typeof onConfirm === "function" && onConfirm();
      this.clearNotification();
    }.bind(this);
    this.callbackOnCancelled = function() {
      this.clearNotification();
    }.bind(this);
  }.bind(this);

  /* Please wait while we validate */
  this.onShowPleaseWait = function(e, title, message, system) {
    this.severity("loading");
    this.title(title);
    this.notification(message);
    this.system(system);
  }.bind(this);

  /* Present user with response after checking something */
  this.onShowResult = function(e, type, title, message) {
    this.severity(type);
    this.title(title);
    this.showCancelOnConfirmation(true);
    this.notification(message);
    this.system("result");
  }.bind(this);

  /* Present user with URU Check notification */
  this.onShowURUDialog = function(e, type, title, message) {
    this.severity(type);
    this.title(title);
    this.notification(message);
    this.system("uru-check");
  }.bind(this);

  /* Present user with URU Check notification (From welcome page)*/
  this.onShowLCCPFromWelcome = function(e) {
    var type = "not-verified-w";
    var title = "Identity Verification";
    var message =
      "<p>As required by UK regulations we need to verify your identity before you can play our games. Please select any payment method on the next page to start the verification.</p>";
    this.severity(type);
    this.title(title);
    this.notification(message);
    this.system("uru-check");
  }.bind(this);

  this.switchToURUUpload = function(e) {
    sb.notify("cashier-switch-to-uru-upload");
  }.bind(this);

  this.URUUploadDoLater = function(e) {
    if (
      document.location.pathname === "/" ||
      document.location.pathname === "/login/" ||
      document.location.pathname === "/promotions/"
    ) {
      sb.initRoute("/start");
      sb.notify("hide-notification-dialog");
    } else {
      sb.notify("hide-notification-dialog");
    }
  }.bind(this);

  this.URUSuccessful = function(e) {
    this.notification("");
    sb.notify("cashier-uru-check-successful");
  }.bind(this);

  /**
   * Show loading animation
   */
  this.onShowLoading = function() {
    this.loading(true);
  }.bind(this);

  /**
   * Hide loading animation
   */
  this.onHideLoading = function() {
    this.loading(false);
  }.bind(this);

  /**
   * Remove notification
   */
  this.clearNotification = function() {
    this.notification("");
    this.onHideLoading();
  }.bind(this);
};
