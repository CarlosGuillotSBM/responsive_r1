var sbm = sbm || {};

/**
 * Track the navigation of the player (Funnel)
 * @param sb The sanbox
 * @constructor
 */
sbm.TrackNavigation = function (sb, options) {
    "use strict";


    /**
     * Module initialization
     */
    this.init = function () {

        sb.listen("tracked-navigation", this.onTrackedNavigation);

        /* First action 
        * we send the page info to track it
        */

        sb.trackNavigation("visit",'');

    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("tracked-navigation");
    };


    /**
     * The paged has been tracked and we need to get it tracked
     */
    this.onTrackedNavigation = function (e, data) {
        if (data.Code === 0) {

            // we keep thep the CookieGUID field of the response as 'CookieTrackGUID' in a browser's cookie
            sb.setCookie("cookieTrackGuid", data.CookieGUID);

        }
    }.bind(this);


};