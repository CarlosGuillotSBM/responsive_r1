 /**
 * Module to track Aspers app
 Events that we need to track:
 * Registration
 * Deposit
 * Deposit Count (number of times they've deposited)
 * Deposit Amount
 * Player ID
 *
 * @param sb
 * @constructor
 */
var sbm = sbm || {};

  
sbm.AspersAppTracking = function (sb) {

    var isWebApp = (typeof webApp !== 'undefined');
    var playerId;


    if (isWebApp) {



        this.init = function () {
            sb.listen('registered-player', registerdPlayer);
            sb.listen('player-has-deposited', playerHasDeposited);
            sb.listen('cashier-first-deposit', firstDeposit);
            sb.listen('done-login', doLogin);
        };


        // Registration on aspers hybrid app
        function registerdPlayer(e, data) {

            playerId = data.PlayerID;

            if (webApp.active()) { //true if is the app view

                var eventName = "af_complete_registration"
                var eventParams = {
                    af_customer_user_id: playerId,
                    af_event_end: new Date().getTime()
                };
                webApp.trackEvent(eventName, JSON.stringify(eventParams));

            }


        }//end registeredPlayer

        function playerHasDeposited(e, data) {
            playerId = data.PlayerID;

            if (webApp.active()) { //true if is the app view

                var eventName = "af_purchase";
                var eventParams = {
                    af_customer_user_id: playerId,
                    af_revenue: "8000000",
                    af_currency: "pounds",
                    af_event_end: new Date().getTime()
                };
                webApp.trackEvent(eventName, JSON.stringify(eventParams));
            }

        }

        function firstDeposit(e, data) {
            playerId = data.PlayerID;

            if (webApp.active()) { //true if is the app view

                var eventName = "af_ftd";
                var eventParams = {
                    af_customer_user_id: playerId,
                    af_revenue: "8000",
                    af_currency: "pounds",
                    af_event_end: new Date().getTime()
                };
                webApp.trackEvent(eventName, JSON.stringify(eventParams));
            }
        }


        function doLogin(e, data) {
            playerId = data.PlayerID;
            var eventName = "af_login";
            var eventParams = {
                af_customer_user_id: playerId,
                af_session_id: data.SessionID,
                af_currency: "pounds",
                af_event_end: new Date().getTime()
            };

            webApp.trackEvent(eventName, JSON.stringify(eventParams));
        }


        this.destroy = function () {


        };
    };
};
