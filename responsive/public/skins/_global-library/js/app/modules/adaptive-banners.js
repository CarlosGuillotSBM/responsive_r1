var sbm = sbm || {};

sbm.AdaptiveBanner = function (sb, options) {
    "use strict";
    this.registeredUser;
    this.deposits;
    this.windowWidth=window.innerWidth;
    this.smallBreakpoint=640;
    this.mediumBreakpoint=1024;
    this.displaySize='';
    var vm= this;
    this.cookieHero = sb.ko.observable(true);
    this.getConfig = function(){
        this.registeredUser = (sb.getFromStorage('username')!= null ? sb.getFromStorage('username') : false);
        this.deposits=0;
        if (this.registeredUser != false){
            this.deposits=(sb.getFromStorage('depositCount'));
        };
        this.displaySize='L';
        if(sb.$('.ui-adaptive-hero-banner').length>0){
            switch(sb.$('.ui-adaptive-hero-banner').data().device){
                case 1: this.displaySize='L';
                break;
                case 2: this.displaySize='M';
                break;
                case 3: this.displaySize='S';
                break;
            }
            if(window.devicePixelRatio >= 2){
                this.displaySize=this.displaySize+'R';
            }
        }
    }.bind(this);
    
    this.init = function () {
        this.cookieHeroExists();
        this.getConfig();
        if(sb.$('.ui-adaptive').length || sb.$('.ui-adaptive-hero-banner').length){
            if(sb.$("span[data-adaptive-restriction]").length){
                this.run();
            }
        }
    };
    
    this.adaptBanner = function (adaptiveElement, rulesetName){
        var adaptiveInfo;
        var $rules=sb.$("span[data-adaptive-element='"+rulesetName+"']");
        var ruleSet= {};
        sb.$.each($rules, function (index, value){
            ruleSet[sb.$(value).data("adaptiveRestriction").toUpperCase()]=$(value)[0];
        });
        if(this.registeredUser!=false){
            if(this.deposits==0){
                adaptiveInfo=sb.$(ruleSet["DEP0"]).data();
            }
            else{
                adaptiveInfo=sb.$(ruleSet["DEP1"]).data();
            }
        }
        else{
            adaptiveInfo=sb.$(ruleSet["NEW"]).data();
        }
        if(adaptiveInfo){
            this.adaptImage(adaptiveElement.find('.ui-adaptive-img'),adaptiveInfo['adaptiveImage']);
            this.adaptHTML(adaptiveElement.find('.ui-adaptive-text'),adaptiveInfo['adaptiveTc']);
            this.adaptLink(adaptiveElement.find('.ui-adaptive-link'),adaptiveInfo['adaptiveLink']);
            this.adaptLink(adaptiveElement.find('.ui-adaptive-tclink'),adaptiveInfo['adaptiveTcLink']);
        }
    }.bind(this);
    
    this.adaptHeroBanner = function (adaptiveElement, rulesetName){
        var adaptiveInfo;
        var $rules=sb.$("span[data-adaptive-element='"+rulesetName+"']");
        var ruleSet= {};
        sb.$.each($rules, function (index, value){
            ruleSet[sb.$(value).data("adaptiveRestriction").toUpperCase()]=$(value)[0];
        });
        if(this.registeredUser!=false){
            if(this.deposits==0){
                adaptiveInfo=sb.$(ruleSet["DEP0"]).data();
            }
            else{
                adaptiveInfo=sb.$(ruleSet["DEP1"]).data();
            }
        }
        else{
            adaptiveInfo=sb.$(ruleSet["NEW"]).data();
        }
        if(adaptiveInfo){
            switch(this.displaySize){
                case 'S':
                    this.adaptBackground(adaptiveElement,adaptiveInfo['adaptiveImageMobile']);
                    break;
                case 'SR':
                    this.adaptBackground(adaptiveElement,adaptiveInfo['adaptiveImageMobileRetina']);
                    break;
                case 'M':
                    this.adaptBackground(adaptiveElement,adaptiveInfo['adaptiveImageTablet']);
                    break;
                case 'MR':
                    this.adaptBackground(adaptiveElement,adaptiveInfo['adaptiveImageTabletRetina']);
                    break;
                case 'LR':
                    this.adaptBackground(adaptiveElement,adaptiveInfo['adaptiveImageDesktopRetina']);
                    break;
                default:
                    this.adaptBackground(adaptiveElement,adaptiveInfo['adaptiveImageDesktop']);
                    break;
            }
            this.adaptHTML(adaptiveElement.find('.hero--firstline'),adaptiveInfo['adaptiveLine1']);
            this.adaptHTML(adaptiveElement.find('.hero--secondline'),adaptiveInfo['adaptiveLine2']);
            this.adaptHTML(adaptiveElement.find('.hero--fourthline'),adaptiveInfo['adaptiveLine3']);
            this.adaptHTML(adaptiveElement.find('.aspersbutton__copy'),adaptiveInfo['adaptiveCtaText']);
            this.adaptLink(adaptiveElement.find('.aspersbutton__copy'),adaptiveInfo['adaptiveCtaLink']);
            this.adaptLink(adaptiveElement.find('.hero--cta'),adaptiveInfo['adaptiveCtaLink']);
            this.adaptHTML(adaptiveElement.find('.hero--terms__link'),adaptiveInfo['adaptiveTc']);
            this.adaptLink(adaptiveElement.find('.hero--terms__link'),adaptiveInfo['adaptiveTcLink']);
            this.adaptHTML(adaptiveElement.find('.hero--terms__extra'),adaptiveInfo['adaptiveCopy']);

        }
    }.bind(this);
    
    this.run = function(){
        // Check for banners
        var $adaptiveElements = sb.$(".ui-adaptive-banner");
        var rulesetName='';
        var adaptiveElement='';
        /*
        sb.$( window ).resize(function() {
            vm.init();
        });
        */
        sb.$.each($adaptiveElements, function (index, value){
            adaptiveElement=(sb.$(value));
            rulesetName=(adaptiveElement[0]['dataset']['adaptiveName']);
            vm.adaptBanner(adaptiveElement,rulesetName);
        });
        // Check for Hero banners
        var $adaptiveElements = sb.$(".ui-adaptive-hero-banner");
        var rulesetName='';
        var adaptiveElement='';
        sb.$.each($adaptiveElements, function (index, value){
            adaptiveElement=(sb.$(value));
            rulesetName=(adaptiveElement[0]['dataset']['adaptiveName']);
            vm.adaptHeroBanner(adaptiveElement,rulesetName);
        });
    }.bind(this);
    
    this.adaptImage = function(element, src){
        sb.$(element).attr('src',src);
    }.bind(this);
    
    this.adaptBackground = function(element, src){
        sb.$(element).css("background-image", "url("+src+")"); 
    }.bind(this);
    
    this.adaptHTML= function(element, text){
        sb.$(element).html(text);
    }.bind(this);
    
    this.adaptLink= function(element, text){
        sb.$(element).attr("href",text);
    }.bind(this);

    this.cookieHeroExists = function(){
        if(sb.getCookie('PlayerID') !== null && sb.getCookie('PlayerID') !== undefined){
            this.cookieHero(false);
        }
    }.bind(this);
}