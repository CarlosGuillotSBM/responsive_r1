var sbm = sbm || {};

/**
* Login Box
* Responsible for handling the login on the website
* @param sb The sandbox
* @constructor
*/
sbm.LoginBox = function (sb, options) {
    "use strict";
    
    this.username = sb.ko.observable(sb.getFromStorage("username"));
    this.password = sb.ko.observable("");
    this.invalidUsername = sb.ko.observable(false);
    this.invalidPassword = sb.ko.observable(false);
    this.loginError = sb.ko.observable("");
    this.salutation = sb.ko.observable("");
    this.lastLogged = sb.ko.observable("");
    this.balance = sb.ko.observable("");
    this.gameId = 0;
    this.roomId = 0;
    this.gamePosition = 0;
    this.gameContainerKey = "";
    this.passwordShowText = sb.ko.observable("Show");
    this.redirectURL = sb.ko.observable("");
    this.$modalWithoutDetails = sb.$(options.modalSelectorWithoutDetails);
    this.showDeposit = sb.ko.observable(false);
    this.loggedInFromModal = false;
    this.lockedUser = sb.ko.observable('');
    
    this.showPassword = sb.ko.observable(false);
    this.sofRequired = sb.ko.observable(false);
    
    /**
    * Module initialization
    */
    this.init = function () {
        sb.listen("done-login", this.onDoneLogin);
        sb.listen("accepted-tc", this.onAcceptedTC);
        sb.listen("set-redirect-URL", this.setRedirectURL);
        sb.listen("open-custom-modal", this.openLoginModalWithoutDetails);
        sb.listen("done-popup-login", this.onDoneLogin);
        sb.listen("process-successful-login", this.onProcessSuccessfulLogin)
        this.$modalWithoutDetails.on("open", function () {
            sb.$("body").addClass("modal-open");
        });
        this.$modalWithoutDetails.on("close", function () {
            sb.$("body").removeClass("modal-open");
        });
        if (sb.$('#terms-modal') && sb.$('#terms-modal').length > 0) {
            sb.$(document).ready( function () {
                sb.$('#terms-modal').foundation('reveal', 'open');
                
            });
        }
        $(document).on('open.fndtn.reveal', '[data-reveal]', function () {
            var modal = $(this);
            if(modal[0].id === "login-modal"){
                sb.notify("show-recaptcha");
            }
          });
          $(document).on('close.fndtn.reveal', '[data-reveal]', function () {
              var modal = $(this);
              if(modal[0].id === "login-modal"){
                  sb.notify("hide-recaptcha");
              }
            });
        
        this.username("");
        this.password("");
    };
    
    /**
    * Module destruction
    */
    this.destroy = function destroy() {
        sb.ignore("done-login");
        sb.ignore("accepted-tc");
        sb.ignore("set-redirect-URL");
        sb.ignore("open-custom-modal");
    };
    
    /**
    * Try to login
    * Check username and password
    * Can receive game and position when launched from game launcher
    */
    this.doLogin = function (gameId, position, containerKey, roomId) {
        // R10-1183 Only for ASPERS DEV
        
        var valid = true;
        if (!this.username()) {
            this.invalidUsername(true);
            valid = false;
        }
        if (!this.password()) {
            this.invalidPassword(true);
            valid = false;
        }
        this.gameId = gameId;
        this.roomId = roomId;
        this.gamePosition = position;
        this.gameContainerKey = containerKey;
        var async = gameId ? false : true; // async = false fixes blocking popups
        // if we are passed a url we'll use it
        valid && sb.notify("do-login", [this.username(), this.password(), async, this.redirectURL()]);
        
    }.bind(this);
    
    
    this.setRedirectURL = function (data, redirectUrl) {
        this.redirectURL(redirectUrl);
    }.bind(this);

    this.setRedirectURLFromExternal = function (redirectUrl) {
        this.redirectURL(redirectUrl);
    }.bind(this);
    
    /**
    * Did player hit the Enter key?
    */
    this.checkForEnterKey = function (data, e) {
        if (e.which == 13) {
            this.doLogin(data.GameLauncher.gameId(), null, null, data.GameLauncher.roomId());
        } else {
            return true;
        }
    }.bind(this);
    
    this.onProcessSuccessfulLogin = function(e, data){
        // Clean the URL if the login is success
        // this.redirectURL('');
        
        this.closeLoginBox();
        
        sb.$("body").addClass("loggedin");
        sb.sendVariableToGoogleManager("PlayerOLTrackingID", data.PlayerID);
        sb.sendVariableToGoogleManager("LastLoggedDate", new Date().getTime());
        sb.sendVariableToGoogleManager("LoginPath", location.pathname);
        sb.notify("refresh-balances");
        sb.notify("get-favourite-list");
        var label = "";
        if (this.gameId) {
            label = this.gameId;
        }
        if (this.gamePosition) {
            label += "+" + this.gamePosition;
        }
        if (this.gameContainerKey) {
            label += "+" + this.gameContainerKey;
        }
        if (this.roomId) {
            label = this.roomId;
        }
        
        sb.sendDataToGoogle("Login", label);
        
        if (data.RedirectURL === '') {
                // launch game/room   
                if (this.gameId) {
                    sb.notify("launch-game", [this.gameId, 0, 1, data.PlayerID, data.SessionID, null]);
                    
                } else if (this.roomId) {
                    sb.notify("launch-bingo-room", [this.roomId, data.PlayerID, data.Username]);
                    
                } else if (window.location.pathname.indexOf("/promotions/") > -1 && sb.$("#custom-modal").hasClass("ui-to-lobby")) {
                    sb.notify("launch-game-window", [false, null, "room"]);
                    sb.$("#custom-modal").removeClass("ui-to-lobby");
                    
                } else if (window.location.pathname.indexOf("/promotions/") > -1 && sb.$("#custom-modal").hasClass('ui-opt-in')) {
                    
                    sb.notify("opt-in-from-modal", [data.PlayerID]);
                    sb.$("#custom-modal").removeClass('opt-in');
                    this.loggedInFromModal = true;
                }
        }
        
        // redirect player to start if not launching a game/room or is launching in a new window
        if ( data.RedirectURL == '' && ((!this.gameId && !this.roomId) || sbm.serverconfig.gameLaunchMode === "newWindow")) {
            var path = location.pathname;
            if (path.indexOf("/login/") > -1 || path.indexOf("/register/") > -1 || path === "/") {
                sb.initRoute("/start/");
            } else if (path.indexOf("/promotions/") > -1 && !this.loggedInFromModal) {
                sb.initRoute("/promotions/");
            } else {
                sb.$("a.ui-go-home").attr("href", "/start/").text("START");
            }
        } else if  (data.RedirectURL != '') {
            
            // redirect player to start if not launching a game/room or is launching in a new window
            if ( data.RedirectURL == '' && ((!this.gameId && !this.roomId) || sbm.serverconfig.gameLaunchMode === "newWindow")) {
                var path = location.pathname;
                if (path.indexOf("/login/") > -1 || path.indexOf("/register/") > -1 || path === "/") {
                    sb.initRoute("/start/");
                } else if (path.indexOf("/promotions/") > -1 && !this.loggedInFromModal) {
                    sb.initRoute("/promotions/");
                } else {
                    sb.$("a.ui-go-home").attr("href", "/start/").text("START");
                }
            } else if  (data.RedirectURL != '') {
                
                var redirectExternal = data.RedirectURL.toLowerCase().indexOf('http');
                
                if (redirectExternal === -1) {
                    if (data.RedirectURL.indexOf("bingo-room") != -1){
                        var roomId=data.RedirectURL.split("?r=")[1];
                        sb.notify("launch-game-window", [false, roomId, "room"]);
                    }
                    else if(data.RedirectURL=="3rad"){    
                        var playerClass = sb.getCookie("Class");
                        var isFunded = sb.getFromStorage("funded");
                        if(playerClass != -1 && isFunded === "true"){
                            sb.notify("threeRadical-request-token",[]);
                        }
                        else{
                            sb.notify("show-notification-warning", ["Sorry, but you do not qualify for this promotion. If you would like more details please contact customer service.", "alert"]);
                        }
                    }
                    if(data.Code != 10003){
                        window.location.href = "/" + data.RedirectURL + "/";
                    }
                }
                else{
                    window.open(data.RedirectURL);
                }
            }
        }
        if(data.URUVerificationStatus == "1"){
            sb.notify("player-verification", [data.PlayerID, data.SessionID]);
        }
        this.savePlayerDetails(data);
        
        this.postExecuteSomeTask();
        
    }.bind(this);
    
    /**
    * Login request has ended
    * Handles response
    */
    this.onDoneLogin = function (e, data) {
        sb.setCookie("URUVerified", data.URUVerificationStatus);
        
        if (data.Code === 0) {
            sb.notify("process-successful-login",data);
            
        } else {
            
            // DEV-7830 we clean the password when the login fails
            this.password("");
            
            if (data.Code === 10000) {
                
                sb.notify("social-responsible-popup");
                
            } else if (data.Code === 10001) {
                
                // show terms and conditions pop-up
                sb.notify("show-terms-popup", [data.ts]);    
                
            } else if ( data.Code === 3 || data.Code === 4 || data.Code === 6 ) {
                this.lockedUser( data.Code );
                if ( data.Msg ) {
                    this.loginError( data.Msg );
                }
            } else  if (data.Code === 9) {
                
                this.savePlayerDetails(data);
                window.location.href = "/accept-terms-and-conditions/";
                
            } else if (data.Code === 11) {
                
                this.closeLoginBox();
                sb.showError(data.Msg);
                
            } else if (data.Code === 15) {
                
                this.closeLoginBox();
                this.sofRequired(true);
                
            } else if (data.Code === 10002) {
                sb.notify("get-aspers-tc-pp", [data.ts,data.tpID]);    
            } else if (data.Code === 10003) {
                sb.notify("age-id-verification-check-if-verified",[sb.getCookie("URUVerified"),data, true]);
            }  else {
                
                if (data.Msg) {
                    this.loginError(data.Msg);
                }
                
            }
        }
    }.bind(this);
    
    this.postExecuteSomeTask = function () {
        // Remove newbie tab if the user is not a newbie anymore
        if (sbm.serverconfig.skinId === 3 && sb.getFromStorage("depositCount") > 3 && location.pathname.indexOf("/promotions/") > -1) {
            $(".newbie").hide();
            $(".promotions .tabs dd").css({width: "20%"});
        }
    };
    
    /**
    * Show the extra information to the user mobile and lucky
    * @param data response from login request
    */
    this.showExtraInformationAfterLoginForPants = function (data) {
        sb.initRoute("/start/");
        /**
        * ON HOLD FOR NOW!!!!!!!!!!
        */
        return;
        
        this.showDeposit(data.Balance < 2);
        this.salutation("Hi " + data.Username);
        this.lastLogged("You last logged in " + data.LastLoginDate);
        this.balance(sb.getFromStorage("currencySymbol") + data.Balance);
        setTimeout(function(){
            sb.$("#login-modal-more-info").foundation("reveal", "open");
        }, 500);
        sb.$(".claimcode").on("click", function(e){
            e.preventDefault();
            sb.$(".claimcode_modal.deposit-now").hide(0, function(){
                sb.$(".claimcode_modal.redeem").fadeIn(1000);
            });
        });
        sb.$(".claimcode_back").on("click", function(){
            sb.$(".claimcode_modal.redeem, .claimcode_modal.success").hide(0, function(){
                sb.$(".claimcode_modal.deposit-now").fadeIn(1000);
            });
            this.balance(sb.getFromStorage("currencySymbol") + sb.getFromStorage("balance"));
            $(".start-claim-code input").val("");
        }.bind(this));
        $(document).on('closed', '#login-modal-more-info', function () {
            sb.initRoute("/start/");
        });
        
    }.bind(this);
    
    /**
    * Save player data on local storage
    * @param data response from login request
    */
    this.savePlayerDetails = function (data) {
        
        sb.saveOnSessionStorage("playerId", data.PlayerID);
        data.SessionID && sb.saveOnSessionStorage("sessionId", data.SessionID); // session might be empty if player needs to accept TC
        sb.saveOnStorage("username", data.Username);
        sb.setCookie("username", data.Username);
        sb.saveOnStorage("lastLoginDate", data.LastLoginDate);
        sb.saveOnStorage("firstName", data.Firstname);
        sb.saveOnStorage("class", data.Class);
        sb.saveOnStorage("balance", data.Balance);
        sb.saveOnStorage("real", data.Real);
        sb.saveOnStorage("bonus", data.Bonus);
        sb.saveOnStorage("bonusWins", data.BonusWins);
        sb.saveOnStorage("points", data.Points);
        sb.saveOnStorage("favourites", JSON.stringify(data.Favourites));
        sb.saveOnStorage("depositCount", data.DepositCount);
        sb.setCookie("DepositCount", data.DepositCount);
        sb.sendVariableToGoogleManager("DepositCount", data.DepositCount);
        sb.saveOnStorage("currencySymbol", this.getCurrencySymbol(data.CurrencyCode));
        sb.saveOnStorage("daysFromRegistration", data.DaysFromRegistration);
        sb.setCookie("DaysSinceRegistration", data.DaysFromRegistration);
        sb.saveOnStorage("comingFrom", window.location.pathname);
        sb.saveOnStorage("aspersCustomerNumber", data.AspersCustomerNumber);
    };
    
    /**
    * Player accepts terms and conditions
    * Do request
    */
    this.acceptTC = function () {
        
        sb.notify("accept-tc", [sb.getFromStorage("playerId")]);
        
    }.bind(this);
    
    /**
    * Terms and conditions acceptance request has ended
    * Handles response
    */
    this.onAcceptedTC = function (e, data) {
        
        if (data.Code === 0) {
            sb.saveOnSessionStorage("sessionId", data.SessionID);
            var referer = sb.getFromStorage("comingFrom");
            if (referer.indexOf("/login/") > -1 || referer === "/") {
                referer = null;
            }
            window.location.href = (referer) ? referer : "/start/";
        } else {
            if (data.Msg) {
                this.loginError(data.Msg);
            }
        }
        
    }.bind(this);
    
    /**
    * Closes all modals
    */
    this.closeLoginBox = function () {
        var $allModals = sb.$(".reveal-modal");
        if ($allModals.length > 0) {
            $allModals.foundation("reveal", "close");
        }
        sb.notify("hide-recaptcha");
    };
    
    /**
    * Returns the currency symbol
    * @param code - Currency Code of the player
    */
    this.getCurrencySymbol = function (code) {
        if (code === "GBP") return "£";
        if (code === "EUR") return "€";
        if (code === "AUD" || code === "CAD") return "$";
        return "";
    };
    
    /**
    * Makes visible/invisible the password
    */
    this.togglePassword = function () {
        if (this.passwordShowText() == "Show") {
            this.passwordShowText("Hide");
            this.showPassword(false);
            sb.$(".show-password").removeClass("eye-open");
            sb.$(".show-password").addClass("eye-shut");
        } else {
            this.passwordShowText("Show");
            this.showPassword(true);
            sb.$(".show-password").removeClass("eye-shut");
            sb.$(".show-password").addClass("eye-open");
        }
    }.bind(this);
    
    /**
    * Opens game info modal
    * @param game Object containing all the necessary data for the info panel
    */
    this.openLoginModalWithoutDetails = function (e, isForOptin) {
        var modalName = "#custom-modal";
        if(sb.serverconfig.skinId === 10){
            modalName = "#login-modal"
            window.scrollTo(0, 0);
            sb.hideLoadingAnimation();
            sb.$(modalName).foundation("reveal","open");
            sb.notify("show-recaptcha");
        }
        else{
            this.$modalWithoutDetails.foundation("reveal", "open");
            sb.notify("show-recaptcha");
            window.scrollTo(0, 0);
            sb.hideLoadingAnimation();
            if (isForOptin) {
                sb.$(modalName).addClass("ui-opt-in");
                sb.$(modalName).removeClass("ui-to-lobby");
            }else{
                sb.$(modalName).addClass("ui-to-lobby");
                sb.$(modalName).removeClass("ui-opt-in");
            }

        }

        }.bind(this);
    
    
};