var sbm = sbm || {};

/**
 * Reversal details
 * @param sb The sandbox
 * @constructor
 */
sbm.Reversal = function (sb) {
    "use strict";

    this.operations = {
        "reverse" : "REVERSE",
        "retry" : "RETRY",
        "resume" : "RESUME"
    };

    this.amountToReverse = sb.ko.observable("");
    this.availableToReverse = sb.ko.observable("");
    this.minReverse = sb.ko.observable("");
    this.minWithdraw = sb.ko.observable("");
    this.minWithdrawFee = sb.ko.observable("");
    this.txReverseBatchID = sb.ko.observable("");
    this.withdrawalsPending = sb.ko.observable("");
    this.balance = sb.ko.observable("");
    this.error = sb.ko.observable("");
    this.operation = sb.ko.observable(this.operations.reverse);
    this.currencySymbol = sb.ko.observable(sb.getFromStorage("currencySymbol") || "");
    this.error = sb.ko.observable("");
    this.buttonActive = sb.ko.observable(true);

    /**
     * Reversal validation
     * @returns {string} First error or empty
     */
    this.validation = function () {
        // var number_no_more_than_2_decimal = /^\d+\.?\d{1,2}$/;
        var number_no_more_than_2_decimal = /^[1-9]\d*(\.?\d{1,2}$){0,1}$/;
        if (number_no_more_than_2_decimal.test(this.amountToReverse()) === false) {
            return "Amount should be only digits formatted to a maximum of 2 decimals";
        }
        if (this.amountToReverse() < (parseFloat(this.minReverse()))) {
            return "Your reversal amount cannot be below the minimum allowable reversal amount";
        }
        if ((this.amountToReverse() > (parseFloat(this.availableToReverse())))) {
            return "Your reversal amount requested cannot exceed your pending withdrawals";
        }
        return "";
    };
};
