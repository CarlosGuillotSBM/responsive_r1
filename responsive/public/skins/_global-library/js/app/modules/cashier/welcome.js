var sbm = sbm || {};

/**
 * Cashier on welcome page
 * Gets the available offers for first time deposits
 * If is not a first time deposit it becomes unavailable
 * @param sb
 * @param options
 * @constructor
 */
sbm.CashierPRD = function (sb, options) {
    "use strict";

    /**
     * Properties
     */

    // will hold some cashier data related
    this.cashierData = {};
    // will set the visibility of the cashier section on the welcome page
    this.visible = sb.ko.observable(false);
    // will hold no more than 2 promos - these should be the first ones from the original array coming from back end
    this.promos = sb.ko.observable([]);
    // loading
    this.loading = sb.ko.observable(true);
    // is a VIP user
    this.isNotVip = sb.ko.observable(true);

    /**
     * Functions
     */

    /**
     * Module initialization
     * Start cashier
     */
    this.init = function () {

        this.cashierData = new sbm.CashierData(sb, options.source);
        sb.listen("cashier-started", this.onStarted);
        sb.listen("cashier-got-account-types", this.onGotAccountTypes);
        this.start();

    }.bind(this);

    /**
     * Module destructor
     */
    this.destroy = function () {
        sb.ignore("cashier-started");
        sb.ignore("cashier-got-account-types");
    };

    /**
     * Start request has finished
     * Handles response
    0 */
    this.onStarted = function (e, data) {

        if (data.Status === "1") {

            // if player has deposited before return and don't do anything else
            if (data.Deposited === 1) {
                return;
            }

            // all good let's get the player accounts
            this.cashierData.customerId = data.CustomerID;
            this.getAccountTypes();

        }

    }.bind(this);

    /**
     * Request Start
     */
    this.start = function () {
        sb.notify("cashier-start", [this.cashierData]);
        this.checkVIPUser();
    };

    /**
     * Request available methods
     */
    this.getAccountTypes = function () {
        sb.notify("cashier-get-account-types");
    };

    this.checkVIPUser = function () {
          var isVIPuser = sb.getFromStorage("userIsVIP");
          if ( isVIPuser ) {
               return this.isNotVip( false );
          }
          return this.isNotVip( true );
    };

    /**
     * Get available methods request has finished
     * Handles response
     */
    this.onGotAccountTypes = function (e, data) {

        // if success
        if (data.Status === "1") {

            // get the 2 first promotions into the observable array that will bound to the view
            // selecting it from the first account type - the promos are not specific per method

            var promos = data.AccountTypes[0].Promos;

            // remove the ones which require a promo code (promoId =  -1)
            promos = sb._.filter(promos, function (p) { return p.PromoID > 0; });

            // get the first 2
            if (promos.length) {
                this.promos(sb._.first(promos, 2));
                this.visible(true);
            }
        }

        this.loading(false);

    }.bind(this);
};
