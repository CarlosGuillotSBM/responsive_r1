var sbm = sbm || {};

/**
 * Withdrawal details
 * @param sb The sandbox
 * @constructor
 */
sbm.Withdrawal = function (sb) {
    "use strict";

    this.operations = {
        "withdraw" : "WITHDRAW",
        "retry" : "RETRY",
        "resume" : "RESUME",
        "unverified" : "UNVERIFIED"
    };
    this.amountToWithdraw = sb.ko.observable("");
    this.operation = sb.ko.observable(this.operations.withdraw);
    this.balance = sb.ko.observable("");
    this.realBalance = sb.ko.observable("");
    this.availableToWithdraw = sb.ko.observable("");
    this.bonusToBeForfeited = sb.ko.observable(0);
    this.minWithdraw = sb.ko.observable("");
    this.withdrawalsPending = sb.ko.observable(0);
    this.withdrawHoursPending = sb.ko.observable("");
    this.currencySymbol = sb.ko.observable(sb.getFromStorage("currencySymbol") || "");
    this.txWithdrawID = sb.ko.observable("");
    this.totalBalance = sb.ko.pureComputed(function () {
        return parseFloat(parseFloat(this.realBalance()) + parseFloat(this.bonusToBeForfeited())).toFixed(2);
    }.bind(this));
    this.transactionEntity = sb.ko.observable(getTransactionEntity());
    this.error = sb.ko.observable("");
    this.buttonActive = sb.ko.observable(true);
    this.enquiryEmail = sb.ko.observable("");
    this.enquiryEmail = sb.ko.observable("");
    this.displayLimits = sb.ko.observable(false);
    this.withdrawLimitCountPerDay = sb.ko.observable(0);
    this.withdrawCountThisDay = sb.ko.observable(0);
    this.withdrawLimitPerDay = sb.ko.observable(0);
    this.withdrawalsThisDay = sb.ko.observable(0);
    this.withdrawLimitPerWeek = sb.ko.observable(0);
    this.withdrawalsThisWeek = sb.ko.observable(0);
    this.withdrawLimitPerMonth = sb.ko.observable(0);
    this.withdrawalsThisMonth = sb.ko.observable(0);
    this.playerIsUnverified = sb.ko.observable(false);
    this.securityEmail = sb.ko.observable("");
    /**
     * Withdrawal validation
     * @returns {string} First error or empty
     */
    this.validation = function () {
        var number_no_more_than_2_decimal = /^[1-9]\d*(\.?\d{1,2}$){0,1}$/;
        if (number_no_more_than_2_decimal.test(this.amountToWithdraw()) === false) {
            return "Amount should be only digits formatted to a maximum of 2 decimals";
        }
        if (this.amountToWithdraw() <= parseFloat(this.availableToWithdraw())) {
            return "";
        }
        return "Amount to withdraw is invalid";
    };

    /**
     * Get the transaction entity for the withdrawal
     * @returns {string} Transaction entity
     */
    function getTransactionEntity() {
        if (sbm.serverconfig.skinId === 1) {
            return 'DAUB SW';
        }
        if (sbm.serverconfig.skinId === 2) {
            return 'DAUB KB';
        }
        if (sbm.serverconfig.skinId === 3) {
            return 'DAUB LP';
        }
        if (sbm.serverconfig.skinId === 5) {
            return 'DAUB MV';
        }
        if (sbm.serverconfig.skinId === 999) {
            return 'DAUB SW';
        }
        return '';
    }
};
