var sbm = sbm || {};

/**
 * Details needed when player is asked to enter the CVN again
 * @param sb The sandbox
 * @constructor
 */
sbm.RetryCVN = function (sb) {
    "use strict";

    this.txDepositID = "";
    this.dsid = "";
    this.cvn = sb.ko.observable("");
    this.source = "";
    this.skinId = "";
    this.ip = "";
    this.playerId = "";
    this.ssid = "";
    this.device = "";
    this.customerId = "";

};