var sbm = sbm || {};

/**
 * Credit card details
 * @param sb The sandbox
 * @param cardToken
 * @constructor
 */
sbm.CreditCard = function (sb, cardToken) {
    "use strict";

    this.firstName = sb.ko.observable("");
    this.lastName = sb.ko.observable("");
    this.number = sb.ko.observable("");
    this.expiryMonth = sb.ko.observable("");
    this.expiryYear = sb.ko.observable("");
    this.cvn = sb.ko.observable("");
    this.cvnOption = sb.ko.observable(false);
    this.token = cardToken;
    this.validator = new sbm.CardValidator(sb);
    this.first6 = "";
    this.creditCardIcon = sb.ko.observable("");
    
    this.getCardType = function(number){
        // visa
        var re = new RegExp("^4");
        if (number.match(re) != null)
        return "Visa";
        
        // Mastercard 
        re = new RegExp("^(5[1-5]|2[2-7])");
        if (number.match(re) != null)
        return "Mastercard";
        
        //Maestro
        re = new RegExp("^(50|5[6-9]|6[0-9]|6759|676770|676774)");
        if (number.match(re) != null)
        return "Maestro";
        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) != null)
        return "AMEX";
        
        // Discover
        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        if (number.match(re) != null)
        return "Discover";
        
        // Diners
        re = new RegExp("^36");
        if (number.match(re) != null)
        return "Diners";
        
        // Diners - Carte Blanche
        re = new RegExp("^30[0-5]");
        if (number.match(re) != null)
        return "Diners - Carte Blanche";
        
        // JCB
        re = new RegExp("^35(2[89]|[3-8][0-9])");
        if (number.match(re) != null)
        return "JCB";
        
        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null)
        return "Visa Electron";
        return "";
    }
    
    
    
    this.typeOfCard = function(newVal){
        var imgPath = "";
        var typeOfCard = this.getCardType(newVal);
        switch(typeOfCard){
            case "Maestro": imgPath ="maestro-icon.svg";
            break;
            case "Mastercard": imgPath ="mastercard-icon.svg";
            break;
            case "Visa": imgPath ="visa-icon.svg";
            break;
        }
        this.creditCardIcon(imgPath);
    }.bind(this);
    this.number.subscribe(this.typeOfCard);

    /**
     * Validates credit card details
     * @returns {string|*} First error or empty
     */
    this.validation = function () {

        // remove any spaces from number
        this.number(this.number().replace(/ /g, ""));
        this.first6 = this.number().substring(0,6);

        return this.validator.isValidCreditCard({
            firstName: this.firstName(),
            lastName: this.lastName(),
            number: this.number(),
            expiryYear: this.expiryYear(),
            expiryMonth: this.expiryMonth(),
            cvn: this.cvn()

        });
    };

};