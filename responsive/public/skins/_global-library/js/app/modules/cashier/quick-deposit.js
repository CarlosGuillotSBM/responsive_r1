var sbm = sbm || {};

/**
 * Handles player interaction on Quick Deposit
 * Responsible module for all user input and actions
 * @param sb
 * @param options
 * @constructor
 */
sbm.QuickDeposit = function (sb, options) {
    "use strict";

    // subscriptions
    this.onMethodSelection = function (accountId) {
        this.handleMethodSelection(accountId);
    }.bind(this);

    this.onPromoSelection = function (promoId) {
        this.handlePromoSelection(promoId);
    }.bind(this);

    this.onCvnChanged = function (newVal) {
        if (newVal) {
            this.cvnInvalid(false);
        }
    }.bind(this);

    this.onSelectedAmountChanged = function (newVal) {
        if (newVal) {
            this.setDepositPromoInfo(this.selectedPromo(), newVal);
        }
    }.bind(this);

    // cashier data needed for the requests to Coin
    this.cashierData = {};

    // controls visibility of the loading gif
    this.loading = sb.ko.observable(true);

    // controls visibility of the success panel
    this.success = sb.ko.observable(false);

    // controls the visibility of the quick deposit panel
    this.opened = sb.ko.observable(false);

    // for error handling
    this.error = sb.ko.observable({
        code: sb.ko.observable(""),
        message: sb.ko.observable(""),
        openTime: sb.ko.observable("")
    });

    // the available promotions
    this.promotions = sb.ko.observableArray([]);
    this.selectedPromo = sb.ko.observable("");
    this.selectedPromo.subscribe(this.onPromoSelection);

    // the available methods
    this.methods = sb.ko.observableArray([]);
    this.selectedMethod = sb.ko.observable("");
    this.selectedMethod.extend({ notify: 'always' });
    this.selectedMethod.subscribe(this.onMethodSelection);

    // amounts
    this.selectedAmount = sb.ko.observable("");
    this.selectedAmount.subscribe(this.onSelectedAmountChanged);
    this.minAmount = sb.ko.observable("");
    this.maxAmount = sb.ko.observable("");

    // cvn
    this.needsCvn = sb.ko.observable(false);
    this.cvnInvalid = sb.ko.observable(false);
    this.cvn = sb.ko.observable("");
    this.cvn.subscribe(this.onCvnChanged);

    // currency
    this.currencySymbol = sb.getFromStorage("currencySymbol");

    // what player is going to get in bonus and prizes
    this.depositPromoInfo = sb.ko.observable("");

    // controls the visibility of the error panel
    this.showError = sb.ko.observable(false);

    this.$quickDepositContainer = $("#ui-quick-deposit-container");

    // jquery ui slider
    this.$slider = sb.$("#ui-withdrawal-slider");

    // deposit response
    this.bonusReceived = sb.ko.observable("");
    this.balance = sb.ko.observable("");
    this.amountDeposited = sb.ko.observable("");
    this.success = sb.ko.observable(false);

    // neteller
    this.showSecureId = sb.ko.observable(false);
    this.secureId = sb.ko.observable("");
    this.secureIdInvalid = sb.ko.observable(false);

    /**
     * module initialization
     */
    this.init = function () {

        // workaround - remove previous added promos
        $("#ui-quick-deposit-promos").html($("#ui-quick-deposit-promos-tmpl").html());

        sb.listen("start-quick-deposit", this.start);
        sb.listen("quick-cashier-started", this.onStarted);
        sb.listen("close-quick-deposit", this.cancel);
        sb.listen("quick-cashier-got-customer-accounts", this.onGotCustomerAccounts);
        sb.listen("quick-deposit-deposited", this.onDeposited);
        sb.listen("launch-game", this.onLaunchGame);
        sb.listen("game-launcher-set-room", this.onLaunchGame);

    };

    /**
     * module destruction
     */
    this.destroy = function () {

        sb.ignore("start-quick-deposit");
        sb.ignore("quick-cashier-started");
        sb.ignore("close-quick-deposit");
        sb.ignore("quick-cashier-got-customer-accounts");
        sb.ignore("quick-deposit-deposited");

    };

    /**
     * show quick deposit panel
     * get customer default account
     */
    this.start = function () {

        this.loading(true);
        this.cashierData = new sbm.CashierData(sb, options.source);

        // coin request to start
        sb.notify("quick-cashier-start", [this.cashierData]);

        this.opened(true);
        this.handleQuickDepositVisibility();


    }.bind(this);

    this.onStarted = function (e, data) {

        if (data.Status === "1") {

            this.cashierData.customerId = data.CustomerID;
            this.cashierData.activeAccounts = data.ActiveAccounts;
            this.cashierData.felfExclude = data.SelfExclude;

            if (data.ActiveAccounts > 0) {
                sb.notify("quick-cashier-get-customer-accounts");
            } else {
                this.handleError("-1", sb.i18n("quickDepNoAccount"));
            }

        } else if (data.Status === "-3") {
            sb.showError(data.StatusMsg);
        } else {
            this.handleError(data.Status, data.StatusMsg, data.OpenTime);
        }

    }.bind(this);

    /**
     * close quick deposit panel
     */
    this.cancel = function () {

        this.opened(false);
        this.loading(true);
        this.success(false);
        this.error().code("");
        this.showError(false);
        sb.hideLoadingAnimation();

    }.bind(this);

    /**
     * get customer default account
     */
    this.onGotCustomerAccounts = function (e, data) {

        if (data.Status === "1") {

            if (!data.Accounts.length) {
                this.handleError("-1", sb.i18n("quickDepNoAccount"));
            }

            // get only the supported methods - credit card and neteller

            var validMethods = sb._.filter(data.Accounts, function (account) {
                return this.isACC(account.AccountTypeCode) || (account.AccountTypeCode === "A_NETELLER");
            }.bind(this));

            // if has any valid method

            if (validMethods.length) {

                // change the accounts as needed for display
                sb._.each(validMethods, function (method) {

                    method.accountDescription = method.Description + " " + method.AccountNumber;

                    // filter promo -1 (only for promo code)
                    method.Promos = sb._.filter(method.Promos, function (promo) {
                        return promo.PromoID !== "-1";
                    });

                    // trim promotions with more than 16 chars
                    sb._.each(method.Promos, function (promo) {
                        if (promo.Description.length > 18) {
                            promo.tooltip = promo.Description;
                            promo.Description = promo.Description.substring(0, 18) + "...";
                        } else {
                            promo.tooltip = "";
                        }
                    });

                    // add option for No Bonus For Me
                    method.Promos.unshift({
                        "PromoID": "0",
                        "Description": sb.i18n("noBonusForMe"),
                        "DepositMin": "" + method.Amounts[0].Amount,
                        "tooltip": "",
                        "Percent": "0",
                        "PrizeAmount": "0",
                        "PrizeType": ""
                    });

                    method.DepositMax = "" + method.Amounts[method.Amounts.length - 1].Amount;
                    method.AccountTypeCode = this.isACC(method.AccountTypeCode) ? "A_CC" : method.AccountTypeCode;

                }.bind(this));

                // store the methods
                this.methods(validMethods);

                // set default
                var index = sb._.findIndex(this.methods(), function (method) {
                    return method.DefaultAccount === "1";
                });
                // if there is a default account
                if (index > -1) {
                    this.selectedMethod(validMethods[index || 0].AccountID);
                    this.cvn("");
                }
            } else {
                this.handleError("-1", sb.i18n("quickDepNoDefaultAccount"));
            }

        } else if (data.Status === "-3") {
            sb.showError(data.StatusMsg);
        } else {
            this.handleError(data.Status, data.StatusMsg, data.OpenTime);
        }

        this.loading(false);

    }.bind(this);


    /**
     * Checks if the account type is a debit/credit card
     * @param accountTypeCode - account type code
     * @returns {boolean}
     */
    this.isACC = function (accountTypeCode) {
        return accountTypeCode === "A_VISA_CREDIT" || accountTypeCode === "A_VISA_DEBIT" || accountTypeCode === "A_ELECTRON" ||
            accountTypeCode === "A_MASTERCARD_CREDIT" || accountTypeCode === "A_MAESTRO" || accountTypeCode === "A_MASTERCARD_DEBIT";
    };

    /**
     * Handles errors
     * @param status Error status code
     * @param msg Message to show
     * @param openTime  Cashier's open time
     */
    this.handleError = function (status, msg, openTime) {

        // session expired
        if (status === "403") {
            alert(msg);
            sb.initRoute("/");
        }
        // other errors
        this.error().code(status);
        this.error().message(msg);
        this.error().openTime(openTime || "");

        this.showError(true);
        sb.hideLoadingAnimation();
        this.loading(false);

    };

    /**
     * The selected method has changed - handles the selection and set promos and amounts (if no promos)
     * @param accountId The accountId of the selected method
     */
    this.handleMethodSelection = function (accountId) {

        if (!accountId) return;

        // clear promo
        this.selectedPromo("");

        // find the selected account by Id
        var method = this.getSelectedMethod(accountId);

        this.promotions(method.Promos);

        this.needsCvn(method.CVN === "1");
        this.cvn("");
        this.selectedPromo("0");

        this.showSecureId(method.AccountTypeCode === "A_NETELLER");
    };

    /**
     * The selected promo has changed - handles the selection and set amounts
     * @param promoId The promoId of the selected method
     */
    this.handlePromoSelection = function (promoId) {

        var promo = this.getSelectedPromo(promoId);
        var method = this.getSelectedMethod(this.selectedMethod());
        if (promo && method) {
            this.setSlider(promo.DepositMin, method.DepositMax, promo.DepositMin);
        }
    };

    /**
     * Quick Deposit slider configuration
     * @param min The minimum value to deposit
     * @param max The maximum value to deposit
     * @param defaultVal The default value to be set on the slider
     */
    this.setSlider = function (min, max, defaultVal) {

        min = parseInt(min, 10);
        max = parseInt(max, 10);
        defaultVal = parseInt(defaultVal, 10);

        this.selectedAmount(parseFloat(defaultVal).toFixed(2));
        this.minAmount(parseFloat(min).toFixed(2));
        this.maxAmount(parseFloat(max).toFixed(2));

        var reachedMax = false;
        var step = 5;

        this.$slider.slider({
            value: defaultVal,
            min: min,
            max: max,
            step: step,
            slide: function (e, ui) {
                reachedMax = ui.value + step > max;
                if (reachedMax) {
                    this.selectedAmount(parseFloat(max).toFixed(2));
                } else {
                    this.selectedAmount(parseFloat(ui.value).toFixed(2));
                }
            }.bind(this)
        });

    }.bind(this);

    /**
     * Retrieve the selected account
     * @param accountId
     * @returns {*}
     */
    this.getSelectedMethod = function (accountId) {
        return sb._.find(this.methods(), function (method) {
           return method.AccountID === accountId;
        });
    };

    /**
     * Retrieve the selected promo
     * @param promoId
     * @returns {*}
     */
    this.getSelectedPromo = function (promoId) {
        return sb._.find(this.promotions(), function (promo) {
            return promo.PromoID === promoId;
        });
    };

    /**
     * Request a new deposit
     */
    this.makeDeposit = function () {

        // if cvn is invalid
        if (this.needsCvn() && !this.isValidCVN(this.cvn())) {
            this.cvnInvalid(true);
            return;
        }

        // if secure Id is invalid
        if (this.showSecureId() && !this.isValidSecureId(this.secureId())) {
            this.secureIdInvalid(true);
            return;
        }

        // valid deposit data let's do the request

        var method = this.getSelectedMethod(this.selectedMethod());

        var depositData = {
            accountId: method.AccountID,
            amount: this.selectedAmount(),
            promoId: this.selectedPromo(),
            cvn: this.cvn(),
            cvnOption: this.needsCvn() ? "0" : "",
            customerId: this.cashierData.customerId,
            accountType: method.AccountTypeCode,
            secureId: this.secureId()
        };

        sb.notify("quick-deposit-deposit", [depositData, this.cashierData]);

        this.loading(true);

    }.bind(this);

    /**
     * Validates if CVN is a number and is 3 or 4 characters length
     * @param cvn
     * @returns {boolean}
     */
    this.isValidCVN = function (cvn) {
        return (!isNaN(cvn) && (cvn.length === 3 || cvn.length === 4)) ? true : false;
    };

    /**
     * Validates a Neteller secure Id
     * @returns {boolean} First error or empty
     */
    this.isValidSecureId = function (secId) {
        // validates if secure Id is 6 digits
        return !isNaN(secId) && secId.length === 6;
    };

    this.onDeposited = function (e, data) {

        this.loading(false);
        if (data.Status === "1") {

            // success

            this.amountDeposited(data.Amount);
            this.bonusReceived(data.Bonus);
            this.balance(data.Balance);
            this.success(true);

            var category = "";

            if (data.DepositSuccessCount === 1) {
                category = "Quick FTD";
            } else if (data.DepositSuccessCount === 2) {
                category = "Quick 2ndDeposit";
            } else if (data.DepositSuccessCount === 3) {
                category = "Quick 3rdDeposit";
            } else {
                category = "Quick Deposit";
            }

            sb.notify("player-has-deposited", data.DepositSuccessCount);
            sb.sendDataToGoogle(category, sbm.core.storage.get("playerId") + "+" + this.gameId);

        } else {
            this.handleError(data.Status, data.StatusMsg, data.OpenTime);
        }

    }.bind(this);

    /**
     * Checks if the quick deposit panel is off screen
     * If it is offscreen lets add a new class to position it inside the iframe
     */
    this.handleQuickDepositVisibility = function () {

        // set again the default position
        this.$quickDepositContainer.removeClass("inside");

        // set the right position depending if is off screen
        var rect = this.$quickDepositContainer[0].getBoundingClientRect();

        if (rect.bottom <= $(window).height() && rect.right <= $(window).width()) {
            this.$quickDepositContainer.removeClass("inside");
        } else {
            this.$quickDepositContainer.addClass("inside");
        }

        sb.$("#ui-quick-deposit-container").draggable();

        var offsetGameHolder = sb.$("#ui-game-launch-holder").position();
        var widthGameHolder = sb.$("#ui-game-launch-holder").width();
        var widthQuickDepositContainer = this.$quickDepositContainer.width();
        var newPositionLeftQuickDeposit = offsetGameHolder.left + widthGameHolder - widthQuickDepositContainer;
        var newPositionTopQuickDeposit = offsetGameHolder.top + sb.$(".games-iframe-content__top-bar").height();

        this.$quickDepositContainer.css({ left: newPositionLeftQuickDeposit + 'px', top: newPositionTopQuickDeposit + 'px'});

    };

    /**
     * Sets the information about what bonus and prize will the player get
     * This info depends on the selected promotion and amount
     * @param promoId The promo Id
     * @param amount The selected amount
     */
    this.setDepositPromoInfo = function (promoId, amount) {
        // get the promotion from the list
        var promo = sb._.find(this.promotions(), function (promo) {
           return promo.PromoID === promoId;
        });

        // check promo data and set the information
        if (promo.PromoID !== "0" && amount > 0) {
            var bonusAmount = amount * (parseInt(promo.Percent, 10) / 100);
            if (bonusAmount > promo.MaxBonus) {
                bonusAmount = promo.MaxBonus;
            }

            bonusAmount = Math.round(bonusAmount * 100) / 100;

            var bonusExtra = parseInt(promo.PrizeAmount, 10) > 0 ? promo.PrizeAmount + " " + promo.PrizeType : "";
            var text = "And get " + this.currencySymbol + bonusAmount + " bonus";
            if (bonusExtra !== "") {
                text += " + " + bonusExtra;
            }
            this.depositPromoInfo(text);
        } else {
            this.depositPromoInfo("");
        }
    };

    /**
     * Save in memory the game that was launched
     * We'll send this information to google tag manager
     * @param e
     * @param gameId The game Id
     */
    this.onLaunchGame = function (e, gameId) {
        this.gameId = gameId;
    }.bind(this);

};