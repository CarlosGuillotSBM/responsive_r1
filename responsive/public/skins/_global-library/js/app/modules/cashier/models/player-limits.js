var sbm = sbm || {};

/**
 * Players limits
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.PlayerLimits = function (sb) {
    "use strict";

    /**
     * Properties
     */

    this.currencySymbol = sb.getFromStorage("currencySymbol");

    // current action being performed
    this.currentAction = sb.ko.observable("");

    // true if player can only set monthly limit
    this.allowOnlyMonthlyLimit = sb.ko.observable(false);

    // deposit limit status
    // 0 - nothing pending
    // 2 - pending confirmation
    // 3 - can confirm increase (already been 24h from request)
    // 4 - pending a deposit limit decrease (at midnight)
    this.depositLimitStatus = "0";

    // possible daily limits
    this.dailyLimits = sb.ko.observableArray([]);

    // possible weekly limits
    this.weeklyLimits = sb.ko.observableArray([]);

    // possible monthly limits
    this.monthlyLimits = sb.ko.observableArray([]);

    // selected limit period
    this.selectedPeriod = sb.ko.observable("");

    // daily limit selected from dropdown
    this.selectedDailyLimit = sb.ko.observable("");

    // free daily amount entered
    this.freeDailyLimitAmount = sb.ko.observable("");

    // weekly limit selected from dropdown
    this.selectedWeeklyLimit = sb.ko.observable("");

    // free weekly amount entered
    this.freeWeeklyLimitAmount = sb.ko.observable("");

    // monthly limit selected from dropdown
    this.selectedMonthlyLimit = sb.ko.observable("");

    // free monthly amount entered
    this.freeMonthlyLimitAmount = sb.ko.observable("");

    // free amount entered on free text input
    this.freeAmountEntered = sb.ko.observable("");

    // daily limit set by none, player or CAT
    this.dailyLimitSetBy = sb.ko.observable("");

    // weekly limit set by none, player or CAT
    this.weeklyLimitSetBy = sb.ko.observable("");

    // monthly limit set by none, player or CAT
    this.monthlyLimitSetBy = sb.ko.observable("");

    // player's password
    this.password = sb.ko.observable("");

    // password validation error message
    this.passwordError = sb.ko.observable("");

    // invalid amount error
    this.invalidAmount = sb.ko.observable("");

    // limits to set
    this.limitsToSave = {
       daily: sb.ko.observable(""),
       weekly: sb.ko.observable(""),
       monthly: sb.ko.observable("")
    };

    // if  player can remove the limits - will set the visibility of the button
    this.canRemoveLimits = sb.ko.observable(true);

    // locked limits 
    this.lockedLimits = {
        active: false,
        perDay: 0,
        perWeek: 0,
        perMonth: 0
    };

    // static property to hold no limit option
    this.NOLIMIT = "No Limit";

    // mark daily limit as changed by player
    this.limitsToSave.daily.subscribe(function () {
        this.limitsToSave.daily() !== 0 && this.dailyLimitSetBy("Player");
    }.bind(this));

    // mark weekly limit as changed by player
    this.limitsToSave.weekly.subscribe(function () {
        this.limitsToSave.weekly() !== 0 && this.weeklyLimitSetBy("Player");
    }.bind(this));

    // mark monthly limit as changed by player
    this.limitsToSave.monthly.subscribe(function () {
        this.limitsToSave.monthly() !== 0 && this.monthlyLimitSetBy("Player");
    }.bind(this));

    // flag that indicates that there are limits to be saved
    this.modified = sb.ko.observable(false);

    // pending daily limit
    this.pendingDailyLimit = sb.ko.observable("");

    // pending weekly limit
    this.pendingWeeklyLimit = sb.ko.observable("");

    // pending monthly limit
    this.pendingMonthlyLimit = sb.ko.observable("");

    // current daily limit
    this.currentDailyLimit = sb.ko.observable("");
    this.currentDailyLimitDisplay = sb.ko.computed(function () {
        var amount = this.currentDailyLimit();
        return amount === "1000000" ? "No Limit" : amount;
    }, this);

    // current weekly limit
    this.currentWeeklyLimit = sb.ko.observable("");
    this.currentWeeklyLimitDisplay = sb.ko.computed(function () {
        var amount = this.currentWeeklyLimit();
        return amount === "1000000" ? "No Limit" : amount;
    }, this);

    // current monthly limit
    this.currentMonthlyLimit = sb.ko.observable("");
    this.currentMonthlyLimitDisplay = sb.ko.computed(function () {
        var amount = this.currentMonthlyLimit();
        return amount === "1000000" ? "No Limit" : amount;
    }, this);

    // flag if true indicates that the request can be confirmed
    this.pendingConfirmation = sb.ko.observable("");

    // Message to show to player on pending screen
    this.depositLimitMsg = sb.ko.observable("");

    // limits set by player to be displayed on responsible gaming screen
    this.displayLimits = {

        daily: sb.ko.computed(function () {

            var amount = this.freeDailyLimitAmount() || this.selectedDailyLimit();
            if (amount === "No Limit" || amount == 1000000) { // needs non strict comparison
                return "You have chosen no limit for daily deposits";
            }

            return "Only allow me to make daily deposits up to " + this.currencySymbol + amount;

        }, this),

        weekly: sb.ko.computed(function () {

            var amount = this.freeWeeklyLimitAmount() || this.selectedWeeklyLimit();
            if (amount === "No Limit"  || amount == 1000000) { // needs non strict comparison
                return "You have chosen no limit for weekly deposits";
            }

            return "Only allow me to make weekly deposits up to " + this.currencySymbol + amount;

        }, this),

        monthly: sb.ko.computed(function () {

            var amount = this.freeMonthlyLimitAmount() || this.selectedMonthlyLimit();
            if (amount === "No Limit" || amount == 1000000) { // needs non strict comparison
                return "You have chosen no limit for monthly deposits";
            }

            return "Only allow me to make monthly deposits up to " + this.currencySymbol + amount;

        }, this)
    };

    // visibility for the deposit limits link on the footer
    this.showDepositLimitsLink = sb.ko.observable(false);

    // holds the current values for the limits
    this.limitBeforeEditing = 0;

    // holds the edit mode on the limit setup
    this.editMode = sb.ko.observable("");

    // bypass disclaimer and go straight to limit setup screen
    this.bypassDisclaimer = false;

    // period that has been reached which is shown on the warning
    this.limitWarningPeriod = "";

    /**
     * Module initialization
     */

    this.init = function () {
        sb.listen("got-limit-values", this.onGotLimitValues);
        sb.listen("request-deposit-limit-change-finished", this.onRequestLimitChangeFinished);
        sb.listen("canceled-deposit-limits", this.onCanceledDepositLimits);
        sb.listen("reset-deposit-limits-finished", this.onResetLimitsFinished);
        sb.listen("set-deposit-limits-finished", this.onSetDepositLimitsFinished);
    };

    /**
     * Module destruction
     */

    this.destroy = function () {
        sb.ignore("got-limit-values");
        sb.ignore("request-deposit-limit-change-finished");
        sb.ignore("canceled-deposit-limits");
        sb.ignore("reset-deposit-limits-finished");
        sb.ignore("set-deposit-limits-finished");
    };

    /**
     * Events received
     */

    // Received response from backend for the get player limits
    this.onGotLimitValues = function (e, data) {

        // reset any limits that were selected before

        this.limitsToSave.daily(0);
        this.limitsToSave.weekly(0);
        this.limitsToSave.monthly(0);


        this.modified(false);
        this.pendingConfirmation(false);

        if (data.Status === "1") {

            // status
            this.depositLimitStatus = data.DepositLimitStatus;

            // limit locks

            this.lockedLimits.active = data.DepositLimitLock === "1" ? true : false;
            this.lockedLimits.perDay = parseInt(data.LockedDepositLimitPerDay, 10);
            this.lockedLimits.perWeek = parseInt(data.LockedDepositLimitPerWeek, 10);
            this.lockedLimits.perMonth = parseInt(data.LockedDepositLimitPerMonth, 10);

            // if limits are locked, the player cannot remove the limits
            this.canRemoveLimits(!this.lockedLimits.active);

            // get deposit limits after a request
            // on 1st limit change request the depositLimitStatus = 0
            // should still go to deposit screen
            if (data.action === "change-request" && this.depositLimitStatus === "0") {
                sb.notify("open-deposit-screen");
                return;
            }

            // normal get limits when it's only to go to the deposit limits screen
            if (this.depositLimitStatus === "0" || this.depositLimitStatus === "1") {

                // OK to set limits

                // daily values

                this.dailyLimits(this.prepareValues("daily", data.DayValues.split(",")));
                this.selectedDailyLimit(data.DepositLimitPerDay);
                this.freeDailyLimitAmount(data.DepositLimitPerDay);
                this.dailyLimitSetBy(data.DepositLimitPerDay === "1000000" ? "" : data.DepositLimitDaySetBy);

                // weekly values

                this.weeklyLimits(this.prepareValues("weekly", data.WeekValues.split(",")));
                this.selectedWeeklyLimit(data.DepositLimitPerWeek);
                this.freeWeeklyLimitAmount(data.DepositLimitPerWeek);
                this.weeklyLimitSetBy(data.DepositLimitPerWeek === "1000000" ? "" : data.DepositLimitWeekSetBy);

                // monthly values

                this.monthlyLimits(this.prepareValues("monthly", data.MonthValues.split(",")));
                this.selectedMonthlyLimit(data.DepositLimitPerMonth);
                this.freeMonthlyLimitAmount(data.DepositLimitPerMonth);
                this.monthlyLimitSetBy(data.DepositLimitPerMonth === "1000000" ? "" : data.DepositLimitMonthSetBy);

                if (this.bypassDisclaimer) {
                    this.editLimit(this.limitWarningPeriod);
                }

            } else {

                // request pending

                // set by

                this.dailyLimitSetBy(data.DepositLimitDaySetBy);
                this.weeklyLimitSetBy(data.DepositLimitWeekSetBy);
                this.monthlyLimitSetBy(data.DepositLimitMonthSetBy);

                // set pending values

                if (data.PendingDepositLimitPerDay === "1000000") data.PendingDepositLimitPerDay = "No Limit";
                if (data.PendingDepositLimitPerWeek === "1000000") data.PendingDepositLimitPerWeek = "No Limit";
                if (data.PendingDepositLimitPerMonth === "1000000") data.PendingDepositLimitPerMonth = "No Limit";

                this.pendingDailyLimit(data.PendingDepositLimitPerDay);
                this.pendingWeeklyLimit(data.PendingDepositLimitPerWeek);
                this.pendingMonthlyLimit(data.PendingDepositLimitPerMonth);

                // set current values

                this.currentDailyLimit(data.DepositLimitPerDay);
                this.currentWeeklyLimit(data.DepositLimitPerWeek);
                this.currentMonthlyLimit(data.DepositLimitPerMonth);

                // message
                this.depositLimitMsg(data.DepositLimitMessage);

                if (this.depositLimitStatus === "3") {
                    // request pending confirmation
                    this.pendingConfirmation(true);
                }

                // action
                this.currentAction("pending");
            }

        } else {
            this.handleError(data.StatusMsg);
        }

    }.bind(this);

    /**
     * prepare values on the right format to be displayed with KO options binding
     * @param period (daily/weekly/monthly)
     * @param valuesArray
     * @returns {Array} the amounts
     */
    this.prepareValues = function (period, valuesArray) {

        var i,
            amount,
            display,
            values = [];

        // setting the limits on the list
        for (i = 0; i < valuesArray.length; i++) {

            amount = parseInt(valuesArray[i], 10);

          //  if (period === "daily" && amount <= 20) break;
           // if (period === "weekly" && amount <= 50) break;

            display = amount === 1000000 ? this.NOLIMIT : this.currencySymbol + amount;

            values.push({
                amount: amount,
                amountToDisplay: display
            });
        }

        return values;
    };

    /**
     * Disclaimer screen
     */

    // Opens disclaimer screen
    this.openDisclaimer = function () {
        this.currentAction("disclaimer");
    };

    // Player clicked on proceed on the disclaimer screen
    this.proceedDisclaimer = function () {

        // if nothing pending
        if (this.depositLimitStatus === "0" || this.depositLimitStatus === "1") {
            this.openResponsible();
        }

    }.bind(this);

    /**
     * Responsible gaming screen
     */

    // opens responsible gaming screen
    this.openResponsible = function () {
        this.currentAction("responsible");
    }.bind(this);

    /**
     * Cancels the setup screen
     * Opens the previous screen
     */
    this.cancelSetup = function () {

        // set back the limit that was being editing
        if (this.editMode() === "edit") {

            var p = this.selectedPeriod();

            if (p === "daily") this.selectedDailyLimit(this.limitBeforeEditing);
            if (p === "weekly") this.selectedWeeklyLimit(this.limitBeforeEditing);
            if (p === "monthly") this.selectedMonthlyLimit(this.limitBeforeEditing);
        }

        // go to responsible gaming
        this.openResponsible();

    }.bind(this);


    // requests a limit change
    this.requestLimitChange = function () {

        var daily,
            weekly,
            monthly;

        daily = this.limitsToSave.daily();
        weekly = this.limitsToSave.weekly();
        monthly = this.limitsToSave.monthly();

        // transform No Limit in -1

        if (daily === this.NOLIMIT) {
            daily = -1;
        }

        if (weekly === this.NOLIMIT) {
            weekly = -1;
        }

        if (monthly === this.NOLIMIT) {
            monthly = -1;
        }

        sb.notify("request-deposit-limit-change", {
            daily: daily,
            weekly: weekly,
            monthly: monthly,
            password: this.password()
        });

    }.bind(this);

    /**
     * Limits setup screen
     */

    // opens the setup screen
    this.openLimitsSetup = function (period, action) {

        // clear form

        this.freeAmountEntered("");
        this.invalidAmount("");
        if (this.allowOnlyMonthlyLimit()) {
            this.selectedPeriod("monthly");
        } else {
            this.selectedPeriod(period);
        }

        // hold the current limit and set it to be the highest one

        if (period === "daily") {
            this.limitBeforeEditing = this.selectedDailyLimit();
            this.selectedDailyLimit(this.dailyLimits()[1].amount);
        }
        if (period === "weekly") {
            this.limitBeforeEditing = this.selectedWeeklyLimit();
            this.selectedWeeklyLimit(this.weeklyLimits()[1].amount);
        }
        if (period === "monthly") {
            this.limitBeforeEditing = this.selectedMonthlyLimit();
            this.selectedMonthlyLimit(this.monthlyLimits()[1].amount);
        }

        // set the editing mode
        var mode = action === "edit" ? "edit" : "new"
        this.editMode(mode);

        // set action
        this.currentAction("setup");

    }.bind(this);

    // check if limit is valid
    // if limit is valid should save it temporarily and prompt the password
    this.validatesLimit = function () {

        var freeAmount = this.freeAmountEntered();
        var selectedPeriod = this.selectedPeriod();

        if (freeAmount) {

            if (isNaN(freeAmount)) {
                this.invalidAmount(sb.i18n("depositLimitsInvalid"));
                return false;
            }

            freeAmount = parseFloat(freeAmount);

            if (freeAmount % 1 > 0) {
                this.invalidAmount("Sorry, decimal values are not valid.");
                return false;
            }

            if (freeAmount < 5) {
                this.invalidAmount("Sorry, the minimum amount is " +  sb.getFromStorage("currencySymbol") + "5.");
                return false;
            }

            if (freeAmount > 1000000) {
                this.invalidAmount("Sorry, the maximum amount is " +  sb.getFromStorage("currencySymbol") + "1,000,000.");
                return false;
            }

            // checking locked limits
            if (this.lockedLimits.active) {

                var max;

                if (selectedPeriod === "daily") max = this.lockedLimits.perDay;
                else if (selectedPeriod === "weekly") max = this.lockedLimits.perWeek;
                else max = this.lockedLimits.perMonth;

                if (freeAmount > max) {
                    this.invalidAmount("Sorry, the maximum amount is " +  sb.getFromStorage("currencySymbol") + max + ". Please contact our Customer Service team.");
                    return false;
                }
            }

        }

        if (selectedPeriod === "daily") {

            if (freeAmount) {
                this.freeDailyLimitAmount(freeAmount);
                this.limitsToSave.daily(freeAmount);
            } else {
                this.freeDailyLimitAmount("");
                this.limitsToSave.daily(this.selectedDailyLimit());
            }

        } else if (selectedPeriod === "weekly") {

            if (freeAmount) {
                this.freeWeeklyLimitAmount(freeAmount);
                this.limitsToSave.weekly(freeAmount);
            } else {
                this.freeWeeklyLimitAmount("");
                this.limitsToSave.weekly(this.selectedWeeklyLimit());
            }

        } else {

            if (freeAmount) {
                this.freeMonthlyLimitAmount(freeAmount);
                this.limitsToSave.monthly(freeAmount);
            } else {
                this.freeMonthlyLimitAmount("");
                this.limitsToSave.monthly(this.selectedMonthlyLimit());
            }
        }

        // validates that daily <= weekly <= monthly

        var dailyAmt,
            weeklyAmt,
            monthlyAmt;

        // get saved limits
        dailyAmt = this.limitsToSave.daily();
        weeklyAmt = this.limitsToSave.weekly();
        monthlyAmt = this.limitsToSave.monthly();

        // check if they were set as no limit - add a big amount but keep daily < weekly < monthly

        if (dailyAmt === this.NOLIMIT) {
            dailyAmt = 9999999;
        }
        if (weeklyAmt === this.NOLIMIT) {
            weeklyAmt = 99999999;
        }
        if (monthlyAmt === this.NOLIMIT) {
            monthlyAmt = 999999999;
        }

        // check if they were not changed - get current amounts then

        if (dailyAmt === 0) {
            if (this.dailyLimitSetBy() !== "") {
                dailyAmt = this.freeDailyLimitAmount();
            }
        }
        if (weeklyAmt === 0) {
            if (this.weeklyLimitSetBy() !== "") {
                weeklyAmt = this.freeWeeklyLimitAmount();
            }
        }
        if (monthlyAmt === 0) {
            if (this.monthlyLimitSetBy() !== "") {
                monthlyAmt = this.freeMonthlyLimitAmount();
            }
        }

        // validation

        dailyAmt = +dailyAmt;
        weeklyAmt = +weeklyAmt;
        monthlyAmt = +monthlyAmt;
        
        var invalid = "",
            useShortcut = false; // only used for increases!

        if (dailyAmt > 0) {
            if (weeklyAmt > 0 && dailyAmt > weeklyAmt) {
                invalid = "Daily limit needs to be lower than weekly limit.";

                if (selectedPeriod === "daily") {
                    useShortcut = true;
                }
            }
            if (monthlyAmt > 0 && dailyAmt > monthlyAmt) {
                invalid = "Daily limit needs to be lower than monthly limit.";
                if (selectedPeriod === "daily") {
                    useShortcut = true;
                }
            }
        }

        if (weeklyAmt > 0) {
            if (monthlyAmt > 0 && weeklyAmt > monthlyAmt) {
                invalid = "Weekly limit needs to be lower than monthly limit.";
                if (selectedPeriod === "weekly") {
                    useShortcut = true;
                }
            }
        }

        if (invalid) {

            // show a confirmation dialog to override the next limit(s)
            if (useShortcut) {

                // on confirmation
                var confirmed = function () {

                    // set the higher limits to match the same amount

                    if (this.selectedPeriod() === "daily") {

                        if (this.weeklyLimitSetBy() !== "") {

                            this.limitsToSave.weekly(dailyAmt);
                            this.freeWeeklyLimitAmount(dailyAmt);
                        }

                        if (this.monthlyLimitSetBy() !== "") {

                            this.limitsToSave.monthly(dailyAmt);
                            this.freeMonthlyLimitAmount(dailyAmt);
                        }

                    } else if (this.selectedPeriod() === "weekly") {

                        this.limitsToSave.monthly(weeklyAmt);
                        this.freeMonthlyLimitAmount(weeklyAmt);
                    }

                    // ok to next screen - enter password

                    this.modified(true);
                    this.enterPassword();
                    return true;

                }.bind(this);

                // on cancellation
                var canceled = function () {

                    this.invalidAmount(invalid);
                    return false;

                }.bind(this);

                // confirmation dialog
                sb.showConfirm(sb.i18n("confirmIncreaseNextLimits"), confirmed, canceled);

            }

            this.invalidAmount(invalid);
            return false;

        }

        // Limits are valid

        this.modified(true);
        this.enterPassword();

    }.bind(this);

    /**
     * Password screen
     */

    // prompts the password
    this.enterPassword = function () {

        // clear password

        this.password("");
        this.passwordError("");

        // set action
        this.currentAction("password");

    }.bind(this);

    // check if password is valid and return to responsible gaming
    this.validatesPassword = function () {

        if (!this.password()) {
            this.passwordError("Please enter a valid password.");
        } else {

            // valid password go back to gambling screen
            this.openResponsible();
        }

    }.bind(this);

    this.onRequestLimitChangeFinished = function (e, data) {

        if (data.Status === "1") {

            // hide deposit limits link right away
            this.showDepositLimitsLink(false);

            // refresh screen
            sb.notify("get-limit-values", ["change-request"]);

            sb.showSuccess(sb.i18n("requestChangeFinished"));

        } else {
            this.handleError(data.StatusMsg);
        }

    }.bind(this);

    this.handleError = function (msg) {
        if (msg) {
            sb.showError(msg);
        } else {
            sb.showError("Sorry there is a system error, if the error persists contact support");
        }
    };

    /**
     * Pending Screen
     */

    // player opted for confirming the pending request
    this.setPlayerLimits = function () {

        // player confirmation
        sb.notify("set-deposit-limits");

    }.bind(this);

    // player opted for canceling the pending request
    this.cancelRequest = function () {
        sb.notify("cancel-deposit-limits");
    }.bind(this);

    // AJAX CancelDepositLimits has finished
    this.onCanceledDepositLimits = function (e, data) {

        if (data.Status === "1") {

            sb.showSuccess("Your request has been canceled successfully");
            sb.notify("open-deposit-screen");

        } else {
            this.handleError(data.StatusMsg);
        }

    }.bind(this);

    // player cancel limits
    // will check the status and prompt the no limits for now screen or deposit screen
    this.cancelLimits = function () {

        if (this.allowOnlyMonthlyLimit()) {

            // set limits as no limit

            this.limitsToSave.daily(-1);
            this.limitsToSave.weekly(-1);
            this.limitsToSave.monthly(-1);
            this.modified(true);

            // action
            this.currentAction("no-limits");

        } else {

            // open the deposit screen
            sb.notify("open-deposit-screen");
        } 

    }.bind(this);

    // player opted for setting "no limits" on all periods
    this.resetLimits = function () {

        sb.notify("reset-deposit-limits");

    }.bind(this);

    // AJAX reset has finished
    this.onResetLimitsFinished = function (e, data) {

        if (data.Status === "1") {

            sb.showSuccess(data.StatusMsg);

            // open deposit screen
            sb.notify("open-deposit-screen", [true, true]);

        } else {
            this.handleError(data.StatusMsg);
        }
    }.bind(this);

    // AJAX SetDepositLimits has finished
    // @param data response object
    this.onSetDepositLimitsFinished = function (e, data) {

        if (data.Status === "1") {

            sb.showSuccess(data.StatusMsg);

            // open deposit screen
            sb.notify("open-deposit-screen", [true, true]);

        } else {
            this.handleError(data.Status, data.StatusMsg, data.OpenTime);
        }
    }.bind(this);

    // player wants to edit a limit
    // the period of the limit - monthly / weekly / daily
    this.editLimit = function (period) {
        this.selectedPeriod(period);
        this.openLimitsSetup(period, "edit");
    }.bind(this);

    // player remove limits
    // will set all 3 limits as No Limit - should save right away
    this.removeLimits = function () {

        // set limits as no limit

        this.limitsToSave.daily(-1);
        this.limitsToSave.weekly(-1);
        this.limitsToSave.monthly(-1);
        this.modified(true);

        // request change
        this.requestLimitChange();

    }.bind(this);
};
