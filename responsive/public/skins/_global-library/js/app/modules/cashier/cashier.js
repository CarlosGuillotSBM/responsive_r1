var sbm = sbm || {};

/**
 * Handles player interation
 * Responsible module for all user input and actions
 * @param sb
 * @param options
 * @constructor
 */
sbm.Cashier = function(sb, options) {
  "use strict";

  // subscriptions
  this.onIdoNotWantBonus = function() {
    if (this.deposit.iDoNotWantBonus()) {
      this.showPanel(this.panels.amounts);
    }
  }.bind(this);

  /**
   * Player navigated to a different screen
   */
  this.activePanelChanged = function(newVal) {
    var panels = this.previousPanels.length;
    // if new panel is different as the previous one
    if (newVal !== this.previousPanels[panels - 2]) {
      // add to array
      this.previousPanels.push(newVal);
      this.currentPanelIndex++;
    } else {
      // we're navigating to previous panel
      // remove the last panel from the array
      this.previousPanels.pop();
      this.currentPanelIndex--;
    }
  }.bind(this);
  this.creditCardIconChanged = function(newVal) {
    this.deposit.methodIcon(newVal);
  }.bind(this);

  // properties
  this.activePanel = sb.ko.observable("");
  this.URUVerified = sb.ko.observable(sb.getCookie("URUVerified"));
  this.previousPanels = [];
  this.currentPanelIndex = -1;
  this.activePanel.subscribe(this.activePanelChanged);
  this.currentAction = sb.ko.observable("");
  this.canMakeDeposit = sb.ko.observable(true);
  this.addingNewMethod = {
    availableMethods: sb.ko.observableArray([]),
    accountType: sb.ko.observable(""),
    card: new sbm.CreditCard(sb, new sbm.CardToken()),
    neteller: new sbm.Neteller(sb),
    paysafe: new sbm.Paysafe(sb)
  };
  this.addingNewMethod.card.creditCardIcon.subscribe(
    this.creditCardIconChanged
  );
  this.customerAccounts = sb.ko.observableArray([]);
  this.deposit = new sbm.Deposit(sb);
  this.deposit.iDoNotWantBonus.subscribe(this.onIdoNotWantBonus);
  this.cashierData = {};
  this.maxAccountsReached = sb.ko.observable(false);
  this.limitReached = sb.ko.observable(false);
  this.panels = {
    accountsList: "ACCOUNTSLIST",
    methods: "METHODS",
    enterDetails: "ENTERDETAILS",
    withdraw: "WITHDRAW",
    reverse: "REVERSE",
    expiry: "EXPIRY",
    retryCVN: "RETRYCVN",
    bankDetails: "BANKDETAILS",
    error: "ERROR",
    amounts: "AMOUNTS",
    promotions: "PROMOTIONS",
    depositLimits: "DEPOSITLIMITS",
    accountVerification: "ACCOUNTVERIFICATION",
    uruDcoumentUpload: "URUDOCUMENTUPLOAD",
    uruPending: "URUPENDINGRESPONSE"
  };
  this.expiryYears = sb.ko.observableArray([]);
  this.dobYears = sb.ko.observableArray([]);
  this.withdrawal = new sbm.Withdrawal(sb);
  this.reversal = new sbm.Reversal(sb);
  this.bankDetails = new sbm.BankDetails(sb);
  this.cardTokenUpdate = new sbm.CardTokenUpdate(sb);
  this.correctCVN = new sbm.RetryCVN(sb);
  this.error = sb.ko.observable({
    code: sb.ko.observable(""),
    message: sb.ko.observable(""),
    openTime: sb.ko.observable("")
  });
  this.forceEnteringCardDetailsAgain = false;
  this.playerLimits = {};
  this.hasDeposited = sb.ko.observable("");
  this.currencySymbol = sb.ko.observable(
    sb.getFromStorage("currencySymbol") || ""
  );
  this.cashierNotAvailable = sb.ko.observable(false);
  this.cardHolder = sb.ko.observable("");
  this.aed = "";
  this.accountVerification = {};
  this.needsToVerifyAccount = sb.ko.observable(false);
  this.showDepositMenu = sb.ko.observable(false);
  this.showReverseMenu = sb.ko.observable(false);
  this.showWithdrawMenu = sb.ko.observable(false);
  this.URUDocumentUpload = {};
  this.accountDescription = sb.ko.observable("");
  this.showPromoCode = sb.ko.observable("No");
  this.backNeeded = sb.ko.observable(true);
  this.bonusSpins = sb.ko.observable(0);
  this.prizeWheelSpins = sb.ko.observable(0);
  this.bingoCards = sb.ko.observable(0);
  this.lostPrizeWheel = sb.ko.observable(0);
  this.lostSpins = sb.ko.observable(0);

  // functions

  /**
   * Player choose to add a new method
   */
  this.AddNewMethod = function() {
    // If not verified
    if (this.URUVerified() != "1") {
      sb.showPleaseWaitBox(
        "Identity Verification",
        "<p>We are required by UK regulations to verify your age and address before you can continue.</p><small>Please bear with us whilst we run this check.</small>"
      );
      sb.notify("age-id-verification-run-uru-check", []);
      this.showWithdrawMenu(false);
      this.showReverseMenu(false);
    } else {
      this.deposit.resetAmounts();
      this.showAvailableMethods();
    }
  }.bind(this);

  /**
   * Do you have promo code? Yes or No option
   */

  this.showPromoCodeBox = function(value) {
    if (value == "Yes") {
      this.showPromoCode(true);
    } else if (value == "No") {
      this.showPromoCode(false);
    }
  }.bind(this);

  /**
   * Player canceled deposit
   */
  this.cancelDeposit = function() {
    if (this.deposit.accountId === 0) {
      this.showPanel(this.panels.methods);
    } else {
      this.showPanel(this.panels.accountsList);
    }
  }.bind(this);

  /**
   * Check player's account
   *
   * @param activeAccounts If player has no accounts, will show available methods to add otherwise will show player's accounts
   * @param maxAccounts Max number of accounts the player can have
   * @param withdrawalPending If there are withdrawals pending will show reverse screen
   * @param forceDeposit
   * @param showDepositLimits
   */
  this.checkAccount = function(
    activeAccounts,
    maxAccounts,
    withdrawalPending,
    showDepositLimits,
    forceDeposit
  ) {
    // if has pending withdrawals we will show the reverse screen

    if (withdrawalPending && !forceDeposit) {
      this.openReverse();
      return;
    }

    // if player needs to set the deposit limits

    if (showDepositLimits === 2 && !forceDeposit) {
      this.openLimitsScreen();
      return;
    }
    // If the player is pending documents
    if (this.URUVerified() == "2") {
      this.showPanel(this.panels.uruDcoumentUpload);
      this.showDepositMenu(false);
      this.needsToVerifyAccount(false);
    }
    // If the player is pending documents
    if (this.URUVerified() == "3") {
      this.showPanel(this.panels.uruPending);
    }
    // show the deposit screen
    if (this.URUVerified() == "0" || this.URUVerified() == "1") {
      this.showPanel(this.panels.methods);
      if (activeAccounts === 0) {
        this.showAvailableMethods();
        this.backNeeded(false);
      } else {
        this.getCustomerAccounts();
        this.backNeeded(true);
      }

      if (activeAccounts === maxAccounts) {
        this.maxAccountsReached(true);
      }
      this.currentAction("deposit");
    }
  }.bind(this);

  // Switch to the URU Document upload screen
  this.switchToURUUpload = function(e, data) {
    this.activePanel(this.panels.uruDcoumentUpload);
    this.needsToVerifyAccount(false);
    sb.notify("hide-notification-dialog");
  }.bind(this);

  this.onURUUploadDoLater = function(e, data) {
    // In case we need to do something in the future
    sb.notify("hide-notification-dialog");
  }.bind(this);

  this.onURUCheckSuccessful = function(e) {
    if (sb.getCookie("URUVerified") == "1") {
      this.showDepositMenu(true);
      this.showWithdrawMenu(true);
      this.showReverseMenu(true);
    }
  }.bind(this);

  /**
   * Check if there are Promo Codes available
   * Will show the input to enter a promo code if any available
   * @param promos The array with the promotions available
   */
  this.checkForPromoCodeAvailability = function(promos) {
    if (!promos) {
      return;
    }

    // remove all promos with promoId -1
    var validPromos = sb._.reject(promos, function(p) {
      return p.PromoID === "-1";
    });

    // check if there was any promoId -1
    if (promos.length > validPromos.length) {
      // set as available
      this.deposit.promoCodeAvailable(true);
    }
    return validPromos;
  };

  /**
   * Module destructor
   */
  this.destroy = function() {
    sb.ignore("cashier-started");
    sb.ignore("cashier-got-account-types");
    sb.ignore("cashier-deposited");
    sb.ignore("cashier-deposit-retried-CVN");
    sb.ignore("cashier-got-customer-accounts");
    sb.ignore("cashier-got-token-update");
    sb.ignore("cashier-got-withdraw-detail");
    sb.ignore("cashier-done-withdrawal");
    sb.ignore("cashier-got-reverse-detail");
    sb.ignore("cashier-done-reverse");
    sb.ignore("cashier-got-new-token");
    sb.ignore("cashier-got-wire-details");
    sb.ignore("cashier-saved-bank-details");
    sb.ignore("cashier-finished-set-new-token-cvn");
    sb.ignore("cashier-navigation");
    sb.ignore("checked-promo-code");
    sb.ignore("open-deposit-screen");
    sb.ignore("got-documents");
    sb.ignore("got-balance");
    sb.ignore("got-player-account");
    this.playerLimits.destroy();
    this.accountVerification.destroy();
  };

  /**
   * Player selected to do a reversal
   * Validates details - if valid requests a reversal
   */
  this.doReversal = function(vm, e) {
    var validation = this.reversal.validation();

    if (validation === "") {
      this.reversal.buttonActive(false);
      sb.notify("cashier-do-reversal", [this.reversal.amountToReverse()]);
    } else {
      sb.showWarning(validation);
    }
  }.bind(this);

  /**
   * Player selected to do a withdrawal
   */
  this.doWithdrawal = function(vm, e) {
    var validation = this.withdrawal.validation();

    if (validation === "") {
      var proceed = true;

      if (
        this.withdrawal.bonusToBeForfeited() > 0 ||
        this.bonusSpins() > 0 ||
        this.prizeWheelSpins() > 0 ||
        this.bingoCards() > 0
      ) {
        var msg =
          "You have active prizes in your account:<br><ul style='margin-top: 0.5rem'>";

        if (this.withdrawal.bonusToBeForfeited() > 0) {
          msg += "<li>" + this.withdrawal.currencySymbol() + " ";
          msg +=
            this.withdrawal.bonusToBeForfeited() + " in bonus funds.</li> ";
        }
        if (this.bonusSpins() > 0) {
          msg += "<li>" + this.bonusSpins() + " Free Spins</li>";
        }
        if (this.prizeWheelSpins() > 0) {
          msg += "<li>" + this.prizeWheelSpins() + " Prize Wheels</li>";
        }
        if (this.bingoCards() > 0) {
          msg += "<li>" + this.bingoCards() + " Bingo Cards</li>";
        }
        msg +=
          "</ul>Some or all of your prizes may be removed if you submit a withdrawal.";

        var onCancel = function() {
          sb.notify("hide-notification-dialog");
        };
        sb.showConfirm(
          msg,
          this.askForConfirmationWithdrawal,
          onCancel,
          "Please confirm",
          false
        );
      } else {
        this.confirmedWithdrawal();
      }
    } else {
      sb.showWarning(validation);
    }
  }.bind(this);

  this.askForConfirmationWithdrawal = function() {
    if (
      this.withdrawal.bonusToBeForfeited() > 0 ||
      this.bonusSpins() > 0 ||
      this.prizeWheelSpins() > 0 ||
      this.bingoCards() > 0
    ) {
      var msg =
        "If you request a withdrawal now you will forfeit the following prizes:<br><ul style='margin-top: 0.5rem'>";

      if (this.withdrawal.bonusToBeForfeited() > 0) {
        msg += "<li>" + this.withdrawal.currencySymbol() + " ";
        msg += this.withdrawal.bonusToBeForfeited() + " in bonus funds.</li> ";
      }
      if (this.bonusSpins() > 0) {
        msg += "<li>" + this.bonusSpins() + " Free Spins</li>";
      }
      if (this.prizeWheelSpins() > 0) {
        msg += "<li>" + this.prizeWheelSpins() + " Prize Wheels</li>";
      }
      if (this.bingoCards() > 0) {
        msg += "<li>" + this.bingoCards() + " Bingo Cards</li>";
      }
      msg +=
        "</ul>Reversing a withdrawal will not reinstate any forfeited prizes.";

      var onCancel = function() {};
      sb.showAreYouSure(msg, this.confirmedWithdrawal, onCancel, "Warning");
    } else {
      this.confirmedWithdrawal();
    }
  }.bind(this);

  /**
   * Withdrawal is valid - do the request
   */
  this.confirmedWithdrawal = function() {
    this.withdrawal.buttonActive(false);
    this.lostPrizeWheel(this.prizeWheelSpins());
    this.lostSpins(this.bonusSpins());
    sb.notify("cashier-do-withdrawal", [this.withdrawal.amountToWithdraw()]);
  }.bind(this);

  /**
   * Player selected a method from the list
   * @param accountTypeCode
   * @param depositData
   */
  this.enterDetails = function(accountTypeCode, depositData) {
    this.deposit.depositAmounts(depositData.amounts);
    this.deposit.promos(depositData.promos);
    this.deposit.needsCvn(false);

    if (accountTypeCode === "A_CC") {
      // get new token
      this.getNewToken();

      this.expiryYears(this.getExpiryYears());
      this.addingNewMethod.card.firstName(this.cashierData.firstName);
      this.addingNewMethod.card.lastName(this.cashierData.lastName);
    }

    this.addingNewMethod.accountType(accountTypeCode);
    this.deposit.accountType(accountTypeCode);

    if (accountTypeCode === "A_PAYPAL" || accountTypeCode === "A_PAYSAFECARD") {
      this.showPanel(this.panels.promotions);
    } else {
      this.showPanel(this.panels.enterDetails);
    }
  };

  /**
   * Request player accounts
   */
  this.getCustomerAccounts = function() {
    sb.notify("cashier-get-customer-accounts");
  };

  /**
   * Get the possible expiry years to add to dropdown
   * @returns {Array}
   */
  this.getExpiryYears = function() {
    var today = new Date();
    var currYear = today.getFullYear();
    var years = [];
    var i;
    for (i = 0; i < 8; i++) {
      years.push({ val: currYear + i, desc: currYear + i });
    }
    return years;
  };

  /**
   * Get possible date of birth years to add to dropdown
   * @returns {Array}
   */
  this.getDobYears = function() {
    var currYear = new Date().getFullYear();
    var maxYear = currYear - 18;
    var minYear = currYear - 111;
    var years = [];

    while (maxYear >= minYear) {
      years.push({ val: maxYear, desc: maxYear });

      maxYear -= 1;
    }

    return years;
  };

  /**
   * Get new token for WorldPay
   */
  this.getNewToken = function() {
    sb.notify("cashier-get-new-token", []);
  };

  /**
   * Auxiliar function to get URL parameters
   * @param name
   * @returns {string}
   */
  this.getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  };

  /**
   * Handles errors
   * @param status Error status code
   * @param msg Message to show
   * @param openTime  Cashier's open time
   */
  this.handleError = function(status, msg, openTime) {
    // session expired
    if (status === "403") {
      alert(msg);
      sb.initRoute("/");
    }
    // other errors
    this.error().code(status);
    this.error().message(msg);
    this.error().openTime(openTime || "");

    this.showPanel(this.panels.error);
    sb.hideLoadingAnimation();
  };

  /**
   * Module initialization
   */
  this.init = function() {
    // check session
    // Even though the session is checked in the controller I left this in case this is not the proper way of the framework
    if (!sb.getFromStorage("sessionId") && options.source !== "Qik") {
      window.location.href = "/";
      //This return was causing a conflict with core.ko.applyBindings(self.vmMain, window.document.body); in Router.js
      //return;
    }

    this.cashierData = new sbm.CashierData(sb, options.source);
    this.playerLimits = new sbm.PlayerLimits(sb);
    this.playerLimits.init();

    this.accountVerification = new sbm.AccountVerification(sb);
    this.URUDocumentUpload = new sbm.URUDocumentUpload(sb);
    this.accountVerification.init();
    this.URUDocumentUpload.init();

    // check source and funded status
    if (options.source === "PRD" && sb.getFromStorage("funded") === "true") {
      this.handleError(
        "-2",
        "You are already a funded player but, you can still take advantage of our deposit offers from the <a datahijack='true' href='/cashier/'> Cashier </a> page."
      );
      sb.logError("Funded player cannot access PRD cashier");
      return;
    }

    sb.listen("cashier-started", this.onStarted);
    sb.listen("cashier-got-account-types", this.onGotAccountTypes);
    sb.listen("cashier-deposited", this.onDeposited);
    sb.listen("cashier-deposit-retried-CVN", this.onDepositRetriedCVN);
    sb.listen("cashier-got-customer-accounts", this.onGotCustomerAccounts);
    sb.listen("cashier-got-token-update", this.onGotTokenUpdate);
    sb.listen("cashier-got-withdraw-detail", this.onGotWithdrawDetail);
    sb.listen("cashier-done-withdrawal", this.onDoneWithdraw);
    sb.listen("cashier-got-reverse-detail", this.onGotReverseDetail);
    sb.listen("cashier-done-reverse", this.onDoneReverse);
    sb.listen("cashier-got-new-token", this.onGotNewToken);
    sb.listen("cashier-got-wire-details", this.onGotBankDetails);
    sb.listen("cashier-saved-bank-details", this.onSavedBankDetails);
    sb.listen(
      "cashier-finished-set-new-token-cvn",
      this.onFinishedSetNewTokenCVN
    );
    sb.listen("cashier-navigation", this.onNavigation);
    sb.listen("cashier-switch-to-uru-upload", this.switchToURUUpload);
    sb.listen("cashier-uru-upload-do-later", this.onURUUploadDoLater);
    sb.listen("checked-promo-code", this.checkedPromoCode);
    sb.listen("open-deposit-screen", this.openDeposit);
    sb.listen("cashier-uru-check-successful", this.onURUCheckSuccessful);
    sb.listen("got-balance", this.onGotBalance);
    sb.listen("got-player-account", this.onGotPlayerAccount);

    // [DEV-3394] hack for bingo client
    var bingoclient = this.getParameterByName("bingoclient");
    if (bingoclient) {
      var secureCookie = sbm.serverconfig.ip === "127.0.0.1" ? false : true;
      sb.$.cookie("bingoclient", 1, { path: "/", secure: secureCookie });
    } else {
      sb.$.removeCookie("bingoclient", { path: "/" });
    }

    var status = this.getParameterByName("Status");
    if (status) {
      // if there was an error
      if (parseInt(status, 10) < 0) {
        this.handleError(status, this.getParameterByName("StatusMsg"));
        history.replaceState({}, "Cashier", window.location.pathname);
      } else if (status === "95") {
        history.replaceState({}, "Cashier", window.location.pathname);
        sb.showWarning(sb.i18n("reenterDetails"));

        // start cashier with default account selected
        this.forceEnteringCardDetailsAgain = true;
        this.start();
      } else if (status === "46") {
        this.correctCVN.txDepositId = this.getParameterByName("TxDepositID");
        this.correctCVN.dsid = this.getParameterByName("dsid");
        this.correctCVN.source = this.getParameterByName("Source");
        this.correctCVN.skinId = sbm.serverconfig.skinId;
        this.correctCVN.ip = sbm.serverconfig.ip;
        this.correctCVN.playerId = this.getParameterByName("PlayerID");
        this.correctCVN.ssid = this.getParameterByName("ssid");
        this.correctCVN.device = this.getParameterByName("Device");
        this.correctCVN.customerId = this.getParameterByName("CustomerID");

        this.activePanel(this.panels.retryCVN);
        sb.showWarning(sb.i18n("invalidCvnRetry"));
        history.replaceState({}, "Cashier", window.location.pathname);
      } else {
        // success - check if is a NewTokenUpdate response

        var expiryDateUpdated =
          this.getParameterByName("Function") === "NewTokenUpdate";
        history.replaceState({}, "Cashier", window.location.pathname);
        if (expiryDateUpdated && status) {
          sb.showSuccess(sb.i18n("expiryDateChanged"));
        }

        // start cashier
        this.start();
      }
    } else {
      this.start();
    }

    // source of funds
    var verifyAccount = !!sb.getFromStorage("verifyAccount");
    if (verifyAccount) {
      this.needsToVerifyAccount(true);
      this.currentAction("account-verification");
    }
  }.bind(this);

  /**
   * Player choose to make a deposit
   * Validates the deposit details
   */
  this.makeDeposit = function(obj, e) {
    // old sites with paypal
    if (window.globalOldSkin && this.deposit.accountType() === "A_PAYPAL") {
      // if mobile or desktop welcome page
      if (
        parseInt(window.globalDevice, 10) > 1 ||
        (parseInt(window.globalDevice, 10) === 1 &&
          window.top.location.pathname.indexOf("welcome") > -1)
      ) {
        this.paypalWindow = window.open("", "_blank");
        this.paypalWindow.document.write(
          "Checkout with PayPal. Please don't close the window..."
        );
      }
    }
    // valid amount?
    if (!this.deposit.amount()) {
      sb.showWarning(sb.i18n("invalidAmount"));
      return;
    }
    // valid CVN?
    if (this.deposit.needsCvn()) {
      var cvn = this.deposit.cvn();
      var valid = !isNaN(cvn) && cvn.length === 3;

      if (!valid) {
        sb.showWarning(sb.i18n("invalidCvn"));
        return;
      }
    }
    // if there are promos available and player did not select I do not want bonus
    if (
      this.deposit.promos().length &&
      (!this.deposit.iDoNotWantBonus() && !this.deposit.iHavePromoCode)
    ) {
      sb.showWarning(sb.i18n("invalidPromotion"));
      return;
    }
    if (!this.deposit.validPromoCode()) {
      sb.showWarning(sb.i18n("invalidPromoCode"));
      return;
    }
    // if neteller secure Id is required and valid
    if (
      this.deposit.showNetellerSecureId() &&
      (isNaN(this.deposit.netellerSecureId()) ||
        this.deposit.netellerSecureId().length !== 6)
    ) {
      sb.showWarning(sb.i18n("invalidNetellerSecureId"));
      return;
    }

    if (this.deposit.showAcknowledged() && !this.deposit.acknowledged()) {
      sb.showWarning(sb.i18n("acknowledgeFirst"));
      return;
    }

    this.canMakeDeposit(false);

    if (!this.deposit.accountId && this.deposit.accountType() === "A_CC") {
      sb.notify("cashier-set-new-token-cvn", [
        this.addingNewMethod.card,
        this.deposit,
        this.aed
      ]);
    } else {
      sb.notify("cashier-deposit", [this.addingNewMethod, this.deposit]);
    }
  }.bind(this);

  /**
   * Deposit request has finished
   * Handles response
   */
  this.onDeposited = function(e, data) {
    if (data.Status === "1") {
      sb.sendVariableToGoogleManager("DepositFail", false);
      sb.setCookie("DepositAmount", data.Amount);
      sb.setCookie("PromoCode", this.deposit.promoCode());
      sb.sendVariableToGoogleManager("PromoCode", this.deposit.promoCode());
      this.handleDepositSuccess(data);
    } else if (data.Status === "-3") {
      if (data.Function === "Deposit") {
        this.canMakeDeposit(true);
      }

      sb.showError(data.StatusMsg);
    } else if (data.Status === "46") {
      this.correctCVN.txDepositId = data.TxDepositID;
      this.correctCVN.dsid = data.dsid;
      this.activePanel(this.panels.retryCVN);
      sb.showWarning(sb.i18n("invalidCvnRetry"));
    } else if (data.Status === "72") {
      // old sites
      if (
        window.globalOldSkin &&
        parseInt(window.globalDevice, 10) === 1 &&
        window.top.location.pathname.indexOf("welcome") > -1
      ) {
        if (this.paypalWindow) {
          this.paypalWindow.location.href = data.PaypalURL;
          this.paypalWindow = null;
        } else {
          window.open(data.PaypalURL, "_blank");
        }

        window.top.location.pathname = "/";
      } else if (
        window.globalOldSkin &&
        parseInt(window.globalDevice, 10) > 1
      ) {
        if (this.paypalWindow) {
          this.paypalWindow.location.href = data.PaypalURL;
          this.paypalWindow = null;
        } else {
          window.open(data.PaypalURL, "_blank");
        }

        window.top.location.pathname = "/mobile/";
      } else {
        window.location.href = data.PaypalURL;
      }
    } else if (data.Status === "79") {
      window.location.href = data.PaySafeCardURL;
    } else if (data.Status === "95") {
      sb.hideLoadingAnimation();
      this.forceEnteringCardDetailsAgain = true;
      // reset the previous selected account
      this.deposit.accountId = "";
      this.deposit.showNetellerSecureId(false);
      this.AddNewMethod();
    } else {
      sb.sendVariableToGoogleManager("DepositFail", true);
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Retry deposit request has finished
   * Handles response
   */
  this.onDepositRetriedCVN = function(e, data) {
    if (data.Status === "1") {
      this.handleDepositSuccess(data);
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Handles a successful response on any deposit made (deposit and retry cvn)
   * @param data Data received from deposit response
   */
  this.handleDepositSuccess = function(data) {
    var urlToPost = "";

    if (data.FirstDeposit === "1") {
      sb.notify("cashier-first-deposit");
      urlToPost = "/cashier/ftd-thank-you/";
    } else if (data.DepositSuccessCount && data.DepositSuccessCount === "2") {
      urlToPost = "/cashier/deposit-2-thank-you/";
    } else if (data.DepositSuccessCount && data.DepositSuccessCount === "3") {
      urlToPost = "/cashier/deposit-3-thank-you/";
    } else {
      urlToPost = "/cashier/thank-you/";
    }
    sb.sendVariableToGoogleManager("DepositCount", data.DepositSuccessCount);
    this.postToUrl(urlToPost, {
      Amount: data.Amount,
      Bonus: data.Bonus,
      FreeSpins: data.FreeSpins,
      FreeCards: data.FreeCards,
      Balance: data.Balance,
      DepositSuccessCount: data.DepositSuccessCount,
      ThreeRadPrize1Name: data.ThreeRadPrize1Name,
      ThreeRadPrize1Amount: data.ThreeRadPrize1Amount,
      ThreeRadPrize2Name: data.ThreeRadPrize2Name,
      ThreeRadPrize2Amount: data.ThreeRadPrize2Amount,
      FreeGamesPrize1Name: data.FreeGamesPrize1Name,
      FreeGamesPrize1Amount: data.FreeGamesPrize1Amount,
      FreeGamesPrize2Name: data.FreeGamesPrize2Name,
      FreeGamesPrize2Amount: data.FreeGamesPrize2Amount
    });
  };

  /**
   * Reversal request has finished
   * Handles response
   */
  this.onDoneReverse = function(e, data) {
    if (data.Status === "1") {
      this.reversal.txReverseBatchID(data.TxReverseBatchID);
      this.reversal.amountToReverse(data.Amount);
      this.reversal.balance(parseFloat(data.Balance).toFixed(2));
      this.reversal.withdrawalsPending(
        parseFloat(data.WithdrawalsPending).toFixed(2)
      );
      this.reversal.minWithdraw(parseFloat(data.MinWithdraw).toFixed(2));
      this.reversal.minWithdrawFee(parseFloat(data.MinWithdrawFee).toFixed(2));

      this.reversal.operation(this.reversal.operations.resume);
    } else {
      if (data.Status === "-3") {
        this.reversal.operation(this.reversal.operations.retry);
        this.reversal.error(data.StatusMsg);
      } else {
        this.handleError(data.Status, data.StatusMsg, data.OpenTime);
      }
    }
  }.bind(this);

  /**
   * Withdrawal request has finished
   * Handles response
   */
  this.onDoneWithdraw = function(e, data) {
    if (data.Status === "1") {
      this.withdrawal.txWithdrawID(data.TxWithdrawID);
      this.withdrawal.amountToWithdraw(data.Amount);
      this.withdrawal.balance(parseFloat(data.Balance).toFixed(2));

      this.withdrawal.bonusToBeForfeited(
        parseFloat(data.BonusForfeited).toFixed(2)
      );
      this.withdrawal.withdrawalsPending(
        parseFloat(data.WithdrawalsPending).toFixed(2)
      );
      this.withdrawal.withdrawHoursPending(data.WithdrawHoursPending);
      this.withdrawal.minWithdraw(parseFloat(data.MinWithdraw).toFixed(2));

      this.withdrawal.operation(this.withdrawal.operations.resume);

      sb.sendVariableToGoogleManager("Withdraw", true);
      sb.notify("prize-wheel-check-for-prizes");
    } else {
      if (data.Status === "-3") {
        this.withdrawal.operation(this.withdrawal.operations.retry);
        this.withdrawal.error(data.StatusMsg);
      } else {
        this.handleError(data.Status, data.StatusMsg, data.OpenTime);
      }
    }
  }.bind(this);

  /**
   * Request for a new token has finished
   * Handles response
   */
  this.onFinishedSetNewTokenCVN = function(e, data) {
    if (data.Status === "1") {
      this.postToUrl(this.addingNewMethod.card.token.url1, {
        Action: "Add",
        OTT: this.addingNewMethod.card.token.token1,
        AcctName:
          this.addingNewMethod.card.firstName() +
          " " +
          this.addingNewMethod.card.lastName(),
        AcctNumber: this.addingNewMethod.card.number(),
        ExpDate:
          this.addingNewMethod.card.expiryMonth() +
          this.addingNewMethod.card.expiryYear()
      });
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Get account types available request has finished
   * Handles response
   */
  this.onGotAccountTypes = function(e, data) {
    if (data.Status === "1") {
      // set the image path for each account
      var i = 0,
        account;
      for (i; i < data.AccountTypes.length; i++) {
        account = data.AccountTypes[i];
        if (account.AccountTypeCode === "A_CC") {
          account.icon = "newmethod-cards.svg";
          account.description = "Credit Card";
        } else if (account.AccountTypeCode === "A_PAYPAL") {
          account.icon = "paypal-icon.svg";
          account.description = "PayPal";
        } else if (account.AccountTypeCode === "A_NETELLER") {
          account.icon = "neteller-icon.svg";
          account.description = "Neteller";
        } else if (account.AccountTypeCode === "A_PAYSAFECARD") {
          account.icon = "paysafe-icon.svg";
          account.description = "PaySafe";
        } else {
          account.icon = "lock-circle-green.svg";
          account.description = "Method Not Available";
        }
      }

      this.addingNewMethod.availableMethods(data.AccountTypes);

      // if tokenization has failed - force entering cc details again
      if (this.forceEnteringCardDetailsAgain) {
        this.forceEnteringCardDetailsAgain = false;

        // show warning
        sb.showWarning(sb.i18n("reenterDetails"));

        // get method details
        var creditCardMethod = sb._.find(data.AccountTypes, function(account) {
          return account.AccountTypeCode === "A_CC";
        });

        // show enter details screen
        if (creditCardMethod) {
          this.selectedNewMethod(creditCardMethod);
        }
      } else {
        // show normal available methods screen
        this.showPanel(this.panels.methods);
      }
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Get bank details request has finished
   * Handles request
   */
  this.onGotBankDetails = function(e, data) {
    if (data.Status === "1") {
      this.bankDetails.bankName(data.BankName);
      this.bankDetails.accountName(data.AccountName);
      this.bankDetails.accountNumber(data.AccountNumber);
      this.bankDetails.branchCode(data.BranchCode);
      this.bankDetails.swiftCode(data.SwiftCode);
      this.bankDetails.address(data.Address);
      this.bankDetails.city(data.City);
      this.bankDetails.state(data.State);
      this.bankDetails.country(data.Country);
      this.bankDetails.postCode(data.PostCode);
      this.bankDetails.correspondingBankName(data.CorrespondingBankName);
      this.bankDetails.wireStatus(data.WireStatus);

      this.showPanel(this.panels.bankDetails);
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Get customer accounts request has finished
   * Handles response
   */
  this.onGotCustomerAccounts = function(e, data) {
    if (data.Status === "1") {
      sb._.each(
        data.Accounts,
        function(account) {
          account.listDescription = account.Description;

          // swap different types of credit/debit cards into A_CC
          if (this.isACC(account.AccountTypeCode)) {
            account.listDescription +=
              account.AccountNumber + " Expires " + account.ExpiryDate;
          }
          if (account.LimitReached !== "") {
            var period = "";

            if (account.LimitReached === "d") period = "DAILY";
            else if (account.LimitReached === "w") period = "WEEKLY";
            else period = "MONTHLY";

            //account.listDescription += " " + period + " LIMIT REACHED";
          }

          // set icons
          if (account.AccountTypeCode === "A_PAYPAL") {
            account.icon = "paypal-icon.svg";
          } else if (account.AccountTypeCode === "A_NETELLER") {
            account.icon = "neteller-icon.svg";
          } else if (account.AccountTypeCode === "A_PAYSAFECARD") {
            account.icon = "paysafe-icon.svg";
          } else {
            switch (account.AccountTypeCode) {
              //VISA
              case "A_VISA_CREDIT":
                account.icon = "visa-icon.svg";
                break;
              case "A_VISA_DEBIT":
                account.icon = "visa-icon.svg";
                break;
              case "A_ELECTRON":
                account.icon = "visa-icon.svg";
                break;
              //Mastercard
              case "A_MASTERCARD_CREDIT":
                account.icon = "mastercard-icon.svg";
                break;
              case "A_MASTERCARD_DEBIT":
                account.icon = "mastercard-icon.svg";
                break;
              //Maestro
              case "A_MAESTRO":
                account.icon = "maestro-icon.svg";
                break;
              default:
                account.icon = "card-icon.png";
            }
          }
        }.bind(this)
      );

      this.customerAccounts(data.Accounts);
      this.showPanel(this.panels.accountsList);

      // reset bonus option
      this.deposit.iDoNotWantBonus(false);

      this.deposit.showAcknowledged(!!data.UKGCAck);
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Get new WorldPay token request has finished
   * Handles response
   */
  this.onGotNewToken = function(e, data) {
    if (data.Status === "1") {
      this.addingNewMethod.card.token.cardHolder = data.CardHolder;
      this.addingNewMethod.card.token.token1 = data.Token1;
      this.addingNewMethod.card.token.txAccountTokenId = data.TxAccountTokenID;
      this.addingNewMethod.card.token.url1 = data.URL1;
      this.cardHolder(data.CardHolder);
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Encrypts the Barclaycard form
   * if configured to use it
   *
   */
  this.encryptBarclaycardForm = function() {
    if (!sbm.serverconfig.adyen) {
      return false;
    }

    // the form element to encrypt
    var $adyenform = sb.$("#adyenform");

    // if form was not founded - exit
    if (!$adyenform.length) return;

    // the form will be encrypted before it is submitted
    // adyen is defined by the adyen script that is included on the page
    window.adyen.createEncryptedForm($adyenform[0], {});

    // get aed value
    // prevent form submission which would cause the page navigation
    $adyenform.on(
      "submit",
      function(e) {
        sb.logInfo("ayden submitted");
        this.aed = sb.$("#adyen-encrypted-data").val();
        e.preventDefault();
        return false;
      }.bind(this)
    );

    // force submitting the form by clicking on submit button defined on the form
    sb.$("#btnAdyenformSubmit").click();
  };

  /**
   * Get reverse detail request has finished
   * Handles response
   */
  this.onGotReverseDetail = function(e, data) {
    if (data.Status === "1") {
      this.reversal.availableToReverse(
        parseFloat(data.AvailableToReverse).toFixed(2)
      );
      this.reversal.minReverse(parseFloat(data.MinReverse).toFixed(2));
      this.reversal.minWithdraw(parseFloat(data.MinWithdraw).toFixed(2));
      this.reversal.minWithdrawFee(parseFloat(data.MinWithdrawFee).toFixed(2));

      this.reversal.operation(this.reversal.operations.reverse);
      this.reversal.buttonActive(true);
      this.showPanel(this.panels.reverse);

      this.setReverseSlider(
        0,
        data.AvailableToReverse,
        data.AvailableToReverse
      );
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Update card expiry request has finished
   * Handles response
   */
  this.onGotTokenUpdate = function(e, data) {
    if (data.Status === "1") {
      this.cardTokenUpdate.txAccountTokenId = data.TxAccountTokenID;
      this.cardTokenUpdate.token1 = data.Token1;
      this.cardTokenUpdate.url1 = data.URL1;

      this.activePanel(this.panels.expiry);
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Get withdrawal detail request has finished
   * Handles response
   */
  this.onGotWithdrawDetail = function(e, data) {
    // reset flag
    this.withdrawal.playerIsUnverified(false);

    // handle status

    if (data.Status === "1") {
      this.withdrawal.realBalance(parseFloat(data.RealBalance).toFixed(2));
      this.withdrawal.availableToWithdraw(
        parseFloat(data.AvailableToWithdraw).toFixed(2)
      );
      this.withdrawal.bonusToBeForfeited(
        parseFloat(data.BonusToBeForfeited).toFixed(2)
      );
      this.withdrawal.minWithdraw(parseFloat(data.MinWithdraw).toFixed(2));
      this.withdrawal.withdrawalsPending(
        parseFloat(data.WithdrawalsPending).toFixed(2)
      );
      this.withdrawal.withdrawHoursPending(data.WithdrawHoursPending);
      this.withdrawal.enquiryEmail(data.EnquiryEmail || "");
      this.withdrawal.displayLimits(data.DisplayLimits || false);

      this.withdrawal.operation(this.withdrawal.operations.withdraw);
      this.withdrawal.buttonActive(true);
      this.showPanel(this.panels.withdraw);
      this.setWithdrawSlider(
        this.withdrawal.minWithdraw() || 0,
        data.AvailableToWithdraw,
        this.withdrawal.minWithdraw() || 0
      );
    } else if (data.Status === "-3") {
      var msg =
        data.StatusMsg +
        "<br/>" +
        sb.i18n("ifRequireWithdrawal") +
        " " +
        "<a href='mailto:" +
        this.encodeEntities(data.EnquiryEmail) +
        "'>" +
        this.encodeEntities(data.EnquiryEmail) +
        "</a>";
      this.withdrawal.displayLimits(data.DisplayLimits);
      this.withdrawal.minWithdraw(parseInt(data.MinWithdraw, 10));
      this.withdrawal.withdrawLimitCountPerDay(
        parseInt(data.WithdrawLimitCountPerDay, 10)
      );
      this.withdrawal.withdrawCountThisDay(
        parseInt(data.WithdrawCountThisDay, 10)
      );
      this.withdrawal.withdrawLimitPerDay(
        parseInt(data.WithdrawLimitPerDay, 10)
      );
      this.withdrawal.withdrawalsThisDay(parseInt(data.WithdrawalsThisDay, 10));
      this.withdrawal.withdrawLimitPerWeek(
        parseInt(data.WithdrawLimitPerWeek, 10)
      );
      this.withdrawal.withdrawalsThisWeek(
        parseInt(data.WithdrawalsThisWeek, 10)
      );
      this.withdrawal.withdrawLimitPerMonth(
        parseInt(data.WithdrawLimitPerMonth, 10)
      );
      this.withdrawal.withdrawalsThisMonth(
        parseInt(data.WithdrawalsThisMonth, 10)
      );
      this.handleError(data.Status, msg, data.OpenTime);
    } else if (data.Status === "104") {
      this.withdrawal.playerIsUnverified(true);
      this.withdrawal.securityEmail(data.SecurityEmail);
      this.withdrawal.operation(this.withdrawal.operations.unverified);
      this.showPanel(this.panels.withdraw);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Save bank details request has finished
   * Handles response
   */
  this.onSavedBankDetails = function(e, data) {
    if (data.Status === "1") {
      sb.showSuccess(sb.i18n("detailsChanged"));
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
  }.bind(this);

  /**
   * Player selected to see Bank details
   * Request bank details
   */
  this.openBankDetails = function() {
    // don't open if there is no customer Id yet (from response of start request)
    if (!this.cashierData.customerId) return;

    this.currentAction("bank-details");
    sb.notify("cashier-get-wire-details");
  }.bind(this);

  /**
   * Player selected deposit
   */
  this.openDeposit = function(forceDeposit) {
    if (!this.cashierData.customerId) return;

    this.resetPanelNavigation();
    this.checkAccount(
      parseInt(this.cashierData.activeAccounts, 10),
      parseInt(this.cashierData.maxAccounts, 10),
      parseInt(this.cashierData.withdrawalsPending, 10),
      parseInt(this.cashierData.showDepositLimits, 10),
      forceDeposit
    );
  }.bind(this);

  /**
   * Player selected to change the expiry date
   * Request WorldPay token update
   */
  this.openExpiry = function(card, e) {
    this.expiryYears(this.getExpiryYears());
    sb.notify("cashier-get-token-update", [card.AccountID]);
  }.bind(this);

  /**
   * Player selected withdrawal operation
   */
  this.openWithdraw = function() {
    // don't open if there is no customer Id yet (from response of start request)
    if (!this.cashierData.customerId) return;

    this.currentAction("withdraw");
    sb.notify("cashier-get-withdraw-detail");
  }.bind(this);

  /**
   * Player selected reversal operation
   */
  this.openReverse = function() {
    // don't open if there is no customer Id yet (from response of start request)
    if (!this.cashierData.customerId) return;

    this.currentAction("reverse");
    sb.notify("cashier-get-reverse-detail");
  }.bind(this);

  /**
                
                * Player selected account-verification operation
                */
  this.openAccountVerification = function() {
    // don't open if there is no customer Id yet (from response of start request)
    if (!this.cashierData.customerId) return;

    // show panel
    this.showPanel(this.panels.accountVerification);
    this.currentAction("account-verification");
  }.bind(this);

  /**
                
                * Player selected player limits operation
                */
  this.openPlayerLimits = function() {
    sb.notify("get-limit-values");
    this.showPanel(this.panels.depositLimits);
  }.bind(this);

  /**
   * Start request has finished
   * Handles response
   */
  this.onStarted = function(e, data) {
    if (data.Status === "1") {
      this.cashierData.customerId = data.CustomerID;
      this.cashierData.firstName = data.Firstname;
      this.cashierData.lastName = data.Lastname;
      this.cashierData.verified = data.Verified;
      this.cashierData.maxAccounts = data.MaxAccounts;
      this.cashierData.activeAccounts = data.ActiveAccounts;
      this.cashierData.withdrawalsPending = data.WithdrawalsPending;
      this.cashierData.forceReversalOfPendingWithdrawal =
        data.ForceReversalOfPendingWithdrawal;
      this.cashierData.felfExclude = data.SelfExclude;
      this.cashierData.secsSinceReg = data.SecsSinceReg;
      this.cashierData.deposited = data.Deposited;
      this.hasDeposited(data.Deposited);
      this.cashierData.canEditBankDetails(!!parseInt(data.ShowWireDetails, 10));
      this.cashierData.showDepositLimits = data.DepositLimitStatus;
      this.cashierData.pctDepositLimit(data.PctDepositLimit);
      this.cashierData.pctDepositLimitPeriod(data.PctDepositLimitPeriod);
      this.playerLimits.showDepositLimitsLink(
        data.Deposited === "0" && data.DepositLimitStatus === "1"
      );
      if (this.URUVerified() != 1) {
        this.showWithdrawMenu(false);
        this.showReverseMenu(false);
        if (this.URUVerified() == 0) {
        }
        if (this.URUVerified() == 2) {
          this.showDepositMenu(false);
        }
        if (this.URUVerified() == 3) {
          this.showDepositMenu(false);
        }
      }

      // check for deposit limits request
      if (this.getParameterByName("settings")) {
        // lets open the deposit limits screen
        this.openLimitsScreen(false);
        return;
      }

      // check for account verification request
      if (this.getParameterByName("verification")) {
        // lets open the deposit limits screen
        this.openAccountVerification();
        return;
      }

      this.checkAccount(
        parseInt(this.cashierData.activeAccounts, 10),
        parseInt(this.cashierData.maxAccounts, 10),
        parseInt(this.cashierData.withdrawalsPending, 10),
        parseInt(this.cashierData.showDepositLimits, 10)
      );
    } else if (data.Status === "-3") {
      sb.showError(data.StatusMsg);
    } else {
      this.handleError(data.Status, data.StatusMsg, data.OpenTime);
    }
    sb.notify("get-player-account", []);
  }.bind(this);

  /**
   * Does a post to a given URL
   * @param path The URL
   * @param params Parameters to pass on the query
   */
  this.postToUrl = function(path, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", path);

    var key;
    for (key in params) {
      if (params.hasOwnProperty(key)) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);

        form.appendChild(hiddenField);
      }
    }

    document.body.appendChild(form);
    form.submit();
  };

  /**
   * Player selected to retry CVN
   */
  this.retryCVN = function() {
    sb.notify("cashier-deposit-retry-CVN", [this.correctCVN]);
  }.bind(this);

  this.saveBankDetails = function() {
    var validation = this.bankDetails.validation();
    if (validation === "") {
      sb.notify("cashier-save-bank-details", [this.bankDetails]);
    } else {
      sb.showWarning(validation);
    }
  }.bind(this);

  /**
   * Player selected to save card's expiry date
   * If valid makes the request
   */
  this.saveExpiry = function() {
    var validator = new sbm.CardValidator(sb);
    var validExpiry = validator.isValidExpiry(
      this.cardTokenUpdate.expiryYear(),
      this.cardTokenUpdate.expiryMonth()
    );

    if (!validExpiry) {
      sb.showWarning(sb.i18n("invalidExpiryDate"));
      return;
    }

    this.postToUrl(this.cardTokenUpdate.url1, {
      Action: "Update",
      OTT: this.cardTokenUpdate.token1,
      ExpDate:
        this.cardTokenUpdate.expiryMonth() + this.cardTokenUpdate.expiryYear()
    });
  }.bind(this);

  /**
   * Player selected an account
   */
  this.selectedAccount = function(account, e) {
    var URUVerification = sb.getCookie("URUVerified");
    if (URUVerification == 1) {
      switch (account.AccountTypeCode) {
        //VISA
        case "A_VISA_CREDIT":
          account.icon = "visa-icon.svg";
          break;
        case "A_VISA_DEBIT":
          account.icon = "visa-icon.svg";
          break;
        case "A_ELECTRON":
          account.icon = "visa-icon.svg";
          break;
        //Mastercard
        case "A_MASTERCARD_CREDIT":
          account.icon = "mastercard-icon.svg";
          break;
        case "A_MASTERCARD_DEBIT":
          account.icon = "mastercard-icon.svg";
          break;
        //Maestro
        case "A_MAESTRO":
          account.icon = "maestro-icon.svg";
          break;
        default:
          account.icon = "card-icon.png";
      }
      if (account.LimitReached !== "") {
        var period = "";
        if (account.LimitReached === "d") period = "daily";
        else if (account.LimitReached === "w") period = "weekly";
        else period = "monthly";

        sb.showWarning(sb.i18n("accountReachedLimit", [period]));
        return;
      }

      if (account.ExpireStatus === "2") {
        sb.showWarning(sb.i18n("cardHasExpired"));
        return;
      }

      // reset some deposit data
      this.deposit.resetAmounts();
      this.deposit.iDoNotWantBonus(false);

      // fill deposit data
      this.deposit.accountId = account.AccountID;
      this.deposit.minDepositAmount = parseInt(account.MinDepositAmount, 10);
      this.deposit.methodIcon(account.icon);
      // swap different types of credit/debit cards into A_CC
      if (this.isACC(account.AccountTypeCode)) {
        this.deposit.accountType("A_CC");
        this.accountDescription(account.listDescription);
      } else {
        this.deposit.accountType(account.AccountTypeCode);
        this.accountDescription("");
      }

      this.deposit.depositAmounts(account.Amounts);

      var promos = this.checkForPromoCodeAvailability(account.Promos);
      this.deposit.promos(promos);
      this.deposit.needsCvn(account.CVN === "1");

      // if account is neteller should show the neteller secure Id
      this.deposit.showNetellerSecureId(
        account.AccountID && account.AccountTypeCode === "A_NETELLER"
      );

      this.showPanel(this.panels.promotions);
    } else {
      this.showDepositMenu(false);
      sb.setCookie("URUVerified", 2);
      sb.notify("age-id-verification-check-if-verified", ["2"], false);
      this.showPanel(this.panels.uruDcoumentUpload);
    }
  }.bind(this);

  /**
   * Player selected one of the available amounts
   */
  this.selectedAmount = function(amount, e) {
    var selectedAmount = parseInt(amount.Amount, 10);
    // do not set amount if wants bonus but amount is below min deposit
    if (
      !this.deposit.doNotWantBonus &&
      this.deposit.promoDepositMin() > selectedAmount
    ) {
      return;
    }
    this.deposit.amount(selectedAmount);
    // clear free amount
    this.deposit.minDepositError("");
    this.deposit.freeAmount(selectedAmount);
  }.bind(this);

  /**
   * Player selected one of the available promotions
   */
  this.selectedPromo = function(promo, e) {
    this.deposit.promoId(parseInt(promo.PromoID, 10));
    this.deposit.promoDepositMin(parseInt(promo.DepositMin, 10));
    this.deposit.amount(0);
    this.activePanel(this.panels.amounts);
    window.scrollTo(0, 0);
  }.bind(this);

  /**
   * Player selected to add a new account
   */
  this.selectedNewMethod = function(method) {
    // If not verified
    var URUVerified = sb.getCookie("URUVerified");
    if (URUVerified != "1") {
      if (URUVerified == "0") {
        sb.showPleaseWaitBox(
          "Identity Verification",
          "<p>We are required by UK regulations to verify your age and address before you can continue.</p><small>Please bear with us whilst we run this check.</small>"
        );
        sb.notify("age-id-verification-run-uru-check", []);
      } else {
        sb.notify("age-id-verification-check-if-verified", [
          URUVerified,
          {},
          false
        ]);
      }
    } else {
      var promos = this.checkForPromoCodeAvailability(method.Promos);
      this.enterDetails(method.AccountTypeCode, {
        amounts: method.Amounts,
        promos: promos,
        cvn: !!method.CVN
      });
      // reset the account Id - when player has selected previously an existent account
      this.deposit.accountId = "";
      this.deposit.showNetellerSecureId(false);
      this.deposit.minDepositAmount = parseInt(method.MinDepositAmount, 10);
    }
  }.bind(this);

  /**
   * Reverse slider configuration
   * @param min The minimum value to reverse
   * @param max The maximum value to reverse
   * @param defaultVal The default value to be set on the slider
   */
  this.setReverseSlider = function(min, max, defaultVal) {
    this.reversal.amountToReverse(parseFloat(defaultVal).toFixed(2));

    min = parseFloat(min);
    max = parseFloat(max);
    var reachedMax = false;
    var step = 1;

    sb.$("#ui-reversal-slider").slider({
      value: defaultVal,
      min: min,
      max: max,
      step: step,
      slide: function(event, ui) {
        reachedMax = ui.value + step > max;
        if (reachedMax) {
          this.reversal.amountToReverse(max.toFixed(2));
        } else {
          this.reversal.amountToReverse(parseFloat(ui.value).toFixed(2));
        }
      }.bind(this)
    });
  }.bind(this);

  /**
   * Withdrawal slider configuration
   * @param min The minimum value to withdraw (integer)
   * @param max The maximum value to withdraw (integer)
   * @param defaultVal The default value to be set on the slider (integer)
   */
  this.setWithdrawSlider = function(min, max, defaultVal) {
    this.withdrawal.amountToWithdraw(parseInt(defaultVal, 10).toFixed(2));

    min = parseInt(min, 10);
    max = parseFloat(max);
    var reachedMax = false;
    var step = 1;

    sb.$("#ui-withdrawal-slider").slider({
      value: defaultVal,
      min: min,
      max: max,
      step: step,
      slide: function(e, ui) {
        reachedMax = ui.value + step > max;
        if (reachedMax) {
          this.withdrawal.amountToWithdraw(max.toFixed(2));
        } else {
          this.withdrawal.amountToWithdraw(parseInt(ui.value, 10).toFixed(2));
        }
      }.bind(this)
    });
  }.bind(this);

  /**
   * Request Start
   */
  this.start = function() {
    this.checkForClaimCode(this.getParameterByName("claim"));

    sb.notify("cashier-start", [this.cashierData]);
  };

  /**
   * Check if there is a code to claim and set it as default
   */
  this.checkForClaimCode = function(code) {
    if (code) {
      // remove all forward slashes - our current setup adds it to the end of the URLs
      code = code.replace(/\//g, "");
      this.deposit.iDoNotWantBonus(false);
      this.deposit.iHavePromoCode(true);
      this.deposit.promoCode(code);
    }
  };

  /**
   * Request the available methods
   */
  this.showAvailableMethods = function() {
    sb.notify("cashier-get-account-types");
  };

  /**
   * Show a screen
   * @param panel The panel to show
   */
  this.showPanel = function(panel, noPush) {
    var path = window.location.pathname;
    var cardDetailsValid = false;

    if (!noPush && history.pushState) {
      history.pushState(panel, path, path);
    }

    var valid = true;
    var goToDepositScreen = false;

    if (panel === this.panels.promotions) {
      if (this.activePanel() === this.panels.enterDetails) {
        valid = this.validDetails();
        cardDetailsValid = true;
      }

      if (!this.deposit.promos().length) {
        goToDepositScreen = true;
      }
    }

    if (valid) {
      if (cardDetailsValid) this.encryptBarclaycardForm();

      this.canMakeDeposit(true);
      this.deposit.resetAmounts();

      if (goToDepositScreen) {
        this.deposit.promoId(0);
        this.deposit.promoDepositMin(0);
        this.activePanel(this.panels.amounts);
      } else {
        this.activePanel(panel);
      }

      window.scrollTo(0, 0);
    }
  }.bind(this);

  /**
   * Validates the chosen account details
   * @returns {boolean}
   */
  this.validDetails = function() {
    var validation = "";
    if (this.deposit.accountType() === "A_CC") {
      validation = this.addingNewMethod.card.validation();
    } else if (this.deposit.accountType() === "A_NETELLER") {
      validation = this.addingNewMethod.neteller.validation();
    } else if (this.deposit.accountType() === "A_PAYSAFECARD") {
      validation = this.addingNewMethod.paysafe.validation();
    }
    if (validation === "") {
      return true;
    }
    sb.showWarning(validation);
    return false;
  };

  /**
   * Checks if the account type is a debit/credit card
   * @param accountTypeCode - account type code
   * @returns {boolean}
   */
  this.isACC = function(accountTypeCode) {
    return (
      accountTypeCode === "A_VISA_CREDIT" ||
      accountTypeCode === "A_VISA_DEBIT" ||
      accountTypeCode === "A_ELECTRON" ||
      accountTypeCode === "A_MASTERCARD_CREDIT" ||
      accountTypeCode === "A_MAESTRO" ||
      accountTypeCode === "A_MASTERCARD_DEBIT"
    );
  };

  /**
   * Show previous panel
   */
  this.goBack = function() {
    if (this.currentPanelIndex == 2) {
      this.deposit.iDoNotWantBonus(false);
    }
    this.showPanel(this.previousPanels[this.currentPanelIndex - 1]);
  }.bind(this);

  /**
   * Resets auxiliary variables used for holding information about panels and current panel index
   */
  this.resetPanelNavigation = function() {
    this.previousPanels = [];
    this.currentPanelIndex = -1;
  }.bind(this);

  this.onNavigation = function(e, panel) {
    this.showPanel(panel, true);
  }.bind(this);

  /**
   * Escapes all potentially dangerous characters, so that the
   * resulting string can be safely inserted into attribute or
   * element text.
   * @param value
   * @returns {string} escaped text
   */
  this.encodeEntities = function(value) {
    return value
      .replace(/&/g, "&amp;")
      .replace(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g, function(value) {
        var hi = value.charCodeAt(0);
        var low = value.charCodeAt(1);
        return "&#" + ((hi - 0xd800) * 0x400 + (low - 0xdc00) + 0x10000) + ";";
      })
      .replace(/([^\#-~| |!])/g, function(value) {
        return "&#" + value.charCodeAt(0) + ";";
      })
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;");
  };

  /**
   * Check for promo code
   */
  this.checkPromoCode = function() {
    this.deposit.canValidatePromo(true);
    var p = this.deposit.promoCode();
    p
      ? sb.notify("check-promo-code", [p])
      : sb.showWarning(sb.i18n("invalidPromoCode"));
  }.bind(this);

  /**
   * Received response from backend for the promo code validation
   */
  this.checkedPromoCode = function(e, data) {
    this.deposit.isValidPromo(data.Code === 0);
    if (data.Code === 0) {
      sb.notifySuccess(sb.i18n("isValidPromoCode"));
      this.deposit.iHavePromoCode(true);
    } else {
      sb.notifyWarning(data.Msg);
    }
  }.bind(this);

  /**
   * Opens the deposit limits disclaimer screen
   * If bypass is set it should open the limit setup instead
   * @param onlyMonthLimit True if player can only set monthly limit
   * @param bypassDisclaimer boolean - if true should go straight to deposit limit change screen
   */
  this.openLimitsScreen = function(onlyForMonthly, bypassDisclaimer) {
    sb.notify("get-limit-values");

    this.showPanel(this.panels.depositLimits);
    this.playerLimits.allowOnlyMonthlyLimit(onlyForMonthly);

    if (bypassDisclaimer === true) {
      this.playerLimits.bypassDisclaimer = true;
      this.playerLimits.limitWarningPeriod = this.cashierData.limitWarningPeriod;
    } else {
      this.playerLimits.openDisclaimer();
    }
  }.bind(this);

  this.onGotBalance = function(e, data) {
    this.prizeWheelSpins(sb.getCookie("PWSpins"));
    this.bonusSpins(data.BonusSpins);
  }.bind(this);

  this.onGotPlayerAccount = function(e, data) {
    this.bingoCards(data.Cards);
  }.bind(this);
};
