var sbm = sbm || {};

/*
	Module used to manage player verification - Source of Funds (DEV-11151)
*/
sbm.AccountVerification = function (sb) {

	// properties

	// each step will have its own action so we can show/hide on the screen
	this.currentAction = sb.ko.observable("loading");

  //see a different message depending on the type
  this.mandatoryType = sb.ko.observable("");

  this.mandatoryTypeKYC = sb.ko.observable("");
  // Files that the player needs to upload for SOF
  this.filesToUploadSOF = sb.ko.observableArray([]);

   // Files that the player needs to upload for KYC
  this.filesToUploadKYC = sb.ko.observableArray([]);

  this.uiUploadFiles;

  this.uploadedFiles = sb.ko.observableArray([]);

  this.fileTypes = [
    "jpeg",
    "pjpeg",
    "png",
    "pdf"
  ];

  this.mandatory;

  this.numberOfFilesToUpload;

	// initialization - must be called when module is created
	this.init = function () {

		// event listening

		sb.listen("got-documents", this.gotDocuments);
    sb.listen("upload-documents-finished", this.onUploadFilesFinished);

    // get Dom element
    this.uiUploadFiles = document.getElementById("ui-file-uploads");

    // add event to be triggered when player uploads files
    if(this.uiUploadFiles !== null)
      this.uiUploadFiles.addEventListener("change", this.onFileUpload);

    // retrieve list of all documents that can be uploaded
    sb.notify("get-documents");
	};

	// destruction - must be called when module is destroyed
	this.destroy = function () {

		sb.ignore("got-documents");
    sb.ignore("upload-documents-finished");
	};

  // got list of documents from core services
  this.gotDocuments = function (e, data) {

    this.currentAction("account-verification-upload");
    var allDocs = JSON.parse(data.Body);
    /* Check the files that we need to get from the player*/
    var verifyAccountData = JSON.parse(sb.getFromStorage("verifyAccount"));
    if(verifyAccountData != null){
      
    var docsToUpload = verifyAccountData.playerAccountVerificationDocuments;

    // return array of documents required
    var docNameListSOF = this.setDocumentListSOF(docsToUpload, allDocs);
    var docNameListKYC = this.setDocumentListKYC(docsToUpload, allDocs);

    this.mandatory = verifyAccountData.mandatory;
    this.mandatoryKYC = verifyAccountData.mandatoryKYC;

    this.numberOfFilesToUpload = (this.mandatory === true? docNameListSOF.length : 1) + (this.mandatoryKYC === true? docNameListKYC.length : 1);

    var docsInfoSOF = this.setDocsToUploadInfo(docNameListSOF, this.mandatory);
    var docsInfoKYC = this.setDocsToUploadInfo(docNameListKYC, this.mandatoryKYC);


    this.filesToUploadSOF(docsInfoSOF);
    this.filesToUploadKYC(docsInfoKYC);

    if (this.mandatory && this.mandatoryKYC) {
      this.mandatoryType("To guarantee customer security and to avoid future restrictions, please provide us with all of the following documents:");
      
      if(this.filesToUploadKYC().length > 0 && this.filesToUploadSOF().length > 0) {
          this.mandatoryTypeKYC("In addition, please also provide us with all of the following documents:");
        } else {
          this.mandatoryTypeKYC("");
        }

    } else if(!this.mandatory && !this.mandatoryKYC) {
       this.mandatoryType("To guarantee customer security and to avoid future restrictions, please provide us with at least one of the following documents:");
       
       if(this.filesToUploadKYC().length > 0) {
          this.mandatoryTypeKYC("In addition, please also provide us with one of the following documents:");
        } else {
          this.mandatoryTypeKYC("");
        }

    } else if (!this.mandatory && this.mandatoryKYC) {
        this.mandatoryType("To guarantee customer security and to avoid future restrictions, please provide us with at least one of the following documents:");
        
        if(this.filesToUploadKYC().length > 0 && this.filesToUploadSOF().length > 0 ) {
          this.mandatoryTypeKYC("In addition, please also provide us with all of the following documents:");
        } else {
          this.mandatoryTypeKYC("");
        }
        
    } else if (this.mandatory && !this.mandatoryKYC) {
        if(this.filesToUploadSOF().length != 0) {
          this.mandatoryType("To guarantee customer security and to avoid future restrictions, please provide us with all of the following documents:");
        } else {
          this.mandatoryType("To guarantee customer security and to avoid future restrictions, please provide us with at least one of the following documents:");
        }
        if(this.filesToUploadKYC().length > 0 && this.filesToUploadSOF().length > 0) {
          this.mandatoryTypeKYC("In addition, please also provide us with one of the following documents:");
        } else {
          this.mandatoryTypeKYC("");
        }
    } 
  }

  }.bind(this);

  // set the info for the player regarding which files they need to upload
  // param docs: array yof docs name
  // param mandatory: true if player needs to upload all files, false just one of them
  this.setDocsToUploadInfo = function (docs, mandatory) {

    var result = []; 
      join = mandatory ? " <b>AND</b> " : " <b>OR</b> ";

    for (var i = 0; i < docs.length; i++) {
      
      if (!i) {
        result.push(docs[i]);
      } else {
        docs[i].filename = join + docs[i].filename; 
        result.push(docs[i]);
      }
    }

    return result;

  }.bind(this);

  this.setDocumentListSOF = function (playerDocsObject, allDocs) {
    var doc,
        docName = "",
        docListSOF = [];
        
    // loop through the players docs to upload
    for (var i=0; i < playerDocsObject.length; i++) {
      /* Use underscore, if we find the id on the array we create a new variable which will give us more info on the file type */
      doc = sb._.find(allDocs, function (d) {        
        return   d.id === playerDocsObject[i].documentTypeId;
      });
      if (doc) {      
        /* If this is a SOF File */
        if(doc.category.id === 0){
          if (playerDocsObject[i].extraInfo !== "" && playerDocsObject[i].extraInfo !== "null" ) {    
            docName = {
              "filename": doc.document + " (" + playerDocsObject[i].extraInfo + ")",
              "format": doc.format
            };
          } else {
            docName = {
              "filename": doc.document, 
              "format": doc.format
            };
          }
          docListSOF.push(docName);
        }
      }
      
    } 
    return docListSOF;
};

  this.setDocumentListKYC = function (playerDocsObject, allDocs) {
    var doc,
        docName = "",
        docListKYC = [];
        
    // loop through the players docs to upload
    for (var i=0; i < playerDocsObject.length; i++) {
      /* Use underscore, if we find the id on the array we create a new variable which will give us more info on the file type */
      doc = sb._.find(allDocs, function (d) {        
        return   d.id === playerDocsObject[i].documentTypeId;
      });
      if (doc) {      
        /* If this is a SOF File */
        if(doc.category.id === 1){
          if (playerDocsObject[i].extraInfo !== "" && playerDocsObject[i].extraInfo !== "null" ) {    
            docName = {
              "filename": doc.document + " (" + playerDocsObject[i].extraInfo + ")",
              "format": doc.format
            };
          } else {
            docName = {
              "filename": doc.document, 
              "format": doc.format
            };
          }
          docListKYC.push(docName);
        }
      }
      
    } 
    return docListKYC;
};


  /************************************************************************************************
                                          Files Upload
  *************************************************************************************************/

  // when player uploads files
  this.onFileUpload = function (input) {

    // validation

    var files = input.target.files;

    for (var i = 0; i < files.length; i++) {
      if (!this.validFileType(files[i].type)) {
        sb.showError("File type '" + files[i].type +"' is not valid. Please remove it and try again.");
        return;
      }
    }

    // all good - add files to the list - append if there's any

    var list = this.uploadedFiles();
    for (var i = 0; i < files.length; i++) {
      list.push(files[i]);
    }
    this.uploadedFiles(list);

  }.bind(this);

  // remove file from the list of uploads
  this.removeFile = function (data) {
  
    var filename = data.name;

    // remove file from array
    var newList = sb._.reject(this.uploadedFiles(), function (file) {
      return file.name === filename;
    });

    this.uploadedFiles(newList);

  }.bind(this);

  // validates if file type is correct
  this.validFileType = function (file) {
    for(var i = 0; i < this.fileTypes.length; i++) {
      // we only consider the extension and not the full type
      if(file.indexOf(this.fileTypes[i]) > -1) {
        return true;
      }
    }
  };

  // validates the size of the files
  // returns true if all good otherwise false
  // limit: 10 MB (10485760 bytes) overall
  this.sizeValidation = function () {

    var files = this.uploadedFiles(),
        total = 0;

    for (var i = 0; i < files.length; i++) {
      total += files[i].size;
    }

    if (total < 10485760) return true;

    return false;
  };

  // upload file
  this.upload = function () {

    // size validation
    
    
    if (this.sizeValidation()) {
      
      if (this.numberValidation()) {
        sb.notify("upload-documents", [this.uploadedFiles(), sb.getFromStorage("playerId"), sb.getFromStorage("sessionId")]);
      
      } else {

        sb.showError("At least " + this.numberOfFilesToUpload + " documents need to be uploaded");        
      }

    } else {

      sb.showError("Upload failed. Total file size exceeds 10MB.");
    }

  }.bind(this);

  this.numberValidation = function () {

    if (this.mandatory) {
      return this.uploadedFiles().length >= this.numberOfFilesToUpload;
    }

    return this.numberOfFilesToUpload > 0;

  };

  // files have been uploaded
  this.onUploadFilesFinished = function (e, data) {

    if (data.Code === 0) {
      
      var c = function () {
        location.pathname = "/start/";
      }
      
      // remove from storage information regarding the SOF
      sb.removeFromStorage("verifyAccount");
      sb.removeFromStorage("verifyAccountPopUp");

      // successful notification
      sb.showSuccess("Submit successful. We will be in touch shortly to inform you whether your documents have been successfully validated.", c);

    } else {

      sb.showError("Upload failed, please try again or contact our support team.");
      this.uploadedFiles([]);
    }
  }.bind(this);

};