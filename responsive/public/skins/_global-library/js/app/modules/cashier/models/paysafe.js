var sbm = sbm || {};

/**
 * Paysafe card details
 * @param sb The sandbox
 * @constructor
 */
sbm.Paysafe = function (sb) {
    "use strict";

    this.firstName = sb.ko.observable("");
    this.lastName = sb.ko.observable("");
    this.email = sb.ko.observable("");
    this.dayOfBirth = sb.ko.observable("");
    this.monthOfBirth = sb.ko.observable("");
    this.yearOfBirth = sb.ko.observable("");
    this.dob = new sb.ko.pureComputed(function () {
        if (this.yearOfBirth() && this.monthOfBirth() && this.dayOfBirth()) {
            return this.yearOfBirth() + "-" + this.monthOfBirth() + "-" + this.dayOfBirth();
        }
        return "";
    }.bind(this));

    /**
     * Validates the card details
     * @returns {string} First error or empty
     */
    this.validation = function () {

        if (this.firstName() === '') {
            return "Invalid first name";
        }
        if (this.lastName() === '') {
            return "Invalid last name";
        }
        if (!this.isValidEmailAddress(this.email())) {
            return "Invalid email";
        }
        if (!this.isValidDOB(this.yearOfBirth(), parseInt(this.monthOfBirth(), 10) - 1, this.dayOfBirth())) {
            return "Invalid date of birth";
        }

        return "";
    };

    /**
     * Email validation
     * @param emailAddress
     * @returns {boolean}
     */
    this.isValidEmailAddress = function (emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };

    /**
     * Date of birth validation
     * @param y Year
     * @param m Month
     * @param d Day
     * @returns {boolean}
     */
    this.isValidDOB = function (y, m, d) {
        var composedDate = new Date(y, m, d);
        return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
    };
};