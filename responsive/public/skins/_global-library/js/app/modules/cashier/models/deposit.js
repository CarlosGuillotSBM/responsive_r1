var sbm = sbm || {};

/**
 * Deposit details
 * @param sb The sandbox
 * @constructor
 */
sbm.Deposit = function (sb) {
    "use strict";

    // subscriptions

    /**
     * Player selected option "I do not want bonus"
     */
    this.selectedIDoNotWantBonus = function () {
        this.amount("");
        this.promoId(0);
        if (this.iDoNotWantBonus()) {
            this.promoDepositMin(0);
        } else {
            this.promoDepositMin(9999);
        }
    }.bind(this);

    /**
     * Player selected option "I have a promo code"
     */
    this.selectedIHavePromoCode = function (newVal) {
        !newVal && this.promoCode("");
        this.amount("");
        this.promoId("-1");
        if (this.iHavePromoCode()) {
            this.promoDepositMin(0);
        }
    }.bind(this);

    /**
     * Player changed the value on "Enter Other Amount"
     */
    this.depositFreeAmountChanged = function (amount) {

        if (amount === "") {
            this.minDepositError("");
            return;
        }

        // is invalid

        if (isNaN(amount)) {
            this.minDepositError("Please enter a valid number");
            return;
        }

        amount = parseFloat(amount);


        // player has selected a bonus - so we need to make sure we meet the minimum deposit for it
        // validate the promo minimum deposit
        if (!this.iDoNotWantBonus() && amount < this.promoDepositMin()) {
            this.minDepositError("Minimum Deposit is " + this.currencySymbol + this.promoDepositMin());
            this.amount("");
            return;
        }

        // player without selecting a bonus - we need to make sure we meet the minimum of the payment method
        if (amount < this.minDepositAmount) {
            this.minDepositError("Minimum Deposit is " + this.currencySymbol + this.minDepositAmount);
            this.amount("");
            return;
        }

        // validate maximum of 1000 (hardcoded for now)
        if (amount > 1000) {
            this.minDepositError("Maximum Deposit is " + this.currencySymbol + "1000");
            this.amount("");
            return;
        }

        // is valid

        // round to 2 decimals if is float
        if (amount % 1 !== 0) {
            amount = Math.round(amount * 100) / 100;
        }

        this.amount(amount);
        this.minDepositError("");

    }.bind(this);

    /**
     * The deposit amount has changed
     */
    this.depositAmountChanged = function (newAmount) {
        var promo = _.find(this.promos(), function (promo) {
            return promo.PromoID == this.promoId(); // equality comparing required
        }.bind(this));
        
        if (promo && promo.PromoID && newAmount > 0) {
            var bonusAmount = newAmount * (parseInt(promo.Percent, 10) / 100);
            if (bonusAmount > promo.MaxBonus) {
                bonusAmount = promo.MaxBonus;
            }

            bonusAmount = Math.round(bonusAmount * 100) / 100;

            var bonusExtra = parseInt(promo.PrizeAmount, 10) > 0 ? promo.PrizeAmount + " " + promo.PrizeType.replace("3 Radical -", "") : "";
            var bonusExtra2 = parseInt(promo.PrizeAmount2, 10) > 0 ? promo.PrizeAmount2 + " " + promo.PrizeType2.replace("3 Radical -", "") : "";
            if(promo.PrizeAmount>0 || promo.bonusAmount > 0){
                var text = " and get ";
            }
            if(bonusAmount>0){
                text = " and get " + this.currencySymbol + bonusAmount + " bonus";
            }
            if (bonusExtra !== "") {
                (bonusAmount >0 ? text += " + " + bonusExtra : text += bonusExtra);
            }
            if (bonusExtra2 !== "") {
                text += " + " + bonusExtra2;
            }            
            this.depositPromoInfo(text);
        } else {
            this.depositPromoInfo("");
        }
    }.bind(this);

    /**
     * Selected account type changed
     */
    this.onAccounTypeChanged = function (newVal) {
        // change icon depending on the selected account (CREDIT AND DEBIT ARE DEALT FROM CASHIER MAIN)
         if (newVal === "A_NETELLER") {
            this.methodIcon("neteller-icon.svg");

        } else if (newVal === "A_PAYSAFECARD") {
            this.methodIcon("paysafe-icon.svg");

        } else if (newVal === "A_PAYPAL") {
            this.methodIcon("paypal-icon.svg");
        }
        // clear promo
        this.promoId(0);
    }.bind(this);

    /**
     * Promo code has changed
     */
    this.promoCodeChanged = function (newVal) {
        this.isValidPromo("");
        this.canValidatePromo(!!newVal);
    }.bind(this);

    // properties

    this.accountId = null;
    this.accountType = sb.ko.observable("");
    this.accountType.subscribe(this.onAccounTypeChanged);
    this.promoId = sb.ko.observable(0);
    this.maxDeposit = 99999;
    this.promoDepositMin = sb.ko.observable(this.maxDeposit);
    this.amount = sb.ko.observable(0);
    this.amount.subscribe(this.depositAmountChanged);
    this.depositPromoInfo = sb.ko.observable("");
    this.freeAmount = sb.ko.observable("");
    this.freeAmount.subscribe(this.depositFreeAmountChanged);
    this.promoCode = sb.ko.observable("");
    this.promoCode.subscribe(this.promoCodeChanged);
    this.affCredit = sb.$.cookie("AffCredit");
    this.txDepositId = null;
    this.dsid = null;
    this.needsCvn = sb.ko.observable(false);
    this.cvn = sb.ko.observable("");
    this.cvnOption = sb.ko.observable(false);
    this.depositAmounts = sb.ko.observableArray([]);
    this.promos = sb.ko.observableArray([]);
    this.iHavePromoCode = sb.ko.observable(false);
    this.iHavePromoCode.subscribe(this.selectedIHavePromoCode);
    this.iDoNotWantBonus = sb.ko.observable(false);
    this.iDoNotWantBonus.subscribe(this.selectedIDoNotWantBonus);
    this.minDepositError = sb.ko.observable("");
    this.currencySymbol = sb.getFromStorage("currencySymbol") || "";
    this.methodIcon = sb.ko.observable("");
    this.promoCodeAvailable = sb.ko.observable(false);
    this.showNetellerSecureId = sb.ko.observable(false);
    this.netellerSecureId = sb.ko.observable("");
    this.isValidPromo = sb.ko.observable("");
    this.canValidatePromo = sb.ko.observable(false);
    this.acknowledged = sb.ko.observable(false);
    this.showAcknowledged = sb.ko.observable(true);
    this.minDepositAmount = 10; // default min amount that a player can deposit - should be overriden by the COIN MinDepositAmount value for the method

    // functions

    /**
     * Checks if is a valid promo code
     * @returns {boolean}
     */
    this.validPromoCode = function () {
        return this.iHavePromoCode() ? this.promoCode().length >= 3 && this.promoCode().length <= 20 : true;
    };

    /**
     * Clear the selected amount and the value on "Enter Other Amount"
     */
    this.resetAmounts = function () {
        this.amount("");
        this.freeAmount("");
    };
};