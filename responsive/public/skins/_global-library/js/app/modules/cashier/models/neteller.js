var sbm = sbm || {};

/**
 * Neteller details
 * @param sb The sandbox
 * @constructor
 */
sbm.Neteller = function (sb) {
    "use strict";

    this.number = sb.ko.observable("");
    this.secureId = sb.ko.observable("");

    /**
     * Validates a Neteller card
     * @returns {string} First error or empty
     */
    this.validation = function () {
        // remove any spaces from number
        var number = this.number().replace(/ /g, "");
        // validates if number is 12 digits
        if (isNaN(number) || number.length !== 12) {
            return "Neteller Number is not valid";
        }
        // validates if secure Id is 6 digits
        if (isNaN(this.secureId()) || this.secureId().length !== 6) {
            return "Secure Id is not valid";
        }
        //  is valid
        return "";
    };

};