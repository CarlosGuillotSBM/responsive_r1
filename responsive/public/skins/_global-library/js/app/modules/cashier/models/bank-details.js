var sbm = sbm || {};
/**
 * Class with all data used for setting player's wire details
 * @param sb sandbox
 * @constructor
 */
sbm.BankDetails = function (sb) {
    "use strict";

    this.wireStatus = sb.ko.observable("");
    this.bankName = sb.ko.observable("");
    this.accountName = sb.ko.observable("");
    this.accountNumber = sb.ko.observable("");
    this.branchCode = sb.ko.observable("");
    this.swiftCode = sb.ko.observable("");
    this.country = sb.ko.observable("");
    this.address = sb.ko.observable("");
    this.city = sb.ko.observable("");
    this.state = sb.ko.observable("");
    this.postCode = sb.ko.observable("");
    this.correspondingBankName = sb.ko.observable("");

    /**
     * Details validation
     * @returns {string} The first error or empty
     */
    this.validation = function () {
        if (this.bankName() === "") {
            return "Invalid Bank Name";
        }
        if (this.accountName() === "") {
            return "Invalid Account Name";
        }
        if (this.accountNumber() === "") {
            return "Invalid Account Number";
        }
        if (this.swiftCode() === "") {
            return "Invalid Swift Code";
        }
        if (this.country() === "") {
            return "Invalid Country";
        }
        if (this.address() === "") {
            return "Invalid Address";
        }
        if (this.city() === "") {
            return "Invalid City";
        }

        return "";
    };
};