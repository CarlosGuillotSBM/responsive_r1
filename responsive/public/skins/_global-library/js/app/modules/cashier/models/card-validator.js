var sbm = sbm || {};

/**
 * Card validation
 * Validates any type of cards
 * param sb - Sandbox
 */
sbm.CardValidator = function (sb) {
    "use strict";

    this.cardType = null;

    /**
     * Validates the credit card data entered
     * @param cardData { firstName, lastName, number, expiryYear, expiryMonth, cvn (optional) }
     * @returns {string} first error or empty
     */
    this.isValidCreditCard = function (cardData) {
        // validates first name
        if (!(cardData.firstName.match("^[a-zA-Z ]+$")) || sb.$.trim(cardData.firstName) === "") {
            return "Invalid first name";
        }
        // validates last name
        if (!(cardData.lastName.match("^[a-zA-Z ]+$")) || sb.$.trim(cardData.lastName) === "") {
            return "Invalid last name";
        }
        // remove any spaces from number
        cardData.number = cardData.number.replace(/ /g, "");
        // validates number
        if (!this.isValidNumber(cardData.number)) {
            return "Card Number is not valid";
        }
        // validates expiry
        if (!this.isValidExpiry(cardData.expiryYear, cardData.expiryMonth)) {
            return "Expiry Date is not valid";
        }
        // validates cvn if applicable
        if (!this.isValidCVN(cardData.cvn)) {
            return "CVN is not valid";
        }
        // card data is valid
        return "";
    };

    /**
     * Validates a Neteller card
     * @param cardData { number, secureId }
     * @returns {string}
     */
    this.isValidNeteller = function (cardData) {
        // remove any spaces from number
        cardData.number = cardData.number.replace(/ /g, "");
        // validates if number is 12 digits
        if (isNaN(cardData.number) || cardData.number.length !== 12) {
            return cashieri18n.netellerNotValid; //'Neteller Number is not valid';
        }
        // validates if secure Id is 6 digits
        if (isNaN(cardData.secureId) || cardData.secureId.length !== 6) {
            return cashieri18n.secureIdNotValid; //'Secure Id is not valid';
        }
        //  is valid
        return '';
    };

    /**
     * Validates the expiry date
     * Checks the expiry date is this month or after
     * @param cardYear
     * @param cardMonth
     * @returns {boolean}
     */
    this.isValidExpiry = function (cardYear, cardMonth) {
        if (isNaN(cardYear) || isNaN(cardMonth)) {
            return false;
        }
        // if not valid month
        if (cardMonth < 1 || cardMonth > 12) {
            return false;
        }
        // get current year and month
        var today = new Date();
        var currYear = today.getFullYear();
        var currMonth = today.getMonth() + 1; // getMonth returns 0-11
        // if card year in the past
        if (cardYear < currYear) {
            return false;
        }
        // if card year is in the future
        if (cardYear > currYear) {
            return true;
        }
        // if same year - month should be >= than current one
        if (cardMonth >= currMonth) {
            return true;
        }
        // invalid
        return false;
    };

    /**
     * Credit card Luhn check
     * @param value
     * @returns {boolean}
     */
    this.luhn = function (value) {
        var nCheck = 0, nDigit = 0, bEven = false;
        value = value.replace(/\D/g, "");
        var n = value.length - 1;
        for (n; n >= 0; n -= 1) {
            var cDigit = value.charAt(n);
            nDigit = parseInt(cDigit, 10);
            if (bEven) {
                if ((nDigit *= 2) > 9) {
                    nDigit -= 9;
                }
            }
            nCheck += nDigit;
            bEven = !bEven;
        }
        return (nCheck % 10) === 0;
    };

    /**
     * Validates the card number
     * @param cardNumber
     * @returns {boolean}
     */
    this.isValidNumber =  function (cardNumber) {
        // empty string
        if (cardNumber === '') {
            return false;
        }
        // Only digits
        if (isNaN(cardNumber)) {
            return false;
        }
        // Luhn check
        if (!this.luhn(cardNumber)) {
            return false;
        }
        // check if VISA
        var validCardNumber = (cardNumber.substring(0, 1) === '4' && cardNumber.length === 16) ? true : false;
        if (validCardNumber) {
            this.cardType = "A_VISA_CREDIT";
            return true;
        }
        // check if MASTERCARD
        var validStart = ["51", "52", "53", "54", "55"];
        validCardNumber = (sb.$.inArray(cardNumber.substring(0, 2), validStart) !== -1 && cardNumber.length === 16) ? true : false;
        if (validCardNumber) {
            this.cardType = "A_MASTERCARD_CREDIT";
            return true;
        }
        // check if MAESTRO
        validStart = ["5018", "5020", "5038", "6304", "6759", "6761", "6762", "6763", "6764", "6765", "6766"];
        validCardNumber = (sb.$.inArray(cardNumber.substring(0, 4), validStart) !== -1 && (cardNumber.length >= 12 && cardNumber.length <= 19)) ? true : false;
        if (validCardNumber) {
            this.cardType = "A_MAESTRO";
            return true;
        }
        // not a valid number
        return false;
    };

    /**
     * Validates if CVN is a number and is 3 chars length
     * @param cvn
     * @returns {boolean}
     */
    this.isValidCVN = function (cvn) {
        // all credit cards except A_MAESTRO require a valid cvn
        var result = true;
        if (this.cardType !== "A_MAESTRO") {
            result = !isNaN(cvn) && cvn.length === 3 ? true : false;
        }
        return result;
    };
};