var sbm = sbm || {};

/**
 * WorldPay token update
 * Used when player updates the card expiry date
 * @param sb The sandbox
 * @constructor
 */
sbm.CardTokenUpdate = function (sb) {

    this.expiryMonth = sb.ko.observable("");
    this.expiryYear = sb.ko.observable("");

    // inherits from CardToken
    this.prototype = new sbm.CardToken();
};

