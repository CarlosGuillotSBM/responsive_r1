var sbm = sbm || {};

/**
 * Holds cashier common data
 * @param sb The sandbox
 * @param source Cashier source (Post Registration/Cashier)
 * @constructor
 */
sbm.CashierData = function (sb, source) {
    "use strict";

    this.skinId = sb.serverconfig.skinId || "";
    this.ip = sb.serverconfig.ip || "";
    this.source = sb.ko.observable(source || "PRD");
    this.device = getDevice();
    this.playerId = sb.getFromStorage("playerId") || "";
    this.ssid = sb.getFromStorage("sessionId") || "";
    this.currencySymbol = sb.getFromStorage("currencySymbol") || "";
    this.canEditBankDetails = sb.ko.observable(false);
    this.pctDepositLimit = sb.ko.observable("");
    this.pctDepositLimitPeriod = sb.ko.observable("");
    this.pctLimitWarning = sb.ko.observable("");
    this.limitWarningPeriod = "";

    this.pctDepositLimit.subscribe(function (newVal) {
        this.setLimitWarning(newVal, this.pctDepositLimitPeriod());
    }.bind(this));

    this.pctDepositLimitPeriod.subscribe(function (newVal) {
        this.setLimitWarning(this.pctDepositLimit() ,newVal);
    }.bind(this));

    this.setLimitWarning = function (pct, period) {

        var pct = parseFloat(pct);

        if (isNaN(pct) || pct < 70 || !period) return;

        if (period === "Day") {
            period = "Daily";
            this.limitWarningPeriod = "daily";
        } else if (period === "Week") {
            period = "Weekly";
            this.limitWarningPeriod = "weekly";
        } else {
            period = "Monthly";
            this.limitWarningPeriod = "monthly";
        }

        this.pctLimitWarning(period + " Limit @ " + pct + "%");
    };

    /**
     * Returns the type of device being used
     * @returns {string} The device
     */
    function getDevice() {

        var deviceCategoryId = sb.serverconfig.device || "1";
        var device = "";

        if (deviceCategoryId === "2") {
            device = "Tablet";
        } else if (deviceCategoryId === "3") {
            device = "Phone";
        } else {
            device = "Web";
        }

        return device;
    }

};