var sbm = sbm || {};

/*
	Module used to manage player verification - Source of Funds (DEV-11151)
*/
sbm.URUDocumentUpload = function (sb) {

	// properties

	// each step will have its own action so we can show/hide on the screen
	this.currentAction = sb.ko.observable("loading");

  this.uiUploadFiles;

  this.uploadedFiles = sb.ko.observableArray([]);

  this.fileTypes = [
    "jpeg",
    "pjpeg",
    "png",
    "pdf"
  ];

  this.mandatory;

  this.numberOfFilesToUpload = 1;

	// initialization - must be called when module is created
	this.init = function () {

		// event listening

		sb.listen("got-documents", this.gotDocuments);
    sb.listen("upload-documents-finished", this.onUploadFilesFinished);

    // get Dom element
    this.uiUploadFiles = document.getElementById("ui-file-uploads");

    // add event to be triggered when player uploads files
    if(this.uiUploadFiles !== null)
      this.uiUploadFiles.addEventListener("change", this.onFileUpload);

    // retrieve list of all documents that can be uploaded
    sb.notify("get-documents");
	};

	// destruction - must be called when module is destroyed
	this.destroy = function () {

		sb.ignore("got-documents");
    sb.ignore("upload-documents-finished");
	};

  /************************************************************************************************
                                          Files Upload
  *************************************************************************************************/

  // when player uploads files
  this.onFileUpload = function (input) {

    // validation

    var files = input.target.files;

    for (var i = 0; i < files.length; i++) {
      if (!this.validFileType(files[i].type)) {
        sb.showError("File type '" + files[i].type +"' is not valid. Please remove it and try again.");
        return;
      }
    }

    // all good - add files to the list - append if there's any

    var list = this.uploadedFiles();
    for (var i = 0; i < files.length; i++) {
      list.push(files[i]);
    }
    this.uploadedFiles(list);

  }.bind(this);

  // remove file from the list of uploads
  this.removeFile = function (data) {
  
    var filename = data.name;

    // remove file from array
    var newList = sb._.reject(this.uploadedFiles(), function (file) {
      return file.name === filename;
    });

    this.uploadedFiles(newList);

  }.bind(this);

  // validates if file type is correct
  this.validFileType = function (file) {
    for(var i = 0; i < this.fileTypes.length; i++) {
      // we only consider the extension and not the full type
      if(file.indexOf(this.fileTypes[i]) > -1) {
        return true;
      }
    }
  };

  // validates the size of the files
  // returns true if all good otherwise false
  // limit: 10 MB (10485760 bytes) overall
  this.sizeValidation = function () {

    var files = this.uploadedFiles(),
        total = 0;

    for (var i = 0; i < files.length; i++) {
      total += files[i].size;
    }

    if (total < 10485760) return true;

    return false;
  };

  // upload file
  this.upload = function () {

    // size validation
    
    
    if (this.sizeValidation()) {
      
      if (this.numberValidation()) {
        sb.notify("upload-documents", [this.uploadedFiles(), sb.getFromStorage("playerId"), sb.getFromStorage("sessionId")]);
      
      } else {

        sb.showError("At least " + this.numberOfFilesToUpload + " documents need to be uploaded");        
      }

    } else {

      sb.showError("Upload failed. Total file size exceeds 10MB.");
    }

  }.bind(this);

  this.numberValidation = function () {

    if (this.mandatory) {
      return this.uploadedFiles().length >= this.numberOfFilesToUpload;
    }

    return this.numberOfFilesToUpload > 0;

  };

  // files have been uploaded
  this.onUploadFilesFinished = function (e, data) {

    if (data.Code === 0) {
      
      var c = function () {
        location.pathname = "/start/";
      }
      
      // remove from storage information regarding the SOF
      sb.removeFromStorage("verifyAccount");
      sb.removeFromStorage("verifyAccountPopUp");

      // successful notification
      sb.showSuccess("Submit successful. We will be in touch shortly to inform you whether your documents have been successfully validated.", c);
      sb.setCookie("URUVerified",3);

    } else {

      sb.showError("Upload failed, please try again or contact our support team.");
      this.uploadedFiles([]);
    }
  }.bind(this);

};