var sbm = sbm || {};

/**
* Responsible for handling the T&Cs and Privacy Policy popup
* @param sb The sandbox
* @constructor
*/
sbm.TermsService = function (sb, options) {
    "use strict";
    
    // subscriptions
    
    // ticked agree with Tcs
    this.onAgreeTCs = function (newVal) {
        if (newVal) {
            
            if (this.hasPP() && !this.agreePP()) {
                
                this.canContinue(false);
            } else {
                if(this.isAspersPlayer && this.agreeData() === false){
                    this.canContinue(false);
                }
                else{
                    this.canContinue(true);                
                }           
            }
            
        } else {
            this.canContinue(false);
        }
    }.bind(this);
    
    // ticked agree with PP
    this.onAgreePP = function (newVal) {
        if (newVal) {
            
            if (this.hasTCs() && !this.agreeTcs()) {
                
                this.canContinue(false);
            } else {
                if(this.isAspersPlayer && this.agreeData() === false){
                    this.canContinue(false);
                    console.log("No data");
                }
                else{
                    this.canContinue(true);                
                }           
            }
            
        } else {
            this.canContinue(false);
        }
    }.bind(this);
    
    this.onAgreeData = function (newVal) {
        if (newVal) {
            if (this.hasTCs() && !this.agreeTcs()) {
                
                this.canContinue(false);
            } else {
                    this.canContinue(true);   
            }
            
        } else {
            this.canContinue(false);
        }
    }.bind(this);
    
    // properties
    this.session = "";
    this.playerID = "";
    this.popupVisible = sb.ko.observable(false);
    this.tcsId = sb.ko.observable("");
    this.tcs = sb.ko.observable("");
    this.ppId = sb.ko.observable("");
    this.pp = sb.ko.observable("");
    this.hasTCs = sb.ko.observable(false);
    this.hasPP = sb.ko.observable(false);
    this.hasCOM = sb.ko.observable(false);
    this.agreeTcs = sb.ko.observable(false);
    this.agreeTcs.subscribe(this.onAgreeTCs);
    this.agreePP = sb.ko.observable(false);
    this.agreePP.subscribe(this.onAgreePP);

    this.agreeEmail = sb.ko.observable(false);
    this.agreeSMS = sb.ko.observable(false);
    this.agreePhone = sb.ko.observable(false);

    this.agreeTPEmail = sb.ko.observable(false);
    this.agreeTPSMS = sb.ko.observable(false);
    this.agreeTPPhone = sb.ko.observable(false);
    this.agreeData = sb.ko.observable(false);
    this.agreeData.subscribe(this.onAgreeData);
    this.isAspersPlayer = sb.ko.observable(false);
    this.canContinue = sb.ko.observable(false);
    
    // module initialization
    this.init = function () {
        sb.listen("show-terms-popup", this.onShowTermsPopup);
        sb.listen("got-tcs-pp", this.onGotTCsPopup);
        sb.listen("got-aspers-tc-pp", this.onGotAspersTcPp);
        sb.listen("tcs-pp-accepted", this.onTCsPPAccepted);
    };
    
    // module destruction
    this.destroy = function () {
        sb.listen("show-terms-popup");
        sb.listen("got-tcs-pp");
        sb.listen("tcs-pp-accepted");
        sb.listen("got-player-entity");
    };
    
    // player needs to accept TCs and PP
    this.onShowTermsPopup = function(e, session, playerID, aspers) {
        
        playerID = (typeof playerID !== 'undefined') ?  playerID : 0;
        aspers = (typeof aspers !== 'undefined') ?  aspers : 0;
        // keep session in memory so we can reuse later
        this.session = session;
        this.playerID = playerID;
        this.isAspersPlayer=aspers;
        if(aspers == 0){
        // TC's and PP need to retrieved from backend
            sb.notify("get-tcs-pp", [session]);
        }
        else{
            sb.notify("get-aspers-tcs-pp", [session]);
        }
        // popup must be displayed    
        this.popupVisible(true);
        
    }.bind(this);
    
    // got TCs and PP from backend
    this.onGotTCsPopup = function(e, data) {
        
        if (data.tcs) {
            this.tcsId(data.tcsId);
            this.tcs(data.tcs);
            this.hasTCs(true);
        }
        if (data.pp) {
            this.ppId(data.ppId);
            this.pp(data.pp);
            this.hasPP(true);  
        }
        
    }.bind(this);
    
    this.onGotAspersTcPp = function(e, data) {   
        this.tcsId(data.tcID);
        this.tcs("");
        this.hasTCs(true);
        this.ppId(data.ppID);
        this.pp("");
        this.hasPP(true);  
        this.hasCOM(true); 
        this.popupVisible(true);  
    }.bind(this);
    
    // agreed with terms
    this.continue = function () {
        
        // player agrees?
        
        var agrees = true;
        
        if (this.hasTCs() && !this.agreeTcs()) {
            agrees = false;
        }
        
        if (this.hasPP() && !this.agreePP()) {
            agrees = false;
        }
        
        if (agrees) {  
            if(this.isAspersPlayer){
                var aspersCustomerNumber = sb.getFromStorage("aspersCustomerNumber")
                sb.notify("aspers-do-first-login", [
                    this.playerID,
                    this.agreeEmail,
                    this.agreeSMS,
                    this.agreePhone,
                    this.agreeData,
                    this.agreeTPEmail,
                    this.agreeTPSMS,
                    this.agreeTPPhone,
                    aspersCustomerNumber]);
                sb.notify("tcs-pp-accept", [this.tcsId(), this.ppId()]);
            }
            else{
                sb.notify("tcs-pp-accept", [this.tcsId(), this.ppId()]);
                // player accepts TCs and PP
            }
            
        }
        
    }.bind(this);
    
    // player does not agree - dismiss pop-up (***** This was removed for now *****)
    // this.doNotAgree = function () {
    
    //     window.location.pathname = "/";
    
    // }.bind(this);
    
    // response to the accept terms
    this.onTCsPPAccepted = function (e, data) {
        // sucess
        if (data.Code === 0) {
            
            // restore the session on coookie and load start page
            this.popupVisible(false);
            sb.setCookie("SessionID", this.session);
            if(this.isAspersPlayer){
                sb.initRoute("/welcome/");
            }
            else{
                sb.initRoute("/start/");
            }
        }
        
    }.bind(this);
    
    
};
