var sbm = sbm || {};

/**
* VIP pre-registration landing page
* Validates VIP and redirects to registration page
*
* Notes: 
* IG stores date of birth as yyyy-mm-ddThh:mm:ss
*
* @param sb The Sandbox
* @param options JSON object with any options for the viewmodel
* @returns {PotentialPlayers} viewmodel PotentialPlayers
*/
sbm.VIPLanding = function (sb, options) {

	// properties
	this.promoCode = new sb.ko.observable("");
	this.dayOfBirth = new sb.ko.observable("");
	this.monthOfBirth = new sb.ko.observable("");
	this.yearOfBirth = new sb.ko.observable("");
	this.error = new sb.ko.observable("");

	// module initialization
	this.init = function () {

		sb.showLoadingAnimation();

		sb.listen("got-customer-number", this.onGotCustomerNumber);
		sb.listen("got-customer", this.onGotCustomer);
	};

	// module destruction
	this.destroy = function () {
		sb.ignore("got-customer-number");
	};

	// form submission validation
	this.isValid = function () {
		
		// promo code

		var promoCode = this.promoCode().substr(2);
		
		if (!promoCode || isNaN(promoCode)) {
			return false;
		}

		// day of birth
		
		var day = this.dayOfBirth();
		
		if (!day || isNaN(day) || day < 1 || day > 31) {
			return false;
		}

		// month

		var month = this.monthOfBirth();

		if (!month || isNaN(month) || month < 1 || month > 12) {
			return false;
		}

		// year

		var year = this.yearOfBirth();

		if (!year || isNaN(year) || year < 1900 || year > 2000) {
			return false;
		}

		return true;
	};

	// Submit form - get customer number from our system
	this.submit = function () {
		
		if (this.isValid()) {

			// call IG API to get the player's details
			// promocode format: ASXXXXXX where XXXXX is the customer number which is what we need to send
        	sb.notify("get-customer", [this.promoCode().substr(2)]);

		} else {
			this.error("Please verify that the data entered is correct");
		}
	}.bind(this);

	// got the customer data
	this.onGotCustomer = function (e, data) {
		if (data.Code === 0) {
        
        	// successful response

        	// validate the customer's date of birth
        	if (data.Customer && data.Customer.DateOfBirth) {

        		// extract DOB

        		var customerDayOfBirth = data.Customer.DateOfBirth.substr(8,2);
	        	var customerMonthOfBirth = data.Customer.DateOfBirth.substr(5,2);
	        	var customerYearOfBirth = data.Customer.DateOfBirth.substr(0,4);

	        	// if date of birth provided by player matches from IG response
	        	if (customerDayOfBirth == this.dayOfBirth() &&
	        		 customerMonthOfBirth == this.monthOfBirth() &&
	        		  customerYearOfBirth == this.yearOfBirth()) {	        		

			        // add the Promo Code to the received data
		        	data.Customer.Promocode = this.promoCode();

		        	// save customer data to be used later on the registration page
		        	sb.saveOnStorage("AspersCustomer", JSON.stringify(data.Customer));

		        	// get the last segment of the current page
		        	var landingPage = location.href.match(/([^\/]*)\/*$/)[1];

		        	// send to the correspondent registration page i.e: registration/hello
		        	location = "/register/" + landingPage;

	        	} else {

					sb.showError("Something went wrong, please verify that the data entered is correct and try again.");
	        	}
        	} else {

				sb.showError("Sorry but we couldn't verify your date of birth.");
        	}

        } else {
            data.Msg && sb.showError(data.Msg);
        }
	}.bind(this);
};