var sbm = sbm || {};

sbm.PrizeWheel = function(sb, options){
    
    "use strict";
    this.displayPrizeWheel = sb.ko.observable(false);    
    this.spinNumber = sb.ko.observable("0");
    this.playerID = sb.getCookie("PlayerID");
    this.sessionID = sb.getCookie("SessionID");
    this.prizeWheelURL = sbm.serverconfig.prizeWheelURL +"?SkinID=" + sbm.serverconfig.skinId;

    // Start listening for requests on tokens
    this.init = function () {
        sb.listen("prize-wheel-show-wheel", this.onShowWheel);
        sb.listen("prize-wheel-hide-wheel", this.onHideWheel);
        sb.listen("prize-wheel-got-prize-info", this.onGotPrizeInfo);
        sb.listen("done-login", this.onDoneLogin);
    };
    // When the module is destroyed stop listening for events.
    this.destroy = function () {
        sb.ignore("prize-wheel-got-prize-info");
        sb.ignore("prize-wheel-show-wheel");
        sb.ignore("prize-wheel-hide-wheel");
        sb.ignore("done-login");
    };
    this.onGotPrizeInfo = function(e, data){
        if(!sb.validSession()){
            this.displayPrizeWheel(false);
        }
        else{
            if(data.Spins > 0){
                sb.setCookie("PWSpins",data.Spins);
                this.spinNumber(data.Spins);
                this.onShowWheel();
            }
            else{
                sb.setCookie("PWSpins",0);
            }
        }
    }.bind(this);

    this.onDoneLogin = function(){
        sb.$(".prize-wheel__notification__icon.hide-for-large-up")[0].href = this.prizeWheelURL + "&uid=" + sb.getCookie("PlayerID") +"&guid=" + sb.getCookie("SessionID") +"&gameid=531&HomeURL=" + sb.serverconfig.baseUrl;
    }.bind(this);
    /* These two functions are for debugging only, maybe we should remove them when we launch this. */
    this.onShowWheel = function(e){
        this.displayPrizeWheel(true);
    }.bind(this);
    this.onHideWheel = function(e){
        this.displayPrizeWheel(false);
    }.bind(this);
};

$(window).focus(function() {
    sbm.core.publish("prize-wheel-check-for-prizes");
});