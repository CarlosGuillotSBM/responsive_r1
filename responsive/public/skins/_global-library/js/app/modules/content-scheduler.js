var sbm = sbm || {};

/**
 * Module responsible for displaying
 * scheduled content depending on some restrictions
 * @param sb The sandbox
 * @param options Options if any
 * @constructor
 */
sbm.ContentScheduler = function (sb, options) {

    "use strict";

    this.scheduledContent = sb.ko.observableArray([]);
    this.days = ["SUN","MON","TUE","WED","THU","FRI","SAT","SUN"];
    this.today = new Date();
    this.day = this.days[this.today.getDay()];
    this.hour = this.today.getHours();
    this.playerClassId = null;       
    this.depositCount = -1;
    this.visibleElements = [];
    this.playerLoggedIn = false;
    // we could create a validation array here of "Validated" objects if in the future this grows
    this.classChecked = false;
    this.classValidated = true;
    this.timeChecked = false;
    this.timeValidated = true;
    this.depositsChecked = false;
    this.depositsValidated = true;

    this.checkDayTime = function (dateTime) {

        this.timeChecked = true;
        return this.checkDay(dateTime.substring(0,3)) && this.checkTime(dateTime.substring(3));

    }.bind(this);

    this.checkDay = function(today) {

        return today.toUpperCase() === this.day;

    }.bind(this);

    this.checkTime = function(time) {
        
        var hours = time.replace(/[\(\)]/g,"").split("-");

        return (time == "" ? true : (hours[0] ? (hours[0] <= this.hour && hours[1] > this.hour) : false));

    }.bind(this);

    /**
    /* Checks the number of deposits of the player. example DEP2 (just with 2 deposits) DEP2+ (two or more deposits)
    */
    this.checkDeposits = function(deposits) {
        this.depositsChecked = true;
        // we split the string using + so depAux[0] will be always 0
        var depAux = deposits.substring(3).split("+");
        if (depAux.length > 1) {
            // if exists then it means there was a + so we compare using >=
            return parseInt(this.depositCount) >= parseInt(depAux[0]);
        } else {
            return parseInt(this.depositCount) == parseInt(depAux[0]);
        }

    }.bind(this);

    /**
    /* Checks the player class of the player to decide whether to display the banner or not. Example VIPSILVER, VIPGOLD
    */
    this.checkPlayerClass = function(playerClass) {
                   
        this.classChecked = true;
        return this.playerClassId !== null && playerClass.substring(3).toUpperCase() === sb.getPlayerClassName(this.playerClassId).toUpperCase();

    }.bind(this);

    this.isContentVisible = function(rulesString) {

        // we restart the visibility per every element
        this.classChecked = false;
        this.classValidated = true;
        this.timeChecked = false;
        this.timeValidated = true;
        this.depositsChecked = false;
        this.depositsValidated = true;
        this.loginStateValidated = true;
        
        // First of all, if there's no rules. The banner will be within the banner to be displayed group, no matter what
        if (rulesString == "") return true;

        var rules = rulesString.replace(/\s/g,"").split(",");
        var i = 0;
        var rule = "";
        while (rules[i]){

            rule = rules[i].substring(0,3);
            switch (rule) {
                case "DEP":
                    if (!this.depositsChecked || !this.depositsValidated) {
                        this.depositsValidated = this.checkDeposits(rules[i]);
                    }

                break; 
                case "VIP":
                    if (!this.classChecked || !this.classValidated) {
                        this.classValidated = this.checkPlayerClass(rules[i]);
                    }

                break; 
                case "OUT":

                    if (sb.getFromStorage("sessionId")) {
                        // If the player is logged in and it finds the OUT tag it hides it
                        this.loginStateValidated = false;
                    }

                break; 
                default:
                    // it might be a day of the week
                    if (!this.timeChecked || !this.timeValidated) {
                        this.timeValidated = this.checkDayTime(rules[i]);
                    }

                break;                 
            }


            i++;
        }

        return this.timeValidated && this.classValidated && this.depositsValidated && this.loginStateValidated;      

    }.bind(this);

    /**
     * Module initialization
     */
    this.init = function () {

        sb.listen("done-login", this.refreshBanners);
        this.refreshBanners();
    };

    /* Module to refresh the displayed content */
    this.refreshBanners = function (e, data) {   
        sb.$('.ui-scheduled-content-container .ui-scheduled-content').hide();     
        if (sb.getFromStorage("sessionId")) {
            this.playerLoggedIn = true;
            if (data) {
                this.playerClassId = data.Class;
                this.depositCount = data.DepositCount;
            } else {
                this.playerClassId = sb.getFromStorage("class");
                this.depositCount = sb.getFromStorage("depositCount");
            }
        };
        this.displayActiveBanners(); 
    }.bind(this);
   
    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("done-login");
    };

    

    this.displayActiveBanners = function () {
        var defaultElements = [];
        sb.$('.ui-scheduled-content-container').each(function (index,e) {
            this.visibleElements = [];
            sb.$(e).find('.ui-scheduled-content').each(function(i,element) {
                if (this.isContentVisible(sb.$(element).attr('data-scheduled-rules'))) {
                    this.visibleElements.push(element);
                }
            }.bind(this));
            defaultElements = sb.$(e).find('.ui-scheduled-default');
            if (options && options.showAllElements){
                if (this.visibleElements.length == 0 && defaultElements && defaultElements.length > 0) {
                    sb.$(defaultElements).each( function (index, e) {
                        e.show();
                    });
                } else if (this.visibleElements.length > 0) {
                    sb.$(this.visibleElements).each( function (index, e) {
                        e.show();
                    }); 
                }                
            } else {
                if (this.visibleElements.length == 0 && defaultElements && defaultElements.length > 0) {
                    sb.$(defaultElements[Math.floor(defaultElements.length * Math.random())]).show();
                } else if (this.visibleElements.length > 0) {
                    sb.$(this.visibleElements[Math.floor(this.visibleElements.length * Math.random())]).show(); 
                }
            }
        }.bind(this));
    }.bind(this);

};