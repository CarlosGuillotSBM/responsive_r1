var sbm = sbm || {};

/**
 * Module Register
 * This module is responsible for providing the viewmodel for the Register form.
 * @param sb The Sandbox
 * @param options JSON object with any options for the viewmodel
 * @returns {Register} viewmodel Register
 * @constructor
 */
sbm.Register = function (sb, options) {
    "use strict";

    var vm = this;

    var rules = getRules();

    // viewmodel properties

    vm.address1 = sb.ko.observable("");
    vm.address2 = sb.ko.observable("");
    vm.addresses = sb.ko.observableArray([]);
    vm.city = sb.ko.observable("");
    vm.countries = sb.ko.observableArray([]);
    vm.state = sb.ko.observable("");
    vm.currencies = sb.ko.observableArray([]);
    vm.dayOfBirth = sb.ko.observable("");
    vm.destroy = destroy;
    vm.doRegistration = doRegistration;
    vm.email = sb.ko.observable("");
    vm.emailNotAvailable = sb.ko.observable("");
    vm.errors = null;
    vm.findUs = sb.ko.observable("");
    vm.firstName = sb.ko.observable("");
    vm.hasPromoCode = sb.ko.observable(false);
    vm.init = init;
    vm.isValidUkPostcode = sb.ko.observable(false);
    vm.landline = sb.ko.observable("");
    vm.lastName = sb.ko.observable("");
    vm.mobilePhone = sb.ko.observable("");
    vm.monthOfBirth = sb.ko.observable("");
    vm.over18 = sb.ko.observable(false);
    vm.DOBOver18 = sb.ko.observable(false);
    vm.password = sb.ko.observable("");
    vm.postcode = sb.ko.observable("");
    vm.postcodeIsValid = sb.ko.observable("");
    vm.postcodeLookup = postcodeLookup;
    vm.promoCode = sb.ko.observable("");
    vm.selectedAddress1 = sb.ko.observable("");
    vm.selectedCountry = sb.ko.observable("");
    vm.selectedCurrency = sb.ko.observable("");
    vm.selectedYearOfBirth = sb.ko.observable("");
    vm.showPassword = sb.ko.observable(false);
    vm.showPromoCode = showPromoCode;
    vm.title = sb.ko.observable("");
    vm.username = sb.ko.observable("");
    vm.usernameNotAvailable = sb.ko.observable("");
    vm.wantsEmail = sb.ko.observable(true);
    vm.wantsSMS = sb.ko.observable(true);
    vm.wantsPost = sb.ko.observable(true);
    vm.wantsPhone = sb.ko.observable(true);
    vm.years = sb.ko.observableArray(getYears());
    vm.raf = "";
    vm.totalSteps = sb.ko.observable(((options && options.registrationSteps) ? options.registrationSteps : 0));
    vm.goToStep = goToStep;
    vm.offlineCasinos = sb.ko.observableArray([
        "None",
        "Milton Keynes", 
        "Newcastle",
        "Northampton",
        "Westfield Stratford City"
    ]);
    vm.offlineCasino = sb.ko.observable("");
    vm.aspersCustomer = sb.ko.observable("");
    vm.customer = false;
    vm.loading = sb.ko.observable(false);

    // 3 step registration
    vm.nextStep = nextStep;
    vm.previousStep = previousStep;
    vm.currentStep = sb.ko.observable(vm.totalSteps() === 0 ? 0 : 1);

    vm.currentValidationRules;

    vm.validations = {
        0: function () {
            vm.title.extend(rules.title);
            vm.firstName.extend(rules.firstName);
            vm.lastName.extend(rules.lastName);
            vm.postcode.extend(rules.postcode).extend({ rateLimit: 500 });
            vm.address1.extend(rules.address1);
            vm.city.extend(rules.city);
            vm.email.extend(rules.email);
            vm.username.extend(rules.username);
            vm.password.extend(rules.password);
            vm.over18.extend(rules.over18);
            vm.selectedCurrency.extend(rules.currency);
            vm.selectedCountry.extend(rules.country);
            vm.promoCode.extend(rules.promoCode);
        },
        1: function () {
            vm.title.extend(rules.title);
            vm.firstName.extend(rules.firstName);
            vm.lastName.extend(rules.lastName);
            vm.email.extend(rules.email);
            vm.username.extend(rules.username);            
            vm.password.extend(rules.password);            

            vm.DOB.extend(rules.dateOfBirth);
            validateDOBOver18();
            vm.mobilePhone.rules.removeAll();

        },
        2: function () {
         
            vm.selectedCurrency.extend(rules.currency);
                 vm.selectedCountry.extend(rules.country);
                       vm.landline.rules.removeAll();
            vm.mobilePhone.rules.removeAll();
            if (vm.selectedCountry() === "GBR") {
                vm.landline.extend(rules.landlineUK);
                vm.mobilePhone.extend(rules.mobilePhone);
            } else {
                vm.landline.extend(rules.landline);
                vm.mobilePhone.extend(rules.mobilePhone);
            }
        
            vm.postcode.extend(rules.postcode).extend({ rateLimit: 500 });
            vm.address1.extend(rules.address1);
            vm.city.extend(rules.city);
       
            vm.state.extend(rules.state);

        },
        3: function () {
            vm.promoCode.extend(rules.promoCode);
            vm.DOBvOver18.extend(rules.over18);
        },
        dateOfBirth:  function () {
            vm.dayOfBirth.extend(rules.dayOfBirth);
            vm.monthOfBirth.extend(rules.monthOfBirth);
            vm.selectedYearOfBirth.extend(rules.selectedYearOfBirth);

        },
        aspersCustomer: function () {
            vm.dayOfBirth.extend(rules.dayOfBirth);
            vm.monthOfBirth.extend(rules.monthOfBirth);
            vm.selectedYearOfBirth.extend(rules.selectedYearOfBirth);

            vm.email.extend(rules.email);
            vm.firstName.extend(rules.firstName);
            vm.promoCode.extend(rules.promoCode);
            vm.lastName.extend(rules.lastName);
            vm.postcode.extend(rules.postcode).extend({ rateLimit: 500 });
            vm.mobilePhone.extend(rules.mobilePhone);
            vm.title.extend(rules.title);
        }
    };

    function validateDOBOver18() {
        if (vm.dayOfBirth().length === 1 && parseInt(vm.dayOfBirth()) < 10) {
            vm.dayOfBirth("0"+vm.dayOfBirth()); 
        }
        if (vm.monthOfBirth().length === 1 && parseInt(vm.monthOfBirth()) < 10) {
            vm.monthOfBirth("0"+vm.monthOfBirth()); 
        }        
        var today = new Date();
        var age = today.getFullYear() - vm.selectedYearOfBirth();
        if (today.getMonth() < vm.monthOfBirth() || (today.getMonth() == vm.monthOfBirth() && today.getDate() < vm.dayOfBirth())){age--;}
        (age >= 18) ? vm.DOBOver18(false) : vm.DOBOver18(true);
    }

    function updateOver18() {
        return vm.over18();
    }
    // viewmodel computed

    vm.DOB = new sb.ko.computed(getDOB);

    vm.DOBvOver18 = new sb.ko.computed(updateOver18);

    vm.showFindAddress = new sb.ko.computed(canShowFindAddress);

    // viewmodel subscriptions

    vm.email.subscribe(onEmailChanged);
    vm.username.subscribe(onUsernameChanged);
    vm.postcode.subscribe(onPostcodeChanged);
    vm.selectedAddress1.subscribe(onSelectedAddress1Changed);
    vm.selectedCountry.subscribe(onSelectedCountry);
    vm.dayOfBirth.subscribeChanged(onDayOfBirthChanged);
    vm.monthOfBirth.subscribeChanged(onMonthOfBirthChanged);
    vm.selectedYearOfBirth.subscribeChanged(onSelectedYearOfBirthChanged);


    // properties not included on viewmodel

    var REGISTER_USERNAME_MAX_CHECKS_REACHED = -8;
    var REGISTER_USERNAME_UNAVAILABLE = 1;
    var REGISTER_USERNAME_BANNED = 2;
    var REGISTER_EMAIL_MAX_CHECKS_REACHED = -8;
    var REGISTER_EMAIL_UNAVAILABLE = 1;
    var REGISTER_EMAIL_BANNED = 2;
    var maxUsernameChecks = false;
    var maxEmailChecks = false;
    var groupId = null;
    var regSessionId = null;

    // all the functions

    /**
     * Makes visible/invisible the password
     */
    this.togglePassword = function () {
        if (this.showPassword() == true) {
            this.showPassword(false);
            sb.$(".show-password").removeClass("eye-open");
            sb.$(".show-password").addClass("eye-shut");
        } else {
            this.showPassword(true);
            sb.$(".show-password").removeClass("eye-shut");
            sb.$(".show-password").addClass("eye-open");
        }
    }.bind(this);

    /**
    * Check if
    * 
    */
    function onDayOfBirthChanged(newVal,oldVal) {
       if (newVal > 31) {
            vm.selectedYearOfBirth.extend(rules.yearOfBirth);
            if (vm.errors().length !== 0) {
                vm.errors.showAllMessages();
            }
            vm.dayOfBirth(oldVal);
       } else 
       if (newVal.length == 2) {
          sb.$('#dayOfBirth').nextAll('.fdatepicker:first').focus();
        }
    }
    function onMonthOfBirthChanged(newVal,oldVal) {
       if (newVal > 12) {            
            vm.selectedYearOfBirth.extend(rules.yearOfBirth);
            if (vm.errors().length !== 0) {
                vm.errors.showAllMessages();
            }
            vm.monthOfBirth(oldVal);
       } else 
       if (newVal.length == 2) {
          sb.$('#monthOfBirth').nextAll('.fdatepicker:first').focus();
        }
    }

   function onSelectedYearOfBirthChanged(newVal,oldVal) {
       if (typeof newVal === 'undefined') {
          return;
       }
       // if player is 18+
       if (newVal > ((new Date().getFullYear())-18)) {          
            
       }  else 
            // if year is greater than 1900
            if (newVal <= 1899) {
                vm.selectedYearOfBirth.extend(rules.yearOfBirth);
                if (vm.errors().length !== 0) {
                    vm.errors.showAllMessages();
                }    
            }

        if (newVal.length == 4) {
          sb.$('#yearOfBirth').nextAll('.fdatepicker:first').focus();
        }
    }

    /**
     * Check if post code is valid for search
     * based on: https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/283357/ILRSpecification2013_14Appendix_C_Dec2012_v1.pdf
     * @returns boolean
     */
    function canShowFindAddress() {

        // validates country
        if (vm.selectedCountry() !== "GBR") {
            return false;
        }

        // validates postcode

        var i = 0,
            regex,
            postCode = vm.postcode().toUpperCase();

        //remove empty spaces
        postCode = postCode.replace(/ /g, "");

        // validates inward

        // lenght
        var incode = postCode.substr(-3);
        if (incode.length !== 3) {
            return false;
        }
        // NAA
        regex = /^[0-9]{1}[A-z]{2}$/g;
        if (!incode.match(regex)) {
            return false;
        }
        // illegal characters C I K M O V
        var illegalChars = ["C", "I", "K", "M", "O", "V"];
        for (i; i < illegalChars.length; i++) {
            if (incode.indexOf(illegalChars[i]) > -1) {
                return false;
            }
        }

        // validates outcode
        var outcode = postCode.substr(0, postCode.length - 3);
        regex = /^[A-z]{1}[0-9]{1}|[A-z]{1}[0-9]{2}|[A-z]{2}[0-9]{1}|[A-z]{2}[0-9]{2}|[A-z]{1}[0-9]{1}[A-z]{1}|[A-z]{2}[0-9]{1}[A-z]{1}$/g;
        if (!outcode.match(regex)) {
            return false;
        }

        // VALID
        vm.postcode(postCode); //  setting the correct format to send on service (no spaces and uppercase)

        return true;
    }

    /**
     * Get date of birth
     * @returns YearMonthDay
     */
    function getDOB() {
        var dob;
        // We receive the date of birth differently whether it's a 3 or 1 step registration
        dob = vm.dayOfBirth() + '/' + vm.monthOfBirth() + '/' + vm.selectedYearOfBirth();
        return dob;
    }

    /**
     * Get validation rules for each field
     */
    function getRules() {
        return {
            dayOfBirth: {
                required: true,
                max: {
                    params: 31,
                    message: 'Add the leading 0'
                }
            },
            monthOfBirth: {
                required: true,
                max: {
                    params: 12,
                    message: 'Add the leading 0'
                }
            },
            yearOfBirth: {
                required: {
                    params: true
                },
                max: {
                    params: 2016
                },
                min: {
                    params: 1900,
                    message: 'Please enter a valid year of birth'
                }
            },
            address1: {
                required: true
            },
            city: {
                required: true
            },
            state: {
                required: true
            },
            country: {
                required: true
            },
            currency: {
                required: true
            },
            dateOfBirth: {
                required: true,
                DOB: true
            },
            email : {
                required: true,
                email: true
            },
            firstName: {
                required: true,
                pattern: /^[a-zA-Z ]+$/
            },
            lastName: {
                required: true,
                pattern: /^[a-zA-Z ]+$/
            },
            promoCode: {
                pattern: {
                    params: /^[a-zA-Z0-9]+$/,
                    message: "Promo contains invalid characters"
                }
            },
            mobilePhone: {
                required: true,
                intlPhoneNumber: 'ui-phone'
            },
            over18: {
                equal: {
                    params: true,
                    message: "You must agree with the Terms & Conditions"
                }
            },
            password: {
                required: true,
                minLength: 5,
                maxLength: 15,
                pattern: {
                    params: /^(?=.*?\d)(\w|[!@\^#\$%\&\*])+$/i,
                    message: "Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces."
                },
                notEqual: {
                    params: function () { return vm.username(); },
                    message: "Username and Password cannot be the same"
                }
            },
            postcode: {
                required: true,
                minLength: 4
            },
            title: {
                required: true
            },
            username : {
                required: true,
                minLength: 3,
                maxLength: 15,
                notEqual: {
                    params: function () { return vm.password(); },
                    message: "Username and Password cannot be the same"
                },
                pattern: {
                    params: /^[a-z]\w+$/i,
                    message: "Username contains invalid characters"
                }
            }
        };
    }

    /**
     * Module destruction
     */
    function destroy() {

        sb.ignore("got-register-session");
        sb.ignore("checked-register-username");
        sb.ignore("checked-register-email");
        sb.ignore("got-postcode-lookup");
        sb.ignore("registered-player");
        sb.ignore("done-login-after-registration");

    }

    /**
     * Player registration action
     */
    function doRegistration() {

        // add validations for computed observables
        // needs to be here or they would appear right away


        //vm.selectedCurrency.extend(rules.currency);
        //vm.selectedCountry.extend(rules.country);

        if (vm.currentStep() === 0) {
            vm.DOB.extend(rules.dateOfBirth);
        }
        vm.validations[vm.currentStep()]();
        if (vm.errors().length === 0) {
            register();
        } else {
            removeBorders();
            vm.errors.showAllMessages();
            addBorders();
            var scrollUpTo = sb.$(".error:visible").first();
            if (scrollUpTo && sbm.serverconfig.device === 3) {
                $("html, body").animate({ scrollTop: scrollUpTo.position().top - 120 }, 1000);
            }
        }
    }

    /**
     * Player goes to previous registration step
     */
    function previousStep() {

        if (vm.currentStep() === 1) {
            window.location.href = '/'; 
        } else {
            vm.goToStep(vm.currentStep()-1);
        }       

    }

    /**
     * Player goes to next registration step
     */
    function nextStep() {

        vm.goToStep(vm.currentStep()+1);       

    }  



    /**
     * Player next registration step
     */
    function goToStep(toStep) {
        var step = 3;
        removeBorders();
        for (var step = 1; step < toStep; step++) {
            vm.validations[step](); 
        }    
        //vm.validations[vm.currentStep()]();

        if (vm.errors().length === 0 && vm.emailNotAvailable() === "" && vm.usernameNotAvailable() === "") {
            removeBorders();
            vm.currentStep(toStep);
            window.scrollTo(0, 0);           
        } else {
            vm.errors.showAllMessages();
            addBorders();
        }
    }

    function removeBorders() {
        sb.$(".border-green").removeClass("border-green");
        sb.$(".border-green-icon").removeClass("border-green-icon");
        sb.$(".border-red").removeClass("border-red");
        sb.$(".border-red-icon").removeClass("border-red-icon");
    }

    function addBorders() {
        sb.$("span.error:hidden").siblings("input,select").addClass('border-green');
        sb.$("span.error:hidden").siblings("div#icon").addClass('border-green-icon');
        sb.$("span.error:visible").siblings("input,select").addClass('border-red');
        sb.$("span.error:visible").siblings("div#icon").addClass('border-red-icon');
    }

    function removeBordersByElement(element) {
        sb.$(element).removeClass("border-green border-red");
        sb.$(element).siblings("#icon").removeClass("border-green-icon border-red-icon");
    }

    function addBordersByElement(element) {
        if (sb.$(element).siblings("span.error").is(":visible")) {
            sb.$(element).addClass('border-red');
            sb.$(element).siblings("div#icon").addClass('border-red-icon');
        } else if (sb.$(element).val() !== "") {
            sb.$(element).addClass('border-green');
            sb.$(element).siblings("div#icon").addClass('border-green-icon');
        }
    }

    function detectBordersWhenFocusChange() {
        $("input,select").on("focusout", function(){
            if (["ui-email", "ui-username"].indexOf($(this).attr("id")) > -1) {
                setTimeout(function() {
                    removeBordersByElement(this);
                    addBordersByElement(this);
                }.bind(this),1000);
            } else {
                removeBordersByElement(this);
                addBordersByElement(this);
            }
        });
    }

    /**
     * Get years to show on dropdown
     * @returns {Array}
     */
    function getYears() {
        var today = new Date();
        var currYear = today.getFullYear();
        var years = [];
        var maxYear = currYear - 18;
        years.push({ val: maxYear, desc: maxYear });
        var i;
        for (i = 0; i < 94; i++) {
            maxYear--;
            years.push({ val: maxYear, desc: maxYear });
        }
        return years;
    }

    /**
     * Module initialization
     */
    function init() {

        sb.notify("get-register-session");
        sb.listen("got-register-session", onRegSession);
        sb.listen("checked-register-username", onCheckedRegisterUsername);
        sb.listen("checked-register-email", onCheckedRegisterEmail);
        sb.listen("got-postcode-lookup", onGotPostcodeLookup);
        sb.listen("registered-player", onRegisteredPlayer);
        sb.listen("done-login-after-registration", onDoneLogin);
        vm.errors = ko.validation.group(vm);

        //toggle the componenet with class msg_body
        sb.$("#ui-registration-terms-toggle").click(function(){
            sb.$("#ui-registration-terms-display").slideToggle(500);
            return false;
        });


        
        $("#ui-phone").on("countrychange", function(e, countryData) {
            vm.mobilePhone.extend(rules.mobilePhone);
        });

        vm.customer = JSON.parse(sb.getFromStorage("AspersCustomer"));
        
        // set borders only if we're not going to populate the form
        if (!vm.customer) detectBordersWhenFocusChange();
        
        var l = window.location.pathname.split("/");
        if (l.length === 4) {
            vm.raf = l[2];
        }

        // When there's no steps, we can check them all at a time
        // also, in three steps we add a separateDialCode, this is a temporary solution till all the skins are sknified
        if (vm.totalSteps() == 0) {
            vm.validations[0]();
            sb.$("#ui-phone").intlTelInput({      
                    utilsScript: '/_global-library/js/vendor/intltelinput/js/utils.js',
                    separateDialCode: true
                });            
        } else {
            vm.title("Mr");
            sb.$("#ui-phone").intlTelInput({      
                    utilsScript: '/_global-library/js/vendor/intltelinput/js/utils.js',
                    separateDialCode: true
                });            

        }
        

        fbHook();

        sb.$("#ui-phone").before("<div id='icon' class='telephone-icon'></div>");
    }

    
    // if we have parameters sent by facebook then pre-populate the registration form   
    function fbHook () {
        vm.firstName(sb.getUrlParameter("facebook_firstname"));
        vm.lastName(sb.getUrlParameter("facebook_lastname"));       
        vm.email(sb.getUrlParameter("facebook_email"));
        vm.postcode(sb.getUrlParameter("facebook_postcode"));
        vm.mobilePhone(sb.getUrlParameter("facebook_mobile"));                          
        
        
        // add promo from url for all sites but SAW
        var l = window.location.pathname.split("/");
        if (l.length === 4) {
            vm.promoCode(l[2]);
        }
        
        
        
        
        
        if(sb.getUrlParameter("facebook_dob") != '')
        {                       
            var d = new Date(sb.getUrlParameter("facebook_dob"));                       
            vm.dayOfBirth(d.getDate());
            vm.monthOfBirth(d.getMonth()+1);
            vm.selectedYearOfBirth(d.getFullYear());                
        }
    }
    
    /**
     * Check if email is available request has ended
     * Handles response
     */
    function onCheckedRegisterEmail(e, data) {

        vm.emailNotAvailable("");

        if (!data) {
            return;
        }

        if (data.Code === REGISTER_EMAIL_MAX_CHECKS_REACHED) {

            maxUsernameChecks = true;

        } else if (data.Code === REGISTER_EMAIL_UNAVAILABLE) {

            vm.emailNotAvailable(data.Msg);

        } else if (data.Code === REGISTER_EMAIL_BANNED) {

            vm.emailNotAvailable(data.Msg);

        }
    }

    /**
     * Check if username is available request has ended
     * Handles response
     */
    function onCheckedRegisterUsername(e, data) {

        vm.usernameNotAvailable("");

        if (!data) {
            return;
        }

        if (data.Code === REGISTER_USERNAME_MAX_CHECKS_REACHED) {

            maxUsernameChecks = true;

        } else if (data.Code === REGISTER_USERNAME_UNAVAILABLE) {

            var suggested = "",
                msg = data.Msg;

            if (data.Suggest) {
                var i;
                for (i = 0; i < data.Suggest.length; i++) {
                    suggested += i === 0 ? data.Suggest[i] : ", " + data.Suggest[i];
                }
                msg += " We suggest you to try one of the following: " + suggested;
            }

            vm.usernameNotAvailable(msg);

        } else if (data.Code === REGISTER_USERNAME_BANNED) {

            vm.usernameNotAvailable(data.Msg);

        }

    }

    /**
     * Email has changed
     * Check if is available
     */
    function onEmailChanged(newVal) {        
        vm.email.extend(rules.email);
        if (!maxEmailChecks && vm.email() && vm.email.isValid()) {
            sb.notify("check-register-email", [newVal, groupId, regSessionId]);
        }
    }

    /**
     * Get addresses for the postcode request has ended
     * Handles response
     */
    function onGotPostcodeLookup(e, data) {
        if (data.Code === 0) {
            if (data.Addresses.length) {
                vm.addresses(data.Addresses);
                vm.city(data.Town);
                vm.state(data.County);
                removeBorders();
                addBorders();
            }
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }

    /**
     * Postcode has changed
     */
    function onPostcodeChanged(newVal) {
        vm.postcodeIsValid(newVal.length >= 4);
        if (newVal.length < 4) {
            vm.addresses([]);
            vm.address1([]);
            vm.address2([]);
            vm.city("");
            vm.state("");
        }
    }

    /**
     * Country has changed
     * @param newVal
     */
    function onSelectedCountry(newVal) {

        if (!newVal) return;

        vm.landline.rules.removeAll();
        //vm.mobilePhone.rules.removeAll();
        vm.selectedCountry.extend(rules.country);

        var countries = vm.countries();
        var country = sb._.find(countries, function (c) {
            return c.Code === newVal;
        });
        sb.$('#ui-phone').intlTelInput("setCountry", country.Code2);
        sb.$(".selected-flag").removeAttr("title");
        //vm.mobilePhone.extend(rules.mobilePhone);
    }

    /**
     * Player registration request has ended
     * Handles response
     */
    function onRegisteredPlayer(e, data) {
        if (data.Code === 0) {
            sb.sendDataToGoogle("Registration", data.PlayerID + "+" + document.referrer);
            sb.setCookie("PromoCode", vm.promoCode());
            sb.sendVariableToGoogleManager("PromoCode", vm.promoCode());
            sb.saveOnSessionStorage("Registered", "1");
            removeCookiesAndStorage();
            sb.notify("do-login-after-registration", [vm.username, vm.password]);
        } else {
            sb.sendVariableToGoogleManager("RegistrationFail", true);
            data.Msg && sb.showError(data.Msg);
        }
    }

    /**
     * Get registration session request has ended
     * Handles response
     */
    function onRegSession(e, data) {

        if (data.Code === 0) {
            vm.currencies(data.Currencies);
            vm.countries(data.Countries);
            groupId = data.GroupID;
            regSessionId = data.RegSessionID;
            // default values
            vm.selectedCurrency(sb.getFromStorage("currencyCode"));
            vm.selectedCurrency.extend(rules.currency);
            var country = sb.getFromStorage("countryCode");
            vm.selectedCountry(country);

            // if it's a VIP customer - populate the form
            if (vm.customer) populateForm(vm.customer);

        } else if (data.Code === -2) {
            data.Msg && sb.showConfirm(data.Msg, function () { window.location.href = "/"; });
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }

    /**
     * Player changed Address 1 field
     */
    function onSelectedAddress1Changed(newVal) {
        if (newVal) {
            var AddressArray = newVal.split("|");
            vm.address1(AddressArray[0]);
            vm.address2(AddressArray[1]);
            vm.addresses([]);
        }
    }

    /**
     * Player has changed the username
     */
    function onUsernameChanged(newVal) {
        vm.username.extend(rules.username);
        if (vm.username.isValid() && !maxUsernameChecks) {
            sb.notify("check-register-username", [newVal, groupId, regSessionId]);
        }
    }

    /**
     * Search addresses for the entered postcode
     * Make request
     */
    function postcodeLookup() {
        sb.notify("get-postcode-lookup", [vm.postcode(), regSessionId]);
    }

    /**
     * Registration
     * Make request
     */
    function register() {

        // if is already registering the player - exit
        if (vm.loading()) return false;

        var matches = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(getDOB());
        if (matches == null) return false;
        var y = matches[3];
        var m = matches[2]; // month 0-11
        var d = matches[1];
        

        var playerDetails = {

            username : vm.username(),
            password : vm.password(),
            email : vm.email(),
            currency : vm.selectedCurrency(),
            promoCode : vm.promoCode(),
            title : vm.title(),
            firstName : vm.firstName(),
            lastName : vm.lastName(),
            dob : y + '-' + m + '-' + d,//vm.selectedYearOfBirth() + "-" + vm.monthOfBirth() + "-" + vm.dayOfBirth(),
            country : vm.selectedCountry(),
            address1 : vm.address1(),
            address2 : vm.address2(),
            city : vm.city(),
            state : vm.state(),
            postcode : vm.postcode(),
            landline : vm.landline(),
            mobile : $('#ui-phone').intlTelInput("getNumber").substring(1), //vm.mobileCountryCode() + vm.mobilePhone(),
            wantsEmail : vm.wantsEmail() ? 1 : 0,
            wantsSMS : vm.wantsSMS() ? 1 : 0,
            wantsPhone : vm.wantsPhone() ? 1 : 0,
            wantsPost : vm.wantsPost() ? 1 : 0,
            landPage : sb.$.cookie("l") || "",
            ref: vm.raf,
            offlineCasino: vm.offlineCasino(),
            aspersCustomer: vm.aspersCustomer() || ""
        };

        sb.notify("register-player", [playerDetails, groupId, regSessionId]);
        vm.loading(true);
    }

    /**
     * Remove cookies
     */
    function removeCookiesAndStorage() {
        sb.$.removeCookie("l", { path: '/' });
        sb.removeFromStorage("aff");
        sb.removeFromStorage("HTTPReferer");
    }

    /**
     * Show promocode input field
     */
    function showPromoCode() {
        vm.hasPromoCode(true);
    }

    /**
     * Login request has ended
     * Handles response
     */
    function onDoneLogin(e, data) {

        if (data.Code === 0) {

            sb.saveOnSessionStorage("playerId", data.PlayerID);
            sb.saveOnSessionStorage("sessionId", data.SessionID);
            sb.saveOnStorage("username", data.Username);
            sb.setCookie("username", data.Username);
            sb.saveOnStorage("lastLoginDate", data.LastLoginDate);
            sb.saveOnStorage("firstName", data.Firstname);
            sb.saveOnStorage("class", data.Class);
            sb.saveOnStorage("balance", data.Balance);
            sb.saveOnStorage("real", data.Real);
            sb.saveOnStorage("bonus", data.Bonus);
            sb.saveOnStorage("bonusWins", data.BonusWins);
            sb.saveOnStorage("points", data.Points);
            sb.saveOnStorage("favourites", JSON.stringify(data.Favourites));
            sb.saveOnStorage("depositCount", data.DepositCount);
            sb.saveOnStorage("funded", !!data.Funded);
            sb.saveOnStorage("currencySymbol", getCurrencySymbol(data.CurrencyCode));
            sb.setCookie("RegistrationDate",data.RegistrationDate);

            var res = window.location.pathname.split("/register/");
            window.location = "/welcome/"+ (typeof res[1] != 'undefined'? res[1]:'');
        } else {
            if (data.Msg) {
                sb.showError(data.Msg);
            }
        }

    }

    /**
     * Returns the currency symbol
     * @param code - Currency Code of the player
     */
    function getCurrencySymbol(code) {
        if (code === "GBP") return "£";
        if (code === "EUR") return "€";
        if (code === "AUD" || code === "CAD") return "$";
        return "";
    }

    // populates the registration form with the data from a VIP Customer
    // @param c - the customer's data
    function populateForm (c) {

        // populate the form

        vm.dayOfBirth(c.DateOfBirth ? c.DateOfBirth.substr(8,2) : "");
        vm.monthOfBirth(c.DateOfBirth ? c.DateOfBirth.substr(5,2) : "");
        vm.selectedYearOfBirth(c.DateOfBirth ? c.DateOfBirth.substr(0,4) : "");
        vm.email(c.EmailAddress || "");
        vm.hasPromoCode(true);
        vm.promoCode(c.Promocode || "");
        vm.firstName(c.Forename || "");
        vm.lastName(c.Surname || "");
        vm.postcode(c.Address.Postcode || "");
        vm.postcodeIsValid(true);
        vm.mobilePhone(c.MobilePhone || "");
        vm.title(c.Title || "");

        // keep customer number to be saved on database
        vm.aspersCustomer(c.Number);

        // set player as VIP to be used on welcome page
        sb.saveOnStorage("userIsVIP", true);

        // focus on username
        sb.$("#ui-firstname").focus();

        // validation borders
        detectBordersWhenFocusChange();

    }

    return vm;
}