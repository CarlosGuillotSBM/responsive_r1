var sbm =sbm || {};

/**
 * It will show warning
 * Snooze the warning
 * @param sb
 * @constructor
 */
sbm.RealityCheck = function (sb, options) {

    /**
     * Properties
     */

        // time interval to show the reality check warning - value in ms
    this.frequency = 0;
    // frequency display on page
    this.frequencyDisplay = sb.ko.observable("");
    // frequency display on page
    this.totalTimeDisplay = sb.ko.observable("");
    // frequency Id from db
    this.frequencyId = sb.ko.observable(0);
    // timer
    this.timer = null;
    // visibility of the warning
    this.warningVisible = sb.ko.observable(false);
    // the game id under the reality check
    this.gameId;
    //reality frequency options
    this.frequencyOptions = sb.ko.observableArray([]);
    // If this is set to true we won't show success message to player
    // when setting the reality frequency
    // This is used when player opted by removing reminder from the warning window
    this.doNotShowRealitySetSettingsResponse = false;
    // if reality check is active
    this.active = sb.ko.observable(false);
    // the number of times the reality check has been confirmed by the player
    this.times = 0;
    // the game container
    this.$iframeContainer = sb.$("#ui-game-launch-holder");
    // little hack to support the old sites
    if (!this.$iframeContainer.length) {
        this.$iframeContainer = sb.$("#ui-game-frame");
    }
    // DEV-6054: indentifying whether a game frame has been launched from the bingo client, 
    // in that case you need load the home page within the same window
    this.fromBingo = false;
    /**
     * Functions
     */

    /**
     * Module initialization
     */
    this.init = function () {

        var enabled = sbm.serverconfig.realityCheckOn, // enabled by configuration
            sessionId = sb.getFromStorage("sessionId"), // get session Id
            self = this;

        if (!sessionId) {
            setTimeout(function () {
                self.init();
            }, 5000);
        }

        // activate if is enabled and has session
        this.active(enabled && sessionId);

        // exit
        if (!this.active()) return;

        sb.logInfo("RC with session being initialized");

        sb.listen("start-reality-check", this.onStartCheck);
        sb.listen("stop-reality-check", this.onStopCheck);
        sb.listen("got-reality-settings", this.gotRealitySettings);
        sb.listen("on-set-reality-settings", this.onSetRealitySettings);
        sb.listen("on-reality-stop-game", this.onStoppedGame);
        sb.listen("on-reality-start-game", this.onStartedGame);

        // get player settings
        if (sessionId) 
            this.getSettings();


        // DEV-5530 DEV-5533 Firefox and IE bug on flash games -
        // subscribe to any "warningVisible" changes
        // Now we're doing it for all browsers - some browsers after an update were not being detected correctly
        this.warningVisible.subscribe(function (newVal) {
            if (newVal) this.$iframeContainer.css("visibility", "hidden");
            else this.$iframeContainer.css("visibility", "visible");
        }.bind(this));

        // DEV-6054: detects wether the games have been launched from the Bingo client
        if (window.opener) {
            try {
                this.fromBingo = window.opener.location.pathname == '/bingo_launch.php'? true : false;
            }
            catch (err) {
                this.fromBingo = false;
            }
        } else {
            this.fromBingo = false;
        }

    };

    /**
     * module destruction
     */
    this.destroy = function () {
        sb.ignore("start-reality-check");
        sb.ignore("stop-reality-check");
        sb.ignore("got-reality-settings");
        sb.ignore("on-set-reality-settings");
    };

    /**
     * start check
     */
    this.onStartCheck = function (e, gameId) {
        this.times = 0;
        this.startCheck(gameId);
    }.bind(this);

    /**
     * stop check
     */
    this.onStopCheck = function () {
        clearInterval(this.timer);
    }.bind(this);

    /**
     * On request to stop game has finished
     */
    this.onStoppedGame = function (e, data) {
        if (data.Code === 0) {
            this.times ++;
            this.totalTimeDisplay(this.getRealityCheckString());
            this.showWarning();
        } else {
            if (data.Code !== -3 && data.Msg) {
                sb.showError(data.Msg);
            }
        }
    }.bind(this);

    /**
     * On request to start game has finished
     */
    this.onStartedGame = function (e, data) {
        if (data.Code !== 0) {
            sb.showError("Sorry there is a system error, if the error persists contact support");
        }
    }.bind(this);

    /**
     * reality check - start time with the correct frequency for the current player
     * hides the warning in case it's visible (e.g. when snoozing)
     */
    this.startCheck = function (gameId) {
        // if not set as No Reminder
        if (this.frequency > 0) {
            this.warningVisible(false);
            this.timer && clearInterval(this.timer);
            this.timer = setInterval(this.stopGameCheck, this.frequency);
            this.gameId = gameId;
            sb.notify("reality-start-game", [this.gameId]);
        }
    }.bind(this);

    /**
     * Player opted by snoozing the warning
     * Will hide warning, start check again and send resume to back end
     */
    this.resumeCheck = function () {

        this.startCheck(this.gameId);

    }.bind(this);

    /**
     * shows the warning
     */
    this.showWarning = function () {
        this.warningVisible(true);
    }.bind(this);

    /**
     * stops the timer
     * stops the game
     */
    this.stopGameCheck = function () {
        clearInterval(this.timer);
        this.gameId && sb.notify("reality-stop-game",[this.gameId]);
    }.bind(this);

    /**
     *  Stop reminders
     *  the warning message should disappear and the player's account should change to the "No reminder" option.
     */
    this.stopReminders = function () {
        this.warningVisible(false);
        this.frequencyId(1);
        clearInterval(this.timer);
        sb.notify("reality-set-settings", [this.frequencyId()]);
        this.doNotShowRealitySetSettingsResponse = true;
    }.bind(this);

    /**
     * Go home - will redirect to start page
     */
    this.goHome = function () {

        // if game was opened in new window
        if (window.opener) {      
            if (this.fromBingo) {
                window.location = sbm.serverconfig.startURL || "/start/";
            } else {
                window.opener.location = sbm.serverconfig.startURL || "/start/";
                window.close();                
            }
        } else {
            // lightbox
            window.location = sbm.serverconfig.startURL || "/start/";
        }
    }.bind(this);

    /**
     * Go to account activity
     */
    this.goToAccountActivity = function () {

        if (!sbm.serverconfig.accountActivityURL) return;

        var url = sbm.serverconfig.accountActivityURL;

        // if game was opened in new window
        if (window.opener) {
            if (this.fromBingo) {
                window.location = url;
            } else {
                window.opener.location = url;
                window.close();                
            }            
        } else {
            // lightbox
            window.parent.location = url;
        }
    }.bind(this);

    /**
     * Gets the settings for a given player and the skin's frequency options
     */
    this.getSettings = function(){
        sb.notify("reality-get-settings");
    }

    /**
     * Gets the settings for a given player and the skin's frequency options
     */
    this.setSettings = function(){
        sb.notify("reality-set-settings", [this.frequencyId()]);
    }.bind(this);

    /**
     * Gets the reality settings and sets the player's frequency
     * Handles response
     */
    this.gotRealitySettings = function(e,data){
        if (data.Code === 0) {
            this.frequencyOptions(data.FrequencyOptions);
            this.frequencyId(data.FrequencyId);
            var selectedFrequency = this.auxSetFrequencyFromOptions(data.FrequencyOptions, data.FrequencyId);
            this.frequency = selectedFrequency.NumMinutes * 60 * 1000; // transform to milliseconds
            this.frequencyDisplay(selectedFrequency.Description);
            this.totalTimeDisplay(this.getRealityCheckString());

            // if gameId is set on the module configuration
            // start check immediately - this is used when launching a game in new window
            options.gameId && this.startCheck(options.gameId);

        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     *
     * Handles response
     */
    this.onSetRealitySettings = function(e,data){
        if (data.Code === 0) {
            !this.doNotShowRealitySetSettingsResponse && sb.showSuccess(data.Msg);
            var selectedFrequency = this.auxSetFrequencyFromOptions(sb.ko.toJS(this.frequencyOptions), this.frequencyId());
            this.frequency = selectedFrequency.NumMinutes * 60 * 1000; // transform to milliseconds
            this.frequencyDisplay(selectedFrequency.Description);
            this.totalTimeDisplay(this.getRealityCheckString());
        } else {
            data.Msg && sb.showError(data.Msg);
        }
    }.bind(this);

    /**
     * Finds on the passed array of options the current frequency
     * returns the configured frequency
     */
    this.auxSetFrequencyFromOptions = function (options, frequencyId) {
        return sb._.find(options, function(option){ return option.ID === frequencyId; });
    }.bind(this);



    /**
     * Set menu interval message on setup.
     * @private
     * @method getRealityCheckString
     */
    this.getRealityCheckString = function () {
        var time = (this.frequency /(60*1000)) * this.times ;

        //Check if store has a value.
        if (typeof time != 'undefined') {
            if (time === 0) {
                // this shouldn't happen
                return "";
            }
            else {
                var hours = Math.floor(time/60);
                var minutes = Math.floor(time % 60);
                var stringTime = "";
                if (hours > 0) {
                    stringTime += hours + " hour";
                    if (hours > 1) {
                        stringTime += "s";
                    }
                    if (minutes > 0) {
                        stringTime += " and ";
                    }
                }
                if (minutes === 1) {
                    stringTime += "1 minute";
                }
                else {
                    stringTime += minutes + " minutes";
                }

                return stringTime;
            }

        }   //Otherwise add the default.
        else {
            //this shouldn't happen
            return "";
        }
    }.bind(this);


};