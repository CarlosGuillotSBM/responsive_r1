var sbm = sbm || {};

/**
 * Game Launcher
 * @param sb The sandbox
 * @param options Options if needed
 * @constructor
 */
sbm.GameLauncher = function (sb, options) {
    "use strict";

    this.playerId = sbm.core.storage.get("playerId");
    this.gameUrl = sb.serverconfig.gameUrl;
    this.gameId = sb.ko.observable("");
    this.roomId = sb.ko.observable("");
    this.gameTitle = sb.ko.observable("");
    this.gameDescription = sb.ko.observable("");
    this.$modal = sb.$(options.modalSelector);
    this.icon = sb.ko.observable("");
    this.thumbnailIcon = sb.ko.observable("");
    this.detailsUrl = sb.ko.observable("");
    this.gamesFound = [];
    this.demoPlay = sb.ko.observable(false);
    this.gamePosition = sb.ko.observable(0);
    this.gameContainerKey = sb.ko.observable("");
    this.gameLoaded = sb.ko.observable(false);

    /**
     * Module initialization
     */
    this.init = function () {

        this.gameId(0);

        // setup auto-complete on search games input
        this.setupSearchBox();

        sb.listen("launch-game", this.openGameWindow);
        sb.listen("game-launcher-set-room", this.setRoom);
        sb.listen("open-game-info-modal", this.openGameInfo);
        sb.listen("got-game-info", this.onGotGameInfo);

        this.$modal.on("open", function () {
            $("body").addClass("modal-open");
            sb.notify("show-recaptcha");
        });
        this.$modal.on("close", function () {
            this.gameId("");
            $("body").removeClass("modal-open");
            sb.notify("hide-recaptcha");
        }.bind(this));
    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("launch-game");
        sb.ignore("game-launcher-set-room");
        sb.ignore("open-game-info-modal");
        sb.ignore("got-game-info");
    };

    /**
     * Setup the games search box
     * Uses jquery ui auto-complete plugin
     */
    this.setupSearchBox = function () {

        var innerHtml;

        var selectedGameFromSearch = function (game) {

            sb.$(".ui-autocomplete-input").blur();

            this.gameId(game.id);
            this.gameTitle(game.value);
            this.gameDescription(game.intro);
            this.detailsUrl(game.detailUrl);
            this.demoPlay(game.demoPlay);
            this.icon(game.screenshot);

            // push the game to the stack in order to be added to the last played games

            sbm.games = sbm.games || [];

            sbm.games.push({
                "id": game.id,
                "icon": game.icon,
                "bg": game.background,
                "title": game.value,
                "type": "game",
                "desc": game.intro,
                "thumb": game.screenshot,
                "detUrl": game.detailUrl
            });

            if (this.getSessionId()) {
                sb.notify("launch-game-window", [0, game.id, "game"]);
                sb.notify("add-last-game", [game.id]);
            } else {
                this.openGameInfo();
            }

        }.bind(this);

        sb.$(".ui-game-search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/data.php",
                    dataType: "json",
                    data: {
                        f: "searchgames",
                        t: request.term
                    },
                    success: function (data) {

                        this.gamesFound.length = 0;

                        if (data.Code === 0) {

                            if (data.Games.length > 0) {

                                sb._.each(data.Games, function (game) {
                                    this.gamesFound.push({
                                        value: game.Title,
                                        label: game.Title,
                                        intro: game.Intro,
                                        detailUrl: game.DetailsURL,
                                        icon: game.Image,
                                        screenshot: game.Image1,
                                        demoPlay: game.DemoPlay,
                                        id: game.ID,
                                        background: game.Text1
                                    });
                                }.bind(this));

                            } else {
                                sb.notifyInfo(sb.i18n("noGamesFound"));
                            }

                        } else {
                            data.Msg && sb.showError(data.Msg);
                        }

                        response(this.gamesFound);

                    }.bind(this)
                });
            }.bind(this),
            create: function (e,ui){

                // compile template
                var searchItemTemplate = $("#ui-desktop-search-item").html();
                searchItemTemplate = _.template(searchItemTemplate);

                $(this).data('ui-autocomplete')._renderItem = function (ul, item) {

                    if (item.label.length > 22) {
                        item.label = item.label.substring(0,22) + "...";
                    }
                    innerHtml = searchItemTemplate(item);

                    return $("<li>").append(innerHtml).appendTo(ul);
                }.bind(this);
            },
            minLength: 3,
            delay: 500,
            select: function (e, ui) {
                selectedGameFromSearch(ui.item);
                this.value = "";
                return false;
            }
        });

    };

    /**
     * Get session Id from storage
     */
    this.getSessionId = function () {
        return sb.getFromStorage("sessionId");
    };

    /**
     * Open a game
     */
    this.openGame = function (vm, e) {

        var $game = $(e.currentTarget);
        var gameId = $game.data("gameid");
        // get game data from games array
        var g = sb._.find(sbm.games, function (game) {
            return game.id === gameId;
        });

        // fill the game properties
        if (g) {

            this.gameId(g.id);
            this.gameTitle(g.title);
            this.gameDescription(g.desc);
            this.icon(g.icon);
            this.thumbnailIcon(g.thumb);
            this.detailsUrl(g.detUrl);
            var demoModeAvailable = g.demo ? true : false;
            this.demoPlay(demoModeAvailable);
            this.gamePosition(this.getPosition($game));
            this.gameContainerKey(this.getContainerKey($game));
            sb.$("body").addClass("modal-open");
            
            if (this.getSessionId()) {
                var variables = {
                    "event" : "game_launch",
                    "GameName" : g.title
                };
                sb.sendSetOfVariablesToGoogleManager(variables);
                this.launchGame(0, gameId);
            } else if ($game.data("infopage") && demoModeAvailable) {
                this.launchGame(1, gameId);
            } else if (!$game.data("infopage")) {
                this.openGameInfo();
            }
        }
        // If this is not on the Game Array
        else{
            this.launchGame(0, gameId);
        }

    }.bind(this);

    /**
     * Get game's position on the hub
     * @param $game
     * @returns Game's position
     */
    this.getPosition = function ($game) {
        var parent = $game.parents(".ui-parent-game-icon");
        if (parent.length) {
            return sb.$(parent).data("position") || 0;
        }
        return 0;
    };

    /**
     * Get key on container DOM element
     * @param $game the game wrapped with jQuery
     * @returns The key
     */
    this.getContainerKey = function ($game) {
        var parent = $game.parents(".ui-parent-game-icon");
        if (parent.length) {
            return sb.$(parent).data("containerkey") || 0;
        }
        return 0;
    };

    /**
     * Launch a game
     * @param fun Fun Mode
     * @param gameId
     */
    this.launchGame = function (fun, gameId, extraParams) {
        if (this.getSessionId() || fun) {

            fun && sb.sendDataToGoogle("Game Demo", gameId + "+" + this.gamePosition() + "+" + this.gameContainerKey());
            !fun && sb.sendDataToGoogle("Game Launch", gameId + "+" + this.gamePosition() + "+" + this.gameContainerKey());
            if(gameId == "gam-prize-wheel"){
                sb.notify("launch-game-window", [fun, gameId, "wheel", sb.getFromStorage("username"), extraParams]);
            }
            else{
                sb.notify("launch-game-window", [fun, gameId, "game", sb.getFromStorage("username"), extraParams]);
                sb.notify("add-last-game", [gameId]);
            }

        }

    };

    /**
     * Launch game in demo mode if available.
     * @gameId Binded on the HTML element
     * @background Game background
     */
    this.tryGame = function (gameId) {
        
        this.launchGame(1, this.gameId() || gameId);
        this.$modal.foundation("reveal", "close");

    }.bind(this);

    /**
     * Close the modal
     */
    this.closeModal = function () {

        this.$modal.foundation("reveal", "close");
    }.bind(this);

    /**
     * Opens a game
     */
    this.openGameWindow = function (e, gameId, fun) {
        sb.notify("launch-game-window", [fun, gameId, "game"]);
        sb.notify("add-last-game", [gameId]);

    }.bind(this);

    /**
     * Opens game info modal
     * @param game Object containing all the necessary data for the info panel
     */
    this.openGameInfo = function (e, game, fun) {

        if (game) {
            this.gameId(game.id);
            this.gameTitle(game.title);
            this.gameDescription(game.desc);
            this.icon(game.thumb);
            this.thumbnailIcon(game.thumb);
            this.detailsUrl(game.detUrl);
            this.demoPlay(fun);
        }
        var $recaptcha = sb.$(".grecaptcha-badge");
        $recaptcha.css("visibility","visible");

        sb.hideLoadingAnimation();
        this.$modal.foundation("reveal", "open");
        window.scrollTo(0, 0);
    }.bind(this);

    this.setRoom = function (e, roomId) {
        this.roomId(roomId);
    }.bind(this);

    /**
    * launches a game using their numeric id
    *
    **/
    
    this.launchGameWithId = function (fun, gameId) {
        sb.notify("get-game-info", [gameId, fun]);
    }

    /**
     * Get game info request has ended
     * Handles response
     */
    this.onGotGameInfo = function (e, data) {

        var success = data.Code === 0 || data.Code === 2;
        var game

        if (data.Code === 2) {
            sb.showInfo(sb.i18n("gameNoAvailableOnMobile"));
            this.editableContent().forMobile(false);
        }
   

        if (success) {

            var sbm = sbm || {};
            sbm.games = sbm.games || [];
            sbm.games.push({
                "id"    : data.Id,
                "icon"  : data.Image,
                "bg"    : data.Text1,
                "title" : data.Title,
                "type"  : "game",
                "desc"  : data.Intro,
                "thumb" : data.Image1,
                "detUrl": data.DetailsURL,
                "demo": data.DemoPlay
            }); 

            this.launchGame(data.Fun,data.Id,data.ExtraParams);
        } else {
            data.Msg && sb.notifyError(data.Msg);
        }

    }.bind(this);

};