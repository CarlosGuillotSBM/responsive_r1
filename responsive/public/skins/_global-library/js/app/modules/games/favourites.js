var sbm = sbm || {};

/**
 * Player's favourite games management
 * @param sb The sandbox
 * @constructor
 */

sbm.Favourites = function (sb) {
    "use strict";

    this.favourites = sb.ko.observableArray([]);
    this.noFavourites = sb.ko.observableArray([]);
    this.gameUrl = sb.serverconfig.gameUrl;
    this.gameLaunched = {};
    this.iconActive = "heart-active.svg";
    this.iconInactive = "heart.svg";
    this.gameLauncherIcon = sb.ko.observable(this.iconInactive);

    /**
     * Module initialization
     */
    this.init = function () {

        sb.listen("finish-set-favourite-game", this.onFinishSetFavouriteGame);
        sb.listen("finish-get-favourite-list", this.onFinishGetFavouriteList);
        sb.listen("launched-game", this.onLaunchedGame);
        this.loadFavourites();

    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("finish-set-favourite-game");
        sb.ignore("finish-get-favourite-list");
        sb.ignore("launched-game");
    };

    /**
     * Get all player's favourite games from local storage
     */
    this.loadFavourites = function () {
        var favourites = [];
        if (sb.getFromStorage("sessionId")) {
            // get favourites array
            favourites = JSON.parse(sb.getFromStorage("favourites"));
            // remove duplicated games
            favourites = this.removeDuplicated(favourites);
            // populate the unique games
            this.populateFavourites(favourites);
        } else {
            this.favourites([]);
        }
    };

    this.onFinishGetFavouriteList = function (e, data){

        if (data.Data[0] && data.Data[0].Code === undefined) {
            
            // get favourites array from response
            var favourites = data.Data;


            // remove all the duplicated games
            favourites = this.removeDuplicated(favourites);


            this.populateFavourites(favourites);
        }
    }.bind(this);


    this.removeDuplicated = function (favourites) {

        if (!favourites) return;

        var unique = [],
            gameFound;

        // loop through all the games
        for(var i = 0; i < favourites.length; i++) {
            
            var game = {}
            
            // get the game
            game = favourites[i];

            // check if is in the unique array
            gameFound = sb._.find(unique, function (g) {
                return g && g.Game === game.Game;
            });

            // check if game was not found
            if (!gameFound) {
                unique.push(game);
            }

        }

        return unique;

    };




    this.populateFavourites = function(favourites) {
        
        if (!favourites || !favourites.length) {
            // set the number of no favourites icons
            this.noFavourites([1]);
            this.favourites([]);
            return;
        }
        
        this.noFavourites([]);

        var title, i = 0;
        sb._.each(favourites, function (favourite) {
            i++;
            title = favourite.Title.length >= 23 ? favourite.Title.substring(0, 23) + "..." : favourite.Title;
            favourite.Title = title.toUpperCase();
            favourite.DetailsURL = favourite.DetailsURL + "/";
            favourite.Position = i;
            var $container = $("div[data-favgameid=" + favourite.Game + "]");
            $container.removeClass("icons-star--off");
            $container.addClass("icons-star--on");
        });
        this.favourites(favourites);
        sb.saveOnStorage("favourites", JSON.stringify(favourites));
    }.bind(this);

    /**
     * Player selected to play a game from the favourites list
     */
    this.launchGame = function (gameId, position, title, icon, detailsUrl) {

        sb.sendDataToGoogle("Game Launch", gameId + "+" + position + "+Favourites");

        // push the game to the stack in order to be added to the last played games

        var g = sb._.find(this.favourites(), function (fav) {
            return fav.Game === gameId;
        });

        if (sbm.games && g) {

            sbm.games.push({
                "id": g.Game,
                "icon": g.Icon,
                "bg": g.Background,
                "title": g.Title,
                "type": "game",
                "desc": g.Description,
                "thumb": g.Screenshot,
                "detUrl": g.DetailsURL
            });
        }

        // launch game

        var game = {
            title: title,
            icon: icon,
            detailsUrl: detailsUrl
        };

        sb.notify("launch-game", [gameId, 0, 1, null, null, game]);

    }.bind(this);

    /**
     * Toggles favourite
     * If not favourite yet, adds it to the player's list otherwise removes it
     */
    this.toggleFavourite = function (vm, e) {
        if (!sb.getFromStorage("sessionId")) {
            sb.showInfo(sb.i18n("needsToBeLoggedInToSetFav"));
            return;
        }

        var gameId = $(e.currentTarget).data("favgameid");
        if (!gameId) {
            return;
        }

        var favourites = sb.getFromStorage("favourites");
        if (!favourites) {
            return;
        }

        favourites = JSON.parse(favourites);

        var favourite = ko.utils.arrayFirst(favourites, function (item) {
            return item.Game == gameId;
        });

        // toggle image and send request to server

        favourite = !favourite; // toggle status
        var favClass = favourite ? "icons-star--on" : "icons-star--off";
        var $container = $("div[data-favgameid=" + gameId + "]");
        // clear old class
        $container.removeClass("icons-star--on");
        $container.removeClass("icons-star--off");

        if (favourite) {
            sb.$('.added-'+gameId).text(sb.i18n("addedToFavourites"));
        } else {
            sb.$('.added-'+gameId).text(sb.i18n("removedFromFavourites"));
        }
        sb.$('.added-'+gameId).stop(true, true).fadeIn(500).delay(2000).fadeOut(500);
        // add class
        $container.addClass(favClass);

        sb.notify("set-favourite-game", [gameId, favourite]);

    }.bind(this);

    /**
     * Player selected to remove a game from the list of favourites
     */
    this.removeFavourite = function (gameId) {

        // dataservice request
        sb.notify("set-favourite-game", [gameId, false]);
        // remove from other section of the page than the favourites list
        sb.$(".game-icon__favourite__icon[data-favgameid=" + gameId + "]").removeClass("icons-star--on").addClass("icons-star--off");

    }.bind(this);

    /**
     * Favourites have changed
     * Loads favourites
     */
    this.onFinishSetFavouriteGame = function (e, data) {
        if (data.Code === 0) {

            sb.saveOnStorage("favourites", JSON.stringify(data.Favourites));
            this.loadFavourites();

        } else {
            if (data.Msg) {
                sb.showError(data.Msg);
            }
        }
    }.bind(this);

    /**
     * Handles received event of game / room launched
     * Checks if is favourite and set the right icon used on game launcher
     * and stores in memory if the game launched is a favourite
     */
    this.onLaunchedGame = function (e, game) {
        if (sb.isFavourite(game)) {
            this.gameLauncherIcon(this.iconActive);
            this.gameLaunched = {
                fav: true,
                id: game
            };
        } else {
            this.gameLauncherIcon(this.iconInactive);
            this.gameLaunched = {
                fav: false,
                id: game
            };
        }
    }.bind(this);

    /**
     * Toggles favourite from the game/bingo launcher
     */
    this.toggleFavouriteFromGameLauncher = function () {
        this.gameLaunched.fav = !this.gameLaunched.fav;
        if (this.gameLaunched.fav) {
            this.gameLauncherIcon(this.iconActive);
        } else {
            this.gameLauncherIcon(this.iconInactive);
        }
        sb.notify("set-favourite-game", [this.gameLaunched.id, this.gameLaunched.fav]);
    }.bind(this);
};