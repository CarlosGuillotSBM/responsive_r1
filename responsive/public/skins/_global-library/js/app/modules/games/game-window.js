var sbm = sbm || {};

/**
* Game Window - used on desktop to load the games
* The game window needs to handle some controls:
* - Maximize
* - Minimize
* - Quick Deposit
* @param sb The sandbox
* @param options Options if needed
* @constructor
*/
sbm.GameWindow = function (sb) {
    "use strict";
    
    this.gameLoaded = sb.ko.observable(false);
    this.$background = sb.$("#ui-game-background");
    this.$iframeContainer = sb.$("#ui-game-launch-holder");
    this.$iframe = sb.$("#ui-game-ifr");
    this.$iframeBorder = sb.$("#ui-game-launch-holder");
    this.$btnClose = sb.$(".ui-close-iframe");
    this.$btnMaximize = sb.$("#ui-maximize-client");
    this.$btnMinimize = sb.$("#ui-minimize-client");
    this.$btnQuickDeposit = sb.$("#ui-quick-deposit");
    this.$btnToggleFav = sb.$("#ui-set-favourite-client");
    this.gameDimensions = { w: 1024, h: 768 };
    this.bingoDimensions = { w: 1024, h: 768 };
    this.paddingTop = 0;
    this.fullscreen = false;
    this.recommendedGames = sb.ko.observable([]);
    this.$recommendedContainer = sb.$(".ui-recommend-games-container");
    this.recommendedTemplate = sb.$("#ui-recommended-games-tmpl").html();
    this.recommendationsLoaded = sb.ko.observable(false);
    this.$slider = sb.$('.game-launch-side-slider');
    this.topBarHeight = sb.$(".games-iframe-content__top-bar").height() + 60;
    this.$body = sb.$("body");
    this.$recommendedPanel = sb.$(".games-iframe-content__right-panel");
    this.sliderLoaded = false;
    this.currentGameId = 0;
    this.isPW = false;
    
    this.token='';
    this.signature='';
    this.playerID=0;
    /**
    * Module initialization
    */
    this.init = function () {
        
        sb.listen("launch-game-window", this.openGameWindow);
        sb.listen("got-game-recommendations", this.gotRecommendedGames);
        
        // game recommendations
        this.$recommendedContainer.html("");
        this.$recommendedContainer.html(this.recommendedTemplate);
        
        // DEV-5530 Firefox bug on flash games
        if (this.isFirefox()) {
            this.$iframeContainer.css({ transform: "initial", position: "fixed", top: "0px", left: "0px" });
        }
    };
    
    this.isFirefox = function () {
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) return true;
        
        return false;
    };
    
    /**
    * Module destruction
    */
    this.destroy = function () {
        sb.ignore("launch-game-window");
        sb.ignore("got-game-recommendations");
    };
    
    /**
    * Launch a game
    * @param fun Fun Mode
    * @param gameId Game Id
    */
    this.launchGame = function (fun, gameId, type, extraParams) {
        
        sb.$("#ui-game-ifr-container").show();
        this.openGameWindow(fun, gameId, type, extraParams);
        
    };
    /**
    * Opens a game on iframe if is desktop or same window if is mobile
    * @param path the page path
    * @param params the post parameters to send in the request
    */
    this.openGameWindow = function (e, fun, gameId, type, username, extraParams) {
        
        var loadGame = function (noCMA) {
            
            // if set to true means CMA dialog was not displayed
            // if false means CMA dialog was displayed and player clicked the Play button
            var URUVerification = sb.getCookie("URUVerified");
            if (!noCMA || type==="3Rad") {
                sb.sendDataToGoogle("CMA", "Alert play");
            }
            
            // get recommended games
            sb.notify("get-game-recommendations");
            
            // get required data to send on URL
            var path = "";
            var playerId = sb.getFromStorage("playerId");
            var sessionId = sb.getFromStorage("sessionId");
            var username = username || sb.getFromStorage("username");
            
            // check device
            var device = sb.getDevice();
            
            // build game path
            
            if (type === "room") {
                var room = gameId ? "Room_" + gameId : "Lobby";
                var roomParam = (gameId == null ? "Room=Lobby" : "RoomName="+room);
                // bingo room path
                
                path = "?" + roomParam +
                "&PlayerID=" + (playerId || "") +
                "&SessionID=" + (sessionId || "") +
                "&Alias=" + (username || "");
                
            }
            
            else if (type === "3Rad") {
                path = sb.serverconfig.threeRadicalURLBoard + "?type=3Rad";
                
            } 
            
            else if (type === "wheel") {
                path = sb.serverconfig.prizeWheelURL + "?SkinID=" + sb.serverconfig.skinId + "&uid=" + playerId + "&guid=" +sessionId + "&gameid=531&HomeURL=" + sb.serverconfig.baseUrl;
                this.isPW = true;
            } else {
                
                // slot game 
                
                path = "?skinID=" + sb.serverconfig.skinId +
                "&PlayerID=" + playerId +
                "&SessionID=" + sessionId +
                "&GameID=" + gameId +
                "&fun=" + fun +
                "&GameLaunchSourceID=1";
            }
            
            path += "&RealDevice=" + sbm.serverconfig.device;
            
            if (extraParams) {
                for (var propertyName in extraParams) {
                    if (extraParams.hasOwnProperty(propertyName)) {
                        path+="&"+propertyName.toString()+"="+extraParams[propertyName].toString();
                    }
                }
            }
            if(URUVerification == 1){
               
                if (device === "web") {
                    
                    path += "&DeviceID=1";
                    
                    // check how it should open the game
                    // bingo client ALWAYS on new window despite configuration
                    
                    if (type === "room" || sbm.serverconfig.gameLaunchMode === "newWindow") {
                        
                        // new window
                        path = sbm.serverconfig.bingoUrl + path;
                        
                        var newWindow = window.open(path, "Game", 'width=1064, height=798,toolbar=0, resizable=1, location=0');
                        newWindow.focus();
                        
                    } else {
                        
                        // lightbox
                        
                        // set the game background
                        if(type!="3Rad"){
                            if (type==="wheel"){
                                this.gameDimensions = {
                                    w: 960,
                                    h: 540
                                };
                            } 
                            
                            this.setBackground(gameId);
                            
                            // add event listener for messages coming from the iframe
                            window.addEventListener("message", this.onGotMessageFromIframe, false);
                            
                            // add event listener for window resize
                            sb.$(window).on("resize", this.onWindowResize);
                        }
                        
                        
                        // show iframe
                        
                        if (type === "room") {
                            path = sbm.serverconfig.bingoUrl + path;
                        }
                        else if (type==="3Rad"){
                            path = sb.serverconfig.threeRadicalURLBoard + "?externalId="+extraParams["externalId"]+"&signature="+extraParams["signature"]+"&nonce="+extraParams["nonce"];
                        } 
                        else if (type==="wheel"){
                            this.gameDimensions = {
                                w: 960,
                                h: 540
                            };
                        } 
                        else {
                            path = sbm.serverconfig.gameUrl + path;
                        }  
                        this.currentGameId = gameId;
                        if(type != "3Rad"){
                            this.$iframe.attr("src", path);
                            
                            this.$iframeContainer.fadeIn(1000, function () {
                                this.$iframe.fadeIn(1000);
                                this.gameLoaded(true);
                                this.$btnClose.show();
                                this.$btnMaximize.show();
                                
                                if (sessionId) {
                                    
                                    if (type === "room") {
                                        
                                        this.gameDimensions = {
                                            w: this.bingoDimensions.w,
                                            h: this.bingoDimensions.h
                                        };
                                        this.adjustGameDimensions();
                                        
                                    }
                                    else if(type === "wheel"){
                                        this.gameDimensions = {
                                            w: 960,
                                            h: 540
                                        };
                                        this.adjustGameDimensions();
                                        this.$recommendedPanel.hide();
                                    }
                                    else {
                                        this.$recommendedPanel.show();
                                        this.$btnToggleFav.show();
                                    }
                                    this.$btnQuickDeposit.show();
                                }
                                
                            }.bind(this));
                            
                            this.$btnClose.on("click", this.closeGamesIframe);
                            this.$btnMaximize.on("click", this.goFullScreen);
                            this.$btnMinimize.on("click", this.restoreSize);
                        }
                        else{
                            var newWindow = window.open(path, "Game", 'width=1064, height=798,toolbar=0, resizable=1, location=0');
                            newWindow.focus();
                        }
                        
                        // start reality check - only for the light box
                        !fun && sb.notify("start-reality-check",[this.currentGameId]);
                    }
                    
                } else {
                    
                    path += "&DeviceID=2";
                    
                    if (type === "room") {
                        path = sbm.serverconfig.bingoUrl + path;
                    }
                    else if (type === "3Rad") {
                        path = sb.serverconfig.threeRadicalURLBoard + "?externalId="+extraParams["externalId"]+"&signature="+extraParams["signature"]+"&nonce="+extraParams["nonce"];
                    } else {
                        path = "/game-frame/thirdparty"+ path;
                    }
                    
                    // mobile - opens game/room in browser window
                    // window.location.href =  path;
                    var newWindow = window.open(path, "Game", 'width=1064, height=798,toolbar=0, resizable=1, location=0');
                    newWindow.focus();
                    /* Added for R10-843 if window exists change on the actual window */
                    /*
                    if(newWindow.location !== path){
                        newWindow.location.href = path;
                    }
                    */
                    /* location.reload();*/
                }
                
                //this.$body.css("position", "fixed");
                
                // notify that a game was launched
                sb.notify("launched-game", [gameId]);
                sb.hideLoadingAnimation();
                
                // CMA hack
                if (type !== "wheel" && type !== "3Rad") {
                    sb.$(".alertbox a.ok-cta").text("OK");
                }
            }
            else{
                sb.notify("age-id-verification-check-if-verified",[URUVerification], false);
            }
            
        }.bind(this);
        
        // CMA check
        // if player is logged in and if bonus balance is equal to or greater that 0.05 a pop-up will show
        
        var sessionId = sb.getFromStorage("sessionId");
        
        if (sessionId) {
            /* Get Game information
            Note: Look into refactoring R10-705 Since we can get the Info from the array now 
            */
            var game = sb._.find(sbm.games, function (game) {
                return game.id === gameId;
            });
            if(type == "room" || type == "wheel" || type == "3Rad"){
                game= { cma:0};
            }
            else{     
                if(game.hasOwnProperty("cma") === false){
                    game.cma = 0;
                }
            }
            var bonus = parseFloat(sb.getFromStorage("bonus"));
            if (!isNaN(bonus) && bonus >= 0.05 && type!="3Rad"  && game.cma == 0 && type != "wheel"  && type!= "room") {
                var URUVerification = sb.getCookie("URUVerified");
                    if(URUVerification == 1){
                    var msg = "All or part of your account balance consists of bonus funds with associated terms and conditions. You can see more about the bonuses you’ve opted into by navigating to: My Account > Account Activity > Promo";
                    // CMA hacks
                    sb.$(".alertbox a.ok-cta").text("Play");
                    sb.$(".alert-background").css("z-index", 9999999999999);
                    
                    sb.showConfirm(msg, loadGame, undefined, "INFORMATION");
                    sb.sendDataToGoogle("CMA", "Alert displayed");
                }
                else{
                    sb.notify("age-id-verification-check-if-verified",[URUVerification],false);
                }
                    
                } else {
                    loadGame(true);
                }
        } else {
            loadGame(true);
        }
        
    }.bind(this);
    
    /**
    * Closes games iframe
    */
    this.closeGamesIframe = function () {
        
        this.$iframe.fadeOut();
        this.$iframeBorder.removeClass("border");
        this.$background.hide();
        this.$iframeContainer.hide();
        this.$iframe.attr("src", "");
        this.gameLoaded(false);
        this.$btnClose.hide();
        this.$btnMaximize.hide();
        this.$btnMinimize.hide();
        this.$btnQuickDeposit.hide();
        this.$btnToggleFav.hide();

        sb.notify('game-iframe-closed');
        
        // remove event listener for messages from the iframe
        window.removeEventListener("message", this.onGotMessageFromIframe, false);
        sb.$(window).off("resize", this.onWindowResize);
        this.$btnClose.off("click", this.closeGamesIframe);
        sb.notify("close-quick-deposit");
        sb.notify("maximize-last-played");
        sb.hideLoadingAnimation();
        this.$slider.css("visibility", "hidden");
        sb.$('body').removeClass('modal-open');
        
        // stop reality check
        sb.notify("stop-reality-check");

        // Check for PW prize count if the opened window was PW
        if(this.isPW === true){
            this.isPW = false;
        }
        
        sb.hideLoadingAnimation();
        
        this.sliderLoaded = false;
        
        // redirect to start?
        var redirectStart = location.pathname === "/" || location.pathname.indexOf("welcome") > -1 || location.pathname.indexOf("cashier/thank") > -1;
        
        if (sb.getFromStorage("sessionId") && redirectStart) {
            sb.initRoute("/start/");
        } else {
            sb.initRoute(location.pathname);
        }
        
    }.bind(this);
    
    /**
    * Got game dimensions
    * @param e event
    */
    this.onGotMessageFromIframe = function (e) {
        if (e.data === "close-game") {
            
            this.closeGamesIframe();
            
        } else {
            
            var dimensions = JSON.parse(e.data);
            if (dimensions.width && dimensions.height) {
                this.gameDimensions = {
                    w: dimensions.width,
                    h: dimensions.height
                };
                this.adjustGameDimensions();
            }
        }
        
    }.bind(this);
    
    /**
    * Adjust game dimensions
    * If window width is bigger than game width, we set the game dimensions
    * If window width is lower than game width, we set the window width and calculate the proportional height
    * @param w width
    * @param h height
    */
    this.adjustGameDimensions = function (fullscreen) {
        
        var newW, newH,
        
        windowW = $("body").innerWidth(),
        windowH = $("body").innerHeight();
        
        if (fullscreen) {
            
            sb.notify("minimize-last-played");
            // check which size of screen is the wider
            if (windowW > windowH) {
                
                // width is wider - expand full height and then calculating the right width maintaining the aspect ratio
                newH = windowH + this.paddingTop;
                newW = (this.gameDimensions.w * newH) / this.gameDimensions.h;
                
            } else {
                
                // height is wider - expand full width and then calculating the right height maintaining the aspect ratio
                newW = windowW;
                newH = (this.gameDimensions.w * windowH) / this.gameDimensions.h;
                
                
            }
            
            // set full screen dimensions
            if (newW && newH) {
                this.$iframeBorder.width(newW).height(newH);
                // recommended games height
                this.$slider.height(newH - this.topBarHeight);
            }
            
        } else {
            
            sb.notify("maximize-last-played");
            // if game fits on window - no adjustment needed
            if (this.gameDimensions.w <= windowW && (this.gameDimensions.h <= windowH)) {
                this.$iframeBorder.width(this.gameDimensions.w).height(this.gameDimensions.h);
                // recommended games height
                this.$slider.height(this.gameDimensions.h - this.topBarHeight);
                
            } else {
                
                // adjustment to the game dimensions is necessary
                
                newW = (windowH * this.gameDimensions.w) / this.gameDimensions.h;
                while (newW > windowW) {
                    windowH -= 10;
                    newW = (windowH * this.gameDimensions.w) / this.gameDimensions.h;
                }
                // Hack for Prize Wheel
                if(this.gameDimensions.w === 960 && this.gameDimensions === 540){
                    if(windowW >= 960 && windowH >= 540){
                        newW = 960;
                        newH = 540;
                    }
                }
                this.$iframeBorder.width(newW).height(windowH);
                // recommended games height
                this.$slider.height(this.gameDimensions.h - this.topBarHeight);
                
            }
        }
        
        this.sliderLoaded && this.$slider.reloadSlider();
    };
    
    this.onWindowResize = function () {
        setTimeout(function () {
            this.adjustGameDimensions(this.fullscreen);
        }.bind(this), 1000);
    }.bind(this);
    
    this.goFullScreen = function () {
        this.$btnMaximize.hide();
        this.$btnMinimize.show();
        this.adjustGameDimensions(true);
        this.fullscreen = true;
    }.bind(this);
    
    this.restoreSize = function () {
        this.$btnMinimize.hide();
        this.$btnMaximize.show();
        this.adjustGameDimensions(false);
        this.fullscreen = false;
    }.bind(this);
    
    this.setBackground = function (gameId) {
        var g = sb._.find(sbm.games, function (game) {
            return game.id === gameId;
        });
        
        if (g && g.bg) {
            
            $.get(g.bg, function () {
                this.$background.css("background-image", "url('" + g.bg + "')");
            }.bind(this));
            
        } else {
            this.$background.css("background-image", "url('/_images/common/black-70-opacity.png')");
        }
        this.$background.fadeIn(1000);
    };
    
    // got recomended games from AJAX request
    this.gotRecommendedGames = function (e, data) {
        
        if (data.Code === 0) {
            var $games = this.filterCurrent(data.Games);
            if (sbm.serverconfig.bingoUrl && data.Games && data.Games.length > 0) {
                var $bingoSlide = {
                    Image :     "/_global-library/_upload-images/games/list-icons/play-bingo-icon.jpg",
                    Image1:     "/_global-library/_upload-images/games/list-icons/play-bingo-icon.jpg",
                    Intro:      "Bingo",
                    DetailsURL: "/bingo/"
                };
                $games.unshift($bingoSlide);
            }
            this.recommendedGames($games);
            var setSlider = function () {
                
                var imgH = this.$slider.find("img").first().height(),
                recommendedPanelH = this.$recommendedPanel.height(),
                slides;
                
                slides = Math.floor((recommendedPanelH - 80) / imgH);
                
                this.$slider.destroySlider && this.$slider.destroySlider();
                
                this.$slider.bxSlider({
                    
                    mode: 'vertical',
                    minSlides: slides,
                    pager: false,
                    auto: false,
                    autoStart:true,
                    autoDelay:10000,
                    speed: 3000,
                    pause:5000,
                    infiniteLoop: false,
                    hideControlOnEnd: true,
                    touchEnabled: false,
                    slideMargin:7,
                    prevText : "",
                    nextText : "",
                    
                    
                    //responsive: true,
                    //adaptiveHeight: true,
                    onSliderLoad: function(){
                        this.$slider.css("visibility", "visible");
                        this.sliderLoaded = true;
                        // this.reloadSlider();
                        
                    }.bind(this)
                });
                
            }.bind(this);
            
            if (data.Games && data.Games.length) {
                // show slider
                setTimeout(setSlider,1000);
                
            } else {
                
                // hide recommendations panel
                this.$recommendedPanel.hide();
            }
            
            
            
        }
        
    }.bind(this);
    
    
    this.launchRecommended = function (game) {
        /*********************/
        var fun = sb.getFromStorage("sessionId") ? 0 : 1;
        sb.sendDataToGoogle("Recommended Game Launch",  game.DetailsURL);
        /*sending info to our tracking system*/
        var label = this.currentGameId+'|'+game.DetailsURL;
        sb._.each(this.recommendedGames(), function (mgame) {
            label +=  "|" + mgame.DetailsURL;
        });
        sb.trackNavigation("launchRecommended",label);
        /*********************/
        if (game.DetailsURL === "/bingo/") {
            this.openGameWindow(null, null, false, "room");
        } else {
            this.openGameWindow(null, fun, game.DetailsURL);
        }
    }.bind(this);
    
    // removes the current game being played from the array of games received
    this.filterCurrent = function (games) {
        
        var i = 0;
        
        for (i; i < games.length; i++) {
            if (games[i].DetailsURL === this.currentGameId) {
                games.splice(i,1);
            }
        }
        
        return games;
    };
    
};