var sbm = sbm || {};

/**
 * Last played games
 * Saves list of last played games on local storage
 * Launches game from the list
 * @param sb The sandbox
 * @constructor
 */
sbm.LastPlayed = function (sb) {
    "use strict";

    this.maxGamesToDisplay = 10;
    this.lastGames = sb.ko.observableArray([]);
    this.$container = sb.$("#ui-last-played-container");
    this.$mobileContainer = sb.$("#ui-last-played-mobile-container");
    this.minimized = sb.ko.observable(false);
    this.toggleText = sb.ko.observable("Hide");

    /**
     * Module initialization
     */
    this.init = function () {
        // workaround - remove previous added games
        this.$container.html($("#ui-lastplayed-tmpl").html());
        this.$mobileContainer.html($("#ui-lastplayed-mobile-tmpl").html());

        sb.listen("add-last-game", this.add);
        sb.listen("minimize-last-played", this.minimize);
        sb.listen("maximize-last-played", this.maximize);

        this.loadLastPlayed();

        sb.$(document).on({
             mouseenter: function () {
                sb.$(this).removeClass("last-played-hover zoomin");
                sb.$(this).addClass("last-played-hover zoomout");
            },
            mouseleave: function () {
                sb.$(this).removeClass("last-played-hover zoomout");
                sb.$(this).addClass("last-played-hover zoomin");
            }
        }, ".last-played-game");

    };

    /**
     * Module destruction
     */
    this.destroy = function () {
        sb.ignore("add-last-game");
        sb.ignore("minimize-last-played");
        sb.ignore("maximize-last-played");
    };

    /**
     * get last games from storage if any
     */
    this.loadLastPlayed = function () {
        var played = JSON.parse(sb.getFromStorage("lastPlayed"));
        if (played) {
            // check if data is correct
            if (played[0].id) {
                this.lastGames(played);
            } else {
                sb.saveOnStorage("lastPlayed", []);
            }
        }
    };

    /**
     * Adds a game to the list
     * Checks for max games to be displayed removes the oldest one
     */
    this.add = function (e, gameId) {

        var game = sb._.find(sbm.games, function (game) {
            return game.id === gameId;
        });

        if (!game) {
            sb.logError("Could not add game to last played list");
            return;
        }

        var games = this.lastGames();

        // check if the game is already on the list

        var i = 0, index = -1;
        for (i; i < games.length; i++) {
            if (games[i].id === gameId) {
                index = i;
                break;
            }
        }

        // is already included?
        if (index > -1) {

            // remove from the list
            games.splice(index, 1);
            // add as first
            games.unshift(game);

        } else {

            // add it as first
            games.unshift(game);

            // needs to remove the oldest?
            if (games.length > this.maxGamesToDisplay) {
                games.pop();
            }
        }

        this.lastGames(games);

        // save list
        this.save();

    }.bind(this);

    /**
     * Launch a game from the list
     * If has session - launches game
     * If no session - launch the info modal box
     */
    this.launch = function (game) {

        // push the game to the stack in order to be added to the last played games

        sbm.games = sbm.games || [];
        
        sbm.games.push({
            "id": game.id,
            "icon": game.icon,
            "bg": game.bg,
            "title": game.title,
            "type": game.type,
            "desc": game.desc,
            "thumb": game.thumb,
            "detUrl": game.detUrl
        });
        
        if (sb.getFromStorage("sessionId")) {
            
            if (game.type === "game") {
                sb.notify("launch-game", [game.id, 0, 1, null, null, null, false, game.bg]);
                sb.sendDataToGoogle("Last Played Game Launch",  game.id);
            } else {
                sb.notify("launch-bingo-room", [game.id, sb.getFromStorage("playerId"), sb.getFromStorage("username"), false]);
                sb.sendDataToGoogle("Last Played Room Launch",  game.id);
            }

        } else {

            if (game.type === "game") {

                sb.notify("open-game-info-modal", [game, false]);
            } else {
                sb.notify("open-room-info-modal", [game.id, game.thumb]);
            }

        }

    }.bind(this);

    /**
     * Save list to local storage
     */
    this.save = function () {
        sb.saveOnStorage("lastPlayed",  JSON.stringify(this.lastGames()));
    };

    this.toggle = function () {
        var minimized = this.minimized();
        this.minimized(!minimized);

        if (minimized) {
            this.maximize();
        } else {
            this.minimize();
        }
    }.bind(this);

    this.minimize = function () {
        this.minimized(true);
        this.toggleText("Show Last Played");
        sb.$('.last-played-game').removeClass("zoomin");
        sb.$('.last-played-toggle').addClass("minimized");
    }.bind(this);

    this.maximize = function () {
        this.minimized(false);
        this.toggleText("Hide");
        sb.$('.last-played-toggle').removeClass("minimized");
    }.bind(this);
};