var sbm = sbm || {};

sbm.GamesMenuSearch = function (sb) {
    "use strict";

    // subscriptions

    this.onSearchTermChanged = function (newValue) {

        // cleared the search term
        if (newValue === "") {
            this.gamesFound([]);
            return;
        }

        // exit if term is shorter than 3 chars
        if (newValue.length < 3) return;

        // do search
        sb.notify("search-games", [newValue]);
        this.searchStarted(true);
        this.searchFinished(false);

    }.bind(this);

    // properties

    this.searchTerm = sb.ko.observable("").extend({ rateLimit: 500 });
    this.searchTerm.subscribe(this.onSearchTermChanged);
    this.gamesFound = sb.ko.observableArray([]);
    this.position = -1;
    this.searchStarted = sb.ko.observable(false);
    this.searchFinished = sb.ko.observable(false);

    // methods

    this.init = function () {

        // remove previous added games
        sb.$("#ui-mobile-search-results-container").html(sb.$("#ui-mobile-search-results-tmpl").html());
        sb.$(".ui-desktop-search-results-container").html(sb.$("#ui-desktop-search-results-tmpl").html()); // selector needs to be by class!

        sb.listen("searched-games", this.onSearchedGames);

        var self = this;

        sb.$(".ui-search-input").on("keydown", function (e) {

            // remove highlight
            sb.$(".ui-desktop-search-results-container > li").removeClass("search-result-highlight");

            switch (e.keyCode) {
                case 38:

                    if (self.position > 0) {
                        self.position--;
                    }
                    break;

                case 40:

                    if (self.position < self.gamesFound().length - 1) {
                        self.position++
                    }
                    break;

                case 13:

                    self.openGame(self.gamesFound()[self.position]);
                    break;
            }

            // add highlight
            sb.$(this).parent().find("ul > li").eq(self.position).addClass("search-result-highlight");

            // scroll
           sb.$(this).parent().find("ul").scrollTop(63 * (self.position - 3));
        });
    };

    this.destroy = function () {
        sb.ignore("searched-games");
    };

    /**
     * Do AJAX request to server to get the games that fill the search criteria
     */
    this.onSearchedGames = function (e, data) {
        var games = [];
        if (data.Games.length > 0) {
            sb._.each(data.Games, function (game) {
                if (game.Title.indexOf("Minigame") === -1) {
                    games.push({
                        id: game.ID,
                        title: game.Title,
                        desc: game.Intro,
                        icon: game.Image,
                        thumb: game.Image1,
                        detUrl: game.DetailsURL,
                        demo: game.DemoPlay,
                        bg: game.Text1,
                        cma: game.CMAFriendly
                    });
                }
            }.bind(this));

            $("html").one("click", function () {
                this.gamesFound([]);
                this.searchTerm("");
            }.bind(this));
        }

        this.position = -1;
        this.gamesFound(games);
        this.searchStarted(false);
        this.searchFinished(true);

    }.bind(this);

    /**
     * Handles the click on the search reasult item
     * @param id - the game Id
     */
    this.openGame = function (game, e) {

        // launch game
        this.launchGame(game, e);

    }.bind(this);

    /**
     * Launch game
     * if logged in - launch game
     * if not logged in - open info
     */
    this.launchGame = function (game, e) {

        // ripple effect
        this.rippleEffect(e);

        // close menu by clearing the search results and clear the search term
        this.searchTerm("");
        this.gamesFound([]);

        // push the game to the stack in order to be added to the last played games

        sbm.games = sbm.games || [];

        sbm.games.push({
            "id": game.id,
            "icon": game.icon,
            "bg": game.bg,
            "title": game.title,
            "type": "game",
            "desc": game.desc,
            "thumb": game.thumb,
            "detUrl": game.detUrl,
            "CMAFriendly": game.cma
        });
        if (sb.getFromStorage("sessionId")) {

            // launch game
            sb.notify("launch-game-window", [0, game.id, "game",{"CMA":game.cma}]);
            sb.notify("add-last-game", [game.id]);

        } else {

            // open game info
            sb.notify("open-game-info-modal", [game, game.demo]);
        }

    }.bind(this);

    /**
     * Riiple effect
     * @param event Click event
     */
    this.rippleEffect = function (e) {

        if (!e) return;

        //jQuery for click effect on search results screen to show growing circle background
        var parent, ink, d, x, y;

        parent = sb.$(sb.$(e.currentTarget).find("a")).parent();
        //create .ink element if it doesn't exist
        if(parent.find(".ink").length == 0)
            parent.prepend("<span class='ink'></span>");

        ink = parent.find(".ink");
        //in case of quick double clicks stop the previous animation
        ink.removeClass("animate");

        //set size of .ink
        if(!ink.height() && !ink.width())
        {
            //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
            d = Math.max(parent.outerWidth(), parent.outerHeight());
            ink.css({height: d, width: d});
        }

        //get click coordinates
        //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
        x = e.pageX - parent.offset().left - ink.width()/2;
        y = e.pageY - parent.offset().top - ink.height()/2;

        //set the position and add class .animate
        ink.css({top: y+'px', left: x+'px'}).addClass("animate");
    };
};