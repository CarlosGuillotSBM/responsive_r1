var sbm = sbm || {};

/**
 * Module to display the games in a table
 * @param sb The Sandbox
 * @param options Options
 * @constructor
 */
sbm.GamesDisplay = function (sb, options) {
    "use strict";

    // holds the current order settings
    this.order = {};

    // save current order settings
    this.setOrder = function (index, type) {
        this.order = {
            index: index,
            type: type
        };
    };

    // text to display on the button that toggles the view mode

    this.displayModeChanged = function (newVal) {
        if (newVal === "icons") {
            this.listIcon("list-icon.svg");
            this.gridIcon("grid-icon-active.svg");
        } else {
            this.listIcon("list-icon-active.svg");
            this.gridIcon("grid-icon.svg");
        }

    }.bind(this);

    this.displayMode = sb.ko.observable(options.defaultMode);
    this.displayMode.subscribe(this.displayModeChanged);

    // icons to show on the elements that toggle the view mode
    this.listIcon = sb.ko.observable("list-icon.svg");
    this.gridIcon = sb.ko.observable("grid-icon-active.svg");

    // module initialization
    this.init = function () {
        this.orderByColumn(0, "STRING", "ASC");
        this.setOrder(0, "ASC");
    };

    this.toggleMode = function (vm, e) {
        var $el = sb.$(e.currentTarget);
        if ($el.hasClass("active")) return;

        sb.$(".games-text-switcher").removeClass("active");

        var mode = $el.data("mode");
        sb.$("[data-mode=" + mode + "]").addClass("active");
        this.displayMode(this.displayMode() === "icons" ? "text" : "icons");
    }.bind(this);

    // new sorting - called on the HTML
    this.orderBy = function (index, fieldType) {

        if (this.order.index === index) {
            if (this.order.type === "ASC") {
                this.orderByColumn(index, fieldType, "DESC");
                this.setOrder(index, "DESC");
                return;
            }
        }
        this.orderByColumn(index, fieldType, "ASC");
        this.setOrder(index, "ASC");

    }.bind(this);

    /**
     * Order rows on the table
     * @param colIndex Column index
     * @param fieldType Type of field
     * @param type Order type ("ASC" or "DESC")
     */
    this.orderByColumn = function (colIndex, fieldType, orderType) {

        // get all tables
        var $tables = sb.$(".ui-text-only-table");

        // loop through all the tables

        var linesA, linesB;

        $tables.each(function () {

            // for each table lets order its rows

            sb.$(this).find("tr").not(':first').sortElements(function(a, b){

                if (orderType === "ASC") {

                    if (fieldType === "STRING") {
                        return $(a).find("td").eq(colIndex).text() > $(b).find("td").eq(colIndex).text() ? 1 : -1;
                    } else {

                        linesA = $(a).find("td").eq(colIndex).text();
                        linesB = $(b).find("td").eq(colIndex).text();
                        if (!linesA || isNaN(linesA)) return 1;
                        if (!linesB || isNaN(linesB)) return -1;
                        return parseInt(linesA, 10) > parseInt(linesB, 10) ? 1 : -1;

                        //return parseInt($(a).find("td").eq(colIndex).text(), 10) > parseInt($(b).find("td").eq(colIndex).text(), 10) ? 1 : -1;
                    }

                } else {
                    if (fieldType === "STRING") {
                        return $(a).find("td").eq(colIndex).text() < $(b).find("td").eq(colIndex).text() ? 1 : -1;
                    } else {

                        linesA = $(a).find("td").eq(colIndex).text();
                        linesB = $(b).find("td").eq(colIndex).text();
                        if (!linesA || isNaN(linesA)) return 1;
                        if (!linesB || isNaN(linesB)) return -1;
                        return parseInt(linesA, 10) > parseInt(linesB, 10) ? -1 : 1;

                        //return parseInt($(a).find("td").eq(colIndex).text(), 10) < parseInt($(b).find("td").eq(colIndex).text(), 10) ? 1 : -1;
                    }

                }
            });

        });
    };
};