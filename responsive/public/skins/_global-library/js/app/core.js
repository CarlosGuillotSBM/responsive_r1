var sbm = sbm || {};

/**
 * Main library
 * Uses jQuery, Knockout, underscore
 * Responsible for starting services
 *
 * @param $ jQuery library
 * @param ko Knockout library
 * @param _ underscore library
 * @param serverconfig server configurations - set on /aplication/config/config.php
 * @param skinconfig - the skin configuration set on the config.js for each skin
 */
sbm.Core = function ($, ko, _, serverconfig, skinconfig) {

    "use strict";

    var self = this;

    // exposing libraries and services

    self.$ = $;
    self.ko = ko;
    self._ = _;
    self.o = $({});
    self.dataservice = null;
    self.logger = null;
    self.router = null;
    self.storage = null;
    self.googleManager = null;
    self.serverconfig = serverconfig;

    /**
     * Starts necessary services
     * Starts the router
     * @param path Page path
     */
    self.start = function () {

        self.googleManager = new sbm.GoogleManager(self, window.dataLayer);
        self.googleManager.init();

        self.router = new sbm.Router(self, skinconfig);
        self.logger = new sbm.Logger();
        self.storage = new sbm.Storage(self);

        self.dataservice = {};
        self.dataservice.web = new sbm.DataserviceWeb(self, serverconfig.webUrl || "");
        self.dataservice.cashier = new sbm.DataserviceCashier(self, serverconfig.cashierUrl);
        self.dataservice.playerManagement = new sbm.DataservicePlayerManagement(self, serverconfig.webUrl);
        self.dataservice.threeRadical = new sbm.DataserviceThreeRadical(self, serverconfig.webUrl);
        self.dataservice.sockets = new sbm.DataserviceSockets(self);
        self.dataservice.prizeWheel = new sbm.DataservicePrizeWheel(self);

        self.dataservice.web.init();
        self.dataservice.cashier.init();
        self.dataservice.playerManagement.init();
        self.dataservice.threeRadical.init();
        self.dataservice.prizeWheel.init();
        if(sbm.serverconfig.socketsEnabled == 1){
            self.dataservice.sockets.init();
        }

        if (sbm.serverconfig.editMode) {
            self.dataservice.admin = new sbm.DataserviceAdmin(self);
            self.dataservice.admin.init();
        }

        self.storage.init();

        // [DEV-9861] go native integration
        if (navigator.userAgent.indexOf('gonative') > -1) {               
            sbm.native.init(self);
            // is it a search request?
            var gameSearchTerm = self.getUrlParameterByName("gst");
            if (gameSearchTerm) {
                self.publish("search-games", [gameSearchTerm]);
            }
        }

        // on document ready initialize route
        $(function () {
            self.router.init();
        });

    };

    /**
     * subscribe event
     */
    self.subscribe = function () {
        self.o.on.apply(self.o, arguments);
    };

    /**
     * unsubscribe event
     */
    self.unsubscribe = function () {
        self.o.off.apply(self.o, arguments);
    };

    /**
     * publish event
     */
    self.publish = function () {
        self.o.trigger.apply(self.o, arguments);
    };

    /**
     * Get the type of device
     * @returns {string} web/tablet/phone
     */
    self.getDevice = function () {
        var device = sbm.serverconfig.device;
        if (device === 2) {
            return "tablet";
        }
        if (device === 3) {
            return "phone";
        }

        return "web";
    };

    /**
     * get URL query parameter
     * @param name Parameter's name
     * @returns {string}
     */
    self.getUrlParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    /**
     * Does a POST to a page with the passed parameters
     * @param path the page path
     * @param params the post parameters to send in the request
     */
    self.postToUrl = function (path, params) {

        if (!path) {
            self.publish("show-notification-error", ["Sorry there is a system error, if the error persists contact support", "alert"]);
            self.logger.log("core/postToUrl - path undefined", self.logger.severity.ERROR);
            return;
        }

        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", path);

        // target depends on the type of device
        // new window for desktop or same window for the other devices
        var target = self.getDevice() === "web" ? "_blank" : "_self";
        form.setAttribute("target", target);

        var key;
        for (key in params) {
            if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();

    };

    /**
     * Get i18n term
     * @param key the term key
     * @param params array with values to be replaced on the term
     * @returns {string}
     */
    self.getTranslation = function (key, params) {
        return sbm.i18n.get(key, params);
    };


    /**
     * It retrieves the translated name of the required skinId
     * @returns {string}
     */
    self.getPlayerClassName = function (playerClassID) {
        return sbm.i18n.get(level+playerClassID, params);
    };

    /**
     *
     * @param gtag
     */
    self.sendToGoogleManager = function (gtag) {
        this.googleManager.pushToDataLayer(gtag);
    };

    /**
     * Send a variable to the GTM
     * @param key string
     * @param value
     */
    self.sendVariableToGoogleManager = function (key, value) {
        this.googleManager.pushVariableToDataLayer(key, value);
    };

    /**
     * Send a variable to the GTM
     * @param gtag
     */
    self.sendSetOfVariablesToGoogleManager = function (variables) {
        this.googleManager.pushSetOfVariablesToDataLayer(variables);
    };

    /**
     * Checks if a given game Id /room Id is a favourite
     * @param game The game Id or room Id
     * @returns {boolean} True if is favourite otherwise false
     */
    self.isFavourite = function (game) {

        var favourites = window.localStorage.getItem("favourites");

        if (!favourites) {
            return false;
        }

        favourites = JSON.parse(favourites);

        var favourite = self._.find(favourites, function (favourite) {
            return favourite.Game === game;
        });

        if (favourite) {
            return true;
        }

        return false;
    };

    return self;

};