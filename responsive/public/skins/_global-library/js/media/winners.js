var sbm = sbm || {};

sbm.media = sbm.media || {};

sbm.media.winners = {

    getAll: function (callback) {

        $.ajax(
            {
                "url": "/data.php",
                "dataType": "json",
                "timeout" : 10000,
                "data": {
                    "f": "getwinners"
                },
                "type": "POST"
            }
        )
            .done(function (data) {
                callback(data);
            });

    }
};