var sbm = sbm || {};

sbm.media = sbm.media || {};

sbm.media.jackpots = {

    getAll: function (callback) {

        $.ajax(
            {
                "url": "/data.php",
                "dataType": "json",
                "timeout" : 10000,
                "data": {
                    "f": "getprogressives"
                },
                "type": "POST"
            }
        )
        .done(function (data) {
            callback(data);
        });

    }
};