/**
 * Starts the application
 * @param skinModules Array with all the modules required on the page
 * @param skinPlugins Array with all the plugins required on the page
 */
(function (skinModules, skinPlugins, sbm) {

    "use strict";

    sbm.skinconfig = sbm.skinconfig || {};

    // adding modules
    if (skinModules && skinModules.length) {
        _.each(skinModules, function (module) {
            sbm.addModule(module);
        });
    }
    
    // adding plugins
    if (skinPlugins && skinPlugins.length) {
        _.each(skinPlugins, function (module) {
            sbm.addPlugin(module);
        });
    }

    // start app
    sbm.core = new sbm.Core($, ko, _, sbm.serverconfig, sbm.skinconfig);
    sbm.core.start();

}(skinModules, skinPlugins, sbm));