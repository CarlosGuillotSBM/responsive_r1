var counter = 0;
var checkInterval = setInterval(function() {
  counter++;
  if (counter > 20) {
    clearInterval(checkInterval)
  }
  // Get the element
    var dateEl = document.getElementById("ui-countdown");
    if (dateEl !== undefined && dateEl !== null) {
      clearInterval(checkInterval);
      // Extract the data value
      var date = dateEl.dataset.endDate; // Jan 01, 2018 15:37:25
      // Set the date we're counting down to
      var countDownDate = new Date(date).getTime();
      // Ensure that's there's a zero in front of a single number.
      function leadZero(n) {
        return n < 10 && n >= 0 ? "0" + n : n;
      }
      // Update the count down every 1 second
      var x = setInterval(function() {
        // Get todays date and time
        var now = new Date().getTime();
        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        // Reset the vars
        days = leadZero(days);
        hours = leadZero(hours);
        minutes = leadZero(minutes);
        seconds = leadZero(seconds);
        // Seperate values into relevant divs
        document.getElementById("ui-countdown-days").innerHTML = days;
        document.getElementById("ui-countdown-hours").innerHTML = hours;
        document.getElementById("ui-countdown-minutes").innerHTML = minutes;
        document.getElementById("ui-countdown-seconds").innerHTML = seconds;
        // If the count down is finished, update so all values are "00"
        if (distance <= 0) {
          clearInterval(x);
          document.getElementById("ui-countdown-days").innerHTML = "00";
          document.getElementById("ui-countdown-hours").innerHTML = "00";
          document.getElementById("ui-countdown-minutes").innerHTML = "00";
          document.getElementById("ui-countdown-seconds").innerHTML = "00";
        }
      }, 1000);
    }
}, 2000);
