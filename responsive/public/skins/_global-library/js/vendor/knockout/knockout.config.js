(function (ko) {

    // ** VALIDATION **

    ko.validation.configure({
        errorClass: 'error'
    });


    // dob format "YYYYMMDD"
    // must be 18 +

    ko.validation.rules.DOB = {
        validator: function (dob, doValidation) {

            var valid = true;

            if (doValidation) {

                // check format

                var matches = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(dob);
                if (matches == null) return false;
                var y = matches[3];
                var m = matches[2] - 1; // month 0-11
                var d = matches[1];

/*
                var matches = /^(\d{4})(\d{2})(\d{2})$/.exec(dob);
                if (matches == null) return false;
                var y = matches[1];
                var m = matches[2] - 1; // month 0-11
                var d = matches[3];                
*/
                var composedDate = new Date(y, m, d);

                valid = composedDate.getDate() == d &&
                    composedDate.getMonth() == m &&
                    composedDate.getFullYear() == y;

                if (!valid) {
                    return false;
                }

                // check if is 18+

                var today = new Date();
                var birthDate = new Date(y, m, d);
                var age = today.getFullYear() - birthDate.getFullYear();
                var month = today.getMonth() - birthDate.getMonth();
                if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }

                return age >= 18;

            }


        },
        message: 'You must be 18+.'
    };

    ko.validation.rules.intlPhoneNumber = {
        validator: function (dob, fieldId) {

            return $('#'+fieldId).intlTelInput('isValidNumber') && $('#'+fieldId).intlTelInput("getNumberType") == intlTelInputUtils.numberType.MOBILE;


        },
        message: "The number isn't valid."
    };    

    ko.validation.registerExtenders();

    // ** CUSTOM BINDINGS **

    // ticker for jackpot amounts
    ko.bindingHandlers.jackpot = {
        init: function (element, valueAccessor) {

            var amount = valueAccessor();

            if (!isNaN(amount)) {

                amount = (amount / 100);

                // decrement the value and auto increment based on timer

                var amountFrom = amount - (amount*0.1) > 0 ? amount - (amount*0.1) : 0;

                $(element).countTo({
                    from: amountFrom,
                    to:  amount,
                    speed: 1200000,
                    refreshInterval: 2000,
                    currencySymbol: window.localStorage.getItem("currencySymbol") || "£"
                });

            }

        }
    };

    // It's used on iFrames and uses the valie of messageHtml instead of any source
    ko.bindingHandlers.bindIframe = {

        init: function () {

        },
        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

            function bindIframe() {

                try {
                    var iframeInit = element.contentWindow.initChildFrame,
                        iframedoc = element.contentDocument.getElementsByTagName("html")[0];
                } catch(e) {
                    // ignored
                }
                if (iframeInit) {
                    iframeInit(ko, valueAccessor());
                }
                else if (iframedoc){
                    iframedoc.setAttribute('data-bind', 'html: messageHtml');
                    ko.applyBindings(valueAccessor(), iframedoc);


                    var $iframe = $("#ui-frame-msg");

                    var setIFrameHeight = function (prevHeight) {

                        var h = $iframe.parent().height();
                        $iframe.height(h);

                        if (h != prevHeight) {
                            setTimeout(function () {
                                setIFrameHeight(h);
                            }, 200);
                        }

                    }
                    setTimeout(function () {
                        setIFrameHeight(0);
                    }, 500);


                }
            }


            ko.utils.registerEventHandler(element, 'load', bindIframe);
        }
    };

    // Favourite games
    // set the right image depending if is favourite or not
    ko.bindingHandlers.favourite = {
        init: function (element, valueAccessor) {

            var gameId = valueAccessor();
            var $elem = $(element);
            var favourites = ($.cookie("SessionID")) ? window.localStorage.getItem("favourites") : false;
            if (!favourites) {
                $elem.addClass("icons-star--off");
                return;
            }

            favourites = JSON.parse(favourites);

            var favourite = ko.utils.arrayFirst(favourites, function (item) {
                return item.Game == gameId;
            });

            $elem.removeClass("icons-star--on");
            $elem.removeClass("icons-star--off");
            if (favourite) {
                $elem.addClass("icons-star--on");
            } else {
                $elem.addClass("icons-star--off");
            }
        }
    };

    // Display favourite game based on the index passed on the data binding
    ko.bindingHandlers.showfavourite = {
        init: function (element, valueAccessor) {

            var index = valueAccessor();
            var $elem = $(element);
            var favourites = window.localStorage.getItem("favourites");
            if (!favourites) {
                return;
            }

            favourites = JSON.parse(favourites);

            var favourite = null;
            if (favourites.length >= index) {
                favourite = favourites[index];
            }

            if (favourite) {
                var $iconTemplate = $(".game-icon");
                var html = $iconTemplate.length ? $($iconTemplate[0]).clone() : "";

                if (html) {
                    // change visuals
                    $elem.removeClass("favourite-game-widget");
                    $elem.html(html);
                    $elem.find(".game-icon__image > img:first").attr("src", "/_global-library/_upload-images/games/list-icons/" + favourite.Icon);

                    // toggle favourite data
                    var $fav = $elem.find(".game-icon__favourite__icon");
                    $fav.attr("data-favgameid", favourite.Game);
                    $fav.attr("alt", favourite.Title);
                    $fav.removeClass("icons-star--off").addClass("icons-star--on");
                    $fav.attr("data-bind", "favourite: " + favourite.Game + ", click: GameLauncher.toggleFavourite");

                    // change data
                    var $elemData = $elem.find(".ui-launch-game");
                    $elemData.attr("data-gameid", favourite.Game);
                    $elemData.attr("data-gamedescription", favourite.Title);
                    $elemData.attr("data-gamedescription", favourite.Description);
                    $elemData.attr("data-details-url", favourite.DetailsURL);
                    $elemData.attr("data-icon", favourite.Screenshot);

                    // set title
                    var title = favourite.Title.length >= 23 ? favourite.Title.substring(0, 23) + "..." : favourite.Title;
                    var $link = $elemData.next("h1").find("a");
                    $link.text(title.toUpperCase()).attr("href", favourite.DetailsURL);

                }

            }

        }
    };

    // subscribe Function to get the old value
    // Way of using it: add it to a variable like variable.subscribeChanged
    ko.subscribable.fn.subscribeChanged = function (callback) {
        var oldValue;
        this.subscribe(function (_oldValue) {
            oldValue = _oldValue;
        }, this, 'beforeChange');

        this.subscribe(function (newValue) {
            callback(newValue, oldValue);
        });
    };    

    // Edit content
    ko.bindingHandlers.edit = {
        init: function (element, valueAccessor) {

            var value = valueAccessor();

            var canEdit = value.page && value.key && sbm.serverconfig.editMode;
            if (!canEdit) {
                $(element).remove();
            } else {
                $(element).show();

                $(element).on("click", function (e) {
                    e.preventDefault();

                    var cacheKey = $("#ui-cache-key").val();

                    var subkey = value.subkey || "";

                    if (!window.editorWindow || !window.editorWindow.window) {
                        if (navigator.appName.indexOf("Microsoft") !== -1) {
                            window.editorWindow = window.open(this.href + "?page=" + value.page + "&key=" + value.key + "&subkey=" + subkey + "&cachekey=" + cacheKey, "height=720,width=900,top=100,left=100,status=1");
                        } else {
                            window.editorWindow = window.open(this.href + "?page=" + value.page + "&key=" + value.key + "&subkey=" + subkey + "&cachekey=" + cacheKey, "EditContent", "height=720,width=900,screenY=100,screenX=100");
                        }
                    } else {
                        editorWindow.focus();
                    }
                });
            }

        }
    };

    // set the default value specified as the text on the DOM element to the observable
    ko.bindingHandlers.textWithDefault = {
        init: function (element, valueAccessor) {
            var observable = valueAccessor();

            observable($(element).text());

            ko.applyBindingsToNode(element, { text: observable });
        }
    };

}(ko));