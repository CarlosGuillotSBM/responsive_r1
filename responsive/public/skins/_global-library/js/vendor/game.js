/**
	Connection
	*/
	

(function( jQuery ) {

if ( window.XDomainRequest && !jQuery.support.cors ) {
	jQuery.ajaxTransport(function( s ) {
		if ( s.crossDomain && s.async ) {
			if ( s.timeout ) {
				s.xdrTimeout = s.timeout;
				delete s.timeout;
			}
			var xdr;
			return {
				send: function( _, complete ) {
					function callback( status, statusText, responses, responseHeaders ) {
						xdr.onload = xdr.onerror = xdr.ontimeout = xdr.onprogress = jQuery.noop;
						xdr = undefined;
						jQuery.event.trigger( "ajaxStop" );
						complete( status, statusText, responses, responseHeaders );
					}
					xdr = new XDomainRequest();
					xdr.open( s.type, s.url );
					xdr.onload = function() {
						var status = 200;
						var message = xdr.responseText;
						var r = JSON.parse(xdr.responseText);
						if (r.StatusCode && r.Message) {
							status = r.StatusCode;
							message = r.Message;
						}
						callback( status , message, { text: message }, "Content-Type: " + xdr.contentType );
					};
					xdr.onerror = function() {
						callback( 500, "Unable to Process Data" );
					};
					xdr.onprogress = function() {};
					if ( s.xdrTimeout ) {
						xdr.ontimeout = function() {
							callback( 0, "timeout" );
						};
						xdr.timeout = s.xdrTimeout;
					}
					xdr.send( ( s.hasContent && s.data ) || null );
				},
				abort: function() {
					if ( xdr ) {
						xdr.onerror = jQuery.noop();
						xdr.abort();
					}
				}
			};
		}
	});
}
});	

function sessionLoginAjax (successFunction, errorFunction) {
    return basicAjax('PlayerID=' + jQuery.cookie('PlayerID') + '&SessionID=' + jQuery.cookie('SessionID'),'WebLoginWithSession', successFunction, errorFunction);
}

	
	function basicAjax(parameters,functionName,successFunction,errorFunction,beforeSendFunction,completeFunction){
	
	if (parameters == undefined) {
		parameters = '';
	}

    if (successFunction == undefined){
		successFunction = function(returnData) {
			return;
		}
	}

    if (errorFunction == undefined){
		errorFunction = function(XMLHttpRequest, textStatus, errorThrown) {
            return;
        }
	}
	
    if (beforeSendFunction == undefined){
		beforeSendFunction = function() {
			return;
		}
	}

    if (completeFunction == undefined){
		completeFunction = function() {
            return;
        }
	}		
	
	var preparameters =	'Key='+globalKey+'&'+
					'Version='+globalVersion+'&'+
					'ResponseFormat='+'JSONP'+'&'+
					'Function='+functionName+'&'+
					'SkinID='+globalSkinID+'&';
  if(functionName == 'WebRegSession' || functionName == 'WebIndex') 
  {
	preparameters = preparameters + 'CountryIPID='+globalCountryIPID+'&';
  }
  preparameters = preparameters + 'PlayerIPAddress=' + globalPlayerIP;
  if (parameters == ""){
	parameters = preparameters
  } else {
	parameters = preparameters + '&' + parameters;
  }
  
  //make ajax call
    return jQuery.ajax({
        url: 'https://wexa.dagacubedev.net/',
		crossDomain:true,
        data: parameters,
        dataType: 'jsonp',		
		beforeSend: function(xhr) {			 
			 beforeSendFunction();
		},
		
		complete: function(){
			 completeFunction();
	    },
        //set time out 30 seconds
        timeout: 30000,
        //on ajax success
        success: function(returnData) {
			successFunction(returnData);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
            errorFunction(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}


/**
	Common bingo game JS functions
 */

var successSessionLogin = function(returnData) {

                    var Code = returnData.Code;

                    if (Code == 0) {
                        var Result = returnData["Data"][0]["ReturnData"];
                        var SessionID = returnData["Data"][0]["SessionID"];
                        var PlayerID = returnData["Data"][0]["PlayerID"];
						var Username = returnData["Data"][0]["Username"];                        
                        var LastLogin = returnData["Data"][0]["LastLoginDate"];
                        var Favourites = returnData["Data"][0]["Favourites"];
                        var Country = returnData["Data"][0]["CountryCode"];
                        var Country2 = returnData["Data"][0]["CountryCode2"];
                        var Currency = returnData["Data"][0]["CurrencyCode"];
                        var Class = returnData["Data"][0]["Class"];
                        var CBID = returnData["Data"][0]["CBID"];

                        if (Result == 0) {
                            
                            //Check for cashback
                            if (CBID != 0) {
                                
                                var CBAmount = returnData["Data"][0]["CBAmount"];
                                var CBdate = returnData["Data"][0]["CBdate"];
                                    jQuery.cookie('CBID', CBID, {
                                    path: '/'
                                });
                                    jQuery.cookie('CBAmount', CBAmount, {
                                    path: '/'
                                });
                                    jQuery.cookie('CBdate', CBdate, {
                                    path: '/'
                                });
                            }
                            else {
                                var CBClaimAmount = returnData["Data"][0]["CBClaimAmount"];
                                var CBClaimdate = returnData["Data"][0]["CBClaimdate"];
                                    if(CBClaimAmount !== null) {
                                        jQuery.cookie('CBID', CBID, {
                                        path: '/'
                                    });
                                        jQuery.cookie('CBClaimAmount', CBClaimAmount, {
                                        path: '/'
                                    });
                                        jQuery.cookie('CBClaimdate', CBClaimdate, {
                                        path: '/'
                                    });
                                    }  

                            } 
                            jQuery.cookie('SessionID', SessionID, {
                                path: '/'
                            });
                            jQuery.cookie('PlayerID', PlayerID, {
                                path: '/'
                            });
                            jQuery.cookie('Username', Username, {
                                path: '/'
                            });
                            jQuery.cookie('LastLogin', LastLogin, {
                                path: '/'
                            });
                            jQuery.cookie('Favourites', Favourites, {
                                path: '/'
                            });
                            jQuery.cookie('Country', Country, {
                                path: '/'
                            });
                            jQuery.cookie('Country2', Country2, {
                                path: '/'
                            });
                            jQuery.cookie('Currency', Currency, {
                                path: '/'
                            });
                            jQuery.cookie('Class', Class, {
                                path: '/'
                            });
                            jQuery('#topLoginForm').submit();

                            window.opener.location.href = servername + "/myaccount";

                        } else if (Result == 1) {
                            spit_error("Sorry, your login details are invalid.");
                        } else if (Result == 2) {
                            spit_error("Sorry, too many failed attempts, check your email.");
                        } else if (Result == 3) {
                            spit_error("Please unlock your account from the security email.");
                        } else if (Result == 4) {
                            spit_error("Sorry, your account is locked. Please contact support.");
                        } else if (Result == 5) {
                            spit_error("Sorry, you have self excluded. Please contact support.");
                        }
                    }


                };
				
var errorSessionLogin = function(XMLHttpRequest, textStatus, errorThrown) {
                    alert('Sorry there is a system error, if the error persists contact support');
                };				
 
//-------------Session login function-------------//
function SessionLogin(sessionid, playerid) {

			sessionLoginAjax (successSessionLogin,errorSessionLogin);
}


	function loginSuccessful(obj) 
	{
		setCookie("SessionID",obj.sessionID);
		setCookie("PlayerID",obj.playerID);
		setCookie("Username",obj.username);
	
		//var parser = document.createElement('a');		
		//parser.href = window.location.href;
		//
		//if (parser.hostname != "bingo.kitty.com"){
			//if(!window.opener){
				//window.open(parser.protocol+"//"+parser.hostname+"/myaccount");
			//}	
			//else
			//{
				//window.opener.location.href = parser.protocol+"//"+parser.hostname+"/myaccount";
			//}
		//}

		SessionLogin(obj.sessionID, obj.playerID);
	}

	
	function goToRegister()
	{
	
		var parser = document.createElement('a');		
		parser.href = window.opener.location.href;
		window.opener.location.href = parser.protocol+"//"+parser.hostname+"/register";

		if (window.opener.progressWindow )			
		{
			window.opener.progressWindow.close()
		}
		window.close();		
	
	}
	
	function goCashier() 
	{			
		jQuery('#cashiercontainer').html('<form method="post" action="' + cashier_url + '" id="cashierform" target="DaGaCubeCashier"><input type="hidden" id="oid" name="oid" value="' + oid + '"><input type="hidden" id="kid" name="kid" value="' + kid + '"><input type="hidden" id="cid" name="cid" value="' + jQuery.cookie("PlayerID") + '"><input type="hidden" id="sid" name="sid" value="' + jQuery.cookie("SessionID") + '"><input type="hidden" id="version" name="version" value="' + version + '"></form>');

            if (navigator.appName.indexOf("Microsoft") != -1) {
                w = window.open("", "DaGaCubeCashier", "height=636,width=840,top=100,left=100,status=1");
            } else {
                w = window.open("", "DaGaCubeCashier", "height=636,width=840,screenY=100,screenX=100");
            }

        jQuery("#cashierform").submit();
		if (w) {w.focus()}	
	}
	
	function liveHelp(link)
	{   
		var lh;
	
		if(link !== undefined && link !== "")
		{
			lh = link;
		}
		else
		{
			//	defaults to bingo Extra
			lh = "https://daub.ehosts.net/netagent/cimlogin.aspx?questid=D34C69EE-C1C5-4924-97D1-068D05200603&portid=52ECDE4B-60C1-444D-95D4-3778F4AF8407";
		}
		
		w = window.open(lh, "liveHelp", "height=480,width=500");		
	}

	
	function reload()
	{
		document.location.reload(true);
		return true;
	}

	function closeWindow()
	{
		window.open('','_self',''); 
		window.close(); 
	}

	function launchGame(url,gameID)
	{
		var gameurl = url + "?gameid=" + gameID;
		
		//alert("launchGame " + gameurl);
		if (navigator.appName.indexOf("Microsoft") != -1) {
			newwindow = window.open(gameurl, 'Spin', 'width=800, height=600, resizable=yes, menubar=no, toolbar=no, scrollbars=no ');
		} else {
			newwindow = window.open(gameurl, 'Spin', 'width=800, height=600, location=no, menubar=no, toolbar=no, scrollbars=no');
		}
		if (window.focus) {newwindow.focus()}	

	}
	
/*sets focus on the SWF when webpage has loaded - required so users can enter text in username textfield without clicking on site first */
   function getAppFocus()
   {
		try{
			document.getElementById("flashcontent").tabIndex = 0;
			document.getElementById("flashcontent").focus();
		}
		catch(e){}
   }	
   
	function setCookie(c_name,value,exdays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}   
	
	
