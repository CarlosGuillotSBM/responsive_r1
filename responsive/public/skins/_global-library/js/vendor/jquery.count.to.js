$.fn.countTo = function (options) {
    // merge the default plugin settings with the custom options
    options = $.extend({}, $.fn.countTo.defaults, options || {});

    // how many times to update the value, and how much to increment the value on each update
    var loops = Math.ceil(options.speed / options.refreshInterval),
        increment = (options.to - options.from) / loops;

    var addCommas = function (str) {
        str=String(str);
        var dot=str.search("\\.");
        var s=(dot!=-1?str.slice(0,dot):str),r="",last=(dot!=-1?str.slice(dot):"");
        for (var i=s.length-1,j=1; i>-1; i--, j++) {
            r=s.charAt(i)+r;
            if (j%3==0&&i!=0) r=","+r;
        }
        return r+last;
    };

    return $(this).each(function() {
        var _this = this,
            loopCount = 0,
            value = options.from,
            interval = setInterval(updateTimer, options.refreshInterval);

        function updateTimer() {
            value += increment;
            loopCount++;
            $(_this).html(options.currencySymbol + ' ' + addCommas(value.toFixed(options.decimals)));

            if (typeof(options.onUpdate) == 'function') {
                options.onUpdate.call(_this, value);
            }

            if (loopCount >= loops) {
                clearInterval(interval);
                value = options.to;

                if (typeof(options.onComplete) == 'function') {
                    options.onComplete.call(_this, value);
                }
            }
        }
    });
};

$.fn.countTo.defaults = {
    from: 0,  // the number the element should start at
    to: 10000000,  // the number the element should end at
    speed: 9999999999,  // how long it should take to count between the target numbers
    refreshInterval: 20000,  // how often the element should be updated
    decimals: 2,  // the number of decimal places to show
    onUpdate: null,  // callback method for every time the element is updated,
    onComplete: null,  // callback method for when the element finishes updating
    currencySymbol: "£" // currency symbol to be appended to the amount
};