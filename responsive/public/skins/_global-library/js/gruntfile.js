﻿module.exports = function(grunt) {



    var copyImages = grunt.option('images') === undefined ? true : grunt.option('images');
    var copyTask = {
        "application folder": {
            expand: true,
            cwd: '../../../../',
            src: 'application/**',
            dest: 'W:/WEB/responsive/'
        },
        "public folder": {
            expand: true,
            cwd: '../../../../',
            src: ['public/**', '!public/skins/_global-library/js/node_modules/**'],
            dest: 'W:/WEB/responsive/'
        }
    };
    if (copyImages == true) {
        var imagesFolder = {
            expand: true,
            cwd: '../../../../',
            src: ['images/**'],
            dest: 'W:/WEB/responsive/'
        };
        var imagesLandingPages = {
            expand: true,
            cwd: '../../../../',
            src: ['landing-pages/**'],
            dest: 'W:/WEB/responsive/'
        };
        copyTask["images folder"] = imagesFolder;
        copyTask["landing-pages folder"] = imagesLandingPages;
    }


    grunt.initConfig({
        sass: { // Task
            dist: { // Target
                options: { // Target options
                    style: 'expanded'
                },
                files: { // Dictionary of files
                    'main.css': '../_sass-compass-foundation/_responsive.luckyvip/scssmain.scss', // 'destination': 'source'
                }
            }
        }
    });


    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        jasmine: {
            pivotal: {
                src: [
                    "app/**/*.js",
                    "test/lib/*.js"
                ],
                options: {
                    '--web-security': false,
                    '--local-to-remote-url-access': true,
                    '--ignore-ssl-errors': true,
                    specs: [
                        "test/spec/config-grunt-task.js",
                        "test/spec/dataservice-web.spec.js",
                        "test/spec/dataservice-cashier.spec.js",
                        "test/spec/dataservice-admin.spec.js"
                    ],
                    vendor: [
                        "bower_components/jquery/dist/jquery.js",
                        "vendor/jquery.cookie.js",
                        "vendor/underscore.js",
                        "vendor/knockout/knockout.js"
                    ]
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            bingoextra: {
                src: [
                    '../../bingoextra/_js/**/*.js',
                    "vendor/fastclick.js"
                ],
                dest: 'build/<%= pkg.name %>.bingoextra.js'
            },
            kittybingo: {
                src: [
                    '../../kittybingo/_js/**/*.js',
                    "vendor/fastclick.js"
                ],
                dest: 'build/<%= pkg.name %>.kittybingo.js'
            },
            luckypantsbingo: {
                src: [
                    '../../luckypantsbingo/_js/**/*.js',
                    "vendor/fastclick.js"
                ],
                dest: 'build/<%= pkg.name %>.luckypantsbingo.js'
            },
            magicalvegas: {
                src: [
                    '../../magicalvegas/_js/**/*.js',
                    "vendor/fastclick.js",
                    "vendor/owl.carousel.min.js"
                ],
                dest: 'build/<%= pkg.name %>.magicalvegas.js'
            },
            kingjack: {
                src: [
                    '../../kingjack/_js/**/*.js',
                    "vendor/fastclick.js",
                    "vendor/owl.carousel.min.js"
                ],
                dest: 'build/<%= pkg.name %>.kingjack.js'
            },
            spinandwin: {
                src: [
                    '../../spinandwin/_js/**/*.js',
                    "vendor/fastclick.js"
                ],
                dest: 'build/<%= pkg.name %>.spinandwin.js'
            },
            luckyvip: {
                src: [
                    '../../luckyvip/_js/**/*.js',
                    "vendor/fastclick.js"
                ],
                dest: 'build/<%= pkg.name %>.luckyvip.js'
            },
            regalwins: {
                src: [
                    '../../regalwins/_js/**/*.js',
                    "vendor/fastclick.js"
                ],
                dest: 'build/<%= pkg.name %>.regalwins.js'
            },
            givebackbingo: {
                src: ['../../givebackbingo/_js/**/*.js'],
                dest: 'build/<%= pkg.name %>.givebackbingo.js'
            },
            aspers: {
                src: [
                    '../../aspers/_js/**/*.js',
                    "vendor/fastclick.js",
                    "vendor/owl.carousel.min.js"
                ],
                dest: 'build/<%= pkg.name %>.aspers.js'
            },
            modules: {
                src: ['app/**/*.js'],
                dest: 'build/<%= pkg.name %>.js'
            },
            vendor: {
                src: [
                    "bower_components/jquery/dist/jquery.js",
                    "vendor/jquery.cookie.js",
                    "vendor/jquery.count.to.js",
                    "vendor/jquery.easing.js",
                    "vendor/jquery-ui.js",
                    "vendor/jquery.ui.touch-punch.js",
                    "bower_components/jquery-slimscroll/jquery.slimscroll.js",
                    "vendor/jquery.lazyload.js",
                    "vendor/jquery.sort-elements.js",
                    "vendor/jquery.bxslider/jquery.bxslider.js",
                    "vendor/intltelinput/js/intltelinput.js",
                    "vendor/intltelinput/js/utils.js",
                    "vendor/dropdown.js",
                    "vendor/datatables.min.js",
                    "bower_components/foundation/js/foundation.js",
                    "bower_components/slick-carousel/slick/slick.js",
                    "bower_components/share-button-master/build/share.js",
                    "bower_components/jquery-placeholder/jquery.placeholder.js",
                    "vendor/underscore.js",
                    "vendor/knockout/knockout.js",
                    "vendor/knockout/knockout.validation.js",
                    "vendor/knockout/knockout.config.js",
                    "vendor/moment.js",
                    "vendor/otherlevels-countdown.js"
                ],
                dest: 'build/<%= pkg.name %>.vendor.js'
            },
            vendor4nonrad: {
                src: [
                    "vendor/underscore.js",
                    "vendor/knockout/knockout.js",
                    "vendor/knockout/knockout.validation.js",
                    "vendor/knockout/knockout.config.js"
                ],
                dest: 'build/<%= pkg.name %>.vendor4nonrad.js'
            },
            media: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'media/**/*.js'
                ],
                dest: 'build/<%= pkg.name %>.media.js'
            }
        },
        babel: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    "build/sbm.trans.js": "build/sbm.js"
                }
            }
        },
        uglify: {
            options: {
                banner: '/**\n' +
                    '* Aqua Gaming Source Code - Confidential Material\n' +
                    '* Copyright © <%= grunt.template.today("yyyy") %> Aqua Gaming. All Rights Reserved.\n' +
                    '* @date <%= grunt.template.today("dd-mm-yyyy HH:MM") %> \n' +
                    '**/\n'
            },
            all: {
                files: {
                    'build/<%= pkg.name %>.min.js': 'build/sbm.trans.js',
                    'build/<%= pkg.name %>.bingoextra.min.js': ['<%= concat.bingoextra.dest %>'],
                    'build/<%= pkg.name %>.kittybingo.min.js': ['<%= concat.kittybingo.dest %>'],
                    'build/<%= pkg.name %>.luckypantsbingo.min.js': ['<%= concat.luckypantsbingo.dest %>'],
                    'build/<%= pkg.name %>.magicalvegas.min.js': ['<%= concat.magicalvegas.dest %>'],
                    'build/<%= pkg.name %>.kingjack.min.js': ['<%= concat.kingjack.dest %>'],
                    'build/<%= pkg.name %>.givebackbingo.min.js': ['<%= concat.givebackbingo.dest %>'],
                    'build/<%= pkg.name %>.luckyvip.min.js': ['<%= concat.luckyvip.dest %>'],
                    'build/<%= pkg.name %>.regalwins.min.js': ['<%= concat.regalwins.dest %>'],
                    'build/<%= pkg.name %>.spinandwin.min.js': ['<%= concat.spinandwin.dest %>'],
                    'build/<%= pkg.name %>.aspers.min.js': ['<%= concat.aspers.dest %>'],
                    'build/<%= pkg.name %>.vendor.min.js': ['<%= concat.vendor.dest %>'],
                    'build/<%= pkg.name %>.vendor4nonrad.min.js': ['<%= concat.vendor4nonrad.dest %>'],
                    'build/<%= pkg.name %>.media.min.js': ['<%= concat.media.dest %>']
                }
            },
            app: {
                files: {
                    'app.min.js': ['app.js']
                }
            }
        },
        responsive_images: {
            tabletGameIcons: {
                options: {
                    sizes: [{
                        name: "medium",
                        rename: false,
                        width: 224
                    }]
                },
                files: [{
                    expand: true,
                    src: ["**/*.{gif,jpg,png}"],
                    cwd: "../../../../images/_global-library/_upload-images/games/list-icons/",
                    dest: "../../../../images/_global-library/_upload-images/games/list-icons-224"
                }]
            },
            phoneGameIcons: {
                options: {
                    sizes: [{
                        name: "small",
                        rename: false,
                        width: 122
                    }]
                },
                files: [{
                    expand: true,
                    src: ["**/*.{gif,jpg,png}"],
                    cwd: "../../../../images/_global-library/_upload-images/games/list-icons/",
                    dest: "../../../../images/_global-library/_upload-images/games/list-icons-122"
                }]
            },
            tabletBingoIcons: {
                options: {
                    sizes: [{
                        name: "medium",
                        rename: false,
                        width: 224
                    }]
                },
                files: [{
                    expand: true,
                    src: ["**/*.{gif,jpg,png}"],
                    cwd: "../../../../images/bingoextra/_upload-images/bingo-rooms/list-icons/",
                    dest: "../../../../images/bingoextra/_upload-images/bingo-rooms/list-icons-224"
                }]
            },
            phoneBingoIcons: {
                options: {
                    sizes: [{
                        name: "small",
                        rename: false,
                        width: 122
                    }]
                },
                files: [{
                    expand: true,
                    src: ["**/*.{gif,jpg,png}"],
                    cwd: "../../../../images/bingoextra/_upload-images/bingo-rooms/list-icons/",
                    dest: "../../../../images/bingoextra/_upload-images/bingo-rooms/list-icons-122"
                }]
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    '../../magicalvegas/_css/main.css': ['../../magicalvegas/_css/main.css'],
                    '../../kingjack/_css/main.css': ['../../kingjack/_css/main.css'],
                    '../../spinandwin/_css/main.css': ['../../spinandwin/_css/main.css'],
                    '../../bingoextra/_css/main.css': ['../../bingoextra/_css/main.css'],
                    '../../luckypantsbingo/_css/main.css': ['../../luckypantsbingo/_css/main.css'],
                    '../../luckyvip/_css/main.css': ['../../luckyvip/_css/main.css'],
                    '../../regalwins/_css/main.css': ['../../regalwins/_css/main.css'],
                    '../../givebackbingo/_css/main.css': ['../../givebackbingo/_css/main.css'],
                    '../../aspers/_css/main.css': ['../../aspers/_css/main.css']
                }
            }
        },
        copy: copyTask,
        sync: {
            "all": {
                files: [{
                    cwd: '../../../../',
                    src: [
                        'application/**',
                        'public/**',
                        '!public/skins/_global-library/js/node_modules/**'
                    ],
                    dest: 'W:/WEB/responsive/'
                }],
                pretend: false,
                verbose: true,
                updateAndDelete: true
            }
        },
        replace: {
            dist: {
                options: {
                    patterns: [{
                        match: "Replace",
                        replacement: "<%= grunt.option('pv') %>"
                    }, {
                        match: "DbPwd",
                        replacement: "<%= grunt.option('pdbpwd') %>"
                    }]
                },
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['../../../../application/config/config.php'],
                    dest: '../../../../application/config/'
                }]
            }
        },
        imagemin: {
            spin_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/spinandwin/_images/",
                    dest: "../../../../../images/spinandwin/_images/",
                    ext: '.jpg'
                }]
            },
            spin_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/spinandwin/_upload-images/",
                    dest: "../../../../../images/spinandwin/_upload-images/",
                    ext: '.jpg'
                }]
            },
            spin_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/spinandwin/_images/",
                    dest: "../../../../../images/spinandwin/_images/",
                    ext: '.png'
                }]
            },
            spin_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/spinandwin/_upload-images/",
                    dest: "../../../../../images/spinandwin/_upload-images/",
                    ext: '.png'
                }]
            },
            magical_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/magicalvegas/_images/",
                    dest: "../../../../../images/magicalvegas/_images/",
                    ext: '.jpg'
                }]
            },
            magical_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/magicalvegas/_upload-images/",
                    dest: "../../../../../images/magicalvegas/_upload-images/",
                    ext: '.jpg'
                }]
            },
            magical_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/magicalvegas/_images/",
                    dest: "../../../../../images/magicalvegas/_images/",
                    ext: '.png'
                }]
            },
            magical_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/magicalvegas/_upload-images/",
                    dest: "../../../../../images/magicalvegas/_upload-images/",
                    ext: '.png'
                }]
            },
            kingjack_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/kingjack/_images/",
                    dest: "../../../../../images/kingjack/_images/",
                    ext: '.jpg'
                }]
            },
            kingjack_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/kingjack/_upload-images/",
                    dest: "../../../../../images/kingjack/_upload-images/",
                    ext: '.jpg'
                }]
            },
            kingjack_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/kingjack/_images/",
                    dest: "../../../../../images/kingjack/_images/",
                    ext: '.png'
                }]
            },
            kingjack_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/kingjack/_upload-images/",
                    dest: "../../../../../images/kingjack/_upload-images/",
                    ext: '.png'
                }]
            },
            extra_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/bingoextra/_images/",
                    dest: "../../../../../images/bingoextra/_images/",
                    ext: '.jpg'
                }]
            },
            extra_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/bingoextra/_upload-images/",
                    dest: "../../../../../images/bingoextra/_upload-images/",
                    ext: '.jpg'
                }]
            },
            extra_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/bingoextra/_images/",
                    dest: "../../../../../images/bingoextra/_images/",
                    ext: '.png'
                }]
            },
            extra_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/bingoextra/_upload-images/",
                    dest: "../../../../../images/bingoextra/_upload-images/",
                    ext: '.png'
                }]
            },
            luckypantsbingo_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/luckypantsbingo/_images/",
                    dest: "../../../../../images/luckypantsbingo/_images/",
                    ext: '.jpg'
                }]
            },
            luckypantsbingo_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/luckypantsbingo/_upload-images/",
                    dest: "../../../../../images/luckypantsbingo/_upload-images/",
                    ext: '.jpg'
                }]
            },
            luckypantsbingo_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/luckypantsbingo/_images/",
                    dest: "../../../../../images/luckypantsbingo/_images/",
                    ext: '.png'
                }]
            },
            luckypantsbingo_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/luckypantsbingo/_upload-images/",
                    dest: "../../../../../images/luckypantsbingo/_upload-images/",
                    ext: '.png'
                }]
            },
            kittybingo_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/kittybingo/_images/",
                    dest: "../../../../../images/kittybingo/_images/",
                    ext: '.jpg'
                }]
            },
            kittybingo_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/kittybingo/_upload-images/",
                    dest: "../../../../../images/kittybingo/_upload-images/",
                    ext: '.jpg'
                }]
            },
            kittybingo_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/kittybingo/_images/",
                    dest: "../../../../../images/kittybingo/_images/",
                    ext: '.png'
                }]
            },
            kittybingo_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/kittybingo/_upload-images/",
                    dest: "../../../../../images/kittybingo/_upload-images/",
                    ext: '.png'
                }]
            },
            luckyvip_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/luckyvip/_images/",
                    dest: "../../../../../images/luckyvip/_images/",
                    ext: '.jpg'
                }]
            },
            luckyvip_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/luckyvip/_upload-images/",
                    dest: "../../../../../images/luckyvip/_upload-images/",
                    ext: '.jpg'
                }]
            },
            luckyvip_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/luckyvip/_images/",
                    dest: "../../../../../images/luckyvip/_images/",
                    ext: '.png'
                }]
            },
            luckyvip_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/luckyvip/_upload-images/",
                    dest: "../../../../../images/luckyvip/_upload-images/",
                    ext: '.png'
                }]
            },
            regalwins_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/regalwins/_images/",
                    dest: "../../../../../images/regalwins/_images/",
                    ext: '.jpg'
                }]
            },
            regalwins_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/regalwins/_upload-images/",
                    dest: "../../../../../images/regalwins/_upload-images/",
                    ext: '.jpg'
                }]
            },
            regalwins_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/regalwins/_images/",
                    dest: "../../../../../images/regalwins/_images/",
                    ext: '.png'
                }]
            },
            regalwins_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/regalwins/_upload-images/",
                    dest: "../../../../../images/regalwins/_upload-images/",
                    ext: '.png'
                }]
            },
            givebackbingo_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/givebackbingo/_images/",
                    dest: "../../../../../images/givebackbingo/_images/",
                    ext: '.jpg'
                }]
            },
            givebackbingo_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/givebackbingo/_upload-images/",
                    dest: "../../../../../images/givebackbingo/_upload-images/",
                    ext: '.jpg'
                }]
            },
            givebackbingo_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/givebackbingo/_images/",
                    dest: "../../../../../images/givebackbingo/_images/",
                    ext: '.png'
                }]
            },
            givebackbingo_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/givebackbingo/_upload-images/",
                    dest: "../../../../../images/givebackbingo/_upload-images/",
                    ext: '.png'
                }]
            },
            aspers_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/aspers/_images/",
                    dest: "../../../../../images/aspers/_images/",
                    ext: '.jpg'
                }]
            },
            aspers_upload_images_jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    expand: true,
                    src: ["**/*.jpg"],
                    cwd: "../../../../../images/aspers/_upload-images/",
                    dest: "../../../../../images/aspers/_upload-images/",
                    ext: '.jpg'
                }]
            },
            aspers_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/aspers/_images/",
                    dest: "../../../../../images/aspers/_images/",
                    ext: '.png'
                }]
            },
            aspers_upload_images_png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    src: ["**/*.png"],
                    cwd: "../../../../../images/aspers/_upload-images/",
                    dest: "../../../../../images/aspers/_upload-images/",
                    ext: '.png'
                }]
            }
        },
        clean: ["node_modules"]
    });

    //grunt.loadNpmTasks("grunt-contrib-jasmine");
    grunt.loadNpmTasks("grunt-contrib-uglify-es");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-responsive-images");
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks("grunt-sync");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-babel');

    //grunt.registerTask("test", ["jasmine"]);
    grunt.registerTask("default", ["concat", "uglify", "responsive_images", "cssmin", "copy"]);
    grunt.registerTask("build", ["concat", "uglify", "responsive_images", "cssmin"]);
    grunt.registerTask("javascript", ["concat","babel","uglify"]);
    grunt.registerTask("images", ["responsive_images"]);
    grunt.registerTask("css", ["cssmin"]);
    grunt.registerTask("copy2dev", ["copy"]);
    grunt.registerTask("sync2dev", ["sync"]);
    grunt.registerTask('replacestr', 'replace');
    grunt.registerTask('compress', ['imagemin']);
    grunt.registerTask('clean-folders', ['clean']);
    grunt.registerTask('sass', ['sass']);
};
