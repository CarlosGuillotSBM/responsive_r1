<div class="browser-not-supported" align='center' style="padding: 35px; font-size: 1.20em;">
    We're sorry, we no longer support this browser. Please switch to
    <?php if (config("Device") === 1) {
        $link = "https://www.google.com/chrome/browser/desktop/";
    } else {
        $link = "https://play.google.com/store/apps/details?id=com.android.chrome&hl=en";
    } ?>
    <a href="<?php echo $link;?>">
        Google Chrome
    </a>
</div>
