<div class="browser-not-supported" align='center' style="padding: 35px; font-size: 1.20em;">
    We're sorry, we no longer support this browser. Please switch to
    <a href="http://www.apple.com/safari/">
        Safari
    </a>
</div>
