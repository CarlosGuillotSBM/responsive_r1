<?php
/* Get player data */
$playerID =  (isset($_COOKIE["PlayerID"])) ? $_COOKIE["PlayerID"] : 0;
$sessionID = (isset($_COOKIE["SessionID"])) ? $_COOKIE["SessionID"] : null;
$userName = (isset($_COOKIE["username"])) ? $_COOKIE["username"] : null;
/* Get device information */
$deviceID = config("Device");
$realDeviceID = config("RealDevice");
$baseUrl = config("BingoURL");
/* Extra parameters to be added to the URL */
$roomName = $_REQUEST["r"] ?? "";
$roomParam = (($roomName == "" || $roomName == "Lobby") ? "?Room=Lobby" : "?RoomName=Room_".$roomName);
$playerParam = "&PlayerID=".$playerID;
$sessionParam = "&SessionID=".$sessionID;
$aliasParam = "&Alias=".$userName;
$deviceParam = "&DeviceID=".$deviceID;
$realDeviceParam = "&RealDevice=".$realDeviceID;
$validUrl = "true";
$Message="";
if($playerID === 0 || $sessionID == null || $userName == null){
    ?>
<div class="main-content loginpage">
    <div data-bind="template: { name: 'room-template' , afterRender: LoginBox.setRedirectURLFromExternal.bind($data,'bingo-room?r=<?=$roomName?>')}" >
    </div>
    <!-- title -->
    <script type="text/html" id="room-template">
    </script>
    <div class="login-box--wrapper">
    <?php include "_global-library/partials/login/login-box.php"; ?>
    </div>
    <!-- CONTENT -->
</div>
<style>
.login-box .login-box__form-wrapper h1{
    display: none !important;
}
.login-box{
    float: none;
}
.login-box h3{
    margin-bottom: 1rem;
    text-align: center;
}
.login-box--wrapper{
    background: #fff;
    padding-top: 1rem;
    padding-bottom: 2rem;
}
.login-box--wrapper .registercta{
    display: block;
    float: none;
}
</style>

<script type="application/javascript">
    skinModules.push({
        id: "ExternalToRoom"
    });
    skinModules.push({
        id: "LoginBox"
    })
</script>
<?php
    $Url="";
    $validUrl = "false";
    $Message = "Oops! Your session has expired. Please log in again.";
}
else{
    $Url = config("URL").$baseUrl.$roomParam.$playerParam.$sessionParam.$aliasParam.$deviceParam.$realDeviceParam;
    ?>
<script>
    window.location.href = '<?=$Url?>'
</script>
<?php
}
?>

</div>