<!-- MIDDLE CONTENT -->
<div class="middle-content player--verification">
  <!-- MIDDLE CONTENT -->
  <div class="middle-content__box">
    <section class="section">
      
      <?php if(config("SkinID") == 12): ?>
            <h1 class="section__title player--verification_title"><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
      <?php endif; ?>
      
      <!-- CONTENT -->
      <?php edit($this->controller,'account-verification'); ?>
      <div class="content-template">
        
        <!-- repeatable content -->
        <?php @$this->repeatData($this->content['account-verification']);?>
        <!-- /repeatable content -->
      </div>
      <!-- /CONTENT-->
    </section>
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->