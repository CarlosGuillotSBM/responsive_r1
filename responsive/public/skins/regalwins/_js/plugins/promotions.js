var sbm = sbm || {};

sbm.Promotions = function () {
	"use strict";

	return {
		run : function () { 
            
            var device = sbm.serverconfig.device;   // Device detection
            var newDotdotdot = new sbm.Dotdotdot(); //dotdotdot object initialitation. External plugin to crop the text
            newDotdotdot.run();                     // we run the dotdotdot plugin
            var imgh;                               // height of the promo image. Text content can't be longer that that
            var titleh;                             // we need to check the title hight to be acurate
            var promotextheight                     // var to store the final height of the content area
            // -----------------------------------
            // function to resize the content area
            // ----------------------------------- 
            var promoResizer = function(){
                if(device == 1){
                    // get the promo img height
                    imgh = $('.promotion .promotion__image').height();
                    // get the title height
                    titleh = $('.promotion .promotion__title__wrapper').height();
                    // we need to take out the title hight
                    promotextheight = imgh - titleh;
                    // also we take out extra pixels for the padding
                    promotextheight = promotextheight -50;
                    // set the new content wrapper height
                    $('.promotion .promotion__content__wrapper').css('height',imgh + 'px');
                    // set the new content height
                    $('.promotion .promotion__content').css('height', promotextheight + 'px');
                    // we apply the dotdotdot on the promotion__content container
                    $(".promotion  .promotion__content").dotdotdot();
                    
                }
            }
            // ----------------------------------- 
            // ----------------------------------- 
            // ----------------------------------- 

            // ----------------------------------- 
            // on resize event listener
            $( window ).resize(function(){
                promoResizer();
            });
            // we trigger the function to make the magic happen
            promoResizer();
            
        }
    };

}