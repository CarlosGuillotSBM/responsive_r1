var sbm = sbm || {};

sbm.Global = function() {
    "use strict";
    return {
        highlightSubNav: function(page) {
            $("ul.games-menu__games-nav > a").removeClass("active");

            if (page.indexOf("/games/") > -1) {
                $("#allslots a").addClass("active");
            } else if (page.indexOf("/slots/table-games/") > -1) {
                $("#table-card a").addClass("active");
            } else if (page == "/slots/")  {
                $("#scratchcards a").addClass("active");
            }
        },
        run: function() {

            if (window.location && window.location && window.location.search !== "") {
                var key = window.location.search.split("=")[1];
                if (key === "ui-my-balances-tab"){
                } else if (key === "ui-redeem-tab"){
                    $("#ui-redeem-tab").click();
                } else if (key === "ui-my-details-tab"){
                    $("#ui-my-details-tab").click();
                } else if (key === "ui-activity-tab"){
                    $("#ui-activity-tab").click();
                }
            }

            // useful variables for the site
            var page = window.location.pathname;
            var fullurl = window.location
            var device = sbm.serverconfig.device;




            /**
             * Help page - forgot password accordion
             */
            $(".ui-accordion").on("click", function () {
               $(this).next().toggle();
               $(this).toggleClass("active");
            });
            // ------------------------------
            // ----
            //hightlight selected nav element
            // ----
            // ------------------------------
            if (page.indexOf("/games/") > -1 || page.indexOf("/slots/table-games/") > -1 || page.indexOf("/slots/") > -1) {
                this.highlightSubNav(page);
            }
            var _current =  location.pathname.split('/')[1]
            var itemID;
            var item;

            //        if (page == "/") {
            //           $('.header').css('position', 'fixed');
            //       }

            // else   {

            //       $('.header').css('position', 'relative');

            //  };
            // fix the header when scrolling
            var $headerscroll = $("#ui-header-scroll, .nav-for-mobile");
            $(document).scroll(function() {
                $headerscroll.css({position: $(this).scrollTop() > 0 ? "fixed":"relative"});
            });

            if (_current == "" || _current == "start") {
                _current = "home";
            };

            if (_current == "register") {
                _current = "join-now";
            };


            // Changing the id of the body when we get the home through ajax navigation.
            $('body').attr('id', _current);
           
            // REVAMP - Games nav active state management
            function setGamesLinks() {
                var $currentPath = document.location.pathname;  // current page path e.g. /slots/
                var $linkSet = false;   // has an active link been set
                
                // Check to see if this is the homepage (no active states)
                if($currentPath == "/") {
                    return false;
                }

                // remove any previous active state
                $('.games-nav a').removeClass('active');

                // loop through all games nav links
                $('.games-nav').find('a').each(function() {
                    var $currentHref = $(this).attr('href');        // current href in loop
                    
                    // set matching href active and smooth scroll to it
                    if($currentHref == $currentPath) {
                        $(this).addClass('active');
                        $linkSet = true;

                        var clickedPosition = $(this).position().left + $(this).width();
                        var windowWidth = $(window).width();
                        var scrollAmount = (clickedPosition - windowWidth) + 30;

                        if(clickedPosition > windowWidth) {
                            $(".games-nav").animate({scrollLeft: scrollAmount}, 1200);
                        }

                        return false;
                    }
                });

                // if no link has been set, default to slots active
                if($linkSet == false)  {
                    $('.games-nav .slots').addClass('active');
                }
            }

            function setDesktopActiveLinks() {
                var $currentPath = document.location.pathname;  // current page path e.g. /slots/
                var $linkSet = false;   // has an active link been set
                
                // remove any previous active state
                $('.header-inner .desktop-nav__link a').removeClass('current');

                // loop through all games nav links
                $('.header-inner .desktop-nav__link').find('a').each(function() {
                    var $currentHref = $(this).attr('href');        // current href in loop
                    
                    // set matching href active and smooth scroll to it
                    if($currentHref == $currentPath) {
                        $(this).addClass('current');
                        $linkSet = true;

                        return false;
                    }
                });
            }

			function setDevice() {
                var ua = navigator.userAgent.toLowerCase();
                var android = ua.indexOf("android") > -1;
                var iphone = /iphone/.test(ua) && !window.MSStream;
                var ipad = /ipad/.test(ua) && !window.MSStream;

                if(android) { $('body').addClass("android"); }
                if(iphone)  { $('body').addClass("iphone"); }
                if(ipad)    { $('body').addClass("ipad"); }
            }

            // REVAMP - add device class to the body tag
            // used to change input colours on registration, possibly login also for iphones due to it ignoring autofill styling
            // due to iOS native popup bottom navigation.
            setDevice();

            // REVAMP - Games nav active state management
            if ($('.games-pages .games-nav').length || $('#my-favourites .games-nav').length) {
                setGamesLinks();
            };

            // Set desktop active state navigation links
            if($('.desktop-nav').length) {
                setDesktopActiveLinks();
            }

            // revamp increase footer padding if 'join now' banner is showing
            if($('.mobilemenu-cta').length) {
                $('.footer .footer__row:nth-child(3)').css('padding-bottom', '100px');
            };

            // used to handle smooth scrolling on homepage welcome message
            // handle links with @href started with '#' and the 'smoothscroll' class only
            $(document).on('click', 'a[href^="#"].smoothscroll', function(e) {
                // target element id
                var id = $(this).attr('href');

                // target element
                var $id = $(id);
                if ($id.length === 0) {
                    return;
                }

                // prevent standard hash navigation (avoid blinking in IE)
                e.preventDefault();

                // top position relative to the document
                var pos = ($id.offset().top -20);

                // animated top scrolling
                $('body, html').animate({scrollTop: pos}, 800);
            });


            // change name of Home link and href attr
            if ($.cookie("SessionID")) {
                $(".ui-go-home").attr("href", "/start/").text("START");
                $(".ui-go-home-logo").attr("href", "/start/");
                var logo = $(".site-logo-mobile");
                logo.addClass("site-logo-mobile-loggedin");
                logo.removeClass("site-logo-mobile");
            } else {
                var logo = $(".site-logo-mobile");
                logo.addClass("site-logo-mobile");
                logo.removeClass("site-logo-mobile-loggedin");
            }

            // --------------
            // SHARE BUTTON
            // --------------

            var config = {
                ui: {
                    flyout: 'middle left'
                },
                networks: {
                    facebook: {
                        url: this.fullurl
                    },
                    twitter: {
                        url: this.fullurl
                    },
                    google_plus: {
                        url: this.fullurl
                    },

                    email: {
                        enabled: false
                    },
                    pinterest: {
                        enabled: false
                    }
                }
            }

            var share = new Share('.share-button', config);

            //We show it if it is not the cashier.
            if (page == '/cashier/' || page == '/my-account/' || page == "/register/" || page == "/welcome/" || page == "/login/") {
                $('.share-button').attr("style", "display: none");
            } else {
                $('.share-button').attr("style", "display: inline");
            }

            // --------------
            // /SHARE BUTTON
            // --------------


            // --------------
            // /cropping text in side promotions
            // --------------
            // $(document).ready(function() {
            //     $(".promotion__content").dotdotdot({
            //         // configuration goes here
            //         watch: "window"
            //     });
            // });



            var page = window.location.pathname;



            //jQuery for click effect on search results screen to show growing circle background
            var parent, ink, d, x, y;
            $(".ripple li a").click(function(e){
                parent = $(this).parent();
                //create .ink element if it doesn't exist
                if(parent.find(".ink").length == 0)
                    parent.prepend("<span class='ink'></span>");

                ink = parent.find(".ink");
                //incase of quick double clicks stop the previous animation
                ink.removeClass("animate");

                //set size of .ink
                if(!ink.height() && !ink.width())
                {
                    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                    d = Math.max(parent.outerWidth(), parent.outerHeight());
                    ink.css({height: d, width: d});
                }

                //get click coordinates
                //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
                x = e.pageX - parent.offset().left - ink.width()/2;
                y = e.pageY - parent.offset().top - ink.height()/2;

                //set the position and add class .animate
                ink.css({top: y+'px', left: x+'px'}).addClass("animate");
            })

            /**
             * Search animation
             */
            $(".search-screen-mobile__header").on("click", function() {
                $(this).animate({top:'-50px'}), 1000, "easeOutBounce";
                $(".search-screen").animate({"bottom": "-100%"}, "slow").css("visibility", "hidden");
                $("body").removeClass("modal-open");
            });

            // REVAMP: Close the 'More' modal
            $("#more-modal .close-more-modal").on("click", function() {
                $("#more-modal").removeClass("active");
                $("body").removeClass("modal-open");
                return false;
            });

            // REVAMP: mobile navigation fade-in
            if (window.matchMedia('(max-width: 1024px)').matches) {
                $( document ).ready(function() {
                    $(".mobilemenu__menubottom").addClass("loaded");
                })
            }
        }
    };

};