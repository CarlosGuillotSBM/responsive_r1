var sbm = sbm || {};

/**
 * Magical Menu
 * Opens a popover menu with game filters
 */
sbm.magicalMenu = function (options) {
    "use strict";

    return {
        run: function () {
            if (typeof options.menuId === 'undefined') {
                var $menuBtn = $(".ui-game-filter-btn");
                var $menuFilter = $(".ui-game-filter-menu");
                var $menuContainer = $(".ui-games-filter-menu");            
            } else {                
                var $menuBtn = $(".ui-game-filter-btn-"+options.menuId);
                var $menuFilter = $(".ui-game-filter-menu-"+options.menuId);
                var $menuContainer = $(".ui-games-filter-menu-"+options.menuId);  
            }

            // change menus visibility and toggle class "select"
            var toggleVisibility = function () {

                // toggle visibility
                $menuFilter.toggle();
                $menuBtn.toggleClass("selected");

                // hide menu when clicked outside
                var visibility = $menuFilter.is(":visible");
                if (visibility) {
                    $(document).on("mouseup", function (e) {

                        if (!$menuContainer.is(e.target) && $menuContainer.has(e.target).length === 0) {
                            $menuFilter.hide();
                            $(document).off("mouseup");
                        }

                    });
                }
            };

            // on magical menu click
            var onMagicalMenuClick = function (e) {

                e.preventDefault();

                toggleVisibility();

                // close menu on link click
                var $links = $menuFilter.find(".tabs-content a");
                $links.on("click", function () {
                    toggleVisibility();
                });

            };

            // attach click event to magical menu
            $menuBtn.on("click", onMagicalMenuClick);
        }
    };

};




