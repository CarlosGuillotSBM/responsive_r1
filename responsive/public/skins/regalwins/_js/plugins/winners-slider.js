var sbm = sbm || {};

sbm.WinnersSlider = function (options) {
    "use strict";

    return {

        $slides: null,
        minSlides: options ? (sbm.serverconfig.device > 1 ? 2 : options.slidesToShow)  : 1,
        mode: options ? options.mode : "vertical",
        run : function () {

            // get the slides
            this.$slides = $(".latest-winners__winner.slide");

            // wrap all slides around a parent div
            this.$slides.wrapAll("<div class='winners-slider' />");

            // if needs to slide
            if (this.$slides.length > this.minSlides) {

                // apply bxSlider plugin onto the parent div
                $(".winners-slider").bxSlider({
                    mode: 'vertical',
                    minSlides: this.minSlides,
                    auto: true,
                    pager: false,
                    controls: false,
                    speed: 1700,
                    pause: 5000,
                    autoDelay: 1000,
                    touchEnabled: false
                });
            }
        }

    };
};