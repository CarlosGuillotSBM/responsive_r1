<?php// include "_global-library/partials/home/home-slide.php" ?>

<script type="application/javascript">
    skinPlugins.push({
        id: "Slick",
        options: {
            responsive: true,
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 10,
            auto: true,
            infinite: true,
            controls: false,
            pager: true,
            element: "bxslider-main-banner"
        }
    });
</script>

<div class="home-main-slider" style="width:100%">
    <ul class="bxslider-main-banner">
        <!-- Slide 1 -->
        <li>
            <a data-gtag="Join Now,Web Promo" href="/register/" data-hijack="true">
                <!-- xlarge width screens on desktop -->
                <img width="100%" class="xlarge-slider" src="../_images/slider/xlarge/slide1.jpg" alt="Get free spins, welcome offer on your first deposit.">
                <!-- medium width screens on desktop -->
                <img width="100%" class="desktop-slider" src="../_images/slider/desktop/slide1.jpg" alt="Get free spins, welcome offer on your first deposit.">
                <!-- mobile -->
                <img class="mobile-slider" src="../_images/slider/mobile/slide1.jpg" alt="Get free spins, welcome offer on your first deposit.">
            </a>
        </li>

        <!-- Slide 2 -->
        <li style="display: none">
            <a data-gtag="Join Now,Web Promo" href="/register/" data-hijack="true">
                <!-- xlarge width screens on desktop -->
                <img width="100%" class="xlarge-slider" src="../_images/slider/xlarge/slide2.jpg" alt="Get free spins, welcome offer on your first deposit.">
                <!-- medium width screens on desktop -->
                <img width="100%" class="desktop-slider" src="../_images/slider/desktop/slide2.jpg" alt="Get free spins, welcome offer on your first deposit.">
                <!-- mobile -->
                <img class="mobile-slider" src="../_images/slider/mobile/slide2.jpg" alt="Get free spins, welcome offer on your first deposit.">
            </a>
        </li>
    </ul>
</div>

<!-- CTA -->
<div class="regalwins-button-wrapper home-slider">
    <a href="/promotions/" class="regalwins-button ui-scroll-top">All Promotions</a>
</div>
