
<div class="vip-level-area">
    <span class="vip-text">Your VIP level</span> <a href="/loyalty/"><span data-bind="text: vipLevel().toLowerCase(), css: 'level-' + playerClass()"></span></a>
    <!-- Logic below related to the two claim upgrade bonus boxes and the confirmation boxes once you have claimed -->
    <a class="btn-lobby-deposit" data-hijack="true" href="/cashier/" data-bind="visible: ClaimUpgradeBonus.ubAmount() > 0 || ClaimCashback.cbAmount() > 0 || ClaimCashback.cbClaimAmount() > 0 || ClaimUpgradeBonus.ubBonusAmount() > 0" style="display: none;">DEPOSIT NOW</a>
</div>



