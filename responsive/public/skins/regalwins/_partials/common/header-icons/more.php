<?xml version="1.0" encoding="UTF-8"?>
<svg width="39px" height="27px" viewBox="0 0 39 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 52.6 (67491) - http://www.bohemiancoding.com/sketch -->
    <title>icon-burger</title>
    <desc>Created with Sketch.</desc>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Mobile" transform="translate(-204.000000, -240.000000)" fill="#FFFFFF">
            <g id="icon-burger" transform="translate(204.000000, 240.000000)">
                <rect id="Rectangle-5" x="11.4285714" y="0" width="26.8009996" height="3.57142857" rx="1.78571429"></rect>
                <path d="M1.78571429,11.4285714 L36.9407512,11.4285714 C37.9269739,11.4285714 38.7264655,12.2280629 38.7264655,13.2142857 L38.7264655,13.2142857 C38.7264655,14.2005085 37.9269739,15 36.9407512,15 L1.78571429,15 C0.799491518,15 1.20777456e-16,14.2005085 0,13.2142857 L0,13.2142857 C-1.20777456e-16,12.2280629 0.799491518,11.4285714 1.78571429,11.4285714 Z" id="Rectangle-5-Copy"></path>
                <path d="M7.5,22.8571429 L36.692304,22.8571429 C37.6785267,22.8571429 38.4780182,23.6566344 38.4780182,24.6428571 L38.4780182,24.6428571 C38.4780182,25.6290799 37.6785267,26.4285714 36.692304,26.4285714 L7.5,26.4285714 C6.51377723,26.4285714 5.71428571,25.6290799 5.71428571,24.6428571 L5.71428571,24.6428571 C5.71428571,23.6566344 6.51377723,22.8571429 7.5,22.8571429 Z" id="Rectangle-5-Copy-2"></path>
            </g>
        </g>
    </g>
</svg>