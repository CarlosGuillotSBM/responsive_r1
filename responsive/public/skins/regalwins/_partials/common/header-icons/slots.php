<svg width="25px" height="18px" viewBox="0 0 25 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 47 (45396) - http://www.bohemiancoding.com/sketch -->
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="icons-/-gambling-/-slots-/-white" transform="translate(0.000000, -4.000000)" fill-rule="nonzero" fill="#FFFFFF">
            <g id="icon-slots" transform="translate(0.000000, 4.000000)">
                <path d="M22.7,0.5 L2.6,0.5 C1.4,0.5 0.4,1.4 0.4,2.6 L0.4,15.4 C0.4,16.5 1.4,17.5 2.6,17.5 L22.7,17.5 C23.9,17.5 24.9,16.6 24.9,15.4 L24.9,2.5 C25,1.4 24,0.5 22.7,0.5 Z M16.5,6.1 C16.1,6.1 15.8,6.4 15.8,6.8 L15.8,16.2 L9.5,16.2 L9.5,6.7 C9.5,6.3 9.2,6 8.8,6 C8.4,6 8.1,6.4 8.1,6.7 L8.1,16.1 L2.6,16.1 C2.1,16.1 1.7,15.8 1.7,15.3 L1.7,2.5 C1.7,2.1 2.1,1.7 2.6,1.7 L8.1,1.7 L8.1,2.1 C8.1,2.5 8.4,2.8 8.8,2.8 C9.2,2.8 9.5,2.5 9.5,2.1 L9.5,1.8 L15.9,1.8 L15.9,2.2 C15.9,2.6 16.2,2.9 16.6,2.9 C17,2.9 17.3,2.6 17.3,2.2 L17.3,1.8 L22.8,1.8 C23.3,1.8 23.7,2.1 23.7,2.6 L23.7,15.4 C23.7,15.8 23.3,16.2 22.8,16.2 L17.3,16.2 L17.3,6.7 C17.2,6.4 16.9,6.1 16.5,6.1 Z" id="Shape"></path>
                <path d="M10.7,6.9 C10.5,6.9 10.3,7.1 10.3,7.2 L10.3,9 L11,9 C11.2,9 11.4,8.8 11.4,8.7 L11.4,8.3 L11.4,8.3 L12.9,8.3 C11.6,8.8 11.8,11.1 11.8,11.1 L13.7,11.1 C13.7,8.8 15.2,7.6 15.2,7.6 L14.8,6.9 L10.7,6.9 Z" id="Shape"></path>
                <path d="M18.4,6.9 C18.2,6.9 18,7 18,7.2 L18,9 L18.7,9 C18.9,9 19.1,8.8 19.1,8.7 L19.1,8.3 L20.6,8.3 C19.3,8.8 19.5,11.1 19.5,11.1 L21.4,11.1 C21.4,8.8 22.9,7.6 22.9,7.6 L22.5,6.9 L18.4,6.9 Z" id="Shape"></path>
                <path d="M2.9,6.9 C2.7,6.9 2.6,7 2.6,7.2 L2.6,9 L3.3,9 C3.5,9 3.7,8.8 3.7,8.7 L3.7,8.3 L5.2,8.3 C3.9,8.8 4.1,11 4.1,11 L6,11 C6,8.7 7.5,7.5 7.5,7.5 L7.1,6.9 L2.9,6.9 Z" id="Shape"></path>
                <path d="M8.9,5.7 L8.8,5.7 C8.5,5.7 8.2,5.4 8.2,5.1 L8.2,3.9 C8.2,3.6 8.5,3.3 8.8,3.3 L8.9,3.3 C9.2,3.3 9.5,3.6 9.5,3.9 L9.5,5.1 C9.5,5.4 9.2,5.7 8.9,5.7 Z" id="Shape"></path>
                <path d="M16.6,5.7 L16.5,5.7 C16.2,5.7 15.9,5.4 15.9,5.1 L15.9,3.9 C15.9,3.6 16.2,3.3 16.5,3.3 L16.6,3.3 C16.9,3.3 17.2,3.6 17.2,3.9 L17.2,5.1 C17.2,5.4 16.9,5.7 16.6,5.7 Z" id="Shape"></path>
            </g>
        </g>
    </g>
</svg>