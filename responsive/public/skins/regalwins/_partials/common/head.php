<!doctype html>
<html class="no-js" lang="en">
<head>
     <link rel="icon" type="image/ico" href="/_images/common/favicon.ico" />
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=1, user-scalable=0,shrink-to-fit=no">
     <!-- meta for iphone -->
     <meta name="apple-mobile-web-app-title" content="Regal Wins">
     <meta name="apple-mobile-web-app-capable" content="yes" />
     <meta name="apple-mobile-web-app-status-bar-style" content="black" />
     <!-- icons for iPhone -->
     <link rel="apple-touch-icon" sizes="180x180" href="/_images/common/apple-icon-180x180.png">
     <link rel="icon" type="image/png" href="/_images/common/favicon-32x32.png" sizes="32x32">
     <link rel="icon" type="image/png" href="/_images/common/favicon-16x16.png" sizes="16x16">
     <link href="/_images/common/android-icon-192x192.png" rel="icon" sizes="192x192" />
     <link href="/_images/common/android-icon-144x144.png" rel="icon" sizes="128x128" />
     <meta name="theme-color" content="#000000" />

     <?php include "_global-library/partials/common/open-graph.php"; ?>
     
     <?php echo $this->seo_meta;?>

     <?php if(Env::isLive()):?>
          <!-- new relic -->
          <?php include "_global-library/trackers/new-relic/regalwins.php"; ?>
          <!-- /relic -->
          <!-- Start Visual Website Optimizer Asynchronous Code -->
          <script type='text/javascript'>
          var _vwo_code=(function(){
               var account_id=259658,
               settings_tolerance=2000,
               library_tolerance=2500,
               use_existing_jquery=false,
               // DO NOT EDIT BELOW THIS LINE
               f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
               </script>
               <!-- End Visual Website Optimizer Asynchronous Code -->
               <?php endif;?>



               <link rel="stylesheet" href="/_global-library/js/vendor/intlTelInput/css/intlTelInput.css<?php echo "?v=" . config("Version"); ?>" />
               <link rel="stylesheet" href="/_css/main.css<?php echo "?v=" . config("Version"); ?>" />
               <link href="https://plus.google.com/111054716570434211985" rel="publisher" />
                <link rel="manifest" href="/manifest.json"/>
                    <?php include '_trackers/header.php';?>
               <script src="/_global-library/js/bower_components/modernizr/modernizr.min.js"></script>

               <script type="application/javascript">
               // sbm initialization
               var skinModules = [], skinPlugins = [];
               // common modules
               skinModules.push({
                    id: "Notifier",
                    options: {
                         global: true
                    }
               });
               skinModules.push({
                    id: "GameLauncher",
                    options: {
                         modalSelector: "#games-info",
                         global: true
                    }
               });
               skinModules.push({
                    id: "GameWindow",
                    options: {
                         global: true
                    }
               });
               skinModules.push({
                    id: "Favourites",
                    options: {
                         imgFavouriteOn: "/_images/common/icons/favourite-icon--on.png",
                         imgFavouriteOff: "/_images/common/icons/favourite-icon--off.png",
                         noFavIconsWeb: 6,
                         noFavIconsTablet: 3,
                         noFavIconsPhone:2,
                         global: true
                    }
               });
               skinModules.push( {
                    id: "MailInbox",
                    options: {
                         global: true
                    }
               });
               skinModules.push( {
                    id: "TrackNavigation",
                    options: {
                         global: true
                    }
               });
               skinModules.push({
                    id: "ContentScheduler",
                    options: {
                         global: true
                    }
               });
               // common plugins
               skinPlugins.push({
                    id: "Global",
                    options: {
                         global: true
                    }
               });
               skinPlugins.push({
                    id: "Tables",
                    options: {
                         global: true
                    }
               });
               skinPlugins.push({
                    id: "magicalMenu",
                    options: {
                         global: true

                    }
               });
               // mobile menu
               skinPlugins.push({
                    id: "MobileMenu",
                    options: {
                         global: true
                    }
               });
               // R10-575 Adaptive Banner
               skinModules.push({
                 id: "AdaptiveBanner",
                 options: {
                     global: true
                 }
               });
              //Prize Wheel
               skinModules.push({
                    id: "PrizeWheel",
                    options: {
                         global: true
                    }
               });
		skinModules.push({
			id: "CMANotifications",
			options: {
				global: true
			}
		});
               </script>
               <?php include "_global-library/partials/common/user-agent-sniffer.php"; ?>
               <!-- SERVER SIDE AND DYNAMIC STYLES -->

                      <!-- SEO Social -->
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Regal Wins",
  "url" : "https://www.regalwins.com/",
  "sameAs" : [ "https://www.facebook.com/regalwins/"] 
} 
</script>
<!--/ SEO Social -->



               <style type="text/css">
               <?php
               echo "/*device: " . config("Device") . 'logged: ' . logged() . "*/\n";
               ?>
               /*CLASS TO HIDE ELEMENTS FOR REAL MOBILE DEVICES*/
               <?php if (config("Device") == 2) { ?>
                    .custom-hide-for-mobile{display:none!important;}
                    <?php }?>
                    .search-input input:focus,.search-input input { font-size: 16px !important;height: 44px;}

                    </style>
                    <!-- /SERVER SIDE AND DYNAMIC STYLES -->
                    </head>
                    <body id="<?php echo $this->controller; ?>">
                    <?php if(Env::isLive()):?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5HJ4KP"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
                    <script type="text/javascript">var rumMOKey='e75e859c3d11253c2c0f516dabeb9f18';(function(){if(window.performance && window.performance.timing && window.performance.navigation) {var site24x7_rum_beacon=document.createElement('script');site24x7_rum_beacon.async=true;site24x7_rum_beacon.setAttribute('src','//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey='+rumMOKey);document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);}})(window)</script>
                    <?php endif;?>
