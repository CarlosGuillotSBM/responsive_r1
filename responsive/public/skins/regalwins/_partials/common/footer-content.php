<!-- FOOTER AREA -->
<div class="clearfix"></div>
<footer id="footer" class="footer">
     <div class="footer__row">
          <div class="footer__row__left">
               <ul class="footer__nav">
                    <li><a href="/about-us/">About</a></li>
                    <li>|</li>
                    <li><a href="/support/">Support</a></li>
                    <li>|</li>
                    <li><a href="/faq/">FAQ </a></li>
                    <li>|</li>
                    <li><a href="/banking/">Banking </a></li>
                    <li>|</li>
                    <li><a href="/terms-and-conditions/">Terms &amp; Conditions</a></li>
                    <li>|</li>
                    <li><a href="/privacy-policy/">Privacy </a></li>
                    <li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none">|</li>
                    <li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none"><a  href="/cashier/?settings=1">Player Settings </a></li>
                    <li >|</li>
                    <li><a href="/responsible-gaming/">Responsible Gaming </a></li>
                    <li>|</li>
                    <li><a href="/complaints-and-disputes/">Complaints and Disputes</a></li>
                    <li>|</li>
                    <li><a  href="/archive/" data-hijack="false">Archive </a></li>
               </ul>
          </div>
     </div>
     <div class="footer__row">
          <div class="footer__row__legal">
               <h1>Regal Wins</h1>
               <p>Regal Wins is licensed and regulated to offer Gambling Services in Great Britain by the UK Gambling Commission, license Number 000-039022-R-319427-004. All the games offered on the website have been approved by the UK Gambling Commission. Details of its current licensed status as recorded on the Gambling Commission's website can be found <a  href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022" target="_blank" data-hijack="false">here.</a></p>
               <p>Regal Wins is also licensed and regulated by the Alderney Gambling Control Commission, License Number: 71 C1, to offer Gambling facilities in jurisdictions outside Great Britain. Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3 UL.</p>
          </div>

          <div class="footer__social--wrapper">
               <ul class="footer__social">
               <li class="footer__social--icons"><a href="https://www.facebook.com/regalwins/" target="_blank"><?php include "footer-icons/facebook.php";?></a></li>
               <li class="footer__social--icons"><a href="https://twitter.com/regalwins/" target="_blank"><?php include "footer-icons/twitter.php";?></a></li>
               </ul>

               <ul class="footer__row__gamblingaware">
                    <li><a href="/terms-and-conditions/"><?php include "footer-icons/18plus.php";?></li>
                    <li><a target="_blank" data-hijack="false" href="http://www.gamcare.org.uk/"><?php include "footer-icons/gamcare.php";?></li>
                    <li><a target="_blank" data-hijack="false" href="https://www.gamstop.co.uk" rel="nofollow"><?php include "footer-icons/gamstop.php";?></a></li>
                    <li><a target="_blank" data-hijack="false" href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage"><?php include "footer-icons/ecogra.php";?></a></li>
                    <li><a target="_blank" data-hijack="false" href="https://www.gamblingcontrol.org/"><?php include "footer-icons/gambling-control.php";?></a></li>
                    <li><a target="_blank" data-hijack="false" href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&lng=EN" rel="nofollow"><?php include "footer-icons/gambling-commission.php";?></a></li>
                    <li><a target="_blank" data-hijack="false" href="https://www.begambleaware.org/" rel="nofollow"><?php include "footer-icons/gamble-aware.php";?></a></li>
               </ul>
        </div>
     </div>
     <div class="footer__row">
          <ul class="footer__row__payments">
               <li><?php include "footer-icons/paypal.php";?></li>
               <li><?php include "footer-icons/maestro.php";?></li>
               <li><?php include "footer-icons/mastercard.php";?></li>
               <li><?php include "footer-icons/visa.php";?></li>
               <li><?php include "footer-icons/neteller.php";?></li>
               <li><?php include "footer-icons/paysafecard.php";?></li>
               <li><?php include "footer-icons/ssl-secure.php";?></li>
          </ul>
          <a href="mailto:support@regalwins.com" class="link-support" title="Email Regal Wins support">support@regalwins.com</a>
     </div>

     <?php
          // Do not show if user has logged in before and has a username value
          if (!isset($_COOKIE["username"])) {
     ?>

     <div class="mobilemenu-cta" data-bind="visible: !validSession()" style="display: none !important;">
          <div class="mobilemenu-cta--wrapper">
               <a href="/register/">Join now</a>
          </div>
     </div>

     <?php
            }
     ?>

     <!-- Mobile menu navbar  -->
     <div class="mobilemenu__menubottom">
          <ul class="mobilemenu__menubottom--wrapper">
               <li class="mobilemenu__menubottom--item mobilemenu__menubottom--home" data-bind="visible: validSession()" style="display:none;">
                    <span class="mobilemenu-messages-icon" data-bind="text: MailInbox.unreadMessages(), visible: MailInbox.unreadMessages() > 0" style="display: none;">0</span>
                    <a data-hijack="false" href="/start/" class="mobilemenu__menubottom--link">
                         <?php include "header-icons/home-icon.php";?>Home
                    </a>
               </li>
               <li class="mobilemenu__menubottom--item mobilemenu__menubottom--home" data-bind="visible: !validSession()" style="display:none;">
                    <a data-hijack="false" href="/" class="mobilemenu__menubottom--link">
                         <?php include "header-icons/home-icon.php";?>Home
                    </a>
               </li>
               <li class="mobilemenu__menubottom--item mobilemenu__menubottom--games">
                    <a data-hijack="false" href="/games/" class="mobilemenu__menubottom--link">
                         <?php include "header-icons/games.php";?>Games</a>
               </li>
               <li class="mobilemenu__menubottom--item mobilemenu__menubottom--promotions">
                    <a data-hijack="false" href="/promotions/" class="mobilemenu__menubottom--link">
                         <?php include "header-icons/promotions.php";?>Promotions</a>
               </li>
               <li class="mobilemenu__menubottom--item mobilemenu__menubottom--search">
                    <a class="mobilemenu__menubottom--link search-icon">
                         <?php include "header-icons/search.php";?>Search
                    </a>
               </li>
               <li class="mobilemenu__menubottom--item mobilemenu__menubottom--my-account">
                    <a class="mobilemenu__menubottom--link" data-reveal-id="login-modal" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data, 'my-account')" style="display: none;">
                         <?php include "header-icons/my-account.php";?>My account
                    </a>
                    <a data-hijack="false" href="/my-account/" class="mobilemenu__menubottom--link" data-bind="visible: validSession()" style="display: none;">
                        <?php include "header-icons/my-account.php";?>My account
                    </a>
               </li>
               <li class="mobilemenu__menubottom--item mobilemenu__menubottom--more">
                    <a class="mobilemenu__menubottom--link more-modal-link">
                         <?php include "header-icons/more.php";?>More
                    </a>
               </li>
          </ul>
     </div>
     <!-- Mobile menu navbar end -->
</footer>
<!-- END FOOTER AREA -->
