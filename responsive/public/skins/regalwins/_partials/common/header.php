<script type="application/javascript">

	window.setTimeout(function () {
		var el = document.getElementById("ui-first-loader");
		if (el) el.parentNode.removeChild(el);
		el = document.getElementById("ui-loader-animation");
		if (el) el.style.display = "none";
	},1000);
</script>

<!-- header desktop -->
<header id="ui-header-scroll" data-bind="css: { 'header--logged-in' :validSession }" class="header header--logged-<?php echo logged(); ?>">
	<!-- COOKIE POLICY -->
	<?php include "_global-library/partials/modals/cookie-policy.php" ?>
	<nav class="header-inner">
		<div class="header-inner__left">
			<div class="header-inner__left__top">
				<!-- left nav logged out view -->
				<ul class="desktop-nav loggedout-left" data-bind="visible: !validSession()">
					<li id="slots" class="desktop-nav__link"><a href="/slots/" data-hijack="true">Slots</a></li>
					<li id="live-casino" class="desktop-nav__link"><a href="/live-dealer/" data-hijack="true">Live Dealer</a></li>
					<li id="table-card" class="desktop-nav__link"><a href="/table-games/" data-hijack="true">Table &amp; Card</a></li>
					<li id="roulette" class="desktop-nav__link"><a href="/roulette-games/" data-hijack="true">Roulette</a></li>
					<li id="scratch-and-arcade" class="desktop-nav__link"><a href="/scratch-and-arcade/" data-hijack="true">Scratch &amp; Arcade</a></li>
					<li id="sports" class="desktop-nav__link u-hidden"><a href="/sports/" data-hijack="true">Sports</a></li>
					<li id="games" class="desktop-nav__link"><a href="/games/" data-hijack="true">All</a></li>
				</ul>
				<!-- left nav logged in view -->
				<ul class="desktop-nav loggedin-left" data-bind="visible: validSession" style="display:none;">
				    <li id="lobby" class="desktop-nav__link"><a href="/start/" data-hijack="true">Home</a></li>
					<li id="slots" class="desktop-nav__link"><a href="/slots/" data-hijack="true">Slots</a></li>
                    <li id="live-casino" class="desktop-nav__link"><a href="/live-dealer/" data-hijack="true">Live Dealer</a></li>
					<li id="table-card" class="desktop-nav__link"><a href="/table-games/" data-hijack="true">Table &amp; Card</a></li>
					<li id="scratch-and-arcade" class="desktop-nav__link"><a href="/scratch-and-arcade/" data-hijack="true">Scratch &amp; Arcade</a></li>
					<li id="sports" class="desktop-nav__link u-hidden"><a href="/sports/" data-hijack="true">Sports</a></li>
					<li id="games" class="desktop-nav__link"><a href="/games/" data-hijack="true">All</a></li>
				</ul>
				<!--  /left nav logged in view -->
			</div>
			<div class="header-inner__left__bottom">

				<div class="cat-search-bar">
					<div class="cat-search-bar__categories"><?php include "_global-library/partials/game-filter-menu/game-filter-menu-8ball.php";?></div>
					<div class="cat-search-bar__games-search">
						<input type="text" placeholder="Search Games" class="autocomplete ui-search-input"
							   data-bind="value: GamesMenuSearch.searchTerm, valueUpdate:'input'">
						<span class="cat-search-bar__games-search__icon"><img src="/_images/common/header/search.png" alt="search"></span>
						<?php include('_global-library/partials/search/search-results-container.php'); ?>
					</div>

				</div>
			</div>
			</div>
		</div>
		<div class="header-inner__middle">
			<h1 class="sitelogo seo-logo">
				<a href="/" data-hijack="true">
					<img src="/_images/revamp/common/header/header-logo.svg" width="300" height="145" />
				</a>
				<span style="display:none;">Regal Wins Casino online The best online casino sites on mobile and desktop.</span>
			</h1>
		</div>
		<div class="header-inner__right">
			<div class="header-inner__right__top">
				<!-- right nav logged out view -->
				<ul class="desktop-nav loggedout-right" data-bind="visible: !validSession()">
					<li id="promotions" class="desktop-nav__link"><a href="/promotions/" data-hijack="true">Promotions</a></li>
					<li id="loyalty" class="desktop-nav__link"><a href="/loyalty/" data-hijack="true">Loyalty</a></li>
					<li id="support" class="desktop-nav__link"><a href="/support/" data-hijack="true">Help</a></li>
				</ul>
				<!-- right nav logged in view -->

				<ul class="desktop-nav loggedin-right" data-bind="visible: validSession" style="display:none;">
				<li id="my-favourites" class="desktop-nav__link" data-bind="visible: Favourites.favourites().length > 0"><a href="/my-favourites/" data-hijack="true">My Favourites</a></li>
					<li id="promotions" class="desktop-nav__link"><a href="/promotions/" data-hijack="true">Promos</a></li>
					<li id="loyalty" class="desktop-nav__link"><a href="/loyalty/" data-hijack="true">Loyalty</a></li>
					<li id="support" class="desktop-nav__link"><a href="/support/" data-hijack="true">Help</a></li>
					<li id="my-account" class="desktop-nav__link"><a href="/my-account/" data-hijack="true">My Account</a></li>
					<li  class="desktop-nav__link"><a href="" data-bind="click: logout">Logout</a></li>
				</ul>
				<!-- /right nav logged in view -->
			</div>
			<div class="header-inner__right__bottom">
				<div class="buttons-bar" style="display: none" data-bind="visible: !validSession()">
					<div class="buttons-bar__login"><a data-reveal-id="login-modal" href="/login/">Login</a></div>
					<div class="buttons-bar__join"><a id="web-join-top" href="/register/">Join now</a></div>
				</div>
				<div class="balance-bar" style="display: none" data-bind="visible: validSession()">
					<!-- logged in -->
					<div class="deposit-display" style="display: none" data-bind="visible: validSession"><a href="/cashier/" class="deposit-link">Cashier</a></div>
					<div class="balance-display" style="display: none" data-bind="visible: validSession"><span data-bind="text: balance">...</span></div>
				</div>
			</div>
		</div>
</nav>
</header>
<!-- /header desktop -->
<!-- header for tablet and mobile -->
<!-- nav for mobile -->
<?php include "_partials/nav-for-mobile/nav-for-mobile.php"; ?>
<!-- / nav for mobile -->

<!-- search screen for mobile -->
<?php include "_partials/search-screen/search-screen.php"; ?>
<!-- / search screen for mobile -->


<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>  ">
<!-- AJAX CONTENT WRAPPER -->
 <?php
        include("_global-library/widgets/prize-wheel/prize-wheel-notification.php");
    ?>
<div id="ajax-wrapper">