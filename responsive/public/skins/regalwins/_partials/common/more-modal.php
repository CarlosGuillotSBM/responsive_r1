
<div id="more-modal">
	<span class="close-more-modal">&#215;</span>
    <div class="more-modal-content">
        <?php include '_partials/games-menu/games-nav.php'; ?>
        <ul class="more-links">
            <li><a href="/loyalty/">VIP & loyalty</a></li>
            <li><a href="/banking/">Banking info</a></li>
            <li><a href="/about-us/">About us</a></li>
            <li><a href="/responsible-gaming/">Play responsibly</a></li>
            <li><a href="/support/">Get in touch</a></li>
            <li><a href="/terms-and-conditions/">The legal stuff</a></li>
            <li><a href="/faq/">FAQ</a></li>
            <li><a href="/complaints-and-disputes/">Complaints & disputes</a></li>
            <li><a href="/privacy-policy/">Privacy matters</a></li>
            <li><a href="/archive/">Archive</a></li>
        </ul>

        <div class="logout-cta-container">
            <a data-hijack="true" title="Log Out" style="" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">Log Out</a>
        </div>
	</div>
</div>