<script type="application/javascript">

    skinModules.push({
        id: "GamesMenuSearch",
        options: {
            global: true
        }
    });

</script>

<script type="text/tmpl" id="ui-mobile-search-results-tmpl">

	<li class="search-result" data-bind="click: $root.GamesMenuSearch.launchGame">
		<a>
			<div class="search-result__thumb">
				<img class="search-result__thumb__img" itemprop="game-icon"
					 data-bind="attr: { src: icon }">
			</div>
			<div class="search-result__name">
				<div class="search-result__name__inner"
					 data-bind="text: title"></div>
			</div>
			<div class="search-result__cta">
				<?php include "_partials/common/play-icon-svg.php"; ?>
			</div>
		</a>
	</li>

</script>

<!-- when search screen is open you must apply body position fixed -->
<!-- Search screen -->



<div class="search-screen">

    <span class="search-screen-mobile__header">×</span>

    <div class="search-inner">

        <div class="search-input">
            <input type="text" placeholder="Search Games" autocomplete="off"
                   class="search-field" data-bind="value: GamesMenuSearch.searchTerm, valueUpdate:'input'">
            <i class="search-input__icon"></i>
        </div>

        <h4 style="color: #FFFFFF" data-bind="visible: GamesMenuSearch.gamesFound().length, text: GamesMenuSearch.gamesFound().length + ' Games Found'"></h4>
        <h4 style="color: #FFFFFF" data-bind="visible: GamesMenuSearch.searchTerm() && !GamesMenuSearch.gamesFound().length">No Games Found</h4>

        <ul id="ui-mobile-search-results-container" class="search-results ripple" data-bind="visible: GamesMenuSearch.gamesFound().length, foreach: GamesMenuSearch.gamesFound">

            <!-- search results go here -->

        </ul>

        <div class="search-inner__last-played" data-bind="visible: !GamesMenuSearch.gamesFound().length && !GamesMenuSearch.searchTerm() && LastPlayed.lastGames().length">
            <h4 style="color: #FFFFFF" data-bind="text: LastPlayed.lastGames().length + ' Last Played Games'"></h4>
            <?php include "_global-library/partials/games/games-last-played-mobile.php"; ?>
        </div>

    </div>
</div>