<ul style="display: none" data-bind="visible: !validSession()" class="login-nav">
    <li><a href="/login/" data-reveal-id="login-modal" class="login-button--nav-for-desktop">LOGIN</a></li>
	<li><a data-gtag="Join Now,Web Top Nav" class="join-now-button--top-nav" href="/register/">JOIN NOW</a></li>
</ul>