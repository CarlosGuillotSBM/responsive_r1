
<div class="home-hero">
    <a href="/register/">
        <img src="/_images/revamp/hero/homepagebanner-mobile.jpg" class="home-hero--banner-mobile">
        <img src="../_images/revamp/hero/homepagebanner-desktop.jpg" class="home-hero--banner" data-bind="visible: AdaptiveBanner.cookieHero">
    </a>
    <div class="home-hero--intro">
        <div class="home-hero--termsconditions">
            <?php edit($this->controller,'hero'); ?>
			<?php @$this->getPartial($this->content['hero'],1, "_partials/banner/homepage-hero-banner.php"); ?>
        </div>
    </div>
</div>
