<?php

$content = array(
    'line-1' =>  $row['Text1'],
    'line-2' =>  $row['Text2'],
    'line-3' =>  $row['Text3'],
    'cta-text' =>  $row['Text4'],
    'cta-url' =>  $row['Text5'],
    't&c-text' =>  $row['Text6'],
    't&c-url' =>  $row['Text7'],
    'extra-copy' =>  $row['Text8'],
    'image-desktop' =>  $row['Text9'],
    'image-desktop-retina' =>  $row['Text10'],
    'image-tablet' =>  $row['Text11'],
    'image-tablet-retina' =>  $row['Text12'],
    'image-mobile' =>  $row['Text13'],
    'image-mobile-retina' =>  $row['Text14']
);
?>

<div class="home-hero">
    <a href="<?=$content['cta-url']?>">
        <img src="<?= $content['image-mobile'] ?>" class="home-hero--banner-mobile">
        <img src="<?= $content['image-desktop'] ?>" class="home-hero--banner">
    </a>
    <div class="home-hero--intro">
        <div class="home-hero--termsconditions">
        <a href="<?php echo $content['t&c-url'] ?>">
            <?php echo $content['t&c-text'] ?>
        </a>
        <p class="slider-text-link-copyright"><?php echo $content['extra-copy'] ?></p>
        </div>
    </div>
</div>