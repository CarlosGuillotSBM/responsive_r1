<!-- balances -->
<?php
    // TODO : refactor this
    $realMoney = "Real Money";
    $bonusMoney = "Bonus Money";
    $freeSpins = "Free Spins";
    $points = "Points";
    if (config("SkinID") == 6) {
    $realMoney = "Real Balance";
    $bonusMoney = "Bonus Balance";
    $freeSpins = "Free Spins Balance";
    $points = "Loyalty Points";
    } else if (config("SkinID") == 5) {
    $points = "Moolahs";
    }
?>

<script type="application/javascript">
    skinModules.push( { id: "PlayerAccount" } );
</script>

<div class="balance-display-mobile" style="display: none">
    <div class="balance-display-mobile__header ui-close-modals"></div>
    <ul>
        <li id='ui-balance-switch' class="balance active" >
            <span class="balance-text">Hide Balance</span>
            <span class="overlay__mybalance--value">
                <fieldset class="switch round" tabindex="0">
                    <input id="switch__mybalance" type="checkbox">
                    <label for="switch__mybalance"></label>
                </fieldset>
            </span>
        </li>
        <li>Real Money<span data-bind="text: real">...</span></li>
        <li>Bonus Money<span data-bind="text: bonus">...</span></li>
        <li>Bonus Wins<span data-bind="text: bonusWins">...</span></li>
        <li>Free Slot Spins<span data-bind="text: bonusSpins" class="bonuswins_amount ui-lbBonusSpins">...</span></li>
        <li>Loyalty Points<span data-bind="text: points">...</span></li>
        <li>Cashback<span data-bind="text: cashback" class="balance_amount ui-lbBalance">...</span></li>
    </ul>

    <a href="/cashier/" class="balance-display-mobile__cta">Deposit</a>
</div>
