<!-- Mobile Top Bar -->
<div class="tab-bar nav-for-mobile" data-bind="css: { 'nav-for-mobile--logged-in' :validSession }">
     <!-- COOKIE POLICY -->
	<?php include "_global-library/partials/modals/cookie-policy.php" ?>
    <!-- MOBILE LOGO -->
    <h1 class="seo-logo">
        <a href="/" class="site-logo ui-go-home-logo menu-link">
            <img src="/_images/revamp/common/header/regalwins-logo-mobile.svg"/>
        </a>
        <span style="display:none;">Regal Wins Casino online The best online casino sites on mobile and desktop.</span>
    </h1>
    <!-- MOBILE ICON -->
    <h1 class="seo-logo">
        <a href="/" class="site-logo-mobile ui-go-home-logo">
            <img src="/_images/revamp/common/header/regalwins-logo-mobile.svg"/>
        </a>
        <span style="display:none;">Regal Wins Casino online The best online casino sites on mobile and desktop.</span>
    </h1>

    <!-- MENU ICONS -->
    <div class="icons-menu">
		<span>
			<!-- Message Inbox-->
            <?php if(config("inbox-msg-on")): ?>
            <?php endif; ?>
            <!-- /Message Inbox-->

		</span>
    </div>

    <!-- deposit display -->
    <div class="deposit-display" style="display: none" data-bind="visible: validSession"><a href="/cashier/" class="deposit-link">Cashier</a></div>

    <!-- balance display -->
    <div class="balance-display" style="display: none" data-bind="visible: validSession"><span data-bind="text: balance">...</span></div>

    <!-- logged out -->
    <div class="nav-for-mobile__ctas">
        <!--		<a style="display: none" data-bind="visible: validSession()" data-reveal-id="login-modal-deposit" class="login-button--nav-for-mobile menu-link">INFO</a>-->
        <a style="display: none" data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login-button--nav-for-mobile menu-link ui-scroll-top">LOGIN</a>
        <!-- 
        <a style="display: none" data-bind="visible: !validSession()" class="join-button--nav-for-mobile menu-link" href="/register/">JOIN NOW</a> -->
    </div>
    <!-- /logged out -->



</div>
<!-- / Mobile Top Bar -->

<!-- Menu Mobile -->
<?php  include'_partials/nav-for-mobile/menu-mobile.php'; ?>
<!-- Account Menu Mobile -->
<?php include'_partials/nav-for-mobile/account-menu-mobile.php'; ?>
<!-- Balance Display Mobile -->
<?php include'_partials/nav-for-mobile/balance-display-mobile.php'; ?>
<!-- My Messages Mobile -->
<?php //include'_partials/my-messages-mobile/single-message-mobile.php'; ?>
