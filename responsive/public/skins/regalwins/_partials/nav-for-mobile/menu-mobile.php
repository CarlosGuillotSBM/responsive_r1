<!-- Mobile Menu -->
<nav class="mobile-menu" style="opacity: 1">

		<div class="mobile-menu-lists ripple">

			<!-- menu mobile top area / slide back  -->
			<div class="mobile-menu-top">
				<div class="mobile-menu-item back-link">
					<span class="menu-link">REGAL WINS MENU</span>
				</div>
			</div>

			<!-- menu lists -->
			<ul class="mobile-menu-list">
				<!-- section title -->
				<li class="mobile-menu-item home-link">
					<a class="menu-link" data-hijack="true" href="/" title="Home">Home</a>
				</li>
				<!-- section title -->
				<li class="mobile-menu-section-title">Games</li>
				<!-- menu items-->
				<!-- commented out search link while feature is finalized -->
				<li class="mobile-menu-item gamesearch-link">
					<a id="ui-search-screen" class="menu-link"  title="New Games">Search Games</a>
				</li>
				<li class="mobile-menu-item newgames-link">
					<a class="menu-link" data-hijack="true" href="/slots/brand-new-slots/" title="New Games">New Games</a>
				</li>
				<li class="mobile-menu-item favourites-link" data-bind="visible: validSession()">
					<a class="menu-link" data-hijack="true" href="/my-favourites/" title="Favourite Games">My Favourites</a>
				</li>
				<li class="mobile-menu-item slots-link">
					<a id="ui-slots-menu-link">Slots </a><span class="slots-categories is-original">
					<span id="UI-id">Categories
						<svg class="chevron-icon" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
							<path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#011A39">
							</path>
						</svg>
					</span>
				</span>
			</li>
			<!-- Slots Categories Hidden Slide Down Panel -->
			<div class="ui-games-filter-menu__popover-wrap games-filter-menu__popover-wrap">

				<div class="games-filter-menu__popover game-filter-menu" align="center">
					<span class="nub"></span>
					<!-- include game categories-->
					<?php include "_global-library/partials/game-filter-menu/game-filter-categories-8ball.php" ?>
				</div>
			</div>

			<li class="mobile-menu-item live-casino has-submenu">
				<a class="menu-link" data-hijack="true" href="/live-dealer/" title="Live Casino">
					Live Dealer
				</a>

				<span class="mobile-menu-item__submenu-toggle slots-categories" data-submenu="live-casino">
					Categories
					<svg class="chevron-icon" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
						<path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#011A39">
						</path>
					</svg>
				</span>

				<div class="mobile-menu-item__submenu">
					<div class="mobile-menu-item__submenu__content">
						<?php include "_global-library/partials/live-dealer/live-dealer-categories.php" ?>
					</div>
				</div>
			</li>

			<li class="mobile-menu-item scratchcards-link">
				<a class="menu-link" data-hijack="true" href="/slots/scratch-and-arcade/" title="Scratchcards">Scratch &amp; Arcade</a>
			</li>

			<li class="mobile-menu-item tablecard-link">
				<a class="menu-link" data-hijack="true" href="/slots/table-games/" title="Table & Card">Table &amp; Card</a>
			</li>
			<li class="mobile-menu-item roulette-link">
				<a class="menu-link" data-hijack="true" href="/slots/roulette-games/" title="Roulette">Roulette</a>
			</li>
			<!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
			<li class="mobile-menu-item sports-link u-hidden">
				<a class="menu-link" data-hijack="true" href="/sports/" title="Sports">Sports</a>
			</li>
			<!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->


			<li class="mobile-menu-item allgames-link">
				<a class="menu-link" data-hijack="true" href="/games/" title="All Games">All Games</a>
			</li>
			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title">Rewards</li>
			<!-- menu items-->
			<li class="mobile-menu-item promotions-link">
				<a class="menu-link" data-hijack="true" href="/promotions/" title="Promotions">Promotions</a>
			</li>
			<li class="mobile-menu-item loyalty-link">
				<a class="menu-link" data-hijack="true" href="/loyalty/" title="Loyalty">Loyalty</a>
			</li>
			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title">Banking</li>
			<!-- menu items-->
		<!-- 	<li class="mobile-menu-item myaccount-link" data-bind="visible: validSession()">
				<a class="menu-link" data-hijack="true" href="/my-account/" title="My Account">My Account</a>
			</li> -->
			<li class="mobile-menu-item cashier-link" data-bind="visible: validSession()">

				<a class="menu-link" data-hijack="true" href="/cashier/" title="Cashier">Cashier</a>
			</li>

			<li class="mobile-menu-item banking-link">
				<a class="menu-link" data-hijack="true" href="/banking/" title="Banking">Banking info</a>
			</li>



			<li class="mobile-menu-section-title">Support</li>


			<li class="mobile-menu-item support-link">
				<a class="menu-link" data-hijack="true" href="/support/" title="Support">Support</a>
			</li>
			<li class="mobile-menu-item faq-link">
				<a class="menu-link" data-hijack="true" href="/faq/" title="Faq">Faq</a>
			</li>



			<li class="mobile-menu-item terms-link">
				<a class="menu-link" data-hijack="true" href="/terms-and-conditions/" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
			</li>
			<li class="mobile-menu-item about-us-link">
				<a class="menu-link" data-hijack="true" href="/about-us/" title="About">About</a>
			</li>
			<li class="mobile-menu-item privacy-link">
				<a class="menu-link" data-hijack="true" href="/privacy-policy/" title="Privacy Policy">Privacy Policy</a>
			</li>
			<li class="mobile-menu-item settings-link" data-bind="visible: validSession()" style="display: none">
				<a class="menu-link" data-hijack="true" href="/cashier/?settings=1" title="Player Settings">Player Settings</a>
			</li>
			<li class="mobile-menu-item responsiblegaming-link">
				<a class="menu-link" data-hijack="true" href="/responsible-gaming/" title="Responsible Gaming">Responsible Gaming</a>
			</li>
			<li class="mobile-menu-item complaints-disputes-link">
				<a class="menu-link" data-hijack="true" href="/complaints-and-disputes/" title="Complaints and Disputes/">Complaints and Disputes</a>
			</li>
			<!--				<li class="mobile-menu-item gamelimits-link">-->
			<!--					<a class="menu-link" data-hijack="true" href="/game-limits/" title="Game Limits">Game Limits</a>-->
			<!--				</li>-->
			<li class="mobile-menu-item logout-link" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">
				<a class="menu-link" data-hijack="true" title="Log Out" style="display: none" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">Log Out</a>
			</li>
			<li class="mobile-menu-item" style="visibility:hidden">
				<!-- spacer to move last link from bottom -->
			</li>
			<!-- /menu items-->
		</ul>
	</div>
	<!-- / menu lists -->
</nav>
<!-- END New Mobile Menu April 2015 Author: Marcus Chadwick -->