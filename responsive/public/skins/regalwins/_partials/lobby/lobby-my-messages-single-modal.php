<!-- messages modal window -->
<div id="single-message" class="reveal-modal recent-messages__modal" data-reveal aria-labelledby="My Messages" aria-hidden="false" role="dialog">
	<div class="lobby-wrap__content-box">
		<!-- header bar -->
<!-- 		<div class="lobby-wrap__content-box__header">
			<div class="title hasicon" >
				<div class="title__icon">
				
					<?php// include 'icon-messages.php'; ?>
				
				</div >
				<div class="title__text messages-section" >MY MESSAGES</div>
				<a class="recent-messages__modal__delete" href=""><span></span>Delete Message</a>
			</div>
		</div> -->
		<!-- message content goes here -->
		<div class="single-message-content" >
    		<!-- ko with: PlayerAccount.selectedMessage -->
			<h1 id="modalTitle" data-bind="text: Subject"></h1>
    		<!-- /ko -->
			<div data-bind="html: PlayerAccount.messageHtml">
			</div>
			<!-- /message content goes here -->
		</div>
	</div>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- end messages modal window -->