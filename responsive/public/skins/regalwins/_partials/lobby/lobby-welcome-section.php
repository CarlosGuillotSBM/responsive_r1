<!-- Welcome Box -->
<div class="lobby-wrap__content-box">
    <!-- header bar -->
<div>
<?php @$this->repeatData($this->content['lobby-banner']);?>
                    <?php edit($this->controller,'lobby-banner'); ?>
</div>

    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-account.php'; ?>
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text">WELCOME <span data-bind="text: username"></span></div>
            <div class="title__link">
                <!--    <a class="mobile-cashback" href="#"> You have cashback CLAIM NOW</a>  -->
            </div>
        </div>
    </div>
    <!-- end header bar -->
    <div class="lobby-wrap__content-box__content">
        <!-- <div class="small-play-wrap">
            <div class="lobby-play-button"> <a href="/games/">PLAY GAMES</a></div>
        </div>
        <div class="small-deposit-wrap">
            <div class="lobby-deposit-button"> <a href="/cashier/">DEPOSIT NOW</a></div>
        </div> -->
        <div class="lobby-wrap__content-box__content__innerwrap--border">
            <!--  make this content a partial -->
            <!-- welcome area -->
            
            <div class="welcome-area">
                <div class="welcome-area__top">
                    
                    <div class="welcome-area__top__right">
                    <!-- VIP level slider not being used yet-->
                   <?php // include'_partials/lobby/lobby-current-vip-slider.php'; ?>

                     <!-- VIP current level -->
                   <?php include'_partials/lobby/lobby-current-vip-level.php'; ?>
                        
                   
                    </div>
                    <div class="welcome-area__top__left">
                        <article class="welcome-intro">
                            We are giving you more chances than ever to win incredible prizes so
                        whether you want to relax by the pool, garden or have your friends around, we have your Friday Bingo fun sorted.</article>
                        
                    </div>
                    
                </div>
                <div class="welcome-area__bottom">
                    <!-- email banner image goes here -->
                    <!--     <img class="welcome-banner" src="http://placehold.it/693x142?text=Offer+Image"> -->
                    <!-- Lobby banner -->
                    
                    <?php @$this->repeatData($this->content['lobby-banner']);?>
                    <?php edit($this->controller,'lobby-banner'); ?>
                    <!-- /Lobby banner -->
                    
                </div>
                
            </div>
            
            <!-- end welcome -->
            <!-- end make this content a partial -->
        </div>
        <?php if(false){ ?>
        <div class="lobby-claim-cashback">
            <h1>Hey JEN8888</h1>
            <p>You have a Friday Cash Back Bonus waiting to be claimed</p>
            
            <div class="lobby-claim-cashback-button"> <a href="/games/">CLAIM NOW</a></div>
        </div>
        <?php } ?>
    </div>
</div>
<!-- End Welcome Box -->