<script type="application/javascript">
     skinModules.push({ id: "ConvertPoints"
                         // options: { name: "Loyalty Points",
                         //            url: "/start/"
                         //       }
                          });
</script>
<div>
    <div class="myaccount-details__column convert-points">
        <div class="start-convert-points">
            <h3>Convert loyalty points into bonus money</h3>
            <p>How many Loyalty Points do you want to convert?</p>
            <input data-bind="value: ConvertPoints.pointsToConvert, valueUpdate: ['input', 'keypress']" placeholder="Minimum 1000" type="text" maxlength="50">
            <span class="error" data-bind="visible: ConvertPoints.errorMsg, text: ConvertPoints.errorMsg"></span>
            <a data-bind="click: ConvertPoints.convert" href="" class="button">Convert Points</a>
            <div class="convert-points-info">
                <a href="/loyalty/">More information</a>
            </div>
        </div>
    </div>
</div>
