
<!-- game menu container -->
<div class="games-filter-menu__popover game-filter-menu ui-game-filter-menu">
	<span class="nub"></span>
	<dl class="tabs ui-filter-tabs" data-tab>
		<dd id="ui-features2" class="active"><a href="#panel2_">Features</a></dd>
		<dd id="ui-paylines2"><a href="#panel3_">Paylines</a></dd>
		<dd id="ui-themes2"><a href="#panel1_">Themes</a></dd>
	</dl>
	<div class="tabs-content">
		<div class="content" id="panel1_">
			<ul>
				<li><a id="ui-jackpot-slots" data-gtag="Game Menu,Themes+Jackpot Slots" href="/slots/jackpot-slots/" data-hijack="true">Jackpot <br>Slots</a></li>
				<li><a id="ui-magic-myth" data-gtag="Game Menu,Themes+Magicand Myth" href="/slots/mystical-slots/" data-hijack="true">Mystical and Magic</a></li>
				<li><a id="ui-animals-nature" data-gtag="Game Menu,Themes+Animals and Nature" href="/slots/animal-lovers/" data-hijack="true">Animal Lovers</a></li>
				<li><a id="ui-movie-adventure" data-gtag="Game Menu,Themes+Movie and Adventure" href="/slots/movie-and-adventure/" data-hijack="true">Movie and Adventure</a></li>
				<li><a id="ui-egyptian" data-gtag="Game Menu,Themes+Eygptian" href="/slots/egyptian/" data-hijack="true">Egyptian</a></li>
				<li><a id="ui-pirate-treasures" data-gtag="Game Menu,Themes+Pirates and Treasures" href="/slots/pirates-and-adventure/" data-hijack="true">Pirates and Adventure</a></li>
				<li><a id="ui-exclusive" data-gtag="Game Menu,Themes+Exclusive" href="/slots/exclusive-games/" data-hijack="true">Exclusive<br>Games</a></li>
				<li><a id="ui-new" data-gtag="Game Menu,Themes+New" href="/slots/brand-new-slots/" data-hijack="true">Brand New<br>Slots</a></li>
				<li><a id="ui-featured" data-gtag="Game Menu,Themes+Featured" href="/slots/featured-games/" data-hijack="true">Featured<br>Games</a></li>

			</ul>
		</div>
		<div class="content active" id="panel2_">
			<ul>
			<li><a id="ui-all-slots" data-gtag="Game Menu,Themes+Featured" href="/slots/all-slots/" data-hijack="true">All<br>Slots</a></li>
				<li><a id="ui-free-spins" data-gtag="Game Menu,Features+Free Spins" href="/slots/free-spins-features/" data-hijack="true">Free <br>Spins<br>Features</a></li>
				<li><a id="ui-bonus-round" data-gtag="Game Menu,Features+Bonus Round" href="/slots/bonus-round-features/" data-hijack="true">Bonus <br>Round</a></li>
				<li><a id="ui-tumbling-reels" data-gtag="Game Menu,Features+Tumbling Reels" href="/slots/3-reel/" data-hijack="true">3 Reel</a></li>
				<li><a id="ui-multiways-extra" data-gtag="Game Menu,Features+Multiways Extra" href="/slots/5-reel/" data-hijack="true">5 Reel</a></li>
				<li><a id="ui-stacked-wilds" data-gtag="Game Menu,Features+Stacked Wilds" href="/slots/stacked-wild-features/" data-hijack="true">Stacked Wilds</a></li>

				<li><a id="ui-click-me" data-gtag="Game Menu,Features+Click Me" href="/slots/click-me-features/" data-hijack="true">Click Me</a></li>

						<li><a id="ui-table-cards" data-gtag="Game Menu,Themes+Featured" href="/table-games/" data-hijack="true">Table & Cards</a></li>
						<li><a id="ui-scratchcards" data-gtag="Game Menu,Themes+Featured" href="/scratch-cards/" data-hijack="true">Scratch Cards</a></li>
			</ul>
		</div>
		<div class="content" id="panel3_">
			<ul>
				<li><a id="ui-10-less" data-gtag="Game Menu,Paylines+10 & Less" href="/slots/10-and-less-line-slots/" data-hijack="true">10 & Less</a></li>
				<li><a id="ui-15" data-gtag="Game Menu,Paylines+15" href="/slots/15-line-slots/" data-hijack="true">15</a></li>
				<li><a id="ui-20" data-gtag="Game Menu,Paylines+20" href="/slots/20-line-slots/" data-hijack="true">20</a></li>
				<li><a id="ui-25" data-gtag="Game Menu,Paylines+25" href="/slots/25-line-slots/" data-hijack="true">25</a></li>
				<li><a id="ui-30" data-gtag="Game Menu,Paylines+30" href="/slots/30-line-slots/" data-hijack="true">30</a></li>
				<li><a id="ui-40" data-gtag="Game Menu,Paylines+40" href="/slots/40-line-slots/" data-hijack="true">40</a></li>
				<li><a id="ui-50-over" data-gtag="Game Menu,Paylines+50 & Over" href="/slots/50-and-more-line-slots/" data-hijack="true">50 & Over</a></li>
			</ul>  </div>
		</div>
	</div>
