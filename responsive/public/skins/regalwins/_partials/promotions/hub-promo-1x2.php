<article class="slot-promo">
	<div class="slot-promo__container">
		<img src="<?php echo $row["Image"];?>" alt="<?php echo $row["Alt"];?>">
		<div class="slot-promo__footer--25x100">
			<h5 class="slot-promo__footer__title"><?php echo $row["Title"];?></h5>
			<div class="slot-promo__footer__cta">view more</div>
		</div>
	</div>
	<div class="slot-promo__border">
		
	</div>
</article>