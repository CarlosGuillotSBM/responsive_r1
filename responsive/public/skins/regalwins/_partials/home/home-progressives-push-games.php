<?php if ($row['DetailsURL'] !== 'playBingo') { ?>
<script type="application/javascript">

	var sbm = sbm || {};
	sbm.games = sbm.games || [];
	sbm.games.push({
		"id"    : <?php echo json_encode($row['DetailsURL']);?>,
		"icon"  : "<?php echo $row['Image'];?>",
		"bg"    : <?php echo json_encode($row['Text1']);?>,
		"title" : <?php echo json_encode($row['Title']);?>,
		"type"  : "game",
		"desc"  : <?php echo json_encode($row["Intro"]);?>,
		"thumb" : "<?php echo $row['Image1'];?>",
		"detUrl": "/slots/game/<?php echo $row['DetailsURL'];?>"
	});

</script>    
	<?php } ?>