
<div class="content-holder home-featured-games-holder">
	<div class="content-holder__content">

		<div class="content-holder__content_left">
            <div class="content-holder__inner start-content">
                
                <script>
                    skinPlugins.push({
                        id: "live-casino",
                        options: {
                            global: true,
                            auto: false
                        }
                    });
                </script>

                <div class="lobby-wrap__content-box__header heading--skin">
                    <?php edit($this->controller,'start-livecasino-games-section'); ?>
                    <?php @$this->repeatData($this->content['start-livecasino-games-section']);?>
                </div>

            </div>
		</div>

	</div>
</div>
