
<div class="content-holder home-featured-games-holder">
	<div class="content-holder__content">

		<div class="content-holder__content_left">
            <div class="content-holder__inner start-content">

                <script type="application/javascript">
                        skinPlugins.push({
                            id: "ProgressiveSlider",
                            options: {
                                slidesToShow: 3,
                                mode: "horizontal",
                                infiniteLoop: true
                        }
                    });
                </script>

                <div class="progressive-jackpots__wrapper slider">
                    <img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
                    <article class="progressive-jackpots">
                        
                        <div class="progressive-jackpots__title">
                            <h1>Progressive jackpots</h1>
                        </div>
                        <!-- <div class="progressive-jackpots__link">
                            <a href="#">view all</a>
                        </div> -->
                        <?php edit($this->controller,'progressives'); ?>
                        <?php @$this->repeatData($this->content['progressives']);?>
                    </article>
                </div>
            
            </div>
		</div>

	</div>
</div>
