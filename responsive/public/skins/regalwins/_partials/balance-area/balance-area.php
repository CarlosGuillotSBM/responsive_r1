<div data-bind="visible: validSession" style="display: none" class="balance-area">
<!-- <div class="balance-area"> -->
    <!-- <div class="balance-area__left"> -->

    <!-- <a href="/my-account/" class="balance-area__user-name" data-bind="text: username" data-hijack="true"></a>
    <a class="cashier-button" href="/cashier/" data-hijack="true">CASHIER</a> -->


    <!-- </div> -->

    <div  class="balance-area__right">
        <div class="balance-area__title">BALANCES</div>
        <ul>
            <li class="balance-area__title__user-name">
                <a href="/my-account/" data-hijack="true">
                    <i><img src="/_images/common/icons/user-24x24.png"></i>
                    <span data-bind="text: username"></span>
                </a>
            </li>
            <li>
                <a href="/promotions/win-a-trip-to-vegas/">
                   <i title="Tickets"><img src="/_images/balance-area/icon-tickets.png" alt="Tickets" blockquote="Tickets"></i>
                   <span class="balance-area__value" data-bind="text: tickets" title='Tickets'></span>
                </a>
            </li>
            <li>
                <a href="/my-account/" data-hijack="true">
                    <i><img src="/_images/balance-area/icon-moolah-coin.png" alt="Moolah Balance" title="Moolah Balance" blockquote="Moolah Balance" ></i>
                    <span class="balance-area__value" data-bind="text: points" title='Moolah Balance'></span>
                </a>
            </li>
            <li>
                <a href="/slots/exclusive/" data-hijack="true">
                    <i><img src="/_images/balance-area/icon-free-spins.png" alt="Free Spins" title="Free Spins Balance" blockquote="Free Spins Balance"></i>
                    <span class="balance-area__value" data-bind="text: bonusSpins" title="Free Spins Balance"></span>
                </a>
            </li>


        </ul>

    </div>



</div>