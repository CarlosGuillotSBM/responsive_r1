<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg width="22px" height="15px" viewBox="0 0 22 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
    <!-- Generator: Sketch 3.3.2 (12043) - http://www.bohemiancoding.com/sketch -->
    <title>mail</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="icon-artboard" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
        <g id="iOS-icon-kit" sketch:type="MSArtboardGroup" transform="translate(-286.000000, -558.000000)" fill="#444444">
            <g id="Icon-sets" sketch:type="MSLayerGroup">
                <g id="Settings-Icons" transform="translate(100.000000, 400.000000)" sketch:type="MSShapeGroup">
                    <g id="SVGs" transform="translate(60.000000, 44.000000)">
                        <path d="M136.93612,124.350111 L133.845516,121.58269 L126.424482,128.212225 C126.624553,128.375908 126.880544,128.473684 127.15955,128.473684 L146.84045,128.473684 C147.151815,128.473684 147.435669,128.349767 147.644594,128.148326 L140.247347,121.540039 L137,124.421056 L136.93612,124.350111 L136.93612,124.350111 Z M137.060116,123.738481 L126.485426,114.216988 C126.675757,114.080413 126.908727,114 127.15955,114 L146.84045,114 C147.121865,114 147.379866,114.099473 147.580689,114.265714 L137.060116,123.738481 L137.060116,123.738481 Z M147.919688,114.733207 C147.971526,114.865048 148,115.008954 148,115.159976 L148,127.313708 C148,127.444206 147.978335,127.56967 147.938444,127.686737 L140.682791,121.153717 L147.919688,114.733207 L147.919688,114.733207 Z M126.097236,127.781212 C126.034704,127.63849 126,127.480435 126,127.313708 L126,115.159976 C126,114.980717 126.04088,114.810955 126.113727,114.659411 L126.113727,114.659411 L133.412257,121.194736 L126.097236,127.781212 L126.097236,127.781212 Z" id="mail"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>