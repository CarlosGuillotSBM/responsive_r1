<!-- Welcome Box -->
<div class="lobby-wrap__content-box">
    <div class="start-header-banner ui-scheduled-content-container">
        <?php edit($this->controller,'start-banner'); ?>
        <?php @$this->repeatData($this->content['start-banner']);?>
    </div>

    <!-- header bar -->
    <div class="lobby-wrap__content-box__header hideme">
        <div class="title">
            <!-- <div class="title__icon"> -->
                <!-- include svg icon partial -->
                <!-- TODO: Remove when absolutely sure not needed -->
                <?php // include 'icon-account.php'; ?>
                <!-- end include svg icon partial -->
            <!-- </div> -->

            <div class="start-welcome">
                <div class="intro-wrapper">
                    <div class="title__text username-welcome">Hi, <span data-bind="text: username"></span></div>
                    <div class="title__text">
                        <?php include'_partials/common/current-vip-level.php'; ?>
                    </div>
                </div>
                <div class="claim-wrapper">
                    <?php
                        // Upgrade Bonus
                        include '_partials/start/start-claim-upgrade-bonus.php';

                        // Cashback
                        include '_partials/start/start-claim-cashback.php';
                    ?>
                </div>
            </div>

            <!-- Logic below related to the two claim upgrade bonus boxes and the confirmation boxes once you have claimed -->
            <a class="btn-lobby-deposit-nobonus" data-hijack="true" href="/cashier/" data-bind="visible: ClaimUpgradeBonus.ubAmount() == 0 && ClaimCashback.cbAmount() == 0 && ClaimCashback.cbClaimAmount() == 0 && ClaimUpgradeBonus.ubBonusAmount() == 0" style="display: none;">DEPOSIT NOW</a>
        </div>

            <!-- Mobile Message Inbox -->
            <?php if(config("inbox-msg-on") && config("SkinID") !== 8): ?>
            <div class="message-inbox-wrapper">
                <a data-hijack="true" href="/my-messages/" title="Message Inbox" class="title__message-inbox">
                    <span class="title__message-inbox__icon"></span>
                    <span>My messages</span>
                    <span class="title__message-inbox__value" data-bind="text: MailInbox.unreadMessages()"></span>
                </a>
                
                <button href="#" data-dropdown="msg-actions" aria-controls="msg-actions" aria-expanded="false" class="tiny button dropdown" data-bind="visible: MailInbox.messages().length > 0">Options</button><br>
                
                <ul id="msg-actions" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                    <li><a data-bind="click: MailInbox.markAsRead">Mark as Read</a></li>
                    <li><a data-bind="click: MailInbox.markAsUnread">Mark as Unread</a></li>
                    <li><a data-bind="click: MailInbox.delete">Delete</a></li>
                </ul>
            </div>
            <ul class="recent-messages expanded" data-bind="foreach: MailInbox.messages">
                <!-- notice the uread class in this message JS needs to append unread class to show its unread also the svg partials needs changing to have read or  -->
                <!-- single message preview -->
                <li class="recent-messages__message" data-bind="visible: $index() < 3 || $parent.MailInbox.expanded(), css: { unread: MailStatus() === 1 }">
                <!-- <li class="recent-messages__message" data-bind="visible: $index() < 3 || $parent.MailInbox.expanded(), css: { unread: MailStatus() === 1, active: $parent.MailInbox.selectedMessage() && $parent.MailInbox.selectedMessage().Id === Id }, click: $parent.MailInbox.revealSingleMessage.bind($data, Id)" data-reveal-id="single-message-modal"> -->
                    <span class="recent-messages__message__icon">
                        <span data-bind="css: { hide: MailStatus() === 2 }" ><?php include 'icon-message-unread.php'; ?></span>
                        <span data-bind="css: { hide: MailStatus() === 1 }" ><?php include 'icon-message-read.php'; ?></span>
                    </span>
                    <span class="recent-messages__message__preview" data-bind="text: Subject, click: $parent.MailInbox.revealSingleMessage.bind($data, Id)" data-reveal-id="single-message-modal"></span>
                    <span class="recent-messages__message__date" data-bind="text: Date, click: $parent.MailInbox.revealSingleMessage.bind($data, Id)" data-reveal-id="single-message-modal"></span>
                    <span class="recent-messages__message__time" data-bind="text: Time"></span>
                    <span class="recent-messages__message__select"><input type="checkbox" data-bind="checked: Clicked, click: $parent.MailInbox.check, clickBubble: false"></span>
                </li>                
                <li>
                    <?php //include'_global-library/partials/start/start-my-messages-single-modal.php'; ?>
                </li>
                <!-- End single message preview -->
                
            </ul>
            <!-- End Accordion Message Foundation -->
            <?php endif; ?>
        </div>
    </div>
    <!-- end header bar -->

    <!-- SINGLE MESSAGE MODAL -->
    <div id="single-message-modal" class="single-message-modal reveal-modal" data-reveal>
        <?php include "_global-library/partials/modals/single-message.php"; ?>
    </div>
    <!-- End single message preview -->

    <!-- <div class="lobby-wrap__content-box__content">
        <div class="small-play-wrap">
            <div class="lobby-play-button"> <a href="/games/">PLAY GAMES</a></div>
        </div>
    </div> -->

</div>
