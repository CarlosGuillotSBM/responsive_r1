<div class="lobby-wrap__content-box welcome-area-wrap">
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-account.php'; ?>
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text">WELCOME <span data-bind="text: username"></span></div>
            
            <div class="title__link"></div>

            <div class="title__last-login" data-bind="visible: lastLoginDate !== ''">Last login date: <span data-bind="text: lastLoginDate"></span></div>
            
            <!-- Mobile Message Inbox -->
            <?php if(config("inbox-msg-on")): ?>
                <a data-hijack="true" href="/start/" title="Message Inbox" class="title__message-inbox">
                    <span class="title__message-inbox__icon"></span>
                    <span class="title__message-inbox__valuec" data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
                </a>
            <?php endif; ?>
        </div>
    </div>
    <!-- end header bar -->
    <!-- Mobile version of cashback -->
    <?php //include'start-claim-cashback--mobile.php'; ?>
    <!-- End Mobile version of cashback -->
    <div class="lobby-wrap__content-box__content">

<!--         <div class="small-play-wrap">
            <div class="lobby-play-button"> <a href="/games/">PLAY GAMES</a></div>
        </div>
        <div class="small-deposit-wrap">
            <div class="lobby-deposit-button"> <a href="/cashier/">DEPOSIT NOW</a></div>
        </div> -->

        <div class="lobby-wrap__content-box__content__innerwrap--border">
            <!--  make this content a partial -->
            <!-- welcome area -->
            
            <div class="welcome-area">
                <div class="welcome-area__top">


                    <div class="welcome-area__top__left ui-scheduled-content-container"> 
                        <?php edit($this->controller,'start-intro-text'); ?>
                        <article class="welcome-intro">
                            <?php @$this->repeatData($this->content['start-intro-text']);?>
                        </article>
                    </div>

                    <div class="welcome-area__top__right">
                        <!-- VIP level slider not being used yet-->
                        <?php // include'_global-library/partials/start/start-current-vip-slider.php'; ?>
                        <!-- VIP current level -->
                        <?php include'_partials/common/current-vip-level.php'; ?>
                    </div>

                    <script>

                        skinModules.push({
                            id: "ContentScheduler"
                        });

                    </script>



                    <div class="welcome-area__top__bottom ui-scheduled-content-container">
                        <!-- email banner image goes here -->
                        <!--     <img class="welcome-banner" src="http://placehold.it/693x142?text=Offer+Image"> -->
                        <!-- Lobby banner -->
                        
                        <?php @$this->repeatData($this->content['start-banner']);?>
                        <?php edit($this->controller,'start-banner'); ?>
                        <!-- /Lobby banner -->
                    
                     </div>

                </div>



                
            </div>
            
            <!-- end welcome -->
            <!-- end make this content a partial -->
        </div>
    </div>
</div>
<!-- End Welcome Box