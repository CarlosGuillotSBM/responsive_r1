<script>

    skinPlugins.push({
        id: "FeaturedGames",
        options: {
            global: true
        }
    });

</script>


<!-- Featured Games Box with tabbed content -->
<div class="lobby-wrap__content-box">
    <!--  header bar -->
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php //include "icon-featured-game.php";?>
                <!--<?php //include "star-icon-black.php";?>-->
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text"><?php echo config("featuredGamesSentence"); ?></div>
            <div class="title__link">
                <a class="title__viewall--featured-games" href="/games/">View All</a>
            </div>


            <!-- Aspers only  -->
            <a style="display: none" class="featured-category-dd" data-hijack="true" href="/games/" class="tiny button dropdown">View All</a><br>
            <!-- / Aspers only  -->

            <button class="featured-category-dd" href="#" data-dropdown="featured-games-category" aria-controls="featured-games-category" aria-expanded="false" class="tiny button dropdown">View All &#9660;</button><br>
            <ul id="featured-games-category" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">

<?php
    $this->gamesCategory = array();
    $i=1;
    do {
        if(count($this->games) > 0) {
            $gc = [
                'id' => 'lobbypanel' . $i,
                'index' => $i++,
                'Games' => $this->games,
                'Category' => $this->category
            ];
            $this->gamesCategory[] = $gc;
?>
                <li<?php if($gc['index'] == 1){echo ' class="active"';}?>>
                    <a class='lazy-load-on-click' href="#<?php echo $gc['id'];?>"><?php echo $gc["Category"]["Description"];?></a>
                </li>
<?php
        }
    } while ($this->GetNextCategory() && $i < 7);
?>
            </ul>            

        </div>
    </div>
    <!-- end header bar -->
    <!-- content-->
    <div class="lobby-wrap__content-box__content featured-games">
        
        <!--  make this content a partial -->
        <dl class="tabs" data-tab>
<?php
            foreach ($this->gamesCategory as $gc) {
?>
            <dd<?php if ($gc['index']==1){echo ' class="active"';}?>><a class='lazy-load-on-click' href="#<?php echo $gc['id'];?>"><?php echo $gc["Category"]["Description"];?></a></dd>
<?php
            };
?>

       <!--     <dd><a href="/games/">All Games</a></dd>          -->
            
        </dl>
        <div class="tabs-content">
<?php
            foreach ($this->gamesCategory as $gc) {
?>
                <div class="ui-featured-tab content <?php if($gc['index']==1){echo 'active';}?>" id="<?php echo $gc['id'];?>">
                    <?php foreach($gc["Games"] as $key => $row) {?>
                    
                        <div class="lobby-game"><?php include '_global-library/_editor-partials/game-icon.php'; ?></div>
<?php if ($key==7) {break;}
                    }
?>
                </div>
<?php
    }
?>
        </div>
        <!-- end make this content a partial -->
    </div>
    
</div>
<!-- end featured games -->