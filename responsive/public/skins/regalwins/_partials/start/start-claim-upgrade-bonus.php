
<div style="display:none" data-bind="visible: ClaimUpgradeBonus.ubID() != 0" class="upgrade-bonus">
	<div class="lobby-claim-cashback pr" data-bind="visible: ClaimUpgradeBonus.ubAmount() > 0" style="display: none;">
		<h3>Upgrade Bonus</h3>	
		<p class="upgrade-text">
			You are eligible for an upgrade bonus
		</p>
		<p class="upgrade-expiry" data-bind="visible: ClaimUpgradeBonus.lastDays">
			Expires on
			<span data-bind="text: ClaimUpgradeBonus.ubDate"></span>.
		</p>
		<a>
			<div class="lobby-claim-cashback-button" data-bind="click: ClaimUpgradeBonus.claimUpgradePlayerBonus">CLAIM BONUS</div>
		</a>
		<div class="upgrade-info">
			<a href="#" data-bind="click: ClaimUpgradeBonus.showInfo">T&amp;Cs apply</a>
		</div>
	</div>
	<div class="lobby-claim-cashback alert-box secondary" data-bind="visible: ClaimUpgradeBonus.ubBonusAmount() > 0">
		<h1>You have successfully claimed a <span data-bind="text: ClaimUpgradeBonus.currency"></span><span data-bind="text: ClaimUpgradeBonus.ubBonusAmount"></span><br>upgrade bonus</h1>
		<a href="#" class="close" data-bind="click: ClaimUpgradeBonus.hidePanel">&times;</a>
	</div>
</div>
