<script type="application/javascript">  
    skinModules.push({
        id: "RadFilter",
        options: {
            max: [{ elementClass: 'promotion-eng',
                count: 2}]
            }
        });
</script>
<!--  My Promotions -->
<div class="lobby-wrap__content-box">
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-promotions.php'; ?>
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text">MY PROMOTIONS</div>
            <div class="title__link">
                <a data-hijack="true" href="/promotions/">View All <span class="title__link__arrow">&raquo; </span></a>
            </div>
        </div>
    </div>
    <!-- end header bar   DEP50+,VIPGOLD-->
    <div class="lobby-wrap__content-box__content my-promotions">
        <?php @$this->repeatData($this->promotions["promotions"],1, "_global-library/partials/promotion/promotion-start-page.php"); ?>
       

    </div>
</div>
<!-- End My Promotions -->