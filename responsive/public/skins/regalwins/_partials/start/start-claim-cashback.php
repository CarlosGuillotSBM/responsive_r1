

<div style="display:none" data-bind="visible: ClaimCashback.cbID() != 0">
	<div class="lobby-claim-cashback" data-bind="visible: ClaimCashback.cbAmount() > 0">
		<h3>Loyalty Rewards</h3>
		<p class="upgrade-text">
			<!-- GBB, RW & LVIP -->
			<?php if (config("SkinID") == 9 || config("SkinID") == 10 || config("SkinID") == 8 ) : ?>
				You have <span data-bind="text: ClaimCashback.currency"></span><span data-bind="text: ClaimCashback.cbAmount"></span>
				<span style="display: none" data-bind="visible: validSession() && (playerClass() == 2 || playerClass() == 3 || playerClass() == 4 || playerClass() == 5)">loyalty reward waiting</span>
			<?php endif;?>
		</p>
        <p class="upgrade-expiry" data-bind="visible: ClaimCashback.lastDays">
			Expires on <span data-bind="text: ClaimCashback.cbDate"></span>.
		</p>
        <a>
			<div class="lobby-claim-cashback-button" data-bind="click: ClaimCashback.claimPlayerCashback"> CLAIM NOW</div>
		</a>
		<div class="upgrade-info">
            <a href="#" data-bind="click: ClaimCashback.showInfo.bind($data, playerClass())">T&amp;Cs apply</a>
        </div>
    </div>

	<!--Message displayed after cashback/bonusback is redeemed-->
	<div class="lobby-claim-cashback alert-box secondary" data-bind="visible: ClaimCashback.cbClaimAmount() > 0">
		<a href="#" class="close" data-bind="click: ClaimCashback.hidePanel">&times;</a>
		<!-- GBB, RW & LVIP -->
		<?php if (config("SkinID") == 9 || config("SkinID") == 10 || config("SkinID") == 8 ) : ?>
			<h1 style="display: none" data-bind="visible: validSession() && (playerClass() == 2 || playerClass() == 3 || playerClass() == 4 || playerClass() == 5)">
			You have successfully claimed <span data-bind="text: ClaimCashback.currency"></span><span data-bind="text: ClaimCashback.cbClaimAmount"></span> of loyalty rewards
		</h1>
		<?php endif;?>
    </div>
</div>
