<script type="application/javascript">
skinModules.push({
     id: "LoginBox",
     options: {
          modalSelectorWithoutDetails: "#custom-modal",
          global: true
     }
});
</script>
<article class="login-box">
     <h1>Login</h1>

     <div class="login-box__form-wrapper">

          <form class="" id="ui-login-form">

               <div class="login-box__input-wrapper">
                    <input type="text" placeholder="Username or email" data-bind="value: LoginBox.username, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" />
                    <span style="display:none" data-bind="visible: LoginBox.invalidUsername" class="error">This field is required.</span>
               </div>
               <div class="login-box__input-wrapper">
                    <input type="password" placeholder="Password" data-bind="attr:{type: LoginBox.passwordShowText() == 'Hide' ? 'text': 'password'}, value: LoginBox.password, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}"/>
                    <span style="display:none" data-bind="visible: LoginBox.invalidPassword" class="error">This field is required.</span>
                    <div class="login-box__show-password show-password eye-shut" data-bind="click: LoginBox.togglePassword">
                         <a class="right" data-bind="text: LoginBox.passwordShowText">Show</a>
                    </div>
               </div>
               <a style="display: none" data-bind="visible: LoginBox.lockedUser, text: LoginBox.loginError" class="error" href="/support"></a>
          <!-- ko ifnot: LoginBox.lockedUser -->
               <span style="display:none" data-bind="visible: LoginBox.loginError, html: LoginBox.loginError" class="error"></span>
          <!-- /ko  -->
               <span class="login-box__login" data-bind="click: LoginBox.doLogin.bind($data, GameLauncher.gameId(), GameLauncher.gamePosition(), GameLauncher.gameContainerKey(), GameLauncher.roomId())">Login</span>
               <div class="clearfix"></div>
               <!-- <a data-gtag="Join Now,Login Box" href="/register/" data-hijack="true" class="login-box__register-link green-cta">JOIN NOW</a> -->
               <a href="/forgot-password/" class="login-box__forgot-password">Forgot Password?</a>
               <a data-gtag="Join Now,Login Box" href="/register/" data-hijack="false" class="login-box__register-here">Don't have an account? Register here</a>

               <div class="gamble-responsibly">
                    <img src="/_images/revamp/common/icons/18plus-gambling-commission.svg" title="18+ Gambling Commission">
                    <p>We encourage you to gamble responsibly while on Regalwins.com</p>
               </div>

          </form>
     </div>

</article>
