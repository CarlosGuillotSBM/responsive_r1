<!-- MIDDLE CONTENT -->
<div class="middle-content">
	<!-- breadcrumbs -->
	<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
	<!--/ breadcrumbs -->
	<div class="middle-content__box">
		<section class="section">
			<h1 class="section__title">ABOUT</h1>
			<!-- CONTENT -->
			<?php edit($this->controller,'about-us'); ?>
			<div class="content-template">
				<!-- repeatable content -->
				<?php $this->repeatData($this->content['about-us']);?>
				<!-- /repeatable content -->
			</div>
			<!-- /CONTENT-->
		</section>
	</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->