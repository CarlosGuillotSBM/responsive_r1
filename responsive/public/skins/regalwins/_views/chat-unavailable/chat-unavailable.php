
<!-- MIDDLE CONTENT -->
<div class="middle-content">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!-- /home breadcrumbs -->
  <section class="section">
    <div class="section-left__title">
      <h1 class="section__title">Chat Unavailable</h1>
    </div>
    <!-- CONTENT -->
    <?php edit($this->controller,'chat-unavailable'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['chat-unavailable']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->