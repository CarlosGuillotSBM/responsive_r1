
<div class="loyaltycrumbs">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
</div>
<div class="middle-content__box">

  <section class="section">
    <h1 class="section__title"><?php echo $this->controller; ?></h1>
    <!-- CONTENT -->
    <?php edit($this->controller,'vip-points'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['vip-points']);?>
      <!-- /repeatable content -->
      
      <div class="loyalty__container__row">
        <div class="loyalty__container__button">
          <a style="display: none" data-bind="visible: !validSession()" id="join-loyalty" href="/register/"  class="button loyalty__cta  expand">JOIN NOW</a>
          <a style="display: none;" data-bind="visible: validSession()" href="/start/" class="button promo__cta  expand">PLAY NOW</a>
        </div>
      </div>
      <div class="cleafix"></div>
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->