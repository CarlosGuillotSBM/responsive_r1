<section class="section">
         <?php edit($this->controller,'hub'); ?>
        <div class="section__header">
            <h1 class="section__title">Your Favourite Games</h1>
        </div>
        <!-- Hub grid -->
        <?php include "_global-library/partials/hub/hub-grid-4.php" ?>
        <!-- /Hub grid -->
    </section>

    <div class="clearfix"></div>
    
    <!-- games list-->
    <?php include '_global-library/partials/games/games-list.php'; ?>
    <!-- end games list -->

