<!-- <script type="application/javascript">
skinPlugins.push( {
id: "Slick",
options: {
global: true,
element: 'bxslider-home-home'
}
} );
</script> -->

<!-- hero image (sits behind top nav) -->
<div class="home-hero-image">
    <?php edit($this->controller,'content-hero-banner'); ?>
    <?php @$this->repeatData($this->content['content-hero-banner']); ?>
    <?php //include '_partials/banner/homepage-hero-image.php'; ?>
</div>


<main class="regalwins--homepage">

    <!-- MIDDLE CONTENT -->
    <div class="middle-content">

        <!-- home container -->
        <div class="home-container">
            <!-- MAIN CONTENT AREA -->

            <?php
                // games nav
                // Always included as will not be visible on larger viewports that are considered mobile viewports by php config device
                // Visibilty handled by css
                include '_partials/games-menu/games-nav.php';

                // Mobile
                if(config("Device") == 2) {
                    // New games slider
                    include '_partials/home-sliders/slider-new-games.php';

                    // Welcome message intro
                    include '_partials/home/home-welcome-intro.php';

                    // scheduled banner
                    include '_partials/common/scheduled-banner.php';

                    // Progressive slots slider
                    include '_partials/home-sliders/slider-progressive-slots.php';

                    // Other games carousels
                    include '_partials/home-sliders/home-carousel-slider-multiple.php';
                }
                // Tablet & Desktop 
                else {
                    // Note: commented block here uses the old style whereby games are added one-by-one
                    // slots by type
                    // include '_partials/games-menu/games-menu.php';

                    // Progressive jackpots
                    include '_partials/home-sliders/slider-progressive-slots.php';

                    // Welcome message full
                    include '_partials/home/home-welcome-full.php';

                    // scheduled banner
                    include '_partials/common/scheduled-banner.php';

                    // Category widget games list
                    include '_partials/home/games-list.php';
                }
            ?>

            <script type="application/javascript">
                skinPlugins.push({
                        id: "ProgressiveSlider",
                        options: {
                        slidesToShow: 1000,
                        mode: "vertical"
                    }
                });
                skinPlugins.push({
                    id: "Slick",
                    options: {
                        slideWidth: 350,
                        responsive: true,
                        minSlides: <?php if (config("RealDevice") == 1) echo 3; else if (config("RealDevice") == 2) echo 2; else echo 1;?>,
                        maxSlides: 10,
                        slideMargin: 10,
                        auto: true,
                        infinite: false,
                        element: "bingo-progressives-slider",
                        pager: false
                    }
                });      
            </script>

            <!-- SEO CONTENT -->
            <section id="welcomefull" class="section middle-content__box">
                <!-- content -->

                <div class="ui-accordion">
                <?php
                    $seoKey = "home-seo-content";
                    include'_global-library/partials/home-seo/mobile.php';  
                ?>
                </div>
            
                <!-- /content -->
            </section>
            <!-- /SEO CONTENT -->

        </div>
        <!-- end home container -->
    </div>
    <!-- END MIDDLE CONTENT -->

</main>