<!-- MIDDLE CONTENT --> 
<div class="middle-content">
    <script type="application/javascript">
    skinModules.push({ id: "ForgotPassword" });
    </script>

    <div class="middle-content__box">
      <!-- breadcrumbs -->
      <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
      <!-- END breadcrumbs -->

      <section class="section">
        <!-- CONTENT -->
        <div class="content-template">

        <!-- FORGOT PASSWORD FORM -->

          <form id="forgotpass" class="hide" name="forgotpass" method="post" action="" style="display: block;">
              <div class="forgotpass__wrapper">
                  <h1>Forgot password</h1>

                  <div class="forgotpass__input">
                      <div data-bind="visible: ForgotPassword.forgot" style="display: none">
                          <p>Enter your e-mail address and we will send you a link to change your password:</p>
                          <p>
                              <input data-bind="value: ForgotPassword.email" type="text" name="textfield" id="fpwd_email" placeholder="Enter your email address">
                              <span data-bind="visible: ForgotPassword.invalidEmail" class="error">Please enter a valid email.</span>
                          </p>
                          <p><input data-bind="click: ForgotPassword.recoverPassword" type="button" name="button" id="btn_submit_forgotpass" value="Send"
                          class="button expand"></p>
                      </div>
                  </div>
                  <div data-bind="visible: ForgotPassword.recover" style="display: none">
                      <p>Enter and repeat your new password:</p>
                      <p><input data-bind="value: ForgotPassword.newPassword" type="password" placeholder="Enter new password" name="textfield" id="reset_pass"></p>
                      <p>
                          <input data-bind="value: ForgotPassword.repeatPassword" type="password" placeholder="Confirm your password" name="textfield" id="reset_pass_repeat">
                          <span data-bind="visible: ForgotPassword.passwordsDoNotMatch" class="error">Password fields do not match.</span>
                          <span data-bind="visible: ForgotPassword.passwordsAreEmpty" class="error">Please fill both fields.</span>
                          <span data-bind="visible: ForgotPassword.invalidNewPassword" class="error">Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces.</span>
                      </p>
                      <p><input data-bind="click: ForgotPassword.resetPassword" type="button" name="button" id="btn_submit_resetpass" value="RESET PASSWORD"
                      class="button expand"></p>
                </div>

                <a data-reveal-id="login-modal" class="login-box__forgot-password">Back to login</a>
                <a data-gtag="Join Now,Login Box" href="/register/" data-hijack="false" class="login-box__register-here">Don't have an account? Register here</a>

                <div class="gamble-responsibly">
                    <img src="/_images/revamp/common/icons/18plus-gambling-commission.svg" title="18+ Gambling Commission">
                    <p>We encourage you to gamble responsibly while on Regalwins.com</p>
               </div>
            </div>
        </form>
    </div>
  </section>

</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->