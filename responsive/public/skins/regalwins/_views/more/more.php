

<div class="middle-content">
    <?php include '_partials/games-menu/games-nav.php'; ?>

    <div class="more-container">
        <ul>
            <li><a href="/loyalty/">VIP & Loyalty</a></li>
            <li><a href="/banking/">Banking info</a></li>
            <li><a href="/about-us/">About us</a></li>
            <li><a href="/responsible-gaming/">Play Responsibly</a></li>
            <li><a href="/support/">Get in touch</a></li>
            <li><a href="/terms-and-conditions/">The Legal Stuff</a></li>
            <li><a href="/faq/">FAQ</a></li>
            <li><a href="/complaints-and-disputes/">Complaints & Disputes</a></li>
            <li><a href="/privacy-policy/">Privacy Matters</a></li>
            <li><a href="/archive/">Archive</a></li>
        </ul>

        <div class="logout-cta-container">
            <a data-hijack="true" title="Log Out" style="" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">Log Out</a>
        </div>
    </div>
</div>