<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<!-- MIDDLE CONTENT -->
<div class="middle-content__box">
  <section class="section">
    <h1 class="section__title">Bring Home the Magic...</h1>
    <!-- CONTENT -->
    <?php edit($this->controller,'mv515'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['mv515']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->