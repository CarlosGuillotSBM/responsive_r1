<script type="application/javascript">
skinModules.push( { id: "ClaimCashback" } );
skinModules.push( { id: "ClaimUpgradeBonus" } );
</script>

  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!--/ breadcrumbs -->
<!-- Lobby Wrap -->
<div class="middle-content lobby-wrap">
    <!-- Lobby Left Column -->
    <div class="lobby-wrap__left">
        <!-- welcome section -->
        <?php  include'_partials/start/start-welcome-section.php'; ?>
        <!-- my messages section -->

        <?php
            // MOBILE & TABLET
            if(config("Device") == 2) {
                // Progressive jackpots
                include '_partials/home-sliders/slider-progressive-slots.php';

                // Other games carousels
                include '_partials/home-sliders/home-carousel-slider-multiple.php';
            }
            // DESKTOP
            else {
                // Progressive jackpots
                include '_partials/home-sliders/slider-progressive-slots.php';

                // Category widget games list
                include '_partials/home/games-list.php';

                // Featured games hub - old
                //include '_partials/home-hub/hub-featured-games.php';

                // Popular slots hub - old
                //include '_partials/home-hub/hub-popular-slots.php';

                // New slots hub - old
                //include '_partials/home-hub/hub-new-slots.php';
            }
        ?>

        <!-- my promotions section -->
        <?php // include'_partials/start/start-my-promotions-section.php'; ?>
    </div>
    <!-- End Left Column -->
    <!-- Right Column -->
    <div class="game-url-seo lobby-wrap__right">
        <div class="two-columns-content-wrap__right">
 
            <!-- lobby right deposit button -->
            <?php // include'_partials/start/start-right-deposit-button.php'; ?>
            <!-- lobby claim cashback button -->
            <?php // include'_partials/start/start-claim-cashback.php'; ?>
            <!-- lobby claim upgrade bonus button -->
            <?php //include'_partials/start/start-claim-upgrade-bonus.php'; ?>
            <!-- lobby right progressives -->
            <?php // include'_partials/start/start-right-progressives.php'; ?>


            <!-- Uncomment these 2 partials once is required -->
            <!-- lobby yesterdays wins -->
            <?php  //include'_partials/start/start-right-yesterdays-wins.php'; ?>
            <!-- lobby right latest winners -->
            <?php  //include'_partials/start/start-right-latest-winners.php'; ?>

        </div>
    </div>
    <!-- End Right Column -->
</div>
<!-- End Lobby Wrap -->

<?php 
    // dont miss out popup
    include "_global-library/partials/modals/dont-miss-out.php";
?>