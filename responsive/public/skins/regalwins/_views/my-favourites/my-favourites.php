<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<div class="middle-content__box">

	<!-- games slider nav -->
	<?php include '_partials/games-menu/games-nav.php'; ?>
	<!-- end games slider nav -->

	<section class="section middle-content__box footer-padding">
		<div class="section__header">
			<h1 class="section__title">My Favourites</h1>
		</div>
		<!-- Favourites row -->
		<div class="favourites">
			<?php include "_global-library/_editor-partials/favourite-game-widget.php" ?>
		</div>
		<!-- /Favourites row -->
	</section>
	<!-- /FAVOURITES -->
</div>	
