<!DOCTYPE html>
<html>
<head>
	<title>Under Maintenance</title>
<style type="text/css">
	body{
		background-color:black;
		color:white;
		text-align: center;
		font-size: 20px;
		font-family: arial;
	}
	h1{
		font-size: 40px;		
	}
    .logo {
        width: 220px;
        height: 160px;
    }
</style>
</head>
<body>
<br><br><br>
<img src="/_images/common/header/header-logo.svg" class="logo">
	<h1>Under Maintenance</h1>
	<div>Sorry, we are offline for just a few minutes. <br><br>Please come back soon.</div>
</div>
</body>
</html>