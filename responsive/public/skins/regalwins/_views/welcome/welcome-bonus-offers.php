<!-- BONUSES -->
<?php
$bonusCount = isset($this->content['welcome-offers']) ? count($this->content['welcome-offers']) : 0;
?>

<section class="bonus">

    <?php if ($bonusCount > 0): ?>

        <h2>You have <?php echo $bonusCount; ?> bonus<?php if ($bonusCount > 1) echo 'es';?>  ready to use now – ENJOY</h2>

    <?php endif; ?>


    <ul>

        <?php edit($this->controller,'welcome-offers',@$this->action); ?>
        <?php @$this->repeatData($this->content['welcome-offers']); ?>

    </ul>

</section>