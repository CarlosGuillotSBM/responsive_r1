<!-- MIDDLE CONTENT -->
<div class="middle-content">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<div class="middle-content__box">
  <?php include '_global-library/partials/games/games-details-page.php'; ?>
  <div class="games-detail-page__bottom">
    <?php edit('gamesdetail','games-detail',$this->row['DetailsURL']); ?>
    <?php @$this->repeatData($this->content['games-detail']); ?>
  </div>
</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->