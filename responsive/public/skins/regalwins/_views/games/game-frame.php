<?php

    $skinId = @$_GET["skinID"];
    $playerId = @$_GET["PlayerID"];
    $sessionId = @$_GET["SessionID"];
    $gameId = @$_GET["GameID"];
    $fun = @$_GET["fun"];
    $source = @$_GET["GameLaunchSourceID"];
    $myAccountURL = config("URL").config("accountActivityURL");
    $deviceId = config("Device");
    $Interval = ((isset($_GET["Interval"]) ? $_GET["Interval"] : 0));
    if (!(is_numeric($Interval)))
      $Interval = 0;

    $HomeURL = ((isset($_GET["HomeURL"]) ? $_GET["HomeURL"] : ""));

    $HistoryURL = ((isset($_GET["HistoryURL"]) ? $_GET["HistoryURL"] : ""));

    $path = config("GameURL") . "?skinID=$skinId&PlayerID=$playerId&SessionID=$sessionId&GameID=$gameId&fun=$fun&GameLaunchSourceID=$source&DeviceId=$deviceId&MyAccountURL=$myAccountURL&HistoryURL=$HistoryURL&HomeURL=$HomeURL&Interval=1";
?>

<?php include '_global-library/partials/reality-check/games-reality-check.php'; ?>

<iframe id="ui-game-frame" src="<?php echo $path ?>"
        style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:0px; height:0px; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
    Your browser doesn't support iframes
</iframe>