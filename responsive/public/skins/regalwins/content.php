<?php

include 'paths.php';
require 'config/config.php';
require 'libs/application.php';
require 'libs/controller.php';	

$ctlr = trim(@$_GET["cid"],'/');
$content_key = '';
$content_stuff = '';
$content_title = '';


function getContent($controller)
{
	global $content_key,$content_title;
	
	if($controller=='') $controller = 'home';

	ob_start();				
	$app = new Application($controller);	
			
	$content_key = @$app->controller->cache_key;
	$content_title = @$app->controller->title;
	return json_encode(ob_get_clean());
}	

$content_stuff = getContent($ctlr);

?>	
{
	"status": "SUCCESS",
	"key": "<?php echo $content_key; ?>",
	"title": "<?php echo $content_title; ?>",
	"#ajax-wrapper": <?php echo $content_stuff;?>
}
