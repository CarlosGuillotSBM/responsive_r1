<?php

// -----------------------------------
// old luckypants affiliate re-directs
$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';

if(preg_match("#^\/slot-games\/scratch-cards\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/scratch-and-arcade/". $queryString);
    exit;
}

if(preg_match("#^\/claim\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/claim/". $queryString);
    exit;
}


//11443
if(preg_match("#^\/boss\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/boss/". $queryString);
    exit;
}

if(preg_match("#^\/spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporary');
    header("Location: /media/affiliates/free-spins/". $queryString);
    exit;
}


//11633
if(preg_match("#^\/play\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/affiliates/match-bonus/". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/5-reel\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/5-reel/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/50-and-over-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/50-and-over-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/40-line-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/40-line-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/3-reel\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/3-reel/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/30-line-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/30-line-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/25-line-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/25-line-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/20-line-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/20-line-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/15-line-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/15-line-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/10-and-less-line-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/10-and-less-line-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/free-spins-features\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/free-spins-features/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/featured-games\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/featured-games/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/exclusive-games\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/exclusive-games/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/egyptian\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/egyptian/ ". $queryString);
    exit;
}


if(preg_match("#^\/slot-games/click-me-features\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/click-me-features/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/brand-new-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/brand-new-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/bonus-round-features\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/bonus-round-features/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/animal-lovers\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/animal-lovers/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/all-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/all-slots/ ". $queryString);
    exit;
}


if(preg_match("#^\/slot-games/action-packed-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/action-packed-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/tumbling-reels-features\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/tumbling-reels-features/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/table-games\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/table-games/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/stacked-wilds-features\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/stacked-wilds-features/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/sports\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/sports/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/scratch-and-arcade\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/scratch-and-arcade/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/roulette-games\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/roulette-games/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/pirate-and-adventure\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/pirate-and-adventure/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/mystical-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/mystical-slots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/multiways-extra-features\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/multiways-extra-features/ ". $queryString);
    exit;
}


if(preg_match("#^\/slot-games/mega-jackpots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/mega-jackpots/ ". $queryString);
    exit;
}

if(preg_match("#^\/slot-games/hungry-for-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/hungry-for-slots/ ". $queryString);
    exit;
}


//R10-204
if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/regalwins/". $queryString);
}

//R10-559
if(preg_match("#^\/200k\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/200k-sleigh/' );
    exit;
}
if(preg_match("#^\/lapland\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/lapland/' );
    exit;
}

//R10-736
if(preg_match("#^\/fiver\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /media/exclusive/fiver/' );
    exit;
}
//R10-1009
if(preg_match("#^\/excluded\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/excluded/' );
    exit;
}