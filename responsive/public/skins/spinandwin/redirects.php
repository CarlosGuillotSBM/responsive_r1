<?php

$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';

// temp redirect for old promo pages
if(preg_match("/past-promotions-offline.*/", $path) > 0)
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/' . $queryString);
    exit;
}

// temp redirect for old mobile pages
if(preg_match("/^\/score\/$/", $path) > 0)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /register/score/' . $queryString );
    exit;
}

// bolton pages
if(preg_match("/\/media\/exclusive\/bolton.*\//", $path) > 0)
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /' . $queryString);
    exit;
}

if(preg_match("/\/register\/bolton.*\//", $path) > 0)
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /' . $queryString);
    exit;
}

//Landing page redirects
if(preg_match("/^\/claim\/$/", $path) > 0)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/exclusive/claim/' . $queryString );
    exit;
}

// DEV-10368

if (preg_match("#^\/free\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/free/" . $queryString);
    exit;
}

// DEV-10401

if (preg_match("#^\/play\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/play/" . $queryString);
    exit;
}

if (preg_match("#^\/spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/spins/" . $queryString);
    exit;
}

//11312
if (preg_match("#^\/valentine\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /promotions/valentine/" . $queryString);
    exit;
}

//11387
if (preg_match("#^\/v-raffle\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /promotions/v-raffle/" . $queryString);
    exit;
}

if(preg_match("#^\/media/affiliates/20-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/exclusive/20-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/ppc/20-free\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/affiliates/as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/exclusive/as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/ppc/as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
}

if(preg_match("#^\/media/ppc/casino-bonus-amp4\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
}  

if(preg_match("#^\/media/ppc/casino-bonus-champions-cup\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/ppc/casino-bonus-koi\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/ppc/double-slots-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
}

if(preg_match("#^\/media/exclusive/first-deposit\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
}  

if(preg_match("#^\/media/exclusive/jackpots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/media/ppc/netent-slots\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /" . $queryString);

    exit;
} 

if(preg_match("#^\/boss\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/boss/" . $queryString);

    exit;
}

//R10-69
if(preg_match("#^\/media/affiliates/casino-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /media/affiliates/first-deposit/' );
    exit;
}

if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/spinandwin/". $queryString);
    exit;
}

//R10-542
if(preg_match("#^\/register/25luck\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /register/' );
    exit;
}
//R10-545
if(preg_match("#^\/200k\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/200k-holiday/' );
    exit;
}
//R10-559
if(preg_match("#^\/lapland\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/lapland/' );
    exit;
}
//R10-1009
if(preg_match("#^\/excluded\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/excluded/' );
    exit;
}