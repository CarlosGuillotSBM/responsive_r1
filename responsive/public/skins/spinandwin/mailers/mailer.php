<?php

	// send a mail on password change requirement

	switch(@$_REQUEST["emailName"])
	{
		// this is old and not used
		case "WebForgotLogin":
			
			//send mail on forgot password success
			
			if(@$_REQUEST["ReturnData"] == 0 && @$_REQUEST["Part"] == 2)
				sendForgotPasswordMail();		
					
			break;
		
		
		case "WebRegister":

			// send a mail on registration success
			sendRegistrationMail();			
			break;			
			
		case "WebPlayerForgotPassword":				
			// send mail to player for forgot password	
			header('Location: /data.php?f=playerforgotpassword&e='.$_REQUEST["Email"]);		
			break;	

		case "COGSResetPassword":				
			header('Location: /data.php?f=playerforgotpassword&e='.$_REQUEST["Email"]);
			// send mail to player for forgot password	
			die();			
			break;	

			
		case "SecurityMail":
			
			// send a security mail if the player has been locked out due to 3 bad password attempts			
			sendSecurityMail();									
			break;				
		

		case "RefundProcessed":
			// sent from cat when a withdrawal is being processed
			sendWithdrawalProcessedMail();							
			break;
		
		case "cashback_credited":
			sendCashbackCreditedMail();
			break;
			

		default:    
			break;
		
	}	



/*
	//---------------------------------------------------------------------------------------
	// MAILERS	
	//---------------------------------------------------------------------------------------
	
	function sendRegistrationMail()
	{
	
	
		global $glo;
		
		if(!isset($_REQUEST["Email"])) return;
		if($_REQUEST["Email"] == "") return;
		
		$template = readFileContent("template.html");	
		if (!isset($_REQUEST["EmailType"]) || $_REQUEST["EmailType"]=='normal') {
			$content = readFileContent("register_content.html");		
		} else if ($_REQUEST["EmailType"]=='referee'){
			$content = readFileContent("register_referee_content.html");				
		}
		
		$content = str_replace("%%Alias%%",$_REQUEST["Username"],$content);				
		$content = str_replace("%%Email%%",$_REQUEST["Email"],$content);
		$content = str_replace("%%Firstname%%",$_REQUEST["Firstname"],$content);						
		$content = str_replace("%%Password%%",$_REQUEST["Password"],$content);
		$content = str_replace("%%RegEmailValidation%%",$_REQUEST["RegEmailValidation"],$content);
		$content = str_replace("%%PromoEmailValidation%%",$_REQUEST["PromoEmailValidation"],$content);
		$content = str_replace("%%ValidationGUID%%",$_REQUEST["ValidationGUID"],$content);
		$content = str_replace("%%PlayerID%%",$_REQUEST["PlayerID"],$content);
		
		$template = str_replace("%%SupportEmail%%",$glo["Skin"]["MailFrom"],$template);
		$template = str_replace("%%CONTENT%%",$content,$template);
		$template = str_replace("%%URL%%",$glo["URL"],$template);							
		$template = str_replace("%%SecureKey%%",md5($_REQUEST["PlayerID"].$glo["Skin"]["ID"].$_REQUEST["Email"]),$template);	
		$template = str_replace("%%SkinID%%",$glo["Skin"]["ID"],$template);			
		$template = str_replace("%%Email%%",$_REQUEST["Email"],$template);
		sendMail ($glo["Skin"]["MailFrom"], $_REQUEST["Email"] , "Welcome to ".$glo["Skin"]["Name"], $template, true); 
				
	}
	

	function sendForgotPasswordMail()
	{
		global $glo;
		
		$template = readFileContent("template.html");	
		$content = readFileContent("forgot_password_content.html");		
		
		$content = str_replace("%%Alias%%",@$_REQUEST["Username"],$content);				
		$content = str_replace("%%GUID%%",@$_REQUEST["GUID"],$content);
		$content = str_replace("%%PlayerID%%",@$_REQUEST["PlayerID"],$content);
		$content = str_replace("%%Firstname%%",@$_REQUEST["Firstname"],$content);	
		
		$content = str_replace("%%Lastname%%",@$_REQUEST["LastName"],$content);
		$content = str_replace("%%Name%%",$glo["Skin"]["Name"],$content);		
				
		$template = str_replace("%%CONTENT%%",$content,$template);
		
		$template = str_replace("%%SupportEmail%%",$glo["Skin"]["MailFrom"],$template);		
		$template = str_replace("%%URL%%",$glo["URL"],$template);	

		
		
		sendMail ($glo["Skin"]["MailFrom"], $_REQUEST["Email"] , $glo["Skin"]["Name"]." Password update",$template, true); 		
	}	

	
	function sendSecurityMail()
	{
		global $glo;
		$template = readFileContent("template.html");							
		$content = readFileContent("security_content.html");		
		
		$content = str_replace("%%Alias%%",@$_REQUEST["Username"],$content);				
		$content = str_replace("%%Email%%",@$_REQUEST["Email"],$content);
		$content = str_replace("%%Firstname%%",@$_REQUEST["Firstname"],$content);								
		$content = str_replace("%%ReactivationGUID%%",@$_REQUEST["ReactivationGUID"],$content);
		$content = str_replace("%%GUID%%",@$_REQUEST["ReactivationGUID"],$content);
		$content = str_replace("%%PlayerID%%",@$_REQUEST["PlayerID"],$content);
		
		$content = str_replace("%%Name%%",$glo["Skin"]["Name"],$content);
		
		
		$template = str_replace("%%CONTENT%%",$content,$template);
		
		$template = str_replace("%%SupportEmail%%",$glo["Skin"]["MailFrom"],$template);
		$template = str_replace("%%URL%%",$glo["URL"],$template);	
		
		
		sendMail ($glo["Skin"]["MailFrom"], $_REQUEST["Email"] , $glo["Skin"]["Name"]." Security Mail", $template, true); 
	}

	function sendWithdrawalProcessedMail()
	{
		global $glo;
		
			//PlayerID
			//Amount
			//Method
			//Ref
		$template = readFileContent("template.html");	
		$content = readFileContent("Withdrawal_processed_content.html");		
		
		$content = str_replace("%%Alias%%",@$_REQUEST["Username"],$content);				
		$content = str_replace("%%Email%%",@$_REQUEST["Email"],$content);
		$content = str_replace("%%Firstname%%",@$_REQUEST["Firstname"],$content);												
		$content = str_replace("%%PlayerID%%",@$_REQUEST["PlayerID"],$content);		
		$content = str_replace("%%Name%%",$glo["Skin"]["Name"],$content);		
		$content = str_replace("%%Method%%",$_REQUEST["Method"],$content);
		$content = str_replace("%%Amount%%",$_REQUEST["Amount"],$content);
		$content = str_replace("%%Referencenumber%%",$_REQUEST["Ref"],$content);

		$template = str_replace("%%CONTENT%%",$content,$template);
		
		$template = str_replace("%%SupportEmail%%",$glo["Skin"]["MailFrom"],$template);
		$template = str_replace("%%URL%%",$glo["URL"],$template);	

						
		sendMail ($glo["Skin"]["MailFrom"], $_REQUEST["Email"] , $glo["Skin"]["Name"]." Withdrawal Processed", $template, true); 

	}
	
	function sendReferAFriendMail()
	{
		global $glo;

		
		$template = readFileContent("refer-template.html");	
	
		
		$emails = explode(';',@$_REQUEST["ValidEmails"]);
		$emailsType = explode(';',@$_REQUEST["ValidEmailsTypes"]);
			
		$referLink = $glo["URL"].'/refer/'.@$_REQUEST["UserName"];
		$count=sizeof($emails);
		for ($i=0; $i<$count; $i++)
		{		
		
		
			if ($emails[$i]!=''){	
				if ($emailsType[$i]>1){
					$content = readFileContent("refer_reminder_content.html");	
					$subject = "Join ". @$_REQUEST["FirstName"]." at Kitty Bingo!";			
				} else {
					$content = readFileContent("refer_content.html");	
					$subject = @$_REQUEST["FirstName"]." invites you to join Kitty Bingo!";				
				}
				$realLink = $referLink;
				$thisMail = explode('@',$emails[$i]);

				if ($thisMail[1]=="gmail.com"){
					$realLink.='/1';
				}else if ($thisMail[1]=="yahoo.com") {				
					$realLink.='/2';
				}
					
				
				$content = str_replace("%%Firstname%%",@$_REQUEST["FirstName"],$content);
				$content = str_replace("%%Lastname%%",@$_REQUEST["LastName"],$content);
				$content = str_replace("%%ReferLink%%",$referLink,$content);
				$content = str_replace("%%RealLink%%",$realLink,$content);
				$content = str_replace("%%Alias%%",@$_REQUEST["UserName"],$content);
				$content = str_replace("%%Message%%",@$_REQUEST["Message"],$content);
				
				$template = str_replace("%%CONTENT%%",$content,$template);				
				$template = str_replace("%%SupportEmail%%",$glo["Skin"]["MailFrom"],$template);
				$template = str_replace("%%URL%%",$glo["URL"],$template);	

				
				sendMail ($glo["Skin"]["MailFrom"], $emails[$i], $subject , $template, true); 				
				
			}
		}
		
	}
	
	function sendCashbackCreditedMail()
	{
		global $glo;
		
		$content = readFileContent("cashback_credited.html");	
		$template = readFileContent("template.html");		

		$firstnames = array();
		$usernames= array();
		$currencies = array();
		$emails = array();
		$amounts = array();


		$firstnames = @$_REQUEST["Firstname"];
		$usernames= @$_REQUEST["Username"];
		$currencies = @$_REQUEST["Currency"];
		$emails = @$_REQUEST["Email"];
		$amounts  = @$_REQUEST["Amount"];

		$count=sizeof($firstnames);
		for ($i=0; $i<$count; $i++)
		{			
			if ($emails[$i] != ''){
				$content = str_replace("%%Alias%%",$usernames[$i],$content);				
				$content = str_replace("%%Firstname%%",$firstnames[$i],$content);		
				$content = str_replace("%%Currency%%",$currencies[$i] ,$content);	
				$content = str_replace("%%Amount%%",$amounts[$i] ,$content);
				$content = str_replace("%%Date%%",date("F j, Y"),$content);
				
				$template = str_replace("%%SupportEmail%%",$glo["Skin"]["MailFrom"],$template);
				$template = str_replace("%%CONTENT%%",$content,$template);
				$template = str_replace("%%URL%%",$glo["URL"],$template);			
				$template = str_replace("%%SecureKey%%",md5($_REQUEST["PlayerID"].$glo["Skin"]["ID"].$_REQUEST["Email"]),$template);	
				$template = str_replace("%%SkinID%%",$glo["Skin"]["ID"],$template);			
				$template = str_replace("%%Email%%",$_REQUEST["Email"],$template);
					
				sendMail ($glo["Skin"]["MailFrom"], $emails[$i] , $glo["Skin"]["Name"]." Cashback Credited", $template, true); 		
			}				
		}	
	}
	*/
?>