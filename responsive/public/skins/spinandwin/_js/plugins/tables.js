var sbm = sbm || {};

sbm.Tables = function () {
    "use strict";

    return {
        run : function () {

            var updateTables = function($tables) {
                $tables.addClass("responsive");

                var $table,
                    $ths,
                    title;

                // for each table
                $.each($tables, function (i, t) {

                    $table = $(t);
                    $ths = $table.find("tr:eq(0)").find("td");

                    // for each row
                    $.each($table.find("tr:gt(0)"), function (j, r) {

                        // each td on the row
                        $.each($(r).find("td"), function (i, td) {
                           $(td).attr("data-th", $ths.eq(i).text());
                        });

                    });

                    $table.find("tr:eq(0)").addClass("table-hide-small");

                });
            };

            var $tables = $("table:not(.nonresponsive)");
            updateTables($tables);

        }
    };

};