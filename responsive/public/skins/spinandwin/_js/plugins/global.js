var sbm = sbm || {};

sbm.Global = function() {
    "use strict";

    return {

        /**
         * Navigation bar highlighting
         * It adds class .current to the proper navigation link
         */
        highlightNav: function() {

            var page = window.location.pathname;

            var _current =  location.pathname.split('/')[1]
            var itemID;
            var item;

            if (_current == "" || _current == "start") {
                _current = "home";
            };

            if (_current == "register") {
                _current = "join-now";
            };

            // Changing the id of the body when we get the home through ajax navigation.
            $('body').attr('id', _current);

            $(".secondary-nav ul li").each(function(index) {
                itemID = $(this).attr('id');
                if (itemID != _current) {
                    $(this).find("a").removeClass("current");
                } else {
                    $(this).find("a").addClass("current");
                }
            });
            $(".nav-for-desktop ul li").each(function(index) {
                itemID = $(this).attr('id');
                if (itemID != _current) {
                    $(this).find("a").removeClass("current");
                } else {
                    $(this).find("a").addClass("current");
                }
            });
            $(".user-nav li").each(function(index) {
                itemID = $(this).attr('id');
                if (itemID != _current) {
                    $(this).find("a").removeClass("current");
                } else {
                    $(this).find("a").addClass("current");
                }
            });
        },

        /**
         * Setup for the 3rd party Share plugin
         */
        shareButton: function() {

            var config = {
                ui: {
                    flyout: 'middle left'
                },
                networks: {
                    facebook: {
                        url: this.fullurl
                    },
                    twitter: {
                        url: this.fullurl
                    },
                    google_plus: {
                        url: this.fullurl
                    },

                    email: {
                        enabled: false
                    },
                    pinterest: {
                        enabled: false
                    }
                }
            }

            var share = new Share('.share-button', config);
            var page = window.location.pathname;
            //We show it if it is not the cashier.
            if (page == '/cashier/' || page == '/my-account/' || page == "/register/" || page == "/welcome/") {
                $('.share-button').attr("style", "display: none!important");
            } else {
                $('.share-button').attr("style", "display: inline!important");
            }
        },

        /**
         * This will make the navigation bar to stick on the top of the window when it reaches the top of the page
         * Scrolling down will make it go to its initial place
         */
        sticky: function() {

            /**
             * on document ready - the nav bar is not on the right place before that
             */
            $(document).ready(function() {
                var sliderVisible = $("#slick-container").is(":visible");
                var $gamenav = $("#ui-games-navigation");
                var $stikyGamenav = $("#ui-sticky-games-navigation");
                var $pagemargin = $("body");
                var pos, windowpos;

                /**
                 * Stick the nav bar to the top
                 */
                var stick = function() {
                    $gamenav.hide();
                    $stikyGamenav.show();
                };

                /**
                 * Nav bar back to the initial position
                 */
                var doNotStick = function() {
                    $stikyGamenav.hide();
                    $gamenav.show();
                };

                /**
                 * Adjust nav bar
                 * Will check the current position and make it stick or not
                 */
                var adjustNavBar = function() {

                    var pos = sliderVisible ? 360 : 90;

                    windowpos = $(window).scrollTop();

                    if (windowpos >= pos) {
                        stick();
                    } else {
                        doNotStick();
                    }
                };

                /**
                 * on scroll handling
                 */
                $(window).scroll(function() {
                    adjustNavBar();
                });

                /**
                 * starting point
                 */
                // if home page and logged in
                if (window.location.pathname === "/" && window.location.pathname === "/start/" && $.cookie("SessionID")) {
                    $(".breadcrumbs__row ").css("margin-top", "80px");
                } else {
                    $(".breadcrumbs__row ").css("margin-top", "0px");
                }

                adjustNavBar();
            });

        },

        rippleEffect : function () {

            //jQuery for click effect on search results screen to show growing circle background
            var parent, ink, d, x, y;
            $(".ripple li a").click(function(e){
                parent = $(this).parent();
                //create .ink element if it doesn't exist
                if(parent.find(".ink").length == 0)
                    parent.prepend("<span class='ink'></span>");

                ink = parent.find(".ink");
                //incase of quick double clicks stop the previous animation
                ink.removeClass("animate");

                //set size of .ink
                if(!ink.height() && !ink.width())
                {
                    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                    d = Math.max(parent.outerWidth(), parent.outerHeight());
                    ink.css({height: d, width: d});
                }

                //get click coordinates
                //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
                x = e.pageX - parent.offset().left - ink.width()/2;
                y = e.pageY - parent.offset().top - ink.height()/2;

                //set the position and add class .animate
                ink.css({top: y+'px', left: x+'px'}).addClass("animate");
            })
        },

        /**
         * This is the initial function that is called from our framework
         * It is called on regular page load or any page swap if the plugin was included for the current page
         */
        run: function() {

            // remove initial loader
            $("#ui-first-loader").remove();

            // highlight navigation bar
            this.highlightNav();

            $(".ui-accordion").on("click", function () {
                $(this).next().toggle();
                $(this).toggleClass("active");
            });

            // change name of Home link and href attr
            if ($.cookie("SessionID")) {
                $(".ui-go-home").attr("href", "/start/").text("START");
                $(".ui-go-home-logo").attr("href", "/start/");
            }

            // sticky bar
            this.sticky();

            // share button
            this.shareButton();

            // ripple effect
            this.rippleEffect();
        }
    };


$('.accordion-navigation a').click(function(e) {
    e.preventDefault();
});
};