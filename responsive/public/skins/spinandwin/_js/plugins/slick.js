var sbm = sbm || {};

sbm.Slick = function () {
    "use strict";
    return {

        backgrounds: [
            "/_images/slider/background/desktop/slide1.jpg"
        
        ],

        $slickContainer: $("#slick-container"),

        $topSlider: $(".top-slider"),

        $slickSlider: $(".slick-slider"),

        run : function () {

            var page = window.location.pathname;

            // logged in or not homepage - hide slider
            if ($.cookie("SessionID") || page !== "/") {

                this.hideSlider();

            } else {
                this.showSlider();
            }
        },

        hideSlider: function () {
            this.$slickContainer.hide();
        },

        showSlider: function () {

            var self = this;
            // set initial background
            self.setBackground(0);
            var tmpl = $("#slick-content").html();
            this.$slickContainer.html(tmpl);
            this.$slickContainer.show();
            $(".slick-slider").slick({
                autoplay: false,
                autoplaySpeed: 9000,
                fade: true,
                cssEase: 'linear',
                infinite: true,
                lazyLoad: "ondemand",
                mobileFirst: true,
                slidesToShow: 1,
                onBeforeChange: function (slick, currentSlide, nextSlide) {
                    var index = nextSlide === 0 ? 0 : -1;
                    
                    /* logic for selecting the background index
                    if (nextSlide === 1) {
                        index = 1
                    } 

                    if (nextSlide === 6) {
                        index = 0
                    }
                    */

                    self.setBackground(index);
                }
            });

            this.handleClicksOnBanners();

        },
        handleClicksOnBanners : function () {

            $(".slick-slider").find("a").on("click", function (e) {

                var $target = $(e.currentTarget);
                var url = $target.attr("href");

                if (!url) {
                    return;
                }

                var pos = $(".slick-slider").slickCurrentSlide();
                var images = $target.find("img");
                var img = images.length ? $(images).attr("src") : "";
                var playerId = $.cookie("PlayerID") || "";

                var label = playerId ? "player=" + playerId + ";url=" + url + ";img=" + img : "url=" + url + ";img=" + img + ";pos=" + pos;

                this.sendDataToGoogle("carousel", label);

            }.bind(this));
        },
        sendDataToGoogle: function (category, label) {

            if (window.dataLayer) {

                window.dataLayer.push({
                    "event": "GAevent",
                    "eventCategory": category,
                    "eventAction": window.location.pathname,
                    "eventLabel": label
                });
            }
        },
        setBackground: function (index) {
            if (index === -1) $("body").css("background-image", "");
            else $("body").css("background-image", "url(" + this.backgrounds[index] + ")");
        }
    };
};



