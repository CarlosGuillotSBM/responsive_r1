<div class="breadcrumbs__row">
	<ul class="breadcrumbs" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<?php foreach($this->breadcrumbs as $key => $value){ ?>
		<li>
			<a href="<?php echo $value;?>" data-hijack="true" itemprop="url">
				<span itemprop="title"><?php echo $key;?></span>
			</a>
		</li>
		<?php }?>
	</ul>
	<!-- Share button only for desktop -->
	<?php echo '<!-- device'.config("Device").'-->'; if (config("Device") == 1) { ?>
	<div class="share-button right"></div>
		<!-- social share links -->
	<div class="social-icons-nav right">

	</div>
	<p class="social-share-links--copy">Find us on:</p>
		<ul class="social-share-links right">
			<li><a href="https://www.facebook.com/SpinAndWin"><img src="/_images/common/social-icons/facebook.png"></a></li>
			<li><a href="https://twitter.com/spinandwin"><img src="/_images/common/social-icons/twitter.png"></a></li>
		</ul>
	<!-- end social share links -->
	<?php } ?>
	<!-- /Share button only for desktop -->
</div>

<!-- end BREADCRUMPS -->