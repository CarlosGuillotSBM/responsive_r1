<div data-bind="visible: validSession" style="display: none" class="balance-area">
    <!-- <div class="balance-area"> -->
    <!-- <div class="balance-area__left"> -->
    <!-- <a href="/my-account/" class="balance-area__user-name" data-bind="text: username" data-hijack="true"></a>
    <a class="cashier-button" href="/cashier/" data-hijack="true">CASHIER</a> -->
    
    <!-- </div> -->
    <div  class="balance-area__right">
        <div class="balance-area__title">BALANCES</div>
        <ul>
            
            <!--     <li class="balance-area__title__user-name">
                <i><img  src="/_images/common/icons/user-24x24.png"></i><a href="/my-account/" data-hijack="true"><span data-bind="text: username"></span></a>
            </li> -->

            <?php if(config("inbox-msg-on")): ?>
                <li class="balance-area__right__message-inbox">
                    <a data-hijack="true" href="/start/" title="Message Inbox">
                        <span class="balances-area__message-inbox__icon"></span>
                        <span class="balances-area__message-inbox__value" data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
                    </a>
                </li>
            <?php endif; ?>

            <li title="Your Account Balance" class="balance-area__right_balance">  
                <a href="/my-account/" data-hijack="true"><i><img src="/_images/balance-area/icon-account-balance-black.png"></i>
                    <span class="balance-area__value" data-bind="text: balance"></span>
                </a>
            </li>
            <li title="Your Account Balance" class="balance-area__right_points">
                <i><a title="Click to learn about loyalty points" href="/promotions/loyalty-points/"><img src="/_images/balance-area/icon-points-coin-black.png"></a></i>
                <a title="Your Points" href="/my-account/" data-hijack="true"><span class="balance-area__value" data-bind="text: points"></span></a></li>
                <li id="cashier" class="no-background"><a href="/cashier/" data-hijack="true">DEPOSIT</a></li>
                <!--     <li>
                    <i><img src="/_images/balance-area/icon-free-spins.png"></i>
                    <a href="/slots/exclusive/" data-hijack="true"><span class="balance-area__value" data-bind="text: bonusSpins"></span></a>
                </li> -->
                
            </ul>
        </div>
        
    </div>