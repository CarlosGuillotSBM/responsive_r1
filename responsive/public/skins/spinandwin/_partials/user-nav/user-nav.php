	
<ul style="display:none" data-bind="visible: validSession()" class="user-nav">
	<li id="logout-button">
        <a data-bind="click: logout" href="" class="logout-button--top-nav" data-gtag="Logout Desktop">LOGOUT</a>
    </li>
	<li id="my-account" class="user-nav__user-name"><a href="/my-account/" data-hijack="true">MY ACCOUNT</a></li>
	
    
</ul>