<!-- messages modal window -->
<div id="single-message" class="reveal-modal recent-messages__modal" data-reveal aria-labelledby="My Messages" aria-hidden="true" role="dialog">
	<div class="lobby-wrap__content-box">
		<!-- header bar -->
<!-- 		<div class="lobby-wrap__content-box__header">
			<div class="title hasicon" >
				<div class="title__icon">
				
					<?php// include 'icon-messages.php'; ?>
				
				</div >
				<div class="title__text messages-section" >MY MESSAGES</div>
				<a class="recent-messages__modal__delete" href=""><span></span>Delete Message</a>
			</div>
		</div> -->
		<!-- message content goes here -->
		<div class="single-message-content">
			<h1 id="modalTitle">Message Title goes Here</h1>
			<p>Unum deserunt necessitatibus usu id, an his debet minimum, ne dolore abhorreant eum. Mei dicat conceptam rationibus no, eos nominati referrentur et. Mel ut elit putant constituam, verear iuvaret civibus ne mei, has at diam facete. Est essent offendit consulatu at. Id has altera verear quaeque. Usu ut vero choro.
				Noster labores lobortis no mea, ei nec mucius facete, dicta omnium senserit sit ut. His ea simul virtute. Cu repudiare theophrastus mediocritatem qui. Agam wisi modus ea pri. Quo an delectus volutpat qualisque, sit nostrud inciderint id, nec in etiam placerat. Eos cu tota veritus, te summo repudiare eum, his cu postea copiosae forensibus.
				Omnis exerci suavitate ex usu, duo natum senserit ea, nibh quando imperdiet eos no. No ius omnium convenire maluisset, solum primis inimicus sea ad. Eu patrioque hendrerit ius. Vim malorum recusabo consectetuer ei, est sint esse eu. Qui congue mediocrem quaerendum et. Decore sapientem assueverit per eu, populo scriptorem duo ea, in minim doming consequat vis.
				Sea eu probo legimus. Sumo inciderint sed at. Insolens conclusionemque ea vel, et nam laoreet persequeris. Harum saepe malorum ut vim, eam vero democritum cu.
				Tation quaestio usu in. Nam congue platonem expetenda cu. Eos malis veniam epicurei id, id quo verterem perpetua posidonium. Vide platonem urbanitas at nec. At falli dolor reprimique sit, cum id vocibus mandamus, eius brute fabulas vim id. An vix inermis reprimique appellantur, ullum animal detracto no vis.
				Quem vide principes in nam. Sint mandamus at eum. In ocurreret urbanitas vim, no movet saperet constituam has. Eos ea labore concludaturque, has qualisque imperdiet ea, ex delectus accusata pericula vix.
				At nec sint consul. Dico consetetur ea usu, harum tractatos nam ei, ei sumo deleniti persecuti eum. Pro ut nobis possit definitiones, nec quas mundi euripidis ei. Id cum animal facilisis disputando. Vim sale maiestatis no, viris omittam invenire ex his, sea nihil dicunt an.
				Ex stet facilisis pro. Per denique corrumpit voluptatum in. Harum sadipscing consectetuer pro ne. Enim veri cu per. Cu facer velit quo, ponderum expetendis disputando ut sit.
				Has id integre adolescens theophrastus. Fugit scaevola vel ut, et est graece atomorum liberavisse. Choro electram no per. Nam tale natum explicari ut, ne dolor possim admodum est.
				Dolorem mediocrem pri an, sit ne aperiri tacimates reprehendunt. Regione adipisci reprimique at vix, mei delicata necessitatibus cu, mei id altera percipit senserit. Aeque sonet ius an. Quas lucilius at per, putant nominavi in has. Dolor dolore insolens quo ei, ex ceteros lobortis ius. Ex vix veritus imperdiet, his case sapientem vituperatoribus cu.
			</p>
			<!-- /message content goes here -->
		</div>
	</div>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- end messages modal window -->