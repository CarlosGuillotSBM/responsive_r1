<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg width="24px" height="23px" viewBox="0 0 24 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
    <!-- Generator: Sketch 3.3.2 (12043) - http://www.bohemiancoding.com/sketch -->
    <title>email_F Copy + Shape</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
        <g id="Desktop" sketch:type="MSArtboardGroup" transform="translate(-55.000000, -680.000000)">
            <g id="email_F-Copy-+-Shape" sketch:type="MSLayerGroup" transform="translate(55.000000, 680.000000)">
                <g sketch:type="MSShapeGroup">
                    <circle id="email_F-Copy" fill="#179a94" cx="12.1459049" cy="11.5" r="11.5"></circle>
                    <path d="M5,17 L7.54545455,17 L7.54545455,9.2 L5,9.2 L5,17 L5,17 Z M19,9.85 C19,9.135 18.4272727,8.55 17.7272727,8.55 L13.7118182,8.55 L14.3163636,5.5795 L14.3354545,5.3715 C14.3354545,5.105 14.2272727,4.858 14.0554545,4.6825 L13.3809091,4 L9.19363636,8.2835 C8.95818182,8.5175 8.81818182,8.8425 8.81818182,9.2 L8.81818182,15.7 C8.81818182,16.415 9.39090909,17 10.0909091,17 L15.8181818,17 C16.3463636,17 16.7981818,16.675 16.9890909,16.207 L18.9109091,11.6245 C18.9681818,11.475 19,11.319 19,11.15 L19,9.9085 L18.9936364,9.902 L19,9.85 L19,9.85 Z" id="Shape" fill="#FFFFFF"></path>
                </g>
            </g>
        </g>
    </g>
</svg>