<!--  My Promotions -->
<div class="lobby-wrap__content-box">
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-promotions.php'; ?>
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text">MY PROMOTIONS</div>
            <div class="title__link">
                <a href="/promotions/">View All <span class="title__link__arrow">&rtrif; </span></a>
            </div>
        </div>
    </div>
    <!-- end header bar -->
    <div class="lobby-wrap__content-box__content my-promotions">
        <?php edit($this->controller,'my-promotions'); ?>
        
        
        <div class="my-promotions__promotion">
            <?php @$this->getPartial($this->content['my-promotions'],1); ?>
            
        </div>
        <div class="my-promotions__promotion">
            
            <?php @$this->getPartial($this->content['my-promotions'],2); ?>
        </div>
        <div class="my-promotions__promotion">
            
            <?php @$this->getPartial($this->content['my-promotions'],3); ?>
        </div>
    </div>
</div>
<!-- End My Promotions -->