<div class="game-nav-items nav-for-desktop">
	<ul>

		<li id="slots"><a href="/slots/" data-hijack="true">SLOTS</a></li>
		<li >
			<img src="/_images/common/game-navigation/game-navigation-divider.jpg">
		</li>
		<li id="table-card"><a href="/table-card/" data-hijack="true">TABLE &amp; CARD</a></li>
		<li ><img src="/_images/common/game-navigation/game-navigation-divider.jpg"></li>
		<li id="live-casino" class="custom-hide-for-mobile"><a href="/live-casino/" data-hijack="true">LIVE CASINO</a></li>
		<li ><img src="/_images/common/game-navigation/game-navigation-divider.jpg"></li>
		<li id="roulette"><a href="/roulette/" data-hijack="true">ROULETTE</a></li>
		<li ><img src="/_images/common/game-navigation/game-navigation-divider.jpg"></li>
		<li id="scratch-and-arcade"><a href="/scratch-and-arcade/" data-hijack="true">SCRATCH &AMP; ARCADE</a></li>
		<li ><img src="/_images/common/game-navigation/game-navigation-divider.jpg"></li>
		<!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
		<li id="sports" class="u-hidden"><a href="/sports/" data-hijack="true">SPORTS</a></li>
		<li class="u-hidden"><img src="/_images/common/game-navigation/game-navigation-divider.jpg"></li>
		<!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
		<li id="games"><a href="/games/" data-hijack="true">ALL</a></li>
		<li ><img src="/_images/common/game-navigation/game-navigation-divider.jpg"></li>
		<li  data-bind="visible: validSession"><a data-bind="visible: validSession" href="/my-favourites/" data-hijack="true">MY FAVOURITES</a></li>
		<li  data-bind="visible: validSession" ><img src="/_images/common/game-navigation/game-navigation-divider.jpg"></li>

	</ul>
</div>