<nav class="secondary-nav secondary-nav--<?php echo $this->controller; ?>">
	<ul>
		<li id="home">
			<a class="ui-go-home-logo" href="/"><img src="/_images/common/icons/home-icon.png"></a>
		</li>
		<li id="promotions">
			<a href="/promotions/">PROMOS</a>
		</li>
		<li class="radical-dice" data-bind="visible: displayThreeRadical(), click: ThreeRadical.requestToken" style="display: none" class>
			<svg>
				<g id="sparkle1">
					<path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
					<path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
					<path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
				</g>
			</svg>
			<a href="" data-hijack="true">Daily play <img class="bounce-dice" src="/_images/common/icons/dice.svg" alt="Extras"/></a>
		</li>
		<li id="community">
			<a href="/community/">COMMUNITY</a>
		</li>		
		<li id="loyalty">
			<a href="/loyalty/">VIP & LOYALTY</a>
		</li>
		<li id="support">
			<a href="/support/" class="drop-target tooltip-info"><span class="tooltip-content">Live Chat</span><img class="support-icon" src="/_images/common/icons/support.svg" alt="Extras"/></a>
		</li>
	</ul>
</nav>
