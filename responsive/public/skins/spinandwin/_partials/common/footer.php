<!-- BOTTOM CONTENT -->
<div class="bottom-content">
</div>
<!-- END Games info modal -->
</div>
<!-- END AJAX WRAPPER - It is needed to add content inside dynamically -->
</div>
<!-- END CONTENT WRAPPER -->


<!-- FOOTER AREA -->
<div class="clearfix"></div>
<footer id="footer" class="footer">
     <div class="footer__row">
          <div class="footer__row__left">
               <ul class="footer__nav">
                    <li><a href="/scratch-and-arcade/">Scratch &amp; Arcade </a></li>
                    <li>|</li>
                    <li><a href="/about-us/">About </a></li>
                    <li>|</li>
                    <li><a href="/support/">Support </a></li>
                    <li>|</li>
                    <li><a href="/faq/">FAQ </a></li>
                    <li>|</li>
                    <li><a href="/banking/">Banking </a></li>
                    <li>|</li>
                    <li><a href="/terms-and-conditions/">Terms &amp; Conditions </a></li>
                    <li>|</li>
                    <li><a href="/privacy-policy/">Privacy Policy </a></li>
                    <li data-bind="visible: validSession" style="display: none">|</li>
                    <li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none">
                         <a  href="/cashier/?settings=1">Player Settings </a>
                    </li>
                    <li>|</li>
                    <li><a href="/responsible-gaming/">Responsible Gaming </a></li>
                    <li>|</li>
                    <li><a href="/complaints-and-disputes/">Complaints and Disputes </a></li>
                    <li>|</li>
                    <li><a  href="https://www.luckyjar.com" target="_blank" data-hijack="false">Affiliates </a></li>
                    <li>|</li>
                    <li><a  href="/archive/">Archive </a></li>
               </ul>
          </div>

     </div>
     <div class="footer__row footer__row-bg">
          <ul class="footer__row__payments">
               <li><img src="/_images/common/payment-icons/paypal.svg"></li>
               <li><img src="/_images/common/payment-icons/maestro-icon.svg"></li>
               <li><img src="/_images/common/payment-icons/mastercard-icon.svg"></li>
               <li><img src="/_images/common/payment-icons/visa.svg"></li>
               <li><img src="/_images/common/payment-icons/neteller.svg"></li>
               <li><img src="/_images/common/payment-icons/paysafecard.svg"></li>
               <li><a href="/terms-and-conditions/"><img src="/_images/common/payment-icons/18-plus.svg"></a></li>
               <li>
                    <a target="_blank" data-hijack="false" href="http://www.gamcare.org.uk/">
                         <img src="/_images/common/payment-icons/gamcare.svg">
                    </a>
               </li>
               <li>
                    <a target="_blank" data-hijack="false" href="https://www.gamstop.co.uk"  rel="nofollow">
                         <img src="/_images/common/payment-icons/gamstop.svg">
                        
                    </a>
               </li>
               <li>
                    <a target="_blank" data-hijack="false" href="http://www.gamblingcontrol.org/">
                         <img src="/_images/common/payment-icons/alderney.svg">
                    </a>
               </li>
               <li>
                    <a target="_blank" data-hijack="false"  rel="nofollow" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022">
                         <img src="/_images/common/payment-icons/gambling-commission.svg"> 
                    </a>
               </li>
               <li>
                    <a target="_blank" data-hijack="false" href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.show&lng=EN">
                         <img src="/_images/common/payment-icons/ecogra.svg">
                    </a>
               </li>
          </ul>
     </div>
     <div class="footer__row">
          <div class="footer__row__legal">
               <p>Spin and Win is licensed and regulated to offer Gambling Services in Great Britain by the UK Gambling Commission, license Number 000-039022-R-319427-004. All the games offered on the website have been approved by the UK Gambling Commission. Details of its current licensed status as recorded on the Gambling Commission's website can be found <a  href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022" target="_blank" data-hijack="false">here.</a></p>

               <p>Spin and Win is also licensed and regulated by the Alderney Gambling Control Commission, License Number: 71 C1, to offer Gambling facilities in jurisdictions outside Great Britain. Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3 UL.</p>

          </div>
     </div>
</footer>
<!-- END FOOTER AREA -->
<!-- close the off-canvas menu -->
<a class="exit-off-canvas" data-gtag="Closed Left Menu"></a>
</div>
<!-- END INNER WRAPPER. It is necesary for foundation off canvas nav -->

</div>
<!-- OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->
<!-- login modal -->
<div id="login-modal" class="reveal-modal" data-reveal>
     <?php include "_partials/login/login-box.php" ?>
     <span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- Claimed prize modal -->
<div id="claimed-prize-modal" class="reveal-modal" data-reveal>
<?php include "_global-library/partials/promotion/promotion-claimed-prize.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- Games info modal -->
<div id="games-info" class="games-info reveal-modal" data-reveal>
     <?php include "_global-library/partials/games/games-details-modal.php" ?>
     <span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Games info modal -->

<!-- Custom modal -->
<div id="custom-modal" class="games-info reveal-modal" data-reveal>
     <?php include "_global-library/partials/bingo/login-box-modal.php" ?>
     <span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Custom modal -->

<?php
include "_global-library/partials/modals/alert.php";
include "_global-library/partials/modals/notification.php";
include "_partials/common/loading-modal.php";
include "_global-library/partials/games/games-iframe.php";
include "_global-library/partials/games/games-last-played.php";
include "_global-library/partials/common/app-js.php";
include "_global-library/partials/common/recaptcha.php";
?>

<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">

</body>
</html>
