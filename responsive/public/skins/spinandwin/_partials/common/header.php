<script type="application/javascript">

window.setTimeout(function () {
	var el = document.getElementById("ui-first-loader");
	if (el) el.parentNode.removeChild(el);
	el = document.getElementById("ui-loader-animation");
	if (el) el.style.display = "none";
},1000);

</script>


<!-- OFF CANVAS -->
<!-- This is the Foundation side nav, everything should be wrapped inside off-canvas-wrap -->
<!-- OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->
<div class="main-wrap off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">
		<!-- Inner-wrap. It is necesary for foundation off canvas nav -->

		<!-- header  -->
		<header data-bind="css: { 'header--logged-in' :validSession }" class="header header--logged-<?php echo logged(); ?>">
			<!-- COOKIE POLICY -->
			<?php include "_global-library/partials/modals/cookie-policy.php" ?>

			<div class="inner-header">
				<!-- header__top__left -->
				<div class="header__left">
					<h1 class="seo-logo">
						<a class="ui-go-home-logo" href="/">
							<img src="/_images/logo/logo-desktop.png"/>
						</a>
						<span style="display:none;">Spin and Win online casino on mobile and desktop.</span>
					</h1>
				</div>
				<!-- /header__top__left -->
				<!-- header__top__middle -->
				<div class="header__middle">
					<?php include "_partials/secondary-nav/secondary-nav.php" ?>
				</div>
				<!-- /header__top__middle -->
				<!-- header__top__right -->
				<div class="header__right">
					<!-- login nav -->
					<?php include "_partials/login-nav/login-nav.php"; ?>
					<!-- /login nav -->
					<!-- user nav -->
					<?php include "_partials/user-nav/user-nav.php"; ?>
					<?php include "_partials/balance-area/balance-area.php"; ?>
					<!-- /user nav -->
				</div>
				<!-- /header__top__right -->

			</div>


		</header>
		<!-- /header  -->
		<!-- header for tablet and mobile -->
		<!-- no semantinc tags to avoid document outline problems -->
		<!-- <div> -->
			<!-- nav for mobile -->
		<?php include "_partials/nav-for-mobile/nav-for-mobile.php"; ?>
			<!-- / nav for mobile -->
		<!-- </div> -->
		<div class="search-screen-wrap">
			<!-- search screen for mobile -->
			<?php include "_partials/search-screen/search-screen.php"; ?>
			<!-- / search screen for mobile -->
		</div>
		<!-- sticky navigation  -->
		<div style="display: none" id="ui-sticky-games-navigation" class="sticky games-navigation middle-content__box">
			<div class="games-navigation__inner">
				<div class="games-navigation__left">


					<!-- nav for desktop -->
					<?php include "_partials/games-navigation/games-navigation.php"; ?>
					<!-- / nav for desktop -->
					<!-- game filter menu -->

					<?php include "_global-library/partials/game-filter-menu/game-filter-menu.php" ?>

					<!-- / game filter menu -->
				</div>
				<div class="games-navigation_right">
					<?php include "_global-library/partials/search/search-input.php" ?>
				</div>
			</div>
		</div>
		<!-- /sticky navigation  -->
		<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" id="fullpage" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>">
			<!-- slider -->
			<?php include "_partials/common/slick-slider.php"; ?>
			<!-- slider -->
			<!-- games-navigation -->
			<div id="ui-games-navigation" class="games-navigation middle-content__box">
				<div class="games-navigation__inner">
					<div class="games-navigation__left">


						<!-- nav for desktop -->
						<?php include "_partials/games-navigation/games-navigation.php"; ?>
						<!-- / nav for desktop -->
						<!-- game filter menu -->
						<div class="cat-search-bar__categories ui-game-filter-btn games-filter-menu"><?php include "_global-library/partials/game-filter-menu/game-filter-menu.php";?></div>


						<!-- / game filter menu -->
					</div>
					<div class="games-navigation_right">

						<?php include "_global-library/partials/search/search-input.php" ?>
					</div>
				</div>
			</div>

			<!-- /games-navigation -->
			<?php
        		include("_global-library/widgets/prize-wheel/prize-wheel-notification.php");
    		?>
			<!-- AJAX CONTENT WRAPPER -->
			<div id="ajax-wrapper">
