<!doctype html>
<html class="no-js" lang="en">
<head>
     <link rel="icon" type="image/ico" href="/_images/common/favicon.ico" />
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

     <?php include "_global-library/partials/common/open-graph.php"; ?>

     <?php echo $this->seo_meta;?>

     <link rel="stylesheet" href="/_global-library/js/vendor/intlTelInput/css/intlTelInput.css<?php echo "?v=" . config("Version"); ?>" />
     <link rel="stylesheet" href="/_css/main.css<?php echo "?v=" . config("Version"); ?>" />


     <link href="https://plus.google.com/+Spinandwin" rel="publisher" />
     <?php include '_trackers/header.php';?>

     <script src="/_global-library/js/bower_components/modernizr/modernizr.min.js"></script>

     <script type="application/javascript">
     // sbm initialization
     var skinModules = [], skinPlugins = [];
     // common modules
     skinModules.push({
          id: "Notifier",
          options: {
               global: true
          }
     });
     skinModules.push({
          id: "GameLauncher",
          options: {
               modalSelector: "#games-info",
               global: true
          }
     });
     skinModules.push({
          id: "GameWindow",
          options: {
               global: true
          }
     });
     skinModules.push({
          id: "Favourites",
          options: {
               imgFavouriteOn: "/_images/common/icons/favourite-icon--on.png",
               imgFavouriteOff: "/_images/common/icons/favourite-icon--off.png",
               noFavIconsWeb: 6,
               noFavIconsTablet: 3,
               noFavIconsPhone:2,
               global: true
          }
     });
     skinModules.push( {
          id: "MailInbox",
          options: {
               global: true
          }
     });
     skinModules.push( {
          id: "TrackNavigation",
          options: {
               global: true
          }
     });
     skinModules.push({
          id: "ContentScheduler",
          options: {
               global: true
          }
     });
     // common plugins
     skinPlugins.push({
          id: "Global",
          options: {
               global: true
          }
     });
     skinPlugins.push({
          id: "MobileMenu",
          options: {
               global: true
          }
     });
     skinPlugins.push({
          id: "Tables",
          options: {
               global: true
          }
     });
     skinPlugins.push({
          id: "Slick",
          options: {
               global: true,
               element: 'bxslider-head'
          }
     });
      //Prize Wheel
     skinModules.push({
          id: "PrizeWheel",
          options: {
               global: true
          }
     });
     // Three Radical
     skinModules.push({
          id: "ThreeRadical",
          options: {
               global: true
          }
     });
		skinModules.push({
			id: "CMANotifications",
			options: {
				global: true
			}
		});
     </script>
     <!-- Browser not supported redirect -->
     <?php include "_global-library/partials/common/user-agent-sniffer.php"; ?>
     <!-- SERVER SIDE AND DYNAMIC STYLES -->
     <style type="text/css">
     <?php
     echo "/*device: " . config("Device") . 'logged: ' . logged() . "*/\n";
     ?>
     body {
          background-image: url('/_images/slider/background/desktop/slide1.jpg');
     }

     </style>
     <!-- /SERVER SIDE AND DYNAMIC STYLES -->

       <!-- SEO Social -->
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Spin And Win",
  "url" : "https://www.spinandwin.com/",
  "sameAs" : [ "https://www.facebook.com/SpinAndWin/",
    "https://twitter.com/spinandwin",
    "https://plus.google.com/+Spinandwin",
    "https://www.youtube.com/user/SpinAndWin/"] 
} 
</script>
<!-- / SEO Social -->


     <?php if(Env::isLive()):?>
          <!-- new relic -->
          <?php include "_global-library/trackers/new-relic/spin-and-win.php"; ?>
          <!-- /relic -->
          <!-- Start Spin and Win Visual Website Optimizer Asynchronous Code -->
          <script type='text/javascript'>
          var _vwo_code=(function(){
               var account_id=271305,
               settings_tolerance=2000,
               library_tolerance=2500,
               use_existing_jquery=false,
               is_spa = 1,
               /* DO NOT EDIT BELOW THIS LINE */
               f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&f='+(+is_spa)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
               </script>
               <!-- End Visual Website Optimizer Asynchronous Code -->
                <link rel="manifest" href="/manifest.json"/>
               <?php endif;?>

               </head>

               <body id="<?php echo $this->controller; ?>">
               <?php if(Env::isLive()):?>
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PKJRB2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
               <script type="text/javascript">var rumMOKey='d8e9375881b0765420d8ce6da1523300';(function(){if(window.performance && window.performance.timing && window.performance.navigation) {var site24x7_rum_beacon=document.createElement('script');site24x7_rum_beacon.async=true;site24x7_rum_beacon.setAttribute('src','//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey='+rumMOKey);document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);}})(window)</script>
               <?php endif;?>
