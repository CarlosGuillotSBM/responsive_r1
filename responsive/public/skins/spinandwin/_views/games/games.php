<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->

<!-- MIDDLE CONTENT -->
<div class="middle-content__box games games-pages">

     <div class="live-casino-banner ui-scheduled-content-container">
        <!-- Edit point Banner -->
        <?php edit($this->controller,'games-latest-offer'); ?>
        <?php @$this->repeatData($this->content['games-latest-offer']); ?>
    </div>
    
    <div class="page-banner">
        <?php edit($this->controller,'page-banner');  ?>
        <?php @$this->getPartial($this->content['page-banner'],1); ?>
    </div>

    <?php include '_global-library/partials/games/games-list.php'; ?>
</div>
<!-- END MIDDLE CONTENT -->

<!-- BOTTOM CONTENT -->
<div class="bottom-content">
    <?php
        if ($this->action != "slots") {
            edit($this->controller.$this->action,'footer-seo', $this->action);
            @$this->getPartial($this->content['footer-seo'], 1); 
        } else {
            edit($this->controller,'footer-seo',$this->action);
            @$this->getPartial($this->content['footer-seo'],$this->action); 
        }
    ?>
</div>
<!-- END Games info modal -->