<?php
$cmaFriendly = @$this->row['CMAFriendly'] ? $this->row['CMAFriendly'] : 0;
$gameProvider = @$this->row['GameProviderID'] ? $this->row['GameProviderID'] : 0;
?>
<script type="application/javascript">
var sbm = sbm || {};
sbm.games = sbm.games || [];
sbm.games.push({
"id"    : <?php echo json_encode(@$this->row['DetailsURL']);?>,
"icon"  : "<?php echo @$this->row['Image'];?>",
"bg"    : <?php echo json_encode(@$this->row['Text1']);?>,
"title" : <?php echo json_encode(@$this->row['Title']);?>,
"type"  : "game",
"desc"  : <?php echo json_encode(@$this->row['Intro']);?>,
"cma": <?php echo json_encode($cmaFriendly);?>,
"provider": <?php echo json_encode($gameProvider);?>,
"thumb" : "<?php echo @$this->row['GameScreen'];?>",
"detUrl": <?php echo json_encode(@$this->row['Path']);?>
});
</script>
<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->

<!-- MIDDLE CONTENT -->
<div class="middle-content__box  games-details-content">
  
  <?php include '_partials/games/games-details-page.php'; ?>
  <div class="clearfix"></div>
  <div class="games-detail-page__bottom">
    <?php edit('gamesdetail','games-detail',$this->row['DetailsURL']); ?>
    <?php @$this->repeatData($this->content['games-detail']); ?>
  </div>
  
</div>
<!-- END MIDDLE CONTENT -->