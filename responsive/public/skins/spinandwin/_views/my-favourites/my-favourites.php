<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->
<div class="middle-content__box lobby-content games">
    
    <!-- FAVOURITES -->
    <section class="section middle-content__box">
        <div class="section__header">
            <h1 class="section__title">My Favourites</h1>
        </div>
        <!-- Favourites row -->
        <div class="favourites">
            <?php include "_global-library/_editor-partials/favourite-game-widget.php" ?>
        </div>
        <!-- /Favourites row -->
    </section>

    <div class="clearfix"></div>
    <!-- /MAGICAL RECOMENDATIONS -->
</div>
<!-- END MAIN CONTENT AREA -->