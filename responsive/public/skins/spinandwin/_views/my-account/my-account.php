<script type="application/javascript">
skinModules.push( { id: "AccountActivity" } );
skinModules.push( { id: "PersonalDetails" } );
skinModules.push( { id: "PlayerAccount" } );
skinModules.push( { id: "ConvertPoints", options: { name: "Points", url: "/promotions/loyalty-points/" } });
skinModules.push( { id: "PlayerReferAFriend" } );
</script>
<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->

<!-- MIDDLE CONTENT -->
<div class="middle-content my-account myaccount-content middle-content__box">
	
	<div class="myaccount__loyalty">
		<div class="vip-level-area">
			<span class="vip-text">Your VIP Level:</span> <a href="/loyalty/"><span class="vip-shape" data-bind="text: vipLevel(), css: 'level-' + playerClass()"></span><span class="vip-level-area-icon"></span></a>

	</div>
	</div>
	<dl class="tabs" data-tab>
		<dd class="first active"><a href="#myaccount-1" data-bind="click: PersonalDetails.getPlayerAccount">Account Balances</a></dd>
		<dd><a id="ui-my-details-tab" href="#myaccount-2" data-bind="click: PersonalDetails.getPersonalDetails">Personal Details</a></dd>
		<dd><a id="ui-activity-tab" href="#myaccount-3" data-bind="click: AccountActivity.getDefaultData">Account Activity</a></dd>
		<dd class="last"><a id="ui-redeem-tab" href="#myaccount-5">Claim Code</a></dd>
<!--		<dd class="last"><a id="ui-raf-tab" href="#myaccount-6" data-bind="click: PlayerReferAFriend.getPlayerRafInfo">Refer a Friend</a></dd>-->

	</dl>
	<div class="tabs-content">
		<!-- tab  content 1 -->
		<div class="content active" id="myaccount-1">

			<?php include '_global-library/partials/my-account/myaccount-balances.php' ?>
			<?php include '_partials/my-account/myaccount-promotions.php' ?>
			</div><!-- end  tab 1 -->
			<!-- tab content 2-->
			<div class="content" id="myaccount-2">
				<?php include '_global-library/partials/my-account/myaccount-details.php' ?>
			</div>
			<!-- end tab content 2 -->
			<!-- tab content 3 -->
			<div class="content" id="myaccount-3">
				<?php include '_global-library/partials/my-account/myaccount-transactions.php' ?>
			</div>
			<!-- end  tab content 3 -->
			<!-- tab content 4 -->
<!--			<div class="content" id="myaccount-4" -->
				<?php //include '_global-library/partials/my-account/myaccount-limits.php' ?>
<!--			</div --> 
			<!-- end  tab content 4 -->
			<!-- tab content 5 -->
			<div class="content" id="myaccount-5">
				<?php include '_global-library/partials/my-account/myaccount-redeem.php' ?>
			</div>
			<!-- end  tab content 5 -->
			<!-- tab content 6 -->
<!--			<div class="content" id="myaccount-6">-->
<!--				--><?php //include '_global-library/partials/my-account/myaccount-refer-a-friend.php' ?>
<!--			</div>-->
			<!-- end  tab content 6 -->

		</div><!-- tabs-content -->
	</div>
	<!-- END MIDDLE CONTENT CENTRE COLUMN -->