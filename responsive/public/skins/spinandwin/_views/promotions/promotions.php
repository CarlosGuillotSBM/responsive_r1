<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
      <script type="application/javascript">   
          skinModules.push({
              id: "RadFilter"
          });
      </script>
<!-- /home breadcrumbs -->
<div class="middle-content__box">
  <section class="section promotions">
    <?php edit($this->controller,'promotions'); ?>
    <div class="tabs-content">
      <!-- PROMOTIONS CATEGORY TABS -->
      <dl class="tabs" data-tab>
        <dd class="last"><a href="#promotions-4">VIP</a></dd>
        <dd><a href="#promotions-3">SPECIALS</a></dd>
        <dd><a id="ui-my-details-tab" href="#promotions-2">WEEKLY</a></dd>
        <dd class=""><a id="ui-activity-tab" href="#promotions-1">WELCOME</a></dd>
        <dd class="first active"><a id="ui-activity-tab" href="#promotions-0">ALL</a></dd>
      </dl>
      <!-- END PROMOTIONS CATEGORY TABS -->
      <!-- VIP PROMOTIONS CONTENT-->
      <div class="content" id="promotions-4">
        <div class="section__header">
          <h1 class="section__title">VIP PROMOTIONS</h1>
        </div>
        <?php @$this->repeatDataWithTabs($this->content["promotions"],5,1); ?>
      </div>
      <!-- END VIP PROMOTIONS CONTENT-->
      <!-- SPECIAL PROMOTIONS CONTENT-->
      <div class="content" id="promotions-3">
        <div class="section__header">
          <h1 class="section__title">SPECIAL PROMOTIONS</h1>
        </div>
        <?php @$this->repeatDataWithTabs($this->content["promotions"],4,1); ?>
      </div>
      <!-- END SPECIAL PROMOTIONS CONTENT-->
      <!-- WEEKLY PROMOTIONS CONTENT-->
      <div class="content" id="promotions-2">
        <div class="section__header">
          <h1 class="section__title">WEEKLY PROMOTIONS</h1>
        </div>
        <?php @$this->repeatDataWithTabs($this->content["promotions"],3,1); ?>
      </div>
      <!-- END WEEKLY PROMOTIONS CONTENT-->
      <!-- WELCOME PROMOTIONS CONTENT-->
      <div class="content" id="promotions-1">
        <div class="section__header">
          <h1 class="section__title">WELCOME PROMOTIONS</h1>
        </div>
        <?php @$this->repeatDataWithTabs($this->content["promotions"],2,1); ?>
      </div>
      <!-- END WELCOME PROMOTIONS CONTENT-->
      <!-- ALL PROMOTIONS CONTENT-->
      <div class="content active" id="promotions-0">
        <div class="section__header">
          <h1 class="section__title">ALL PROMOTIONS</h1>
        </div>
        <?php //@$this->repeatDataWithTabs($this->content["promotions"],1,1); ?>
        <?php @$this->repeatData($this->content["promotions"],1, "_global-library/partials/promotion/promotion.php"); ?>
      </div>
      <!-- END ALL PROMOTIONS CONTENT-->
    </div>
    <!-- END TABS CONTENT -->
    <div class="clearfix"></div>
    <!--  PROMOTIONS SEO CONTENT-->
    <?php edit($this->controller,'promotions-seo-content'); ?>
    <?php @$this->getPartial($this->content['promotions-seo-content'],1); ?>
    <!--  END PROMOTIONS SEO CONTENT-->
  </section>
  <!-- END PROMOTIONS SECTION -->
  
</div>
<!-- END MIDDLE CONTENT BOX-->