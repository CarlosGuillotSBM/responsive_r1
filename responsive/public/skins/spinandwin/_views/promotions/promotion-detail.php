<script type="application/javascript">
skinModules.push({
id: "OptIn"
});

skinModules.push({
  id: "RadFilter"
});
</script>
<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->
<!-- MIDDLE CONTENT -->
<div class="middle-content__box" itemscope itemtype="http://schema.org/Article">
  <!-- END breadcrumbs -->
  <section class="section">
    <h1 class="section__title"><?php echo $this->content["Title"];?></h1>
    <!-- PROMOTIONS CONTENT -->
    <div class="">
      <div class="clearfix"></div>
      
      <?php edit($this->controller,'promotions'); ?>
      
      <!-- promotions list -->
      <div class="promotions-detail">
        
        <!-- Promotions detail -->
        <article class="promotion--featured">
          <div class="promotion__wrapper">
            
            <div class="promotion__image">
              <img src="<?php echo $this->content["Text8"] != '' ? $this->content["Text8"] : $this->content["Image"]?>" alt="<?php //echo $this->content["Alt"]?>">
            </div>
            <div class="promotion__content__wrapper">
            
              <div class="promotion__content">
                <?php
                echo $this->content["Body"];
                ?>
              </div>

              <!-- CTAS -->
              <div class="promo__container__row">
                <!-- OPT-IN -->
                <a style="display: none" class="promo__cta opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>
                <?php if($this->content['Text4'] === '') {?>
                  <!-- PLAY NOW -->
                  <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__cta join-now"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                  <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__cta join-now"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                <?php } else { ?>
                  <!-- PLAY NOW -->
                  <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__cta join-now"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                  <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__cta join-now"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                <?php }?>                 
                <?php
                if ($this->content['DetailsURL'] == "christmas-slots-race") {
                include "_global-library/widgets/leaderboard/leaderboard.php";
                }
                ?>
              </div>
              <!-- /CTAS -->

              <!-- PROMO FOOTER NAV MOBILE -->
                <div class="footer-nav__container">
                    <a data-hijack="true" style="display: none" data-tournament="<?php echo $this->content['DetailsURL'] ?>" class="cta-opt-in">OPT-IN</a>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php }?>                     
                    <a data-hijack="true" href="/promotions/<?php echo $this->getNextPromoURL($this->content["DetailsURL"]); ?>" class="cta-next-promo">NEXT PROMO ></a>
                </div>
              
              <dl id="ui-promo-tcs" class="promotion__terms-conditions accordion" data-accordion>
                <dd class="accordion-navigation active">
                <a href="#panelTC">Terms &amp; Conditions <span style="font-size:25px;">&#8964;</span> </a>
                <div id="panelTC" class="content active">
                  <?php echo @$this->content["Terms"];?>
                </div>
                </dd>
              </dl>
            </div>
            <div class="_author-block">
              <div class="_author-block__biography">
                <ul>
                  <li class="_author-block__social-icons"><a href="https://www.facebook.com/SpinAndWin" target="_blank"><img src="/_images/common/social-icons/facebook.png"></a></li>
                  <li class="_author-block__social-icons"><a href="https://twitter.com/spinandwin" target="_blank"><img src="/_images/common/social-icons/twitter.png"></a></li>
                </ul>
              </div>
            </div>
          </div>
        </article>
        <!-- /Promotions detail -->
        <div class="promotion" id="related--promos" >
          <!-- promo 2 -->
           <?php @$this->getPartial($this->promotions['promotions'],2); ?>
           <!-- /promo 2 -->
          <!-- promo 3 -->
          <?php @$this->getPartial($this->promotions['promotions'],3); ?>
          <!-- /promo 3 -->
          <!-- promo 4 -->
          <?php @$this->getPartial($this->promotions['promotions'],4); ?>
          <!-- /promo 4 -->
        </div>
      </div>
      <!-- /promotions list -->
    </div>
    <!-- PROMOTIONS CONTENT -->
  </section>
</div>
<!-- /MIDDLE CONTENT -->