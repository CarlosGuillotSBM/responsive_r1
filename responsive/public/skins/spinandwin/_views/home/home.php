<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php"; ?>
<!-- /home breadcrumbs -->
<div class="middle-content__box">
	
    <!-- VEGAS FAVOURITES -->
    <section class="section  games-list home-games">
        <?php edit($this->controller,'hub'); ?>
        <div class="section__header">
           <!-- <span class="section__header__viewall"><a data-hijack="true" href="/games/">MORE GAMES</a></span>-->
            <h1 class="section__title">SPIN FAVOURITES</h1>
        </div>
        <?php include "_global-library/partials/hub/hub-grid-7.php" ?>
        <div class="clearfix"></div>
    </section>
    <!-- /VEGAS FAVOURITES -->

    <!-- NEW ON THE BLOCK -->
    <section class="section middle-content__box games-list home-games new-games">
        <?php edit($this->controller,'new-games'); ?>
        <div class="section__header">
            <!--<span class="section__header__viewall"><a data-hijack="true" href="/games/">MORE GAMES</a></span>-->
            <h1 class="section__title">NEW ON THE BLOCK</h1>
        </div>
        
        <!-- Hub grid -->
        <?php
        $hub = 'new-games';
        include "_global-library/partials/hub/hub-grid-8.php"
        ?>
        <!-- /Hub grid -->
    </section>
    <!-- /NEW ON THE BLOCK-->
    
    <!-- Live CASINO -->
    
    <section class="section middle-content__box games-list home-games new-games">
        <?php edit($this->controller,'live-casino'); ?>
        <div class="section__header">
            <h1 class="section__title">LIVE CASINO</h1>
        </div>
        <!-- Hub grid -->
        <?php
        $hub = 'live-casino';
        include "_global-library/partials/hub/hub-grid-8.php"
        ?>
        <!-- /Hub grid -->
    </section>
    <!-- /Live CASINO -->

    <!-- SEO CONTENT -->
    <section class="section middle-content__box">
        <!-- content -->
        <?php if(config("Device") == 1) {
            edit($this->controller,'home-seo-content');
            @$this->getPartial($this->content['home-seo-content'],1);
        } else {
            $seoKey = "home-seo-content";
            include'_global-library/partials/home-seo/mobile.php';
        }?>
        <!-- /content -->
    </section>
    <!-- /SEO CONTENT -->
</div>
