<!-- Main Hero Offer Section -->
<section class="ui-main-offer">

	<figure class="row">
	  <div class="ui-main-hero wow bounce" data-wow-delay=".15s">
	  	<img width="343" height="444" src="/_images/ab/main-hero.png" alt="">
	  </div>
	  <figcaption class="wow bounce" data-wow-delay=".35s">
	  	<h1>100 FREE SPINS<br>+100% BONUS UP TO £500</h1>
	  	<h3></h3>
	  	<!-- <img src="/_images/ab/main-offer-slot-logo.png" width="686" height="95" alt=""> -->
	  	<a class="join-now" href="/register/">Join Now</a>
	  	<a class="ui-main-hero-tcs" href="/promotions/triple-bonus-signup-offer/?showtcs=1"> Min Deposit £20. Free spins on pre-selected games. Wagering and T&amp;Cs apply.</a>
	  </figcaption>
	</figure>

	<div id="particle"></div>

</section>

<!-- Features Section -->
<section class="ui-features">
	<div class="row">
		<article class="wow bounceIn" wow-data-offset="10">
		  	<img src="/_images/ab/gift-icon.svg" height="auto" width="200">
			<h2>A world full of games</h2>
			<p>Come and play the most innovative online slots. A classic collection of table and card games including Roulette, Blackjack and Poker, also available as part of our Live Casino!</p>
		</article>
		<article class="wow bounceIn" wow-data-offset="10">
		  	<img src="/_images/ab/slot-icon.svg" height="auto" width="200">
			<h2>Fun, prizes and suprises!</h2>
			<p>Regular promotions and surprise offers that keep the good times rolling game after game. Up to £1,000 on your first 3 deposits with us, as well as up to 200 free spins! </p>
		</article>
		<article class="wow bounceIn" wow-data-offset="10">
		  	<img src="/_images/ab/rewards-icon.svg" height="auto" width="200">
			<h2>Loyalty Rewards</h2>
			<p>Get exclusive perks such as daily cashback, invite-only promotions, table game insurance, prize draws and a personalised VIP programme and manager at your service.</p>
		</article>
	</div>
</section>

<!-- Slots Section -->
<section class="ui-slots">
	<div class="row">

	<!-- NEW ON THE BLOCK -->
    <section class="games-list home-games new-games">
        <?php edit($this->controller,'new-games-ab'); ?>
        <div class="section__header">
            <span class="section__header__viewall"><a data-hijack="true" href="/games/">MORE GAMES</a></span>
            <h1 class="section__title">Featured Games</h1>
        </div>

        <!-- Hub grid -->
        <?php
        $hub = 'new-games-ab';
        include "_global-library/partials/hub/hub-grid-8.php"
        ?>
        <!-- /Hub grid -->
    </section>
    <!-- /NEW ON THE BLOCK-->

	</div>
</section>

<!-- SEO Section -->
<section class="ui-seo">
	<div class="row">

		<article class="text-partial">
			<h1>Spin and Win - Let the Good Times Roll - As Seen on TV</h1>
			<div class="ui-seo-content">
				<p>Spinandwin.com, as seen on TV, is one of the most exciting online casinos offering the very best games from innovative and top game providers in the online gaming industry to players from the UK and European community.&nbsp; Spin and Win casino promises to you an unparalleled gaming experience with high quality gaming graphics that will certainly tickle your fancy.</p>
				<p>At Spin and Win casino you will discover over 300 games that cater to the penchant of <a href="/slots/">all online slots lovers</a>, as well as for those who are fans of roulette, <a href="/scratchcards/">scratch cards</a>&nbsp;or table &amp; card games such as Black Jack and Poker. Spinandwin believes in giving to its players the best possible atmosphere where they can enjoy responsible gaming and discover the passion and fun that prevails on the site.</p>
				<p>Fairness and security are the basic fundamentals and essence on which Spinandwin.com has been brought into existence. Spin and Win Casino has carefully chosen and gathered the most popular and trendy games that are fun to play and captivating from top gaming providers to provide a world class gaming service that is unmatched. You will encounter a unique set of thrilling games that are <a href="/slots/exclusive/">exclusively available at Spinandwin</a>.</p>
				<p>Join Spinandwin.com today and start spinning with £20 Free on the featured game to experience a new era of gaming entertainment. New players will have a warm welcome with 3 magnificent bonuses on their first three deposits up to £1,000. You will be able to double your first deposit by 200% up to £500, while on your second and third deposits you will receive a cool 100% bonus up to £250.</p>
				<p>At Spinandwin.com casino we pride ourselves on giving more than other sites which you will discover throughout the array of promotions on offer! Spin and Win Casino is doubtlessly convinced that continuous innovation and creativity are the main contributors to provide the best gaming environment for you.</p>
				<p>With our very own Loyalty Club we look forward to improve your experience with each spin that you take. Not only will you move up through the various ranks when playing at UK's top mobile casino site, Spinandwin.com, but you will be rewarded for your loyalty with a huge range of exclusive advantages such as Loyalty Points, Weekly Promos, Dedicated VIP manager, Table Game Insurance and Weekly Free Bets among many other special offers just for you.</p>
				<p>Start spinning now at Spinandwin.com and Let the Good Times Roll.&nbsp;</p>
			</div>
		</article>

	</div>
</section>

<!-- Main Hero Offer Particles -->
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<!-- WOW JS Animations -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<!-- JS -->
<script>

// Particles Effect on Main Hero
	var options = {"particles":{"number":{"value":99,"density":{"enable":true,"value_area":552.4033491425909}},"color":{"value":"#FFD47D"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":3},"image":{"src":"img/github.svg","width":70,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":2,"random":true,"anim":{"enable":false,"speed":40,"size_min":0.1,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":1.5782952832645452,"direction":"none","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"repulse"},"onclick":{"enable":true,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":400,"size":40,"duration":2,"opacity":8,"speed":3},"repulse":{"distance":200,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":false};
particlesJS("particle", options);


// Trigers WOW JS animation css
new WOW().init();

</script>
