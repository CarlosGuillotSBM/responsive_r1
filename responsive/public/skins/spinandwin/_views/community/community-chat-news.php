<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->
<!-- MIDDLE CONTENT -->
<div class="middle-content__box community-chat-news">
	<div class="two-columns-content-wrap">
		<!-- community nav -->
		<?php include "_partials/community-nav/community-nav.php" ?>
		<!-- /community nav -->
		<!-- left column -->
		<div class="two-columns-content-wrap__left">
			<!-- chat hosts area -->
			<div class="community-chat-news__hosts">
				<?php edit($this->controller,'chat-host'); ?>
				<?php @$this->repeatData($this->content['chat-host']);?>
			</div>
			<!-- /chat hosts area -->
			<div class="clearfix">&nbsp;</div>
			<div class="community-chat-news__article">
				<?php edit($this->controller,'chat-news'); ?>
				<!-- repeatable content -->
				<?php @$this->repeatData($this->content['chat-news']);?>
				<!-- /repeatable content -->
			</div>
			<div class="clearfix"></div>
		</div>
		<!--  /left column -->
		<!-- right column -->
		<div class="two-columns-content-wrap__right">
		<?php if (config("RealDevice") != 3) { ?>
			<?php edit($this->controller,'chat-news-side-content'); ?>
			<!-- repeatable content -->
			<?php @$this->repeatData($this->content['chat-news-side-content']);?>
			<!-- /repeatable content -->
		<?php } ?>
		</div>
		<!-- /right column -->
	</div>
	<!-- /content  -->
</div>
<!-- END MIDDLE CONTENT -->