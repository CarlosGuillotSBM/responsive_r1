<?php
	$titleHeaderHeight = 25;
	$slideHeight = 95;
	$slidesToShow = 3;
?>
<script type="application/javascript">

	skinPlugins.push({
		id: "ProgressiveSlider",
		options: {
			slidesToShow: <?php echo $slidesToShow ?>,
			mode: "vertical"
		}
	});
	skinPlugins.push({
		id: "WinnersSlider",
		options: {
			slidesToShow: <?php echo $slidesToShow ?>,
			mode: "vertical"
		}
	});
</script>


<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php";
?>
<!-- /home breadcrumbs -->
<!-- MIDDLE CONTENT -->
<div class="middle-content__box community">
	<section class="section">
		<h1 class="section__title">COMMUNITY</h1>
		<div class="two-columns-content-wrap">
			<!-- community nav -->
			
			<?php include "_partials/community-nav/community-nav.php" ?>
			<!-- /community nav -->
			<!-- left column -->
			<div class="two-columns-content-wrap__left">
				<div class="section__header">
					<h1 class="section__title" itemprop="name"><?php echo @$this->content['Title'];?> - 
					<span class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></span>
					</h1>
				</div>
				<article class="article community-detail">
					<div class="article__content">
					<?php edit($this->controller,$this->action,$this->route[0],"display: block;");?>
						<div class="article__content__body" itemprop="articleBody">
							<img class="article__image" src="<?php echo @$this->content['Image'];?>" alt="<?php echo @$this->content['ImageAlt'];?>">
							<?php echo @$this->content['Body'];?>
						</div>
					</div>
					
						<!-- Winner Slot Details -->
			<?php 
			if(@$this->content['Text7'] != '')
			{
				include "_global-library/_editor-partials/community-winner-game.php"; 
			}	
			?>
			<!-- End Winner Slot Details -->
					<?php if(exists(@$this->content['Text1'])):?>
					<!-- Author Block -->
					<div class="_author-block">
						<div class="_author-block__avatar">
							<?php if(exists(@$this->content['Image1'])):?>
							
							<img width="" src="<?php echo @$this->content['Image1'];?>" alt="<?php echo @$this->content['ImageAlt1'];?>">
							
							<?php endif;?>
						</div>
						
						<h4>
						
						<span itemprop="author" itemscope itemtype="http://schema.org/Person"></span>
						<span itemprop="name"><?php echo @$this->content['Text1'];?></span>
						
						</h4>
						
						<div class="_author-block__line">
							<div class="_author-block__lineleft"></div>
							<div class="_author-block__lineright"></div>
						</div>
						
						<div class="_author-block__biography">
							<p><?php echo @$this->content['Terms'];?></p>
							
						</div>
						<ul>
							<a target="_blank" href="<?php echo @$this->content['Text2'];?>" class="_author-block__link" target="_author">
								<li class="_author-block__social-icons">Author's Social Profile: <img src="/_images/common/social/icon-gl.png"></li>
							</a>
						</ul>
						
					</div>
						<!-- Author Block -->
					<?php endif;?>	
					</article>
				</div>
				<!--  /left column -->
				<!-- right column -->
				<div class="two-columns-content-wrap__right">
				<?php if (config("RealDevice") != 3) { ?>
					<?php edit($this->controller,'blog-side-content'); ?>
					<!-- repeatable content -->
					<?php @$this->repeatData($this->commoncontent['blog-side-content']);
					?>
					<!-- /repeatable content -->
						
					<!-- prog jackpots -->
					<div class="progressive-jackpots__wrapper slider">

						<img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
						
						<article class="progressive-jackpots" style="height: <?php echo ($slidesToShow * $slideHeight + $titleHeaderHeight). 'px' ?> ; overflow: hidden">
							
							<div class="progressive-jackpots__title">
								<h1>Progressive Jackpots</h1>
							</div>

							<?php edit($this->controller,'progressives'); ?>						
							<?php @$this->repeatData($this->commoncontent['progressives']);?>
							
						</article>
					</div>
					<!-- / prog jackpots -->
					<!-- yesterdays winning -->
					<div class="yesterdays-win__wrapper">
						
						<img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
						
						<article class="yesterdays-win">
							
							<div class="yesterdays-win__title">
								<h1>Yesterdays Total Winnings</h1>
							</div>
							
							<div class="yesterdays-win__box">
								<div class="yesterdays-win__box__amount"><span>&pound;<?php echo @$this->winners_total[0]['Win'];?></span></div>
							</div>
						</article>
					</div>
					<!--/ yesterdays winning -->
					<!-- latest winners -->
					<div class="latest-winners__wrapper slider">
						<img class="hub-shape-holder" src="/_images/common/4x3-bg.png">

						<article class="latest-winners" style="height: <?php echo ($slidesToShow * $slideHeight + $titleHeaderHeight). 'px' ?> ; overflow: hidden">

							<div class="latest-winners__title">
								<h1>Recent Winners</h1>
							</div>
							<?php edit($this->controller,'latest-winners'); ?>
							<?php @$this->repeatData($this->commoncontent['latest-winners']);?>
						</article>
					</div>
					<!-- /latest winners -->
					
				<?php } ?>
				</div>
				<!-- /right column -->
			</section>
		</div>
		<!-- END MIDDLE CONTENT -->