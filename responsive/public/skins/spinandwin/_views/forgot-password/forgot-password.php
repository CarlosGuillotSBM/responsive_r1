<script type="application/javascript">
    skinModules.push({ id: "ForgotPassword" });
</script>

<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->

  <!-- MIDDLE CONTENT -->
  <div class="middle-content__box">

    <section class="section">
      <h1 class="section__title"><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
      <!-- CONTENT -->
      
      <div class="content-template">
        
        
        <!-- FORGOT PASSWORD FORM -->
        <form id="forgotpass" class="hide" name="forgotpass" method="post" action="" style="display: block;">
          <div class="forgotpass__wrapper">
            <div class="forgotpass__input">
              <div data-bind="visible: ForgotPassword.forgot" style="display: none">
                <p>Enter your email address and we will send you a link to change your password:</p>
                <p>
                <input data-bind="value: ForgotPassword.email" type="text" name="textfield" id="fpwd_email">
                <span data-bind="visible: ForgotPassword.invalidEmail" class="error">Please enter a valid email.</span>
                </p>
                <p><input data-bind="click: ForgotPassword.recoverPassword" type="button" name="button" id="btn_submit_forgotpass" value="SUBMIT"
                class="button expand"></p>
              </div>
              <div data-bind="visible: ForgotPassword.recover" style="display: none">
                <p>Enter and repeat your new password:</p>
                <p><input data-bind="value: ForgotPassword.newPassword" type="password" placeholder="Enter new password" name="textfield" id="reset_pass"></p>
                <p>
                <input data-bind="value: ForgotPassword.repeatPassword" type="password" placeholder="Confirm your password" name="textfield" id="reset_pass_repeat">
                <span data-bind="visible: ForgotPassword.passwordsDoNotMatch" class="error">Password fields do not match.</span>
                <span data-bind="visible: ForgotPassword.passwordsAreEmpty" class="error">Please fill both fields.</span>
                <span data-bind="visible: ForgotPassword.invalidNewPassword" class="error">Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces.</span>
                </p>
                <p><input data-bind="click: ForgotPassword.resetPassword" type="button" name="button" id="btn_submit_resetpass" value="RESET PASSWORD"
                class="button expand"></p>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /CONTENT-->
    </section>
  </div>
  <!-- END MIDDLE CONTENT -->
