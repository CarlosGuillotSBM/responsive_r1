<!DOCTYPE html>
<html>
<head>
	<title>Under Maintenance</title>
<style type="text/css">
	body{
		background-color:white;
		color:#419aeb;
		text-align: center;
		font-size: 20px;
		font-family: arial;
	}
	h1{
		font-size: 40px;		
	}

	.logo {
		width: 200px;
	}

</style>
</head>
<body>
<br><br><br>
<img class="logo" src="/_images/logo/logo-desktop.png">	
<h1>Under Maintenance</h1>
<div>Sorry, we are offline for just a few minutes. <br><br>Please come back soon.</div>
</div>
</body>
</html>