<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->
 <!-- MIDDLE CONTENT -->
  <div class="middle-content__box">
    <section class="section">
      <!-- CONTENT -->
      <div class="content-template">
      	<div class="login-box--loginpage">
			<?php include'_partials/login/login-box.php' ?>
		</div>
		<div class="login-banner">
		    <?php edit($this->controller,'login',$this->action); ?>
		<?php @$this->repeatData($this->content[$this->controller],1,''); ?>

    </div>
		
      </div>
      <!-- /CONTENT-->

      <div class="login-terms">
                  <!-- Accordion -->
                <?php edit('login','Terms-and-coditions-login'.$this->action);?>

                <dl id="ui-promo-tcs" class="accordion accordion_login" data-accordion>

                  <dd class="accordion-navigation active">
                    
                    <a href="#panelTC" class="accordion-navigation__toggle accordion_login-terms">Terms &amp; Conditions</a>

                    <div id="panelTC" class="content active">
                      <?php @$this->getPartial($this->content['Terms-and-coditions-login'.$this->action],1); ?>
                    </div>
                  </dd>
                </dl>
                <!-- /Accordion -->
</div>

      
    </section>


  </div>
  <!-- END MIDDLE CONTENT -->



