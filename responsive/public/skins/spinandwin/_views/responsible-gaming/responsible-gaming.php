<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->
<!-- MIDDLE CONTENT -->
<div class="middle-content__box">
  <section class="section">
    <h1 class="section__title"><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
    <!-- CONTENT -->
    <?php edit($this->controller,'responsible-gaming'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['responsible-gaming']);?>
      <!-- /repeatable content -->
    </div>


    <?php include "_global-library/partials/reality-check/reality-check-settings.php"; ?>


    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->