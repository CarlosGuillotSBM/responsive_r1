<style type="text/css">


</style>
<!-- MIDDLE CONTENT -->
<div class="accept-terms middle-content__box">
    
    <?php // include 'spinandwin/_partials/breadcrumbs/breadcrumbs.php'; ?>
    <!-- END breadcrumbs -->
    <section class="section">
		<?php edit($this->controller,'terms-popup'); ?>
        <h1 class="section__title">Our Terms &amp; Conditions have changed</h1>
        <div class="small-12 medium-6 large-6  small-centered medium-centered large-centered columns" style="margin-top:20px;margin-bottom:20px;">
            <a class="button small-12 medium-12 large-12" style="font-size:0.7em; width:100%; height:44px; line-height:22px" data-bind="click: LoginBox.acceptTC">I Accept the Terms &amp; Conditions</a>
        </div>
        <!-- CONTENT -->
        <div class="content-template">
            <!-- repeatable content -->
            <?php @$this->repeatData($this->content['terms-popup']);?>
            <!-- /repeatable content -->
        </div>
        <!-- /CONTENT-->
    </section>
</div>
<!-- END MIDDLE CONTENT -->
<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->