<?php
// the hub data is common for all the same controller
// special exception for exclusive slots so it can have it's own hub
$page = $this->controller;
if($this->action == 'exclusive') $page.= 'exclusive';
?>
<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->

    

    <div class="games-pages">

    <div  class="live-casino-banner ui-scheduled-content-container">
        <!-- Edit point Banner -->
        <?php edit($this->controller,'games-latest-offer'); ?>
        <?php @$this->repeatData($this->content['games-latest-offer']); ?>
    </div>

        <div class="live-casino-banner">
            <?php edit($this->controller,'live-casino-main-offer-banner', $this->action); ?>
            <?php @$this->repeatData($this->content['live-casino-main-offer-banner']);?>
		</div>

        <!-- Hub grid -->
        <section  class="section games-list live-casino-list middle-content__box">
            <?php edit($page,'acquisition-title');?>
            <?php @$this->getPartial($this->content['acquisition-title'], 1); ?>

            <?php edit($page,'acquisition');
				$countable = (array)@$this->content['acquisition'];
                if(count(@$countable) > 0):
            ?>

            <div class=" games-list middle-content__box top-games-hub">
                <?php
                $hub = 'acquisition';
                include "_global-library/partials/hub/hub-grid-9.php"
                ?>
            </div>
        </section>
        <?php endif;?>
    <div class="games-pages middle-content__box">
        <div class="page-banner">
            <?php edit($this->controller,'page-banner'); ?>
            <?php @$this->getPartial($this->content['page-banner'],1); ?>
        </div>
        

        <!-- games list-->
        <?php include '_partials/slots/games-list.php'; ?>
        <!-- end games list -->
        <!-- END Games info modal -->

        <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
            <div class="cta-bottom-wrap ">
                <div class="cta-bottom fullwidth">
                    <div class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                        <div class="inner">
                            <a data-hijack="true" class="menu-link" href="/register/">JOIN NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-content">
        <?php
            if ($this->action == 'exclusive') {
                edit($this->controller.$this->action,'footer-seo');
            } else {
                edit($this->controller,'footer-seo',$this->action);
            }
        ?>
        <?php @$this->getPartial($this->content['footer-seo'],$this->action); ?>
    </div>

</div>
<!-- END MIDDLE CONTENT -->