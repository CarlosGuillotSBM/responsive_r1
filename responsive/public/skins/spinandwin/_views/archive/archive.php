<!-- breadcrumbs -->
<?php include "spinandwin/_partials/breadcrumbs/breadcrumbs.php" ?>
<!-- /home breadcrumbs -->
<!-- MIDDLE CONTENT -->
<div class="middle-content__box">
  <section class="section">
    <h1 class="section__title archive-title"><?php echo $this->controller; ?></h1>
    
    <div class="content-template">
      <!-- Archive Page Content -->
      
      <!--     <?php edit($this->controller,'archive'); ?>
      -->
      <!-- repeatable content -->
      <!--  <?php// @$this->repeatData($this->content['archive']);?> -->
      <!-- /repeatable content -->
      <!--   <a href='/archive/promotions/'>Promotions Archive</a>&nbsp;&nbsp;&nbsp;&nbsp;
      <a href='/archive/winners/'>Winners Archive</a>&nbsp;&nbsp;&nbsp;&nbsp;
      <a href='/archive/blogs/'>Blog Archive</a> -->
      <div class="archive-content">
        <div class="archive-content__box">
          <a href="/archive/promotions/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic">
              </div>
              <h4>PROMOTIONS ARCHIVE</h4>
            </div>
          </a>
        </div>
        <div class="archive-content__box">
          <a href="/archive/blog/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic"></div>
              <h4>BLOG ARCHIVE</h4>
            </div>
          </a>
        </div>
        <div class="archive-content__box">
          <a href="/archive/winners/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic"></div>
              <h4>WINNERS ARCHIVE</h4>
            </div>
          </a>
        </div>
        <div class="archive-content__box">
          <a href="/archive/games/">
            <div class="archive-content__box__content">
              <div class="archive-content__box__content__graphic"></div>
              <h4>GAME ARCHIVE</h4>
            </div>
          </a>
        </div>
		
      </div>
      <!-- End Archive Page Content -->
    </div>
    <!-- /two columns -->
  </section>
</div>
<!-- /CONTENT-->

</div>
<!-- END MIDDLE CONTENT -->
<!-- BOTTOM CONTENT -->
<div class="bottom-content">
</div>
<!-- END Games info modal -->
</div>
<!-- END HOME MAIN CONTENT AREA -->