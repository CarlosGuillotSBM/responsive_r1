<?php if(Env::isLive()) {?>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WNZVCNZ"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager -->

    <!-- Facebook pixel only for REGISTRATION page - should only fire once! -->
    <?php
        if(!isset($_COOKIE['fb-reg-tracker']) && strpos($_SERVER['REQUEST_URI'], 'register')) {
            // set the cookie
            // setcookie("fb-reg-tracker", true, time()+60*60*24*45); // expires in 45 days
        }
    ?>
    <!-- End Facebook pixel only for REGISTRATION page - should only fire once! -->

<?php } ?>