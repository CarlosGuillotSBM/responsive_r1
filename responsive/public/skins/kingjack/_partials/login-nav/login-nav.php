<ul data-bind="visible: !validSession()" class="login-nav">
    <li>
        <a href="/login/" data-reveal-id="login-modal" class="btn btn--secondary"> Login </a>
    </li>
	<li>
        <a data-gtag="Join Now,Web Top Nav" class="btn btn--primary" href="/register/"> Join Now </a>
    </li>
</ul>