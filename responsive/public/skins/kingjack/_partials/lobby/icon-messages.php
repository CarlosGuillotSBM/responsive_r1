<?xml version="1.0" encoding="UTF-8" standalone="no"?>
                            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
                            <svg width="24px" height="23px" viewBox="0 0 24 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                <!-- Generator: Sketch 3.3.2 (12043) - http://www.bohemiancoding.com/sketch -->
                                <title>email_F</title>
                                <desc>Created with Sketch.</desc>
                                <defs></defs>
                                <g id="Welcome" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                    <g id="Desktop" sketch:type="MSArtboardGroup" transform="translate(-55.000000, -443.000000)" fill="#ffffff">
                                    <path d="M67.1459049,466 C73.4971798,466 78.6459049,460.851275 78.6459049,454.5 C78.6459049,448.148725 73.4971798,443 67.1459049,443 C60.7946299,443 55.6459049,448.148725 55.6459049,454.5 C55.6459049,460.851275 60.7946299,466 67.1459049,466 Z M60.6843424,450.352116 L67.1658517,456.387676 L73.5859049,450.352116 L60.6843424,450.352116 Z M60.2459049,451.230528 L63.8330072,454.523147 L60.2459049,457.753446 L60.2459049,451.230528 Z M74.0459051,451.182221 L74.0459049,457.753446 L70.3606215,454.514109 L74.0459051,451.182221 Z M69.7503119,455.061959 L67.1509734,457.484054 L64.5122849,455.061959 L60.6973187,458.64 L73.6046281,458.64 L69.7503119,455.061959 Z" id="email_F" sketch:type="MSShapeGroup"></path>
                                </g>
                            </g>
                            </svg>