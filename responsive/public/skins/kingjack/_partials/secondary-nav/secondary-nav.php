<nav class="secondary-nav secondary-nav--<?php echo $this->controller; ?>">
	<ul>
		<li data-bind="visible: !validSession()" id="home"><a data-hijack="true" href="/">home</a></li>
		<li style="display: none" data-bind="visible: validSession()" id="home"><a data-hijack="true" href="/start/">Dashboard</a></li>
		<li id="games"><a data-hijack="true" href="/games/">Games</a> </li>

		<!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
		<li id="sports" class="u-hidden"><a data-hijack="true" href="/sports/">Sports</a></li>
		<!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
		<li id="slots"><a data-hijack="true" href="/slots/">Slots</a> </li>

		<li id="live-casino"><a data-hijack="true" href="/live-casino/">Live Casino</a> </li>

		<li id="promotions"><a data-hijack="true" href="/promotions/">Promos</a></li>
		<li id="winners"><a data-hijack="true" href="/community/winners/">winners</a></li>

		<li id="loyalty"><a data-hijack="true" href="/loyalty/vip-club/">Loyalty</a> </li>
		<!-- <li id="vegas-lounge"><a data-hijack="true" href="/lounge/">LOUNGE</a> </li> -->

		<li id="support"><a href="/support/" class="drop-target">support</a> </li>

		<li style="display: none" data-bind="visible: validSession()" id="my-account" class="user-nav__user-name"><a href="/my-account/" data-hijack="true">My Account</a></li>
	</ul>
</nav>