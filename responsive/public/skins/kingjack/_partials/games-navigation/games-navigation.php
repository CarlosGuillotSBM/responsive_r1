<div class="game-nav-items nav-for-desktop">
	<ul>

		<li id="slots"><a href="/games/" data-hijack="true">GAMES</a></li>
		<li id="table-card"><a href="/table-card/" data-hijack="true">TABLE &amp; CARD</a></li>
		<li id="live-casino" class="custom-hide-for-mobile"><a href="/live-casino/" data-hijack="true">LIVE CASINO</a></li>
		<li id="roulette"><a href="/roulette/" data-hijack="true">ROULETTE</a></li>
		<li id="scratch-and-arcade"><a href="/scratch-and-arcade/" data-hijack="true">SCRATCH &AMP; ARCADE</a></li>
		<li id="games"><a href="/slots/" data-hijack="true">ALL</a></li>
		<li  data-bind="visible: validSession"><a data-bind="visible: validSession" href="/my-favourites/" data-hijack="true">MY FAVOURITES</a></li>
	</ul>
</div>
