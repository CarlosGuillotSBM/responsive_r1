<div class="searchbox">
  <div class="searchbox__mask">
      <span class="searchbox__icon">
        <svg viewBox="0 0 19 19">
          <g id="UI" stroke="none">
              <g id="m-home-navigation-/-logged-in" transform="translate(-41.000000, -645.000000)">
                  <g id="menu-/-mobile-/-logged-in" transform="translate(0.000000, 48.000000)">
                      <g id="mobile-/-menu-/-item" transform="translate(2.000000, 587.000000)">
                          <g id="icon-/-home" transform="translate(39.000000, 10.000000)">
                              <g id="icon-search-white" transform="translate(0.750000, 0.750000)">
                                  <path
                                    d="M17.2235023,16.5000529 L12.9120617,12.0159385 C14.0206089,10.6981475 14.6279884,9.04009682 14.6279884,7.31399419 C14.6279884,3.28112139 11.346867,0 7.31399419,0 C3.28112139,0 0,3.28112139 0,7.31399419 C0,11.346867 3.28112139,14.6279884 7.31399419,14.6279884 C8.82799099,14.6279884 10.2707558,14.1713407 11.5042769,13.3044734 L15.8484714,17.8226138 C16.0300493,18.0111877 16.2742731,18.1151736 16.5359869,18.1151736 C16.7837087,18.1151736 17.0187105,18.0207277 17.1971083,17.8490078 C17.576164,17.4842621 17.588248,16.8794266 17.2235023,16.5000529 L17.2235023,16.5000529 Z M7.31399419,1.90799848 C10.2949238,1.90799848 12.7199899,4.33306456 12.7199899,7.31399419 C12.7199899,10.2949238 10.2949238,12.7199899 7.31399419,12.7199899 C4.33306456,12.7199899 1.90799848,10.2949238 1.90799848,7.31399419 C1.90799848,4.33306456 4.33306456,1.90799848 7.31399419,1.90799848 L7.31399419,1.90799848 Z"
                                    id="Shape"
                                    transform="translate(8.744709, 9.057587) rotate(-360.000000) translate(-8.744709, -9.057587)"
                                  />
                              </g>
                          </g>
                      </g>
                  </g>
              </g>
          </g>
        </svg>
      </span>
      <input
        type="text"
        class="searchbox__input autocomplete"
        placeholder="Search Games"
        data-bind="value: GamesMenuSearch.searchTerm, valueUpdate:'input'"
      >

      <?php include('_global-library/partials/search/search-results-container.php'); ?>
  </div>
</div>