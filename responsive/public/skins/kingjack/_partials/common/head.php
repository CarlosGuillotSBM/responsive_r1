<!doctype html> 
<html class="no-js" lang="en">
<head>
    <link rel="icon" type="image/ico" href="/_images/common/faviconnew.ico" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <?php include "_global-library/partials/common/open-graph.php"; ?>
    
    <?php include '_trackers/head.php';?>     
<style type="text/css">
/* Slider */
/* line 41, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider {
  position: relative;
  display: block;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -ms-touch-action: pan-y;
  touch-action: pan-y;
  -webkit-tap-highlight-color: transparent;
}

/* line 57, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-list {
  position: relative;
  overflow: hidden;
  display: block;
  margin: 0;
  padding: 0;
}
/* line 64, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-list:focus {
  outline: none;
}
/* line 68, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-loading .slick-list {
  background: white url("../_images/common/ajax-loader.gif") center center no-repeat;
}
/* line 72, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-list.dragging {
  cursor: pointer;
  cursor: hand;
}

/* line 78, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slick-track {
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
  -o-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
}

/* line 86, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-track {
  position: relative;
  left: 0;
  top: 0;
  display: block;
}
/* line 92, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-track:before, .slick-track:after {
  content: "";
  display: table;
}
/* line 98, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-track:after {
  clear: both;
}
/* line 102, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-loading .slick-track {
  visibility: hidden;
}

/* line 107, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slide {
  float: left;
  height: 100%;
  min-height: 1px;
  display: none;
}
/* line 112, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
[dir="rtl"] .slick-slide {
  float: right;
}
/* line 118, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slide.slick-loading img {
  display: none;
}
/* line 124, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slide.dragging img {
  pointer-events: none;
}
/* line 128, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-initialized .slick-slide {
  display: block;
}
/* line 132, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-loading .slick-slide {
  visibility: hidden;
}
/* line 136, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-vertical .slick-slide {
  display: block;
  height: auto;
  border: 1px solid transparent;
}

/* Icons */
@font-face {
  font-family: "slick";
  src: url("../_fonts/slick/slick.eot");
  src: url("../_fonts/slick/slick.eot?#iefix") format("embedded-opentype"), url("../_fonts/slick/slick.woff") format("woff"), url("../_fonts/slick/slick.ttf") format("truetype"), url("../_fonts/slick/slick.svg#slick") format("svg");
  font-weight: normal;
  font-style: normal;
}
/* Arrows */
/* line 156, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev,
.slick-next {
  position: absolute;
  display: block;
  height: 30px;
  width: 30px;
  line-height: 0;
  font-size: 0;
  cursor: pointer;
  background: transparent;
  color: transparent;
  top: 50%;
  margin-top: -10px;
  padding: 0;
  border: none;
  outline: none;
}
/* line 173, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev:hover, .slick-prev:focus,
.slick-next:hover,
.slick-next:focus {
  outline: none;
  background: transparent;
  color: transparent;
}
/* line 178, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev:hover:before, .slick-prev:focus:before,
.slick-next:hover:before,
.slick-next:focus:before {
  opacity: 1;
}
/* line 183, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev.slick-disabled:before,
.slick-next.slick-disabled:before {
  opacity: 0.25;
}

/* line 188, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev:before, .slick-next:before {
  font-family: "slick";
  font-size: 30px;
  line-height: 1;
  color: white;
  opacity: 0.75;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

/* line 198, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev {
  left: -25px;
}
/* line 201, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
[dir="rtl"] .slick-prev {
  left: auto;
  right: -25px;
}
/* line 206, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev:before {
  content: "←";
}
/* line 209, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
[dir="rtl"] .slick-prev:before {
  content: "→";
}

/* line 215, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-next {
  right: -25px;
}
/* line 218, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
[dir="rtl"] .slick-next {
  left: -25px;
  right: auto;
}
/* line 223, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-next:before {
  content: "→";
}
/* line 226, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
[dir="rtl"] .slick-next:before {
  content: "←";
}

/* Dots */
/* line 234, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider {
  margin-bottom: 30px;
}
@media only screen and (max-width: 40em) {
  /* line 234, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider {
    margin-bottom: 0px;
  }
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 234, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider {
    margin-bottom: 0px;
  }
}

/* line 246, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-dots {
  position: absolute;
  bottom: -45px;
  list-style: none;
  display: block;
  text-align: center;
  padding: 0;
  width: 100%;
}
/* line 255, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-dots li {
  position: relative;
  display: inline-block;
  height: 30px;
  width: 30px;
  margin: 0 5px;
  padding: 0;
  cursor: pointer;
}
/* line 264, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-dots li button {
  border: 0;
  background: transparent;
  display: block;
  height: 30px;
  width: 30px;
  outline: none;
  line-height: 0;
  font-size: 0;
  color: transparent;
  padding: 5px;
  cursor: pointer;
}
/* line 277, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-dots li button:hover, .slick-dots li button:focus {
  outline: none;
}
/* line 280, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-dots li button:hover:before, .slick-dots li button:focus:before {
  opacity: 1;
}
/* line 285, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-dots li button:before {
  position: absolute;
  top: 0;
  left: 0;
  content: "•";
  width: 30px;
  height: 30px;
  font-family: "slick";
  font-size: 6px;
  line-height: 30px;
  text-align: center;
  color: black;
  opacity: 0.25;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
/* line 303, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-dots li.slick-active button:before {
  color: black;
  opacity: 0.75;
}

/* line 315, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slide > a img {
  width: 60%;
  max-width: 812px;
}
@media only screen and (max-width: 40em) {
  /* line 315, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slide > a img {
    width: 100% !important;
  }
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 315, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slide > a img {
    width: 80% !important;
  }
}

/* line 329, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-next {
  top: 210px;
  right: 5% !important;
  z-index: 999 !important;
  position: absolute;
}
@media only screen and (max-width: 40em) {
  /* line 329, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-next {
    top: 120px;
  }
}
@media only screen and (min-width: 64.0625em) {
  /* line 329, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-next {
    right: 15% !important;
  }
}

/* line 344, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-prev {
  top: 210px;
  left: 5% !important;
  z-index: 999 !important;
  position: absolute;
}
@media only screen and (max-width: 40em) {
  /* line 344, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-prev {
    top: 120px;
  }
}
@media only screen and (min-width: 64.0625em) {
  /* line 344, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-prev {
    left: 15% !important;
  }
}

/* line 359, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.top-slider {
  top: 60px;
  color: #fff;
  width: 100%;
  overflow: visible;
  height: 100%;
  max-height: 408px;
}

/* line 368, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider {
  height: 100%;
  max-height: 408px;
}
@media only screen and (max-width: 40em) {
  /* line 373, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .hidemobile {
    visibility: hidden;
  }
}
/* line 378, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider a {
  z-index: 0 !important;
  color: white;
  display: block;
  width: 100%;
  height: 100%;
  top: 0 !important;
  position: relative;
  text-align: center;
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide1 {
  background: url("../_images/slider/background/desktop/slide1.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide1 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide1 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide1.jpg");
  }
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide2 {
  background: url("../_images/slider/background/desktop/slide2.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide2 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide2 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide2.jpg");
  }
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide3 {
  background: url("../_images/slider/background/desktop/slide3.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide3 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide3 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide3.jpg");
  }
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide4 {
  background: url("../_images/slider/background/desktop/slide4.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide4 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide4 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide4.jpg");
  }
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide5 {
  background: url("../_images/slider/background/desktop/slide5.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide5 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide5 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide5.jpg");
  }
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide6 {
  background: url("../_images/slider/background/desktop/slide6.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide6 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide6 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide6.jpg");
  }
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide7 {
  background: url("../_images/slider/background/desktop/slide7.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide7 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide7 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide7.jpg");
  }
}
/* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-slider .slide8 {
  background: url("../_images/slider/background/desktop/slide8.jpg");
  background-position: center !important;
  background-repeat: no-repeat !important;
  max-width: 2560px;
  height: 100%;
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide8 {
    background-size: 260% !important;
  }
}
@media only screen and (max-width: 40em) {
  /* line 391, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide8 {
    background-size: 100% !important;
    background: url("../_images/slider/foreground/mobile/slide8.jpg");
  }
}
@media only screen and (min-width: 64.0625em) {
  /* line 413, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-slider .slide1 {
    background: transparent;
  }
}

/* line 420, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slick-container {
  margin-top: -1px;
}
@media only screen and (max-width: 40em) {
  /* line 420, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slick-container {
    margin-top: -3px !important;
  }
}

/* line 428, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slider-text-link {
  position: absolute;
  display: block;
  padding: 8px;
  padding-top: 2px;
  padding-bottom: 3px;
  background: rgba(0, 0, 0, 0.3);
  border-radius: 3px;
  position: absolute;
  right: 24%;
  transform: translateX(-50%);
  bottom: 18%;
  font-size: 11px;
  line-height: 16px;
  text-decoration: underline;
  font-weight: bold;
}
@media only screen and (max-width: 40em) {
  /* line 428, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slider-text-link {
    right: 31%;
    bottom: 9%;
  }
}
@media only screen and (min-width: 40.0625em) and (max-width: 64em) {
  /* line 428, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slider-text-link {
    right: 10%;
    bottom: 4%;
  }
}
@media only screen and (min-width: 90.0625em) {
  /* line 428, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
  .slider-text-link {
    right: 26%;
    bottom: 21%;
  }
}

/* line 461, ../../../../_sass-compass-foundation/_responsive.spinandwin/scss/partials/modules/user-interface/_slick-slide.scss */
.slider-text-link a {
  color: #FBFBFB;
  font-weight: bold;
}

</style>

    <?php echo $this->seo_meta;?>

    <link rel="stylesheet" href="/_global-library/js/vendor/intlTelInput/css/intlTelInput.css<?php echo "?v=" . config("Version"); ?>" />
    <link rel="stylesheet" href="/_css/main.css<?php echo "?v=" . config("Version"); ?>" />


    <link href="https://plus.google.com/+Spinandwin" rel="publisher" />

    <script src="/_global-library/js/bower_components/modernizr/modernizr.min.js"></script>

    <script type="application/javascript">
        // sbm initialization
        var skinModules = [], skinPlugins = [];
        // common modules
        skinModules.push({
            id: "Notifier",
            options: {
                global: true
            }
        });
        skinModules.push({
            id: "GameLauncher",
            options: {
                modalSelector: "#games-info",
                global: true
            }
        });
        skinModules.push({
            id: "GameWindow",
            options: {
                global: true
            }
        });
        skinModules.push({
            id: "Favourites",
            options: {
                imgFavouriteOn: "/_images/common/icons/favourite-icon--on.png",
                imgFavouriteOff: "/_images/common/icons/favourite-icon--off.png",
                noFavIconsWeb: 6,
                noFavIconsTablet: 3,
                noFavIconsPhone:2,
                global: true
            }
        });
        skinModules.push( { 
            id: "MailInbox",
            options: {
                global: true
            }
        });
        skinModules.push( {
            id: "TrackNavigation",
            options: {
                global: true
            }
        });
        skinModules.push({
            id: "ContentScheduler",
            options: {
                global: true
            }
        });
        // common plugins
        skinPlugins.push({
            id: "Global",
            options: {
                global: true
            }
        });
        skinPlugins.push({
            id: "MobileMenu",
            options: {
                global: true
            }
        });
        skinPlugins.push({
            id: "Tables",
            options: {
                global: true
            }
        });
        skinPlugins.push({
            id: "Slick",
            options: {
                global: true,
                element: 'bxslider-head'
           }
        });
        //Prize Wheel
       skinModules.push({
            id: "PrizeWheel",
            options: {
                 global: true
            }
       });
		skinModules.push({
			id: "CMANotifications",
			options: {
				global: true
			}
		});
    </script>

     <!-- Browser not supported redirect -->
     <?php include "_global-library/partials/common/user-agent-sniffer.php"; ?>    
    <!-- SERVER SIDE AND DYNAMIC STYLES -->
    <style type="text/css">
        <?php
            echo "/*device: " . config("Device") . 'logged: ' . logged() . "*/\n";
        ?>
        body {
            background-image: url('/_images/slider/background/desktop/slide1.jpg');
        }

    </style>
    <!-- /SERVER SIDE AND DYNAMIC STYLES -->

    <!-- SEO Social -->
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "King Jack Casino",
  "url" : "https://www.kingjackcasino.com/",
  "sameAs" : [ "https://www.facebook.com/kingjackcasino/",
    "https://twitter.com/kingjackcasino",
    "https://plus.google.com/110790120289551582137"] 
} 
</script>
    <!-- / SEO Social -->



                <?php if(Env::isLive()):?>
       <!-- Start Spin and Win Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=271305,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
is_spa = 1,
/* DO NOT EDIT BELOW THIS LINE */
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&f='+(+is_spa)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->
                    <link rel="manifest" href="/manifest.json"/>
        <?php endif;?>

    <link rel="manifest" href="/manifest.json"/>



<meta name="msapplication-TileColor" content="#6F36A1">  
<meta name="msapplication-TileImage" content="/_images/common/ms-icon.png">
<meta name="theme-color" content="#6F36A1">

</head>

<body id="<?php echo $this->controller; ?>">
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WNZVCNZ"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager -->
<?php if(Env::isLive()):?>
<!-- new relic -->
<?php include "_global-library/trackers/new-relic/king-jack-casino.php"; ?>
<!-- /relic -->
<script type="text/javascript">var rumMOKey='d8e9375881b0765420d8ce6da1523300';(function(){if(window.performance && window.performance.timing && window.performance.navigation) {var site24x7_rum_beacon=document.createElement('script');site24x7_rum_beacon.async=true;site24x7_rum_beacon.setAttribute('src','//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey='+rumMOKey);document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);}})(window)</script>  
<?php endif;?>
<?php include '_trackers/header.php';?> 

