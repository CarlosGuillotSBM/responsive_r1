<script type="application/javascript">

window.setTimeout(function () {
	var el = document.getElementById("ui-first-loader");
	if (el) el.parentNode.removeChild(el);
	el = document.getElementById("ui-loader-animation");
	if (el) el.style.display = "none";
},1000);

</script>


<!-- OFF CANVAS -->
<!-- This is the Foundation side nav, everything should be wrapped inside off-canvas-wrap -->
<!-- OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->
<div class="main-wrap off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">
		<!-- Inner-wrap. It is necesary for foundation off canvas nav -->

		<!-- header  -->
		<header data-bind="css: { 'header--logged-in' :validSession }" class="header header--logged-<?php echo logged(); ?> clearfix">
			<!-- COOKIE POLICY -->
			<?php include "_global-library/partials/modals/cookie-policy.php" ?>

			<div class="header__inner">
				<div class="header__left">
					<?php include "_partials/logo/logo.php" ?>
				</div>

				<div class="header__middle">
					<div class="header__row">
						<?php include "_partials/nav-primary/nav-primary.php" ?>
					</div>
					<div class="header__row">
						<ul class="btn-group">
							<li class="btn-group__item">
								<div class="ui-game-filter-btn">
									<?php include "_global-library/partials/game-filter-menu/game-filter-menu.php";?>
								</div>
							</li>
							<li class="btn-group__item">
								<?php include "searchbox.php" ?>
							</li>
						</ul>
					</div>
				</div>

				<div class="header__right">
					<div class="header__row">
						<?php include "_partials/nav-secondary/nav-secondary.php"; ?>
					</div>
					<div class="header__row">
						<?php include "_partials/login-nav/login-nav.php"; ?>
						<?php include "_partials/user-nav/user-nav.php"; ?>
						<div>
							<?php include "_partials/balance-area/balance-area.php"; ?>
						</div>
					</div>
				</div>
			</div>
		</header>

		<!-- header for tablet and mobile -->
		<!-- no semantinc tags to avoid document outline problems -->
		<div>
			<!-- nav for mobile -->
			<?php include "_partials/nav-for-mobile/nav-for-mobile.php"; ?>
			<!-- / nav for mobile -->
		</div>
		<div class="search-screen-wrap">
			<!-- search screen for mobile -->
			<?php include "_partials/search-screen/search-screen.php"; ?>
			<!-- / search screen for mobile -->
		</div>
		<!-- sticky navigation  -->
		<div style="display: none" id="ui-sticky-games-navigation" class="sticky games-navigation middle-content__box">
			<div class="games-navigation__inner">
				<div class="games-navigation__left">

					<!-- nav for desktop -->
					<?php include "_partials/games-navigation/games-navigation.php"; ?>
					<!-- / nav for desktop -->
					<!-- game filter menu -->

					<?php include "_global-library/partials/game-filter-menu/game-filter-menu.php" ?>

					<!-- / game filter menu -->
				</div>
				<div class="games-navigation_right">
					<?php include "_global-library/partials/search/search-input.php" ?>
				</div>
			</div>
		</div>
		<!-- /sticky navigation  -->
		<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" id="fullpage" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>">
			<!-- slider -->
			<!-- 	<?php include "_partials/common/slick-slider.php"; ?> -->
			<!-- slider -->
			<!-- games-navigation -->
			<div id="ui-games-navigation" class="games-navigation middle-content__box">
				<div class="games-navigation__inner">
					<div class="games-navigation__left">


						<!-- nav for desktop -->
						<?php include "_partials/games-navigation/games-navigation.php"; ?>
						<!-- / nav for desktop -->
						<!-- game filter menu -->
						<div class="cat-search-bar__categories ui-game-filter-btn games-filter-menu"><?php include "_global-library/partials/game-filter-menu/game-filter-menu.php";?></div>


						<!-- / game filter menu -->
					</div>
					<div class="games-navigation_right">

						<?php include "_global-library/partials/search/search-input.php" ?>
					</div>
				</div>
			</div>

			<!-- /games-navigation -->


			<?php
        	include("_global-library/widgets/prize-wheel/prize-wheel-notification.php");
    		?>
			<!-- AJAX CONTENT WRAPPER -->
			<div id="ajax-wrapper">
