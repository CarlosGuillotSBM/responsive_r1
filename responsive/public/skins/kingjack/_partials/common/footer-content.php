<!-- FOOTER AREA -->
<div class="clearfix"></div>
<footer id="footer" class="footer">
     <div class="footer__row">
          <div class="footer__row__left">
               <ul class="footer__nav">
                    <li><a href="/about-us/">About</a></li>
                    <li>|</li>

                    <li><a href="/support/">Support</a></li>
                    <li>|</li>
                    <li><a href="/faq/">FAQ </a></li>
                    <li>|</li>
                    <li><a href="/banking/">Banking </a></li>
                    <li>|</li>
                    <li><a href="/terms-and-conditions/">Terms &amp; Conditions</a></li>
                    <li>|</li>


                    <li><a href="/privacy-policy/">Privacy </a></li>
                    <li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none">|</li>
                    <li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none"><a  href="/cashier/?settings=1">Player Settings </a></li>
                    <li >|</li>
                    <li><a href="/responsible-gaming/">Responsible Gaming </a></li>
                    <li>|</li>
                    <li><a href="/complaints-and-disputes/">Complaints and Disputes </a></li>
                    <li>|</li>
                    <li><a  href="https://www.luckyjar.com" data-hijack="false">Affiliates </a></li>
                    <li>|</li>
                    <li><a  href="/archive/" data-hijack="false">Archive </a></li>
               </ul>

               <ul class="footer__nav social-share-links">
                    <li>
                         <a href="https://www.facebook.com/kingjackcasino" target="_blank">
                         <img src="/_images/common/social-icons/facebook.png">
                         </a>
                    </li>
                    <li>
                         <a href="https://twitter.com/kingjackcasino" target="_blank">
                         <img src="/_images/common/social-icons/twitter.png">
                         </a>
                    </li>
               </ul>

          </div>

     </div>
     <div class="footer__row">
          <ul class="footer__row__payments">
               <li><img src="/_images/common/payment-icons/paypal-icon.svg"></li>
               <li><img src="/_images/common/payment-icons/maestro-icon.svg"></li>
               <li><img src="/_images/common/payment-icons/mastercard-icon.svg"></li>
               <li><img src="/_images/common/payment-icons/visa-icon.svg"></li>
               <li><img src="/_images/common/payment-icons/neteller-icon.svg"></li>
               <li><img src="/_images/common/payment-icons/paysafecard-icon.svg"></li>
               <li><a href="/terms-and-conditions/"><img src="/_images/common/payment-icons/18-icon.svg"></a></li>
               <li>
                    <a target="_blank" data-hijack="false" href="http://www.gamcare.org.uk/">
                         <img src="/_images/common/payment-icons/gamcare-icon.svg">
                    </a>
               </li>
               <li>
                    <a target="_blank" data-hijack="false" href="https://www.gamstop.co.uk" rel="nofollow">
                         <img src="/_images/common/payment-icons/gamstop.svg">
                    </a>
               </li>
               <li><a target="_blank" data-hijack="false" href="http://www.gamblingcontrol.org/"><img src="/_images/common/payment-icons/alderney.svg"></a></li>
               <li><a target="_blank" data-hijack="false" rel="nofollow" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022"><img src="/_images/common/payment-icons/gambling-commision.svg"></a></li>
               <li><a target="_blank" data-hijack="false" href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&lng=EN"><img src="/_images/common/payment-icons/ecogra.svg"></a></li>
          </ul>
     </div>
     <div class="footer__row">
          <div class="footer__row__legal">
               <p>King Jack Casino is licensed and regulated to offer Gambling Services in Great Britain by the UK Gambling Commission, license Number 000-039022-R-319427-004. All the games offered on the website have been approved by the UK Gambling Commission. Details of its current licensed status as recorded on the Gambling Commission's website can be found <a  href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022" target="_blank" data-hijack="false">here.</a></p>

               <p>King Jack Casino is also licensed and regulated by the Alderney Gambling Control Commission, License Number: 71 C1, to offer Gambling facilities in jurisdictions outside Great Britain. Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3 UL.</p>
          </div>
     </div>
</footer>
<!-- END FOOTER AREA -->
