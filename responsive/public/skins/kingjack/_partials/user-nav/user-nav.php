<ul style="display:none" data-bind="visible: validSession()" class="user-nav">
	<li id="balance" class="user-nav__item">
		<a href="/cashier/" data-hijack="true" class="user-nav__link link-balance">
			Balance:
			<span class="link-balance__value" data-bind="text: balance"></span>
		</a>
	</li>

	<li id="cashier" class="user-nav__item">
    <a href="/cashier/" data-hijack="true" class="user-nav__link link-deposit">Deposit</a>
  </li>
</ul>