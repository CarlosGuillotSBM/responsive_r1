<section class="selling-points section middle-content__box ">
	<?php edit($this->controller,'selling-points'); ?>
	<div class="selling-points__container">



		<div class="selling-points__container__row">

			<div class="selling-points__item">
				<div class="selling-points-image">
					<img src="/_images/selling-points/moolah-icon.svg">
				</div>
				<div class="selling-points-content">
					<?php @$this->getPartial($this->content['selling-points'],1); ?>
				</div>
			</div>

			<div class="selling-points__item">
				<div class="selling-points-image">
					<img src="/_images/selling-points/diamond-icon.svg">
				</div>
				<div class="selling-points-content">
					<?php @$this->getPartial($this->content['selling-points'],2); ?>
				</div>
			</div>
		</div>




		<div class="selling-points__container__row">
			<div class="selling-points__item">
				<div class="selling-points-image">
					<img src="/_images/selling-points/exchange-icon.svg">
				</div>
				<div class="selling-points-content">
					<?php @$this->getPartial($this->content['selling-points'],3); ?>
				</div>
			</div>

			<div class="selling-points__item">
				<div class="selling-points-image">
					<img src="/_images/selling-points/question-icon.svg">
				</div>
				<div class="selling-points-content">
					<?php @$this->getPartial($this->content['selling-points'],4); ?>
				</div>
			</div>
		</div>

		<div class="selling-points__container__row">
			<div class="selling-points__item">
				<div class="selling-points-image">
					<img src="/_images/selling-points/cup-icon.svg">
				</div>
				<div class="selling-points-content">
					<?php @$this->getPartial($this->content['selling-points'],5); ?>
				</div>
			</div>

			<div class="selling-points__item">
				<div class="selling-points-image">
					<img src="/_images/selling-points/clubs-icon.svg">
				</div>
				<div class="selling-points-content">
					<?php @$this->getPartial($this->content['selling-points'],6); ?>
				</div>
			</div>
	</div>

	<div class="selling-points__container__row">
		<div class="selling-points__container__button">
			<a data-gtag="Join Now,Selling Points" href="/register/" class="green-cta expand">JOIN NOW</a>
		</div>
	</div>

</div>
</section>