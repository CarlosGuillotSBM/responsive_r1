<nav class="nav nav--primary nav--primary--<?php echo $this->controller; ?>">
  <ul class="nav__list nav--primary__list">
    <li class="nav__item" data-bind="visible: !validSession()" id="home">
      <a class="nav__link" data-hijack="true" href="/">home</a>
    </li>
    <li class="nav__item" style="display: none" data-bind="visible: validSession()" id="home">
      <a class="nav__link" data-hijack="true" href="/start/">Dashboard</a>
    </li>
    <li class="nav__item" id="games">
      <a class="nav__link" data-hijack="true" href="/games/">All Games</a>
    </li>
    <li class="nav__item" id="slots">
      <a class="nav__link" href="/slots/" data-hijack="true">Slots</a>
    </li>
    <li class="nav__item" id="live-casino">
      <a class="nav__link" href="/live-casino/" data-hijack="true">Live Casino</a>
    </li>
    <!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
    <li class="nav__item u-hidden" id="sports">
      <a class="nav__link" href="/sports/" data-hijack="true">Sports</a>
    </li>
    <!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
    <li class="nav__item" id="promotions">
      <a class="nav__link" data-hijack="true" href="/promotions/">Promos</a>
    </li>
  </ul>
</nav>