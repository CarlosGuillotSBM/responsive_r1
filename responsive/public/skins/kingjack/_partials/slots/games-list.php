<script type="application/javascript">
	skinModules.push({
		id: "GamesDisplay",
		options: {
			defaultMode: "icons"
		}
	});
</script>


<?php
do {
	if(count($this->games) > 0) {
?>


	<section class="<?php echo @$this->category["Class"];?> games-list middle-content__box">
		<div class="section__header hideme">
			<h1 class="section__title">
				<?php echo $this->getCategoryHref();?>

				<ul class="section__action">
					<li class="section__action__item">
						<span class="section__header__viewall section__action__viewall">
							<?php echo $this->getCategoryHref('View All');?>
						</span>
					</li>
					<li class="section__action__item">
						<span class="section__action__toggle toggle-list-view games-text-switcher" title="List view" data-bind="click: GamesDisplay.toggleMode" data-mode="list">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-286 409.4 23 23" xml:space="preserve">
								<g id="Welcome" class="st0">
									<g id="Games-_x28_Desktop_x29_" transform="translate(-931.000000, -246.000000)">
										<g id="featured-games" transform="translate(46.000000, 236.000000)">
											<g id="text-icon" transform="translate(886.000000, 10.000000)">
												<rect x="-286.5" y="413.9" class="st1" width="12.5" height="2"/>
												<rect x="-286.5" y="419.9" class="st1" width="22" height="2"/>
												<rect x="-286.5" y="425.9" class="st1" width="22" height="2"/>
											</g>
										</g>
									</g>
								</g>
							</svg>
						</span>
					</li>
					<li class="section__action__item">
						<span class="section__action__toggle toggle-grid-view active games-text-switcher" title="Grid view" data-bind="click: GamesDisplay.toggleMode" data-mode="grid">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-288 411.9 19 18" xml:space="preserve">
								<rect id="rect-3" x="-288" y="411.9" class="st0" width="8" height="7"/>
								<rect id="rect-4" x="-288" y="421.9" class="st0" width="8" height="8"/>
								<rect id="rect-3_1_" x="-277" y="411.9" class="st0" width="8" height="7"/>
								<rect id="rect-4_1_" x="-277" y="421.9" class="st0" width="8" height="8"/>
							</svg>
						</span>
					</li>
				</ul>
			</h1>
		</div>

		<!-- ICON VIEW -->

		<div data-bind="visible: GamesDisplay.displayMode() === 'icons'" class="games-list__grid">
	        <?php $i = 1; foreach($this->games as $row){ ?>
				<!-- item -->
				<div class="games-list__grid__child ui-parent-game-icon" data-position="<?php echo $i ?>" data-containerkey="<?php echo $this->category['Key']; ?>">
					<?php include '_global-library/_editor-partials/game-icon.php'; ?>
				</div>
				<!-- /item -->
			<?php $i++; }?>
		</div>

		<!-- TEXT VIEW-->
		<div style="display: none" data-bind="visible: GamesDisplay.displayMode() === 'text'" class="games-list__grid">
			<?php include '_global-library/partials/games/games-text-display.php' ?>
		</div>

	</section>

 <?php }

} while ($this->GetNextCategory());?>


