
<!-- game menu container -->
<div class="games-filter-menu__popover game-filter-menu ui-game-filter-menu">
	<span class="nub"></span>
	<dl class="tabs ui-filter-tabs" data-tab>
		<dd id="ui-features2" class="active"><a href="#panel2_">Features</a></dd>
		<dd id="ui-paylines2"><a href="#panel3_">Paylines</a></dd>
		<dd id="ui-themes2"><a href="#panel1_">Themes</a></dd>
	</dl>
	<div class="tabs-content">
		<div class="content" id="panel1_">
			<ul>
				<li><a id="ui-jackpot-slots" data-gtag="Game Menu,Themes+Jackpot Slots" href="/slots/jackpot-slots/" data-hijack="true">Jackpot Slots</a></li>
				<li><a id="ui-magic-myth" data-gtag="Game Menu,Themes+Magicand Myth" href="/slots/magic-and-myth/" data-hijack="true">Magic and Myth</a></li>
				<li><a id="ui-animals-nature" data-gtag="Game Menu,Themes+Animals and Nature" href="/slots/animals-and-nature/" data-hijack="true">Animals and Nature</a></li>
				<li><a id="ui-movie-adventure" data-gtag="Game Menu,Themes+Movie and Adventure" href="/slots/movie-and-adventure/" data-hijack="true">Movie and Adventure</a></li>
				<li><a id="ui-egyptian" data-gtag="Game Menu,Themes+Eygptian" href="/slots/egyptian/" data-hijack="true">Eygptian</a></li>
				<li><a id="ui-pirate-treasures" data-gtag="Game Menu,Themes+Pirates and Treasures" href="/slots/pirates-and-treasure/" data-hijack="true">Pirates and Treasure</a></li>
				<li><a id="ui-exclusive" data-gtag="Game Menu,Themes+Exclusive" href="/slots/exclusive/" data-hijack="true">Exclusive</a></li>
				<li><a id="ui-new" data-gtag="Game Menu,Themes+New" href="/slots/new/" data-hijack="true">New</a></li>
				<li><a id="ui-featured" data-gtag="Game Menu,Themes+Featured" href="/slots/featured/" data-hijack="true">Featured</a></li>

			</ul>
		</div>
		<div class="content active" id="panel2_">
			<ul>
			<li><a id="ui-all-slots" data-gtag="Game Menu,Themes+Featured" href="/slots/" data-hijack="true">All Slots</a></li>
				<li><a id="ui-free-spins" data-gtag="Game Menu,Features+Free Spins" href="/slots/exclusive/" data-hijack="true">Free Spins</a></li>
				<li><a id="ui-bonus-round" data-gtag="Game Menu,Features+Bonus Round" href="/slots/bonus-round/" data-hijack="true">Bonus Round</a></li>
				<li><a id="ui-tumbling-reels" data-gtag="Game Menu,Features+Tumbling Reels" href="/slots/3-reel-slots/" data-hijack="true">3 Reel</a></li>
				<li><a id="ui-multiways-extra" data-gtag="Game Menu,Features+Multiways Extra" href="/slots/5-reel-slots/" data-hijack="true">5 Reel</a></li>
				<li><a id="ui-stacked-wilds" data-gtag="Game Menu,Features+Stacked Wilds" href="/slots/stacked-wilds/" data-hijack="true">Stacked Wilds</a></li>

				<li><a id="ui-click-me" data-gtag="Game Menu,Features+Click Me" href="/slots/click-me/" data-hijack="true">Click Me</a></li>

						<li><a id="ui-table-cards" data-gtag="Game Menu,Themes+Featured" href="/table-card/" data-hijack="true">Table & Cards</a></li>
						<li><a id="ui-scratchcards" data-gtag="Game Menu,Themes+Featured" href="/scratchcards/" data-hijack="true">Scratch Cards</a></li>
			</ul>
		</div>
		<div class="content" id="panel3_">
			<ul>
				<li><a id="ui-10-less" data-gtag="Game Menu,Paylines+10 & Less" href="/slots/10-and-less-line-slots/" data-hijack="true">10 & Less</a></li>
				<li><a id="ui-15" data-gtag="Game Menu,Paylines+15" href="/slots/15-line-slots/" data-hijack="true">15</a></li>
				<li><a id="ui-20" data-gtag="Game Menu,Paylines+20" href="/slots/20-line-slots/" data-hijack="true">20</a></li>
				<li><a id="ui-25" data-gtag="Game Menu,Paylines+25" href="/slots/25-line-slots/" data-hijack="true">25</a></li>
				<li><a id="ui-30" data-gtag="Game Menu,Paylines+30" href="/slots/30-line-slots/" data-hijack="true">30</a></li>
				<li><a id="ui-40" data-gtag="Game Menu,Paylines+40" href="/slots/40-line-slots/" data-hijack="true">40</a></li>
				<li><a id="ui-50-over" data-gtag="Game Menu,Paylines+50 & Over" href="/slots/50-and-more-line-slots/" data-hijack="true">50 & Over</a></li>
			</ul>  </div>
		</div>
	</div>
