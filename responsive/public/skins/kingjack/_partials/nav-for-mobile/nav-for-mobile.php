<!-- Mobile Top Bar -->
<div class="tab-bar nav-for-mobile clearfix">
	<!-- COOKIE POLICY -->
	<?php include "_global-library/partials/modals/cookie-policy.php" ?>
	<!-- logged out -->
	<a data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login-button--nav-for-mobile menu-link" href="/login/">Login</a>
	<!-- /logged out -->
	
	<!-- logged in -->
	<div style="display: none" data-bind="visible: validSession" class="small--balances">
			<!-- <a class="right-off-canvas-toggle icons__balance-white"></a> -->
			<!-- =================== Cashier  ================= -->
			<a class="icons__deposit cashier--placeholder" data-hijack="true" href="/cashier/" data-gtag="Mobile Menu Cashier Click"></a>
			
	</div>
		<!--/ logged in -->
		<h1 class="seo-logo image-replacement">
		<a data-hijack="true" href="/" class="site-logo ui-go-home-logo menu-link">
		<img src="/_images/logo/logo_onesizefitsall.svg" alt="King Jack Casino Logo" style="display: none;">
		</a>
		<span style="display:none;">King Jack Casino New online casino sites on mobile and desktop.</span>
	</h1>

	<!-- Start of navigation toggle -->
	<div class="menu-toggle-wrap">
		<div class="burger">
			<div class="span"></div>
		</div>
	</div>
</div>
<!-- End of navigation toggle -->
<!-- New Mobile Menu April 2015 Author: Marcus Chadwick -->
<nav class="mobile-menu" style="opacity: 1">
	<div class="cta-bottom-wrap">
		<div class="cta-bottom">
			<!-- Logged in VIP Level Box -->
			<div style="display: none" class="mobile-menu-top-area__vip-level" data-bind="visible: validSession()">

					<a href="/loyalty/vip-club/" class="vip--level--wrapper">
					<span> your status: </span><span class="vip-shape" data-bind="text: vipLevel(), css: 'level-' + playerClass()"></span><span class="vip-level-area-icon"></span>
					</a>

			</div>
		</div>
		<!-- 		<div class="mobile-menu-top-area__row">
			<?php// include "_global-library/partials/search/search-input.php" ?>
		</div> -->
	</div>

	<!-- =================== Cashier  ================= -->
	<!-- <div class="cashier-link cashier-buttonstyle" data-bind="visible: validSession()">
		<a class="menu-link" data-hijack="true" href="/cashier/" title="Cashier">Cashier</a>
	</div> -->

	<div style="display: none" class="mobile-menu-top-area__balance  " data-bind="visible: validSession()">
		<div class="inner balance-bg">
			Balance<span data-bind="text: balance">£00000.00</span>
		</div>
	</div>


	<div class="mobile-menu-lists">
	<!-- //removed "ripple" class -->
		<!-- menu lists -->
		<!-- Logged in Balance Box -->

		<ul class="mobile-menu-list">
			<!-- section title -->
			<!-- <li class="mobile-menu-section-title">Games</li> -->
			<!-- menu items-->
			<!-- commented out search link while feature is finalized -->
			<!-- menu items-->


			<!-- apears when logged in -->
			<li class="mobile-menu-item  dash-link" data-bind="visible: validSession()">
				<a class="menu-link" data-hijack="true" href="/start/" title="Start">Dashboard</a>
			</li>


			<!-- apears when logged in -->
			<li class="mobile-menu-item myaccount-link" data-bind="visible: validSession()">
				<a class="menu-link" data-hijack="true" href="/my-account/" title="My Account">My Account</a>
			</li>

			<!--
						<li class="mobile-menu-item favourites-link" data-bind="visible: validSession()">
								<a class="menu-link" data-hijack="true" href="/my-favourites/" title="Favourite Games">My Favourites</a>
			</li> -->



			<!-- apears when logged out -->
			<!--JOIN NOW top Burger menu-->
			<div class="cta-top-wrap">
				<div class="cta-top">
					<div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
						<div class="inner">
							<a class="menu-link" href="/register/">Join Now</a>
						</div>
					</div>
				</div>
			</div>
			<li class="mobile-menu-item  home-link "  style="display: none" data-bind="visible: !validSession()">
				<a  class="menu-link" data-hijack="true" href="/" title="Home">Home</a>
			</li>

			<li class="mobile-menu-item allgames-link">
				<a class="menu-link" data-hijack="true" href="/games/" title="All Games">All Games</a>
			</li>

			<!-- <li class="mobile-menu-item newgames-link">
					<a class="menu-link" data-hijack="true" href="/slots/new/" title="New Games">New Games</a>
			</li> -->

			<li class="mobile-menu-item slots-link has-submenu">
				<a id="ui-slots-menu-link">Slots</a>

				<span class="mobile-menu-item__submenu-toggle" data-submenu="slots">
					By type
					<svg class="chevron-icon" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
						<path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#ffffff">
						</path>
					</svg>
				</span>

				<div class="mobile-menu-item__submenu">
					<div class="mobile-menu-item__submenu__content">
						<div class="games-filter-menu__popover-wrap">
							<div class="games-filter-menu__popover game-filter-menu" align="center">
								<span class="nub"></span>
								<!-- include game categories-->
								<?php include "_global-library/partials/game-filter-menu/game-filter-categories.php" ?>
							</div>
						</div>
					</div>
				</div>
			</li>

			<li class="mobile-menu-item live-casino has-submenu">
				<a class="menu-link" data-hijack="true" href="/live-casino/" title="Live Casino">
					Live Casino
				</a>

				<span class="mobile-menu-item__submenu-toggle" data-submenu="live-casino">
					Categories
					<svg class="chevron-icon" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
						<path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#ffffff">
						</path>
					</svg>
				</span>

				<div class="mobile-menu-item__submenu">
					<div class="mobile-menu-item__submenu__content">
						<?php include "_global-library/partials/live-casino/live-casino-categories.php" ?>
					</div>
				</div>
			</li>

			<!-- Start: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->
			<li class="mobile-menu-item sports-link u-hidden">
				<a class="menu-link" data-hijack="true" href="/sports/" title="Promos">Sports</a>
			</li>
			<!-- End: Todo: Unhide sports link when confirmed by removing class="u-hidden" -->

			<li class="mobile-menu-item gamesearch-link">
				<a id="ui-search-screen" class="menu-link"  title="Search Games">Search Games</a>
			</li>

			<!-- <li class="mobile-menu-item newgames-link">
					<a class="menu-link" data-hijack="true" href="/slots/new/" title="New Games">New Games</a>
			</li> -->


		<!-- <li class="mobile-menu-item live-casino">
						<a class="menu-link" data-hijack="true" href="/live-casino/" title="Live Casino">Live Casino</a>
		</li> -->
		<!-- <li class="mobile-menu-item tablecard-link">
						<a class="menu-link" data-hijack="true" href="/table-card/" title="Table & Card">Table &amp; Card</a>
		</li> -->
		<!-- <li class="mobile-menu-item roulette-link">
						<a class="menu-link" data-hijack="true" href="/roulette/" title="Roulette">Roulette</a>
		</li> -->
		<!-- 				<li class="mobile-menu-item scratch-and-arcade-link">
			<a class="menu-link" data-hijack="true" href="/scratch-and-arcade/" title="Scratch &amp; Arcade">Scratch &amp; Arcade</a>
		</li> -->

		<!-- /menu items-->
		<!-- section title -->
		<!-- <li class="mobile-menu-section-title">Rewards</li> -->
		<!-- menu items-->
		<li class="mobile-menu-item promotions-link">
			<a class="menu-link" data-hijack="true" href="/promotions/" title="Promos">Promos</a>
		</li>
		<li class="mobile-menu-item loyalty-link">
			<a class="menu-link" data-hijack="true" href="/loyalty/vip-club/" title="Loyalty">Loyalty</a>
		</li>
		<li class="mobile-menu-item community-link">
			<a class="menu-link" data-hijack="true" href="/community/winners/" title="Community">Winners</a>
		</li>
		<!-- /menu items-->


		<!-- /menu items-->
		<!-- section title -->
		<li class="nestedmenu-item-expand--parent">
			<dl id="ui-promo-tcs" class="accordion accordion_login" data-accordion="">
				<dd class="accordion-navigation">
				<a  class="moreinfo-menu-link" href="#panelTC2">information</a>
				<div id="panelTC2" class="content">
					<ol>
						<!-- menu items-->

						<!-- <li class="mobile-menu-item community-link">
								<a class="menu-link" data-hijack="true" href="/community/" title="Community">Community</a>
						</li> -->

						<li class="mobile-menu-item faq-link">
							<a class="menu-link" data-hijack="true" href="/faq/" title="Faq">FAQ</a>
						</li>

						<li class="mobile-menu-item support-link">
							<a class="menu-link" href="/support/" title="Support">Support</a>
						</li>


							<li class="mobile-menu-item banking-link">
							<a class="menu-link" data-hijack="true" href="/banking/" title="Banking">Banking</a>
						</li>


						<li class="mobile-menu-item terms-link">
							<a class="menu-link" data-hijack="true" href="/terms-and-conditions/" title="Terms & Conditions">Terms &amp; Conditions</a>
						</li>

							<li class="mobile-menu-item privacy-link">
							<a class="menu-link" data-hijack="true" href="/privacy-policy/" title="Privacy Policy">Privacy Policy</a>
						</li>

						<li class="mobile-menu-item responsiblegaming-link">
							<a class="menu-link" data-hijack="true" href="/responsible-gaming/" title="Responsible Gaming">Responsible Gaming</a>
						</li>

						<li class="mobile-menu-item about-us-link">
							<a class="menu-link" data-hijack="true" href="/about-us/" title="About Us">About Us</a>
						</li>

						<li class="mobile-menu-item settings-link" data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none">
							<a class="menu-link" data-hijack="true" href="/cashier/?settings=1" title="Player Settings">Player Settings</a>
						</li>

						<li class="mobile-menu-item complaints-disputes-link">
							<a class="menu-link" data-hijack="true" href="/complaints-and-disputes/" title="Complaints and Disputes">Complaints and Disputes</a>
						</li>

						<li class="mobile-menu-item archive-link">
							<a class="menu-link" data-hijack="true" href="/archive/" title="Archive">Archive</a>
						</li>
					</ol>
				</div>
				</dd>
			</dl>
		</li>
		<!--				<li class="mobile-menu-item gamelimits-link">-->
		<!--					<a class="menu-link" data-hijack="true" href="/game-limits/" title="Game Limits">Game Limits</a>-->
		<!--				</li>-->
		<li class="mobile-menu-item logout-link" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">
			<a class="menu-link" data-hijack="true" title="Log Out" style="display: none" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">Log Out</a>
		</li>

		<!-- /menu items-->
	</ul>
</div>
<!-- / menu lists -->
</nav>
<!-- END New Mobile Menu April 2015 Author: Marcus Chadwick