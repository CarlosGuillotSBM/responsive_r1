<nav class="nav nav--secondary nav--secondary--<?php echo $this->controller; ?>">
  <ul class="nav__list nav--secondary__list">
    <li class="nav__item" id="winners">
      <a class="nav__link" data-hijack="true" href="/community/winners/">Winners</a>
    </li>
    <li class="nav__item" id="loyalty">
      <a class="nav__link" data-hijack="true" href="/loyalty/vip-club/">Loyalty</a>
    </li>
    <li class="nav__item" id="support">
      <a class="nav__link" href="/support/" class="drop-target">Support</a>
    </li>
    <li class="nav__item" style="display: none" data-bind="visible: validSession()" id="my-account" class="user-nav__user-name">
      <a class="nav__link" href="/my-account/" data-hijack="true">My Account</a>
    </li>
    <li class="nav__item" id="logout-button" data-bind="visible: validSession()">
      <a class="nav__link nav__link--logout" data-bind="click: logout" href="" class="logout-button--top-nav" data-gtag="Logout Desktop">Logout</a>
    </li>
  </ul>
</nav>