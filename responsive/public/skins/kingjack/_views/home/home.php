<!-- CONTENT -->
<main>
<!-- OFFER SECTION -->
     <section class="offer">
          <a href="/register" class="offer__image">
               <img src="_images/slider/desktop/200-bonus-sash-hp-banner-desktop.jpg" alt="New online casino site Get casino bonuses on your first deposit">
          </a>
          <a href="/register" class="offer__image offer__image--mobile">
               <img src="_images/slider/mobile/200-bonus-sash-hp-banner-mobile.jpg" alt="New online casino site Get casino bonuses on your first deposit">
          </a>

          <div class="homepage__hero--terms">
                  <?php edit($this->controller,'hero'); ?>
         <?php @$this->getPartial($this->content['hero'],1, "_partials/banner/homepage-hero-banner.php"); ?>
               </div>


     </section>
    <script type="application/javascript">
          skinPlugins.push({
               id: "OwlCarousel"
          });
     </script>
     <!-- FEATURE SLOTS SECTION  -->
     <section class="section logolead  clearfix padding--top--20">
          <h1 class="section__title">Royal Picks</h1>
     </section>
     <ul class="section container feature-slots">
          <?php $notLazy = false; //we can't use lazy display since it applies before the carousel and it turns out a game dissapears ?>
          <!-- Slots We Love  -->
          <li class="vegas-favourites">
               <nav>
                    <h3>slots we love</h3>

                    <a data-hijack="true" class="view-all" href="/slots/">View all</a>
               </nav>
               <?php $editorBlock = 'slots-we-love';
               $slideTemplate = '_global-library/_editor-partials/game-icon-banner.php' ?>

               <?php include '_global-library/partials/common/owl-carousel.php'; ?>
          </li>

          <!-- Vegas Favourites -->
          <li class="rocking-slots">
               <nav>
                    <h3>vegas favourites</h3>


               </nav>
               <?php $editorBlock = 'vegas-favourites';?>
               <?php include '_global-library/partials/common/owl-carousel.php'; ?>
          </li>

          <!-- Hot on the Strip -->
          <li class="new-on-the-strip">
               <nav>
                    <h3>hot on the strip</h3>

                    <!--   <a data-hijack="true" class="view-all" href="/slots/new/">View all</a>
               -->    </nav>
               <?php $editorBlock = 'hot-on-the-strip';?>
               <?php include '_global-library/partials/common/owl-carousel.php'; ?>

          </li>
     </ul>

    <div class="clearfix"></div>

    <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
         <div class="cta-bottom-wrap ">
              <div class="cta-bottom fullwidth">
                   <div  class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                        <div class="inner">
                             <a data-hijack="true" class="menu-link" href="/register/">Join Now</a>
                        </div>
                   </div>
              </div>
         </div>
    </div>

    <!-- CATEGORIES SLOTS SECTION -->
    <div class="categories-slots-wrap">
         <!-- DESKTOP -->
         <ul class="categories-slots">
              <li><a data-hijack="true" href="/slots/"><img src="../_images/home-new/slots-categories/slots.png"></a></li>
              <li><a data-hijack="true" href="/live-casino/"><img src="./_images/home-new/slots-categories/live-casino.png"></a></li>
              <li><a data-hijack="true" href="/table-card/"><img src="./_images/home-new/slots-categories/table-card.png"></a></li>
              <li><a data-hijack="true" href="/roulette/"><img src="./_images/home-new/slots-categories/roulette.png"></a></li>
              <li><a data-hijack="true" href="/scratch-and-arcade/"><img src="./_images/home-new/slots-categories/scratch-arcade.png"></a></li>
         </ul>
    </div>

    <a data-hijack="true" class="view-all footer--design--viewall" href="/games/">View all games</a>

    <div class="bottom-content">
        <div class="seo container seo-content">
             <?php edit($this->controller,'SEO-Title');?>
             <h1><?php $removeTags = true; @$this->repeatData($this->content['SEO-Title'],0,"_global-library/_editor-partials/text-removing-tags.php");?></h1>
             <?php edit($this->controller,'SEO-Content');?>
             <div class="seo-copy sbm-read-more ui-toggle"><?php $removeTags = true; @$this->repeatData($this->content['SEO-Content'],0,"_global-library/_editor-partials/text.php");?></div>
        </div>
    </div>

</main>
<script>
    /*
    JS Alternative
const containers = document.querySelectorAll('.sbm-read-more');
Array.prototype.forEach.call(containers, (container) => {  // Loop through each container
    var p = container.querySelector('p');
    var divh = container.clientHeight;
    console.log(divh);
    while (p.offsetHeight > divh) { // Check if the paragraph's height is taller than the container's height. If it is:
        p.textContent = p.textContent.replace(/\W*\s(\S)*$/, '...'); // add an ellipsis at the last shown space
    }
})
*/
</script>
<!-- / CONTENT -->