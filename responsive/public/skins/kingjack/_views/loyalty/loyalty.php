
<div class="loyaltycrumbs">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
</div>
<!-- MIDDLE CONTENT -->
<div class="middle-content__box loyalty-wrapper">
  <section class="section">
    <h1 class="section__title"><?php echo $this->controller; ?></h1>
    <!-- <div class="content-template"> -->
    <!-- <?php edit($this->controller,'loaylty-intro'); ?> -->
    
    <!-- </div> -->
    <!-- CONTENT -->
    <div class="content-template">
      <!-- two columns -->
      <div class="two-equal-columns-layout">
        <!-- left column -->
        
        <div class="two-equal-columns-layout__left">
          <div class="content-template">
            <?php edit($this->controller,'magical-moolahs-intro'); ?>
            <a href="/loyalty/magical-moolahs/" data-hijack="true">
              <?php @$this->repeatData($this->content['magical-moolahs-intro']);?>
            </a>
          </div>
          
        </div>
        <!-- /left column -->
        <!-- right column -->
        
        <div class="two-equal-columns-layout__right">
          <div class="content-template">
            <?php edit($this->controller,'vip-club-intro'); ?>
            <a href="/loyalty/vip-club/" data-hijack="true">
              <?php @$this->repeatData($this->content['vip-club-intro']);?>
            </a>
          </div>
          
          
        </div>
        <!-- /right column -->
        
        
      </div>
      <!-- /two columns -->
    </div>
    <!-- /CONTENT-->
  </section>

</div>
<!-- END MIDDLE CONTENT -->