<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <script type="application/javascript">
        skinModules.push({
            id: "RadFilter"
        });
    </script>
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->
    <div class="middle-content__box">
        <section class="section">
            <div class="logolead">
                <h1 class="section__title">
                    <?php echo $this->controller; ?>
                </h1>
            </div>
            <!-- PROMOTIONS CONTENT -->
            <div class="promotions">

                <div class="lounge__intro">
                    <?php edit($this->controller,'promotions-intro'); ?>
                    <?php @$this->getPartial($this->content['promotions-intro'],1); ?>
                </div>
                <!-- <div class="promotions__header">
          <div class="promotions__filter">
            <select>
              <option>Filter 1</option>
              <option>Filter 2</option>
              <option>Filter 3</option>
              <option>Filter 4</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div> -->
                <div class="clearfix"></div>

                <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                    <div class="cta-bottom-wrap ">
                        <div class="cta-bottom fullwidth">
                            <div class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                                <div class="inner">
                                    <a data-hijack="true" class="menu-link" href="/register/">Join Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php edit($this->controller,'promotions'); ?>

                <!-- promotions list -->
                <div class="promotions__list">
                    <!-- Promotions repeater -->
                    <!-- Last parameter set up wich one is the featured -->
                    <?php // $this->repeatData($this->content["promotions"],1,''); ?>
                    <?php $this->repeatData($this->content["promotions"],1, "_global-library/partials/promotion/promotion.php"); ?>
                    <!-- /Promotions repeater -->
                    <div class="clearfix"></div>
                </div>
                <!-- /promotions list -->
                <!-- SEO CONTENT -->
                <div class="lounge__intro">
                    <?php edit($this->controller,'promotions-seo-content'); ?>
                    <?php @$this->getPartial($this->content['promotions-seo-content'],1); ?>
                </div>
                <!-- /SEO CONTENT -->
            </div>
            <!-- PROMOTIONS CONTENT -->
        </section>
    </div>
    <!-- END MIDDLE CONTENT BOX-->
</div>

<!-- END MIDDLE CONTENT -->
<div class="bottom-content">
    <?php edit($this->controller,'promotions-footer-seo'); ?>
    <?php @$this->getPartial($this->content['promotions-footer-seo'],1); ?>
</div>

