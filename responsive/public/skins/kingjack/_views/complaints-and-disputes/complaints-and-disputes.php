<!-- MIDDLE CONTENT -->
<div class="middle-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!--/ breadcrumbs -->
  <!-- MIDDLE CONTENT -->
  <div class="middle-content__box">
    <section class="section logolead">
      <h1 class="section__title"><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
      
      <!-- CONTENT -->
      <?php edit($this->controller,'complaints-and-disputes'); ?>
      <div class="content-template">
        <!-- repeatable content -->
        <?php @$this->repeatData($this->content['complaints-and-disputes']);?>
      </div>
      <!-- /CONTENT-->

    </section>
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->