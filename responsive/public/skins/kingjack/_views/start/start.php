<script type="application/javascript">
skinModules.push( { id: "ClaimCashback" } );
skinModules.push( { id: "ClaimUpgradeBonus" } );
</script>
    <!-- Lobby Wrap -->
    <div class="middle-content lobby-wrap">
         <div class="lobby-wrap__content-box copytitle">
             <div class="logolead">
                 <div class="title__icon">
                     <!-- include svg icon partial -->
                     <?php include 'icon-account.php'; ?>
                     <!-- end include svg icon partial -->
                 </div>
                 <div class="title__text">WELCOME <span data-bind="text: username"></span></div>
             </div>
         </div>
        <!-- Lobby Left Column -->
        <div class="lobby-wrap__left">
            <!-- welcome section -->
            <?php  include'_global-library/partials/start/start-welcome-section.php'; ?>
            <!-- my messages section -->
            <?php if(config("inbox-msg-on")): ?>
                <?php  include'_global-library/partials/start/start-my-messages-section.php'; ?>
            <?php endif; ?>
            <!-- featured games section -->
            <?php  include'_global-library/partials/start/start-featured-games-section.php'; ?>
            <!-- my promotions section -->
            <?php  include'_global-library/partials/start/start-my-promotions-section.php'; ?>

        </div>
        <!-- End Left Column -->
        <!-- Right Column -->
        <div class="game-url-seo lobby-wrap__right">
            <div class="two-columns-content-wrap__right">


                <!-- lobby right deposit button -->
                <?php  include'_global-library/partials/start/start-right-deposit-button.php'; ?>
                <!-- lobby claim cashback button -->
                <?php  include'_global-library/partials/start/start-claim-cashback.php'; ?>
                <!-- lobby claim upgrade bonus button -->
                <?php  include'_global-library/partials/start/start-claim-upgrade-bonus.php'; ?>
                <!-- lobby right progressives -->
                <?php  include'_global-library/partials/start/start-right-progressives.php'; ?>
                <!-- lobby yesterdays wins -->
                <?php  include'_global-library/partials/start/start-right-yesterdays-wins.php'; ?>
                <!-- lobby right latest winners -->
                <?php  include'_global-library/partials/start/start-right-latest-winners.php'; ?>
      
            </div>
        </div>
        <!-- End Right Column -->
    </div>
    <!-- End Lobby Wrap -->

<?php 
    // dont miss out popup
    include "_global-library/partials/modals/dont-miss-out.php";
?>