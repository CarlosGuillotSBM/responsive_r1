<!-- MIDDLE CONTENT -->
<div class="middle-content">
     <!-- breadcrumbs -->
     <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
     <!--/ breadcrumbs -->
     <!-- MIDDLE CONTENT -->
     <div class="middle-content__box">
          <section class="section logolead">
               <h1 class="section__title"><?php echo $this->controller; ?></h1>
               <!-- CONTENT -->
               <?php edit($this->controller,'support'); ?>
               <div class="content-template">
                    <!-- repeatable content -->
                    <?php @$this->repeatData($this->content['support']);?>
                    <!-- /repeatable content -->
                    <!-- <article class="text-partial">
                         <h1>Live Chat</h1>
                         <span id="chatClient"></span>
                         <script src="https://supportchannel.playersupport.org/netagent/client/unified/portalsettings.js" language="JavaScript"></script>
                         <script src="https://supportchannel.playersupport.org/netagent/client/unified/includes2/portaldetection.js" language="JavaScript"></script>
                         <script language="JavaScript">DetectDevice("D5E44E14-2BAC-4C7F-8D87-928D29C3238B", "chatClient", "", "");</script>
                    </article> -->
               </div>
               <!-- /CONTENT-->
          </section>
     </div>
     <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->
