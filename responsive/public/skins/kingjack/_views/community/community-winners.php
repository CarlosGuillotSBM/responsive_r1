<?php
	$titleHeaderHeight = 25;
	$slideHeight = 82;
	$slidesToShow = 3;
?>
<script type="application/javascript">
	skinPlugins.push({
		id: "ProgressiveSlider",
		options: {
			slidesToShow: <?php echo $slidesToShow ?>,
			mode: "vertical"
		}
	});
	skinPlugins.push({
		id: "WinnersSlider",
		options: {
			slidesToShow: <?php echo $slidesToShow ?>,
			mode: "vertical"
		}
	});
</script>


<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->
    <!-- MIDDLE CONTENT -->
    <div class="middle-content__box community">
        <section class="section">
            <!-- <h1 class="community-heading">COMMUNITY</h1> -->
            <div class="logolead">
                <h1 class=" section__title community-heading">Winners</h1>
            </div>


            <div class="two-columns-content-wrap">
                <!-- community nav -->

                <?php include "_partials/community-nav/community-nav.php" ?>
                <!-- /community nav -->
                <!-- left column -->
                <div class="two-columns-content-wrap__left">
                    <div class="section__header">
                        <h1 class="section__title">COMMUNITY WINNERS</h1>

                    </div>
                    <?php edit($this->controller,'winners'); ?>
                    <!-- repeatable content -->
                    <?php @$this->repeatData($this->content['winners']);?>
                    <!-- /repeatable content -->


                </div>
                <!--  /left column -->
                <!-- right column -->
            <div class="game-url-seo lobby-wrap__right">
                <div class="two-columns-content-wrap__right">


                    <!-- lobby right deposit button -->
                    <?php // include'_global-library/partials/start/start-right-deposit-button.php'; ?>
                    <!-- lobby claim cashback button -->
                    <?php  //include'_global-library/partials/start/start-claim-cashback.php'; ?>
                    <!-- lobby right progressives -->
                    <?php  include'_global-library/partials/start/start-right-progressives.php'; ?>
                    <!-- lobby yesterdays wins -->
                    <?php  include'_global-library/partials/start/start-right-yesterdays-wins.php'; ?>
                    <!-- lobby right latest winners -->
                    <?php  include'_global-library/partials/start/start-right-latest-winners.php'; ?>

                </div>
            </div>
            <!-- End Right Column -->

            </section>
        </div>
    <!-- END MIDDLE CONTENT BOX-->

    <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
        <div class="cta-bottom-wrap ">
            <div class="cta-bottom fullwidth">
                <div class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                    <div class="inner">
                        <a data-hijack="true" class="menu-link" href="/register/">JOIN NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MIDDLE CONTENT -->
