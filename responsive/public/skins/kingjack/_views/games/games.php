<?php
// the hub data is common for all the same controller
// special exception for exclusive slots so it can have it's own hub
$page = $this->controller;
if($this->action == 'exclusive') $page.= 'exclusive';
?>
    <!-- MIDDLE CONTENT -->
    <div class="middle-content">
        <!-- breadcrumbs -->
        <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>

      <?php edit($page,'acquisition');
      if(is_array(@$this->content['acquisition']) && count(@$this->content['acquisition']) > 0):
      ?>
      <div class=" games-list middle-content__box top-games-hub">
        <?php
        $hub = 'acquisition';
        include "_global-library/partials/hub/hub-grid-9.php"
        ?>
      </div>
    </section>
    <?php endif;?>
    <!-- END Games info modal -->

        <div class="games-pages">
            <!-- games list-->
            <?php include '_global-library/partials/games/games-list.php'; ?>
            <!-- end games list -->
            <!-- END Games info modal -->

            <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                <div class="cta-bottom-wrap ">
                    <div class="cta-bottom fullwidth">
                        <div class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                            <div class="inner">
                                <a data-hijack="true" class="menu-link" href="/register/">JOIN NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bottom-content">
            <?php
                if ($this->action != "slots") {
                    edit($this->controller.$this->action,'footer-seo', $this->action);
                    @$this->getPartial($this->content['footer-seo'], 1); 
                } else {
                    edit($this->controller,'footer-seo',$this->action);
                    @$this->getPartial($this->content['footer-seo'],$this->action); 
                }
            ?>
        </div>

    </div>
    <!-- END MIDDLE CONTENT -->