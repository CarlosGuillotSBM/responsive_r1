<!-- MIDDLE CONTENT -->
<div class="middle-content">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<div class="section middle-content__box">

 <div class="section__header logolead">
        <h1 class="section__title">
        Game Details
        </h1>
      </div>
  <?php include '_global-library/partials/games/games-details-page.php'; ?>
  <div class="games-detail-page__bottom">
    <?php edit('gamesdetail','deposit-count-based',$this->row['DetailsURL']); ?>
    <?php @$this->repeatData($this->content['deposit-count-based']); ?>    
    <?php edit('gamesdetail','games-detail',$this->row['DetailsURL']); ?>
    <?php @$this->repeatData($this->content['games-detail']); ?>
  </div>

     <a data-hijack="true"  class="back-to-games" href="/games/">Back To All Games</a>

  <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
          <div class="cta-bottom-wrap ">
            <div class="cta-bottom fullwidth">
              <div  class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                <div class="inner">
                  <a data-hijack="true" class="menu-link" href="/register/">JOIN NOW</a>
                </div>
              </div>
            </div>
          </div>
        </div>






</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->
