
<!-- MIDDLE CONTENT -->	
<div class="middle-content">
<!-- breadcrumbs -->
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
<!--/ breadcrumbs -->
<div class="middle-content__box">
	<!-- breadcrumbs -->
	
	<!-- END breadcrumbs -->
	<section class="section">
		<!-- CONTENT -->
		<div class="content-template">
			
			<div class="error-page">
				<div class="error-page__image">
					<img src="/_images/common/melvis-guitar.png">
				</div>
				<div class="error-page__text">

<h1 class="error-page__error">Oops!</h1>
<p class="error-page__msg">Error Code: 404 </p>
<p class="error-page__msg">
We’re sorry, we can’t find the page you’re looking for. </p>
<p class="error-page__msg">Start fresh from the <a href="/">homepage</a>
</p>
							
	
<p class="error-page__msg">
or check out our latest <a href="/promotions/">promotions </a>
</p>

				</div>
				
			</div>
			
		</div>
		<!-- /CONTENT-->
	</section>
</div>
<!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->