<!DOCTYPE html>
<html>
<head>
	<title>Under Maintenance</title>
<style type="text/css">
	body{
		background-color:#3C1C6F;
		color:white;
		text-align: center;
		font-size: 20px;
		font-family: arial;
		margin-top: 12%;
	}
	img {
		width: 260px;
	}

	h1{
		font-size: 40px;		
	}

</style>
</head>
	<body>
		<br><br><br>
	<img src="/_images/logo/logo_onesizefitsall.svg" class="logo">
		<h1>Under Maintenance</h1>
		<div>Sorry, we are offline for just a few minutes. <br><br>Please come back soon.</div>
	</div>
	</body>
</html>