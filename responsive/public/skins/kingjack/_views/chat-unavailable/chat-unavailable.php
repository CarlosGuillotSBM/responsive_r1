<div class="main-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!-- END breadcrumbs -->
<!--   <section class="section"> -->

     <section class="section logolead">
      <h1 class="section__title">Chat Unavailable</h1>
<!--     </section> -->

    <!-- CONTENT -->
    <?php edit($this->controller,'chat-unavailable'); ?>
    <div class="middle-content__box">

      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['chat-unavailable']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->