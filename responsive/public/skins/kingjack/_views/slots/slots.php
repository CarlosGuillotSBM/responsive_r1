<?php
// the hub data is common for all the same controller
// special exception for exclusive slots so it can have it's own hub
    $page = $this->controller;
    if($this->action == 'exclusive') $page.= 'exclusive';
?>

<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->

    <div class="section__header logolead">
        <h1 class="section__title">
            <?php echo $key; ?>
        </h1>
    </div>

     <div class="live-casino-banner banner-edit-point ui-scheduled-content-container">
        <!-- Edit point Banner -->
        <?php edit($this->controller,'games-latest-offer'); ?>
        <?php @$this->repeatData($this->content['games-latest-offer']); ?>
    </div>


    <div class="page-banner">
        <?php edit($this->controller,'slots-main-offer-banner', $this->action); ?>
        <?php @$this->repeatData($this->content['slots-main-offer-banner']);?>
    </div>

    <div class="games-pages">

        <!-- games list-->
        <?php include '_partials/slots/games-list.php'; ?>
        <!-- end games list -->
        <!-- END Games info modal -->

        <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
            <div class="cta-bottom-wrap ">
                <div class="cta-bottom fullwidth">
                    <div class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                        <div class="inner">
                            <a data-hijack="true" class="menu-link" href="/register/">JOIN NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MIDDLE CONTENT -->

<div class="bottom-content">
    <?php
        if ($this->action != "slots") {
            edit($this->controller.$this->action,'footer-seo', $this->action);
            @$this->getPartial($this->content['footer-seo'], 1); 
        } else {
            edit($this->controller,'footer-seo',$this->action);
            @$this->getPartial($this->content['footer-seo'],$this->action); 
        }
    ?>
</div>

