<?php

// old luckypants affiliate re-directs
$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';

/*
if(preg_match("#^\/signup\/$#", $path) == 1 || preg_match("#^\/register-3step\/$#", $path) == 1) && (config("RealDevice") == 1 || config("RealDevice") == 2)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /register/". $queryString);
    exit;
}
*/


if(preg_match("#^\/loyalty\/$#", $path) == 1 || preg_match("#^\/loyalty\/magical-moolahs\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /loyalty/vip-club/". $queryString);
    exit;
}

// DEV-10401

if (preg_match("#^\/play\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/play/". $queryString);
    exit;
}

if (preg_match("#^\/spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/spins/". $queryString);
    exit;
}


if (preg_match("#^\/claim\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/claim/". $queryString);
    exit;
}


if (preg_match("#^\/free\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/free/". $queryString);
    exit;
}


//R10-204
if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/kingjackcasino/". $queryString);
    exit;
}
//R10-559
if(preg_match("#^\/200k\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/200k-holiday/' );
    exit;
}
if(preg_match("#^\/lapland\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/lapland/' );
    exit;
}
//R10-1009
if(preg_match("#^\/excluded\/$#", $path))
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /promotions/excluded/' );
    exit;
}