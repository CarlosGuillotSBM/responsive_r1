<script type="application/javascript">

    skinPlugins.push({
        id: "Slick",
        options: {
            slideWidth: <?php if (config("RealDevice") == 1) echo 310; else if (config("RealDevice") == 2) echo 360; else echo 600?>,
            responsive: true,
            minSlides: 1,
            maxSlides: <?php if (config("RealDevice") == 1) echo 3; else if (config("RealDevice") == 2) echo 2; else echo 1?>,
            slideMargin: 20,
            auto: true,
            infinite: true,
            element: 'bxslider-unmissable-promos'
        }
    });

</script>

<!-- CONTENT HOLDER WRAP MOBILE -->
<div class="holder-mobile">

	<!-- Title -->
	<h1 class="title-mobile">UNMISSABLE PROMOTIONS</h1>

	<!-- Content Wrap -->
	<div class="content-holder-mobile">
		<!-- <ul class="bxslider"> -->
		<ul class="bxslider-unmissable-promos">
			<?php // @$this->repeatData($this->content["unmissable-promos-mobile"],0,"_partials/promos/unmissable-promos-for-mobile-slide.php"); ?>
            <?php include'_partials/promos/unmissable-promos-mobile-slide.php'; ?>
		</ul>
	</div>
	<!-- /Content Wrap -->

</div>
<!-- /CONTENT HOLDER WRAP MOBILE -->