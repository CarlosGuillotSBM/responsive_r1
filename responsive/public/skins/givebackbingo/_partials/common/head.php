<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

	<!-- Chrome, Firefox OS, Opera and Vivaldi -->
	<meta name="theme-color" content="#e53776">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#e53776">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#e53776">

	<?php include "_global-library/partials/common/open-graph.php"; ?>
	
	<?php echo $this->seo_meta;?>

	<!-- icons for iPhone -->
	<link rel="apple-touch-icon" href="/_images/common/lp-apple-touch-icon.png" />
	<link rel="icon" type="image/ico" href="/_images/common/favicon.ico" />
	<link href="https://plus.google.com/107827792670475608547/" rel="publisher" />

	<link rel="stylesheet" href="/_global-library/js/vendor/intlTelInput/css/intlTelInput.css<?php echo "?v=" . config("Version"); ?>" />
	<link rel="stylesheet" href="/_css/main.css<?php echo "?v=" . config("Version"); ?>" />
	<link rel="stylesheet" href="/_css/slick.css" />
	<?php
		include '_trackers/header.php';
	?>

	<script src="/_global-library/js/bower_components/modernizr/modernizr.js"></script>


	<script type="application/javascript">
	// sbm initialization
	var skinModules = [], skinPlugins = [];
	// common modules
	skinModules.push({
		id: "Notifier",
		options: {
			global: true
		}
	});
	skinModules.push({
		id: "GameLauncher",
		options: {
			modalSelector: "#games-info",
			global: true
		}
	});
	skinModules.push({
		id: "BingoLauncher",
		options: {
			modalSelector: "#rooms-info",
			global: true
		}
	});
	skinModules.push({
		id: "GameWindow",
		options: {
			global: true
		}
	});
	skinModules.push({
		id: "Favourites",
		options: {
			imgFavouriteOn: "/_images/common/icons/favourite-icon--on.png",
			imgFavouriteOff: "/_images/common/icons/favourite-icon--off.png",
			noFavIconsWeb: 6,
			noFavIconsTablet: 3,
			noFavIconsPhone:2,
			global: true
		}
	});
	skinModules.push( {
		id: "MailInbox",
		options: {
			global: true
		}
	});
	skinModules.push( {
		id: "TrackNavigation",
		options: {
			global: true
		}
	});
	skinModules.push( {
		id: "RadSwitch",
		options: {
			global: true
		}
	});
	skinModules.push( {
		id: "ClaimCashback",
		options: {
			global: true
		}
	});
	skinModules.push( {
		id: "ClaimUpgradeBonus",
		options: {
			global: true
		}
	});
	skinModules.push({
		id: "ContentScheduler",
		options: {
			global: true
		}
	});
	// common plugins
	skinPlugins.push({
		id: "Global",
		options: {
			global: true
		}
	});
	skinPlugins.push({
		id: "Tables",
		options: {
			global: true
		}
	});
	skinPlugins.push({
		id: "MobileMenu",
		options: {
			global: true
		}
	});
	 //Prize Wheel
     skinModules.push({
          id: "PrizeWheel",
          options: {
               global: true
          }
     });
		skinModules.push({
			id: "CMANotifications",
			options: {
				global: true
			}
		});
	</script>


			       <!-- SEO Social -->
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Give Back Bingo",
  "url" : "https://www.givebackbingo.com/",
  "sameAs" : [ "https://www.facebook.com/givebackbingo/",
    "https://twitter.com/GiveBackBingo"] 
}
</script>
<!--/ SEO Social -->


	<!-- Browser not supported redirect -->
	<?php include "_global-library/partials/common/user-agent-sniffer.php"; ?>
    <link rel="manifest" href="/manifest.json"/>
</head>

<body id="<?php echo $this->controller; ?>" class="">
	<?php if(Env::isLive()):?>

	<!-- new relic -->
	<?php include "_global-library/trackers/new-relic/give-back-bingo.php"; ?>
	<!-- /relic -->
	
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K2TJRH3"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
		<script type="text/javascript">var rumMOKey='0a99255be807f850a9a51df841668571';(function(){if(window.performance && window.performance.timing && window.performance.navigation) {var site24x7_rum_beacon=document.createElement('script');site24x7_rum_beacon.async=true;site24x7_rum_beacon.setAttribute('src','//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey='+rumMOKey);document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);}})(window)</script>
	<?php endif;?>
