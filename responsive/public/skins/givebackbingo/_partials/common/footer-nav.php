<article class="footer-nav">
    <ul>
        <li><a href="/about-us/">About us</a></li>
        <li><a href="/faq/">FAQ</a></li>
        <li><a href="/banking/">Banking</a></li>
    </ul>
    <ul>
        <li><a href="/terms-and-conditions/">Terms &amp; Conditions</a></li>
        <li><a href="/privacy-policy/">Privacy Policy</a></li>
        <li><a href="/responsible-gaming/">Responsible Gaming</a></li>
       
    </ul>
    <ul>
        <li><a href="/complaints-and-disputes/">Complaints and Disputes</a></li>
         <li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none"><a  href="/cashier/?settings=1">PLAYER SETTINGS</a></li>
        <li><a href="/archive/">Archive</a></li>
    </ul>
</article>
