<script type="application/javascript">
	window.setTimeout(function () {
		var el = document.getElementById("ui-first-loader");
		if (el) el.parentNode.removeChild(el);
		el = document.getElementById("ui-loader-animation");
		if (el) el.style.display = "none";
	},1000);
</script>
<!-- OFF CANVAS -->
<!-- This is the Foundation side nav, everything should be wrapped inside off-canvas-wrap -->
<!-- OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->

<div class="main-wrap top-message">
	<!-- header  -->
	<header class="header header--<?php echo $this->controller; ?>">

		<!-- HEADER DESKTOP -->
		<div class="header-desktop">
			<!-- COOKIE POLICY -->
			<?php include "_global-library/partials/modals/cookie-policy.php" ?>


			<div class="header-wrap">
				<div class="navigation__container">
					<?php include "_partials/common/navigation-desktop.php"  ?>
				</div>
			</div>

		</div>
		<!-- /HEADER DESKTOP -->

		<!-- HEADER MOBILE old-->
		<!-- 		<div class="header-mobile">
			<?php// include "_partials/common/navigation-mobile.php" ?>
		</div> -->
		<!-- nav for mobile -->
		<?php include "_partials/nav-mobile/nav-mobile.php"; ?>
		<!-- / nav for mobile -->
		<!-- search screen for mobile -->
		<?php include "_partials/search-screen/search-screen.php"; ?>
		<!-- / search screen for mobile -->
	</header>


</div>
<!-- /HEADER MOBILE -->
<!-- /header  -->
<div class="clearfix"></div>
 <?php
        include("_global-library/widgets/prize-wheel/prize-wheel-notification.php");
    ?>
<!-- AJAX CONTENT WRAPPER -->
<div id="ajax-wrapper">
	<!-- MAIN CONTENT AREA -->
	<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>  ">
		<!-- TOP CONTENT -->
		<div class="top-content">
		</div>
		<!-- /TOP CONENT -->