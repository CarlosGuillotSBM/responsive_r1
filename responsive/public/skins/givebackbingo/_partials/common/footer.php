<!-- BOTTOM CONTENT -->
<div class="bottom-content">
</div>
<!-- END Games info modal -->
</div>
<!-- END HOME MAIN CONTENT AREA -->
</div>
<!-- END AJAX WRAPPER - It is needed to add content inside dynamically -->

<!-- FOOTER AREA -->
<footer id="footer" class="footer" data-bind="css: { 'logged-in': validSession() }">
		<!-- FOOTER NAVIGATION  -->
		<div class="footer-nav-wrap">

			<?php include "_partials/common/footer-nav.php" ?>
			<!-- SUPPORT ONLINE  -->
			<div class="online-support">
				<div target="_blank" data-hijack="false" class="support-online-title">
					<a href="#" onclick="javascript:void window.open('<?php echo config('LiveChatURL'); ?>','1323353895481','width=590,height=480,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;">
						<h1>Online Support</h1>
						<span>24h Live Chat</span>
						<img src="/_images/common/support.svg">
					</a>
				</div>
			</div>
		</div>
		<!-- /FOOTER NAVIGATION  -->

		<!-- Container  -->
			<div class="footer__inner">				
				<div class="footer-legal">
				<!-- FOOTER FLOATING SOCIAL ICONS  -->
					<div class="follow-us">
						<a target="_blank" href="https://www.facebook.com/givebackbingo/" class="hi-icon"  data-hijack="false"><img src="/_images/common/social/facebook.svg"></a>
					</div>
						<!-- FOOTER FLOATING PAYMENT METHODS  -->
						<ul class="footer__row__payments">
							<li> <img class="paypal" src="/_images/common/pay-methods/paypal.svg"></li>
							<li> <img class="maestro" src="/_images/common/pay-methods/maestro-icon.svg"></li>
							<li> <img class="mastercard" src="/_images/common/pay-methods/mastercard-icon.svg"></li>
							<li> <img class="visa" src="/_images/common/pay-methods/visa.svg"></li>
							<li> <img class="neteller" src="/_images/common/pay-methods/neteller.svg"></li>
							<li> <img class="paysafecard" src="/_images/common/pay-methods/paysafecard.svg"></li>
							<li><a href="/terms-and-conditions"><img src="/_images/common/commissions/18-plus.svg"></a></li>
							<li><a target="_blank" data-hijack="false" href="http://www.gamcare.org.uk/"><img src="/_images/common/commissions/gamcare.svg"></a></li>
							<li><a target="_blank" data-hijack="false" href="https://www.gamstop.co.uk"  rel="nofollow" ><img src="/_images/common/commissions/gamstop.svg"> </a></li>
							<li><a target="_blank" data-hijack="false" href="http://www.gamblingcontrol.org/"><img src="/_images/common/commissions/gambling-control.svg"></a></li>
							<li><a target="_blank" data-hijack="false" rel="nofollow" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022"><img class="gambling" src="/_images/common/commissions/gambling-commission.svg"> </a></li>
							<li><a target="_blank" data-hijack="false" href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&lng=EN"><img src="/_images/common/commissions/ecogra.svg"></a></li>

						</ul>
					
					<div class="clearfix"></div>
					<!-- FOOTER COMMISSIONS ICONS -->
					<div class="footer-commissions">
						<ul class="commissions-list-2">
							<!-- <li><a target="_blank" data-hijack="false" href="http://www.stridegaming.com/"><img src="/_images/common/commissions/stridegaming-logo.png"></a></li> -->
							<!-- <li><a target="_blank" data-hijack="false" href="http://www.londonstockexchange.com/exchange/prices-and-markets/stocks/summary/company-summary/JE00BWT5X884JEGBXASQ1.html"><img src="/_images/common/commissions/london-stock-exchange.png"></a></li> -->
						</ul>
					</div>
					<div class="clearfix"></div>

					<!-- FOOTER FLOATING CONTENT LEGAL AND CONTACT  -->
					<div class="footer-terms small-12 small-centered medium-6 medium-centered columns">
						<div class="clearfix"><p>All Financial Transactions processed by WorldPay. © Give Back Bingo. All rights reserved.</p> </div>
						
						<div class="clearfix"><p>Give Back Bingo is licensed and regulated to offer Gambling Services in Great Britain by the UK Gambling Commission, license Number 000-039022-R-319427-004. </p></div>
						
						<div class="clearfix"><p>All the games offered on the website have been approved by the UK Gambling Commission. Details of its current licensed status as recorded on the Gambling Commission's website can be found <a target="_blank" data-hijack="false" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022">here</a>. </p></div>

						<div class="clearfix"><p>Give Back Bingo is also licensed and regulated by the Alderney Gambling Control Commission, License Number: 71 C1, to offer Gambling facilities in jurisdictions outside Great Britain.
						
						Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3UL.</p></div>
					</div>



					<!-- BACK TO TOP -->
					<a class="back-to-top" href="#ajax-wrapper" data-hijack="false">
						<img src="/_images/common/accordion-up-arrow.png" alt="Back To Top">
					</a>

				</div>










				<div class="clearfix"></div>
			</div>
	</div>

	<!-- /Container -->
	<!-- /FOOTER FLOATING CONTENT LEGAL AND CONTACT -->

</footer>
<!-- END FOOTER AREA -->
</div>
<!-- END INNER WRAPPER. It is necesary for foundation off canvas nav -->
</div>

<!-- login modal -->
<div id="login-modal" class="reveal-modal" data-reveal>
<?php include "_partials/login/login-box.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->

<!-- login modal deposit -->
<div id="login-modal-deposit" class="reveal-modal" data-reveal>
<?php include "_partials/login/login-box-deposit.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- Claimed prize modal -->
<div id="claimed-prize-modal" class="reveal-modal" data-reveal>
<?php include "_global-library/partials/promotion/promotion-claimed-prize.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- terms modal -->
<!-- <div id="terms-modal" class="reveal-modal" data-reveal style="display:block">
<span class="close-reveal-modal">&#215;</span>
</div> -->
<!-- /terms modal -->

<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">
<!-- END OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->

<!-- Games info modal -->
<div id="games-info" class="games-info reveal-modal" data-reveal>
	<?php include "_partials/games/games-details-modal.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Games info modal -->

<!-- Login Box modal -->
<!-- <div id="login-box-modal" class="reveal-modal" data-reveal>
	<?php //include "_partials/login/login-box-modal.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>

<a href="#" data-reveal-id="login-box-modal">login-box</a> -->
<!-- Login Box modal -->

<!-- Bingo room modal -->
<div id="rooms-info" class="games-info reveal-modal" data-reveal>
	<?php include "_partials/bingo-rooms/room-detail-modal.php" ?>
	<?php //include "_global-library/partials/bingo/room-detail-modal.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Bingo room modal -->

<!-- Custom modal -->
<div id="custom-modal" class="games-info reveal-modal" data-reveal>
	<?php include "_partials/bingo-rooms/no-room-detail-modal.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Custom modal -->

<?php
	include "_global-library/partials/modals/alert.php";
	include "_global-library/partials/modals/notification.php";
	include "_partials/common/loading-modal.php";
	include "_global-library/partials/games/games-iframe.php";
	include "_global-library/partials/games/games-last-played.php";
	include "_global-library/partials/common/app-js.php";
	include "_global-library/partials/common/recaptcha.php";
?>

<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">

</body>
</html>