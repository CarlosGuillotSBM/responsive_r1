<script>
// make this script work by adding into correct js file
//jQuery('.recent-messages-expand').on('click',function(){
//$(this).toggleClass('collapsed expanded');
//});
</script>
<!-- My Messages Box -->
<div class="lobby-wrap__content-box">
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon" >
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-messages.php'; ?>
                <!-- end include svg icon partial -->
            </div >
            <div class="title__text messages-section" >MY MESSAGES</div>
            <div class="title__option-links">
                
                
                <span class="select-all-messages"><input type="checkbox"> </span>
            </div>
            <select name="mobile-message-actions" class="mobile-message-actions" id="mobile-message-actions">
                <option>Choose Action</option>
                <option value="Mark as Read">Mark as read</option>
                <option value="Mark as Unread">Mark as uread</option>
                <option value="Delete">Delete</option>
            </select>
        </div>
    </div>
    <!-- end header bar -->
    
    <div class="lobby-wrap__content-box__content">
        <!-- make this content a partial -->
        <!-- recent messagages -->
        <div class=" lobby-wrap__content-box__content__innerwrap--border">
            <ul class="recent-messages collapsed">
                <!-- notice the uread class in this message JS needs to append unread class to show its unread also the svg partials needs changing to have read or  -->
                
                <!-- single message preview -->
                <li  class="recent-messages__message unread" data-reveal-id="single-message">
                    <span class="recent-messages__message__icon">
                        <!-- include svg icon partial -->
                        <?php include 'icon-message-unread.php'; ?>
                        <!-- end include svg icon partial -->
                    </span>
                    <span class="recent-messages__message__date">20/07/15</span>
                    <span class="recent-messages__message__preview">Win a Trip to Las Vegas with our fantastic new competition. When you play on magical vegas</span>
                    <span class="recent-messages__message__time">13:00</span>
                    <span class="recent-messages__message__select"><input type="checkbox"></span>
                </li>
                <!-- End single message preview -->
                <!-- single message preview -->
                <li class="recent-messages__message" data-reveal-id="single-message">
                    <span class="recent-messages__message__icon">
                        <!-- include svg icon partial -->
                        <?php include 'icon-message-read.php'; ?>
                        <!-- end include svg icon partial -->
                    </span>
                    <span class="recent-messages__message__date">20/07/15</span>
                    <span class="recent-messages__message__preview">Summer Madness ar Magical Vegas, get extra Moolahs when you play on slots this weekend</span>
                    <span class="recent-messages__message__time">13:00</span>
                    <span class="recent-messages__message__select"><input type="checkbox"></span>
                </li>
                <!-- End single message preview -->
                <!-- single message preview -->
                <li class="recent-messages__message" data-reveal-id="single-message">
                    <span class="recent-messages__message__icon">
                        <!-- include svg icon partial -->
                        <?php include 'icon-message-read.php'; ?>
                        <!-- end include svg icon partial -->
                    </span>
                    <span class="recent-messages__message__date">20/07/15</span>
                    <span class="recent-messages__message__preview">Thanks for joining the pack at Magical Vegas, we have lots of fun slots and casino games</span>
                    <span class="recent-messages__message__time">13:00</span>
                    <!-- needs the slect box excluding from the click that opens the signle message modal -->
                    <span class="recent-messages__message__select"><input type="checkbox"></span>
                </li>
                <!-- End single message preview -->
                <!-- single message preview -->
                <li class="recent-messages__message" data-reveal-id="single-message">
                    <span class="recent-messages__message__icon">
                        <!-- include svg icon partial -->
                        <?php include 'icon-message-read.php'; ?>
                        <!-- end include svg icon partial -->
                    </span>
                    <span class="recent-messages__message__date">20/07/15</span>
                    <span class="recent-messages__message__preview">Thanks for joining the pack at Magical Vegas, we have lots of fun slots and casino games</span>
                    <span class="recent-messages__message__time">13:00</span>
                    <!-- needs the slect box excluding from the click that opens the signle message modal -->
                    <span class="recent-messages__message__select"><input type="checkbox"></span>
                </li>
                <!-- End single message preview -->
                <!-- single message preview -->
                <li class="recent-messages__message" data-reveal-id="single-message">
                    <span class="recent-messages__message__icon">
                        <!-- include svg icon partial -->
                        <?php include 'icon-message-read.php'; ?>
                        <!-- end include svg icon partial -->
                    </span>
                    <span class="recent-messages__message__date">20/07/15</span>
                    <span class="recent-messages__message__preview">Thanks for joining the pack at Magical Vegas, we have lots of fun slots and casino games</span>
                    <span class="recent-messages__message__time">13:00</span>
                    <!-- needs the slect box excluding from the click that opens the signle message modal -->
                    <span class="recent-messages__message__select"><input type="checkbox"></span>
                </li>
                <!-- End single message preview -->
                <!-- single message preview -->
                
            </ul>
             <!-- shown by default -->
            <div class="recent-messages-expand">
                &#9660; Expand all messages &#9660;
            </div>

            <!-- hidden by default -->
              <div class="recent-messages-collapse">
                &#9650; Collaspse messages &#9650;
            </div>
            <!-- End single message preview -->
            
        </div>
        <!-- end recent messagages -->
    </div>
</div>
<!-- End My Messages Box -->