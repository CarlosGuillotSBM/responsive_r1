<div class="switch-top-message ui-switch-bar">
	<div class="switch-top-message__wrap" style="position: relative;">

		<!-- SWITCH TO OLD -->
		<a class="switch ui-switch-bar-text" target="_top" data-bind="click: RadSwitch.switchBarSwitch" data-gtag="Switch to Old Top Link">switch to our classic site</a>

		<!-- WELCOME MESSAGE -->
		<div class="message">
			<!-- STAR ANIMATION -->
			<svg width="390" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				  <defs><path id="star-yellow" fill="#ffcc00" d="M0-10L2.939-4.05L9.511-3.09L4.755 1.55L5.878 8.09L0 5L-5.878 8.09L-4.755 1.55L-9.511-3.09L-2.939-4.05Z"/></defs>
				  <defs><path id="star-purple" fill="#9B74DD" d="M0-10L2.939-4.05L9.511-3.09L4.755 1.55L5.878 8.09L0 5L-5.878 8.09L-4.755 1.55L-9.511-3.09L-2.939-4.05Z"/></defs>
				  <defs><path id="star-pink" fill="#ff8fd7" d="M0-10L2.939-4.05L9.511-3.09L4.755 1.55L5.878 8.09L0 5L-5.878 8.09L-4.755 1.55L-9.511-3.09L-2.939-4.05Z"/></defs>
				  <g>
				  <rect fill="transparent" height="100" width="300" class="color-fill"/>
				    <g transform="translate(100,40)"><use xlink:href="#star-yellow" class="glittering"/></g>
				    <g transform="translate(60,5)"><use xlink:href="#star-purple" class="glittering1"/></g>
				    <g transform="translate(120,10)"><use xlink:href="#star-pink" class="glittering2"/></g>
				 </g>
			</svg>

			<p>WELCOME TO OUR NEW-LOOK WEBSITE!</p>
			<span class="luke"></span>
			<span class="reminder" data-bind="click: RadSwitch.switchBarNoReminders" >GOT IT! DON’T REMIND ME AGAIN.</span>
		</div>
	</div>

	<!-- CLOSE BT -->
	<div class="close ui-close-switch-bar" data-bind="click: RadSwitch.closeBar"></div>
</div>