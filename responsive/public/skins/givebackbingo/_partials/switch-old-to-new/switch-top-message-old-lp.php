<style>
/*OLD LP SWITCH TOP MESSAGE CSS*/
.switch-top-message-old-lp {
	padding-left: 0.3125rem;
	padding-right: 0.3125rem;
	width: 100%;
	float: left;
	position: fixed;
	top: 0;
	font-family: Helvetica, Arial, sans-serif;
	font-size: 0.8em;
	line-height: 4.2;
	background: #fffacb;
	z-index: 9999;
	height: 45px;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap {
	margin: 0 auto;
	max-width: 62.5rem;
	width: 100%;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap h2 {
	color: #5820A9;
	float: left;
	margin: 0;
	line-height: 3.2;
	font-weight: 600;
	font-size: 15px;
	padding-right: 5px;
	font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
	letter-spacing: 0;
	position: absolute;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap h2 span {
	font-weight: normal;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap .cta-wrap {
	padding-left: 0.3125rem;
	padding-right: 0.3125rem;
	width: 33.33333%;
	float: left;
	float: right;
	position: relative;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap .cta-wrap .luke {
	background: url(/_images/switch-old-to-new/luke-hands-up.png) no-repeat center center;
	background-size: 130%;
	position: absolute;
	height: 45px;
	width: 70px;
	display: block;
	margin: 0;
	margin-left: 50px;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap .cta-wrap svg {
	position: absolute;
	float: right;
	right: 0;
	z-index: -1;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap .cta-wrap .glittering {
	animation: glitterOne 4.5s linear 0s infinite normal;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap .cta-wrap .glittering1 {
	animation: glitterOne 3.5s linear 0s infinite normal;
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap .cta-wrap .glittering2 {
	animation: glitterTwo 3.0s ease-in 1s infinite normal;
}
@keyframes glitterOne {
	0% {
	-webkit-transform: scale(1);
	opacity: 1;
	}
	25% {
	-webkit-transform: scale(0.5);
	opacity: 0;
	}
	50% {
	-webkit-transform: scale(1);
	opacity: 1;
	}
	75% {
	-webkit-transform: scale(0.5);
	opacity: 0;
	}
	100% {
	-webkit-transform: scale(1);
	opacity: 1;
	}
}
@keyframes glitterTwo {
	0% {
	-webkit-transform: scale(0.5);
	opacity: 1;
	}
	25% {
	-webkit-transform: scale(0.3);
	opacity: 0.5;
	}
	50% {
	-webkit-transform: scale(0.8);
	opacity: 1;
	}
	75% {
	-webkit-transform: scale(0.3);
	opacity: 0.5;
	}
	100% {
	-webkit-transform: scale(0.5);
	opacity: 1;
	}
}
.switch-top-message-old-lp .switch-top-message-old-lp__wrap .cta-wrap .cta {
	height: 30px;
	width: 180px;
	background: url(/_images/switch-old-to-new/cta-try-button.svg) no-repeat center center;
	float: right;
	top: 8px;
	position: relative;
}
.switch-top-message-old-lp .close {
	height: 100%;
	width: 40px;
	position: absolute;
	top: 0;
	right: 0;
	background: url(/_images/switch-old-to-new/close-message.svg) no-repeat center center;
	background-size: 30px;
	cursor: pointer;
}
</style>
<div class="switch-top-message-old-lp">
	<div class="switch-top-message-old-lp__wrap" style="position: relative;">
		<h2>WE HAVE A FAB NEW-LOOK SITE! <span>TRY IT NOW AND ENJOY OUR UPGRADED FEATURES.</span></h2>
		<!-- STAR ANIMATION -->
		<div class="cta-wrap">
			<svg width="390" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<defs><path id="star-yellow" fill="#ffcc00" d="M0-10L2.939-4.05L9.511-3.09L4.755 1.55L5.878 8.09L0 5L-5.878 8.09L-4.755 1.55L-9.511-3.09L-2.939-4.05Z"/></defs>
				<defs><path id="star-purple" fill="#9B74DD" d="M0-10L2.939-4.05L9.511-3.09L4.755 1.55L5.878 8.09L0 5L-5.878 8.09L-4.755 1.55L-9.511-3.09L-2.939-4.05Z"/></defs>
				<defs><path id="star-pink" fill="#ff8fd7" d="M0-10L2.939-4.05L9.511-3.09L4.755 1.55L5.878 8.09L0 5L-5.878 8.09L-4.755 1.55L-9.511-3.09L-2.939-4.05Z"/></defs>
				<g><rect fill="transparent" height="100" width="300" class="color-fill"/>
				<g transform="translate(100,40)"><use xlink:href="#star-yellow" class="glittering"/></g>
			<g transform="translate(60,5)"><use xlink:href="#star-purple" class="glittering1"/></g>
		<g transform="translate(120,10)"><use xlink:href="#star-pink" class="glittering2"/></g>
	</g>
</svg>
<span class="luke"></span>
<a class="cta" href="#" target="_top"></a>
</div>
</div>
<!-- CLOSE BT -->
<div class="close"></div>
</div>