<!-- HEADER -->
<div class="header">
	<h1>OUR CLASSIC SITE IS ONLY AVAILABLE UNTIL <date class="ui-3weeks-date">21.06.2016</date></h1>
	
</div>

<!-- INFO -->
<div class="info">
	<ul>
		<p>1000s of players are enjoying the new look Lucky Pants site! Try now to enjoy the following upgraded features:</p>
		<li>Brand new look and feel on mobile and PC devices.</li>
		<li>Faster, simpler cashier.</li>
		<li>All your favourites games + new NetEnt Slots.</li>
		<li>Brand new message inbox - never miss any of our great news or offers.</li>
	</ul>
	<div class="luke">
		<span></span>
	</div>
</div>

<!-- FOOTER -->
<div class="bottom">
	<h2>THERE’S MORE WAYS THAN EVER TO GET LUCKY, SO THROW ON YOUR LUCKY PANTS AND TRY OUR NEW SITE NOW!</h2>
	<!-- CTAS -->
	<div class="cta">
		<a class="stay" href="">STAY IN THE NEW SITE</a>
		<a class="switch" target="_top" data-bind="click: RadSwitch.switchBarSwitch" data-gtag="Switch to Old 3 Weeks">OR GO TO OUR CLASSIC SITE</a>
	</div>
</div>