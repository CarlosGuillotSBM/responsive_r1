<script type="application/javascript">
skinModules.push({
		id: "LoginBox",
		options: {
            modalSelectorWithoutDetails: "#custom-modal",
            global: true
		}
	});
</script>


<!-- LOGIN BOX -->
<article class="login-box">

	<div class="login-box__form-wrapper">
		<h1>login</h1>
		<!-- FORM -->
		<form class="" id="ui-login-form">

			<div class="login-box__input-wrapper">
				<div class="login-box__username">
					<input id="login_name" name="Username" placeholder="Username" type="text" placeholder="User Name" data-bind="value: LoginBox.username, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" />
					<img src="/_images/common/icons/login-username.svg">
				</div>
				<span style="display: none" data-bind="visible: LoginBox.invalidUsername" class="error">This field is required.</span>
			</div>

			<div class="login-box__input-wrapper">
				<div class="login-box__password">
					<input name="Password" id="login_password" placeholder="Password" type="password" placeholder="Password" data-bind="attr:{type: LoginBox.passwordShowText() == 'Hide' ? 'text': 'password'}, value: LoginBox.password, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}"/>
					<img src="/_images/common/icons/login-password.svg">

					<div class="login-box__show-password" data-bind="click: LoginBox.togglePassword">
					<a class="right" data-bind="text: LoginBox.passwordShowText">Show</a>

				</div>
				<span style="display: none" data-bind="visible: LoginBox.invalidPassword" class="error">This field is required.</span>
			</div>


			</div>
			<a style="display: none" data-bind="visible: LoginBox.lockedUser, text: LoginBox.loginError" class="error" href="/help"></a>
		<!-- ko ifnot: LoginBox.lockedUser -->
			<span style="display: none" data-bind="visible: LoginBox.loginError, html: LoginBox.loginError" class="error"></span>
		<!-- /ko -->
			<a href="/forgot-password/" class="login-box__forgot-password">Forgot Password?</a>
			<a class="cta-login" data-bind="click: LoginBox.doLogin.bind($data, GameLauncher.gameId(), GameLauncher.gamePosition(), GameLauncher.gameContainerKey(), GameLauncher.roomId())">Login</a>
		</form>
		<!-- / FORM -->

	</div>


	<!-- LOGIN OFFER CTA -->
	<div class="join-now-box">
		<p>Not Registered yet?</p>
		<span>
			<!-- Edit point  -->
			<?php edit($this->controller,'login-box-offer'); ?>
			<?php @$this->getPartial($this->content['login-box-offer'],1); ?>
		</span>
		<!-- CTA -->
		<a class="registercta" href="/register/" data-hijack="true" >Join Now</a>
	</div>
	<!-- /LOGIN OFFER CTA -->

</article>
