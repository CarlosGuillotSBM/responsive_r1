<script type="application/javascript">
	skinModules.push({
			id: "LoginBox",
			options: {
				global: true
			}
		});
</script>

<!-- LOGIN BOX -->
<article class="login-box" style="display: none" data-bind="visible: validSession()" >
	
	<div class="login-box__form-wrapper">
		<h2>Hi <span data-bind="text: username">...</span></h2>
		<div class="login-box__last-login" data-bind="visible: lastLoginDate !== ''">Last login date: <span data-bind="text: lastLoginDate"></span></div>
		<div class="login-box__balance">Total Balance <span data-bind="text: balance" class="balance_amount ui-lbBalance">...</span>
			<a class="cta-cashier" href="/cashier/">DEPOSIT NOW</a>
		</div>
        <div class="login-box__continue">or <span class="close-reveal-modal">continue</span></div>
	</div>
	<!-- RIBBONS -->
	<span class="ribbon-left"></span>
	<span class="ribbon-right"></span>
	<!-- LOGIN CTA -->
<!-- 	<div class="join-now-box">
		<p>Not Registered yet?</p>
		<span>JOIN TO GET <b>£5 BINGO BONUS & 20 FREE SPINS ON REMIX</b></span>
		<a class="registercta" href="/register/" data-hijack="true" >Join Now</a>
	</div> -->
</article>