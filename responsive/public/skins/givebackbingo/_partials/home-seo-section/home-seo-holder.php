<div class="content-holder home-seo">
	<!-- content holder title -->
	<!-- content holder content area -->
	<div class="content-holder__content">
		<!-- content holder content full width inner holder-->
		<!-- <div class="content-holder__content_full-width"></div> -->
		
		<!-- content holder content left inner holder-->
		<div class="content-holder__content_full-width">
			<!-- <div class="content-holder__title"> -->
				<!-- <h1>ABOUT Give Back Bingo</h1> -->
				<!-- content holder title right link -->
				<!-- <span class="content-holder__title__link"><a data-bind="visible: validSession, click: playBingo" class="desktop-bingo-link">Play Bingo &raquo;</a></span> -->
			<!-- </div> -->

			<div class="content-holder__inner">
				<!-- Bingo Scedule  -->
				<?php include "_partials/home-seo-section/home-seo-inner.php"; ?>
				<!-- end Bingo Scedule  -->
			</div>

		</div>

	</div>
</div>