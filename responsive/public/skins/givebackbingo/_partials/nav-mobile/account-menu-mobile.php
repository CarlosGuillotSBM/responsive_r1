<div class="account-menu-mobile" style="display: none" data-bind="visible: validSession">
    <div class="account-menu-mobile__header ui-close-modals">My Account</span></div>
    <ul>
<!--        <li id="account-my-bonuses"><a href="/my-account/" class="ui-close-modals" data-hijack="true" title="My Bonuses">My Bonuses</a></li>-->
        <li id="account-cashier"><a href="/cashier/" class="ui-close-modals" data-hijack="true" title="Cashier">Cashier</a></li>
        <li id="account-my-start-page"><a href="/start/" class="ui-close-modals" data-hijack="true" title="Start page">Start page</a></li>
        <li id="account-my-favourite-games"><a href="/my-favourites/" class="ui-close-modals" data-hijack="true" title="Favourite games">Favourite games</a></li>
        <li id="account-claim-code"><a href="/my-account/?tab=ui-redeem-tab" class="ui-close-modals" data-hijack="false" title="Enter a claim code">Enter a claim code</a></li>
        <li id="account-personal-details"><a href="/my-account/?tab=ui-my-details-tab" class="ui-close-modals" data-hijack="false" title="Personal details">Personal details</a></li>
        <li id="account-account-activity"><a href="/my-account/?tab=ui-activity-tab" class="ui-close-modals" data-hijack="false" title="Account Activity">Account activity</a></li>
        <li class="logout">
            <a data-hijack="true" title="Log Out" style="display: none" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">Log Out</a>
        </li>
    </ul>
</div>

<div class="account-menu-mobile__reveal-modal-bg"></div>