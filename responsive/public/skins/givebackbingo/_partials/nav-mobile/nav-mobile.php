<!-- Mobile Top Bar -->
<div class="tab-bar nav-for-mobile clearfix">
	<!-- COOKIE POLICY -->
	<?php include "_global-library/partials/modals/cookie-policy.php" ?>
	<!-- MOBILE LOGO -->
	<h1 class="seo-logo">
		<a data-hijack="true" href="/" class="site-logo ui-go-home-logo menu-link">
			<img src="/_images/common/GBB-logo-lrg-long.png"/>
		</a>
		<span style="display:none;">Give Back Bingo online The best online bingo sites on mobile and desktop.</span>
	</h1>
	<!-- MOBILE ICON -->
	<h1 class="seo-logo">
		<a data-hijack="true" href="/" class="site-logo-mobile ui-go-home-logo" style="display: none" data-bind="visible: validSession">
			<img src="/_images/common/lb--mobile--logo.png" alt="Give Back Bingo online The best online bingo sites on mobile and desktop.">
		</a>
	</h1>
	<!-- MOBILE ICON LOGGED OUT -->
	<h1 class="seo-logo">
		<a data-hijack="true" href="/" class="site-logo-mobile-logged-out ui-go-home-logo" data-bind="visible: !validSession()">
			<img src="/_images/logo/GBB-logo-web.png"/>
		</a>
		<span style="display:none;">Give Back Bingo online The best online bingo sites on mobile and desktop.</span>
	</h1>

	<!-- Start of navigation toggle -->
	<div class="menu-toggle-wrap">
		<div class="burger">
			<div class="span"></div>
		</div>
	</div>
	<!-- MENU ICONS -->
	<div class="icons-menu">
		<span>
			<!-- Message Inbox-->
			<?php if(config("inbox-msg-on")): ?>
			<div class="mail-icon" title="Message Inbox" style="display: none" data-bind="visible: validSession">
				<a href="/my-messages/" target="_top">
					<img src="/_images/common/mobile-menu-icons-svg/mail-icon.svg">
				</a>
				<span class="mail-value" data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
			</div>
			<?php endif; ?>
			<!-- /Message Inbox-->
			<!-- account Icon -->
			<button class="account-icon" style="display: none" data-bind="visible: validSession">
			<img src="/_images/common/mobile-menu-icons-svg/acc-icon.svg" alt="account">
			</button>
			<!-- Search Icon -->
			<button class="search-icon" style="display: none" data-bind="visible: validSession">
			<img src="/_images/common/mobile-menu-icons-svg/search-icon.svg" alt="search">
			</button>
		</span>
	</div>
	<!-- balance display -->
	<div class="balance-display" style="display: none" data-bind="visible: validSession"><span data-bind="text: balance">...</span></div>

	<!-- logged out -->
	<div class="nav-for-mobile__ctas">
		<!--		<a style="display: none" data-bind="visible: validSession()" data-reveal-id="login-modal-deposit" class="login-button--nav-for-mobile menu-link">INFO</a>-->
		<a style="display: none" data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login-button--nav-for-mobile menu-link ui-scroll-top">LOGIN</a>
		<a style="display: none" data-bind="visible: !validSession()" class="join-button--nav-for-mobile menu-link" href="/register/">JOIN NOW</a>
	</div>
	<!-- /logged out -->



</div>
<!-- / Mobile Top Bar -->

<!-- Menu Mobile -->
<?php  include'_partials/nav-mobile/menu-mobile.php'; ?>
<!-- Account Menu Mobile -->
<?php include'_partials/nav-mobile/account-menu-mobile.php'; ?>
<!-- Balance Display Mobile -->
<?php include'_partials/nav-mobile/balance-display-mobile.php'; ?>
<!-- My Messages Mobile -->
<?php //include'_partials/my-messages-mobile/my-messages-mobile.php'; ?>
<!-- My Messages Mobile -->
<?php include'_partials/my-messages-mobile/single-message-mobile.php'; ?>
