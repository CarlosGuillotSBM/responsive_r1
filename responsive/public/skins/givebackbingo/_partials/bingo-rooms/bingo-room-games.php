<li>
  <img class="bingo-room__image" src="<?php echo $row['Image']; ?>" alt="<?php echo $row['Alt']; ?>">
  <a style="display: none" data-bind="visible: validSession(), click: playBingo" href="<?php echo $row['Path']; ?>" class="cta-join">PLAY NOW</a>
  <a data-bind="visible: !validSession()"  data-nohijack="true" href="/register/" class="cta-join">Join Now</a>
</li>
<!--li class="bingo-room__wrapper">
    <a data-gtag="#" data-hijack="true" href="<?php echo $row['Path']; ?>">
        <img class="bingo-room__image" src="<?php echo $row['Image']; ?>" alt="<?php echo $row['Alt']; ?>">
        <div class="bingo-room__content-wrap">
            <h1 class="bingo-room__title"><?php echo $row['Title'];?></h1>
            <div class="bingo-room__content"><?php echo $row['Intro'];?></div>
            <span class="bingo-room__more-info">Read More ❯</span>
        </div>
    </a>
</li-->