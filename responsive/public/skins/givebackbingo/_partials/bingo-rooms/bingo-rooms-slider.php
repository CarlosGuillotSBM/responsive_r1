<script type="application/javascript">

	skinPlugins.push({
		id: "Slick",
		options: {
			slideWidth: <?php if (config("RealDevice") == 1) echo 235; else if (config("RealDevice") == 2) echo 235; else echo 600;?>,
			minSlides: 1,
			maxSlides: <?php if (config("RealDevice") == 1) echo 100; else if (config("RealDevice") == 2) echo 10; else echo 1;?>,
			slideMargin: 10,
			auto: true,
			infinite: false,
			element: 'bxslider-bingo-rooms'
		}
	});
	
</script>


<?php edit($this->controller,'bingo-rooms-slider'); ?>
<ul class="bxslider bxslider-bingo-rooms">

	<?php @$this->repeatData($this->content['bingo-rooms-slider'], 1, "_partials/bingo-rooms/bingo-room-slide.php");?>

</ul>