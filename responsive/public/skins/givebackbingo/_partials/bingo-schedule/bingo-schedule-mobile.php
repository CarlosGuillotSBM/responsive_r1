<script type="application/javascript">
    skinModules.push({id: "BingoScheduler"});
</script>

<!-- CONTENT HOLDER WRAP MOBILE -->
<div class="holder-mobile">

	<!-- Title -->
	  <div class="content-holder__title">
	<h1>BINGO SCHEDULE</h1>
</div>
	<!-- Content Wrap -->
	<div class="content-holder-mobile bingo-schedule-mobile ui-bingo-carousel-container">
		<!-- <ul class="bxslider bxslider-bingo"> -->
		<ul data-bind="foreach: BingoScheduler.openedRooms, visible: BingoScheduler.openedRooms().length > 0">
			<?php include "_partials/bingo-schedule/bingo-schedule-mobile-slide.php" ?>
		</ul>
	</div>
	<!-- /Content Wrap -->

</div>
<!-- /CONTENT HOLDER WRAP MOBILE -->