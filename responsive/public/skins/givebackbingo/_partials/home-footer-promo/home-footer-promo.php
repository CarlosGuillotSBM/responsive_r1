<div class="home-footer-promo">
	<div class="promo-banner__inner">
		<!-- JOIN NOW BUTTON -->
		<div class="joinnow">
			<a class="button postfix registercta" data-nohijack="true" href="/register/">Join Now</a>
		</div>
		<!-- OFFER -->
		<div class="promo-banner__offer">
			<h1>FREE £5</h1>
			<p>NO DEPOSIT REQUIRED</p>
		</div>
		<!-- BANNER -->
		<a href="/register/" data-hijack="true"><img src="/_images/home-footer-promo/foreground.png"></a>
	</div>
</div>