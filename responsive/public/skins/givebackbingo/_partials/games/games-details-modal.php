<div class="game-detail-modal">

    <!-- GAME TITLE -->
    <a class="game-detail-wrap__title-bar">
        <h1 data-bind="text: GameLauncher.gameTitle" class="ui-game-title"></h1>
    </a>
    
    <!-- GAME CONTENT -->
    <div class="game-detail-content">

        <!-- GAME IMAGE + GAME DETAILS LINK -->
        <div class="game-detail-content__left">
            <span data-bind="attr: { 'data-gameid': GameLauncher.gameId, 'data-demoplay': GameLauncher.demoPlay }" data-nohijack="true" class="games-info__try">
                <img data-bind="attr: { src: GameLauncher.icon }">

                <!-- IMAGE CTA -->
                <div class="game-cta">
                     <a href="/register/" data-bind="visible: GameLauncher.demoPlay" alt="Join Now" class="join-now">Join Now</a>
                     <a href="/register/" data-bind="visible: !GameLauncher.demoPlay()" alt="Join Now" class="join-now-full">Join Now</a>
                </div>
            </span>
        </div>

        <!-- CTA'S -->
        <div class="game-detail-content__right">
            <div class="login-box--loginpage">
                <?php include'_partials/login/login-box.php' ?>
            </div>
        </div>

    </div>

    <!-- GAME DETAILS -->


    <div class="game-detail-content__right">
        <article  class="game-detail__description">
            <h3>Game description</h3>
            <p data-bind="html: GameLauncher.gameDescription"></p>
            <a data-bind="attr: { href: GameLauncher.detailsUrl() + '/' }">Read More</a>
        </article>

    </div>

 </div>
