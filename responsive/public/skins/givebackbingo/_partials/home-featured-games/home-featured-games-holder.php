<div class="content-holder home-featured-games-holder">
	<!-- content holder title -->
	<!-- content holder content area -->
	<div class="content-holder__content ">
		<!-- content holder content full width inner holder-->
		<!-- <div class="content-holder__content_full-width"></div> -->
		
		<!-- content holder content left inner holder-->
		<div class="content-holder__content_left">
			<div class="content-holder__title">
				<h1>SLOTS</h1>
				<!-- content holder title right link -->
				<span class="content-holder__title__link"><a href="/games/">View All &raquo;</a></span>
			</div>
			
			<div class="content-holder__inner">
				<?php include "_partials/home-featured-games/home-featured-games-inner.php"; ?>
			</div>
			
		</div>
		<!-- content holder content right inner holder-->
		<div class="content-holder__content_right">
	<div class="content-holder__title">
					<h1 style="width: 50%;">CHARITY</h1>
                    <!-- content holder title right link -->
                    <span class="content-holder__title__link"><a href="/community/charity">View All &raquo;</a></span>
				</div>
				<div class="content-holder__inner latest-offer-promo ui-scheduled-content-container">
					<!-- Edit point Banner -->
					<?php edit($this->controller,'home-latest-offer'); ?>
				
				<?php @$this->getPartial($this->content['home-latest-offer'],1); ?>
					<!-- Edit point Banner -->
				</div>
	</div>
</div>
</div>