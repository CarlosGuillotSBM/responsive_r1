<script>

    skinPlugins.push({
        id: "FeaturedGames",
        options: {
            global: true
        }
    });

</script>
<?php
    $this->gamesCategory = array();
    $i = 1;
    do {
        if (count($this->games) > 0) {
            $gc = [
                'id' => 'home-featured-panel' . $i,
                'index' => $i++,
                'Games' => $this->games,
                'Category' => $this->category
            ];
            $this->gamesCategory[] = $gc;
        }
    } while ($this->GetNextCategory() && $i < 7);
?>

<div class="home-featured-games">
    <!-- Bingo Category Tabs -->
    <div class="home-featured-games__tabs">
        <dl class="tabs" data-tab>

<?php
    foreach ($this->gamesCategory as $gc) {
?>

            <dd<?php if($gc['index']==1){echo ' class="active"';}?>><a class='lazy-load-on-click' href="#<?php echo $gc['id'];?>"><?php echo $gc["Category"]["Description"];?></a></dd>
            
<?php };?>
        </dl>
    </div>

    <div class="tabs-content">

<?php
    foreach ($this->gamesCategory as $gc) {
?>
                <div class="ui-featured-tab content <?php if($gc['index']==1){echo 'active';}?>" id="<?php echo $gc['id']; ?>">
                    <?php foreach($gc["Games"] as $row) {?>
                    
                        <div ><?php include '_global-library/_editor-partials/game-icon.php'; ?></div>
                        
                    <?php }?>
                </div>
                
        <?php };?>
            
    </div>
</div>