<div class="welcome-post-reg__modal">

    <!-- TOP HEADER CLOSE MODAL -->
    <a href="/start/" data-gtag="Welcome Start" target="_top" >
      <div class="header-wrap">
        <div class="header-container">
          <div class="logo"></div>
            <p class="header-username">Welcome <span data-bind="text: username">...</span>, thanks for joining!</p>
        </div>
      </div>
    </a>

    <!-- MIDDLE CONTENT -->
    <div class="main-content welcome-post-reg__content">
      <!--     <div class="inline_logo"></div> -->
      <!-- BONUSES -->
      <section class="bonus">
        <h2>You have 2 bonuses ready to use now – ENJOY BOTH</h2>
        <ul>
          <!-- 1st bonus -->
          <li>
            <div class="claim-wrap">
              <div class="claim-container"><span>£5 FREE BINGO BONUS</span></div>
              <a href="#" class="play-cta" data-bind="click: playBingo" data-gtag="Welcome Play Bingo">Play Bingo</a>
            </div>
            <div class="icon">
              <span class="betty"><img src="/_images/welcome/betty.png" alt=""></span>
            </div>
          </li>
          <script type="text/javascript">
          var sbm = sbm || {};
          sbm.games = sbm.games || [];
          sbm.games.push({
          "id"    : "spinata-grande",
          "icon"  : "/_global-library/_upload-images/games/list-icons/spinata-grande-slots-game.jpg",
          "bg"    : "\/_global-library\/_upload-images\/games\/backgrounds\/spinata-grande-slots-background.jpg",
          "title" : "Spinata Grande",
          "type"  : "game",
          "desc"  : "Spi\u00f1ata Grande\u2122 is a fun-filled game with spinning pi\u00f1atas and festive features! This 5-reel, 4-row, 40-line video slot contains Colossal symbols, a Mini-Slot feature and Colossal Wild in Free Spins.Spi\u00f1ata Grande\u2122 is a smashing fiesta with candy, confetti and colossal fun - so take this rich treat for a spin!",
          "thumb" : "/_global-library/_upload-images/games/screenshots/spinata-grande-slots.jpg",
          "detUrl": "\/slots\/game\/spinata-grande",
            "demo": 1 });
          </script>
          <!-- 2nd bonus -->
          <li>
            <div class="claim-wrap">
              <div class="claim-container"><span>20 FREE SPINS ON Spinata Grande</span></div>
              <a href="#" class="play-cta" data-bind="click: GameLauncher.openGame" data-gameid="spinata-grande" data-gtag="Welcome Spin Now">SPIN NOW</a>
            </div>
            <div class="icon">
              <span class="spinata"><img src="/_images/welcome/spinata.png" alt=""></span>
            </div>
          </li>
        </ul>
      </section>
      <!-- OFFERS -->
      <section class="deposit">
        <h2>Plus, your 1st deposit offer is waiting – CHOOSE ONE</h2>
        <ul>
          <!-- 1st offer -->
          <li>
            <div class="claim-wrap">
              <div class="claim-container"><span>300% Bingo Bonus + 100 Free Spins</span></div>
              <a href="/cashier/" class="deposit-cta" data-gtag="Welcome Left Deposit">DEPOSIT TO CLAIM</a>
            </div>
            <div class="icon">
              <span class="betty"><img src="/_images/welcome/betty-yay.png" alt=""></span>
            </div>
          </li>
          <!-- 2nd offer -->
          <li>
            <div class="claim-wrap">
              <div class="claim-container"><span>100% SLOTS BONUS + 100 Free Spins</span></div>
              <a href="/cashier/" class="deposit-cta" data-gtag="Welcome Right Deposit">DEPOSIT TO CLAIM</a>
            </div>
            <div class="icon">
              <span class="imagery4"><img src="/_images/switch-old-to-new/luke-hands-up.png" alt=""></span>
            </div>
          </li>
        </ul>
      </section>
      <a href="/cashier/" data-gtag="Welcome Footer Cashier Link" class="no-bonus-link">You can also choose to play without taking a bonus. Find this option in the <span>cashier.</span></a>
      <!-- LOCK -->
      <!--     <section class="lock">
        <ul>
          <li>
            <div class="claim-wrap">
              <div class="claim-container"><span>2ND DEPOSIT BONUS 150% BINGO BONUS</span></div>
            </div>
            <div class="icon"></div>
          </li>
          <li>
            <div class="claim-wrap">
              <div class="claim-container"><span>2ND DEPOSIT BONUS 2ND DEPOSIT BONUS</span></div>
            </div>
            <div class="icon"></div>
          </li>
        </ul>
      </section> -->
      
    </div>
    <!-- END MIDDLE CONTENT -->
  </div>