<!-- <div class="loader"><img src="../_imgages/loader/ajax-loader.gif"></div> -->
<h3 class="playerdetails__header">Your Personal Details</h3>
<div id="playerdetails">


	<!--
	Update Security and Contact Details Column
	/////////////////////////////////////////////////////////////////////////////////////////////
	-->
	
	<!-- <div class="myaccount-details__column__contact-details" > -->
		
		<!-- Header  -->
		<!-- <div class="contact-details__header">Update Security and <br>Contact Details</div> -->
		
		<!-- Phone Contact -->
<!-- 		<div class="contact-details__phone">
			<input class="landline" placeholder="Landline phone" data-bind="value: PersonalDetailsNew.landPhone" type="text" maxlength="50">
			<input class="extention" placeholder="Extention" data-bind="value: PersonalDetailsNew.landPhoneExt" type="text" maxlength="10">
		</div> -->
		
		<!-- Mobile -->
<!-- 		<div>
			<input class="mobile" placeholder="Mobile phone" data-bind="value: PersonalDetailsNew.mobilePhone" type="text" maxlength="50">
			<span data-bind="visible: PersonalDetailsNew.invalidUkMobile" class="error">Mobile phone is 11 digits long, begins with 071 to 075 inclusive or 077 to 079 inclusive.</span>
			<span data-bind="visible: PersonalDetailsNew.invalidMobile" class="error">Please specify a valid phone number</span>
		</div> -->
		
		
		<!-- Security Question -->
		<!-- 		<select data-bind="options: PersonalDetailsNew.securityQuestions,
			optionsValue: 'SecurityQuestionID',
			optionsText: 'DisplayDescription',
			value: PersonalDetailsNew.securityQuestion">
		</select> 

			<option value="-1" selected="selected">Choose security question</option>
			<option value="1" class="ticked">Option 1</option>
			<option value="2">Option 2</option>
			<option value="3">Option 3</option>
			<option value="4">Option 4</option>
			<option value="5">Option 5</option>

	-->
		<!-- ko if: PersonalDetailsNew.receivedPersonalDetails -->
<!-- 		<select id="cd-dropdown" class="cd-select" data-bind=" options: PersonalDetailsNew.securityQuestions,
			optionsValue: 'SecurityQuestionID',
			optionsText: 'DisplayDescription',
			value: PersonalDetailsNew.securityQuestion,
			optionsAfterRender: PersonalDetailsNew.selectOption" type="checkbox" name="securityQuestion">

		</select>
		<span data-bind="visible: PersonalDetailsNew.invalidSecurityQuestion" class="error">You need to pick a question</span> -->
		<!-- /ko -->
		<!-- Your Security answer -->
<!-- 		<input placeholder="Type security answer" data-bind="value: PersonalDetailsNew.securityAnswer" type="password" maxlength="50">
		<span data-bind="visible: PersonalDetailsNew.invalidSecurityAnswer" class="error">This field is required</span> -->

		<!-- ko if: PersonalDetailsNew.wantsSMSBoxVisible -->
<!-- 		<select id="cd-dropdown-sms-bonus" class="cd-select" 
			data-bind=" 
						value: PersonalDetailsNew.wantsSMS, 
						selected: PersonalDetailsNew.wantsSMS" type="checkbox" name="WantsSms">
			<option value="1" selected="selected">Text me regular bonuses / offers</option>
			<option value="0">No I don’t want to receive any</option>
		</select> -->
		<!-- /ko -->

		<!-- Create new Password -->
<!-- 		<div>
			<input class="password" data-bind="value: PersonalDetailsNew.newPassword" type="password" maxlength="15" placeholder="Create a new password">
			<span data-bind="visible: PersonalDetailsNew.invalidNewPassword" class="error">Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces</span>
			<span data-bind="visible: PersonalDetailsNew.UsernameAndPassTheSame" class="error">Username and Password cannot be the same</span>
		</div>
		<div>
			<input class="password" data-bind="value: PersonalDetailsNew.repeatPassword" type="password" maxlength="15" placeholder="Repeat your new password">
			<span data-bind="visible: PersonalDetailsNew.passwordsDontMatch" class="error">Passwords do not match</span>
		</div>	 -->	

		<!-- Current Password -->
<!-- 		<div class="contact-details__current-password">
			<input data-bind="value: PersonalDetailsNew.currentPassword" type="password" maxlength="15" placeholder="Enter current password">
			<span data-bind="visible: PersonalDetailsNew.invalidCurrentPassword" class="error">This field is required</span>
			<a data-bind="click: PersonalDetailsNew.updateDetails" href="" class="button expand">Update Details</a>
		</div>
	</div> -->



	<!--
	Registered Details Column
	/////////////////////////////////////////////////////////////////////////////////////////////
	-->
	<div class="myaccount-details__column__registered-details">
		
		<!-- Header  -->
		<div class="registered-details__header">
			<p class="title">Your Registered Details</p>
			<p class="warning">* Not updateable on site</p>
		</div>
		
		<!-- Personal Details  -->
		<div class="registered-details__personal-details">
			
			<!-- Change Password -->
			<div class="registered-details__password">
				<label>Change Password:</label>
				<input class="password" data-bind="value: PersonalDetailsNew.newPassword" type="password" maxlength="15" placeholder="xxxxxx">
			</div>

			<!-- Username -->
			<div class="registered-details__username">
				<label>Username:</label>
				<input data-bind="value: username" type="text" maxlength="50" placeholder="" disabled="disable">
			</div>

			<!-- Title -->
			<div class="registered-details__title">
				<label>Title:</label>
				<input data-bind="value: PersonalDetailsNew.title" type="text" maxlength="50" placeholder="" disabled="disable">
			</div>

			<!-- First Name -->
			<div class="registered-details__first-name">
				<label>First Name:</label>
				<input data-bind="value: PersonalDetailsNew.firstName" type="text" maxlength="50" placeholder="" disabled="disable">
			</div>

			<!-- Last Name -->
			<div class="registered-details__last-name">
				<label>Last Name:</label>
				<input data-bind="value: PersonalDetailsNew.lastName" type="text" maxlength="50" placeholder="" disabled="disable">
			</div>

			<!-- Extention -->
			<div class="registered-details__extention">
				<label>Extention:</label>
				<input class="extention" placeholder="Extention" data-bind="value: PersonalDetailsNew.landPhoneExt" type="text" maxlength="10">
			</div>

			<!-- Landline -->
			<div class="registered-details__landline">
				<label>Landline:</label>
				<input class="landline" placeholder="Landline phone" data-bind="value: PersonalDetailsNew.landPhone" type="text" maxlength="50">
			</div>

			<!-- Mobile -->
			<div class="registered-details__mobile">
				<label>Mobile:</label>
				<input class="mobile" placeholder="Mobile phone" data-bind="value: PersonalDetailsNew.mobilePhone" type="text" maxlength="50">
			</div>

			<!-- Email  -->
			<div class="registered-details__email">
				<label>Email:</label>
				<input data-bind="value: PersonalDetailsNew.email" type="text" maxlength="100" placeholder="" disabled="disable">
			</div>


			<!-- Opt In / If the user is not currently opted in then we show this the whole button is clickle 
			and once clicked will show a tick in the box -->

			<!-- ko if: !PersonalDetailsNew.wantsSMSBoxVisible() -->
			<label class="opt-in" data-bind="click: PersonalDetailsNew.toggleSMS">Text me regular bonuses & offers <span data-bind="css: { inactive: PersonalDetailsNew.wantsSMS() == 0, active: PersonalDetailsNew.wantsSMS() == 1 }"></span></label>
			<!-- /ko -->

			<!-- ko if: PersonalDetailsNew.wantsSMSBoxVisible -->
			<!-- <select id="cd-dropdown-sms-bonus" class="cd-select" 
				data-bind=" 
							value: PersonalDetailsNew.wantsSMS, 
							selected: PersonalDetailsNew.wantsSMS" type="checkbox" name="WantsSms">
				<option value="1" selected="selected">Text me regular bonuses / offers</option>
				<option value="0">No I don’t want to receive any</option>
			</select> -->
			<!-- /ko -->


			<!-- Email bonus -->
		
			<!-- Opt In / If the user is not currently opted in then we show this the whole button is clickle 
			and once clicked will show a tick in the box -->

			<!-- ko if: !PersonalDetailsNew.wantsEmailBoxVisible() -->
			<label class="opt-in" data-bind="click: PersonalDetailsNew.toggleEmail">Email me regular bonuses & offers <span data-bind="css: { inactive: PersonalDetailsNew.wantsEmail() == 0, active: PersonalDetailsNew.wantsEmail() == 1 }"></span></label>		
			<!-- /ko -->

			<!-- ko if: PersonalDetailsNew.wantsEmailBoxVisible -->
			<!-- <select id="cd-dropdown-email-bonus" class="cd-select" 
						data-bind=" 
								value: PersonalDetailsNew.wantsEmail, 
								selected: PersonalDetailsNew.wantsEmail" type="checkbox" name="WantsEmail">
				<option value="1" selected="selected">Email me regular bonuses / offers</option>
				<option value="0">No I don’t want to receive any</option>
			</select> -->
			<!-- /ko -->
			<!-- SMS bonus -->

			<!-- Opt In / If the user is not currently opted in then we show this the whole button is clickle 
			and once clicked will show a tick in the box -->

			
			<!-- Address Details -->
			<div class="registered-details__address">

				<!-- Street -->
				<div class="registered-details__street">
					<div>
						<label>Address Line 1:</label>
						<input data-bind="value: PersonalDetailsNew.address1" type="text" maxlength="100" placeholder="" disabled="disable">
					</div>
					<div>
						<label>Address Line 2:</label>
						<input data-bind="value: PersonalDetailsNew.address2" type="text" maxlength="100" placeholder="" disabled="disable">
					</div>
				</div>
				
				<!-- City -->
				<div>
					<label>City:</label>
					<input data-bind="value: PersonalDetailsNew.city" type="text" maxlength="50" placeholder="" disabled="disable">
				</div>
				<div>
					<label>County / State:</label>
					<input data-bind="value: PersonalDetailsNew.state" type="text" maxlength="50" placeholder="" disabled="disable">
				</div>
				
				<div>
					<label>Post Code:</label>
					<input data-bind="value: PersonalDetailsNew.postcode" type="text" maxlength="15" placeholder="" disabled="disable">
				</div>
				<div>
					<label>Country:</label>
					<input data-bind="value: PersonalDetailsNew.countryName" type="text" placeholder="" disabled="disable">
				</div>
			</div>
			<!-- / Address Details -->


			<!-- Support Details -->
			<div class="registered-details__support">
				<p>* If you need to change any of the details above, please contact support using a method below.</p>
				<span>Email: <a href="mailto:support@givebackbingo.com">support@givebackbingo.com</a></span>
				<span class="chat">Live Chat: <a href="#">Launch chat window</a></span>
				<span itemprop="telephone">Freephone: <a href="tel:0800 901 2511">0800 901 2511</a></span>
				<span itemprop="telephone">Phone: <a href="tel:0203 700 1157">0203 700 1157</a></span>
			</div>

		</div>
		

	</div>

	<!-- Edit Password Partial -->
	<?php include'_partials/start-skin-only/start-myaccount-details-edit-password.php'; ?>

	<!-- Edit Landline Partial -->
	<?php include'_partials/start-skin-only/start-myaccount-details-edit-landline.php'; ?>

	<!-- Edit Extention Partial -->
	<?php include'_partials/start-skin-only/start-myaccount-details-edit-extention.php'; ?>

	<!-- Edit Mobile Partial -->
	<?php include'_partials/start-skin-only/start-myaccount-details-edit-mobile.php'; ?>

</div>
