<script type="application/javascript">
    skinModules.push({ id: "RedeemPromos" });
</script>
<div>

    <div class="myaccount-details__column claim-code">
        <h3>Claim code</h3>

        <div class="start-claim-code">
        	<h2>Do you have a claim code?</h2>
            <form autocomplete="off">
                <input data-bind="value: RedeemPromos.promo" placeholder="Enter Claim Code" type="text" maxlength="50" autocomplete="off">
            </form>
            <span style="display: none" data-bind="visible: RedeemPromos.error, text: RedeemPromos.error" class="error"></span>
            <a data-bind="click: RedeemPromos.redeemPromo" href="" class="button expand claim-button">SUBMIT</a>
            <p class="claim-desposit-code-info">* If you have a deposit promo code, please enter it in the cashier when you make a deposit</p>
        </div>
    </div>
</div>