<?php
$roomParameter = (!isset($_REQUEST["RoomName"]) || $_REQUEST["RoomName"] == "Lobby") ? "Room=Lobby" : "RoomName=".$_REQUEST["RoomName"];
$room = @$_REQUEST["RoomName"];
$playerId = (!isset($_REQUEST["PlayerID"])) ? @$_COOKIE["PlayerID"] : @$_REQUEST["PlayerID"];
$alias = (!isset($_REQUEST["Alias"])) ? @$_COOKIE["username"] : @$_REQUEST["Alias"];
$sessionId = (!isset($_REQUEST["SessionID"])) ? @$_COOKIE["SessionID"] : @$_REQUEST["SessionID"]; 

$file = substr(__FILE__,7);
$glo = array ("Application" => "agsSkins");
$module = substr($file,strrpos($file,"\\")+1);         //this php file
require_once("D:/WEB/inc/vars.php"); //global variables 
require_once("D:/WEB/inc/genFuncs.php");   //general functions

global $glo;

$globalLiveHelp = $glo["Skin"]["LiveHelp"];
$globalLiveHelpTrigger = $glo["Skin"]["LiveHelpTrigger"];

switch(strtolower($_SERVER["SERVER_NAME"])) {
    case 'givebackbingo.dagacubedev.net':
        header("location: http://givebackbingo.dagacubedev.net/html5/bingo/public/givebackbingo/index.php?". $roomParameter .
               "&PlayerID=" . $playerId . "&SessionID=" . $sessionId . "&Alias=" . $alias . "&globalLiveHelp=" . urlencode($globalLiveHelp) . "&globalLiveHelpTrigger=" . urlencode($globalLiveHelpTrigger));
        break;
    case 'www.givebackbingo.com':
        header("location: /html5/bingo/public/givebackbingo/index.php?". $roomParameter .
            "&PlayerID=" . $playerId . "&SessionID=" . $sessionId . "&Alias=" . $alias . "&globalLiveHelp=" . urlencode($globalLiveHelp) . "&globalLiveHelpTrigger=" . urlencode($globalLiveHelpTrigger));
        break;
    default:
        exit("Sorry there is a system error, if the error persists contact support.");
        break;
}

exit;
