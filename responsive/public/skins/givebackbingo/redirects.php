<?php

// -----------------------------------
// old luckypants affiliate re-directs
$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';

if(preg_match("[^double-slots-bonus$|^no-deposit-required$|^30-free-as-seen-on-tv$|^play-with-40-as-seen-on-tv$|^play-with-40-as-seen-on-tv$|^30-free-plus-15-free-spins$|^300-bonus$|^free-bonus-rain$]", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/affiliates/" . $path . $queryString);
    exit;
}

if(preg_match("[^lucky-scratch$|^tv-ad$|^free-spins$|^summer-offer$]", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/" . $path . $queryString);
    exit;
}

if(preg_match("#^\/general-promotional-terms-and-conditions\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /terms-and-conditions/" . $path . $queryString);

    exit;
}

if(preg_match("#^\/giveaway\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/great-british-giveaway/". $queryString);
    exit;
}

if(preg_match("#\/slots-game\/#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: " . preg_replace("#\/slots\/#", "/slot-games/", $path). $queryString);
    exit;
}

if(preg_match("#^\/50k\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/50k/". $queryString);
    exit;
}

if(preg_match("#^\/slot-games\/scratch-cards\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slot-games/scratch-and-arcade". $queryString);
    exit;
}

if(preg_match("#^\/win\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/affiliates/scratch-card/". $queryString);
    exit;
}

if(preg_match("#^\/spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporary');
    header("Location: /media/affiliates/new-welcome-offer/". $queryString);
    exit;
}

if(preg_match("#^\/media/ppc/affiliates/scratch-card\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

//R10-204
if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/givebackbingo/". $queryString);
    exit;
}
//R10-1009
if(preg_match("#^\/excluded\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/excluded/". $queryString);
    exit;
}

if(preg_match("#^\/slot-games\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots". $queryString);
    exit;
}

if(preg_match("#^\/slot-game\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots". $queryString);
    exit;
}