<!-- MAIN CONTENT AREA -->
<div class="main-content privacy-policy-content">

    <!-- TITLE WRAP -->
    <div class="section-left__title">
      <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>

  <!-- TWO COLUMNS LAYOUT -->
  <div class="two-columns-content-wrap__left__title">
    
    <!-- LEFT COLUMN -->
    <div class="two-columns-content-wrap__left">
  <div class="theme--content--header"></div>
      <!-- WRAP CONTENT LEFT -->
      <div class="two-columns-content-wrap__left__inner" itemscope itemtype="http://schema.org/SoftwareApplication">
        <!-- COLLAPSE -->
        <?php edit($this->controller,'privacy-policy-details'); ?>
        <ul class="accordion accordion-wrap">
          <?php @$this->repeatData($this->content['privacy-policy-details'], 1, "_global-library/partials/common/text-accordion.php");?>
        </ul>

      </div>

    </div>

    <?php if (config("RealDevice") != 3) { ?>

      <!-- RIGHT COLUMN -->
      <div class="two-columns-content-wrap__right">
        <div class="theme--content--header"></div>
        <?php edit($this->controller,'privacy-policy__side-content'); ?>
        <?php @$this->repeatData($this->content['privacy-policy__side-content']); ?>
        <div class="clearfix"></div>
      </div>
      <!-- /RIGHT COLUMN -->

    <?php } ?>

  </div>
  <!-- /TWO COLUMNS LAYOUT -->

</div>
<!-- /MAIN CONTENT AREA -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>

