<!-- MIDDLE CONTENT -->
<div class="main-content error-content">

	<!-- Title -->
	<!-- <div class="section-left__title">
		<h4><span>ERROR 404</span></h4>
	</div> -->

	<section class="section">
		<div class="error-page">
			<div class="error-page__image error-page__text">
				<!-- <img src="/_images/common/error-404/error-404.png"> -->
				<h1 class="error-page__error">	
					<img src="/_images/logo/GBB-logo-web.png">
					</h1>
			</div>
			<div class="error-page__text">
				<h1 class="error-page__error">
				WHOOPS!
				</h1>
				<p class="error-page__msg">
					THE PAGE YOU REQUESTED DOES NOT EXIST.
				</p>
				<div class="error-page__suggestion clearfix">
					<p><a data-bind="attr: { href: (!validSession()) ? '/' : '/start/'}">Please try again. Thanks!</a></p>
					<!-- <div class="games-list__grid games-list__promoted-games "></div> -->
					<!-- <a class="registercta" href="/register/" data-hijack="true">JOIN NOW</a> -->
				</div>
			</div>
		</div>
	</section>
		
</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
	<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>