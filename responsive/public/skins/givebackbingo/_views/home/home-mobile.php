<!-- MOBILE CASHBACK -->
<div style="display: none;" data-alert class="mobile-cashback alert-box" data-bind="visible: validSession">
    <a href="#" class="close" data-bind="visible: ClaimCashback.cbAmount() > 0" >X</a>
    <?php include'_global-library/partials/start/start-claim-cashback.php'; ?>
</div>
<!-- / MOBILE CASHBACK -->

<!-- MOBILE HOME VIEW -->
<div class="home-mobile">
    <!-- offer -->
    <div class="home-main-slider desktop-mobile">
        <ul class="" data-width="500">
            <!-- Hero Area -->
            <li>
                <a data-gtag="Join Now,Web Promo" href="/register/">
                    <img class="banner" src="/_images/home-sliders/mobile/main-offer-new.png"></a>
                <div class="slider-text-link"> <?php edit($this->controller,'hero'); ?>
         <?php @$this->getPartial($this->content['hero'],1, "_partials/banner/homepage-hero-banner.php"); ?></div>

            </li>

        </ul>
    </div>

    <!-- Featured Games Box with tabbed content -->
    <div class="home-mobile-carousels" data-bind="css: { 'logged-in': validSession() }">
        <?php edit($this->controller,'home-carousels'); ?>
        <?php @$this->repeatData($this->content['home-carousels']);?>
    </div>

    <!-- Charity -->
    <?php include'_partials/home-community-section/home-community-charity.php'; ?>

    <!-- bingo schedule -->
    <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>

    <!-- Community Section Holder Row -->
    <?php include'_partials/home-community-section/home-community-holder.php'; ?>

    <?php
        $seoKey = "seo-content-2";
        include'_global-library/partials/home-seo/mobile.php';
    ?>

    <!-- footer bar -->
    <div class="footer-bar-mobile" data-bind="visible: validSession()" style="display: none">
        <a data-hijack="true" data-bind="click: playBingo" class="cta-join">PLAY BINGO</a>
        <a data-hijack="true" href="/cashier/" class="cta-cashier">DEPOSIT NOW</a>
    </div>
</div>
<!-- /MOBILE HOME VIEW -->
