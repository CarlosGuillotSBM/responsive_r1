<script type="application/javascript">
  skinModules.push({
    id: "OptIn",
    options: {details : true}
  });
</script>

<div class="two-columns-content-wrap">
  <!-- HOME MAIN CONTENT AREA -->
  <section class="section">
    <!-- PROMOTIONS CONTENT -->
    <div class="middle-content__box">
      <div class="clearfix"></div>

      <?php edit($this->controller,'promotions'); ?>
      
      <!-- promotions list -->
      <!-- Promotions detail -->
      <div class="promotions-detail">
       

        <article class="promotion--featured">

          <div class="two-columns-content-wrap">
            
            <!-- LEFT COLUMN -->
            <div class="two-columns-content-wrap__left">

              <div class="promotion__content__wrapper">

              <div class="theme--content--header">  <h1 class="section-left__title"><?php echo $this->content["Title"];?></h1></div>

                    <div class="theme--content--container">
                <div class="promotion__content__img__wrapper">                    
                    <a data-hijack="true" data-bind="click: playBingo">
						          <img src="<?php echo $this->content["Text8"] != '' ? $this->content["Text8"] : $this->content["Image"]?>" alt="<?php //echo $this->content["Alt"]?>">
					         </a>
                </div>
                
                <!-- TOP CTA'S -->
                <div class="promo__container__row">
                    <a data-hijack="true" style="display: none" class="promo__container__button cta-opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>

                    <!-- span to be removed by javascript - DO NOT REMOVE!! -->
                    <span></span>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'PLAY NOW'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'PLAY NOW'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'PLAY NOW'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'PLAY NOW'; else echo $this->content['Text2']; ?></a>
                    <?php }?> 
                    <!--a data-hijack="true" style="display: none" data-bind="visible: !validSession()" id="join-promo" href="/register/" class="promo__container__button cta-join">JOIN NOW</a>
                    <a data-hijack="true" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join">PLAY NOW</a-->
                </div> 

                <!-- CONTENT -->
                <div class="promo__container__content">
                  <?php echo $this->content["Body"]; ?>
                </div>

                <!-- PROMO FOOTER NAV MOBILE -->
                <div class="footer-nav__container">
                    <a data-hijack="true" style="display: none" data-tournament="<?php echo $this->content['DetailsURL'] ?>" class="cta-opt-in">OPT-IN</a>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="cta-join"><?php if($this->content['Text3'] === '') echo 'PLAY NOW'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="cta-join"><?php if($this->content['Text2'] === '') echo 'PLAY NOW'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'PLAY NOW'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'PLAY NOW'; else echo $this->content['Text2']; ?></a>
                    <?php }?>                     
                    <!--a data-hijack="true" style="display: none" data-bind="visible: !validSession(), click: playBingo" class="cta-join">PLAY NOW</a>
                    <a data-hijack="true" style="display: none" data-bind="visible: validSession(), click: playBingo" class="cta-join">PLAY NOW</a-->
                    <a data-hijack="true" href="/promotions/<?php echo $this->getNextPromoURL($this->content["DetailsURL"]); ?>" class="cta-next-promo">NEXT PROMO ></a>
                </div>

                  <!-- TOP CTA'S -->
                <div class="promo__container__row">
                    <a data-hijack="true" style="display: none" class="promo__container__button cta-opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>

                    <!-- span to be removed by javascript - DO NOT REMOVE!! -->
                    <span></span>

                    <a data-hijack="true" style="display: none" data-bind="visible: !validSession()" id="join-promo" href="/register/" class="promo__container__button cta-join">JOIN NOW</a>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'PLAY NOW'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'PLAY NOW'; else echo $this->content['Text2']; ?></a>
                    <?php }?>  
                </div> 

                <!-- Accordion -->
                <dl id="ui-promo-tcs" class="promotion__terms-conditions accordion" data-accordion>
                  <dd class="accordion-navigation active">
                    <a href="#panelTC" class="accordion-navigation__toggle">Terms &amp; Conditions</a>
                    <div id="panelTC" class="content active">
                      <?php echo @$this->content["Terms"];?>
                    </div>
                  </dd>
                </dl>
                <!-- /Accordion -->
				
              </div>
              </div>
            </div>
            <!-- /LEFT COLUMN -->


              <?php if (config("RealDevice") != 3) { ?>
              <!-- right column -->
              <div class="two-columns-content-wrap__right">
                  <div class="theme--content--header"> </div>
                  <div class="theme--content--container ui-scheduled-content-container">
                      <?php edit($this->controller,'promotion-details__side-content',$this->action); ?>
                      <?php @$this->repeatData(@$this->sideBanners); ?>
                  </div>

                  <div class="content-holder__content_right">
                      <!-- content holder title right link -->
                      <?php
                      $titleHeaderHeight = 25;
                      $slideHeight = 95;
                      $slidesToShow = 3;
                      ?>
                      <script type="application/javascript">
                          skinPlugins.push({
                              id: "ProgressiveSlider",
                              options: {
                                  slidesToShow: <?php echo $slidesToShow ?>,
                                  mode: "vertical"
                              }
                          });
                      </script>
                      <div class="content-holder__inner progressive-jackpot-wrap">
                          <?php edit($this->controller,'promo-detail-progressives'); ?>
                          <div class="progressive-jackpots__wrapper">
                              <div class="progressive-jackpots">
                                  <?php
                                  if(config("RealDevice") < 3) {
                                      @$this->repeatData($this->promotions['promo-detail-progressives'],1);
                                  }
                                  ?>
                              </div>
                          </div>
                      </div>

                  </div>

              </div>
              <!-- /right column -->
              <?php } ?>

          </div>
        </article>

      </div>
    </div>

  </section>

</div>
<!-- END HOME MAIN CONTENT AREA -->  

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>





