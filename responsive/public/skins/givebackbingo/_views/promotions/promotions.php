<!--wrapper -->
<div class="main-content promotions-content">
    <script type="application/javascript">   
        skinModules.push({
            id: "RadFilter"
        });
    </script>
    <div class="border--styling--wrap middle-content__box">
        <!--PROMOTIONS SECTION -->
        <section class="section promotions">
            <div class="content-holder__title">
                <h1><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
            </div>
            <div class="theme--ui--background--color tabs-content">
                <!-- ALL -->
                <div class="content active" id="promotions-0">
                    <div class="content-holder__inner">
                        <?php $variables = array();
                          $variables["WhichTab"] = 1; ?>

                        <?php edit($this->controller,'promotions'); ?>
                        <?php 
                            if (isset($this->content["promotions"])) {
                                $this->repeatData($this->content["promotions"],1,"_global-library/partials/promotion/promotion.php",$variables); 
                            }
                        ?>
                    </div>
                </div>
                <a class="promotional-tcs" href="/terms-and-conditions/general-promotional-terms-and-conditions/">General Promotional Terms &amp; Conditions</a>
            </div>
            <div class="clearfix"></div>
        </section>
       
    <!-- END PROMOTIONS SECTION -->
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
            <div class="main-content">
            <?php if (config("RealDevice") !== 3) {
                edit($this->controller,'home-seo-content'); 
                @$this->getPartial($this->content['home-seo-content'],1);
            }
            ?> 
             </div> 
<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>