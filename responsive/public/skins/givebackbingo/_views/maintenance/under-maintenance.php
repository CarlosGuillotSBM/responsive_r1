<!DOCTYPE html>
<html>
<head>
	<title>Under Maintenance</title>
<style type="text/css">
	body{
		background-color:white;
		color:#6b57c8;
		text-align: center;
		font-size: 20px;
		font-family: arial;
	}
	h1{
		font-size: 40px;
		color: #f91a8b;
		margin-bottom: -10px;		
	}
	.logo{
		margin-top: 30px;
        width: 385px;
        height: 135px;
	}
	p{
		font-size: 14px;
		margin-top: 20px;
	}
</style>
</head>
<body>
<img src="/_images/logo/GBB-logo-web.png" class="logo">
	<h1>Under Maintenance</h1>
	<p>Sorry, we are offline for just a few minutes. Please come back soon.</p>
</div>
</body>
</html>