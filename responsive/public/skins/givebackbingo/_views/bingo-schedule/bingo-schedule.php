<!-- bingo schedule content -->
<div class="main-content bingo-schedule-content">

<!-- Title -->
<div class="section-left__title">
	<h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
</div>

	 <div class="content-holder bingo-schedule-holder">
	  <?php include "_global-library/partials/bingo/bingo-schedule.php" ?> <!-- home breadcrumbs -->
	</div>


</div>
<!-- /main content -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>
