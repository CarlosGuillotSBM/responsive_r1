<div class="main-content banking-content">

  <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h4><span><?php echo $this->controller; ?></span></h4>
  </div>

  <!-- CONTENT WRAP -->
  <div class="section middle-content__box">
    <div class="theme--content--header"></div>
    <div class="text-partial-content">

        <!-- BANKING LIST -->
        <?php edit($this->controller,'banking-list'); ?>

        <?php @$this->repeatData($this->content['banking-list']);?>

    </div>
  </div>
  <!-- / CONTENT WRAP -->

</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>