<?php edit($this->controller,'splash'); ?>
<div class="splash-content">
  <header class="splash-header">
    <div class="header__yellow-line"></div>
    <div class="row">
      <div class="header__logo">
        <a href="/"><img src="/_images/splash/logo.png" /><span>Bingo Extra</span></a>
        <div class="header__logo__tagline">Always Something Extra</div>
      </div>
      <div class="header__login-join">
       <a href="/login/"> Already joined? <span>Login</span></a>
      </div>
    </div>
  </header>
  <div class="row content">
    <div class="content__main-message">
      <a href="/register/"><img src="/_images/splash/main-message.jpg" /></a>
    </div>
    <div class="content__steps">
      <div class="content__steps__step">
       <a href="/register/"><img src="/_images/splash/step1.png" /></a> 
      </div>
      <div class="content__steps__step">
          <a href="/register/"><img src="/_images/splash/step2.png" /></a>
      </div>
      <div class="content__steps__step">
         <a href="/register/"><img src="/_images/splash/step3.png" /></a> 
      </div>
    </div>
    <div class="content__tagline">
      <div>
        <div>There's always something extra at Bingo Extra!</div>
      </div>
    </div>
    <div class="content__banners">
      <div class="content__banners__banner1">
          <a href="/register/"><img src="/_images/splash/banner1.png" /></a>
      </div>
      <div class="content__banners__banner2">
         <a href="/register/"><img src="/_images/splash/banner2.jpg" /></a>
      </div>
    </div>
  </div>
</div>