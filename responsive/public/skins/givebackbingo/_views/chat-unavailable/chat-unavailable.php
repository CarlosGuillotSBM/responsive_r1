<div class="main-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!-- END breadcrumbs -->
  <section class="section">
    <div class="section-left__title">
      <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>
    <!-- CONTENT -->
    <?php edit($this->controller,'chat-unavailable'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['chat-unavailable']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->