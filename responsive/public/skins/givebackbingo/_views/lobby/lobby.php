<script type="application/javascript">
skinModules.push( { id: "MailInbox" } );
</script>
<!-- Lobby Wrap -->
<div class="middle-content lobby-wrap">
    <!-- Lobby Left Column -->
    <div class="lobby-wrap__left">
        <!-- welcome section -->
        <?php  include'_global-library/partials/start/start-welcome-section.php'; ?>
        <!-- my messages section -->
        <?php  include'_global-library/partials/start/start-my-messages-section.php'; ?>
        <!-- featured games section -->
        <?php  include'_global-library/partials/start/start-bingo-schedule.php'; ?>       
        <!-- featured games section -->
        <?php  include'_global-library/partials/start/start-featured-games-section.php'; ?>
        <!-- my promotions section -->
        <?php  include'_global-library/partials/start/start-my-promotions-section.php'; ?>
    </div>
    <!-- End Left Column -->
    <!-- Right Column -->
    <div class="game-url-seo lobby-wrap__right">
        <div class="two-columns-content-wrap__right">
            <!-- lobby right deposit button -->
            <?php  include'_global-library/partials/start/start-right-deposit-button.php'; ?>
            <!-- lobby right banner -->
            <?php  include'_global-library/partials/start/start-right-banner.php'; ?>
            <!-- lobby right progressives -->
            <?php  include'_global-library/partials/start/start-right-progressives.php'; ?>
            <!-- lobby yesterdays wins -->
            <?php  include'_global-library/partials/start/start-right-yesterdays-wins.php'; ?>
            <!-- lobby right latest winners -->
            <?php  include'_global-library/partials/start/start-right-latest-winners.php'; ?>
        </div>
    </div>
    <!-- End Right Column -->
</div>
<!-- End Lobby Wrap -->