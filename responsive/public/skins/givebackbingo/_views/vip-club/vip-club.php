


<div class="main-content lucky-club-content">
  <!-- Title -->
  <div class="content-holder__content_left">
    <h1><?php echo str_replace('-', ' ', $this->controller_url)?></h1>

  </div>

  <div class="section middle-content__box">

    <article class="text-partial">

      <!-- INTRO -->
      <div class="lucky-club__intro">
        <?php edit($this->controller,'vip-club-intro'); ?>
        <div class="content-template">
          <!-- repeatable content -->
          <?php @$this->repeatData($this->content['vip-club-intro']);?>
        </div>
      </div>

      <!-- CTA -->
      <div class="lucky-club__cta">
          <!-- Play Now -->
          <a class="button--join" data-hijack="true" data-bind="visible: validSession() , click: playBingo" style="display:none;">Play Bingo</a>
          <!-- Login Register Area OUT-->
          <div class="logged-out-buttons" data-bind="visible: !validSession()" style="display:none;">
            <a class="button--join" data-hijack="true" href="/register/">Join Now</a>
          </div>
      </div>

      <!-- Swipe Left/Right Icon -->
      <div class="lucky-club__table-arrows-icon">
        <img src="/_images/common/icons/touch-icon.png" alt="Left and Right Arrows">
      </div>

      <!-- LUCKY CLUB TABLE WRAP -->
      <div class="lucky-club__table-wrap">


        <div class="lucky-club__table">

          <!-- Touch Icon Inside Table -->
          <!-- <div class="lucky-club__table-touch-icon">
            <img src="/_images/common/icons/touch-icon.png" alt="Touch">
          </div> -->


          <!-- Loyalty Icons Row --> 
          <div class="lucky-club__table-row__header">
            <div class="lucky-club__table-row__title">Benefits to enjoy</div>
            <ul class="lucky-club__table-row__loyalty">
              <li><img src="/_images/myaccount-loyalty/0.png" alt="Checkmark"></li>
              <li><img src="/_images/myaccount-loyalty/1.png" alt="Checkmark"></li>
              <li><img src="/_images/myaccount-loyalty/2.png" alt="Checkmark"></li>
              <li><img src="/_images/myaccount-loyalty/3.png" alt="Checkmark"></li>
              <li><img src="/_images/myaccount-loyalty/4.png" alt="Checkmark"></li>
              <li><img src="/_images/myaccount-loyalty/5.png" alt="Checkmark"></li>
            </ul>
          </div>

          <!-- Cashback Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">BonusBack every Friday</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>5%</li>
              <li>10%</li>
              <li>12%</li>
            </ul>
          </div>

          <!--Upgrade Bonus --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Upgrade Bonus</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>      

          <!-- Daily Free Spins Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Daily Free Spins</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>2</li>
              <li>3</li>
              <li>5</li>
              <li>10</li>
            </ul>
          </div>

          <!-- Daily Free Cards Row --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Daily Free Cards</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>2</li>
              <li>3</li>
              <li>5</li>
              <li>10</li>
            </ul>
          </div>   

          <!-- Birthday Bonus Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Birthday Offer</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- DOUBLE Loyalty Points --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">DOUBLE Loyalty Points</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Free Bingo</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo £50 a day Row --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Free Bingo £50 a day</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo £100 a week Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Free Bingo £100 a week</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo £500 a month Row --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Free Bingo £500 a month</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Win a iPad Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Win an iPad</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

        </div>
      </div>
       <!-- /LUCKY CLUB TABLE WRAP -->

        <!-- LOYALTY POINTS TABLES -->
        <div>
        <?php edit($this->controller,'vip-club-points', "", (config("RealDevice") == 1) ? "top: 1200px;" : "top: 800px;"); ?>
          <!-- repeatable content -->
          <?php @$this->repeatData($this->content['vip-club-points']);?>
        </div>


        <!-- Terms &amp; Conditions -->
           <?php edit($this->controller,'vip-club-tcs', "", ""); ?>
        <dl class="lucky-club__terms-conditions accordion" data-accordion="">
            <dd class="accordion-navigation active">
            <a href="#panelTC">Terms &amp; Conditions</a>
            <div id="panelTC" class="content active">

            <!-- CONTENT -->

            <div class="content-template">
              <!-- repeatable content -->
              <?php @$this->repeatData($this->content['vip-club-tcs']);?>
            </div>
          </div>
          </dd>
        </dl>
        <!-- / Terms &amp; Conditions -->
      </article>

    </div>
 

  </div>
<!-- END MIDDLE CONTENT  -->
<div class="main-content">
<?php if (config("RealDevice") !== 3) {
    edit($this->controller, 'vip-clup-seo-content');
    @$this->getPartial($this->content['vip-clup-seo-content'], 1);
}
?> 
 </div>


<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>