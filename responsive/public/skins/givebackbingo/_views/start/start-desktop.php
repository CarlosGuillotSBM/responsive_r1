<!-- games menu -->
<?php // include '_partials/games-menu/games-menu.php'; ?>

<!-- MIDDLE CONTENT -->
<div class="main-content start">
    <div class="start-content">

        <!-- TOP CONTENT HOLDER -->
        <div class="start-content__top">
            <div class="start-content__top-left">MY ACCOUNT</span></div>
            <ul class="start-content__top-right">
                <li class="button--bingo"><a data-hijack="true" data-bind="visible: validSession, click: playBingo">Play Bingo</a></li>
                <li class="button--slots"><a data-hijack="true" href="/slot-games/">Play Slots</a></li>
                <li class="button--deposit"><a data-hijack="true" href="/cashier/">Deposit Now</a></li>
            </ul>
        </div>

        <!-- TABS HOLDER WRAP -->
        <div class="lobby-wrap myaccount-content">
            <!-- left sidebar (tabs-menu)  -->
            <div class="lobby-wrap__left">

                <!-- DESKTOP TABS MENU  -->
                <dl class="tabs" data-tab>

                    <dd class="first active"><a href="#myaccount-1">Overview</a></dd>

                    <dd><a id="ui-my-balances-tab" href="#myaccount-2">Account Balances</a></dd>
                    <dd><a id="ui-my-convert-tab" href="#myaccount-3">Convert Points</a></dd>

                    <dd><a id="ui-redeem-tab" href="#myaccount-4">Claim Code</a></dd>

                    <dd><a id="ui-my-details-tab" data-bind="click: PersonalDetailsNew.getPersonalDetails" href="#myaccount-5">Personal Details</a></dd>
                    <dd><a id="ui-activity-tab" href="#myaccount-6">Account Activity</a></dd>

                </dl>

                <!-- user cashback -->
                <div style="display: none;" class="lobby-claim-cashback__wrap alert-box" data-alert data-bind="visible: validSession">
                    <!--<a href="#" class="close" data-bind="visible: ClaimCashback.cbID() != 0" >X</a>-->
                    <?php include'_global-library/partials/start/start-claim-cashback.php'; ?>
                </div>
                <!-- user upgrade bonus -->
                <div style="display: none;" class="lobby-claim-cashback__wrap alert-box" data-alert data-bind="visible: validSession">
                    <!--<a href="#" class="close" data-bind="visible: ClaimUpgradeBonus.ubID() != 0" >X</a>-->
                    <?php include'_global-library/partials/start/start-claim-upgrade-bonus.php'; ?>
                </div>

                <!-- user balance -->
                <?php include '_global-library/partials/start/start-left-balances.php' ?>


                <!-- MOBILE TABS MENU -->
                <a href="#" data-dropdown="id01" data-options="is_hover:false" class="button dropdown mobile-tabs-menu__top">My Account</a>
                <div id="id01" data-dropdown-content class="f-dropdown mobile-tabs-menu__dropdown">
                    <dl class="tabs show-for-small-only" data-tab>

                        <dd class="first active"><a href="#myaccount-1">Overview</a></dd>

                        <dd><a id="ui-my-balances-tab" href="#myaccount-2">Account Balances</a></dd>
                        <dd><a id="ui-my-convert-tab" href="#myaccount-3">Convert Points</a></dd>

                        <dd><a id="ui-redeem-tab" href="#myaccount-4">Claim Code</a></dd>

                        <dd><a id="ui-my-details-tab" data-bind="click: PersonalDetailsNew.getPersonalDetails" href="#myaccount-5">Personal Details</a></dd>
                        <dd><a id="ui-activity-tab" href="#myaccount-6">Account Activity</a></dd>

                    </dl>
                </div>

                <!-- progressive jackpots widget -->
                <div class="mobile-hide-bingo">
                    <?php include '_partials/start/start-left-progressives.php' ?>
                </div>

            </div>

            <!-- right holder tabs-content -->
            <div class="lobby-wrap__right tabs-content">
                <!-- tab content 1 -->
                <div class="content active" id="myaccount-1">
                    <?php include '_global-library/partials/start/start-right-welcome-section.php' ?>
                    <!-- my messages section -->
                    <?php if(config("inbox-msg-on")): ?>
                        <?php include'_global-library/partials/start/start-my-messages-section.php'; ?>
                    <?php endif; ?>
                    <!-- feature games slider -->
                    <?php include '_partials/start-skin-only/start-featured-games-section.php'; ?>
                    <!-- bingo schedule -->
                    <?php include "_global-library/partials/start/start-bingo-schedule.php"; ?>
                    <!-- Community Section Holder Row -->
                    <?php include'_partials/home-community-section/home-community-holder.php'; ?>
                </div>
                <!-- tab content 2-->
                <div class="content" id="myaccount-2">
                    <?php include '_global-library/partials/my-account/myaccount-balances.php' ?>
                    <?php include '_global-library/partials/my-account/myaccount-promotions.php' ?>
                </div>
                <!-- tab content 3 -->
                <div class="content" id="myaccount-3">
                    <?php include '_partials/start-skin-only/start-convert-points.php' ?>
                </div>
                <!-- tab content 4 -->
                <div class="content" id="myaccount-4">
                    <?php // include '_global-library/partials/my-account/myaccount-redeem.php' ?>
                    <?php include '_partials/start-skin-only/start-claim-code.php' ?>
                </div>
                <!-- tab content 5 -->
                <div class="content" id="myaccount-5">
                    <h3 class="playerdetails__header">Your Personal Details</h3>
                    <?php include '_global-library/partials/my-account/myaccount-details.php' ?>
                    <?php // include '_partials/start-skin-only/start-myaccount-details.php' ?>
                </div>
                <!-- tab content 6-->
                <div class="content" id="myaccount-6">
                    <?php include '_global-library/partials/my-account/myaccount-transactions.php' ?>
                </div>

            </div>
            <!-- /tabs-content -->
        </div>
        <!-- / TABS HOLDER WRAP -->
    </div>

</div>

<!-- END MIDDLE CONTENT CENTRE COLUMN -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>
