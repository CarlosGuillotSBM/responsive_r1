<!-- MOBILE CASHBACK -->
<div style="display: none;" data-alert class="mobile-cashback alert-box" data-bind="visible: validSession">
    <!--<a href="#" class="close" data-bind="visible: ClaimCashback.cbAmount() > 0" >X</a>-->
    <?php include'_global-library/partials/start/start-claim-cashback.php'; ?>
</div>
<!-- / MOBILE CASHBACK -->

<!-- MOBILE UPGRADE BONUS -->
<div style="display: none;" data-alert class="mobile-cashback alert-box" data-bind="visible: validSession">
   <!-- <a href="#" class="close" data-bind="visible: ClaimUpgradeBonus.ubAmount() > 0" >X</a>-->
    <?php include'_global-library/partials/start/start-claim-upgrade-bonus.php'; ?>
</div>
<!-- / MOBILE UPGRADE BONUS -->

<!-- MOBILE HOME VIEW -->
<div class="home-mobile">
    <!-- offer -->
    <div class="home-main-slider desktop-mobile">
        <ul class="" data-width="500">
            <!-- Hero Area -->
            <li class="ui-scheduled-content-container">
                <?php edit($this->controller,'top-banner', ""); ?>
                <?php @$this->repeatData($this->content['top-banner']);?>
            </li>
        </ul>
    </div>

    <!-- Featured Games Box with tabbed content -->
    <div class="home-mobile-carousels" data-bind="css: { 'logged-in': validSession() }">
        <?php edit($this->controller,'home-carousels', "", "top: 120px;"); ?>
        <?php @$this->repeatData($this->content['home-carousels']);?>
    </div>

    <!-- bingo schedule -->
    <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>

    <!-- Community Section Holder Row -->
    <?php include'_partials/home-community-section/home-community-holder.php'; ?>

    <!-- footer bar -->
    <div class="footer-bar-mobile" data-bind="visible: validSession()" style="display: none">
        <a data-hijack="true" data-bind="click: playBingo" class="cta-join">PLAY BINGO</a>
        <a data-hijack="true" href="/cashier/" class="cta-cashier">DEPOSIT NOW</a>
    </div>
</div>
<!-- /MOBILE HOME VIEW -->
