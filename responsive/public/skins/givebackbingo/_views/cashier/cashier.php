<!-- MIDDLE CONTENT -->
<div class="main-content cashier-content">

  <!-- CONTENT -->
  <?php //edit($this->controller,'help'); ?>

  <!-- CASHIER WRAP -->
  <section class="cashierholder">
    <!-- CONTENT -->
    <div class="content-template">
      <!-- PRD CASHIER -->
      <script type="application/javascript">
      skinModules.push( { id: "Cashier", options: { source: "Cas" } } );
      </script>
      <?php include '_global-library/partials/cashier/cashier.php'; ?>
      <!-- /PRD CASHIER -->
    </div>
    <!-- /CONTENT-->
  </section>

</div>
<!-- /MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>