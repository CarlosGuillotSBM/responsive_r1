<!--games content -->
<div class="main-content games-content">

	

	<!-- games menu -->
	<?php include '_partials/games-menu/games-menu.php'; ?>

	<!-- banner -->	
	<div class="middle-content__box ui-scheduled-content-container">
		<!-- Edit point Banner -->
		<?php edit($this->controller,'games-latest-offer'); ?>
		<?php @$this->getPartial($this->content['games-latest-offer'],1); ?>
	</div>

	<!-- games list-->
	<section class="section middle-content__box games-section">
		<?php include '_global-library/partials/games/games-list.php'; ?>
		<!-- end games list -->

	</section>		

	<?php if (config("RealDevice") !== 3):?>
		<section class="section middle-content__box games-section">
			<?php
				if ($this->action != "slots") {
					edit($this->controller.$this->action,'footer-seo', $this->action);
					@$this->getPartial($this->content['footer-seo'], 1); 
				} else {
					edit($this->controller,'footer-seo',$this->action);
					@$this->getPartial($this->content['footer-seo'],$this->action); 
				}
			?>
		</section>
    <?php endif; ?>

	<!-- BOTTOM CONTENT -->
</div>

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>