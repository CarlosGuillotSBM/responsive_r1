<!-- MIDDLE CONTENT -->
<div class="my-messages-mobile-content">
    <div class="section-left__title">
        <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
        
    </div>
    <div class="my-messages-mobile-content__container">

        <?php if(config("inbox-msg-on")): ?> 
        <?php  include'_global-library/partials/start/start-my-messages-section.php'; ?>
        <?php endif; ?>

    </div>
</div>
<!-- /MIDDLE CONTENT -->