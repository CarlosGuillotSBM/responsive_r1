<!-- single post main content -->
<div class="main-content community-blog">

    <!-- Title -->
    <div class="section-left__title">
        <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>

    <!-- content -->
    <div class="middle-content__box">			<!-- community nav -->
        <?php // include "_partials/community-nav/community-nav.php" ?>
        <!-- /community nav -->
        <div class="two-columns-content-wrap middle-content__box">
            <!-- left column -->
            <div class="two-columns-content-wrap__left ">
                <div class="theme--content--header"><h1>Charity</h1></div>
                <div class="theme--content--container">

                    <?php edit($this->controller,$this->action); ?>
                    <!-- repeatable content -->
                    <?php @$this->repeatData($this->content[$this->action]);?>
                    <!-- /repeatable content -->
                </div>
            </div>
            <!--  /left column -->
            <!-- right column -->
            <div class="two-columns-content-wrap__right">
                <?php edit($this->controller,'charity-side-content'); ?>
                <div class="theme--content--header"></div>
                <div class="theme--content--container">
                    <?php if (config("RealDevice") != 3) { ?>

                        <!-- repeatable content -->
                        <?php @$this->repeatData($this->content['charity-side-content']);?>
                        <!-- /repeatable content -->
                    <?php } ?>

                </div>
            </div>
            <!-- /right column -->
        </div>
        <!-- /content  -->

    </div>

</div>
<!-- /single post main content -->
</div>

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>