<script type="application/javascript">
// skinPlugins.push( {id: "rad_dotdotdot"} );
</script>
<!-- single post main content -->
<div class="main-content community">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  
	<!-- content -->
	<div class="middle-content__box">
		<!-- community nav -->
		<?php // include "_partials/community-nav/community-nav.php" ?>
		<!-- left column -->
		<div class="two-columns-content-wrap">
			<div class="two-columns-content-wrap__left">
				<!-- top left area -->
				<?php edit($this->controller,'blog'); ?>
				<!-- latest winners slider -->


                <!-- chat news area -->
                <section class="community__section">
                    <!-- Title -->
                    <div class="section-left__title__community">
                        <h4><span>Charity of the month</span>
                            <a href="/community/charity/"  data-hijack="true">view all</a>
                        </h4>
                    </div>

                    <div style="cursor: pointer" class="theme--content--container" onclick="location.href='/community/charity/<?php echo $this->content['charity'][0]["DetailsURL"] ?>'">
                        <div class="community__section__post"><?php @$this->getPartial($this->content['charity'],1, "_global-library/_editor-partials/text-intro-and-image-left.php"); ?></div>
                    </div>
                </section>

				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community margin--top--20px">
						<h4><span>Blog</span>
							<a href="/community/blog/"  data-hijack="true">view all</a>				
						</h4>
					</div>
					<?php //@$this->repeatData($this->content['blog']);?>

 					<div class="theme--content--container">
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],1); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],3); ?></div>
					</div>
				</section>

                <?php /* TODO. Added once it is required DEV-7621
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community margin--top--20px">
						<h4><span>Winners</span>
							<a href="/community/winners/"  data-hijack="true">view all</a>
						</h4>
					</div>

 					<div class="theme--content--container">
					<div class="community__section__post"><?php @$this->getPartial($this->content['winners'],1); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['winners'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['winners'],3); ?></div>
					</div>
				</section>
				
				<!-- chat host area -->
				<section class="community__section community__section__chat-thumbs">
					
					<!-- Title -->
					<div class="section-left__title__community margin--top--20px">
						<h4><span>Chat Moderator</span></h4>
					</div>
					
					 <div class="theme--content--container">
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],1); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],3); ?></div>
					</div>
				</section>
                */?>

			</div>
			<!--  /left column -->
			<!-- right column -->
			<div class="two-columns-content-wrap__right">
                <div class="theme--content--header"> </div>
                <div class="theme--content--container ui-scheduled-content-container">
                    <?php if (config("RealDevice") != 3) { ?>
                    <?php edit($this->controller,'blog-side-content'); ?>
                    <!-- repeatable content -->
                    <?php @$this->repeatData($this->content['blog-side-content']);?>
                    <!-- /repeatable content -->
                    <?php } ?>
                </div>

                <div class="content-holder__content_right">
                    <!-- content holder title right link -->
                    <?php
                    $titleHeaderHeight = 25;
                    $slideHeight = 95;
                    $slidesToShow = 3;
                    ?>
                    <script type="application/javascript">
                        skinPlugins.push({
                            id: "ProgressiveSlider",
                            options: {
                                slidesToShow: <?php echo $slidesToShow ?>,
                                mode: "vertical"
                            }
                        });
                    </script>
                    <div class="content-holder__inner progressive-jackpot-wrap">
                        <?php edit($this->controller,'community-progressives'); ?>
                        <div class="progressive-jackpots__wrapper">
                            <div class="progressive-jackpots">
                                <?php
                                if(config("RealDevice") < 3) {
                                    @$this->repeatData($this->content['community-progressives'],1);
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>

			</div>
			<!-- /right column -->
		</div>
	</div>
</div>
</div>
<!-- /single post main content -->	<!-- /content  -->
<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>