<div class="main-content">
  <section class="section">
    <h1 class="section__title"><?php echo $this->controller; ?></h1>
    <!-- CONTENT -->
    <?php edit($this->controller,'affiliates'); ?>
    <div class="content-template">
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['affiliates']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>