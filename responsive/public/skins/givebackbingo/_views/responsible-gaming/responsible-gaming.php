<!-- MIDDLE CONTENT -->
<div class="main-content">

  <section class="section">
    <div class="section-left__title">
      <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>

    <!-- CONTENT -->
    <?php edit($this->controller,'responsible-gaming'); ?>
    <div class="content-template">
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['responsible-gaming']);?>
      <!-- reality check dropdown -->
      <?php include '_global-library/partials/reality-check/reality-check-settings.php'; ?>
    </div>
    <!-- /CONTENT-->

  </section>

</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>