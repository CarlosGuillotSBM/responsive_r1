<!-- MAIN CONTENT AREA -->
<div class="main-content">
  <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h4><span>General Promotional Terms &amp; Conditions</span></h4>
  </div>
  
  <!-- CONTENT -->
  <!-- TWO COLUMNS LAYOUT -->
  <div class="two-columns-content-wrap__left__title">
    
    <!-- LEFT COLUMN -->
    <div class="two-columns-content-wrap__left">
      <!-- WRAP CONTENT LEFT -->
      <div class="two-columns-content-wrap__left__inner" itemscope itemtype="http://schema.org/SoftwareApplication">
        
        <!-- CONTENT -->
        <?php edit($this->controller,'generalTerms'); ?>
        <ul class="accordion accordion-wrap">
          <?php @$this->repeatData($this->content['generalTerms'], 1, "_global-library/partials/common/text-accordion.php");?>
        </ul>
      </div>
    </div>
    <?php if (config("RealDevice") != 3) { ?>
    <!-- RIGHT COLUMN -->
    <div class="two-columns-content-wrap__right">
      <?php edit($this->controller,'generalTermsSide__side-content',$this->subaction); ?>
      <?php @$this->repeatData($this->content['generalTermsSide__side-content']); ?>
      <div class="clearfix"></div>
    </div>
    <?php } ?>
  </div>
  <!-- /TWO COLUMNS LAYOUT -->
  
</div>
<!-- /MAIN CONTENT AREA -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>