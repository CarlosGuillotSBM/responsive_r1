<!-- MIDDLE CONTENT -->
<div class="main-content">
  <section class="section">
    <div class="section-left__title">
      <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>
    <!-- CONTENT -->
    <?php edit($this->controller,'terms-and-conditions'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['terms-and-conditions']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->

  </section>
</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>