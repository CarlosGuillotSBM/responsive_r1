<!-- bingo content -->
<div class="main-content bingo-content">

    <!-- banner -->
    <div class="games-content__banner ui-scheduled-content-container">
        <!-- Edit point Banner -->
        <?php edit($this->controller,'bingo-page-top-banner'); ?>
        <?php @$this->repeatData($this->content['bingo-page-top-banner']); ?>
    </div>

    <div class="clearfix"></div>

    <!-- Bingo DESKTOP Schedule Holder Row -->
    <div class="mobile-hide-bingo">
        <?php include "_partials/bingo-schedule/bingo-schedule-holder-home.php"; ?>
    </div>

    <!-- MOBILE Bingo Schedule Holder Row -->
    <div class="mobile-show-bingo">
        <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>
    </div>


    <div class="clearfix"></div>

    <!-- bingo list 90 ball-->
    <section class="section middle-content__box bingo-section">
        <!-- end bingo list -->
        <?php edit($this->controller,'90-ball-title'); ?>
        <?php if($this->action == 'bingo' || $this->action == '90-ball'):?>
        <div class="section__header section-full-width__title">

            <h4><span><?php @$this->getPartial($this->content['90-ball-title'],1); ?></span></h4>
        </div>
        <!-- Hub grid -->
        <?php
        $hub = '90-ball';
        include "_global-library/partials/hub/hub-grid-bingo-3.php";
        ?>
    </section>
    <?php endif;?>


    <?php if($this->action == 'bingo' || $this->action == '75-ball'):?>
        <!-- bingo list 75 ball-->
        <section class="section middle-content__box bingo-section">
            <div class="section__header section-full-width__title">
                <?php edit($this->controller,'75-ball-title'); ?>
                <h4><span><?php @$this->getPartial($this->content['75-ball-title'],1); ?></span></h4>
            </div>

            <!-- Hub grid -->
            <?php
            $hub = '75-ball';
            include "_global-library/partials/hub/hub-grid-bingo-3.php";
            ?>
        </section>
    <?php endif;?>



    <?php if($this->action == 'bingo' || $this->action == '5-line'):?>
        <!-- 5 line bingo rooms-->
        <section class="section middle-content__box bingo-section">
            <!-- end bingo list -->
            <div class="section__header section-full-width__title">
                <?php edit($this->controller,'5-line-bingo-title'); ?>
                <h4><span><?php @$this->getPartial($this->content['5-line-bingo-title'],1); ?></span></h4>
            </div>

            <!-- Hub grid -->
            <?php
            $hub = '5-line-bingo';
            include "_global-library/partials/hub/hub-grid-bingo-3.php";
            ?>
        </section>
    <?php endif;?>


    <?php if($this->action == 'bingo' || $this->action == 'pre-buy'):?>
        <!-- bingo list PRE BUY ball-->
        <section class="section middle-content__box bingo-section">
            <!-- end bingo list -->

            <div class="section__header section-full-width__title">
                <?php edit($this->controller,'pre-buy-title'); ?>
                <h4><span><?php @$this->getPartial($this->content['pre-buy-title'],1); ?></span></h4>
            </div>

            <!-- Hub grid -->
            <?php
            $hub = 'pre-buy';
            include "_global-library/partials/hub/hub-grid-bingo-3.php";
            ?>
        </section>
    <?php endif;?>

    <div class="clearfix"></div>

    <?php
        edit($this->controller,'footer-seo',$this->action);
        @$this->getPartial($this->content['footer-seo'],$this->action);
    ?>
</div>

<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>