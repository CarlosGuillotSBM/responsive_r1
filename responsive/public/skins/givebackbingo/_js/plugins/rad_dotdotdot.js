var sbm = sbm || {};

sbm.rad_dotdotdot = function () {
	"use strict";

	return {
		run : function () { 
            
            var device = sbm.serverconfig.device;   // Device detection
            var newDotdotdot = new sbm.Dotdotdot(); //dotdotdot object initialitation. External plugin to crop the text
            newDotdotdot.run();                     // we run the dotdotdot plugin
            var imgh;                               // height of the promo image. Text content can't be longer that that
            var titleh;                             // we need to check the title hight to be acurate
            var promotextheight                     // var to store the final height of the content area
            // -----------------------------------
            // function to resize the content area
            // ----------------------------------- 
            var textResizer = function(){
                if(device == 1){
                    // get the promo img height
                    imgh = $('.community__section__post .ui-rad_dotdotdot__height__reference').height();
                    // we need to take out the title hight
                    promotextheight = imgh;
                    // also we take out extra pixels for the padding
                    promotextheight = promotextheight -20;
                    // set the new content height
                    $('.community__section__post .article__content__body').css('overflow', 'hidden');
                    $('.community__section__post .article__content__body').css('height', promotextheight + 'px');
                    // we apply the dotdotdot on the promotion__content container
                    $(".community__section__post .article__content").dotdotdot();
                    
                }
            }
            // ----------------------------------- 
            // ----------------------------------- 
            // ----------------------------------- 

            // ----------------------------------- 
            // on resize event listener
            $( window ).resize(function(){
                textResizer();
            });
            // we trigger the function to make the magic happen
            textResizer();
            
        }
    };

}