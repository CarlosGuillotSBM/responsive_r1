var sbm = sbm || {};

sbm.Global = function() {
    "use strict";

    return {

        highlightCommunityNav: function(page) {
            $("ul.community-nav").removeClass("active");

            if (page === "/community/") {
                $("#communityhome > a").addClass("active");
            } else if (page === "/community/blog/") {
                $("#communityblog > a").addClass("active");
            } else if (page === "/community/tv/") {
                $("#communitytv > a").addClass("active");
            } else if (page === "/community/winners/") {
                $("#communitywinners > a").addClass("active");
            } else if (page === "/community/chat-news/") {
                $("#communitychat-news > a").addClass("active");
            }
        },
        highlightSubNav: function(page) {
            $("ul.games-menu__games-nav > a").removeClass("active");

            if (page.indexOf("/games/") > -1) {
                $("#allslots a").addClass("active");
            } else if (page.indexOf("/table-games/") > -1) {
                $("#table-card a").addClass("active");
            } else if (page.indexOf("/scratch-cards/") > -1) {
                $("#scratchcards a").addClass("active");
            }
        },
        showFacebookFeed: function() {

        },
        run: function() {

            // remove initial loader
            $("#ui-first-loader").remove();

            // useful variables for the site
            var page = window.location.pathname;
            var fullurl = window.location;
            var device = sbm.serverconfig.device;

            if (page.indexOf("/community/") > -1) {
                this.highlightCommunityNav(page);
            } else if (page.indexOf("/games/") > -1 || page.indexOf("/slot-games/table-games/") > -1 || page.indexOf("/slot-games/scratch-cards/") > -1) {
                this.highlightSubNav(page);
            }
            // ------------------------------
            // ----
            //hightlight selected nav element
            // ----
            // ------------------------------
            var _current =  location.pathname.split('/')[1]
            var itemID;
            var item;
            if (_current == "/" || _current == "/start/") {
                _current = "home";
            };

            if (_current == "register") {
                _current = "join-now";
            };
            
            // Changing the id of the body when we get the home through ajax navigation.
            $('body').attr('id', _current);

            $(".navigation-desktop ul li").each(function(index) {
                var _splitCurrent = _current.split("/");
                _splitCurrent = (_splitCurrent.length > 1) ? _splitCurrent[1] : _splitCurrent[0];
                itemID = $(this).attr('id');
                if (_splitCurrent.indexOf(itemID) > -1) {
                    $(this).find("a").addClass("current");
                } else {
                    $(this).find("a").removeClass("current");
                }
            });

            $(".user-nav-left div").each(function(index) {
                itemID = $(this).attr('id');
                if (itemID != _current) {
                    $(this).find("a").removeClass("current");
                } else {
                    $(this).find("a").addClass("current");
                }
            });

            $(".community-nav li").each(function(index) {
                itemID = $(this).attr('id');
                if (itemID != _current) {
                    $(this).find("a").removeClass("current");
                } else {
                    $(this).find("a").addClass("current");
                    // check if it is a community page to highlite the community main button
                    if (page.indexOf('community')) {
                        $('.navigation-desktop ul #community a').addClass("current");
                    };
                }
            });

            // change name of Home link and href attr
            if ($.cookie("SessionID")) {
                $(".ui-go-home").attr("href", "/start/").text("START");
                $(".ui-go-home-logo").attr("href", "/start/");
            }

            // --------------
            // SHARE BUTTON
            // --------------

            var config = {
                ui: {
                    flyout: 'middle left'
                },
                networks: {
                    facebook: {
                        url: this.fullurl
                    },
                    twitter: {
                        url: this.fullurl
                    },
                    google_plus: {
                        url: this.fullurl
                    },

                    email: {
                        enabled: false
                    },
                    pinterest: {
                        enabled: false
                    }
                }
            }

            var share = new Share('.share-button', config);

            //We show it if it is not the cashier.
            if (page == '/cashier/' || page == '/my-account/' || page == "/register/" || page == "/welcome/") {
                $('.share-button').attr("style", "display: none!important");
            } else {
                $('.share-button').attr("style", "display: inline!important");
            }

            // -----------
            // STIKY GAMES NAV AFTER REACHING THE BOTTOM OF THE MAIN NAV
            // -----------

            var shouldStickNavBar = page.indexOf('/games/') > -1 ||
                page.indexOf('/slot-games/') > -1 ||
                page.indexOf('/slot-games/table-games/') > -1 ||
                page.indexOf('/slot-games/roulette/') > -1 ||
                page.indexOf('/slot-games/roulette/') > -1 ||
                page.indexOf('/slot-games/scratch-cards/') > -1;

            if (shouldStickNavBar) {

                if (device == 'xxxx') {

                    var $gamesMenu = $('.games-menu');

                    // if there is a games menu
                    if ($gamesMenu.length) {

                        var $headerDesktop = $('.header-desktop');
                        var $gamesList = $('.games-list');
                        var headerH;
                        var myOffsetTop;
                        var myScrollTop;
                        var initOffset = $gamesMenu.offset().top - $headerDesktop.height();
                        $(window).scroll(function() {

                            headerH = $headerDesktop.height();
                            myOffsetTop = $gamesMenu.offset().top - headerH;
                            myScrollTop = $(window).scrollTop();

                            if (myScrollTop > myOffsetTop) {
                                if (!($gamesMenu.hasClass('sticky-games-nav'))) {
                                    $gamesMenu.addClass('sticky-games-nav');
                                    $('#ajax-wrapper').css('margin-top', $gamesMenu.height());
                                }

                            }
                            if (myScrollTop < initOffset) {
                                if ($gamesMenu.hasClass('sticky-games-nav')) {
                                    $gamesMenu.removeClass('sticky-games-nav');
                                    $gamesList.css('margin-top', '0px');
                                }
                            }
                        });
                    }

                };


            };



            // fix the header when scrolling
            var $headerscroll = $('.nav-for-mobile');
            var $footerscroll = $('.footer-bar-mobile');
            $(document).scroll(function() {
                // $headerscroll.css({position: $(this).scrollTop() > 0? "fixed":"relative"});

             if ($(this).scrollTop() > 50){  
                $headerscroll.addClass("sticky");
                // $footerscroll.addClass("on-scroll");
              }
              else {
                $headerscroll.removeClass("sticky");
                // $footerscroll.removeClass('on-scroll');
              }

            });

            // ctas footer nav on mobile hides when scroll down
            // $(function() {
            // var prevScroll = $(document).scrollTop();
            // $(window).scroll(function() {
            //         var newScroll = $(document).scrollTop();
            //         if(newScroll < prevScroll) {
            //             $footerscroll.removeClass("on-scroll");
            //         } else {
            //             $footerscroll.addClass('on-scroll');
            //         }
            //         prevScroll = newScroll;
            //     });
            // });



            // -----------
            //Click effect on search results screen to show growing circle background
            // -----------
            var parent, ink, d, x, y;
            $(".ripple li a").click(function(e) {
                parent = $(this).parent();
                //create .ink element if it doesn't exist
                if (parent.find(".ink").length == 0)
                    parent.prepend("<span class='ink'></span>");

                ink = parent.find(".ink");
                //incase of quick double clicks stop the previous animation
                ink.removeClass("animate");

                //set size of .ink
                if (!ink.height() && !ink.width()) {
                    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                    d = Math.max(parent.outerWidth(), parent.outerHeight());
                    ink.css({
                        height: d,
                        width: d
                    });
                }

                //get click coordinates
                //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
                x = e.pageX - parent.offset().left - ink.width() / 2;
                y = e.pageY - parent.offset().top - ink.height() / 2;

                //set the position and add class .animate
                ink.css({
                    top: y + 'px',
                    left: x + 'px'
                }).addClass("animate");
            })

            /**
             * FACEBOOK FEED
             */
            if (page.indexOf("/community/") > -1) {
                this.showFacebookFeed();
            }

            /**
             * Dropdowns
             */
            var $dropdownLink = $(".f-dropdown a"),
                $menu = $dropdownLink.closest("div");

            $dropdownLink.on("click", function () {

                $menu.toggleClass("open close");
                $menu.prev().text($(this).text());
                $menu.prev().removeClass("open");
            });

            $(".button.dropdown.mobile-tabs-menu__top").on("click", function () {
                $(this).toggleClass("open");
            });


            /**
             * Help page - forgot password accordion
             */
            $(".ui-accordion").on("click", function () {
               $(this).next().toggle();
               $(this).toggleClass("active");
            });

            /**
             * Search animation
             */
            $(".search-screen-mobile__header").on("click", function() {
                $(this).animate({top:'-50px'}), 1000, "easeOutBounce";
                $(".search-screen").animate({"bottom": "-100%"}, "slow").css("visibility", "hidden");
            });

            /**
             * Scroll to top on the login buttons
             */
            $(".ui-scroll-top").on("click", function () {
                $(window).scrollTop(0);
            });


        }
    };

};