var sbm = sbm || {};

/**
 * Twitter Feed
 */
sbm.Twitter = function () {
    "use strict";

    return {
        run: function () {

            // only runs if is desktop device
            if (sbm.serverconfig.device === 1) {
                !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}(document,"script","twitter-wjs");
            }
        }
    };

};