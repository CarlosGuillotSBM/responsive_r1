<?php

	// send a mail on password change requirement

	switch(@$_REQUEST["emailName"])
	{
		// this is old and not used
		case "WebForgotLogin":
			
			//send mail on forgot password success
			
			if(@$_REQUEST["ReturnData"] == 0 && @$_REQUEST["Part"] == 2)
				sendForgotPasswordMail();		
					
			break;
		
		
		case "WebRegister":

			// send a mail on registration success
			sendRegistrationMail();			
			break;			
			
		case "WebPlayerForgotPassword":				
			// send mail to player for forgot password	
			header('Location: /data.php?f=playerforgotpassword&e='.$_REQUEST["Email"]);		
			break;	

		case "COGSResetPassword":				
			header('Location: /data.php?f=playerforgotpassword&e='.$_REQUEST["Email"]);
			// send mail to player for forgot password	
			die();			
			break;	

			
		case "SecurityMail":
			
			// send a security mail if the player has been locked out due to 3 bad password attempts			
			sendSecurityMail();									
			break;				
		

		case "RefundProcessed":
			// sent from cat when a withdrawal is being processed
			sendWithdrawalProcessedMail();							
			break;
		
		case "cashback_credited":
			sendCashbackCreditedMail();
			break;
			

		default:    
			break;
		
	}	

?>