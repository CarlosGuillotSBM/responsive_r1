<div class="start-convert-points-content">
	<h3 class="start-convert-points-content__header">Convert Loyalty Points into Bonus Money</h3>
	<div class="start-convert-points-content__wrap">
		<p class="start-convert-points-content__text" data-bind="text: 'How many ' + ConvertPoints.pointsName() + ' do you want to convert?'"></p>
		<input class="start-convert-points-content__field" data-bind="value: ConvertPoints.pointsToConvert, valueUpdate: ['input', 'keypress']" type="tel" placeholder="Minimum 1000" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
		<a href="#" class="start-convert-points-content__button" data-bind="click: ConvertPoints.convert">Convert</a>
		<span class="start-convert-points-content__error" data-bind="visible: ConvertPoints.errorMsg, text: ConvertPoints.errorMsg" 
		class="error"/></span>
	</div>
</div>