<script>

    skinPlugins.push({
        id: "FeaturedGames",
        options: {
            global: true
        }
    });

</script>


<!-- Featured Games Box with tabbed content -->
<div class="lobby-wrap__content-box">
	<?php edit($this->controller,'start-featured-games-section'); ?>
	<?php @$this->repeatData($this->content['start-featured-games-section']);?>
</div>

<!-- end featured games -->