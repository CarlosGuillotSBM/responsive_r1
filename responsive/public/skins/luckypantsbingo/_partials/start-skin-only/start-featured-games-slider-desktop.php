<div class="lobby-wrap__content-box__content bx-wrapper ui-loading-carousel">
    <div class="bx-dots-loading"></div>
</div>

<div class="lobby-wrap__content-box__content featured-games ui-featured-game-icons" style="visiblity: hidden; height: 0px">

	<!-- BX SLIDER CARROUSEL -->
	<div class="feature-games-slider">
	    <?php edit($this->controller,'feature-games-slider'); ?>
	    <ul class="bxslider bxslider-start-featured-games">
	        <?php @$this->repeatData($this->content['feature-games-slider'], 1, "_partials/start-skin-only/start-featured-games-slide.php");?>
	        <?php include "_partials/start-skin-only/start-featured-games-slide.php";?>
	    </ul>
	</div>
	<!-- / BX SLIDER CARROUSEL -->
    
</div>