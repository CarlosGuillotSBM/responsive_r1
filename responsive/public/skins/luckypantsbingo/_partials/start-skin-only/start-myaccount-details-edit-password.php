<div class="myaccount-details__edit-password">
	<div class="edit-wrap">
		<div class="edit-wrap__inner">
			<!-- Create new Password -->
			<div class="edit-row">
				<input class="password" data-bind="value: PersonalDetailsNew.newPassword" type="password" maxlength="15" placeholder="Create a new password">
				<div class="show-password" data-bind="click: LoginBox.togglePassword"><a class="right" data-bind="text: LoginBox.passwordShowText">Show</a></div>
				<span data-bind="visible: PersonalDetailsNew.invalidNewPassword" class="error">Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces.</span>
				<span data-bind="visible: PersonalDetailsNew.UsernameAndPassTheSame" class="error">Username and Password cannot be the same</span>
			</div>
			<!-- Repeat new Password -->
			<div class="edit-row">
				<input class="password" data-bind="value: PersonalDetailsNew.repeatPassword" type="password" maxlength="15" placeholder="Repeat your new password">
				<div class="show-password" data-bind="click: LoginBox.togglePassword"><a class="right" data-bind="text: LoginBox.passwordShowText">Show</a></div>
				<span data-bind="visible: PersonalDetailsNew.passwordsDontMatch" class="error">Passwords do not match</span>
			</div>
			<!-- Enter Current Password -->
			<div class="edit-row">
				<input class="password" data-bind="value: PersonalDetailsNew.currentPassword" type="password" maxlength="15" placeholder="Enter current password">
				<div class="show-password" data-bind="click: LoginBox.togglePassword"><a class="right" data-bind="text: LoginBox.passwordShowText">Show</a></div>
				<span data-bind="visible: PersonalDetailsNew.invalidCurrentPassword" class="error">This field is required</span>
			</div>
			<!-- BUTTONS -->
			<div class="edit-row">
				<a data-bind="click: PersonalDetailsNew.updateDetails" href="" class="button confirm">Confirm</a>
				<a data-bind="click: PersonalDetailsNew.updateDetails" href="" class="button cancel">Cancel</a>
			</div>
		</div>
	</div>
</div>
<div class="myaccount-details__edit-password-bg"></div>