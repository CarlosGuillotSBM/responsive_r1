<!-- LOGIN BOX -->
<article class="login-box">
	<div class="login-box__form-wrapper">

        <!--claim code-->
        <div class="claimcode_modal deposit-now" data-bind="css: {'deposit-now_noCTA': !LoginBox.showDeposit()}">
            <h2 data-bind="text: LoginBox.salutation"></h2>
            <p data-bind="text: LoginBox.lastLogged"></p>
            <div class="claimcode_box">
                Your balance is <span data-bind="text: LoginBox.balance"></span>
                <span data-bind="visible: LoginBox.showDeposit"><a data-hijack="true" href="/cashier/" class="claim-code_cta">DEPOSIT NOW</a></span>
            </div>
            <a href="#" class="claimcode">ENTER CLAIM CODE</a>
        </div>
        <!--end claim code-->

        <!--Redeem claim code-->
        <div class="claimcode_modal redeem" style="display: none;">
            <h2 class="claimcode_back"><span class="back_button"><img src="/_images/common/back-icon.svg"></span>BACK</h2>

            <div class="claimcode_box">
                <span>REDEEM A CLAIM CODE</span>
                <div class="start-claim-code">
                    <input data-bind="value: RedeemPromos.promo" placeholder="Enter Code" type="text" maxlength="50">
                </div>
                <span style="display: none" data-bind="visible: RedeemPromos.error, text: RedeemPromos.error" class="error"></span>
                <span><a data-bind="click: RedeemPromos.redeemPromo" href="" class="claim-code_cta ">CLAIM NOW</a></span>
                <span class="terms">*If you have a deposit promo code, please enter it in the cashier when you make a deposit</span>                
            </div>

        </div>
        <!--End Redeem claim code-->

        <!--Start Playing now-->
        <div class="claimcode_modal success" style="display: none;">            
            <h2 class="claimcode_back"><span class="back_button"><img src="/_images/common/back-icon.svg"></span>BACK</h2>
            <div class="claimcode_box">
                <span class="" >YOU HAVE<br> SUCCESSFULLY<br> REDEEMED YOUR CODE</span>

                <span><a data-hijack="true" href="/start/" class="claim_startPlaying_cta">START PLAYING NOW</a></span>
            </div>
        </div>
        <!--End Start Playing now-->

	</div>	
	<!-- RIBBONS -->
	<span class="ribbon-left"></span>
	<span class="ribbon-right"></span>

	<!-- LOGIN OFFER CTA -->
	<div class="join-now-box ui-scheduled-content-container" data-bind="visible: !validSession">
		<p>Not Registered yet?</p>
		<span>
			<!-- Edit point  -->
			<?php edit($this->controller,'login-box-offer'); ?>
			<?php @$this->repeatData($this->content['login-box-offer']); ?>
		</span>
		<!-- CTA -->
		<a class="registercta" href="/register/" data-hijack="true" >Join Now</a>

	</div>
	<!-- /LOGIN OFFER CTA -->
    <!-- LOGIN OFFER CTA -->
    <div class="join-now-box ui-scheduled-content-container" style="display: none" data-bind="visible: validSession">        
            <!-- Edit point  -->

            <?php edit($this->controller,'login-box-offer-logged-in'); ?>
            <?php 
            @$this->repeatData($this->content['login-box-offer-logged-in']); ?>

    </div>
    <!-- /LOGIN OFFER CTA -->    

</article>