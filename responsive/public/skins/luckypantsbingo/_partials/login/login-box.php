<script type="application/javascript">
skinModules.push({
	id: "LoginBox",
	options: {
		modalSelectorWithoutDetails: "#custom-modal",
		global: true
	}
});
</script>
<!-- LOGIN BOX -->
<article class="login-box">

	<div class="login-box__form-wrapper">

		<h1>login</h1>

		<!-- FORM -->
		<form class="login-box__form" id="ui-login-form">
			<div class="login-box__input-wrapper">
				<div class="login-box__username">
					<input id="login_name" name="Username"  onFocus="sbm.core.publish('show-recaptcha')" onfocusout="sbm.core.publish('hide-recaptcha')" placeholder="Username" type="text" placeholder="User Name" data-bind="value: LoginBox.username, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" />
					<img src="/_images/common/icons/login-username.svg">
				</div>
				<span style="display: none" data-bind="visible: LoginBox.invalidUsername" class="error">This field is required.</span>
				<div class="login-box__input-wrapper">
					<div class="login-box__password">
						<input name="Password" id="login_password"  onFocus="sbm.core.publish('show-recaptcha')" onfocusout="sbm.core.publish('hide-recaptcha')" placeholder="Password" type="password" placeholder="Password" data-bind="attr:{type: LoginBox.passwordShowText() == 'Hide' ? 'text': 'password'}, value: LoginBox.password, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}"/>
						<img src="/_images/common/icons/login-password.svg">
						<div class="login-box__show-password" data-bind="click: LoginBox.togglePassword">
							<a class="right" data-bind="text: LoginBox.passwordShowText">Show</a>
						</div>
						<span style="display: none" data-bind="visible: LoginBox.invalidPassword" class="error">This field is required.</span>
					</div>
					<a style="display: none" data-bind="visible: LoginBox.lockedUser, text: LoginBox.loginError" class="error" href="/help/"></a>
				<!-- ko ifnot: LoginBox.lockedUser -->
					<span style="display: none" data-bind="visible: LoginBox.loginError, html: LoginBox.loginError" class="error"></span>
				<!-- /ko -->

					<a href="/forgot-password/" class="login-box__forgot-password">Forgot Password?</a>
					<a class="cta-login" data-bind="click: LoginBox.doLogin.bind($data, GameLauncher.gameId(), GameLauncher.gamePosition(), GameLauncher.gameContainerKey(), GameLauncher.roomId())">Login</a>
				</div>
			</div>
		</form>
		<!-- / FORM -->

		<!-- Account locked -->
		<div style="display:none" class="account-locked">
			<p>Your Lucky Pants Bingo Account is temporarily locked. Please contact our support team to re-activate your account.</p>
			<div class="help-content__info">
				<!--   <h1>Live Chat</h1> -->
				<a href="#" data-hijack="false" target="_blank" onclick="javascript:void window.open('<?php echo config('LiveChatURL'); ?>','1323353895481','width=590,height=480,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;">
					<span>24h Live Chat</span>
				</a>
			</div>
			<a target="_blank" data-hijack="false" href="mailto:support@luckypantsbingo.com"> support@luckypantsbingo.com</a><br>
			<a target="_blank" data-hijack="false" href="tel:08082386070"> 0808 238 6070 (Freephone)</a><br>
			<a  target="_blank" data-hijack="false" href="tel:02035519705"> 0203 551 9705 (Landline)</a>
		</div>
		<!-- End Account locked -->

	</div>

	<!-- RIBBONS -->
	<span class="ribbon-left"></span>
	<span class="ribbon-right"></span>


	<!-- LOGIN OFFER CTA -->
	<div class="join-now-box">
		<p>Not Registered yet?</p>
		<span>
			<!-- Edit point  -->
			<?php edit($this->controller,'login-box-offer'); ?>
			<?php @$this->getPartial($this->content['login-box-offer'],1); ?>
		</span>
		<!-- CTA -->
		<a class="registercta" href="/register/" data-hijack="true" >Join Now</a>
	</div>
	<!-- /LOGIN OFFER CTA -->
</article>
