<script type="application/javascript">
skinModules.push({
        id: "LoginBox",
        options: {
            global: true
        }
    });
</script>


<div class="login-box-modal">

    <h1 class="login-box-wrap__title-bar" data-bind="text: BingoRoomDetails.name()"></h1>
    
    <!-- CONTENT -->
    <div class="login-box-content">

<?php @$this->getPartial($this->content['login-box-offer'],1); ?>

        <!-- GAME IMAGE + GAME DETAILS LINK -->
        <div class="login-box-content__left">
     <?php edit($this->controller,'login-box-offer'); ?>
            <img alt="" src="/_upload-images/standard-size/community/desktop/easter-win-trip-sa-500x375.jpg" />

            <!-- IMAGE CTA -->
            <div class="game-cta">
                 <a href="/register/" alt="Join Now" class="join-now-full">Join Now</a>
            </div>

        </div>

        
        <div class="login-box-content__right">

            <!-- LOGIN -->
            <div class="login-box--loginpage">
                <?php include'_partials/login/login-box.php' ?> 
            </div>

        </div>


        
    </div>
    <!-- / CONTENT -->


</div>