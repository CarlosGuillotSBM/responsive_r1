<script type="application/javascript">

    skinPlugins.push({
        id: "Slick",
        options: {
            slideWidth: <?php if (config("RealDevice") == 1) echo 310; else if (config("RealDevice") == 2) echo 360; else echo 600?>,
            responsive: true,
            minSlides: 1,
            maxSlides: <?php if (config("RealDevice") == 1) echo 3; else if (config("RealDevice") == 2) echo 2; else echo 1?>,
            slideMargin: 20,
            auto: true,
            infinite: true,
            element: 'bxslider-related-promos'
        }
    });

</script>

<div class="promos-slider">
    <ul class="bxslider bxslider-related-promos">
        <?php @$this->getPartial($this->promotions['promotions'],2, "_partials/promos/promo-slide.php"); ?>
        <?php @$this->getPartial($this->promotions['promotions'],3, "_partials/promos/promo-slide.php"); ?>
        <?php @$this->getPartial($this->promotions['promotions'],4, "_partials/promos/promo-slide.php"); ?>
    </ul>
</div>