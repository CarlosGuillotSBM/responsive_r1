<script type="application/javascript">
    skinModules.push({
        id: "OptIn"
    });
</script>

<!-- PROMO WRAP -->
<article class="promotion<?php echo @$row["Feature"]?>">
    <div class="promotion__wrapper">
        
        <!-- PROMO IMAGE -->
        <div class="promotion__image">
            <img src="<?php echo $row["Image"]?>" alt="<?php echo $row["Alt"]?>" onclick="location.href='/promotions/<?php echo $row["DetailsURL"]?>/'">
        </div>

        <!-- TITLE -->
        <div class="promotion__title__wrapper" onclick="location.href='/promotions/<?php echo $row["DetailsURL"]?>/'">
            <h1 class="promotion__title">
                <a data-gtag="Promotion,<?php echo '/promotions/'.$row['DetailsURL'];?>" data-hijack="true" href="/promotions/<?php echo $row["DetailsURL"]?>/">
                    <?php echo $row["Title"]?>
                </a>
            </h1>
        </div>

        <!-- CONTENT WRAP -->
        <div class="promotion__content">

            <!-- CONTENT -->
            <span onclick="location.href='/promotions/<?php echo $row["DetailsURL"]?>/'"><?php echo $row["Intro"];?></span>

            <!-- Terms &amp; Conditions MODAL -->
            <div id="promo-terms-and-conditions-modal-<?php echo $row["ID"]; ?>" class="reveal-modal" data-reveal>
                <h4 class="promotion__title">Terms &amp; Conditions</h4>
                <?php echo $row["Terms"]; ?>
                <span class="close-reveal-modal">&#215;</span>
            </div>
            <!-- Terms &amp; Conditions BUTTON -->
            <a class="promotion__terms" data-reveal-id="promo-terms-and-conditions-modal-<?php echo $row["ID"]; ?>">Terms &amp; Conditions &#10095;</a>

        </div>

        <!-- OPT-IN -->
        <a class="promotion__optin" style="display: none" data-tournament="<?php echo $row['DetailsURL'] ?>">OPT-IN</a>
		<!-- READ MORE -->
        <span onclick="location.href='/promotions/<?php echo $row["DetailsURL"]?>/'" class="promotion__more-info">Read More &#10095;</span>
		<!-- PLAY NOW -->
        <span data-bind="visible: validSession, click: playBingo" class="promotion__play-now">Play Now</span>

        <div class="clearfix"></div>

    </div>
</article>
<!-- / PROMO WRAP -->	