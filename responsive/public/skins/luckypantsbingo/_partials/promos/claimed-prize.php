<!-- Claim modal -->
<article class="login-box">
	<div class="login-box__form-wrapper">
		<div class="competition-wrapper">
            <h2>Congratulations you won</h2>
            <div class="competition-copy">
                <h3>You won <span class="competition-amount"></span> in the <span class="competition-name"></span></h3>
            </div>
                            <!-- CTA -->
                <a class="cta-my-account" href="/my-account/" data-hijack="true" >My account</a>
		</div>
	</div>
</article>
<!-- END Claim modal -->