<!--
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
$$$$ SEO MICRO DATA FOR THIS PARTIAL
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
Name: itemprop="name"
Image: itemprop="image"
URL: itemprop="url"
Screenshot: itemprop="screenshot"
-->
<div class="game-details-page" itemscope itemtype="http://schema.org/SoftwareApplication">
    <h1 class="game-detail-wrap__title-bar" itemprop="name"><?php echo @$this->row["Title"];?></h1>
    <div class="game-detail-content">
        
        <!-- GAME SLIDER -->
        <div class="game-detail-content__left">
            <div class="overlay-holder">
                <?php include '_partials/games/game-slider.php'; ?>
            </div>
        </div>

        <!-- CTAS MOBILE -->
        <span <?php if(@$this->row["Status"]==1) echo 'data-bind="click: GameLauncher.openGame"';?> data-infopage="true" data-gameid="<?php echo @$this->row["DetailsURL"];?>" data-gametitle="<?php echo @$this->row["Title"] ?>" data-icon="<?php echo @$this->row["Image"];?>" data-nohijack="true" class="games-info__try">
            <!-- PLAY NOW -->
            <div style="display: none;" data-bind="visible: validSession() && <?php echo @$this->row["Status"];?>" class="overlay">
                <p alt="Play Now" class="play-now-button--mobile">Play Now</p>
            </div>
        </span>

        <!-- LOGIN BUTTON -->
        <a style="display: none;" href="/login/" data-bind="visible: !validSession(), click: GameLauncher.openGame" data-gameid="<?php echo @$this->row["DetailsURL"];?>" class="login-button--mobile ui-scroll-top">LOGIN</a>
        <!-- JOIN NOW -->
        <a style="display: none;" data-bind="visible: !validSession()" href="/register/" alt="Join Now" class="join-now-button--mobile">Join Now</a>
        <!-- / CTAS MOBILE -->
        



        <!-- GAME DETAIL WRAP -->
        <div class="game-detail-content__right">

            <!-- GAME DETAILS -->
            <aside class="game-detail__wrap">
                <h3>Game Details</h3>
                <ul class="game-detail-content__right__game-table">
                    <li><span class='game-detail-content__right__game-table__title'>Publisher</span><span itemprop="publisher" class="game-detail-content__right__game-table__provider"><?php echo @$this->row["ProviderName"];?></span></li>
                    <li><span class='game-detail-content__right__game-table__title'>Paylines</span><?php echo @$this->row["PayLines"];?></li>
                    <li><span class='game-detail-content__right__game-table__title'>Feature</span><?php echo @$this->row["Feature"];?></li>
                    <li><span class='game-detail-content__right__game-table__title'>Minimum Bet</span><?php echo @$this->row["MinBet"];?></li>
                    <li><span class='game-detail-content__right__game-table__title'>Progressive</span><?php echo @$this->row["Progressive"];?></li>
                    
                    <li><span class='game-detail-content__right__game-table__title'>RTP</span><?php  echo @$this->row["RTP"];?></li>
                    
                </ul>
            </aside>
                        
            <!-- CTAS DESKTOP -->
            <span <?php if(@$this->row["Status"]==1) echo 'data-bind="click: GameLauncher.openGame"';?> data-infopage="true" data-gameid="<?php echo @$this->row["DetailsURL"];?>" data-gametitle="<?php echo @$this->row["Title"] ?>" data-icon="<?php echo @$this->row["Image"];?>" data-nohijack="true" class="games-info__try">
                <div style="display: none;" data-bind="visible: validSession() && <?php echo @$this->row["Status"];?>" class="overlay">
                    <p alt="Play Now" class="play-now-button--desktop">Play Now</p>
                </div>
            </span> 

            <!-- LOGIN BUTTON -->
            <a style="display: none;" href="/login/" data-bind="visible: !validSession(), click: GameLauncher.openGame" data-gameid="<?php echo @$this->row["DetailsURL"];?>" class="login-button--desktop ui-scroll-top">LOGIN</a>

            <!-- JOIN NOW -->
            <div style="display: none;" data-bind="visible: !validSession()" class="overlay">
                <p alt="Join Now" class="join-now-button--desktop"><a href="/register/">Join Now</a></p>
            </div>


        </div>
                            <!-- AQUISITION BANNER -->
        <div class="game-detail-content__promo">
            <?php edit('gamesdetail','aquisition-banner');?>
            <?php @$this->repeatData($this->content['aquisition-banner']);?>
        </div>
    </div>
</div>