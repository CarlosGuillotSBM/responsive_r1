<script type="application/javascript">
	skinModules.push( { id: "PersonalDetailsNew" } );
</script>

<div class="mobile-main-content__modal">
  <!-- HEADER CLOSE MODAL -->
  <a class="mobile-main-content__header">My Personal Details</a>
  <!-- CONTENT -->
  <div class="mobile-main-content__content">
	<?php include '_partials/start-skin-only/start-myaccount-details.php' ?>
  </div>
  <!-- / CONTENT -->
</div>