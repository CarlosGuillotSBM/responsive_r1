<div class="content-holder home-seo">
	<!-- content holder title -->
	<!-- content holder content area -->
	<div class="content-holder__content">
	<!-- content holder content full width inner holder -->
		<!-- content holder content left inner holder -->
		<div class="content-holder__content_full-width">
			<div class="content-holder__inner">
				<!-- Bingo Scedule  -->
				<?php include "_partials/home-seo-section/home-seo-inner.php"; ?>
				<!-- end Bingo Scedule  -->
			</div>
		</div>
	</div>
</div>