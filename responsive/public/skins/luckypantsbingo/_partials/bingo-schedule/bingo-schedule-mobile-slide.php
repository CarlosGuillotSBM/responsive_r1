<li
    data-bind="click: $root.BingoScheduler.openRoom,
               css: { active: secondsLive() < 60, 'funded' : playText === 'Funded Only' }" >
	<div class="name" data-bind="html: name"></div>
	<div class="price" data-bind="text: cardPrice"></div><div class="room-name"><span class="icon" data-bind="text: displayGameType"></span></div>
	<div class="jackpot" data-bind="text: displayJackpot"></div>
	<div class="starts" data-bind="text: displaySecondsLive"></div><a class="cta-play">PLAY</a>
</li>