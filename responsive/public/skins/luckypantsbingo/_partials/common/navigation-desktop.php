
  <!--  *********** Navigation Desktop Loogged out  *********** -->

<div class="navigation-desktop">
		<ul class="navigation-desktop__nav">
			<h1 class="seo-logo">
				<a href="/" data-hijack="true" class="header__logo ui-go-home-logo"><img src="/_images/logo/logo-sml.png"/></a>
				<span style="display: none;">Lucky Pants Bingo online the best online bingo sites on mobile and desktop.</span>
			
			</h1>
			<li id="home"><a class="ui-go-home" href="/" data-hijack="true">Home</a></li>
			<li id="bingo" data-bind="visible: !validSession()"><a href="/bingo/" data-hijack="true">Bingo</a></li>
			<li style="display: none" data-bind="visible: validSession, click: playBingo"><a>Play Bingo</a></li>
			<li id="slots"><a href="/slots/" data-hijack="true">Slots &amp; Games</a></li>
			<li id="my-favourites" style="display: none" data-bind="visible: validSession()"><a href="/my-favourites/" data-hijack="true" data-bind="visible: Favourites.favourites().length > 0">My Favourites</a></li>
<!-- 			<li id="games"><a href="/games/" data-hijack="true">Games</a></li> -->
			<li id="promotions"><a href="/promotions/" data-hijack="true">Promos</a></li>
			<li class="radical-dice" data-bind="visible: displayThreeRadical(), click: ThreeRadical.requestToken" style="display: none" class>
				<svg>
					<g id="sparkle1">
						<path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
						<path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
						<path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
					</g>
				</svg>
				<a href="" data-hijack="true" style="padding-top: 2px;">Daily play <img class="bounce-dice" src="/_images/common/nav-social-icons/dice.svg" alt="Extras"/></a>
			</li>
			<li id="lucky-club"><a href="/lucky-club/" data-hijack="true">Lucky Club</a></li>
			<li id="community"><a href="/community/" data-hijack="true">Community</a></li>
			<li id="help"><a href="/help/" data-hijack="true">Help</a></li>


			<ul class="nav-social-icons">
				<li><a height="27px" width="27px" target="_blank" href="#" onclick="javascript:void window.open('<?php echo config('LiveChatURL'); ?>','1323353895481','width=590,height=480,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;" data-hijack="false"><img src="/_images/common/nav-social-icons/chat.svg" alt="Chat"></a></li>

				<!-- Hidden due to task to add my favourites link to loggen in view -->

				<li><a style="display: none" height="27px" width="27px" target="_blank" data-bind="visible: !validSession()" href="https://facebook.com/luckypantsbingo" data-hijack="false"><img src="/_images/common/nav-social-icons/facebook.svg" alt="Facebook"></a></li>
				<li><a style="display: none" height="27px" width="27px" target="_blank" data-bind="visible: !validSession()" href="https://twitter.com/luckypantsbingo" data-hijack="false"><img src="/_images/common/nav-social-icons/twitter.svg" alt="Twitter"></a></li>
			</ul>


					<div class="header__login" >

					<!-- Login Register Area IN-->
					<div class="user-nav" data-bind="visible: validSession()" style="display:none;">

						<!-- Nav Left -->
						<div class="user-nav-left">
							<div class="user-nav__alias" title="Account Balance">
								<a href="/start/"  data-hijack="true">
									<p>Balance</p>
									<span data-bind="text: balance" ></span>
								</a>
							</div>
							<div class="user-nav__balance" title="Real Money">
								<a href="/start/"  data-hijack="true">
									<p>Real</p>
									<span data-bind="text: real"></span>
								</a>
							</div>
							<div class="user-nav__slots" title="Bonus Balance">
								<a href="/start/"  data-hijack="true">
									<p>Bonus</p>
									<span data-bind="text: bonusBalance"></span>
								</a>
							</div>
						</div>

						<!-- Nav Right -->
						<div class="user-nav-right">

							<!-- Message Inbox-->
							<?php if(config("inbox-msg-on")): ?>
								<div class="user-nav__message-inbox" title="Message Inbox" data-bind="visible: MailInbox.unreadMessages() > 0">
									<a href="/start/"  data-hijack="true" >
										<img src="/_images/balance-area/icon-message-inbox.svg">
										<span class="user-nav__message-inbox__value" data-bind=" text: MailInbox.unreadMessages()"></span>
									</a>
								</div>
							<?php endif; ?>
							<!-- /Message Inbox-->

							<div id="cashier" class="user-nav__cashier" title="Launch the Cashier"><a href="/cashier/"  data-hijack="true">CASHIER</a></div>
							<div class="user-nav__logout"><a data-bind="click: logout"  data-hijack="true" data-gtag="Logout Desktop">LOGOUT</a></div>
							<!-- <div id="my-account"  class="user-nav__my-account"><a href="/my-account/"  data-hijack="true">My Account</a></div> -->

						</div>

					</div>
					<!-- /Login Register Area IN-->


					<!-- PLAY BINGO-->
					<a class="button--join" data-hijack="true" data-bind="visible: validSession() , click: playBingo" style="display:none;">Play Bingo</a>


					<!-- Login Register Area OUT-->
					<div class="logged-out-buttons" data-bind="visible: !validSession()" style="display:none;">
						<?php include'_partials/login/login-box.php'; ?>
						<a class="button--join" href="/register/">Join Now</a>
					</div>
					<!-- /Login Register Area OUT-->

				</div>
		</ul>
</div>

  <!--  *********** Navigation Desktop Loogged out end *********** -->
