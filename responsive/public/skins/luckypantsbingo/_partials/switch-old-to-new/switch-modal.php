

<!-- HEADER -->
<div class="header">
	<h1>LUCKY PANTS BINGO HAS GOT A BRAND NEW-LOOK!</h1>
	
</div>

<!-- INFO -->
<div class="info">
	<ul>
		<p>Welcome to our fantastic new website! You can now enjoy these upgraded features:</p>
		<li>Brand new look and feel on mobile and PC devices.</li>
		<li>Faster, simpler cashier.</li>
		<li>All your favourites games + new NetEnt Slots.</li>
		<li>Brand new message inbox - never miss any of our great news or offers.</li>
	</ul>
	<div class="luke">
		<span></span>
	</div>
</div>

<!-- FOOTER -->
<div class="bottom">
	<h2>THERE'S MORE WAYS THAN EVER TO GET LUCKY, SO SIT BACK, PUT ON YOUR LUCKY PANTS AND ENJOY THE RIDE!</h2>
	<!-- CTAS -->
	<div class="cta">
        <!--
		<a class="stay" data-bind="click: RadSwitch.switchModalStay">STAY ON THE NEW SITE</a>
		<a class="switch" data-hijack="true" data-bind="click: RadSwitch.switchModalSwitch" target="_blank">SWITCH BACK TO CLASSIC SITE</a>
		-->
	</div>
</div>