<!-- Mobile Menu -->
<nav class="mobile-menu" style="opacity: 1">



		<div class="mobile-menu-lists ripple">

			<!-- menu mobile top area / slide back  -->
			<div class="mobile-menu-top">
				<div class="mobile-menu-item back-link">
					<span class="menu-link">LUCKY PANTS MENU</span>
				</div>
			</div>

			<!-- menu lists -->
			<ul class="mobile-menu-list">
				<!-- section title -->
				<li class="mobile-menu-item home-link">
					<a class="menu-link" data-hijack="true" href="/" title="Home">Home</a>
				</li>
				<!-- section title -->
				<li class="mobile-menu-section-title">Bingo</li>
				<li class="mobile-menu-item bingo-link">
					<a class="menu-link" data-hijack="true" href="/bingo/" data-bind="visible: !validSession()" title="Bingo">Bingo</a>
					<a class="menu-link" data-hijack="true" style="display: none" data-bind="visible: validSession, click: playBingo" title="Play Bingo">Play Bingo</a>
				</li>
				<!-- section title -->
				<li class="mobile-menu-section-title">Games</li>
				<!-- menu items-->
				<!-- commented out search link while feature is finalized -->
				<li class="mobile-menu-item gamesearch-link">
					<a id="ui-search-screen" class="menu-link"  title="New Games">Search Games</a>
				</li>
				<li class="mobile-menu-item newgames-link">
					<a class="menu-link" data-hijack="true" href="/slots/new/" title="New Games">New Games</a>
				</li>
				<li class="mobile-menu-item favourites-link" data-bind="visible: validSession()">
					<a class="menu-link" data-hijack="true" href="/my-favourites/" title="Favourite Games">My Favourites</a>
				</li>
				<li class="mobile-menu-item slots-link">
					<a id="ui-slots-menu-link">Slots </a><span class="slots-categories is-original">
					<span id="UI-id">Categories
						<svg class="chevron-icon is-original" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
							<path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#ffffff">
							</path>
						</svg>
					</span>
				</span>
			</li>
			<!-- Slots Categories Hidden Slide Down Panel -->
			<div class="games-filter-menu__popover-wrap is-original">

				<div class="games-filter-menu__popover game-filter-menu" align="center">
					<span class="nub"></span>
					<!-- include game categories-->
					<?php include "_global-library/partials/game-filter-menu/game-filter-categories.php" ?>
				</div>
			</div>

			<li class="mobile-menu-item scratchcards-link">
				<a class="menu-link" data-hijack="true" href="/scratch-and-arcade/" title="Scratchcards">Scratch &amp; Arcade</a>
			</li>

			<li class="mobile-menu-item tablecard-link">
				<a class="menu-link" data-hijack="true" href="/table-card/" title="Table & Card">Table &amp; Card</a>
			</li>

			<li class="mobile-menu-item allgames-link">
				<a class="menu-link" data-hijack="true" href="/games/" title="All Games">All Games</a>
			</li>
			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title">Rewards</li>
			<!-- menu items-->
			<li class="mobile-menu-item promotions-link">
				<a class="menu-link" data-hijack="true" href="/promotions/" title="Promotions">Promotions</a>
			</li>
			<li class="mobile-menu-item dailyplay-link" data-bind="visible: displayThreeRadical()">
				<img class="bouncing-dice" src="/_images/common/icons/mv-dice-burger.svg" alt="Extras"/>
				<a class="menu-link" data-hijack="false" href="/game-frame/three-radical" target="threerad" title="Daily play">Daily play		
					<svg class="dice-svg">
						<g id="sparkle1">
							<path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
							<path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
							<path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
						</g>
					</svg>
				</a>
			</li>
			<li class="mobile-menu-item loyalty-link">
				<a class="menu-link" data-hijack="true" href="/lucky-club/" title="Extra Club">Lucky Club</a>
			</li>
			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title">Banking</li>
			<!-- menu items-->
		<!-- 	<li class="mobile-menu-item myaccount-link" data-bind="visible: validSession()">
				<a class="menu-link" data-hijack="true" href="/my-account/" title="My Account">My Account</a>
			</li> -->
			<li class="mobile-menu-item cashier-link" data-bind="visible: validSession()">

				<a class="menu-link" data-hijack="false" href="/cashier/" title="Cashier">Cashier</a>
			</li>

			<li class="mobile-menu-item banking-link">
				<a class="menu-link" data-hijack="true" href="/banking/" title="Banking">Banking info</a>
			</li>

			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title">Social</li>
			<!-- menu items-->
			<li class="mobile-menu-item community-link">
				<a class="menu-link" data-hijack="true" href="/community/" title="Community">Community</a>
			</li>

			<li class="mobile-menu-section-title">Support</li>


			<li class="mobile-menu-item support-link">
				<a class="menu-link" data-hijack="true" href="/help/" title="Help">Help</a>
			</li>
			<li class="mobile-menu-item faq-link">
				<a class="menu-link" data-hijack="true" href="/faq/" title="Faq">Faq</a>
			</li>



			<li class="mobile-menu-item terms-link">
				<a class="menu-link" data-hijack="true" href="/terms-and-conditions/" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
			</li>
			<li class="mobile-menu-item about-us-link">
				<a class="menu-link" data-hijack="true" href="/about-us/" title="About Us">About Us</a>
			</li>
			<li class="mobile-menu-item privacy-link">
				<a class="menu-link" data-hijack="true" href="/privacy-policy/" title="Privacy Policy">Privacy Policy</a>
			</li>
			<li class="mobile-menu-item settings-link" data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none">
				<a class="menu-link" data-hijack="false" href="/cashier/?settings=1" title="Player Settings">Player Settings</a>
			</li>
			<li class="mobile-menu-item responsiblegaming-link">
				<a class="menu-link" data-hijack="true" href="/responsible-gaming/" title="Responsible Gaming">Responsible Gaming</a>
			</li>
			<li class="mobile-menu-item complaints-disputes-link">
				<a class="menu-link" data-hijack="true" href="/complaints-and-disputes/" title="Complaints and Disputes">Complaints and Disputes</a>
			</li>
			<li class="mobile-menu-item archive-link">
				<a class="menu-link" data-hijack="true" href="/archive/" title="Archive">Archive</a>
			</li>
			<!--				<li class="mobile-menu-item gamelimits-link">-->
			<!--					<a class="menu-link" data-hijack="true" href="/game-limits/" title="Game Limits">Game Limits</a>-->
			<!--				</li>-->
			<li class="mobile-menu-item logout-link" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">
				<a class="menu-link" data-hijack="true" title="Log Out" style="display: none" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">Log Out</a>
			</li>
			<li class="mobile-menu-item" style="visibility:hidden">
				<!-- spacer to move last link from bottom -->
			</li>
			<!-- /menu items-->
		</ul>
	</div>
	<!-- / menu lists -->
</nav>
<!-- END New Mobile Menu April 2015 Author: Marcus Chadwick -->