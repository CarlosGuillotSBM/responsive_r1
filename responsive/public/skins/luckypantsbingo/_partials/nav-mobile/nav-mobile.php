<!-- Mobile Top Bar -->
<div class="tab-bar nav-for-mobile clearfix">
	<!-- COOKIE POLICY -->
	<?php include "_global-library/partials/modals/cookie-policy.php" ?>
	<!-- MOBILE LOGO -->
	<h1 class="seo-logo">
		<a data-hijack="true" href="/" class="site-logo ui-go-home-logo menu-link"><img src="/_images/common/logo-horitzontal.svg"/></a>
		<span style="display: none;">Lucky Pants Bingo online the best online bingo sites on mobile and desktop</span>
	</h1>

	<!-- MOBILE ICON -->
	<h1 class="seo-logo">
		<a data-hijack="true" href="/" class="site-logo-mobile ui-go-home-logo"><img src="/_images/common/lp-apple-touch-icon.png"/></a>
		<span style="display: none;">Lucky Pants Bingo online the best online bingo sites on mobile and desktop</span>
	</h1>
	<!-- Start of navigation toggle -->
	<div class="menu-toggle-wrap">
		<div class="burger">
			<div class="span"></div>
		</div>
	</div>
<!-- MENU ICONS -->
	<div class="icons-menu">
		<span>
			<a class="dice-icon" data-bind="visible: displayThreeRadical()" href="/game-frame/three-radical" data-hijack="false" target="threerad" style="display: none">
				<svg>
					<g id="sparkle1">
						<path class="st1" d="M11,100.2c0,0,2-10.4,3.8,0c0,0,10,1.8,0,3.6c0,0-1.8,10.6-3.8,0C5,102.7,5.3,101.5,11,100.2z"/>
						<path class="st2" d="M6.3,85.1c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C2.6,86.6,2.7,85.8,6.3,85.1z"/>
						<path class="st3" d="M31.2,108.9c0,0,1.2-6.4,2.4,0c0,0,6.2,1.1,0,2.2c0,0-1.1,6.6-2.4,0C27.5,110.4,27.6,109.6,31.2,108.9z"/>
					</g>
				</svg>
				<img class="bounce-dice" src="/_images/common/icons/dice-mobilenav.svg" alt="extras">
			</a>
			<!-- Message Inbox-->
			<?php if(config("inbox-msg-on")): ?>
			<div class="mail-icon" title="Message Inbox" style="display: none" data-bind="visible: validSession">
				<a href="/my-messages/" target="_top">
					<img src="/_images/common/mobile-menu-icons-svg/mail-icon.svg">
				</a>
				<span class="mail-value" data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
			</div>
			<?php endif; ?>
			<!-- /Message Inbox-->
			<!-- account Icon -->
			<button class="account-icon" style="display: none" data-bind="visible: validSession">
			<img src="/_images/common/mobile-menu-icons-svg/acc-icon.svg" alt="account">
			</button>
			<!-- Search Icon -->
			<button class="search-icon">
			<img src="/_images/common/mobile-menu-icons-svg/search-icon.svg" alt="search">
			</button>
		</span>
	</div>
	<!-- balance display -->
	<div class="balance-display" style="display: none" data-bind="visible: validSession"><span data-bind="text: balance">...</span></div>

	<!-- logged out -->
	<div class="nav-for-mobile__ctas">
		<!--		<a style="display: none" data-bind="visible: validSession()" data-reveal-id="login-modal-deposit" class="login-button--nav-for-mobile menu-link">INFO</a>-->
		<a style="display: none" data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login-button--nav-for-mobile menu-link ui-scroll-top">LOGIN</a>
		<a style="display: none" data-bind="visible: !validSession()" class="join-button--nav-for-mobile menu-link" href="/register/">JOIN NOW</a>
	</div>
	<!-- /logged out -->



</div>
<!-- / Mobile Top Bar -->

<!-- Menu Mobile -->
<?php  include'_partials/nav-mobile/menu-mobile.php'; ?>
<!-- Account Menu Mobile -->
<?php include'_partials/nav-mobile/account-menu-mobile.php'; ?>
<!-- Balance Display Mobile -->
<?php include'_partials/nav-mobile/balance-display-mobile.php'; ?>
<!-- My Messages Mobile -->
<?php //include'_partials/my-messages-mobile/my-messages-mobile.php'; ?>
<!-- My Messages Mobile -->
<?php include'_partials/my-messages-mobile/single-message-mobile.php'; ?>
