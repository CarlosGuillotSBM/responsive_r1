var sbm = sbm || {};

/**
 * Magical Menu
 * Opens a popover menu with game filters
 */
sbm.magicalMenu = function () {
    "use strict";

    return {
        run: function () {

            var $menuBtn = $("#magicalMenuBtnLnk");
            //var $menuBtnLink = $("#magicalMenuBtnLnk");
            var $menuFilter = $("#ui-game-filter-menu");
            var $menuContainer = $(".games-filter-menu");

            // on magical menu click
            var onMagicalMenuClick = function (e) {

                e.preventDefault();

                // toggle visibility
                $menuFilter.toggle();
                $menuBtn.toggleClass("selected");

                // hide menu when clicked outside
                var visibility = $menuFilter.is(":visible");
                if (visibility) {
                    $(document).on("mouseup", function (e) {

                        if (!$menuContainer.is(e.target) && $menuContainer.has(e.target).length === 0) {
                            hideMenu();
                        }

                    });
                }

            };

            var hideMenu = function () {
                $menuFilter.hide();
                $(document).off("mouseup");
            };

            var toggleMenu = function (e) {
                e.stopPropagation();
                hideMenu();
            };

            // attach click event to magical menu
            $menuBtn.on("click", onMagicalMenuClick);
            //$menuBtnLink.on("click", toggleMenu);
        }
    };

};