<!-- MOBILE CASHBACK -->
<div style="display: none;" data-alert class="mobile-cashback alert-box" data-bind="visible: validSession">
    <!--<a href="#" class="close" data-bind="visible: ClaimCashback.cbAmount() > 0" >X</a>-->
    <?php include'_global-library/partials/start/start-claim-cashback.php'; ?>
</div>
<!-- / MOBILE CASHBACK -->

<!-- MOBILE UPGRADE BONUS -->
<div style="display: none;" data-alert class="mobile-cashback alert-box" data-bind="visible: validSession">
    <!--<a href="#" class="close" data-bind="visible: ClaimUpgradeBonus.ubAmount() > 0" >X</a>-->
    <?php include'_global-library/partials/start/start-claim-upgrade-bonus.php'; ?>
</div>
<!-- / MOBILE UPGRADE BONUS -->

<!-- MOBILE HOME VIEW -->
<div class="home-mobile">
    <!-- offer -->
    <?php edit($this->controller,'home-adaptive-banners', ''); ?>
    <div class="home-main-slider desktop-mobile ui-scheduled-content-container">
        <ul>
            <!-- Hero Area -->
            <li>
                <?php @$this->repeatData($this->content['home-adaptive-banners']);?></a>
            </li>
        </ul>
    </div>
    <!-- bingo schedule -->
    <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>
    <!-- Featured Games Box with tabbed content -->
    <div class="home-mobile-carousels">
        <?php edit($this->controller,'home-carousels'); ?>
        <?php @$this->repeatData($this->content['home-carousels']);?>
    </div>

    <?php include'_partials/home-seo-section/home-seo-holder.php'; ?>
</div>

<!-- /MOBILE HOME VIEW -->

<!-- MOBILE MY ACCOUNT OVERLAY -->
<div class="account-my-bonuses-mobile">
    <!-- future US to be implemented -->
</div>
<div class="myaccount-claim-code-mobile">
    <?php include '_partials/my-account-mobile/myaccount-claim-code-mobile.php' ?>
</div>
<div class="myaccount-details-mobile">
    <?php include '_partials/my-account-mobile/myaccount-details-mobile.php' ?>
</div>
<div class="myaccount-convert-points-mobile">
    <?php include '_partials/my-account-mobile/myaccount-convert-points-mobile.php' ?>
</div>