<?php

/**
 * Include the different partials depending on the device
 * This page is no responsive and some tablets need to have both views
 * because they show the phone view in portrait and desktop oin landscape
 */

if (config("RealDevice") == 3) {
    // phone
    include 'home-mobile.php';
} else {
    include 'home-mobile.php';
    include 'home-desktop.php';
}