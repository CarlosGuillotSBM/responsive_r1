<!--wrapper -->
<div class="main-content promotions-content">
        <script type="application/javascript">   
          skinModules.push({
              id: "RadFilter"
          });
      </script>

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  
  <div class="middle-content__box">

    <section class="section promotions">
      <?php edit($this->controller,'promotions'); ?>

      <!-- DESKTOP PROMOTIONS CATEGORY MENU -->
      <dl class="tabs" data-tab>

            <dd class="first active"><a id="ui-activity-tab"  href="#promotions-0">All Promos</a></dd>
            <dd><a id="ui-activity-tab" href="#promotions-2">Today</a></dd>
            <dd><a id="ui-activity-tab" href="#promotions-3">Tomorrow</a></dd>
            <dd><a id="ui-activity-tab" href="#promotions-4">Weekend</a></dd>
            <dd class="newbie"><a id="ui-activity-tab" href="#promotions-5">Newbie</a></dd>
            <dd><a id="ui-activity-tab" href="#promotions-6">Specials</a></dd>
<!--             <dd><a class="last" href="#promotions-7">Past Promos</a></dd> -->

      </dl>
      
      <!-- MOBILE PROMOTIONS CATEGORY MENU -->
      <div class="mobile-tabs-menu__wrap">
       <a href="#" data-dropdown="id02" data-options="is_hover:false" class="button dropdown mobile-tabs-menu__top">All Promos</a>
        <div id="id02" data-dropdown-content class="f-dropdown mobile-tabs-menu__dropdown">
          <dl class="tabs show-for-small-only" data-tab>

            <dd class="first active"><a href="#promotions-0">All Promos</a></dd>
            <dd><a href="#promotions-2">Today</a></dd>
            <dd><a href="#promotions-3">Tomorrow</a></dd>
            <dd><a href="#promotions-4">Weekend</a></dd>
            <dd class="newbie"><a href="#promotions-5">Newbie</a></dd>
            <dd><a href="#promotions-6">Specials</a></dd>
<!--             <dd><a href="#promotions-7">Past Promos</a></dd> -->

          </dl>
        </div>

      </div>
      
      <div class="tabs-content">

        <!-- PROMOTIONS CATEGORY TABS -->

        <!-- ALL -->
        <div class="content active" id="promotions-0">
          <div class="content-holder__inner">
            <?php //@$this->repeatDataWithFilter($this->promotions,'ALL',1, "_partials/promos/promo.php"); ?>
            <?php $variables = array(); 
              $variables["WhichTab"] = 1; ?>
            <?php $this->repeatData($this->content["promotions"],1,"_global-library/partials/promotion/promotion.php",$variables); ?>
          </div>
        </div>

        <!-- Today -->
        <div class="content" id="promotions-2">
          <div class="content-holder__inner">
            <?php $variables["WhichTab"] = 2; ?>
            <?php @$this->repeatDataWithFilter($this->content["promotions"],getToday().',ALL',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
          </div>
        </div>

        <!-- Tomorrow -->
        <div class="content" id="promotions-3">
          <div class="content-holder__inner">
            <?php $variables["WhichTab"] = 3; ?>
            <?php @$this->repeatDataWithFilter($this->content["promotions"],getTomorrow().',ALL',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
          </div>
        </div>

        <!-- Weekend -->
        <div class="content" id="promotions-4">
          <div class="content-holder__inner">
            <?php $variables["WhichTab"] = 4; ?>
            <?php @$this->repeatDataWithFilter($this->content["promotions"],'FRI,SAT,SUN,ALL',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
          </div>
        </div>

        <!-- Newbie -->
        <div class="content" id="promotions-5">
          <div class="content-holder__inner">
            <?php $variables["WhichTab"] = 5; ?>
            <?php @$this->repeatDataWithFilter($this->content["promotions"],'NEWBIE',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
          </div>
        </div>

        <!-- Specials -->
        <div class="content" id="promotions-6">
          <div class="content-holder__inner">
            <?php $variables["WhichTab"] = 6; ?>
            <?php @$this->repeatDataWithFilter($this->content["promotions"],'SPECIALS',1, "_global-library/partials/promotion/promotion.php",$variables); ?>
          </div>
        </div>

        <!-- Past Promos -->
        <div class="content" id="promotions-7">
          <div class="content-holder__inner">
            <?php @$this->repeatDataWithFilter($this->pastPromos,7,1, "_partials/promos/promo.php"); ?>
          </div>
        </div>


        <a class="promotional-tcs" href="/terms-and-conditions/general-promotional-terms-and-conditions/">General Promotional Terms &amp; Conditions</a>

      </div>
      <!-- END TABS CONTENT -->
      <div class="clearfix"></div>


    </section>
    <!-- END PROMOTIONS SECTION -->
    
  </div>
  <!-- END MIDDLE CONTENT BOX-->
  <!--  PROMOTIONS SEO CONTENT-->
  <?php edit($this->controller,'promotions-seo-content'); ?>
  <?php @$this->getPartial($this->content['promotions-seo-content'],1); ?>
  <!-- <article class="text-partial content-holder">
    <div class="content-holder__content">
      <div class="content-holder__title">
        <h1>Best Casino Promotions, Offers and Bonuses - LUCKYPANTS - As Seen on TV</h1>
      </div>
      <div class="content-holder__inner">
        <p>Welcome to the Spin and Win Promotions page, home to some of the best Casino promotions online. Amongst the Promotions we offer are sign up bonuses, deposit match bonuses, cash backs, tournaments, competitions and Loyalty Points! You can see above exactly which promotions we have available and, as well as regular weekly offers, we also run Special Promotions each and every month. If you’re a big casino player – whether a member of our site or thinking of joining – check out our VIP tab above which shows offers tailored for you!</p>
        <p>There exists many sites that offer different types of promotions nowadays but do we know who the real winners are? <a href="/">At Spin and Win online Casino</a>, you can be sure that you will be presented with the top promotional offers available. Originality and ingenuity are amongst the key factors that the site takes into consideration when creating its promotions. The site even has a special team that undertakes the creation of exclusive offers.</p>
        <p>Upon clicking on this Promotions tab, you will have full details on the ongoing promotions; amongst which are the first deposit package, 2nd and 3rd deposit offers, first month Cashback, Triple Bonus, Wallet Whopper, Cashback, Free Bet, Weekly Deposit, Daily Payback, Free Spins, Daily Match Deposit, Loyalty Points and VIP Loyalty, with more added on a very regular basis. So bookmark this Spin and Win Casino page as your home for all the Casino offers you need!</p>
      </div>
    </div>
  </article> -->
  <!--  END PROMOTIONS SEO CONTENT-->
</div>

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>