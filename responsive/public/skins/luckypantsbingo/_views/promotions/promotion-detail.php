<script type="application/javascript">
  skinModules.push({
    id: "OptIn",
    options: {
        modalSelectorWithoutDetails: "#custom-modal",
        details : true
    }
  });
</script>

<div class="two-columns-content-wrap">
  <!-- HOME MAIN CONTENT AREA -->
  <section class="section">
    <!-- PROMOTIONS CONTENT -->
    <div class="middle-content__box">
      <div class="clearfix"></div>

      <?php edit($this->controller,'promotions'); ?>
      
      <!-- promotions list -->
      <!-- Promotions detail -->
      <div class="promotions-detail">
            <!-- title -->
    <div class="section-left__title">
      <h4><?php echo str_replace('-', ' ', $this->controller_url)?></h4>
    </div>

        <div class="section__header section-full-width__title">
         
         <h1 class="section-left__title"><?php echo $this->content["Title"];?></h1>
        </div>


        <article class="promotion--featured">

          <div class="two-columns-content-wrap">
            
            <!-- LEFT COLUMN -->
            <div class="two-columns-content-wrap__left">
              <div class="promotion__content__wrapper">
                
                <div class="promotion__content__img__wrapper">                    
                    <a data-hijack="true" data-bind="click: playBingo">
						          <img src="<?php echo $this->content["Text8"] != '' ? $this->content["Text8"] : $this->content["Image"]?>" alt="<?php //echo $this->content["Alt"]?>">
					         </a>
                </div>
                
                <!-- TOP CTA'S -->
                <div class="promo__container__row">
                    <a data-hijack="true" style="display: none" class="promo__container__button cta-opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>

                    <!-- span to be removed by javascript - DO NOT REMOVE!! -->
                    <span></span>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php }?> 
                </div> 

                <!-- CONTENT -->
                <div class="promo__container__content">
                  <?php echo $this->content["Body"]; ?>
                </div>
 
                <!-- BOTTOM CTA'S -->
                <div class="promo__container__row">
                    <a data-hijack="true" style="display: none" class="promo__container__button cta-opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>

                    <!-- span to be removed by javascript - DO NOT REMOVE!! -->
                    <span></span>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php }?> 
                </div>

                <!-- PROMO FOOTER NAV MOBILE -->
                <div class="footer-nav__container">
                    <a data-hijack="true" style="display: none" data-tournament="<?php echo $this->content['DetailsURL'] ?>" class="cta-opt-in">OPT-IN</a>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php }?>                     
                    <a data-hijack="true" href="/promotions/<?php echo $this->getNextPromoURL($this->content["DetailsURL"]); ?>" class="cta-next-promo">NEXT PROMO ></a>
                </div>

                <!-- Accordion -->
                <dl id="ui-promo-tcs" class="promotion__terms-conditions accordion" data-accordion>
                  <dd class="accordion-navigation active">
                    <a href="#panelTC" class="accordion-navigation__toggle">Terms &amp; Conditions</a>
                    <div id="panelTC" class="content active">
                      <?php echo @$this->content["Terms"];?>
                    </div>
                  </dd>
                </dl>
                <!-- /Accordion -->
				
              </div>
            </div>
            <!-- /LEFT COLUMN -->

            <!-- RIGHT COLUMN -->
            <div class="two-columns-content-wrap__right ui-scheduled-content-container">


              <?php edit($this->controller,'promotion-details__side-content',$this->action); ?>
              <?php @$this->repeatData(@$this->sideBanners); ?>
              <?php //edit('promotions','promo-detail__'.$this->action); ?>
              <?php //@$this->repeatData($this->promotions['promo-detail__'.$this->action]); ?>
              <div class="clearfix"></div>
            </div>
            <!-- /RIGHT COLUMN -->

          </div>
        </article>

      </div>
    </div>

    <!-- RELATED PROMOS -->
    <div class="middle-content__box related-promos">
      <!-- Title -->
      <h1 class="section-left__title">Related Promos</h1>
      <!-- /Title -->
      <div class="related-promos__wrap">
        <!-- promos -->
        <?php @$this->getPartial($this->promotions['promotions'],2, "_global-library/partials/promotion/promotion.php"); ?>
        <?php @$this->getPartial($this->promotions['promotions'],3, "_global-library/partials/promotion/promotion.php"); ?>
        <?php @$this->getPartial($this->promotions['promotions'],4, "_global-library/partials/promotion/promotion.php"); ?>
      </div>

      <!-- related promos slider on mobile -->
      <?php include "_partials/promos/related-promos-slider.php"; ?>

    </div>
    <!-- /RELATED PROMOS -->


  </section>

</div>
<!-- END HOME MAIN CONTENT AREA -->  

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>
