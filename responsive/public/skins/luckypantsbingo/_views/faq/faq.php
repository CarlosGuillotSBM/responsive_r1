<!-- MAIN CONTENT AREA -->
<div class="main-content">

  <!-- TITLE WRAP -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  

  <!-- CONTENT -->
  <div class="content-template">

    <!-- TWO COLUMNS LAYOUT -->
    <div class="two-columns-content-wrap__left__title">
      
      <!-- LEFT COLUMN -->
      <div class="two-columns-content-wrap__left">
        <!-- WRAP CONTENT LEFT -->
        <div class="two-columns-content-wrap__left__inner" itemscope itemtype="http://schema.org/SoftwareApplication">
          
        <!-- INTRO -->
        <?php edit($this->controller,'faq-intro'); ?>
        <?php @$this->getPartial($this->content['faq-intro'],1, "_global-library/_editor-partials/text-only.php"); ?>

        <!-- ACCORDION LIST -->
        <?php edit($this->controller,'faq-details'); ?>
        <ul class="accordion accordion-wrap">
          <?php @$this->repeatData($this->content['faq-details'], 1, "_global-library/partials/common/text-accordion.php");?>
        </ul>

        </div>
      </div>

      <?php if (config("RealDevice") != 3) { ?>

      <!-- RIGHT COLUMN -->
      <div class="two-columns-content-wrap__right">
        <?php edit('faq','faq__side-content',$this->subaction); ?>
        <?php @$this->repeatData($this->content['faq__side-content']); ?>
        <div class="clearfix"></div>
      </div>

      <?php } ?>

    </div>
    <!-- /TWO COLUMNS LAYOUT -->

  </div>
  <!-- /MAIN CONTENT AREA -->
</div>
  <!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>