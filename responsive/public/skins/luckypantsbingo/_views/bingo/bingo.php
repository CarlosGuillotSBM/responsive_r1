<!-- bingo content -->
<div class="main-content bingo-content">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>

  <!-- Bingo Schedule Holder Row -->
  <div class="mobile-hide-bingo">
    <?php include "_partials/bingo-schedule/bingo-schedule-holder-home.php"; ?>
  </div>

  <div class="clearfix"></div>

    <div class="mobile-show-bingo">
        <?php include'_partials/bingo-schedule/bingo-schedule-mobile.php'; ?>
    </div>

    <div class="clearfix"></div>

    <article class="bingo-hub">
      <!-- bingo content part1 -->
    <div class="bingo-content__part1 middle-content__box">

        <div class="section__header section-full-width__title">
          <?php edit($this->controller,'bingo-top-title'); ?>
          <h4><span><?php @$this->getPartial($this->content['bingo-top-title'],1); ?></span></h4>
        </div>

        <!-- bingo rooms slider -->
        <div class="bingo-room-slider">
          <?php include "_partials/bingo-rooms/bingo-rooms-slider.php"; ?>
        </div>

    </div>

    <section class="middle-content__box bingo-section">
      <?php edit($this->controller,'footer-'.$this->action); ?>
      <?php @$this->repeatData($this->content['footer-'.$this->action],1); ?>
    </section>

  </article>

</div>

<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>