<script type="application/javascript">
//skinModules.push({ id: "BingoRoomDetailsPage", options: { roomId: <?php echo @$this->bingo["Text1"]; ?> } });
</script>
<!-- MAIN CONTENT AREA -->
<div class="main-content bingo-details-page">

  <!-- Title -->
  <!-- <div class="section-left__title">
    <h4><span><?php // echo $this->content["Title"];?></span></h4>
  </div> -->
  
  <div class="middle-content__box">
    <!-- Title -->
    <h1 class="section__title"><?php echo @$this->content["Title"];?></h1>

    <!-- TWO COLUMNS LAYOUT -->
    <div class="two-columns-content-wrap">
      
      
      <!-- LEFT COLUMN -->
      <div class="two-columns-content-wrap__left"  itemscope itemtype="http://schema.org/SoftwareApplication">
        <!-- GAME DETAILS  -->
        <!--
        $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        $$$$ SEO MICRO DATA FOR THIS PARTIAL
        $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        Name: itemprop="name"
        Image: itemprop="image"
        URL: itemprop="url"
        Screenshot: itemprop="screenshot"
        -->
        <!-- WRAP CONTENT LEFT -->
        
        <div class="bingo-detail-content">
          <?php edit($this->controller, "bingo-detail-acquisition-banner",$this->action); ?>
          <div class="bingo-detail-content__img">
            <?php
            @$this->repeatData($this->category["bingo-detail-acquisition-banner"], 1, "_global-library/_editor-partials/banner.php");
            ?>
          </div>
          
          <!-- ACQUISITION CONTENT -->
          <div class="bingo-detail-content__acquisition">
            <div data-bind="visible: !validSession()" class="bingo-detail-content__acquisition__cta">
              <a class="cta-login ui-scroll-top" data-reveal-id="custom-modal">LOGIN</a>
              <a class="cta-join" data-nohijack="true" href="/register/">Join Now!</a>
            </div>
          </div>

          <!-- SEO TEXT -->
          <!-- <div class="bingo-detail-content__seo">
            <?php //edit($this->controller, 'bingo-detail-seo'.$this->action); ?>
            <?php //@$this->repeatData($this->category["bingo-detail-seo".$this->action], 1, "_global-library/_editor-partials/text-only.php"); ?>
          </div> -->

          <!-- BINGO SCHEDULE -->
          <div class="bingo-detail-content__bingo-schedule">
            <?php include "_global-library/partials/bingo/bingo-schedule.php" ?>
          </div>
          <div class="_copy-block__copy-text">
      <?php edit($this->controller,"bingo-copy-text-".$this->action,"","margin-top: -10px"); ?>
      <?php @$this->repeatData($this->category["bingo-copy-text-".$this->action],1); ?>
          </div>
          
          <!-- /GAME DETAILS -->
          <!-- END MIDDLE CONTENT -->
          <!-- BOTTOM CONTENT -->
          <!-- ARTICLE -->
          <div class="games-detail-page__bottom">
            
            <?php echo @$this->content["Body"]; ?>
          </div>
          <!-- BINGO ROOMS -->
          <ul class="games-detail-page__rooms">
            <?php edit($this->controller,'bingo-rooms',$this->action); ?>
            <?php @$this->repeatData($this->category['bingo-rooms'],1,'_partials/bingo-rooms/bingo-room-games.php'); ?>
          </ul>
          <!-- AUTHOR -->
          <div class="_author-block">
            <div class="_author-block__avatar">
              <?php if(exists(@$this->content['Image1'])):?>
              <img width="" src="<?php echo @$this->content['Image1'];?>" alt="<?php echo @$this->content['Text1'];?>">
              <?php endif;?>
            </div>
            <h4>
            <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
              <span itemprop="name"><?php echo @$this->content['Text1'];?></span>
            </span></h4>
            <div class="_author-block__line">
              <div class="_author-block__lineleft"></div>
              <div class="_author-block__lineright"></div>
            </div>
            <div class="_author-block__biography">
              <p></p><p>
              <?php echo @$this->content['Terms'];?>
              </p><p></p>
            </div>
            <ul>
              <a target="_blank" href="<?php echo @$this->content['Text2'];?>" class="_author-block__link">
                <li class="_author-block__social-icons">Author's Social Profile: <img src="/_images/common/social/icon-gl.png"></li>
              </a>
            </ul>
          </div>
          
          <!-- /WRAP CONTENT LEFT -->
        </div>
        <!-- /LEFT COLUMN -->
        <!-- RIGHT COLUMN -->
        <div class="two-columns-content-wrap__right">
          <?php edit($this->controller, 'bingo-detail-right-column', $this->action); ?>
          <?php @$this->repeatData($this->category["bingo-detail-right-column"], 1); ?>
        </div>
        <!-- /RIGHT COLUMN -->
      </div>
    </div>
    <!-- /TWO COLUMNS LAYOUT -->
  </div>
</div>
<!-- /MAIN CONTENT AREA -->
<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>