<script type="application/javascript">
skinModules.push( { id: "ClaimCashback" } );
</script>
<script type="application/javascript">
skinModules.push( { id: "AccountActivity" } );
skinModules.push( { id: "PersonalDetails" } );
skinModules.push( { id: "PlayerAccount" } );
skinModules.push( { id: "ConvertPoints", options: { name: "Loyalty Points", url: "/lucky-club/" } });
skinModules.push( { id: "PlayerReferAFriend" } );
</script>
<!-- MIDDLE CONTENT -->
<div class="main-content " style="display: block;">



  <div class="lobby-wrap myaccount-content-view">  <!-- TITLE -->
  <div class="section-left__title">
    <h4><span class="title"><?php if (config("RealDevice") == 1) { echo str_replace('-', ' ', $this->controller_url); } ?></span></h4>
  </div>
    <!-- left sidebar (tabs-menu)  -->
    <div class="lobby-wrap__left">
      
      <!-- MOBILE TABS MENU -->
      <a href="#" data-dropdown="id01" data-options="is_hover:false" class="button dropdown mobile-tabs-menu__top" style="display:none !important">My Account</a>
      <div id="id01" data-dropdown-content class="f-dropdown mobile-tabs-menu__dropdown">
        <dl class="tabs" data-tab>
          
          <dd class="first active"><a id="ui-my-balances-tab" href="#myaccount-1">Account Balances</a></dd>
          <dd><a id="ui-my-convert-tab" href="#myaccount-2">Convert Points</a></dd>
          <dd><a id="ui-redeem-tab" href="#myaccount-3">Claim Code</a></dd>
          <dd><a id="ui-my-details-tab" data-bind="click: PersonalDetails.getPersonalDetails" href="#myaccount-4">Personal Details</a></dd>
          <dd><a id="ui-activity-tab" href="#myaccount-5">Account Activity</a></dd>
          
        </dl>
      </div>
      <!-- progressive jackpots widget -->
      <div class="mobile-hide-bingo">
        <?php include '_global-library/partials/start/start-left-progressives.php' ?>
      </div>
    </div>
    <!-- right holder tabs-content -->
    <div class="lobby-wrap__right">
      <div class="tabs-content">
        <!-- ACCOUNT BALANCES -->
        <div class="content" id="myaccount-1">
          <?php include '_global-library/partials/my-account/myaccount-balances.php' ?>
          <?php include '_global-library/partials/my-account/myaccount-promotions.php' ?>
        </div>
        <!-- CONVERT POINTS -->
        <div class="content" id="myaccount-2">
          <?php include '_partials/start-skin-only/start-convert-points.php' ?>
        </div>
        <!-- CLAIM CODE -->
        <div class="content" id="myaccount-3">
          <?php // include '_global-library/partials/my-account/myaccount-redeem.php' ?>
          <?php include '_partials/start-skin-only/start-claim-code.php' ?>
        </div>
        <!-- ACCOUNT DETAILS -->
        <div class="content" id="myaccount-4">
          <?php include '_global-library/partials/my-account/myaccount-details.php' ?>
          <?php //include '_partials/start-skin-only/start-myaccount-details.php' ?>
        </div>
        <!-- ACCOUNT ACTIVITY -->
        <div class="content" id="myaccount-5">
          <?php include '_global-library/partials/my-account/myaccount-transactions.php' ?>
        </div>
      </div>
      
    </div>
    <!-- /tabs-content -->
  </div>
  <!-- / TABS HOLDER WRAP -->
</div>
<!-- END MIDDLE CONTENT CENTRE COLUMN -->
<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>