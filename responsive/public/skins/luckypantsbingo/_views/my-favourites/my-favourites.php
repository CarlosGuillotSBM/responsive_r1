<div class="main-content my-favourites">

    <!-- TITLE -->
    <div class="section-left__title">
        <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>

    <!-- MIDDLE CONTENT -->
    <div class="middle-content__box">

        <section class="one-column-content-wrap favourites">
            <!-- Favourites row -->
            <?php include "_global-library/_editor-partials/favourite-game-widget.php" ?>
        </section>

    </div>
    <!-- END MIDDLE CONTENT -->
</div>

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>