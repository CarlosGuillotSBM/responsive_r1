


<div class="main-content lucky-club-content">
  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>

  <div class="section middle-content__box">

    <article class="text-partial">

      <!-- INTRO -->
      <div class="lucky-club__intro">
        <?php edit($this->controller,'lucky-club-intro'); ?>
        <div class="content-template">
          <!-- repeatable content -->
          <?php @$this->repeatData($this->content['lucky-club-intro']);?>
        </div>
      </div>

      <!-- CTA -->
      <div class="lucky-club__cta">
          <!-- Play Now -->
          <a class="button--join" data-hijack="true" data-bind="visible: validSession() , click: playBingo" style="display:none;">Play Bingo</a>
          <!-- Login Register Area OUT-->
          <div class="logged-out-buttons" data-bind="visible: !validSession()" style="display:none;">
            <a class="button--join" data-hijack="true" href="/register/">Join Now</a>
          </div>
      </div>

      <!-- Swipe Left/Right Icon -->
      <div class="lucky-club__table-arrows-icon">
        <img src="/_images/common/icons/touch-icon.png" alt="Left and Right Arrows">
      </div>

      <!-- LUCKY CLUB TABLE WRAP -->
      <div class="lucky-club__table-wrap">


        <div class="lucky-club__table">

          <!-- Touch Icon Inside Table -->
          <!-- <div class="lucky-club__table-touch-icon">
            <img src="/_images/common/icons/touch-icon.png" alt="Touch">
          </div> -->


          <!-- Loyalty Icons Row --> 
          <div class="lucky-club__table-row__header">
            <div class="lucky-club__table-row__title">Benefits to enjoy</div>
            <ul class="lucky-club__table-row__loyalty">
              <li><img src="/_images/common/icons/LP-newbie-icon.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/LP-bronze-icon.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/LP-silver-icon.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/LP-gold-icon.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/LP-ruby-icon.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/LP-emerald-icon.png" alt="Checkmark"></li>
            </ul>
          </div>

          <!-- 1st, 2nd & 3rd Deposit Bonus Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">1st, 2nd & 3rd Deposit Bonus</div>
            <ul class="lucky-club__table-row__loyalty">
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
            </ul>
          </div>

          <!-- Cashback Row --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Cashback</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>12%</li>
              <li>15%</li>
            </ul>
          </div>

          <!-- Bonusback Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Bonusback</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>5%</li>
              <li>-</li>
              <li>-</li>
            </ul>
          </div>

          <!--Upgrade Bonus --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Upgrade Bonus</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>      

          <!-- Daily Free Spins Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Daily Free Spins</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>2</li>
              <li>3</li>
              <li>5</li>
              <li>10</li>
            </ul>
          </div>

          <!-- Daily Free Cards Row --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Daily Free Cards</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>2</li>
              <li>3</li>
              <li>5</li>
              <li>10</li>
            </ul>
          </div>   

          <!-- Withdrawal Pending Period Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Reduced Withdrawal Pending Period</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo Row --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Free Bingo</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo £50 a day Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Free Bingo £50 a day</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo £100 a week Row --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Free Bingo £100 a week</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Free Bingo £500 a month Row --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Free Bingo £500 a month</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Exclusive VIP £500 Cash Bingo Game --> 
          <div class="lucky-club__table-row">
            <div class="lucky-club__table-row__title">Exclusive VIP &pound;500 Cash Bingo Game</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

          <!-- Dedicated VIP manager --> 
          <div class="lucky-club__table-row__even">
            <div class="lucky-club__table-row__title">Dedicated VIP Manager</div>
            <ul class="lucky-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
              <li><img src="/_images/common/icons/checkmark.png"></li>
            </ul>
          </div>

        </div>
      </div>
       <!-- /LUCKY CLUB TABLE WRAP -->

        <!-- LOYALTY POINTS TABLES -->
        <div class="content-template">
        <?php edit($this->controller,'lucky-club-points'); ?>
          <!-- repeatable content -->
          <?php @$this->repeatData($this->content['lucky-club-points']);?>
        </div>

        <!-- Terms &amp; Conditions -->
        <?php edit($this->controller,'lucky-club-tcs'); ?>
        <dl class="lucky-club__terms-conditions accordion" data-accordion="">
          <dd class="accordion-navigation active">
          <a href="#panelTC">Terms &amp; Conditions</a>
          <div id="panelTC" class="content active">
            
            <!-- CONTENT -->
            
            <div class="content-template">
              <!-- repeatable content -->
              <?php @$this->repeatData($this->content['lucky-club-tcs']);?>
            </div>
          </div>
          </dd>
        </dl>
        <!-- / Terms &amp; Conditions -->
      </article>
    </div>
  </div>
<!-- END MIDDLE CONTENT  -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>