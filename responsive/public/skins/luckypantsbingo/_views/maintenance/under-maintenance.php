<!DOCTYPE html>
<html>
<head>
	<title>Under Maintenance</title>
<style type="text/css">
	body{
		background-color:white;
		color:#6b57c8;
		text-align: center;
		font-size: 20px;
		font-family: arial;
	}
	h1{
		font-size: 40px;
		color: #f91a8b;
		margin-bottom: -10px;		
	}
	.logo{
		margin-top: 30px;
	}
	p{
		font-size: 14px;
	}
</style>
</head>
<body>
<img class="logo" src="/_images/logo/logo-sml.png"> 
	<h1>Under Maintenance</h1>
	<p>Sorry, we are offline for just a few minutes. Please come back soon.</p>
</div>
</body>
</html>