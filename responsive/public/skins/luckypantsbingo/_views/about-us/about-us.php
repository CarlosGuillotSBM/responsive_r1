<!-- MIDDLE CONTENT -->
<div class="main-content">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>

  <!-- TWO COLUMNS LAYOUT -->
  <div class="two-columns-content-wrap__left__title">
    
    <!-- LEFT COLUMN -->
    <div class="two-columns-content-wrap__left">
      <!-- CONTENT -->
      <?php edit($this->controller,'about-us'); ?>
      <!-- repeatable content -->
      <?php $this->repeatData(@$this->content['about-us']);?>
    </div>

    <?php if (config("RealDevice") != 3) { ?>

    <!-- RIGHT COLUMN -->
    <div class="two-columns-content-wrap__right">
      <?php edit($this->controller,'about-us-side__content',$this->subaction); ?>
      <?php @$this->repeatData($this->content['about-us-side__content']); ?>
      <div class="clearfix"></div>
    </div>
    <?php } ?>
    
  </div>
  <!-- /TWO COLUMNS LAYOUT -->

</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>