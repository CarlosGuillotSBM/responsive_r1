<script type="application/javascript">
// skinPlugins.push( {id: "rad_dotdotdot"} );
</script>
<!-- single post main content -->
<div class="main-content community">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  
	<!-- content -->
	<div class="middle-content__box">
		<!-- community nav -->
		<?php include "_partials/community-nav/community-nav.php" ?>
		<!-- left column -->
		<div class="two-columns-content-wrap">
			<div class="two-columns-content-wrap__left">
				<!-- top left area -->
				<?php edit($this->controller,'blog'); ?>
				<!-- latest winners slider -->
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Winner Stories</span></h4>
					</div>
					<!-- Slider -->
					<?php include "_partials/community/community-latest-winners-slider.php"; ?>
				</section>
				<!-- blog area -->
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Blog</span></h4>
					</div>
					<?php //@$this->repeatData($this->content['blog']);?>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],1); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],3); ?></div>
				</section>
				
				<!-- chat host area -->
				<section class="community__section community__section__chat-thumbs">
					
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Chat Moderator Bios</span></h4>
					</div>
					
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],1); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],3); ?></div>
				</section>
				<!-- chat news area -->
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Chat News</span></h4>
					</div>
					
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-news'],1); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-news'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['chat-news'],3); ?></div>
				</section>
			</div>
			<!--  /left column -->
			<!-- right column -->
			<div class="two-columns-content-wrap__right ui-scheduled-content-container">
				<?php if (config("RealDevice") != 3) { ?>
				<?php edit($this->controller,'blog-side-content'); ?>
				<!-- repeatable content -->
				<?php @$this->repeatData($this->content['blog-side-content']);?>
				<!-- /repeatable content -->
				
				<?php } ?>
			</div>
			<!-- /right column -->
		</div>
	</div>
</div>
</div>
<!-- /single post main content -->	<!-- /content  -->
<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>