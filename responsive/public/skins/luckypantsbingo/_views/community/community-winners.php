<!-- single post main content -->
<div class="main-content community-blog">

  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
  
	<!-- content -->
	<div class="middle-content__box">			
		<!-- community nav -->
		<?php include "_partials/community-nav/community-nav.php" ?>
		<!-- /community nav -->
		<div class="two-columns-content-wrap middle-content__box">
			<!-- left column -->
			<div class="two-columns-content-wrap__left">
				<!-- latest winners slider -->
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Winner Stories</span></h4>
					</div>
					<!-- Slider -->
					<?php include "_partials/community/community-latest-winners-slider.php"; ?>
				</section>
				<!-- latest winners area -->
				<section class="community__section">
					<!-- Title -->
					<div class="section-left__title__community">
						<h4><span>Winner Stories</span></h4>
					</div>
					<?php edit($this->controller,$this->action); ?>
					<!-- repeatable content -->
					<?php @$this->repeatData($this->content[$this->action]);?>
					<!-- /repeatable content -->
				</section>
			</div>
			<!--  /left column -->
			<!-- right column -->
			<div class="two-columns-content-wrap__right">
				<?php if (config("RealDevice") != 3) { ?>
				<?php edit($this->controller,'blog-side-content'); ?>
				<!-- repeatable content -->
				<?php @$this->repeatData($this->content['blog-side-content']);?>
				<!-- /repeatable content -->
				
				<?php } ?>
			</div>
			<!-- /right column -->
		</div>
		<!-- /content  -->
	</div>
</div>
</div>
<!-- /single post main content -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
<?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>