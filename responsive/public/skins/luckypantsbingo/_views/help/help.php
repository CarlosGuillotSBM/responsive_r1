<!-- MIDDLE CONTENT -->
<div class="main-content help-content">
  <!-- Title -->
  <div class="section-left__title">
    <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
  </div>
    
    <!-- LEFT COLUMN -->
    <div class="two-columns-content-wrap__left">
      <!-- CONTENT -->
      <?php edit($this->controller,'support'); ?>
      <div class="content-template">
        <?php @$this->repeatData($this->content['support']);?>
      </div>
      <!-- /TITLE WRAP -->
      <!-- CONTENT WRAP -->
      <section class="text-partial">
        <div class="text-partial-content">
          <!-- CONTENT WRAP TOP -->
          <section class="help-content-top">
            
            <!-- EMAIL -->
            <div class="help-content__section">
              <img src="/_images/common/support/email.svg">
              <div class="help-content__info">
                <h1>Email</h1>
                <a target="_blank" data-hijack="false" href="mailto:support@luckypantsbingo.com">support@luckypantsbingo.com<em>(for all account-related queries)</em></a>
                <a htarget="_blank" data-hijack="false" href="mailto:promotions@luckypantsbingo.com">promotions@luckypantsbingo.com<em>(for all promotion-related queries)</em></a>
                <a target="_blank" data-hijack="false" href="mailto:vip@luckypantsbingo.com">vip@luckypantsbingo.com<em>(for all VIP-related queries)</em></a>
                <p>Please note that emails will be responded to within 24 hours after receipt</p>
              </div>
            </div>
            <!-- PHONE NUMBERS -->
            <div class="help-content__section">
              <img src="/_images/common/support/phone.svg">
              <div class="help-content__info">
                <h1>Phone Numbers</h1>
                <h2>United Kingdom</h2>
                <span itemprop="telephone"><a target="_blank" data-hijack="false" href="tel:08082386070">0808 238 6070 (Freephone)</a></span>
                <span itemprop="telephone"><a target="_blank" data-hijack="false" href="tel:02035519705">0203 551 9705 (Landline)</a></span>
                <p>Please note that inbound and outbound calls may be recorded for security and training purposes.</p>
              </div>
            </div>
            <!-- LIVE CHAT -->
            <div class="help-content__section">
              <img src="/_images/common/support/live-chat.svg">
              <div class="help-content__info">
                <h1>Live Chat</h1>
                <a href="#" data-hijack="false" target="_blank" onclick="javascript:void window.open('<?php echo config('LiveChatURL'); ?>','1323353895481','width=590,height=480,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=1,left=0,top=0');return false;">
                  <span>24h live chat link</span>
                </a>
              </div>
            </div>
          </section>
          <!-- CONTENT WRAP BOTTOM -->
          <!-- <section class="help-content-bottom"> -->
            <!-- CORPORATE CONTACT -->
<!--             <div class="help-content__section">
              <img src="/_images/common/support/corporate-chat.svg">
              <div class="help-content__info">
                <h1>Corporate Contact</h1>
                <a target="_blank" data-hijack="false" href="http://www.stridegaming.com/contact-us/">
                  <span>Corporate & press enquiries</span>
                </a>
              </div>
            </div>
          </section> -->
          <!-- CONTENT WRAP PASSWORD -->
          <section class="help-content-password"  style="display: none; cursor: pointer"  data-bind="visible: !validSession()" >
            <div class="help-content__section">
              <img src="/_images/common/support/lock.svg">
              <!-- COLLAPSE
              http://foundation.zurb.com/sites/docs/v/5.5.3/components/accordion.html
              -->
              <ul class="accordion help-content__info">
                <li class="accordion-navigation">
                  <a data-hijack="false" class="ui-accordion">
                    <h1>Forgot your Password?</h1>
                    <img src="/_images/common/accordion-down-arrow.svg">
                  </a>
                  <div class="content" style="display: none">
                    <!--  <span>Enter your email address and we will send you a password reset email.</span> -->
                    <?php include '_partials/common/forgot-password.php'; ?>
                  </div>
                </li>
              </ul>
            </div>
          </section>
        </div>
      </section>
      <!-- / CONTENT WRAP -->
      <!-- repeatable content -->
     <?php // $this->repeatData($this->content['about-us']);?>
    </div>

</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>
