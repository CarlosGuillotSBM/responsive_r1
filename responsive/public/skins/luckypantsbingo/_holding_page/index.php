<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <title>BINGO EXTRA - More Than Just Bingo</title>

  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="shortcut icon" href="_holding_page/images/landing-responsive-template/favicon.png">
  <link rel="apple-touch-icon" sizes="57x57" href="_holding_page/images/landing-responsive-template/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="_holding_page/images/landing-responsive-template/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="_holding_page/images/landing-responsive-template/apple-touch-icon-114x114.png">

  <link rel="stylesheet" href="_holding_page/css/landing-responsive-template/style.css">
  <!--[if lt IE 9]>
    <script src="js/shiv.js"></script>
  <![endif]-->

</head>
<body>

<!-- Header section -->

<!-- logo -->
<div class="section row row-one">
  <div class="col three tablet-five mobile-eight mobile-center ">
    <img src="_holding_page/images/logo.png" width="100%">
  </div>

<!-- member text -->
<!--    <div class="col nine right tablet-full mobile-full memberText center-text">
    <h4>Already registered? <a href="">Login</a></h4>
  </div>
</div> -->
<!-- end header -->



<!-- main landing graphic -->
<div class="section row row-one">
  <div class="col twelve tablet-twelve mobile-full">
    <img src="_holding_page/images/coming-soon.png" width="100%" >
  </div>
</div>
<!-- end main landing graphic -->

<!-- footer section -->

<!-- device image section -->
<div class="section row row-three">
  <div class="col six mobile-full tablet-full center-text ">
  <img src="_holding_page/images/responsive-landing-assets/devices.png"  class="mobile compatible">
 </div>
   <div class="col six mobile-full tablet-full center-text ">
  <img src="_holding_page/images/responsive-landing-assets/cards.png" class="cards">
 </div>
</div>

<!-- end desktop cards section -->


<!-- terms section -->
<div class="section row row-one">
  <div class="col twelve center-text ">
<!--   <p>All Financial Transactions processed by WorldPay © Lucky Pants Bingo 2013. All rights reserved.</p>
<p>Licensed by the Alderney Gambling Control Commission, License Number: 71 C1. Lucky Pants Bingo is licensed and regulated to offer online gaming services under the laws of Alderney through Daub Alderney Limited, which makes no representation as to legality of such services in other jurisdictions.</p><p> Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3 UL</p> -->
 </div>
</div>

<!-- end footer section -->


















<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="_holding_page/js/landing-responsive-template/default.js"></script>



<script src="_holding_page/js/landing-responsive-template/backstretch.min.js"></script>
<script type="text/javascript">
  $.backstretch("_holding_page/images/bg.png")
</script>

</body>
</html>
