<?php

// old luckypants affiliate re-directs
$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';

if(preg_match("[^double-slots-bonus$|^no-deposit-required$|^30-free-as-seen-on-tv$|^play-with-40-as-seen-on-tv$|^play-with-40-as-seen-on-tv$|^30-free-plus-15-free-spins$|^300-bonus$|^free-bonus-rain$]", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/affiliates/" . $path. $queryString);
    exit;
}

if(preg_match("[^lucky-scratch$|^tv-ad$|^free-spins$|^summer-offer$]", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /media/exclusive/" . $path. $queryString);
    exit;
}

if(preg_match("#^\/general-promotional-terms-and-conditions\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /terms-and-conditions/" . $path . $queryString);

    exit;
}

if(preg_match("#^\/giveaway\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/great-british-giveaway/". $queryString);
    exit;
}

if(preg_match("/\/bingo-schedule\/$/", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);
    exit;
}

if(preg_match("#^\/50k\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/50k/". $queryString);
    exit;
}

// DEV-10368

if (preg_match("#^\/free\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/free/". $queryString);
    exit;
}

//DEV-10401


if (preg_match("#^\/claim\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/claim/". $queryString);
    exit;
}

if (preg_match("#^\/play\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/play/". $queryString);
    exit;
}

if (preg_match("#^\/spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/spins/". $queryString);
    exit;
}

// 10983
if (preg_match("#^\/advent-1\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-1/". $queryString);
    exit;
}
if (preg_match("#^\/advent-4\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-4/". $queryString);
    exit;
}
if (preg_match("#^\/advent-6\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-6/". $queryString);
    exit;
}
if (preg_match("#^\/advent-8\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-8/". $queryString);
    exit;
}
if (preg_match("#^\/advent-12\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-12/". $queryString);
    exit;
}
if (preg_match("#^\/advent-13\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-13/". $queryString);
    exit;
}
if (preg_match("#^\/advent-15\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-15/". $queryString);
    exit;
}
if (preg_match("#^\/advent-17\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-17/". $queryString);
    exit;
}
if (preg_match("#^\/advent-18\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-18/". $queryString);
    exit;
}
if (preg_match("#^\/advent-22\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-22/". $queryString);
    exit;
}
if (preg_match("#^\/advent-25\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-25/". $queryString);
    exit;
}

//11246
if (preg_match("#^\/victorious-secret\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/victorious-secret/". $queryString);
    exit;
}

if(preg_match("#^\/media/exclusive/deposit-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

if(preg_match("#^\/media/ppc/deposit-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

if(preg_match("#^\/media/exclusive/first-deposit-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

if(preg_match("#^\/media/ppc/first-deposit-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

if(preg_match("#^\/media/ppc/welcome-offer\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

//DEV-11792 

if(preg_match("#^\/media/affiliates/registration-offer\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/affiliates/exclusive-offer/' );
    exit;
}

if(preg_match("#^\/media/affiliates/double-slots-bonus\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/affiliates/slots-deposit-bonus/' );
    exit;
}

if(preg_match("#^\/media/affiliates/no-deposit-required\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/affiliates/exclusive-offer/' );
    exit;
}

if(preg_match("#^\/media/affiliates/30-free-as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/affiliates/bingo-deposit-bonus-amp/' );
    exit;
}

if(preg_match("#^\/media/affiliates/play-with-40-as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/affiliates/bingo-deposit-bonus-amp/' );
    exit;
}

if(preg_match("#^\/media/affiliates/30-free-plus-15-free-spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/affiliates/bingo-deposit-bonus-amp/' );
    exit;
}

if(preg_match("#^\/media/affiliates/free-bonus-rain\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header('Location: /media/affiliates/exclusive-offer/' );
    exit;
}

//R10-15 
if (preg_match("#^\/boom\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/boomerang/". $queryString);
    exit;
}


//R10-204
if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/luckypantsbingo/". $queryString);
    exit;
}

//R10-523
if (preg_match("#^\/vipbucks\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/vipbucks/". $queryString);
    exit;
}

//R10-762
if (preg_match("#^\/slots/game/temple-of-iris-jackpot\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/game/temple-of-iris-progressive/". $queryString);
    exit;
}

//R10-957
if (preg_match("#^\/air\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/air/". $queryString);
    exit;
}

if (preg_match("#^\/scratch\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/your-pants/". $queryString);
    exit;
}
//R10-1009
if (preg_match("#^\/excluded\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/excluded/". $queryString);
    exit;
}

//R10-1251
if (preg_match("#^\/deal-no-deal\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/deal-no-deal/". $queryString);
    exit;
}