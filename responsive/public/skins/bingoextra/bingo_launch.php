<?php
/*
$alias = @$_REQUEST["Alias"];
$room = @$_REQUEST["RoomName"];
$playerId = @$_REQUEST["PlayerID"];
$sessionId = @$_REQUEST["SessionID"];
*/

$route = dirname(dirname(dirname(dirname(__FILE__)))).'/application/';

require $route.'config/config.php';

$roomParameter = (!isset($_REQUEST["RoomName"]) || $_REQUEST["RoomName"] == "Lobby") ? "Room=Lobby" : "RoomName=".$_REQUEST["RoomName"];
$room = @$_REQUEST["RoomName"];
$playerId = (!isset($_REQUEST["PlayerID"])) ? @$_COOKIE["PlayerID"] : $_REQUEST["PlayerID"];
$alias = (!isset($_REQUEST["Alias"])) ? @$_COOKIE["username"] : $_REQUEST["Alias"];
$sessionId = (!isset($_REQUEST["SessionID"])) ? @$_COOKIE["SessionID"] : $_REQUEST["SessionID"]; 


if (!isset($_REQUEST["frame"])) {
	$params = "?$roomParameter&PlayerID=$playerId&SessionID=$sessionId&Alias=$alias";
	header("Location: /game-frame/".$params);
}

$file = substr(__FILE__,7);
$glo = array ("Application" => "agsSkins");
$module = substr($file,strrpos($file,"\\")+1);         //this php file
require_once("D:/WEB/inc/vars.php"); //global variables
require_once("D:/WEB/inc/genFuncs.php");   //general functions
require_once "D:\\WEB\\inc\\NewTrackerScripts\\".$glo["Skin"]["ID"].".php";
// Getting request

global $glo;

$globalLiveHelp = $glo["Skin"]["LiveHelp"];
$globalLiveHelpTrigger = $glo["Skin"]["LiveHelpTrigger"];
/*
$roomParameter = (empty($_REQUEST["RoomName"])) ? "Room=Lobby" : "RoomName=".$_REQUEST["RoomName"];
$room = @$_REQUEST["RoomName"];
$playerId = (empty($_REQUEST["PlayerID"])) ? $_COOKIE["PlayerID"] : $_REQUEST["PlayerID"];
$alias = (empty($_REQUEST["Alias"])) ? $_COOKIE["username"] : $_REQUEST["Alias"];
$sessionId = (empty($_REQUEST["SessionID"])) ? $_COOKIE["SessionID"] : $_REQUEST["SessionID"]; 
*/


if(config("bingoHtml5Only") || $glo["AGS"]["Device"] == "Tablet" || $glo["AGS"]["Device"] == "Phone")
{
	switch(strtolower($_SERVER["SERVER_NAME"])) {
        case 'responsive.bingoextra.dev':
        case 'bingoextra.dagacubedev.net':
            header("location: /html5/bingo/public/bingoextra/index.php?". $roomParameter .
                   "&PlayerID=" . $playerId . "&SessionID=" . $sessionId . "&Alias=" . $alias . "&globalLiveHelp=" . urlencode($globalLiveHelp) . "&globalLiveHelpTrigger=" . urlencode($globalLiveHelpTrigger));
            break;
        case 'www.bingoextra.com':
            header("location: /html5/bingo/public/bingoextra/index.php?". $roomParameter .
                "&PlayerID=" . $playerId . "&SessionID=" . $sessionId . "&Alias=" . $alias . "&globalLiveHelp=" . urlencode($globalLiveHelp) . "&globalLiveHelpTrigger=" . urlencode($globalLiveHelpTrigger));
            break;
        default:
            exit("Sorry there is a system error, if the error persists contact support.");
            break;
    }

    exit;
}

if (in_array($glo["Computer"],$glo["DevComputers"]))
{
	$serverName = "https://".$_SERVER["SERVER_NAME"];
	$bingoPath = "https://flash.dagacubedev.net/bingo/extra";
}
else
{
	$serverName = "https://".$_SERVER["SERVER_NAME"];
	if(@$_SERVER["SERVER_PORT"] == 8433)
	{
		$bingoPath = "https://gameclient.dagacube.net:8433/bingo/extra";
	}
	else
	{
		$bingoPath = "https://gameclient.dagacube.net/bingo/extra";
	}
}


?>

<HTML>
<HEAD>
    <TITLE>Bingo Extra!</TITLE>
    <STYLE>
        BODY{font-family:Verdana,Helvetica,Sans Serif;font-size:10pt;background-color: #ffffff;margin:0px 0px 0px 0px;}
        FORM{margin:0px 0px 0px 0px;}
    </STYLE>

</HEAD>
<body onLoad = "getAppFocus();">
<?php SendTrackersTopOfPage();?>
	<script type="text/javascript">
	var skin_id = "<?php echo $glo['Skin']['ID'];?>";
	var cashier_url = "<?php echo $glo['AGS']['CashierURL'];?>";
	var oid = "1";
	var kid = "<?php echo $glo['Skin']['ID'];?>";
	var version = "1";
	var dagacube_url = '<?php echo $glo["AGS"]["GameURL"]; ?>';
	var servername = '<?php echo $serverName;?>';
	var globalKey = '<?php echo $glo["Skin"]["Key"];?>';
	var globalVersion = '<?php echo $glo["Version"];?>';
	var globalSkinID = '<?php echo $glo["Skin"]["ID"];?>';
	var globalPlayerIP = '<?php echo $glo["IPAddress"];?>';
	var globalCountryIPID = '<?php echo $glo["CountryIPID"];?>';
	var globalWexaUrl = '<?php echo $glo["AGS"]["WEXAURL"];?>';
	var sbm = {
		serverconfig : {}
	};

	var globalLiveHelp = '<?php echo $globalLiveHelp;?>';
	var globalLiveHelpTrigger = '<?php echo $globalLiveHelpTrigger;?>';


	function depositSuccess(Username,PlayerID,Amount)
	{
		document.getElementById("amount").value = Amount;
		document.getElementById("ftd").value = 0;
		document.getElementById("DepositForm").submit();
		return;
	}

	</script>
	<script type="text/javascript" src="/_global-library/js/build/sbm.vendor.min.js"></script>

	<script type="text/javascript" src="/_global-library/js/vendor/swfobject.js"></script>
	<script type="text/javascript" src="/_global-library/js/vendor/game.js"></script>

	 <div id="cashiercontainer" style="margin:0px;padding:0px;display:none"></div>

<div id="flashcontent">
	<iframe id="DepSuccess" name="DepSuccess" width="100%" height="100%" frameborder="0" scrolling="no" style="display:block;" src="flash-msg-be.html"></iframe>
</div>

<script type="text/javascript">
    var so = new SWFObject("<?php echo $bingoPath;?>/extra_bingo.swf?cache=<?php echo time();?>", "bingoextra", "100%", "100%", "10", "#fbdc9c");

    so.addVariable("playerID", "<?php echo $playerId; ?>");
    so.addVariable("username", "<?php echo $alias; ?>");
    so.addVariable("sessionID", "<?php echo $sessionId; ?>");
    so.addVariable("country", "<?php echo (isset($_COOKIE["CountryCode"])) ? $_COOKIE["CountryCode"] : "GBR";?>");
    so.addVariable("skinID", "<?php echo $glo['Skin']['ID'];?>");
    so.addVariable("IP", "<?php echo $_SERVER["REMOTE_ADDR"];?>");
    so.addVariable("RoomID", "<?php echo $room; ?>" );
    so.addVariable("NewbieRoom", "Room_120" );
    <?php if (isset($_REQUEST["GroupID"])){?>
    so.addVariable("GroupID", "<?php echo $_REQUEST["GroupID"];?>");
    <?php }?>

    so.addVariable("Device", "<?php echo @$_COOKIE["Device"]; ?>" );
    <?php if (isset($_GET["avatarOption"])){?>
    so.addVariable("OpenAvatarOptions", "<?php echo $_GET["avatarOption"] ?>" );
    <?php }?>
    so.addParam("base", "<?php echo $bingoPath;?>/");
    so.addParam("allowScriptAccess", "always");
    so.addParam("wmode", "window");
    so.addParam("allowfullscreen", "false");
    so.addParam("allownetworking", "all");
    so.write("flashcontent");
</script>
	<form id="DepositForm" method="POST" style="display:none" action="/deposit-thankyou" target="DepSuccess">
		<input type="hidden" id="amount" name="amount">
		<input type="hidden" id="ftd" name="ftd">
	</form>
	<iframe id="DepSuccess" name="DepSuccess" width="0" height="0" frameborder="0" scrolling="no" style="display:none;"></iframe>
	<?php SendTrackersBottomOfPage(); ?>
	</body>
</body>
</html>
