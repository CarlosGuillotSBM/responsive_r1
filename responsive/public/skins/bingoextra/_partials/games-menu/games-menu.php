<script type="application/javascript">
	skinPlugins.push({
		id: "magicalMenu",
		options: {
			global: true
		}
	});
</script>
<div class="games-menu custom-hide-for-mobile">
	<div class="games-menu__container">
		<!-- games nav -->
		<ul class="games-menu__games-nav">
			<li id="allslots"><a href="/slots/" data-hijack="true"><img src="/_images/common/icons/icon-slots.png">ALL GAMES</a></li>
		</li>
		<li id="table-card"><a href="/table-card/" data-hijack="true"><img src="/_images/common/icons/table-cards--game-icon.png">TABLE &amp; CARD GAMES</a>
		<!-- <li id="roulette"><a href="/roulette/" data-hijack="true"><img src="/_images/common/icons/roulette-icon.png">ROULETTE</a></li> -->
		<li id="scratchcards" class="custom-hide-for-mobile"><a href="/scratch-and-arcade/" data-hijack="true"><img src="/_images/common/icons/scratch-cards-icon.png">SCRATCH &amp; ARCADE</a></li>
		<li id="magicalMenu" class="games-filter-menu"><a id="magicalMenuBtnLnk"  data-hijack="false">SLOTS FILTER</a>
		<?php include "_partials/games-menu/magic-menu-popover.php"; ?>
	</li>
	<li class="games-menu__games-nav__search "><?php include '_global-library/partials/search/search-input.php'; ?></li>
</ul>
<!-- /games nav -->
</div>
</div>