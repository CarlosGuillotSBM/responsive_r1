<script type="application/javascript">
skinModules.push({
     id: "LoginBox",
     options: {
          modalSelectorWithoutDetails: "#custom-modal",
          global: true
     }
});
</script>
<article class="login-box">
     <h1>Player login</h1>

     <div class="login-box__form-wrapper">

          <form class="" id="ui-login-form">

               <div class="login-box__input-wrapper">
                    <input type="text" placeholder="User Name" data-bind="value: LoginBox.username, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}" />
                    <span style="display: none" data-bind="visible: LoginBox.invalidUsername" class="error">This field is required.</span>
               </div>
               <div class="login-box__input-wrapper">
                    <input type="password" placeholder="Password" data-bind="attr:{type: LoginBox.passwordShowText() == 'Hide' ? 'text': 'password'}, value: LoginBox.password, valueUpdate: ['input', 'keypress'], event:{keypress: LoginBox.checkForEnterKey}"/>
                    <span style="display: none" data-bind="visible: LoginBox.invalidPassword" class="error">This field is required.</span>
                    <div class="login-box__show-password">
                         <a data-bind="click: LoginBox.togglePassword, text: LoginBox.passwordShowText" class="right">Show</a>
                    </div>
               </div>
               <a style="display: none" data-bind="visible: LoginBox.lockedUser, text: LoginBox.loginError" class="error" href="/help/"></a>
	<!-- ko ifnot: LoginBox.lockedUser -->
               <span style="display: none" data-bind="visible: LoginBox.loginError, html: LoginBox.loginError" class="error"></span>
	<!-- /ko -->
               <a class="cta-login" data-bind="click: LoginBox.doLogin.bind($data, GameLauncher.gameId(), GameLauncher.gamePosition(), GameLauncher.gameContainerKey(), GameLauncher.roomId())">Login</a>
               <a class="registercta" href="/register/" data-hijack="true" >Join Now</a>

               <a href="/forgot-password/" class="login-box__forgot-password">Forgot Password?</a>

          </form>
     </div>
</article>
