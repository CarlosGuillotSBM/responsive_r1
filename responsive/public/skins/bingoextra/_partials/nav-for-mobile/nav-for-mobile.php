<!-- Mobile Top Bar -->
<div class="tab-bar nav-for-mobile">
	<!-- COOKIE POLICY -->
	<?php include "_global-library/partials/modals/cookie-policy.php" ?>


	<!-- logged in -->
	<div style="display: none" data-bind="visible: validSession" class="small--balances">
		<!-- <a class="right-off-canvas-toggle icons__balance-white"></a> -->
		<a class="icons__deposit menu-link" data-hijack="true" href="/cashier/" data-gtag="Mobile Menu Cashier Click"></a>
	</div>
	<!--/ logged in -->
	<h1 class="seo-logo">
		<a data-hijack="true" href="/" class="site-logo ui-go-home-logo menu-link">
			<img src="/_images/common/logo-horitzontal.svg"/>
		</a>
		<span style="display: none;">Bingo Extra the best online bingo sites on mobile and desktop.</span>
	</h1>
<!-- Start of navigation toggle -->
<div class="menu-toggle-wrap">
  <div class="burger">
    <div class="span"></div>
  </div>
</div>

<!-- logged out -->
<a style="display: none" data-bind="visible: !validSession()" class="joinnow-button--nav-for-mobile menu-link" href="/register/">JOIN NOW</a>
<a style="display: none" data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login-button--nav-for-mobile menu-link" href="/login/">LOGIN</a>


<!-- /logged out -->
  </div>
  <!-- End of navigation toggle -->


<!-- New Mobile Menu April 2015 Author: Marcus Chadwick -->
<nav class="mobile-menu" style="opacity: 1">
	<div class="mobile-menu-top-area">
		<div class="mobile-menu-top-area__row">
			<div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">

<!-- <div class="inner">
<span >BINGO EXTRA MENU</span>
</div> -->
			</div>

			<!-- Logged in Balance Box -->
			<div style="display: none" class="mobile-menu-top-area__balance" data-bind="visible: validSession()">
				<a href="/my-account/"><div class="inner">
					Balance<span data-bind="text: balance">£00000.00</span>
				</div></a>
			</div>
			<!-- Logged in VIP Level Box -->
			<div style="display: none" class="mobile-menu-top-area__vip-level" data-bind="visible: validSession()">

				<div class="vip-level-area">
					<a href="/vip/"><span class="vip-shape" data-bind="text: vipLevel(), css: 'level-' + playerClass()"></span><span class="vip-level-area-icon"></span></a>
				</div>

			</div>
		</div>
<!-- 		<div class="mobile-menu-top-area__row">
			<?php// include "_global-library/partials/search/search-input.php" ?>
		</div> -->
	</div>
	<div class="mobile-menu-lists ripple">
		<!-- menu lists -->
		<ul class="mobile-menu-list">
			<!-- section title -->
			<li class="mobile-menu-section-title">Bingo</li>
				<li class="mobile-menu-item bingo-link">
				<a class="menu-link" data-hijack="true" href="/bingo-schedule/" title="New Games">Bingo</a>
			</li>
			<!-- section title -->
			<li class="mobile-menu-section-title">Games</li>
			<!-- menu items-->
			<!-- commented out search link while feature is finalized -->
				<li class="mobile-menu-item gamesearch-link">
				<a id="ui-search-screen" class="menu-link"  title="New Games">Search Games</a>
			</li>
			<li class="mobile-menu-item newgames-link">
				<a class="menu-link" data-hijack="true" href="/slots/new/" title="New Games">New Games</a>
			</li>
			<li class="mobile-menu-item favourites-link" data-bind="visible: validSession()">
				<a class="menu-link" data-hijack="true" href="/my-favourites/" title="Favourite Games">My Favourites</a>
			</li>
			<!-- Slots Categories Hidden Slide Down Panel -->
			<li class="mobile-menu-item slots-link has-submenu">
				<a id="ui-slots-menu-link" class="menu-link">Slots</a>

				<span class="mobile-menu-item__submenu-toggle" data-submenu="slots">
					Categories
					<svg class="chevron-icon" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
						<path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#ffffff">
						</path>
					</svg>
				</span>

				<div class="mobile-menu-item__submenu">
					<span class="nub"></span>
					<!-- include game categories-->
					<div class="mobile-menu-item__submenu__content">
						<!-- Slots Categories Hidden Slide Down Panel -->
						<div class="games-filter-menu__popover-wrap">
							<div class="games-filter-menu__popover game-filter-menu" align="center">
								<span class="nub"></span>
								<!-- include game categories-->
								<?php include "_global-library/partials/game-filter-menu/game-filter-categories.php" ?>
							</div>
						</div>
					</div>
				</div>
			</li>

			<li class="mobile-menu-item scratchcards-link">
				<a class="menu-link" data-hijack="true" href="/scratch-and-arcade/" title="Scratchcards">SCRATCH &amp; ARCADE</a>
			</li>

			<li class="mobile-menu-item tablecard-link">
				<a class="menu-link" data-hijack="true" href="/table-card/" title="Table & Card">Table &amp; Card</a>
			</li>

			<li class="mobile-menu-item allgames-link">
				<a class="menu-link" data-hijack="true" href="/games/" title="All Games">All Games</a>
			</li>
			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title">Rewards</li>
			<!-- menu items-->
			<li class="mobile-menu-item promotions-link">
				<a class="menu-link" data-hijack="true" href="/promotions/" title="Promotions">Promotions</a>
			</li>
			<li class="mobile-menu-item loyalty-link">
				<a class="menu-link" data-hijack="true" href="/extra-club/" title="Extra Club">Extra Club</a>
			</li>
			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title" data-bind="visible: validSession()">Account</li>
			<!-- menu items-->
			<li class="mobile-menu-item myaccount-link" data-bind="visible: validSession()">
				<a class="menu-link" data-hijack="true" href="/my-account/" title="My Account">My Account</a>
			</li>
			<li class="mobile-menu-item cashier-link" data-bind="visible: validSession()">
				<a class="menu-link" data-hijack="true" href="/cashier/" title="Cashier">Cashier</a>
			</li>
			<!-- /menu items-->
			<!-- section title -->
			<li class="mobile-menu-section-title">About</li>
			<!-- menu items-->
			<li class="mobile-menu-item community-link">
				<a class="menu-link" data-hijack="true" href="/community/" title="Community">Community</a>
			</li>
			<li class="mobile-menu-item support-link">
				<a class="menu-link" data-hijack="true" href="/support/" title="Support">Support</a>
			</li>
			<li class="mobile-menu-item faq-link">
				<a class="menu-link" data-hijack="true" href="/faq/" title="Faq">Faq</a>
			</li>
			<li class="mobile-menu-item terms-link">
				<a class="menu-link" data-hijack="true" href="/terms-and-conditions/" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
			</li>
			<li class="mobile-menu-item about-us-link">
				<a class="menu-link" data-hijack="true" href="/about-us/" title="About Us">About Us</a>
			</li>
			<li class="mobile-menu-item privacy-link">
				<a class="menu-link" data-hijack="true" href="/privacy-policy/" title="Privacy Policy">Privacy Policy</a>
			</li>
			<li class="mobile-menu-item settings-link" data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none">
				<a class="menu-link" data-hijack="true" href="/cashier/?settings=1" title="Player Settings">Player Settings</a>
			</li>
			<li class="mobile-menu-item responsiblegaming-link">
				<a class="menu-link" data-hijack="true" href="/responsible-gaming/" title="Responsible Gaming">Responsible Gaming</a>
			</li>
			<li class="mobile-menu-item complaints-disputes-link">
				<a class="menu-link" data-hijack="true" href="/complaints-and-disputes/" title="Complaints and Disputes/">Complaints and Disputes</a>
			</li>
			<li class="mobile-menu-item archive-link">
				<a class="menu-link" data-hijack="true" href="/archive/" title="Archive/">Archive</a>
			</li>

			<!-- <li class="mobile-menu-item gamelimits-link">
				<a class="menu-link" data-hijack="true" href="/game-limits/" title="Game Limits">Game Limits</a>
			</li>-->
			
			<li class="mobile-menu-item logout-link" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">
				<a class="menu-link" data-hijack="true" title="Log Out" style="display: none" data-bind="visible: validSession, click: logout" data-gtag="Logout Mobile">Log Out</a>
			</li>
			<li class="mobile-menu-item">
				<!-- spacer to move up logout so its not touching bottom -->
			</li>
			<!-- /menu items-->
		</ul>
	</div>
	<!-- / menu lists -->
</nav>

<!-- END New Mobile Menu April 2015 Author: Marcus Chadwick -->
