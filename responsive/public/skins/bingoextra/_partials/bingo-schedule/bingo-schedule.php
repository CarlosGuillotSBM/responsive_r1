<script type="application/javascript">
    skinModules.push({id: "BingoScheduler"});
</script>


<div class="bingo-schedule">
    <!-- Bingo Category Tabs -->
    <div class="bingo-schedule__tabs">
        <dl class="tabs" data-tab role="tablist">
            <dd class="active"><a href="#bingo-schedule-cat1">All</a></dd>
            <dd><a href="#bingo-schedule-cat2">Bingo 90</a></dd>
            <dd><a href="#bingo-schedule-cat3">Bingo 75</a></dd>
            <dd><a href="#bingo-schedule-cat4">Bingo 5L</a></dd>
            <dd><a href="#bingo-schedule-cat5">Prebuys</a></dd>
        </dl>
    </div>

    <!-- End Bingo Category Tabs -->
    <ul class="bingo-schedule__header">
        <li class="bingo-schedule__header__title room-name-col">Room</li>
<!--        <li class="bingo-schedule__header__title feature-col">Feature</li>-->
        <li class="bingo-schedule__header__title jackpot-col">Jackpot</li>
        <li class="bingo-schedule__header__title price-col">Price</li>
        <li class="bingo-schedule__header__title starts-col">Starts In</li>
        <li class="bingo-schedule__header__title play-col"></li>
    </ul>

    <div class="bingo-schedule__rows">

        <!--  Bingo Schedule Single Row -->

        <div class="tabs-content">

            <!-- ALL -->
            <div style="display: none" data-bind="foreach: BingoScheduler.openedRooms, visible: BingoScheduler.openedRooms().length > 0"
                class="content active" id="bingo-schedule-cat1" role="tabpanel">

          

              <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

      

            </div>

            <!-- 90 -->
            <div data-bind="foreach: BingoScheduler.rooms90"
                 class="content" id="bingo-schedule-cat2" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>

            <!-- 75 -->
            <div data-bind="foreach: BingoScheduler.rooms75"
                 class="content" id="bingo-schedule-cat3" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>


            <!-- 5L -->
            <div data-bind="foreach: BingoScheduler.rooms5L"
                 class="content" id="bingo-schedule-cat4" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>


            <!-- PRE -->
            <div data-bind="foreach: BingoScheduler.roomsPre"
                 class="content" id="bingo-schedule-cat5" role="tabpanel">

                <?php include "_global-library/partials/bingo/bingo-room.php"; ?>

            </div>


        </div>
    </div>
    <div class="clearfix"></div>

</div>