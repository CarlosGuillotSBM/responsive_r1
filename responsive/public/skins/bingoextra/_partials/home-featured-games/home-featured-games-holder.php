<div class="content-holder home-featured-games-holder">
	<!-- content holder title -->
	<!-- content holder content area -->
	<div class="content-holder__content ">
		<!-- content holder content full width inner holder-->
		<!-- <div class="content-holder__content_full-width"></div> -->
		
		<!-- content holder content left inner holder-->
		<div class="content-holder__content_left">
			<div class="content-holder__title">
				<h1>FEATURED GAMES</h1>
				<!-- content holder title right link -->
				<span class="content-holder__title__link"><a href="/games/">View All &raquo;</a></span>
			</div>
			
			<div class="content-holder__inner">
				<?php include "_partials/home-featured-games/home-featured-games-inner.php"; ?>
			</div>
			
		</div>
		<!-- content holder content right inner holder-->
		<div class="content-holder__content_right ">
			<div class="content-holder__title">
				<h1>PROGRESSIVE JACKPOTS</h1>
				<!-- content holder title right link -->
				<?php
					$titleHeaderHeight = 25;
					$slideHeight = 95;
					$slidesToShow = 3;
				?>
				<script type="application/javascript">
					skinPlugins.push({
						id: "ProgressiveSlider",
						options: {
							slidesToShow: <?php echo $slidesToShow ?>,
							mode: "vertical"
						}
					});
				</script>
			</div>
			<div class="content-holder__inner ">
				
				<?php edit($this->controller,'home-progressives'); ?>
				<div class="progressive-jackpots__wrapper">
					<div class="progressive-jackpots">
						<?php 
							if(config("RealDevice") < 3)
							{
								@$this->repeatData($this->content['home-progressives'],1);
							}	
						?>
								
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>