<!-- loading modal -->
<div id="ui-loader-animation" data-bind="visible: Notifier.loading" class="loading">
	<img id="spinner-fallback" src="/_images/common/loading.gif" style="display: none;">
	<img src="/_images/common/loaders/three-dots.svg" width="70" alt="">
</div>