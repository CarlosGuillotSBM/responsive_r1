<!-- BOTTOM CONTENT -->
<div class="bottom-content">
</div>
<!-- END Games info modal -->

</div>
<!-- END HOME MAIN CONTENT AREA -->
</div>
<!-- END AJAX WRAPPER - It is needed to add content inside dynamically -->
<!-- FOOTER AREA -->
<div class="clearfix"></div>
<!-- FOOTER NAVIGATION  -->
<div class="footer-nav-wrap">
	<?php include "_partials/common/footer-nav.php" ?>
</div>
<!-- /FOOTER NAVIGATION  -->
<!-- FOOTER FLOATING CONTENT LEGAL AND CONTACT  -->
<!-- /FOOTER FLOATING CONTENT LEGAL AND CONTACT -->
<footer id="footer" class="footer">
	<div class="footer-top">
	</div>
	<div class="footer-bottom">
	</div>
	<div class="footer-container">
		<!-- Container  -->
		<div class="footer">
			<div class="footer__inner">
				<div class="follow-us hi-icon-effect-4 hi-icon-effect-4a">
					<h4>Follow us on:</h4>
					<a target="_blank" href="https://facebook.com/bingoextra" class="hi-icon"  data-hijack="false"><img src="/_images/common/social/facebook.svg"></a>
					<a target="_blank" href="https://twitter.com/bingoextra" class="hi-icon" data-hijack="false"><img src="/_images/common/social/twitter.svg" onerror="this.onerror=null; this.src='image.png'"></a>
				</div>
				<div class="live-chat">
					<a target="_blank" data-hijack="false" href="<?php echo config('LiveChatURL'); ?>"><img src="/_images/common/live-chat.png" target="_blank"></a></div>
				</div>
				<div class="footer-legal">
					<ul class="footer__row__payments">
						<li> <img src="/_images/common/pay-methods/paypal.svg"></li>
						<li> <img src="/_images/common/pay-methods/maestro-icon.svg"></li>
						<li> <img src="/_images/common/pay-methods/mastercard-icon.svg"></li>
						<li> <img src="/_images/common/pay-methods/neteller.svg"></li>
						<li> <img src="/_images/common/pay-methods/paysafecard.svg"></li>
						<li> <img src="/_images/common/pay-methods/visa.svg"></li>
						<li> 
							<a href="/terms-and-conditions/">
								<img  src="/_images/common/pay-methods/18-plus.svg">
							</a>
						</li>
						<li>
							<a target="_blank" data-hijack="false" href="http://www.gamcare.org.uk/">
								<img  src="/_images/common/pay-methods/gamcare.svg">
							</a>
						</li>
						<li>
			            	<a target="_blank" data-hijack="false" href="https://www.gamstop.co.uk" rel="nofollow">
			            		<img src="/_images/common/pay-methods/gamstop.svg" />
			            	</a>
			            </li>
						<li><a target="_blank" data-hijack="false" href="http://www.gamblingcontrol.org/"><img src="/_images/common/pay-methods/alderney.svg"></a></li>
						<li><a target="_blank" data-hijack="false" rel="nofollow" href="https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022" ><img src="/_images/common/pay-methods/gambling-commission.svg"></a></li>
						<li>
		                    <a target="_blank" data-hijack="false" href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.show&lng=EN">
		                         <img src="/_images/common/pay-methods/ecogra.svg">
		                    </a>
			            </li>
			            
					</ul>
					<div class="footer-terms">
						All Financial Transactions processed by WorldPay. © Bingo Extra. All rights reserved.<br />
						Bingo Extra is licensed and regulated to offer Gambling Services in Great Britain by the UK Gambling Commission, license Number 000-039022-R-319427-004. All the games offered on the website have been approved by the UK Gambling Commission. Details of its current licensed status as recorded on the Gambling Commission's website can be found <a href='https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch.aspx?ExternalAccountId=39022' target='_new'>here</a>.
						Bingo Extra is also licensed and regulated by the Alderney Gambling Control Commission, License Number: 71 C1, to offer Gambling facilities in jurisdictions outside Great Britain.
						Our principal postal address is Inchalla, Le Val, Alderney, Channel Islands GY9 3UL.
					</div>
				</div>
				<div class="clearfix"></div>
			</div>

			<!-- /Container -->
			<!-- Support online Title -->
			<div class="support-online">
				<div target="_blank" data-hijack="false" class="support-online-title"><a href="<?php echo config('LiveChatURL');?>"><img src="/_images/common/support-online-title.png"></a></div>
			</div>
			<!-- /Support online Title -->
		</div>
	</footer>
	<!-- END FOOTER AREA -->
</div>
<!-- END INNER WRAPPER. It is necesary for foundation off canvas nav -->
</div>
<!-- login modal -->
<div id="login-modal" class="reveal-modal" data-reveal>
	<?php include "_partials/login/login-box.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- Claimed prize modal -->
<div id="claimed-prize-modal" class="reveal-modal" data-reveal>
	<?php include "_global-library/partials/promotion/promotion-claimed-prize.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">
<!-- END OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->
<!-- Games info modal -->
<div id="games-info" class="games-info reveal-modal" data-reveal>
	<?php include "_global-library/partials/games/games-details-modal.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Games info modal -->
<!-- Room info modal -->
<div id="rooms-info" class="games-info reveal-modal" data-reveal>
	<?php include "_global-library/partials/bingo/room-detail-modal.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Games info modal -->

<!-- Custom modal -->
<div id="custom-modal" class="reveal-modal" data-reveal>
	<?php include "_global-library/partials/bingo/login-box-modal.php" ?>
	<span class="close-reveal-modal">&#215;</span>
</div>
<!-- END Custom modal -->


<?php
include "_global-library/partials/modals/alert.php";
include "_global-library/partials/modals/notification.php";
include "_partials/common/loading-modal.php";
include "_global-library/partials/games/games-iframe.php";
include "_global-library/partials/games/games-last-played.php";
include "_global-library/partials/common/app-js.php";
include "_global-library/partials/common/recaptcha.php";
?>

<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">

</body>
</html>
