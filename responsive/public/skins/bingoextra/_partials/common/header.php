<script type="application/javascript">

	window.setTimeout(function () {
		var el = document.getElementById("ui-first-loader");
		if (el) el.parentNode.removeChild(el);
		el = document.getElementById("ui-loader-animation");
		if (el) el.style.display = "none";
	},1000);

</script>


<!-- OFF CANVAS -->
<!-- This is the Foundation side nav, everything should be wrapped inside off-canvas-wrap -->
<!-- OFF CANVAS WRAPPER. It is necesary for foundation off canvas nav -->
<div class="main-wrap">
	<!-- header  -->
	<header class="header header--<?php echo $this->controller; ?>">

		<!-- HEADER DESKTOP -->
		<div class="header-desktop">
			<!-- COOKIE POLICY -->
			<?php include "_global-library/partials/modals/cookie-policy.php" ?>
			<div class="header-wrap">
				<h1 class="seo-logo">
					<a href="/" data-hijack="true" class="header__logo ui-go-home-logo">
						<img src="/_images/logo/logo-sml.png"/>
					</a>
					<span style="display: none;">Bingo Extra the best online bingo sites on mobile and desktop.</span>
				</h1>
			</div>
			<div class="header-wrap">
				<div class="header__login" >
					<!-- Login Register Area OUT-->
					<div class="logged-out-buttons" data-bind="visible: !validSession()" style="display:none;">
						<div class="button--login"><a data-reveal-id="login-modal" href="/login/">Login</a></div>
						<div class="button--join"><a href="/register/">Join Now</a></div>
					</div>
					<!-- /Login Register Area OUT-->
					<!-- Login Register Area IN-->
					<div class="user-nav" data-bind="visible: validSession()" style="display:none;">
						<div class="user-nav-left">
							<div class="user-nav__logout"><a data-bind="click: logout"  data-hijack="true" data-gtag="Logout Desktop">Logout</a></div>
							<div id="my-account"  class="user-nav__my-account"><a href="/my-account/"  data-hijack="true">My Account</a></div>
							<div id="cashier" class="user-nav__cashier" title="Launch the Cashier"><a href="/cashier/"  data-hijack="true">CASHIER</a></div>
						</div>
						<div class="user-nav-right">
							<!-- Message Inbox-->
							<?php if(config("inbox-msg-on")): ?>
								<div class="user-nav__message-inbox" title="Message Inbox">
									<a href="/start/"  data-hijack="true" >
										<img src="/_images/balance-area/icon-message-inbox.png">
										<span class="user-nav__message-inbox__value" data-bind="visable: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
									</a>
								</div>
							<?php endif; ?>
							<!-- /Message Inbox-->
							<div class="user-nav__alias" title="Account Balance"><a href="/my-account/"  data-hijack="true" ><img src="/_images/balance-area/wallet-22x22.png"><span data-bind="text: balance" ></span></a></div>
							<div class="user-nav__balance" title="Loyalty Points"><a href="/my-account/"  data-hijack="true"><img src="/_images/common/icons/trophy.svg"><span data-bind="text: points"></span></a></div>
							<div class="user-nav__slots" title="Free Spins"><a href="/my-account/"  data-hijack="true"><img src="/_images/common/icons/free-spins.svg"><span data-bind="text: bonusSpins"></span></a></div>
						</div>
					</div>
					<!-- /Login Register Area IN-->
				</div>
			</div>

			<div class="header__container">
				<?php include "_partials/common/navigation-desktop.php"  ?>

			</div>
		</div>
		<!-- /HEADER DESKTOP -->

		<!-- HEADER MOBILE old-->
		<!-- <div class="header-mobile">
			<?php //include "_partials/common/navigation-mobile.php" ?>
		</div> -->
		<!-- nav for mobile -->
		<?php include "_partials/nav-for-mobile/nav-for-mobile.php"; ?>
		<!-- / nav for mobile -->
		<!-- search screen for mobile -->
		<?php include "_partials/search-screen/search-screen.php"; ?>
		<!-- / search screen for mobile -->
	</header>


</div>
<!-- /HEADER MOBILE -->
<!-- /header  -->
<div class="clearfix"></div>
<!-- AJAX CONTENT WRAPPER -->
 <?php
        include("_global-library/widgets/prize-wheel/prize-wheel-notification.php");
    ?>
<div id="ajax-wrapper">
	<!-- MAIN CONTENT AREA -->
	<div data-bind="css: { 'content-wrapper--logged-in' :validSession }" class="content-wrapper content-wrapper--<?php echo $this->controller; ?>  ">
		<!-- TOP CONTENT -->
		<div class="top-content">
		</div>
		<!-- /TOP CONENT -->