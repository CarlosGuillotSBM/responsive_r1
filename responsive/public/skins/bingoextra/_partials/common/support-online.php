<div class="footer__right">
<!-- SUPPORT ONLINE CONTENT -->
<div class="footer__right__inner">
<div class="footer__right__inner__social">
<div class="live-chat"><img src="/_images/common/live-chat.png"></div>
<div class="social">
	<ul>
	<li> <img src="/_images/common/social/icon-fb.png" class="social-icon"></li>
	<li> <img  src="/_images/common/social/icon-tw.png" class="social-icon"></li>
	<li> <img  src="/_images/common/social/icon-gl.png" class="social-icon"></li>
	</ul>
</div>
</div>
<div class="footer__right__inner__email"><a href="mailto:bingoextra@support.com"><span>Bingoextra@support.com</span></a></div>
</div>
</div>