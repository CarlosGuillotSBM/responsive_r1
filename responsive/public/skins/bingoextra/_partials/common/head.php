<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1" />


 <?php include "_global-library/partials/common/open-graph.php"; ?>
      

	<?php echo $this->seo_meta;?>




	<!-- icons for iPhone -->
	<link rel="apple-touch-icon" href="/_images/common/be-apple-touch-icon.png" />
	<link rel="icon" type="image/ico" href="/_images/common/favicon.ico" />
	<link href="https://plus.google.com/107827792670475608547/" rel="publisher" />
	<link rel="stylesheet" href="/_global-library/js/vendor/intlTelInput/css/intlTelInput.css<?php echo "?v=" . config("Version"); ?>" />
	<link rel="stylesheet" href="/_css/main.css<?php echo "?v=" . config("Version"); ?>" />
	<link rel="stylesheet" href="/_css/slick.css" />
			<?php
			include '_trackers/header.php';
			?>
	<script src="/_global-library/js/bower_components/modernizr/modernizr.js"></script>

	<script type="application/javascript">
	// sbm initialization
	var skinModules = [], skinPlugins = [];
	// common modules
	skinModules.push({
		id: "Notifier",
		options: {
			global: true
		}
	});
	skinModules.push({
		id: "GameLauncher",
		options: {
			modalSelector: "#games-info",
			global: true
		}
	});
	skinModules.push({
		id: "BingoLauncher",
		options: {
			modalSelector: "#rooms-info",
			modalSelectorWithoutDetails: "#custom-modal",
			global: true
		}
	});
	skinModules.push({
		id: "GameWindow",
		options: {
			global: true
		}
	});
	skinModules.push({
		id: "Favourites",
		options: {
			imgFavouriteOn: "/_images/common/icons/favourite-icon--on.png",
			imgFavouriteOff: "/_images/common/icons/favourite-icon--off.png",
			noFavIconsWeb: 6,
			noFavIconsTablet: 3,
			noFavIconsPhone:2,
			global: true
		}
	});
	skinModules.push( {
		id: "MailInbox",
		options: {
			global: true
		}
	});
	skinModules.push( {
		id: "TrackNavigation",
		options: {
			global: true
		}
	});
	skinModules.push({
		id: "ContentScheduler",
		options: {
			global: true
		}
	});
	// common plugins
	skinPlugins.push({
		id: "Global",
		options: {
			global: true
		}
	});
	skinPlugins.push({
		id: "Tables",
		options: {
			global: true
		}
	});
	skinPlugins.push({
		id: "MobileMenu",
		options: {
			global: true
		}
	});
	 //Prize Wheel
     skinModules.push({
          id: "PrizeWheel",
          options: {
               global: true
          }
     });
		skinModules.push({
			id: "CMANotifications",
			options: {
				global: true
			}
		});
	</script>
	<!-- Browser not supported redirect -->
	<?php include "_global-library/partials/common/user-agent-sniffer.php"; ?>


	       <!-- SEO Social -->
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Bingo Extra",
  "url" : "https://www.bingoextra.com/",
  "sameAs" : [ "https://www.facebook.com/bingoextra",
    "https://twitter.com/bingoextra",
    "https://plus.google.com/+Bingoextra",
    "https://www.youtube.com/channel/UCT9c7Q1qQqbOuONLMDfQFCA"]
}
</script>
<!--/ SEO Social -->



	<?php if(Env::isLive()):?>

		<!-- new relic -->
		<?php include "_global-library/trackers/new-relic/bingo-extra.php"; ?>
		<!-- /relic -->
		<!-- Start Bingo Extra Visual Website Optimizer Asynchronous Code -->
		<script type='text/javascript'>
		var _vwo_code=(function(){
			var account_id=271311,
			settings_tolerance=2000,
			library_tolerance=2500,
			use_existing_jquery=false,
			is_spa = 1,
			/* DO NOT EDIT BELOW THIS LINE */
			f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&f='+(+is_spa)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
			</script>
			<!-- End Visual Website Optimizer Asynchronous Code -->
        <link rel="manifest" href="/manifest.json"/>
			<?php endif;?>
			</head>
			<body id="<?php echo $this->controller; ?>" class="">
			<?php if(Env::isLive()):?>
			<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W7WCV8" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<script type="text/javascript">var rumMOKey='76c5dea161cd7cf28ffe9fb92b26ce0d';(function(){if(window.performance && window.performance.timing && window.performance.navigation) {var site24x7_rum_beacon=document.createElement('script');site24x7_rum_beacon.async=true;site24x7_rum_beacon.setAttribute('src','//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey='+rumMOKey);document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);}})(window)</script>
			<?php endif;?>
