<article class="footer-nav">
					<ul>
					<li><a href="/help/">HELP</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/about-us/">ABOUT</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/faq/">FAQ</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/banking/">BANKING</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/terms-and-conditions/">TERMS & CONDITIONS</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/privacy-policy/">PRIVACY POLICY</a></li>
					<li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none"><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li data-bind="visible: validSession() && window.location.pathname == '/start/'" style="display: none"><a  href="/cashier/?settings=1" data-hijack="true">PLAYER SETTINGS</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/responsible-gaming/">RESPONSIBLE GAMING</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/complaints-and-disputes/">COMPLAINTS AND DISPUTES</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="https://www.luckyjar.com/uk/" target="_blank">AFFILIATES</a></li>
					<li><img src="/_images/common/star-sml.png" class="star-sml"></li>
					<li><a href="/archive/" target="_blank">ARCHIVE</a></li>
					</ul>
</article>