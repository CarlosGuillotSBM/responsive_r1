
  <!--  *********** Navigation Desktop Loogged out  *********** -->

<div class="navigation-desktop">
		<ul class="navigation-desktop__nav">
			<li id="home"><a class="ui-go-home" href="/" data-hijack="true">Home</a></li>
			<li data-bind="visible: !validSession()"><a href="/bingo/" data-hijack="true">Bingo</a></li>
			<li style="display: none" data-bind="visible: validSession, click: playBingo"><a>Bingo</a></li>
			<li id="slots"><a href="/slots/" data-hijack="true">Slots</a></li>
			<li id="games"><a href="/games/" data-hijack="true">All Games</a></li>
			<li id="promotions"><a href="/promotions/" data-hijack="true">Promotions</a></li>
			<li id="community"><a href="/community/" data-hijack="true">Community</a></li>
			<li id="extraclub"><a href="/extra-club/" data-hijack="true">Extra Club</a></li>
			<li id="support"><a href="/support/" data-hijack="true">Support</a></li>
		</ul>
</div>

  <!--  *********** Navigation Desktop Loogged out end *********** -->




