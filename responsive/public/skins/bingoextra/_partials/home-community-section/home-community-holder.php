<div class="content-holder home-community-holder">
	<!-- content holder title -->
	<!-- content holder content area -->
	<div class="content-holder__content">
		<!-- content holder content full width inner holder-->
		<!-- <div class="content-holder__content_full-width"></div> -->
		
		<!-- content holder content left inner holder-->
		<div class="content-holder__content_full-width">
			<div class="content-holder__title">
				<h1>COMMUNITY</h1>
				<!-- content holder title right link -->
				<span class="content-holder__title__link"><a href="/community/">View All &raquo;</a></span>
			</div>
			
			<div class="content-holder__inner">
				<!-- Featured Games Home  -->
				<?php include "_partials/home-community-section/home-community-inner.php"; ?>
							<!-- Featured Games Home  -->
			</div>
			
		</div>
		<!-- content holder content right inner holder-->
		
		</div>
	</div>
