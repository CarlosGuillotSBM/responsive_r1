<?php

    $xml_data = '<deliverymessage>'.
            '<created>2016-08-09T14:50:29+0000</created>'.
            '<creatingUserId>EFF2FAEA0A931B3144A23B80390EC3F4</creatingUserId>'.
            '<id>6FC88BD20A931B3172B69BA36B43B7B0</id>'.
            '<modified>2016-08-09T14:50:29+0000</modified>'.
            '<ownerUserId>628824A20A931B3170545EB3EDFB354C</ownerUserId>'.
            '<version>0</version>'.
            '<body>STOP2 </body>'.
            '<cost>'.
            '<amount>0</amount>'.
            '<currency>AUD</currency>'.
            '</cost>'.
            '<deliveryMethod>SMS</deliveryMethod>'.
            '<direction>MO</direction>'.
            '<inReplyTo>6FC796730A931B3172B69BA347EE534D</inReplyTo>'.
            '<logicalMessageId>6FC795980A931B3172B69BA367C529BC</logicalMessageId>'.
            '<originator>447486954478</originator>'.
            '<parts>1</parts>'.
            '<recipient>60777</recipient>'.
            '<routeId>MO</routeId>'.
            '<senderReference></senderReference>'.
            '<status>ACCEPTED</status>'.
            '</deliverymessage>';

    $xml_data = urlencode($xml_data);
    $url = 'http://responsive.bingoextra.dev/movibate/?s=6&f=optout&xml='.$xml_data;

    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

    $result = curl_exec($ch);

    if ( curl_errno($ch) ) {
        $result = 'cURL ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
    } else {
        $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        switch($returnCode){
            case 200:
                break;
            default:
                $result = 'HTTP ERROR -> ' . $returnCode;
                break;
        }
    }

    curl_close($ch);