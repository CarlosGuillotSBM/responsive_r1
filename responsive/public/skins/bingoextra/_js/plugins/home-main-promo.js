var sbm = sbm || {};

sbm.HomeMainPromo = function () {
	"use strict";

	return {
		run : function () { 

            var onResize = function(){
                
                if ($(".home-main-promo").length || $(".home-main-promo--city").length) {
                    var _w = $( window ).width();
                    var _h = $( window ).height();
                    var _headerheight;
                    var teaserview = 25;
                    var sectionheight;
                    // if the we are on mobile
                    if(_w < 640)
                    {
                        _headerheight = 50; //$('.header').height();
                        teaserview = 25;
                        sectionheight = (_h - _headerheight)-teaserview;
                        if ($(".home-main-promo--city").length){
                            $(".home-main-promo--city").css("height", sectionheight);
                        }
                        if ($(".home-main-promo").length){
                            $(".home-main-promo").css("height", sectionheight);
                        }
                         
                    }
                }
            };

            $( window ).resize(function() {
                onResize();
            });

            onResize();
        }
    }

}