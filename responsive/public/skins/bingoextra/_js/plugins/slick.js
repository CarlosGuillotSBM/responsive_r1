var sbm = sbm || {};

sbm.Slick = function () {
    "use strict";

    return {

        setSlider: function ($el, options) {

            // make all the images visible
            $el.find("li").show();

            // start bxSlider plugin
            $el.bxSlider(options);

            // handles clicks on banners

            $el.find("a").on("click", function (e) {

                var $target = $(e.currentTarget);
                var url = $target.attr("href");

                if (!url) {
                    return;
                }

                var pos = $el.getCurrentSlide();
                var images = $target.find("img");
                var img = images.length ? $(images).attr("src") : "";
                var playerId = $.cookie("PlayerID") || "";

                var label = playerId ? "player=" + playerId + ";url=" + url + ";img=" + img : "url=" + url + ";img=" + img + ";pos=" + pos;

                this.sendDataToGoogle("carousel", label);

            }.bind(this));
        },

        sendDataToGoogle: function (category, label) {

            if (window.dataLayer) {

                window.dataLayer.push({
                    "event": "GAevent",
                    "eventCategory": category,
                    "eventAction": window.location.pathname,
                    "eventLabel": label
                });
            }
        },

        run : function () {

            /**
             * BxSlider plugin initialization for homepage slider
             */

            // cache dom elem
            var $bxSlider = $('.bxslider');
            var i = 0,
                options;

            for (i; i < $bxSlider.length; i++) {

                options = {
                    auto: true,
                    pause: '7000',
                    autoHover: true,
                    slideWidth: $bxSlider.data("width"),
                    minSlides: 1,
                    slideMargin: 0,
                    mode: 'fade',
                    pager: false
                };

                this.setSlider($($bxSlider[i]), options);
            }

        }
    };


}