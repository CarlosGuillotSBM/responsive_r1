var sbm = sbm || {};

/**
 // MOBILE NAV ICON AND SLIDE OUT NAVIGATION
 // Hamburger menu button toggle animation
 // animation for the mobile-menu slide from right needs to toggle the height and opacity on .mobile-menu currently right: -320px; and 0 opacity
 // new values AFTER ANIMATED should be right: 0PX; opacity: 1;

 // Categories button toggle animation
 // animation for the game categories slide down needs to be toggle the height and opacity on
 // .games-filter-menu__popover-wrap is set currently 0px height and 0 opacity
 // new values after animation should be height: 246px; opacity: 1;
 */

sbm.MobileMenu = function() {
    "use strict";

    return {

        /**
         * common properties
         */
        menuOffSet: "-414px",
        menuTopAreaOffset: "-414px",
        categoriesMenuExpanded: false,
        categoriesMenuHeight: "246px",
        menuItemHeight: 50,

        /**
         * Toggles the menu visibility
         * @param open - true to open, false to close
         */
        toggleMenu: function(open) {

            $(".mobile-menu").css("visibility", "visible").animate({
                "right": open ? "0px" : this.menuOffSet
            }, "slow");

            $(".mobile-menu-top-area").css("visibility", "visible").animate({
                "right": open ? "0px" : this.menuTopAreaOffset
            }, "slow");
        },
        /**
         * Handles the clicks on the burger icon
         */
        clicksOnBurger: function() {

            var $burgerMenu = $('.menu-toggle-wrap');
            var $burger = $(".burger");

            $burgerMenu.on("click", function() {
                $burger.toggleClass("active");
                if ($burger.hasClass("active")) {
                    // open menu
                    this.toggleMenu(true);
                } else {
                    // close menu
                    this.toggleMenu(false);
                }

                //hide search screen
                $(".search-screen").css("visibility", "hidden").animate({
                    "bottom": "-100%"
                }, "slow");
                $("body").removeClass("modal-open");

            }.bind(this));
        },
        /**
         * Handles clicks on the Slots categories sub-menu
         */
        clicksOnCategories: function() {

            $(".slots-categories").on("click", function() {

                // rotate categories sub-menu arrow
                var transformation = this.categoriesMenuExpanded ? "scale(1, 1)" : "scale(1, -1)";
                $(".chevron-icon").css("transform", transformation);

                // sub-menu animation
                $(".games-filter-menu__popover-wrap").animate({
                    "height": this.categoriesMenuExpanded ? "0px" : this.categoriesMenuHeight,
                    "opacity": this.categoriesMenuExpanded ? "0" : "1"
                }, "slow");

                // set flag to know the sub-menu state
                this.categoriesMenuExpanded = !this.categoriesMenuExpanded;

                //hide search screen
                $(".search-screen").css("bottom", "-100%");
                //remove fixed position from the body to allow scrolling again

            }.bind(this));
        },
        /**
         * Handles clicks on links
         * Should close menu and sub-menu
         */
        clicksOnLinks: function() {
            $(".mobile-menu").find("a.menu-link").on("click", function() {
                this.categoriesMenuExpanded = false;
                $(".burger").removeClass("active");
                this.toggleMenu(false);



                //hide search screen
                $(".search-screen").css("bottom", "-100%");
                //remove fixed position from the body to allow scrolling again



            }.bind(this));
        },
        highlightLinks: function(route) {
            var $links = $(".mobile-menu").find("a.menu-link");
            $links.removeClass("active");
            $links.each(function() {
                if ($(this).attr("href") === route) {
                    $(this).addClass("active");
                    return false;
                }
            });
        },
        clicksOnSearch: function () {
            // click to open game search screen
            $("#ui-search-screen").click(function(e) {
                e.preventDefault();
                //show search screen
                $(".search-screen").css("visibility", "visible").delay(500).animate({
                    "bottom": "0px"
                }, "slow");
                $(".burger").toggleClass("active");
                $("body").addClass("modal-open");
            });
        },
        clicksOnSlotsLink: function () {
            $("#ui-slots-menu-link").on("click", function() {
                window.location.href = "/slots/";
            });
        },
        /**
         * Handles clicks on the Slots and Live Casino categories sub-menu
         */
        toggleSubmenu: function(toggleBtn) {
            var height = $(toggleBtn).siblings('.mobile-menu-item__submenu').outerHeight();
            var $parent = $(toggleBtn).closest('.mobile-menu-item');

            if ($parent.hasClass('is-active')) {
                $parent.removeClass('is-active');
                $parent.css('height', this.menuItemHeight);
                $parent.find('.chevron-icon').css('transform', 'scale(1, 1)');
            } else {
                $parent.addClass('is-active');
                $parent.css('height', this.menuItemHeight + height);
                $parent.find('.chevron-icon').css('transform', 'scale(1, -1)');
            }

            // set flag to know the sub-menu state
            this.categoriesMenuExpanded = !this.categoriesMenuExpanded;

            //hide search screen
            $(".search-screen").css("bottom", "-100%");
        },

        initSubmenus: function() {
            var self = this;
            $('.mobile-menu-item__submenu-toggle').on('click', function() {
                self.toggleSubmenu.call(self, this);
            });
        },
        /**
         * The starting point
         * This function is called from the initialization of the framework
         */
        run: function() {

            this.clicksOnBurger();
            // this.clicksOnCategories();
            this.initSubmenus();
            this.clicksOnLinks();
            this.highlightLinks(window.location.pathname);
            this.clicksOnSearch();
            this.clicksOnSlotsLink();
        }
    };



};