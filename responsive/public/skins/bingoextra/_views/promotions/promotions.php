<div class="two-columns-content-wrap promotions">
	<script type="application/javascript">   
	    skinModules.push({
	        id: "RadFilter"
	    });
	</script>
	<!-- home breadcrumbs -->
	<?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
	<!-- /home breadcrumbs -->
	<div class="section-left__title">
		<h4><span>Promotions</span></h4>
	</div>
	<!-- main content top -->
	<div class="main-content__top">
		<div class="main-content__top__left">
			<div class="promotion-featured">
				<?php edit($this->controller,'featured-promotion'); ?>
				<?php @$this->repeatData($this->content['featured-promotion']);?>
			</div>
		</div>
		<div class="main-content__top__right">
			<h1><span>Extra Offer!</span></h1>
			<div class="main-content__top__right__inner">
				<?php edit($this->controller,'side-content'); ?>
				<?php  @$this->repeatData($this->content['side-content']);?>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<!-- /main content top -->
	<!-- main content bottom -->
	<div class="main-content__bottom">
		<?php edit($this->controller,'promotions'); ?>
		<div class="promo-container">
		
			<script type="application/javascript">   
			    skinModules.push({
			        id: "RadFilter"
			    });
			</script>
			<!-- Promotions repeater -->
			<!-- Last parameter set up wich one is the featured -->
			<?php //@$this->repeatData($this->content['promotions']);?>
			<?php @$this->repeatData($this->content["promotions"],1, "_global-library/partials/promotion/promotion.php"); ?>
			<!-- /Promotions repeater -->
		</div>
	</div>
	<!-- /main content bottom -->
</div>