<script type="application/javascript">
    skinModules.push({
        id: "OptIn"
    });
</script>
<div class="two-columns-content-wrap">
    <!-- home breadcrumbs -->
    <?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
    <!-- /home breadcrumbs -->
    <!-- HOME MAIN CONTENT AREA -->
    <section class="section">
        <!-- PROMOTIONS CONTENT -->
        <div class="">
            <div class="clearfix"></div>

            <?php edit($this->controller,'promotions'); ?>

            <!-- promotions list -->

            <div class="promotions-detail">
                <!-- Promotions detail -->
                <article class="promotion--featured">
                    <div class="promotion__wrapper">
                        <div class="promotion__content__wrapper">
                            <h1 class="section__title"><?php echo $this->content["Title"];?></h1>
                            <div class="promotion-image__cta"><img src="<?php echo $this->content["Text8"] != '' ? $this->content["Text8"] : $this->content["Image"]?>" alt="<?php //echo $this->content["Alt"]?>">
                                <!-- CTAS -->
                                <div class="promo__container__cta">
                                    <a data-hijack="true" style="display: none" class="promo__container__button cta-opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>

                                    <!-- span to be removed by javascript - DO NOT REMOVE!! -->
                                    <span></span>
                                    <?php if($this->content['Text4'] === '') {?>
                                        <!-- PLAY NOW -->
                                        <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                                        <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                                    <?php } else { ?>
                                        <!-- PLAY NOW -->
                                        <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                                        <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                                    <?php }?>
                                </div>
                                <!-- /CTAS -->
                            </div>


                            <?php
                            echo $this->content["Body"];
                            ?>

                            <!-- CTAS -->
                            <div class="promo__container__cta">
                                <a data-hijack="true" style="display: none" class="promo__container__button cta-opt-in" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>


                                <!-- PROMO FOOTER NAV MOBILE -->
                                <div class="footer-nav__container">
                                    <a data-hijack="true" style="display: none" data-tournament="<?php echo $this->content['DetailsURL'] ?>" class="cta-opt-in">OPT-IN</a>
                                    <?php if($this->content['Text4'] === '') {?>
                                        <!-- PLAY NOW -->
                                        <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                                        <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                                    <?php } else { ?>
                                        <!-- PLAY NOW -->
                                        <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                                        <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                                    <?php }?>
                                    <a data-hijack="true" href="/promotions/<?php echo $this->getNextPromoURL($this->content["DetailsURL"]); ?>" class="cta-next-promo">NEXT PROMO ></a>
                                </div>

                                <!-- Accordion -->
                                <dl id="ui-promo-tcs" class="promotion__terms-conditions accordion" data-accordion>
                                    <dd class="accordion-navigation active">
                                        <a href="#panelTC">Terms &amp; Conditions</a>
                                        <div id="panelTC" class="content active">
                                            <?php echo @$this->content["Terms"];?>
                                        </div>
                                    </dd>
                                </dl>
                                <!-- /Accordion -->
                            </div>
                        </div>
                </article>
                <!-- /Promotions detail -->
                <!-- RELATED PROMOS -->
                <div>
                    <!-- Title -->
                    <div class="section-left__title">
                        <h4><span>Related Promos</span></h4>
                    </div>
                    <!-- /Title -->
                    <div class="three-columns-equal related-promos">
                        <!-- promos -->
                        <?php @$this->getPartial($this->promotions['promotions'],2, "_global-library/partials/promotion/promotion.php"); ?>
                        <?php @$this->getPartial($this->promotions['promotions'],3, "_global-library/partials/promotion/promotion.php"); ?>
                        <?php @$this->getPartial($this->promotions['promotions'],4, "_global-library/partials/promotion/promotion.php"); ?>
                    </div>
                    <!-- /RELATED PROMOS -->



                </div>
                <!-- /promotions list -->
            </div>
            <!-- PROMOTIONS CONTENT -->
    </section>
</div>
<!-- END HOME MAIN CONTENT AREA -->