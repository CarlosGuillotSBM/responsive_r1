<!-- MIDDLE CONTENT -->
<div class="main-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!-- END breadcrumbs -->
  <section class="section">
    <div class="section-left__title">
      <h4><span>Bingo Rules</span></h4>
    </div>
    <!-- CONTENT -->
    <?php edit($this->controller,'bingorules'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['bingorules']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->