<!-- MIDDLE CONTENT -->
<div class="middle-content__box">

	<section class="section">
		<!-- CONTENT -->
		<div class="content-template">
			
			<div class="error-page">
				
				<div class="error-page__image">
					<img src="/_images/common/error-404/error-404.png">
				</div>
				<div class="error-page__text">
					<h1 class="error-page__error">
					WHOOPS!
					</h1>
					<p class="error-page__msg">
						THE PAGE YOU REQUESTED DOES NOT EXIST.
					</p>
					<div class="error-page__suggestion clearfix">
						<p><a href="/">Please try again. Thanks!</a></p>
						<div class="games-list__grid games-list__promoted-games ">
							
						</div>
						<!-- <a class="registercta" href="/register/" data-hijack="true">JOIN NOW</a> -->
					</div>
				</div>
				
			</div>
			<!-- /CONTENT-->
		</section>
	</div>
	<!-- END MIDDLE CONTENT -->
	<!-- BOTTOM CONTENT -->
	<div class="bottom-content">
	</div>
	<!-- END Games info modal -->
	
</div>
<!-- END HOME MAIN CONTENT AREA -->