
  <!-- MIDDLE CONTENT -->
  <div class="main-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!-- END breadcrumbs -->
    <section class="section">
      <h1 class="section__title"><?php echo $this->controller; ?></h1>
      <!-- CONTENT -->
      <?php edit($this->controller,'support'); ?>
      <div class="content-template">
        
        <!-- repeatable content -->
        <?php @$this->repeatData($this->content['support']);?>
        <!-- /repeatable content -->
      </div>
      <!-- /CONTENT-->
    </section>
  </div>
  <!-- END MIDDLE CONTENT -->