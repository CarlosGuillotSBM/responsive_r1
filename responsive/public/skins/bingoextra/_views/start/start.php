<script type="application/javascript">
    skinModules.push( { id: "ClaimCashback" } ); 
    skinModules.push( { id: "ClaimUpgradeBonus" } );
</script>
<!-- Lobby Wrap -->
<div class="middle-content lobby-wrap">
    <!-- Lobby Left Column -->
    <div class="lobby-wrap__left">
        <!-- welcome section -->
        <?php  include'_global-library/partials/start/start-welcome-section.php'; ?>
        <!-- my messages section -->
        <?php if(config("inbox-msg-on")): ?>
            <?php include'_global-library/partials/start/start-my-messages-section.php'; ?>
        <?php endif; ?>
        <!-- featured games section -->
        <div class="mobile-hide-bingo">
            <?php  include'_global-library/partials/start/start-bingo-schedule.php'; ?>
        </div>
        <div class="clearfix"></div>
        <div class="mobile-show-bingo">
            <h1>BINGO SCHEDULE</h1>
            <a href="/bingo-schedule/"><img class="mobile-bingo-room-cta" src="/_images/mobile-bingo-room-cta/bingo-schedule-cta.jpg" alt="View Bingo Schedule" ></a>
        </div>
        
        <div class="clearfix"></div>
        
        <?php  include'_global-library/partials/start/start-featured-games-section.php'; ?>
        <!-- my promotions section -->
        <?php  include'_global-library/partials/start/start-my-promotions-section.php'; ?>
    </div>
    <!-- End Left Column -->
    <!-- Right Column -->
    <div class="game-url-seo lobby-wrap__right">
        <div class="two-columns-content-wrap__right">
            <!-- lobby right deposit button -->
            <?php  include'_global-library/partials/start/start-right-deposit-button.php'; ?>
            <!-- lobby claim cashback button -->
            <?php  include'_global-library/partials/start/start-claim-cashback.php'; ?>
            <!-- lobby claim upgrade bonus button -->
            <?php  include'_global-library/partials/start/start-claim-upgrade-bonus.php'; ?>
            <!-- lobby right banner -->
            <?php  include'_global-library/partials/start/start-right-banner.php'; ?>
            <!-- lobby right progressives -->
            <?php  include'_global-library/partials/start/start-right-progressives.php'; ?>
            <!-- lobby yesterdays wins -->
            <?php  // include'_global-library/partials/start/start-right-yesterdays-wins.php'; ?>
            <!-- lobby right latest winners -->
            <?php  include'_global-library/partials/start/start-right-latest-winners.php'; ?>
        </div>
    </div>
    <!-- End Right Column -->
</div>
<!-- End Lobby Wrap -->
<!-- lobby my messages single modal -->
<?php 
    // dont miss out popup
    include "_global-library/partials/modals/dont-miss-out.php";
?>