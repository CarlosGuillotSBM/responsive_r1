<!-- single post main content -->
<div class="main-content community-blog">
	<!-- home breadcrumbs -->
	<?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
	<!-- /home breadcrumbs -->
	<!-- content -->
	<div class="two-columns-content-wrap">
		<!-- community nav -->
		<?php include "_partials/community-nav/community-nav.php" ?>
		<!-- /community nav -->
		<!-- left column -->
		<div class="two-columns-content-wrap__left">
			<?php edit($this->controller,'blog'); ?>
			<!-- repeatable content -->
			<?php @$this->repeatData($this->content['blog']);?>
			<!-- /repeatable content -->

		</div>
		<!--  /left column -->
		<!-- right column -->
		<div class="two-columns-content-wrap__right">
		<?php if (config("RealDevice") != 3) { ?>
			<?php edit($this->controller,'blog-side-content'); ?>
			<!-- repeatable content -->
			<?php @$this->repeatData($this->content['blog-side-content']);?>
			<!-- /repeatable content -->
		<?php } ?>
		</div>
		<!-- /right column -->
	</div>
	<!-- /content  -->
</div>
<!-- /single post main content -->
