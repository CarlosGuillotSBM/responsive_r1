<script type="application/javascript"> 
// skinPlugins.push( {id: "rad_dotdotdot"} );
</script>
<!-- single post main content -->
<div class="main-content community">
	<!-- home breadcrumbs -->
	<?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
	<!-- /home breadcrumbs -->

	<!-- content -->
	<div class="two-columns-content-wrap">
		<!-- community nav -->
		<?php include "_partials/community-nav/community-nav.php" ?>
		<!-- /community nav -->
		<!-- left column -->
		<div class="two-columns-content-wrap__left">
			<!-- top left area -->
			<?php edit($this->controller,'blog'); ?>
			<div class="community__top-post">
				<?php @$this->getPartial($this->content['blog'],1); ?>
			</div>
			<!-- /top left area -->
			<!-- blog area -->
			<section class="community__section">
				<?php //@$this->repeatData($this->content['blog']);?>
				<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],2); ?></div>
					<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],3); ?></div>
						<div class="community__section__post"><?php @$this->getPartial($this->content['blog'],4); ?></div>
			</section>
			<!-- /blog area -->
			<!-- winners area -->
			<section class="community__section">
				<!-- Title -->
				<div class="section-left__title__community">
					<h4><span>Winner Stories</span></h4>
				</div>
				<!-- /Title -->
				<div class="community__section__post"><?php @$this->getPartial($this->content['winners'],1); ?></div>
				<div class="community__section__post"><?php @$this->getPartial($this->content['winners'],2); ?></div>
				<div class="community__section__post"><?php @$this->getPartial($this->content['winners'],3); ?></div>
			</section>
			<!-- /winners area -->
			<!-- chat host area -->			
			
			<section class="community__section community__section__chat-thumbs">
				<!-- Title -->								
				<div class="section-left__title__community">
					<h4><span>Chat Moderator Bios</span></h4>
				</div>
				
				<!-- /Title -->								
				<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],1); ?></div>
				<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],2); ?></div>
				<div class="community__section__post"><?php @$this->getPartial($this->content['chat-host'],3); ?></div>
			</section>
			<!-- /chat host area -->
				<!-- chat news area -->
			<section class="community__section">
				<!-- Title -->				
				<div class="section-left__title__community">
					<h4><span>Chat News</span></h4>
				</div>
				<!-- /Title -->
				
				<div class="community__section__post"><?php @$this->getPartial($this->content['chat-news'],1); ?></div>
				<div class="community__section__post"><?php @$this->getPartial($this->content['chat-news'],2); ?></div>
				<div class="community__section__post"><?php @$this->getPartial($this->content['chat-news'],3); ?></div>
			</section>
			<!-- /chat news area -->
		</div>
		<!--  /left column -->
		<!-- right column -->
		<div class="two-columns-content-wrap__right ui-scheduled-content-container">
		<?php if (config("RealDevice") != 3) { ?>
			<?php edit($this->controller,'blog-side-content'); ?>
			<!-- repeatable content -->
			<?php @$this->repeatData($this->content['blog-side-content']);?>
			<!-- /repeatable content -->

		<?php } ?>
		</div>
		<!-- /right column -->
		
	</div>
	<!-- /content  -->
</div>
<!-- /single post main content -->