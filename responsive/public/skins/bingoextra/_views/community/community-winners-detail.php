<script type="application/javascript">

	var sbm = sbm || {};
	sbm.games = sbm.games || [];
	sbm.games.push({
		"id"    : <?php echo json_encode(@$this->content['DetailsURL']);?>,
		"icon"  : "<?php echo @$this->content['Image'];?>",
		"bg"    : <?php echo json_encode(@$this->content['Text1']);?>,
		"title" : <?php echo json_encode(@$this->content['Title']);?>,
		"type"  : "game",
		"desc"  : <?php echo json_encode(@$this->content["Intro"]);?>,
		"thumb" : "<?php echo @$this->content['Image1'];?>",
		"detUrl": <?php echo json_encode(@$this->content['Path']);?>,
		"demo": <?php echo json_encode(@$this->content['DemoPlay']);?>
	});

</script>

<!--single post main content -->
<div class="main-content community-article">
	<!-- home breadcrumbs -->
	<?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
	<!-- /home breadcrumbs -->
	<!-- content -->
	<?php edit($this->controller,$this->action,$this->subaction); ?>
	
	<div class="content-template">
		<!-- POST CONTENT -->
		<article class="article">
			<div class="article__content">
				<h1 class="article__content__title" itemprop="name">
				<div class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></div>
				<a class="article__url" href="" itemprop="url" ><?php echo @$this->content['Title'];?> </a>
				</h1>
				
				<div class="article__content__body" itemprop="articleBody">
					<img width="" src="<?php echo @$this->content['Text3'];?>" alt="<?php echo @$this->content['ImageAlt'];?>">
					<?php echo @$this->content['Body'];?>
				</div>
			</div>
			<span class="article__publisher" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
			<span itemprop="name">Bingo Extra</span>
			</span>

			
				<!-- Winner Slot Details -->
				<?php if(exists(@$this->content['Text11'])):?>
				<div class="article__content" style="clear:both">
					<div class="winner-game">
						<div class="winner-game__left">
							<h4>Won on: <a href="<?php echo @$this->content['Path'];?>"><?php echo @$this->content['Name'];?></a></h4>
							<h4>Won amount: <span><?php echo @$this->content['Text11'];?></span></h4>
							<p><?php echo @$this->content['Text12'];?><br /><a href="<?php echo @$this->content['Path'];?>">More Info on <em><?php echo @$this->content['Name'];?></em> »</a></p>
						</div>

						<div class="winner-game__right">
							<article class="game-icon">
								<div class="game-icon__wrapper">							
									<div class="game-icon__image">
										<img data-bind="click: GameLauncher.openGame"
											 data-gameid="<?php echo $this->content["DetailsURL"];?>"
											 class="lazy" src="/_images/common/no-image.jpg" alt="<?php echo $this->content["Alt"];?>"
											 data-original="<?php echo $this->content["Image"];?>">
										<div class="clearfix"></div>
								
									</div>
									<!-- overlay -->
									<div  class="game-icon__overlay">

										<div data-bind="click: GameLauncher.openGame" class="ui-launch-game play-icon "
											data-gameid="<?php echo $this->content["DetailsURL"];?>">
											<img class="playimg" src="/_images/common/icons/play.png">
										</div>
									</div>									
									<!-- end overlay -->
								</div>						
							</article>							
							<a data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $this->content["DetailsURL"];?>" class="button winner-game__right-btnJoinnow">PLAY NOW</a>
						</div>
					</div>
				</div>			
			<?php endif;?>	
			<!-- End Winner Slot Details -->
			
			<!-- Author Block -->
			<div class="_author-block">
				<div class="_author-block__avatar">
					<?php if(exists(@$this->content['Image1'])):?>
			
						<img width="" src="<?php echo @$this->content['Text7'];?>" alt="<?php echo @$this->content['Text8'];?>">
				
					<?php endif;?>
				</div>
				
				<h4>
				
					<span itemprop="author" itemscope itemtype="http://schema.org/Person">
					<span itemprop="name"><?php echo @$this->content['Text5'];?></span>
		
				</h4>
				
				<div class="_author-block__line">
					<div class="_author-block__lineleft"></div>
					<div class="_author-block__lineright"></div>
				</div>
				
				<div class="_author-block__biography">
					<p><?php echo @$this->content['Terms'];?></p>
					
				</div>
				<ul>
					<a target="_blank" href="<?php echo @$this->content['Text6'];?>" class="_author-block__link" target="_author">
					<li class="_author-block__social-icons">Author's Social Profile: <img src="/_images/common/social/icon-gl.png"></li>
					</a>
				</ul>
				
			</div>
			<!-- Author Block -->
		</article>
		<!-- /POST CONTENT -->
	</div>
</div>
<!-- /content -->

