<!--single post main content -->
<div class="main-content community-article">
	<!-- home breadcrumbs -->
	<?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
	<!-- /home breadcrumbs -->
	<!-- content -->
	<?php edit($this->controller,$this->action,$this->route[0]);?>
	
	<div class="content-template">
		<!-- POST CONTENT -->
		<article class="article">
			<div class="article__content">
				<h1 class="article__content__title" itemprop="name">
				<div class="article__content__date" itemprop="datePublished"><?php echo @$this->content['Date'];?></div>
				<a class="article__url" href="" itemprop="url" ><?php echo @$this->content['Title'];?> </a>
				</h1>
				
				<div class="article__content__body" itemprop="articleBody">
					<img width="" src="<?php echo @$this->content['Image'];?>" alt="<?php echo @$this->content['ImageAlt'];?>">
					<?php echo @$this->content['Body'];?>
				</div>
			</div>
			<span class="article__publisher" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
			<span itemprop="name">Bingo Extra</span>
			</span>

						
			<!-- Author Block -->
			<div class="_author-block">
				<div class="_author-block__avatar">
					<?php if(exists(@$this->content['Image1'])):?>
			
						<img width="" src="<?php echo @$this->content['Image1'];?>" alt="<?php echo @$this->content['ImageAlt1'];?>">
				
					<?php endif;?>
				</div>
				
				<h4>
				
					<span itemprop="author" itemscope itemtype="http://schema.org/Person">
					<span itemprop="name"><?php echo @$this->content['Text1'];?></span>
		
				</h4>
				
				<div class="_author-block__line">
					<div class="_author-block__lineleft"></div>
					<div class="_author-block__lineright"></div>
				</div>
				
				<div class="_author-block__biography">
					<p><?php echo @$this->content['Terms'];?></p>
					
				</div>
				<ul>
					<a target="_blank" href="<?php echo @$this->content['Text2'];?>" class="_author-block__link" target="_author">
					<li class="_author-block__social-icons">Author's Social Profile: <img src="/_images/common/social/icon-gl.png"></li>
					</a>
				</ul>
				
			</div>
			<!-- Author Block -->
		</article>
		<!-- /POST CONTENT -->
	</div>
</div>
<!-- /content -->