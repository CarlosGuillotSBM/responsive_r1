<div class="main-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!-- END breadcrumbs -->
  <section class="section">
    <!-- Title -->
    <div class="section-left__title">
      <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>
    <!-- /Title -->
    <!-- CONTENT -->
    <?php edit($this->controller,'about-us'); ?>
    <div class="content-template">
      <!-- repeatable content -->
      <?php $this->repeatData($this->content['about-us']);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->