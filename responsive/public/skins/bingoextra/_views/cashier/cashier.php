<style type="text/css">
    .cashierholder > .content-template > .cashier {
        float: left;
    }
</style>


<!-- MIDDLE CONTENT -->
  <div class="main-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!-- END breadcrumbs -->
    <section class="section cashier-wrapper">
      <!-- CONTENT -->
      <?php edit($this->controller,'help'); ?>
 
        
    <section class="cashierholder">
    
      <!-- CONTENT -->
      <div class="content-template">
        <!-- PRD CASHIER -->
        <script type="application/javascript">
        skinModules.push( { id: "Cashier", options: { source: "Cas" } } );
        </script>
        <?php include '_global-library/partials/cashier/cashier.php'; ?>
        <!-- /PRD CASHIER -->
      </div>
      <!-- /CONTENT-->
    </section>
  </div>
   <!-- /MIDDLE CONTENT -->
  