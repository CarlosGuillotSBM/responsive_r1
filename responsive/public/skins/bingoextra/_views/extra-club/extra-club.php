<!-- MIDDLE CONTENT -->
<div class="main-content extra-club-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!-- END breadcrumbs -->
  <section class="section">
    <!-- Title -->
    <div class="section-left__title">
      <h4><span><?php echo str_replace('-', ' ', $this->controller_url)?></span></h4>
    </div>


    <!-- /Title -->
    <!-- CONTENT -->
    <?php edit($this->controller,'extra-club'); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content['extra-club']);?>
      <!-- /repeatable content -->
    </div>

<article class="text-partial">

<p>The EXTRAS never stop coming at Bingo Extra, and nowhere is this more evident than within our rewards programme! We have 5 VIP levels for you to climb and each brings you more and more perks! Check out the HUGE list below!</p>

<!-- <table class="responsive" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<th class="loyalty__col-heading">Benefits to enjoy</th>
<th class="bronze loyalty__col "><img src="/_images/common/icons/BE-newbie-icon.jpg"></th>
<th class="silver loyalty__col "><img src="/_images/common/icons/BE-bronze-icon.jpg"></th>
<th class="gold loyalty__col"><img src="/_images/common/icons/BE-silver-icon.jpg"></th>
<th class="ruby loyalty__col"><img src="/_images/common/icons/BE-gold-icon.jpg"></th>
<th class="emerald loyalty__col"><img src="/_images/common/icons/BE-ruby-icon.jpg"></th>
<th class="platinum loyalty__col"><img src="/_images/common/icons/BE-emerald-icon.jpg"></th>
</tr>
</tr>
<tr class="odd">
<td class="left-col-captions">1st, 2nd & 3rd Deposit Bonus</a></td>
<td class="bronze loyalty__col"><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></td>
<td class="silver loyalty__col"></td>
<td class="silver loyalty__col"></td>
<td class="gold loyalty__col"></td>
<td class="ruby loyalty__col"></td>
<td class="emerald loyalty__col"></td>
</tr>
<tr>
<td class="left-col-captions">Re-Deposit Bonus</td>
<td class="bronze loyalty__col"></td>
<td class="silver loyalty__col">25 % or 50%</td>
<td class="gold loyalty__col">25 % or 50%</td>
<td class="ruby loyalty__col">25 % or 50%</td>
<td class="emerald loyalty__col">25 % or 50%</td>
<td class="platinum loyalty__col">25 % or 50%</td></td>
</tr>
<tr class="odd">
<td class="left-col-captions">Daily Free Spins</td>
<td class="bronze loyalty__col"></td>
<td class="silver loyalty__col"></td>
<td class="gold loyalty__col">1</td>
<td class="ruby loyalty__col">2</td>
<td class="emerald loyalty__col">3</td>
<td class="platinum loyalty__col">4</td>
</tr>
<tr>
<td class="left-col-captions">Daily Free Cards</td>
<td class="bronze loyalty__col"></td>
<td class="silver loyalty__col"></td>
<td class="gold loyalty__col">1</td>
<td class="ruby loyalty__col">2</td>
<td class="emerald loyalty__col">3</td>
<td class="platinum loyalty__col">4</td>
</tr>
<tr class="odd">
<td class="left-col-captions">Upgrade Bonus</td>
<td class="bronze loyalty__col"></td>
<td class="silver loyalty__col"></td>
<td class="gold loyalty__col">£2</td>
<td class="ruby loyalty__col">£10</td>
<td class="emerald loyalty__col">£25</td>
<td class="platinum loyalty__col">£50</td>
</tr>
<tr>
<td class="left-col-captions">Birthday Bonus</td>
<td class="bronze loyalty__col"></td>
<td class="silver loyalty__col">10 Free Spins </td>
<td class="gold loyalty__col">£2 + 3 Spins</td>
<td class="ruby loyalty__col">£5 + 5 Spins</td>
<td class="emerald loyalty__col">£20</td>
<td class="platinum loyalty__col">£50</td>
</tr>
<tr class="odd">
<td class="left-col-captions">Dedicated VIP Manager</td>
<td class="bronze loyalty__col"></td>
<td class="silver loyalty__col"></td>
<td class="gold loyalty__col"></td>
<td class="ruby loyalty__col"></td>
<td class="emerald loyalty__col"><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></td>
<td class="platinum loyalty__col"><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></td>
</tr>
<tr>
<td class="left-col-captions">Withdrawal Pending Period</td>
<td class="bronze loyalty__col"><img src="/_images/common/icons/clock-48h.jpg"></td>
<td class="silver loyalty__col"><img src="/_images/common/icons/clock-48h.jpg"></td>
<td class="gold loyalty__col"><img src="/_images/common/icons/clock-36h.jpg"></td>
<td class="ruby loyalty__col"><img src="/_images/common/icons/clock-36h.jpg"></td>
<td class="emerald loyalty__col"><img src="/_images/common/icons/clock-24h.jpg"></td>
<td class="platinum loyalty__col"><img src="/_images/common/icons/clock-24h.jpg"></td>
</tr>
</tbody></table> -->


<!-- Swipe Left/Right Icon -->
      <div class="extra-club__table-arrows-icon">
        <img src="/_images/common/icons/touch-icon.png" alt="Left and Right Arrows">
      </div>

      <!-- LUCKY CLUB TABLE WRAP -->
      <div class="extra-club__table-wrap">


        <div class="extra-club__table">

          <!-- Touch Icon Inside Table -->
          <!-- <div class="extra-club__table-touch-icon">
            <img src="/_images/common/icons/touch-icon.png" alt="Touch">
          </div> -->


          <!-- Loyalty Icons Row --> 
          <div class="extra-club__table-row__header">
            <div class="extra-club__table-row__title">Benefits to enjoy</div>
            <ul class="extra-club__table-row__loyalty">
              <li><img src="/_images/common/icons/BE-newbie-icon.jpg" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/BE-bronze-icon.jpg" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/BE-silver-icon.jpg" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/BE-gold-icon.jpg" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/BE-ruby-icon.jpg" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/BE-emerald-icon.jpg" alt="Checkmark"></li>
            </ul>
          </div>

          <!-- 1st, 2nd & 3rd Deposit Bonus Row --> 
          <div class="extra-club__table-row__even">
            <div class="extra-club__table-row__title">1st, 2nd & 3rd Deposit Bonus</div>
            <ul class="extra-club__table-row__loyalty">
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
            </ul>
          </div>

          <!-- Daily Free Spins Row --> 
          <div class="extra-club__table-row">
            <div class="extra-club__table-row__title">Daily Free Spins</div>
            <ul class="extra-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>1</li>
              <li>2</li>
              <li>3</li>
              <li>4</li>
            </ul>
          </div>

          <!-- Daily Free Cards Row --> 
          <div class="extra-club__table-row__even">
            <div class="extra-club__table-row__title">Daily Free Cards</div>
            <ul class="extra-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>1</li>
              <li>2</li>
              <li>3</li>
              <li>4</li>
            </ul>
          </div> 

          <!--Upgrade Bonus --> 
          <div class="extra-club__table-row">
            <div class="extra-club__table-row__title">Upgrade Bonus</div>
            <ul class="extra-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
            </ul>
          </div> 


          <!-- Dedicated VIP Manager --> 
          <div class="extra-club__table-row__even">
            <div class="extra-club__table-row__title">Dedicated VIP Manager</div>
            <ul class="extra-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
            </ul>
          </div>


          <!-- Withdrawal Pending Period Row --> 
          <div class="extra-club__table-row">
            <div class="extra-club__table-row__title">Reduced Withdrawal Pending Period</div>
            <ul class="extra-club__table-row__loyalty">
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li>-</li>
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
              <li><img src="/_images/common/icons/checkmark.png" alt="Checkmark"></li>
            </ul>
          </div>

        </div>
      </div>
       <!-- /LUCKY CLUB TABLE WRAP -->


<p>The more you play, the more points you earn; the more points you earn the bigger the rewards! Plus, the higher up the VIP loyalty scheme you climb, the quicker and easier it is to earn your EXTRA Loyalty Points. Take a look:</p><br>


<table class="nonresponsive">
<thead>
<tr>
<td><strong>Game Type</strong></td>
<td><strong>Real Money Spent</strong></td>
<td><strong>Loyalty Points Earned</strong></td>
</tr>
</thead>
<tbody>
<tr>
<td><strong>Bingo</strong></td>
<td><strong>50p</strong></td>
<td><strong>1 point</strong></td>
</tr>
<tr>
<td><strong>Slots & Scratchcards</strong></td>
<td><strong>£1</strong></td>
<td><strong>1 point</strong></td>
</tr>
</thead>
<tbody>
<tr>
<td><strong>Table & Card Games</strong></td>
<td><strong>£2</strong></td>
<td><strong>1 point</strong></td>
</tr>
</tbody>


<table class="nonresponsive">
<thead>
<tr>
<td><strong>Player Status</strong></td>
<td><strong>Extra Loyalty Points to Bonus</strong></td>
</tr>
</thead>
<tbody>
<tr>
<td><strong>All Players</strong></td>
<td><strong>1,000 points = £1</strong></td>
</tr>
<tr>
<td><strong>VIP Ruby & Emerald</strong></td>
<td><strong>1,000 points = £1.50</strong></td>
</tr>
</tbody>
</table>

<div class="Loyalty_textBox">
To convert your points, head on down to "My Account" and click Convert Points (Tip: next to your Points balance!) - once you have a minimum of 1,000 points you can convert the points into bonus money to play with!
</div>

<H3>Benefits to enjoy</H3>

<p><b>Deposit Bonus</b>
<i>On your 1st deposit pick a 100% bingo bonus or 100% slots bonus.</i></p>

<p><b>Daily Free Spins</b>
<i>Simply deposit and wager to receive free spins every day for the next 7 days. Use it or lose it!</i></p>

<p><b>Daily Free Cards</b>
<i>Simply deposit and wager to receive free cards worth 5p each every day for 7 days. Use them or lose them!</i></p>

<p><b>Upgrade Bonus</b>
<i>Each time you climb a tier, from Gold and above in the Extra Club you will be awarded an upgrade bonus.</i></p>

<p><b>Withdrawal Pending Period</b>
<i>The higher you climb up the tiers of the Extra Club, the shorter your pending withdrawal period will be. Get your request processed quicker!</i></p>

<dl class="promotion__terms-conditions accordion" data-accordion="">
                <dd class="accordion-navigation active">
                <a href="#panelTC">Terms &amp; Conditions</a>
                <div id="panelTC" class="content active">
                  <p>&nbsp;</p>
<ol>
<li>This Loyalty Scheme is only valid for real money accounts on Bingo Extra. Play for fun accounts, will not be eligible for any part of the loyalty promotions.</li>
<li>The participation in the loyalty program is with invite only.</li>

<li>The VIP Upgrade Bonus will be issued upon reaching a new level, this will have to be claimed once logged in within 7 days of being upgraded. Gold players receive a bingo bonus subject to 4x wagering and Ruby and Emerald players receive a slot bonus with a 25x wagering.</li>
<li>By accepting a bonus, the player automatically accepts the terms &amp; conditions of the bonus credited.</li>
<li>Bonus money can only be played on bingo games and carries a 4x wager requirement unless specified otherwise. Bets placed on bingo will count 150% towards meeting the requirements. For example: a wager of £1 counts £1.50 towards wager requirement.</li>
<li>The bonus is for play purposes only until wagering requirements have been met. The bonus and bonus wins will be converted to real upon completion of wagering requirements.</li>
<li>If you request a withdrawal prior to having reached the minimum wagering requirements, your bonus and winnings will be declared null &amp; void and all active bonuses will automatically be removed from the account.</li>
<li>Cards bought at sale price will remain at the original card value.</li>
<li>Free cards have no cash equivalent.</li>
<li>Winnings from free cards will be in bonus funds.</li>
<li>Free spins and cards will expire after 24 hours. All standard free spins T&amp;Cs apply. Maximum convertible amount is £5</li>
<li>Players must deposit and wager a minimum of £10 on that day, for all freebies (cards &amp; Spins).</li>
<li>All freebies (cards &amp; Spins) cannot be accumulated.</li>
<li>Withdrawal pending period times are valid from Monday to Friday only. *Exclusions apply.</li>
<li>Loyalty Points will only be earned on real money wagers, not bonus money wagers.</li>

<li>Loyalty Points generated through play can be converted into bonus funds at the following rates based on a player's VIP level

<ol>
<li>Ruby and Emerald VIP players can convert their points at a rate of 1,000 = £1.50</li>
<li>All other VIP levels can convert their points at a rate of 1,000 = £1</li>
</ol>
</li>

<li>The bonus awarded from a Loyalty points conversion, has a wagering requirement of 10x its value and can be used on any bingo or exclusive slot game (excluding progressive games)</li>
<li>A player can convert once they have 1,000 points upwards.</li>
<li>All standard site terms &amp; conditions apply.</li>
<li> We reserve the right to alter these terms &amp; conditions at any time.</li>
<li>Management decision is final.</li>
<li>All Upgrades and Downgrades within the Loyalty club are subject to management’s discretion and the company reserves the right to change a player’s level with no correspondence.</li>
<li>General Promotional Terms &amp; Conditions apply. For more details <a href="../terms-and-conditions/general-promotional-terms-and-conditions/" class="terms-link">click here</a></li>
</ol>              </div>
                </dd>
              </dl>

</article>



</div>
<!-- END MIDDLE CONTENT