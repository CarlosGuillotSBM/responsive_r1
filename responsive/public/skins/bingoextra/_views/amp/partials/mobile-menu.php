<amp-sidebar id="sidebar" layout="nodisplay" class="mobile-menu"  side="right">
	<div class="mobile-menu-top-area">
		<div class="mobile-menu-top-area__row">
			<div class="mobile-menu-top-area__join">
				<div class="inner">
					<span>BINGO EXTRA MENU</span>
				</div>
			</div>
			
		</div>
	</div>
	<div class="mobile-menu-lists ripple">
		<!-- menu lists -->
		<ul class="mobile-menu-list">
			<!-- section title -->
			<li class="mobile-menu-section-title">Bingo</li>
			<li class="mobile-menu-item bingo-link">
				<a class="menu-link" href="/bingo-schedule/" target="_top" title="New Games">Bingo</a>
			</li>
			<!-- section title -->
			<li class="mobile-menu-section-title">Games</li>
			<!-- menu items-->
			<!-- commented out search link while feature is finalized -->
			<li class="mobile-menu-item gamesearch-link">
				<a id="ui-search-screen" class="menu-link" target="_top" title="New Games">Search Games</a>
			</li>
			<li class="mobile-menu-item newgames-link">
				<a class="menu-link" href="/slots/new/" target="_top" title="New Games">New Games</a>
			</li>
			<li class="mobile-menu-item slots-link">
				<a id="ui-slots-menu-link">Slots </a><span class="slots-categories">
				<span id="UI-id">Categories
					<svg class="chevron-icon" width="11px" height="7px" viewBox="0 0 11 7" version="1.1" xmlns="http://www.w3.org/2000/svg">
						<path d="M5.5,6.34852814 L0.651471863,1.5 L1.5,0.651471863 L5.5,4.65147186 L9.5,0.651471863 L10.3485281,1.5 L5.5,6.34852814 Z" fill="#ffffff">
						</path>
					</svg>
				</span>
			</span>
		</li>
		
		<li class="mobile-menu-item scratchcards-link">
			<a class="menu-link" href="/scratch-and-arcade/" target="_top" title="Scratchcards">SCRATCH &amp; ARCADE</a>
		</li>
		<li class="mobile-menu-item tablecard-link">
			<a class="menu-link" href="/table-card/" target="_top" title="Table &amp; Card">Table &amp; Card</a>
		</li>
		
		<li class="mobile-menu-item allgames-link">
			<a class="menu-link" href="/games/" target="_top" title="All Games">All Games</a>
		</li>
		<li class="mobile-menu-section-title">Rewards</li>
		<li class="mobile-menu-item promotions-link">
			<a class="menu-link" href="/promotions/" target="_top" title="Promotions">Promotions</a>
		</li>
		<li class="mobile-menu-item loyalty-link">
			<a class="menu-link" href="/extra-club/" target="_top" title="Extra Club">Extra Club</a>
		</li>
		<!-- section title -->
		<li class="mobile-menu-section-title">About</li>
		<li class="mobile-menu-item community-link">
			<a class="menu-link" href="/community/" target="_top" title="Community">Community</a>
		</li>
		<li class="mobile-menu-item support-link">
			<a class="menu-link" href="/support/" target="_top">Support</a>
		</li>
		<li class="mobile-menu-item faq-link">
			<a class="menu-link" href="/faq/" target="_top" title="Faq">Faq</a>
		</li>
		<li class="mobile-menu-item terms-link">
			<a class="menu-link" href="/terms-and-conditions/" target="_top" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
		</li>
		<li class="mobile-menu-item about-us-link">
			<a class="menu-link" href="/about-us/" target="_top" title="About Us">About Us</a>
		</li>
		<li class="mobile-menu-item privacy-link">
			<a class="menu-link" href="/privacy-policy/" target="_top" title="Privacy Policy">Privacy Policy</a>
		</li>
		<li class="mobile-menu-item responsiblegaming-link">
			<a class="menu-link" href="/responsible-gaming/" target="_top" title="Responsible Gaming">Responsible Gaming</a>
		</li>
		<li class="mobile-menu-item">
			<!-- spacer to move up logout so its not touching bottom -->
		</li>
		<!-- /menu items-->
	</ul>
</div>
<!-- / menu lists -->
</amp-sidebar>