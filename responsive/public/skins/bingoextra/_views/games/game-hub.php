<?php
$titleHeaderHeight = 25;
$slideHeight = 95;
$slidesToShow = 3;
?>
<script type="application/javascript">
skinPlugins.push({
id: "ProgressiveSlider",
options: {
slidesToShow: <?php echo $slidesToShow ?>,
mode: "vertical"
}
});
skinPlugins.push({
id: "WinnersSlider",
options: {
slidesToShow: <?php echo $slidesToShow ?>,
mode: "vertical"
}
});
</script>
<!-- single post main content -->
<div class="main-content game-seo-url">
    <!-- home breadcrumbs -->
    <?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
    <!-- /home breadcrumbs -->

    <!-- content -->
    <div class="two-columns-content-wrap ">
            <h1 class="section__title">
        
                   <!-- Editable Title -->
            <?php edit($this->controller,'editable-title'); ?>
            <?php @$this->repeatData($this->content['editable-title']);?>
            <!-- /Editable Title -->
            <!-- prog jackpots -->
        </h1>
                   <!-- Editable Title -->
            <?php edit($this->controller,'editable-title'); ?>
            <?php @$this->repeatData($this->content['editable-title']);?>
            <!-- /Editable Title -->
     
        <div class="two-columns-content-wrap__left">

<!-- Provider Hub -->
<section class="section">

    <div class="section__header">
  <h1 class="section__title"><a href="/IGT/">IGT <?php echo $this->controller_url ?> <span>View All</span></a></h1>
    </div>
    <!-- Hub grid -->
    <?php
    $hub = 'igt-slots';
    include "_global-library/partials/hub/hub-grid-game-seo-url.php" ?>
    <!-- /Hub grid -->
</section>
<!-- /Provider Hub -->
<div class="clearfix"></div>


<!-- Provider Hub -->
<section class="section">

    <div class="section__header">
          <h1 class="section__title"><a href="/MICROGAMING/">Microgaming <?php echo $this->controller_url ?> <span>View All</span></a></h1>
    </div>
    <!-- Hub grid -->
    <?php
    $hub = 'microgaming-slots';
    include "_global-library/partials/hub/hub-grid-game-seo-url.php" ?>
    <!-- /Hub grid -->
</section>
<!-- /Provider Hub -->
<div class="clearfix"></div>


<!-- Provider Hub -->
<section class="section">

    <div class="section__header">
        <h1 class="section__title"><a href="/NETENT/">NETENT <?php echo $this->controller_url ?> <span>View All</span></a></h1>

    </div>
    <!-- Hub grid -->
    <?php
    $hub = 'netent-slots';
    include "_global-library/partials/hub/hub-grid-game-seo-url.php" ?>
    <!-- /Hub grid -->
</section>
<!-- /Provider Hub -->
<div class="clearfix"></div>
        <!-- seo content -->
        <?php edit($this->controller,'seo-content'); ?>
        <?php @$this->getPartial($this->content['seo-content'],1); ?>
        <!-- seo content -->
        </div>
        <!--  /left column -->
        <!-- right column -->
        <div class="two-columns-content-wrap__right">
               <!-- Aquisition banner -->
            <?php edit($this->controller,'aquisition-banner'); ?>
            <?php @$this->repeatData($this->content['aquisition-banner']);?>
            <!-- /Aquisition banner -->
     <!-- prog jackpots -->
            <div class="progressive-jackpots__wrapper slider">
                <img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
                <article class="progressive-jackpots" style="height: <?php echo ($slidesToShow * $slideHeight + $titleHeaderHeight). 'px' ?> ; overflow: hidden">
                    
                    <div class="progressive-jackpots__title">
                        <h1>Progressive Jackpots</h1>
                    </div>
                    <?php edit($this->controller,'progressives'); ?>
                    <?php @$this->repeatData($this->content['progressives']);?>
                </article>
            </div>
            <!-- / prog jackpots -->
            <!-- yesterdays winning -->
            <div class="yesterdays-win__wrapper">
                <img class="hub-shape-holder" src="/_images/common/4x3-bg.png">
                <article class="yesterdays-win">
                    <div class="yesterdays-win__title">
                        <h1>Yesterdays Total Winnings</h1>
                    </div>
                    <div class="yesterdays-win__box">
                        <div class="yesterdays-win__box__amount"><span>&pound;<?php echo @$this->winners_total[0]['Win'];?></span></div>
                    </div>
                </article>
            </div>
            <!--/ yesterdays winning -->
        </div>
        <!-- /right column -->
        
    </div>
    <!-- /content  -->
</div>
<!-- /single post main content -->