<!--games content -->
<div class="main-content games-content">
	<!-- home breadcrumbs -->
	<?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
	<!-- /games content -->

	<?php
		// the hub data is common for all the same controller
		// special exception for exclusive slots so it can have it's own hub
		$page = $this->controller;
		if($this->action == 'exclusive') $page.= 'exclusive';

	?>
	<!-- banner -->	
	<div class="games-content__banner ui-scheduled-content-container">
		<!-- Edit point Banner -->
		<?php edit($this->controller,'games-latest-offer'); ?>
		<?php @$this->repeatData($this->content['games-latest-offer']); ?>
	</div>


	<section class="section middle-content__box games-section">
		<!-- games menu -->
		<?php include '_partials/games-menu/games-menu.php'; ?>

		<!-- /games menu -->
		<?php include '_global-library/partials/games/games-list.php'; ?>
		<!-- end games list -->

		<div class="bottom-content">
			<?php
				if ($this->action != "slots") {
					edit($this->controller.$this->action,'footer-seo', $this->action);
					@$this->getPartial($this->content['footer-seo'], 1); 
				} else {
					edit($this->controller,'footer-seo',$this->action);
					@$this->getPartial($this->content['footer-seo'],$this->action); 
				}
			?>
		</div>
	</section>
	<!-- BOTTOM CONTENT -->
</div>


