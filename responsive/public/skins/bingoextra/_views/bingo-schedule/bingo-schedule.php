<!-- bingo content -->
<div class="main-content bingo-content">
  <!-- home breadcrumbs -->
  <?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>

 <div class="content-holder bingo-schedule-holder">
  <?php include "_global-library/partials/bingo/bingo-schedule.php" ?>
  </div>

</div>
<!-- /main content -->
