<!-- MIDDLE CONTENT -->
<div class="middle-content">

  <div class="middle-content__box">
    <section class="section archive">
      <!-- Archive CATEGORY TABS -->
      <dl class="archive-tabs">
        <dd class="first <?php echo ($this->action == 'promotions') ? 'active' : '';?>"><a data-hijack="true" href="/archive/promotions/">PROMOTIONS</a></dd>
        <dd class="<?php echo ($this->action == 'blog') ? 'active' : '';?>"><a data-hijack="true" href="/archive/blog/">BLOG</a></dd>
        <dd class="<?php echo ($this->action == 'winners') ? 'active' : '';?>"><a data-hijack="true" href="/archive/winners/">WINNERS</a></dd>
        <dd class="<?php echo ($this->action == 'games') ? 'active' : '';?>"><a data-hijack="true" href="/archive/games/">GAMES</a></dd>
      </dl>
      <!-- END Archive  CATEGORY TABS -->
      
      <div class="content-template">
        <!-- Archive List Content -->
        <div class="archive__wrapper">
          <div class="archive__wrapper__left">
            <!-- make this this left year block dymanic but keeping div structure intact -->
            <!--  year blocks need making into accordian buttons that hide and show the div with the panel id -->
            <?php
            $currentMonth = '';
            foreach($this->dates as $row=>$value){?>
            <ul class="archive-accordion">
              <li class="archive-accordion__navigation">
                <!-- active class needs adding to an a below  if this is current year -->
                <a href="#panel-<?php echo $row;?>" class="active"><?php echo $row;?></a>
                <div id="panel-<?php echo $row;?>" class="archive-accordion__content">
                  <ul>
                    <?php
                    foreach($value as $item){
                    // remember selected for the title.
                    if($item['Selected'] == 'active') $currentMonth = $item['DateString'].' '.$row;?>
                    
                    <li class='<?php echo $item['Selected'];?>'><a data-hijack="true" href="<?php echo $item['url'];?>"><?php echo $item['DateString'];?></a></li>
                    
                    <?php }?>
                  </ul>
                </div>
              </li>
            </ul>
            
            <?php }?>
          </div>
          <div class="archive__wrapper__right">
            <?php
            switch($this->action)
            {
            case 'promotions': edit('promotions','promotions'); break;
            case 'winners': edit('lounge','winners'); break;
            case 'blog': edit('lounge','blog'); break;
            case 'tv': edit('lounge','tv'); break;
            case 'tips': edit('lounge','tips'); break;							
            }			
			
			$archivehack = str_replace('games','games-detail',$this->action);						
            ?>
            <div class="archive__wrapper__right__title">

            
              <h1>ARCHIVED <?php echo strtoupper($this->action).' : '.strtoupper($currentMonth);?></h1>
              <!-- make title dymanic bleow leaving h1 and dic structure -->
             
            <!-- </div> -->
            <div class="archive__wrapper__right__inner">

             

              <?php @$this->repeatData($this->content[$archivehack],0,'_global-library/_editor-partials/archive-text-and-image-left.php');?>

            </div>
          </div>
        </div>
        
        <!-- End Archive List Content -->
      </div>
      <!-- /two columns -->
    </section>
  </div>
  <!-- /CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->

<!-- BREADCRUMBS  -->
<div class="footer-breadcrumbs">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
</div>