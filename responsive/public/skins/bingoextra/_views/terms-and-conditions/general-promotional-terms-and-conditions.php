<!-- MIDDLE CONTENT -->
<div class="main-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!-- END breadcrumbs -->
  <section class="section">
    <h1 class="section__title">General Promotional Terms &amp; Conditions</h1>
    <!-- CONTENT -->
    <?php edit($this->controller,$this->action); ?>
    <div class="content-template">
      
      <!-- repeatable content -->
      <?php @$this->repeatData($this->content[$this->action]);?>
      <!-- /repeatable content -->
    </div>
    <!-- /CONTENT-->
  </section>
</div>
<!-- END MIDDLE CONTENT -->