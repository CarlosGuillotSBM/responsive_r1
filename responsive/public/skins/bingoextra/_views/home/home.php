<div class="main-content home">
	<!-- home breadcrumbs -->
	<?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>
	<!-- /home breadcrumbs -->
	<!-- main content top -->
	<div class="main-content__top">
		<?php include "_partials/home-slider/home-slider.php"; ?>
		<!-- End slider area -->
	</div>
	<!-- /main content top -->
	<div class="clearfix"></div>
		<!-- Featured Games Holder Row -->
	<?php  include'_partials/home-featured-games/home-featured-games-holder.php'; ?>
	<!-- end Featured Games Holder Row -->
	<div class="clearfix"></div>
	<!-- Bingo Schedule Holder Row -->
	<div class="mobile-hide-bingo">
		<?php include "_partials/bingo-schedule/bingo-schedule-holder-home.php"; ?>
	</div>
	<div class="clearfix"></div>
	<div class="mobile-show-bingo">
		<h1>BINGO SCHEDULE</h1>
		<a href="/bingo-schedule/"><img class="mobile-bingo-room-cta" src="/_images/mobile-bingo-room-cta/bingo-schedule-cta.jpg" alt="View Bingo Schedule" ></a>
	</div>
	
	<div class="clearfix"></div>
	
	

	<!-- Community Section Holder Row -->
	<?php include'_partials/home-community-section/home-community-holder.php'; ?>
	<!-- end F Community Section Holder Row -->
	<div class="clearfix"></div>
	<!-- SEO AREA -->
	<?php include'_partials/home-seo-section/home-seo-holder.php'; ?>
	<!-- end F Community Section Holder Row -->
	<!-- /main content games hub -->
</div>
<!-- /main content -->