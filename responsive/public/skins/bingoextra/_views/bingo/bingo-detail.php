<script type="application/javascript">
  skinModules.push({ id: "BingoRoomDetailsPage", options: { roomId: <?php echo @$this->bingo["Text1"]; ?> } });
</script>

<!-- MAIN CONTENT AREA -->
<div class="bingo-details-page">
  <!-- TWO COLUMNS LAYOUT -->
  <div class="middle-content two-columns-content-wrap">
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!-- LEFT COLUMN -->
    <div class="two-columns-content-wrap__left">
      <!-- GAME DETAILS  -->
      <!--
      $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      $$$$ SEO MICRO DATA FOR THIS PARTIAL
      $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      Name: itemprop="name"
      Image: itemprop="image"
      URL: itemprop="url"
      Screenshot: itemprop="screenshot"
      -->
      <div itemscope itemtype="http://schema.org/SoftwareApplication">
        
        <!-- Title -->
        <div>
          <h1 class="section__title"><?php echo @$this->bingo['Title'];?></h1>
        </div>
        <!-- /Title -->
        <div class="bingo-detail-content">
          
          <div class="bingo-detail-content__left">
            <div class="overlay-holder">
              <span class="games-info__try">
              <div style="display: none; cursor: pointer" class="overlay">
                <img alt="Play Now" src="/_images/common/play-icons/play-now.png" class="play-now-button">
              </div>
              <div style="display: none; cursor: pointer"
                class="overlay">
                <img alt="Try Game" src="/_images/common/play-icons/try-game.png" class="play-now-button">
              </div>
              <img alt="<?php echo @$this->bingo['Alt'];?>" itemprop="screenshot" src="<?php echo @$this->bingo['Image'];?>" />
              </span>
            </div>
            
          </div>
          
          
          <div class="bingo-detail-content__right">
            <aside class="bingo-detail__wrap">
              <h3>Room Details</h3>
              <ul style="display: none" data-bind="visible: BingoRoomDetailsPage.room().id" class="bingo-detail-content__right__game-table">
                <li><span class='bingo-detail-content__right__game-table__title'>Jackpot Amount</span><span data-bind="text: BingoRoomDetailsPage.room().displayJackpot" itemprop="publisher"></span></li>
                <li><span class='bingo-detail-content__right__game-table__title'>Game Type</span><span data-bind="text: BingoRoomDetailsPage.room().displayGameType" itemprop="publisher"></span></li>
                <li><span class='bingo-detail-content__right__game-table__title'>Starting in</span><span data-bind="text: BingoRoomDetailsPage.room().displaySeconds" itemprop="publisher"></span></li>
                <li><span class='bingo-detail-content__right__game-table__title'>Players</span><span data-bind="text: BingoRoomDetailsPage.room().players" itemprop="publisher"></span></li>
                <li><span class='bingo-detail-content__right__game-table__title'>Card Price</span><span data-bind="text: BingoRoomDetailsPage.room().cardPrice" itemprop="publisher"></span></li>
              </ul>

              <ul style="display: none" data-bind="visible: !BingoRoomDetailsPage.room().id" class="bingo-detail-content__right__game-table">
                <li>Opening Soon</li>
              </ul>

            </aside>
            
          </div>
          <!-- ACQUISITION CONTENT -->
          <div class="bingo-detail-content__acquisition">
              <?php edit('bingodetail','bingo-detail__acquisition',@$this->row['DetailURL']); ?>
              <?php @$this->repeatData($this->content['bingo-detail__acquisition']); ?>
			  <!--
              <article class="text-partial">
                <h1>AQ Text</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque dictum odio non pulvinar.</p>
              </article>
			  -->
              <div style="display: none" data-bind="visible: !validSession()" class="bingo-detail-content__acquisition__cta">
                <div>
                  <div class="cta-login" data-reveal-id="login-modal"><a>LOGIN</a></div>
                </div>
                <div>
                  <div class="cta-join"><a data-nohijack="true" href="/register/">Join Now</a></div>
                </div>
              </div>

            <div style="display: none" data-bind="visible: validSession()" class="bingo-detail-content__acquisition__cta">
              <div>
                <div data-bind="click: BingoRoomDetailsPage.openRoom.bind($data,'<?php echo @$this->bingo["Text1"];?>', '<?php echo @$this->bingo['Image'];?>')" class="cta-join" data-infopage="true"><a>PLAY</a></div>
              </div>
            </div>


          </div>
          <!-- ACQUISITION CONTENT -->
        </div>

      </div>
      <!-- /GAME DETAILS -->
      <!-- END MIDDLE CONTENT -->
      <!-- BOTTOM CONTENT -->
      <div class="clearfix"></div>
      <div class="games-detail-page__bottom">
        <!-- ARTICLE -->	  
		    <?php edit('bingodetail','bingo-detail__article',$this->subaction); ?>
            <?php @$this->repeatData($this->content['bingo-detail__article']); ?>
        <!-- /ARTICLE -->
      </div>
    </div>
    <!-- /LEFT COLUMN -->
    <!-- RIGHT COLUMN -->
    <div class="two-columns-content-wrap__right">
		<?php edit('bingodetail','bingo-detail__side-content',$this->subaction); ?>
		<?php @$this->repeatData($this->content['bingo-detail__side-content']); ?>
		<div class="clearfix"></div>
    </div>
    <!-- /RIGHT COLUMN -->
  </div>
  <!-- /TWO COLUMNS LAYOUT -->
</div>
<!-- /MAIN CONTENT AREA -->
