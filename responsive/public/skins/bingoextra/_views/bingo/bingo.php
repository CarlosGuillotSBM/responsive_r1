<!-- bingo content -->
<div class="main-content bingo-content">
  <!-- home breadcrumbs -->
  <?php include "_global-library/widgets/breadcrumbs/breadcrumbs.php" ?>


    <article class="bingo-hub">
  <!-- bingo content part1 -->
  <div class="bingo-content__part1">
      <div class="section__header section-full-width__title">
          <?php edit($this->controller,'bingo-top-title'); ?>
      <h4><span><?php @$this->getPartial($this->content['bingo-top-title'],1); ?></span></h4>
  </div>
    <div class="bingo-content__part1__left">
        <?php edit($this->controller,'bingo-featured'); ?>
      <?php @$this->getPartial($this->content['bingo-featured'],1); ?>
    </div>
    <div class="bingo-content__part1__center">
      <?php @$this->getPartial($this->content['bingo-featured'],2); ?>
    </div>
    <div class="bingo-content__part1__right">
      <?php @$this->getPartial($this->content['bingo-featured'],3); ?>
    </div>
  </div> 
  <!-- /bingo content part1 -->

  <!-- bingo list 90 ball-->
  <section class="section middle-content__box bingo-section">  
    <!-- end bingo list -->
      <?php edit($this->controller,'90-ball-title'); ?>
      <?php if($this->action == 'bingo' || $this->action == '90-ball'):?>
  <div class="section__header section-full-width__title">

      <h4><span><?php @$this->getPartial($this->content['90-ball-title'],1); ?></span></h4>
  </div>      
      <!-- Hub grid -->
      <?php     
	  $hub = '90-ball';	
      include "_global-library/partials/hub/hub-grid-bingo.php";
      ?>
  </section>
  <?php endif;?>

  
  <?php if($this->action == 'bingo' || $this->action == '75-ball'):?>
  <!-- bingo list 75 ball-->
  <section class="section middle-content__box bingo-section">
  <div class="section__header section-full-width__title">
      <?php edit($this->controller,'75-ball-title'); ?>
      <h4><span><?php @$this->getPartial($this->content['75-ball-title'],1); ?></span></h4>
  </div>
          
      <!-- Hub grid -->
      <?php      
	  $hub = '75-ball';
      include "_global-library/partials/hub/hub-grid-bingo.php";
      ?>
  </section>
  <?php endif;?>



  <?php if($this->action == 'bingo' || $this->action == '5-line'):?>
  <!-- 5 line bingo rooms-->
  <section class="section middle-content__box bingo-section">      
  <!-- end bingo list -->
  <div class="section__header section-full-width__title">
      <?php edit($this->controller,'5-line-bingo-title'); ?>
      <h4><span><?php @$this->getPartial($this->content['5-line-bingo-title'],1); ?></span></h4>
  </div>
          
      <!-- Hub grid -->
      <?php      
	  $hub = '5-line-bingo';
      include "_global-library/partials/hub/hub-grid-bingo.php";
      ?>
  </section>
  <?php endif;?>


  <?php if($this->action == 'bingo' || $this->action == 'pre-buy'):?>
  <!-- bingo list PRE BUY ball-->
  <section class="section middle-content__box bingo-section">  
  <!-- end bingo list -->

  <div class="section__header section-full-width__title">
      <?php edit($this->controller,'pre-buy-title'); ?>
      <h4><span><?php @$this->getPartial($this->content['pre-buy-title'],1); ?></span></h4>
  </div>
          
      <!-- Hub grid -->
      <?php      
	  $hub = 'pre-buy';
      include "_global-library/partials/hub/hub-grid-bingo.php";
      ?>
  </section>
 <?php endif;?>



 


  <section class="section middle-content__box bingo-section">
		<?php edit($this->controller,'footer-'.$this->action); ?>
		<?php @$this->repeatData($this->content['footer-'.$this->action],1); ?>
  </section>

  </article>

</div>
<!-- /main content -->
