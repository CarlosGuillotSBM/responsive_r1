<?php

$path = @parse_url($_SERVER["REQUEST_URI"],PHP_URL_PATH);
$queryString = (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) )? '?'.$_SERVER['QUERY_STRING'] : '';

if(preg_match("#^\/giveaway\/$#", $path ,$matches) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/great-british-giveaway/". $queryString);
    exit;
}

if(preg_match("#^\/50k\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/50k/". $queryString);
    exit;
}

// DEV-10401

if (preg_match("#^\/play\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/play/". $queryString);
    exit;
}

if (preg_match("#^\/spins\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/spins/". $queryString);
    exit;
}


if (preg_match("#^\/claim\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/claim/". $queryString);
    exit;
}


if (preg_match("#^\/free\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: /media/exclusive/free/". $queryString);
    exit;
}

// 10983
if (preg_match("#^\/advent-1\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-1/". $queryString);
    exit;
}
if (preg_match("#^\/advent-4\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-4/". $queryString);
    exit;
}
if (preg_match("#^\/advent-6\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-6/". $queryString);
    exit;
}
if (preg_match("#^\/advent-8\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-8/". $queryString);
    exit;
}
if (preg_match("#^\/advent-13\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-13/". $queryString);
    exit;
}
if (preg_match("#^\/advent-15\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-15/". $queryString);
    exit;
}
if (preg_match("#^\/advent-18\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-18/". $queryString);
    exit;
}
if (preg_match("#^\/advent-22\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-22/". $queryString);
    exit;
}
if (preg_match("#^\/advent-25\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/advent-25/". $queryString);
    exit;
}

//DEV-11246
if (preg_match("#^\/sweetheart-surprises\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/sweetheart-surprises/". $queryString);
    exit;
}

if(preg_match("#^\/media/exclusive/as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

if(preg_match("#^\/media/ppc/as-seen-on-tv\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}

if(preg_match("#^\/media/exclusive/twinkle\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/sweetheart-surprises/". $queryString);

    exit;
}   

if(preg_match("#^\/media/ppc/twinkle\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}   

if(preg_match("#^\/media/ppc/welcome-offer\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /". $queryString);

    exit;
}   

//R10-204
if (preg_match("#^\/facebook\/$#", $path) == 1)
{
    header('HTTP/1.1 302 Moved Temporarily');
    header("Location: https://www.facebook.com/bingoextra/". $queryString);
    exit;
}

//R10-472
if (preg_match("#^\/register/twinkle\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /register/". $queryString);
    exit;
}

//R10-982
if (preg_match("#^\/tv.html\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /community/tv/". $queryString);
    exit;
}

if (preg_match("#^\/slots.html\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /slots/". $queryString);
    exit;
}

if (preg_match("#^\/promotions.html\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/". $queryString);
    exit;
}
//R10-1009
if (preg_match("#^\/excluded\/$#", $path) == 1)
{
    header('HTTP/1.1 301 Moved Permanently');
    header("Location: /promotions/excluded/". $queryString);
    exit;
}