<script type="application/javascript">
skinPlugins.push( {
id: "Slick",
options: {
global: true,
element: 'bxslider-home-home'
}
} );
</script>
<!-- slider area -->
<?php include "_partials/common/slider.php"; ?>
<!-- End slider area -->
<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <!-- home container -->
    <div class="home-container">
        <!-- MAIN CONTENT AREA -->
    <?php if (config("Device") == 2 || config("Device") ==3):
                @$this->repeatData($this->content['hero']); 
    endif;
    ?>

        <!-- games menu -->
        <?php include '_partials/games-menu/games-menu.php'; ?>
        <!-- end games menu -->

        <!-- VIP FAVOURITES -->
        <section class="section middle-content__box games-list home-games">
            <?php edit($this->controller,'hub'); ?>
            <div class="section__header">
                <h1 class="section__title">LUCKY VIP FAVOURITES</h1>
            </div>
            
            <?php include "_global-library/partials/hub/hub-grid-4-equal-columns-2-rows.php" ?>
            <div class="clearfix"></div>
        </section>
        <!-- /VIP FAVOURITES -->
        <!-- NEW GAMES -->
        <section class="section middle-content__box games-list home-games new-games">
            <?php edit($this->controller,'new-games'); ?>
            <div class="section__header">
                <h1 class="section__title">LUCKY VIP NEW GAMES</h1>
            </div>
            
            <!-- Hub grid -->
            <?php
                $hub = 'new-games';
                include "_global-library/partials/hub/hub-grid-4-equal-columns-2-rows.php"
            ?>
            
            <!-- /Hub grid -->
        </section>
        <!-- /NEW GAMES -->
        <!-- LIVE CASINO -->
        <section class="section middle-content__box games-list home-games">
            <?php edit($this->controller,'live-casino'); ?>
            <div class="section__header">
                <h1 class="section__title">LIVE DEALER</h1>
            </div>
            
            <?php
                $hub = 'live-casino';
                include "_global-library/partials/hub/hub-grid-4-equal-columns-2-rows.php"
            ?>
            <div class="clearfix"></div>
        </section>
        <!-- /LIVE CASINO -->
        <script type="application/javascript">
            skinPlugins.push({
                    id: "ProgressiveSlider",
                    options: {
                    slidesToShow: 1000,
                    mode: "vertical"
                }
            });
            skinPlugins.push({
                id: "Slick",
                options: {
                    slideWidth: 350,
                    responsive: true,
                    minSlides: <?php if (config("RealDevice") == 1) echo 3; else if (config("RealDevice") == 2) echo 2; else echo 1;?>,
                    maxSlides: 10,
                    slideMargin: 10,
                    auto: true,
                    infinite: false,
                    element: "bingo-progressives-slider",
                    pager: false
                }
            });      
        </script>

        <section class="section middle-content__box games-list home-games new-games">
            <div class="section__header">
                <h1 class="section__title">PROGRESSIVE JACKPOTS</h1>
            </div>
            <!-- bingo-progressives ticker -->
            <div class="content-holder__ticker">
                <?php edit($this->controller,'home-progressives'); ?>
                <ul class="bingo-progressives-slider progressive-jackpots">
                    <?php @$this->repeatData($this->content['home-progressives'], 0, "_partials/home/home-progressives-slide.php");?>
                </ul>
            </div>
        </section>

        <!-- SEO CONTENT -->
        <section class="section middle-content__box">
            <!-- content -->
            <?php if(config("Device") == 1) {
                edit($this->controller,'home-seo-content');
                @$this->getPartial($this->content['home-seo-content'],1);
            } else {
                $seoKey = "home-seo-content";
                include'_global-library/partials/home-seo/mobile.php';
            }?>
            <!-- /content -->
        </section>
        <!-- /SEO CONTENT -->
    </div>
    <!-- end home container -->
</div>
<!-- END MIDDLE CONTENT -->