<?php
// the hub data is common for all the same controller
// special exception for exclusive slots so it can have it's own hub
$page = $this->controller;
if($this->action == 'exclusive') $page.= 'exclusive';
?>
<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->

    <!-- Banner Offer -->
    <?php edit($this->controller,'games-latest-offer'); ?>
    <div class="games-content__banner ui-scheduled-content-container spacing-bottom">
        <?php @$this->repeatData($this->content['games-latest-offer']); ?>
    </div>

    <div class="games-pages">
        <!-- Hub grid -->
        <section  class="section games-list live-casino-list middle-content__box" id="live-dealer">
            <?php edit($page,'acquisition-title');?>
            <?php @$this->getPartial($this->content['acquisition-title'], 1); ?>

            <?php edit($page,'acquisition');
                if(is_array(@$this->content['acquisition']) && count(@$this->content['acquisition']) > 0):
            ?>

            <div class=" games-list middle-content__box top-games-hub">
                <?php
                $hub = 'acquisition';
                include "_global-library/partials/hub/hub-grid-9.php"
                ?>
            </div>
        </section>
        <?php endif;?>
        <!-- games list-->
        <?php include '_partials/slots/games-list.php'; ?>
        <!-- end games list -->
        <!-- END Games info modal -->

        <div style="display: none" class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
            <div class="cta-bottom-wrap ">
                <div class="cta-bottom fullwidth">
                    <div class="mobile-menu-top-area__join" data-bind="visible: !validSession()">
                        <div class="inner">
                            <a data-hijack="true" class="menu-link" href="/register/">JOIN NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-content">
        <?php
            if ($this->action == 'exclusive') {
            edit($this->controller.$this->action,'footer-seo', "bottom:0px");
        } else {
            edit($this->controller,'footer-seo',$this->action, "bottom:0px");
        } ?>
        <?php @$this->getPartial($this->content['footer-seo'],$this->action); ?>
    </div>

</div>
<!-- END MIDDLE CONTENT -->