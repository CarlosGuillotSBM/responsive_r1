<script type="application/javascript">
  skinModules.push({
    id: "OptIn",
    options: {
        modalSelectorWithoutDetails: "#custom-modal",
        details : true
    }
  });
</script>

<!-- MIDDLE CONTENT -->
<div class="middle-content">
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!--/ breadcrumbs -->
  <!-- MIDDLE CONTENT -->
  <div class="middle-content__box" itemscope itemtype="http://schema.org/Article">
    <section class="section">
      <h1 class="section__title"><?php echo $this->content["Title"];?></h1>
      <!-- PROMOTIONS CONTENT -->
      <div class="">
        <div class="clearfix"></div>
        
        <?php edit($this->controller,'promotions',$this->action); ?>
        
        <!-- promotions list -->
        <div class="promotions-detail">
          
          <!-- Promotions detail -->
          <article class="promotion--featured">
            <div class="promotion__wrapper">
              <div class="promotion__content__wrapper">

                <div class="promotion__content">                
                  <div class="promotion__image">
                    <img src="<?php echo $this->content["Text8"] != '' ? $this->content["Text8"] : $this->content["Image"]?>" alt="<?php //echo $this->content["Alt"]?>">
                   
                  </div>

                  <?php
                  echo $this->content["Body"];
                  ?>
                <!-- CTAS -->
                <div class="promo__container__cta">
                  <!-- OPT-IN -->
                  <a style="display: none" class="cta-opt-in cta-full-width" data-tournament="<?php echo $this->content['DetailsURL'] ?>">OPT-IN</a>
                  <?php if($this->content['Text4'] === '') {?>
                    <!-- PLAY NOW -->
                    <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                    <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                  <?php } else { ?>
                    <!-- PLAY NOW -->
                    <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                    <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                  <?php }?>                   
                  <?php
                  if ($this->content['DetailsURL'] == "christmas-slots-race") {
                  include "_global-library/widgets/leaderboard/leaderboard.php";
                  }
                  ?>
                </div>
                <!-- /CTAS -->                    
                </div>

               

                <div class="clearfix"></div>

                 <!-- PROMO FOOTER NAV MOBILE -->
                <div class="footer-nav__container">
                    <a data-hijack="true" style="display: none" data-tournament="<?php echo $this->content['DetailsURL'] ?>" class="cta-opt-in">OPT-IN</a>
                    <?php if($this->content['Text4'] === '') {?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: playBingo" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: playBingo" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php } else { ?>
                      <!-- PLAY NOW -->
                      <a data-hijack="false" style="display: none" data-bind="visible: !validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" id="join-promo" class="promo__container__button cta-join"><?php if($this->content['Text3'] === '') echo 'Play Now'; else echo $this->content['Text3']; ?></a>
                      <a data-hijack="false" style="display: none" data-bind="visible: validSession(), click: navigateOnLogin.bind($data,'<?php echo $this->content['Text4']; ?>')" class="promo__container__button cta-join"><?php if($this->content['Text2'] === '') echo 'Play Now'; else echo $this->content['Text2']; ?></a>
                    <?php }?>                     
                    <a data-hijack="true" href="/promotions/<?php echo $this->getNextPromoURL($this->content["DetailsURL"]); ?>" class="cta-next-promo">NEXT PROMO ></a>
                </div>

                <!-- Terms &amp; Conditions -->
                <dl id="ui-promo-tcs" class="promotion__terms-conditions accordion" data-accordion>
                  <dd  class="accordion-navigation active">
                  <a class="promotion__terms-conditions__link" href="#panelTC">Terms &amp; Conditions</a>
                  <div id="panelTC" class="content active">
                    <?php echo $this->content["Terms"];?>
                  </div>
                  </dd>
                </dl>
                <!-- /Terms &amp; Conditions -->


              </div>
            </div>
          </article>
        </div>
        <!-- /promotions list -->
      </div>
      <!-- PROMOTIONS CONTENT -->
    </section>
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->