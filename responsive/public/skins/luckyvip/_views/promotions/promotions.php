<!-- MIDDLE CONTENT -->
<div class="middle-content">
        <script type="application/javascript">   
          skinModules.push({
              id: "RadFilter"
          });
      </script>
  <!-- breadcrumbs -->
  <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
  <!--/ breadcrumbs -->
  <div class="middle-content__box">
    <section class="section">
      <h1 class="section__title"><?php echo $this->controller; ?></h1>
      <!-- PROMOTIONS CONTENT -->
      <div class="promotions">
        <div class="clearfix"></div>
        
        <?php edit($this->controller,'promotions'); ?>
        
        <!-- promotions list -->
        <div class="promotions__list">
          <!-- Promotions repeater -->
          <?php $this->repeatData($this->content["promotions"],1, "_global-library/partials/promotion/promotion.php"); ?>
          <!-- /Promotions repeater -->
          <div class="clearfix"></div>
        </div>
        <!-- /promotions list -->
        <!-- SEO CONTENT -->
        <div class="lounge__intro">
            <?php edit($this->controller,'promotions-seo-content'); ?>
            <?php @$this->getPartial($this->content['promotions-seo-content'],1); ?>
        </div>
        <!-- /SEO CONTENT -->
      </div>
      <!-- PROMOTIONS CONTENT -->
    </section>
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->