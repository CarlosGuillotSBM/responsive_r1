<!-- MIDDLE CONTENT -->
<div class="middle-content">
  <!-- MIDDLE CONTENT -->
  <div class="middle-content__box">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!-- END breadcrumbs -->
    <section class="section">
      <h1 class="section__title"><?php echo str_replace('-', ' ', $this->controller_url)?></h1>
      <!-- CONTENT -->
      <?php edit($this->controller,'terms-and-conditions'); ?>
      <div class="content-template">
        
        <!-- repeatable content -->
        <?php @$this->repeatData($this->content['terms-and-conditions']);?>
        <!-- /repeatable content -->
      </div>
      <!-- /CONTENT-->
    </section>
  </div>
  <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->