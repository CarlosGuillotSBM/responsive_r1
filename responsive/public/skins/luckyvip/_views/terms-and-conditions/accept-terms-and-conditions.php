<!-- MIDDLE CONTENT -->
<div class="middle-content">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->
    <!-- MIDDLE CONTENT -->
    <div class="middle-content__box">
        <section class="section">
			<?php edit($this->controller,'terms-popup'); ?>
            <h1 class="section__title">Our Terms &amp; Conditions have changed</h1>
            <div class="small-6 small-centered columns">
                <a data-bind="click: LoginBox.acceptTC" class="button">Click here to accept the Terms &amp; Conditions</a>
            </div>
            <!-- CONTENT -->
            <div class="content-template">
                <!-- repeatable content -->
                <?php @$this->repeatData($this->content['terms-popup']);?>
                <!-- /repeatable content -->
            </div>
            <!-- /CONTENT-->
        </section>
    </div>
    <!-- END MIDDLE CONTENT BOX-->
</div>
<!-- END MIDDLE CONTENT -->