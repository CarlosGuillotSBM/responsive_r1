<?php
    // the hub data is common for all the same controller
    // special exception for exclusive slots so it can have it's own hub
    $page = $this->controller;
    if($this->action == 'exclusive') $page.= 'exclusive';
?>
<!-- MIDDLE CONTENT -->
<div class="middle-content ">
    <!-- breadcrumbs -->
    <?php include '_global-library/widgets/breadcrumbs/breadcrumbs.php'; ?>
    <!--/ breadcrumbs -->

    <!-- banner --> 
    <div class="games-content__banner ui-scheduled-content-container">
        <!-- Edit point Banner -->
        <?php edit($this->controller,'games-latest-offer'); ?>
        <?php @$this->repeatData($this->content['games-latest-offer']); ?>
    </div>

    <div class="games-pages nofloat-content">
        <!-- games menu -->
        <?php include '_partials/games-menu/games-menu.php'; ?>
        <!-- end games menu -->

        <!-- games list-->
        <?php include '_global-library/partials/games/games-list.php'; ?>
        <!-- end games list -->
    </div>

    <div class="bottom-content">
        <?php
			if ($this->action != "slots") {
				edit($this->controller.$this->action,'footer-seo', $this->action);
				@$this->getPartial($this->content['footer-seo'], 1); 
			} else {
				edit($this->controller,'footer-seo',$this->action);
				@$this->getPartial($this->content['footer-seo'],$this->action); 
			}
		?>
    </div>
</div>
<!-- END MIDDLE CONTENT -->