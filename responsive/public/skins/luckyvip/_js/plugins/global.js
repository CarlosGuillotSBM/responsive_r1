var sbm = sbm || {};

sbm.Global = function() {
    "use strict";

    return {
        highlightSubNav: function(page) {
            $("ul.games-menu__games-nav > a").removeClass("active");

            if (page.indexOf("/games/") > -1) {
                $("#allslots a").addClass("active");
            } else if (page.indexOf("/slots/table-games/") > -1) {
                $("#table-card a").addClass("active");
            } else if (page.indexOf("/slots/") > -1) {
                $("#scratchcards a").addClass("active");
            }
        },
        run: function() {


            if (window.location && window.location && window.location.search !== "") {
                var key = window.location.search.split("=")[1];
                if (key === "ui-my-balances-tab"){
                } else if (key === "ui-redeem-tab"){
                    $("#ui-redeem-tab").click();
                } else if (key === "ui-my-details-tab"){
                    $("#ui-my-details-tab").click();
                } else if (key === "ui-activity-tab"){
                    $("#ui-activity-tab").click();
                }
            }

            if (window.location && window.location && window.location.search !== "") {
                var key = window.location.search.split("=")[1];
                if (key === "ui-my-balances-tab"){
                } else if (key === "ui-claim-tab"){
                    $("#ui-redeem-tab").click();
                } else if (key === "ui-my-details-tab"){
                    $("#ui-my-details-tab").click();
                } else if (key === "ui-activity-tab"){
                    $("#ui-activity-tab").click();
                }
            }

            // useful variables for the site
            var page = window.location.pathname;
            var fullurl = window.location
            var device = sbm.serverconfig.device;




            /**
             * Help page - forgot password accordion
             */
            $(".ui-accordion").on("click", function () {
               $(this).next().toggle();
               $(this).toggleClass("active");
            });
            // ------------------------------
            // ----
            //hightlight selected nav element
            // ----
            // ------------------------------
            if (page.indexOf("/games/") > -1 || page.indexOf("/slots/table-games/") > -1 || page.indexOf("/slots/") > -1) {
                this.highlightSubNav(page);
            }
            var _current =  location.pathname.split('/')[1]
            var itemID;
            var item;

            //        if (page == "/") {
            //           $('.header').css('position', 'fixed');
            //       }

            // else   {

            //       $('.header').css('position', 'relative');

            //  };
// fix the header when scrolling
            var $headerscroll = $('#ui-header-scroll, .nav-for-mobile');
            $(document).scroll(function() {
                $headerscroll.css({position: $(this).scrollTop() > 2? "fixed":"relative"});
            });

            if (_current == "" || _current == "start") {
                _current = "home";
            };

            if (_current == "register") {
                _current = "join-now";
            };


            // Changing the id of the body when we get the home through ajax navigation.
            $('body').attr('id', _current);


            $(".desktop-nav__link").each(function(index) {
                itemID = $(this).attr('id');
                if (itemID != _current) {
                    $(this).find("a").removeClass("current");
                } else {
                    $(this).find("a").addClass("current");
                }
            });


            // change name of Home link and href attr
            if ($.cookie("SessionID")) {
                $(".ui-go-home").attr("href", "/start/").text("START");
                $(".ui-go-home-logo").attr("href", "/start/");
                var logo = $(".site-logo-mobile");
                logo.addClass("site-logo-mobile-loggedin");
                logo.removeClass("site-logo-mobile");
            } else {
                var logo = $(".site-logo-mobile");
                logo.addClass("site-logo-mobile");
                logo.removeClass("site-logo-mobile-loggedin");
            }

            // --------------
            // SHARE BUTTON
            // --------------

            var config = {
                ui: {
                    flyout: 'middle left'
                },
                networks: {
                    facebook: {
                        url: this.fullurl
                    },
                    twitter: {
                        url: this.fullurl
                    },
                    google_plus: {
                        url: this.fullurl
                    },

                    email: {
                        enabled: false
                    },
                    pinterest: {
                        enabled: false
                    }
                }
            }

            var share = new Share('.share-button', config);

            //We show it if it is not the cashier.
            if (page == '/cashier/' || page == '/my-account/' || page == "/register/" || page == "/welcome/" || page == "/login/") {
                $('.share-button').attr("style", "display: none");
            } else {
                $('.share-button').attr("style", "display: inline");
            }

            // --------------
            // /SHARE BUTTON
            // --------------


            // --------------
            // /cropping text in side promotions
            // --------------
            // $(document).ready(function() {
            //     $(".promotion__content").dotdotdot({
            //         // configuration goes here
            //         watch: "window"
            //     });
            // });



            var page = window.location.pathname;



            //jQuery for click effect on search results screen to show growing circle background
            var parent, ink, d, x, y;
            $(".ripple li a").click(function(e){
                parent = $(this).parent();
                //create .ink element if it doesn't exist
                if(parent.find(".ink").length == 0)
                    parent.prepend("<span class='ink'></span>");

                ink = parent.find(".ink");
                //incase of quick double clicks stop the previous animation
                ink.removeClass("animate");

                //set size of .ink
                if(!ink.height() && !ink.width())
                {
                    //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                    d = Math.max(parent.outerWidth(), parent.outerHeight());
                    ink.css({height: d, width: d});
                }

                //get click coordinates
                //logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
                x = e.pageX - parent.offset().left - ink.width()/2;
                y = e.pageY - parent.offset().top - ink.height()/2;

                //set the position and add class .animate
                ink.css({top: y+'px', left: x+'px'}).addClass("animate");
            })

            /**
             * Search animation
             */
            $(".search-screen-mobile__header").on("click", function() {
                $(this).animate({top:'-50px'}), 1000, "easeOutBounce";
                $(".search-screen").animate({"bottom": "-100%"}, "slow").css("visibility", "hidden");
                $("body").removeClass("modal-open");
            });
        }
    };

};