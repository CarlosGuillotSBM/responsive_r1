var sbm = sbm || {};

sbm.Slick = function (options) {
    "use strict";

    return {

        sb: null,
        vm: null,
        pluginOptions: options,

        setSlider: function ($el, options) {

            // make all the images visible
            $el.find("li").show();

            // start bxSlider plugin
            var slider = $el.bxSlider(options);

            // notifications
            if (options.sandboxRequired) {
                this.sb.listen("bingo-rooms-updated", function () {
                    slider.reloadSlider();
                }.bind(this));
            }

            // handles clicks on banners

            $el.find("a").on("click", function (e) {

                var $target = $(e.currentTarget);
                var url = $target.attr("href");

                if (!url) {
                    return;
                }

                var pos = $el.getCurrentSlide();
                var images = $target.find("img");
                var img = images.length ? $(images).attr("src") : "";
                var playerId = $.cookie("PlayerID") || "";

                var label = playerId ? "player=" + playerId + ";url=" + url + ";img=" + img : "url=" + url + ";img=" + img + ";pos=" + pos;

                this.sendDataToGoogle("carousel", label);

            }.bind(this));
        },

        sendDataToGoogle: function (category, label) {

            if (window.dataLayer) {

                window.dataLayer.push({
                    "event": "GAevent",
                    "eventCategory": category,
                    "eventAction": window.location.pathname,
                    "eventLabel": label
                });
            }
        },

        run : function (sb, vm) {

            if (sb) {
                this.sb = sb;
                this.vm = vm;
            }

            /**
             * BxSlider plugin initialization for homepage slider
             */

            // cache dom elem
            var $bxSlider = $('.'+(this.pluginOptions.element? this.pluginOptions.element : 'bxslider'));
            var i = 0,
                options;
            if ($bxSlider) {
                for (i; i < $bxSlider.length; i++) {

                    if (!this.pluginOptions) {

                        // default options
                        this.pluginOptions = {
                            auto: false,
                            pause: 5500,
                            autoHover: false,
                            slideWidth: $bxSlider.data("width"),
                            minSlides: 1,
                            slideMargin: 0,
                            mode: 'fade',
                            pager: false,
                            onSlideBefore: function($slideElement, oldIndex, newIndex){
                                //var $lazy = $slideElement.find(".lazy-bx")
                                //var $load = $lazy.attr("data-src");
                                //$lazy.attr("src",$load).removeClass("lazy-bx");
                            },
                            controls: false
                        };
                    }

                    this.setSlider($($bxSlider[i]), this.pluginOptions);

                }
            }


        }
    };


}