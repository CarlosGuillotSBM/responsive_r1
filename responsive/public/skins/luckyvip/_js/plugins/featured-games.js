var sbm = sbm || {};

sbm.FeaturedGames = function () {
    "use strict";

    return {

        loadLazyImages: function () {

            var tab, $currTarget;

            $("img.lazy").lazyload({
                event : "loadimage"
            });

            $(".lazy-load-on-click").on("click", function (e) {

                e.preventDefault();

                $currTarget = $(e.currentTarget);

                // close dropdown
                $currTarget.foundation("dropdown", "close", $("[data-dropdown-content]"));

                // trigger lazy loading
                $("img.lazy").trigger("loadimage");

                // hide tabs
                $(".ui-featured-tab").hide();

                // show the selected tab
                tab = $currTarget.attr("href");
                $(tab).show();

            });

        },

        run : function () {

            this.loadLazyImages();
        }

    };
};