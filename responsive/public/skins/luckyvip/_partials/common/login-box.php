
<div class="small-12 login-box-wrapper" data-bind="visible: LoginBox.visible">

	<div class="row">

		<div class="small-8 columns">
			<input type="text" placeholder="User" name="user" data-bind="value: LoginBox.username">
		</div>
		<div class="small-4 columns forgot-password-text">
			<a href="/forgotpassword">Forgot your password</a>
		</div>
		<div class="small-8 columns">
			<input type="password" placeholder="Password" name="password" data-bind="value: LoginBox.password">
		</div>

		<div class="small-4 columns login-button">
			<a href="" data-bind="click: LoginBox.doLogin">Login</a>
		</div>
	</div>


</div>