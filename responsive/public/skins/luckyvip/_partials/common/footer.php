<!-- BOTTOM CONTENT -->
<div class="bottom-content">
</div>
<!-- END Games info modal -->
</div>
<!-- END AJAX WRAPPER - It is needed to add content inside dynamically -->
</div>
<!-- content-wrapper  -->
<?php include "_partials/common/footer-content.php"; ?>

<!-- login modal -->
<div id="login-modal" class="reveal-modal" data-reveal>
<?php include "_partials/login/login-box.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- Claimed prize modal -->
<div id="claimed-prize-modal" class="reveal-modal" data-reveal>
<?php include "_global-library/partials/promotion/promotion-claimed-prize.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>
<!-- /login modal -->
<!-- Games info modal -->
<div id="games-info" class="games-info reveal-modal" data-reveal>
<?php include "_global-library/partials/games/games-details-modal.php" ?>
<span class="close-reveal-modal">&#215;</span>
</div>

<!-- END Games info modal -->

<!-- Custom modal -->
<div id="custom-modal" class="games-info reveal-modal" data-reveal>
    <?php include "_global-library/partials/bingo/login-box-modal.php" ?>
    <span class="close-reveal-modal">&#215;</span>
</div>
<!-- END custom modal -->

<?php
    include "_global-library/partials/modals/alert.php";
    include "_global-library/partials/modals/notification.php";
    include "_partials/common/loading-modal.php";
    include "_global-library/partials/games/games-iframe.php";
    include "_global-library/partials/games/games-last-played.php";
    include "_global-library/partials/common/app-js.php";
	include "_global-library/partials/common/recaptcha.php";
?>

<input type="hidden" id="ui-cache-key" value="<?php echo $this->cache_key;?>">

</body>
</html>