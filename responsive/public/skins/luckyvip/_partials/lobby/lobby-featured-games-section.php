<script>
    
//needs script coding for mobile view featured games category select menu, this needs to switch tabs



</script>


<!-- Featured Games Box with tabbed content -->
<div class="lobby-wrap__content-box">
    <!--  header bar -->
    <!-- header bar -->
    <div class="lobby-wrap__content-box__header">
        <div class="title hasicon">
            <div class="title__icon">
                <!-- include svg icon partial -->
                <?php include 'icon-featured-game.php'; ?>
                <!-- end include svg icon partial -->
            </div>
            <div class="title__text">FEATURED GAMES</div>
            <div class="title__link">
                <a class="title__viewall--featured-games" href="#">View All <span class="title__link__arrow">&rtrif; </span></a>
            </div>
            <select class="featured-games-categories" name="featured-games-categories" id="featured-games-categories">
                <!--option value="Featured">Featured</option>
                <option value="Favourites">Favourites</option>
                <option value="New Games">New Games</option>
                <option value="Jackpots">Jackpots</option>
                <option value="Exclusive">Exclusive</option>
                <option value="Casino">Casino</option-->

                <?php
                $this->gamesCategory = array();
                $i=1;
                do {
                    if(count($this->games) > 0) {
                        $gc = [
                            'id' => 'lobbypanel' . $i,
                            'index' => $i++,
                            'Games' => $this->games,
                            'Category' => $this->category
                        ];
                        $this->gamesCategory[] = $gc;
                        ?>

                
                    <dd<?php if($gc['index']==1){echo ' class="active"';}?>><a class='lazy-load-on-click' href="#<?php echo $gc['id'];?>"><?php echo $this->category["Description"];?></a></dd>
                    
                <?php
                    }} while ($this->GetNextCategory() && $i < 7);
                ?>

            </select>
        </div>
    </div>
    <!-- end header bar -->
    <!-- content-->
    <div class="lobby-wrap__content-box__content featured-games">
        
        <!--  make this content a partial -->
        <dl class="tabs" data-tab>
<?php
        foreach ($this->gamesCategory as $gc) {
?>
            <dd<?php if($gc['index']==1){echo ' class="active"';}?>><a class='lazy-load-on-click' href="#<?php echo $gc['id'];?>"><?php echo $gc["Category"]["Description"];?></a></dd>
            
<?php };?>
            
        </dl>
        <div class="tabs-content">

<?php
foreach ($this->gamesCategory as $gc) {
?>
            <div class="content <?php if($gc['index']==1){echo 'active';}?>" id="<?php echo $gc['id'];?>">
                <?php foreach($gc["Games"] as $row) {?>

                    <div class="lobby-game"><?php include '_global-library/_editor-partials/game-icon.php'; ?></div>

                <?php }?>
            </div>
                
<?php };?>
        </div>
        <!-- end make this content a partial -->
    </div>
    
</div>
<!-- end featured games -->

