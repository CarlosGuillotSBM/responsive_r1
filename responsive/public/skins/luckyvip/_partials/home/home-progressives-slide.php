<!-- jackpot -->
<?php if ($row['DetailsURL'] !== 'playBingo') { ?>
<li data-bind="click: GameLauncher.openGame" data-gameid="<?php echo $row["DetailsURL"];?>"  style="width:300px !important;">
<?php } else {?>	
<li data-bind="click: playBingo">
<?php } ?>
    <!-- avatar -->
    <div class="ticker-avatar">
        <img src="<?php echo @$row['Image'];?>" alt="">
    </div>
    <!-- winner info -->
    <div class="slider--copy--container">
    <div class="ticker-info" data-bind="click: validSession ? playBingo : null">
        <!-- slot -->
        <p class="ticker-slot"><?php echo ellipsis($row["Title"], 15); ?></p>
    </div>
    <!-- amount -->
    <span class="ticker-amount ui-prog-jackpot-amount" data-amount="<?php echo $this->getProgressiveByName(@$row['Text2']);?>">...</span>

	<span class="slider--copy--CTA" style="visibility: hidden" data-bind="text: validSession() ? 'Play Now' : 'Try Game', style: { visibility: 'visible' }">Play Now</span>
</div>
</li>