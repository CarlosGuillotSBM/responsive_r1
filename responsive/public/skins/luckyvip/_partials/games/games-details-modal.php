<div class="game-detail-modal">
    <div class="game-detail-wrap__title-bar">
        <h1 data-bind="text: GameLauncher.gameTitle" class="ui-game-title"></h1>
    </div>
    <div class="game-detail-content">
        <div class="game-detail-content__left">
            <span data-bind="attr: { 'data-gameid': GameLauncher.gameId, 'data-demoplay': GameLauncher.demoPlay }" data-nohijack="true" class="games-info__try">
                <div style="cursor: pointer" data-bind="visible: GameLauncher.demoPlay, click: GameLauncher.tryGame.bind($data, false)" class="overlay">
                    <img alt="Try Game" data-bind="attr: {src: !validSession()? '/_images/common/play-icons/try-game.png' : '/_images/common/play-icons/play-now.png' }"
                    class="play-now-button">
                </div>
                <img data-bind="attr: { src: GameLauncher.icon }">
            </span>
        </div>
        <div class="game-detail-content__right">
            <article  class="game-detail__description">
                <h3>Game Description</h3>
                <p data-bind="html: GameLauncher.gameDescription"></p>
            </article>
            <?php include'_partials/login/login-box.php' ?>
        </div>
    </div>
</div>
