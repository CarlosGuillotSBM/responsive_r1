
<div class="games-filters">
    <div class="games-menu__container">
        <!-- games nav -->
        <ul class="games-menu__games-nav">
            <li id="allslots"><a href="/games/" data-hijack="true">ALL GAMES</a></li>
            <li id="table-card"><a href="/slots/table-games/" data-hijack="true">TABLE &amp; CARD GAMES</a></li>

            <li id="scratchcards" class="custom-hide-for-mobile"><a href="/slots/" data-hijack="true">SLOTS</a></li>
            <li id="magicalMenu" class="games-filter-menu games-filter-menu__slots-by-type right">
                <div class="cat-search-bar__categories games-filter-menu"><?php $customMenuId = "home-above"; include "_global-library/partials/game-filter-menu/game-filter-menu-8ball.php"; unset($customMenuId);?></div>
            </li>
        </ul>
        <!-- /games nav -->
    </div>
</div>
