<!-- balances -->
<?php
    // TODO : refactor this
    $realMoney = "Real Money";
    $bonusMoney = "Bonus Money";
    $freeSpins = "Free Spins";
    $points = "Points";
    if (config("SkinID") == 6) {
    $realMoney = "Real Balance";
    $bonusMoney = "Bonus Balance";
    $freeSpins = "Free Spins Balance";
    $points = "Loyalty Points";
    } else if (config("SkinID") == 5) {
    $points = "Moolahs";
    }
?>

<script type="application/javascript">
    skinModules.push( { id: "PlayerAccount" } );
</script>

<div class="balance-display-mobile" style="display: none">
    <div class="balance-display-mobile__header ui-close-modals">My Balances</div>
    <ul>

        <li id='ui-balance-switch' class="balance active" >
            <span class="balance-text">HIDE BALANCE</span>
            <label class="switch">
                <div class="slider round"></div>
            </label>
        </li>


        <li>REAL MONEY<span data-bind="text: real">...</span></li>
        <li>BONUS MONEY<span data-bind="text: bonus">...</span></li>
        <li>BONUS WINS<span data-bind="text: bonusWins">...</span></li>
        <li>FREE SLOT SPINS<span data-bind="text: bonusSpins" class="bonuswins_amount ui-lbBonusSpins">...</span></li>
        <li><div class="loyalty_points">LOYALTY POINTS</div><span data-bind="text: points">...</span></li>
        <li>CASHBACK<span data-bind="text: cashback" class="balance_amount ui-lbBalance">...</span></li>

    </ul>
</div>
