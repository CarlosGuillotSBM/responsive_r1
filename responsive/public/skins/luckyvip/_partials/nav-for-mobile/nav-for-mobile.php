<!-- Mobile Top Bar -->
<div class="tab-bar nav-for-mobile">
    <!-- COOKIE POLICY -->
    <?php include "_global-library/partials/modals/cookie-policy.php" ?>
    <!-- MOBILE LOGO -->
    <h1 class="seo-logo">
        <a data-hijack="true" href="/" class="site-logo ui-go-home-logo menu-link">
            <img src="/_images/common/luckyvip-logo-mobile.svg"/>
        </a>
        <span style="display:none;">Lucky VIP Casino online The best online casino sites on mobile and desktop.</span>
    </h1>
    <!-- MOBILE ICON -->
    <a data-hijack="true" href="/" class="site-logo-mobile ui-go-home-logo">
        <h1 class="image-replacement" href="/"><?php echo config("Name"); ?></h1>
    </a>
    <!-- Start of navigation toggle -->
    <div class="menu-toggle-wrap">
        <div class="burger">
            <div class="span"></div>
        </div>
    </div>
    <!-- MENU ICONS -->
    <div class="icons-menu">
		<span>
			<!-- Message Inbox-->
            <?php if(config("inbox-msg-on")): ?>
                <div class="mail-icon" title="Message Inbox" style="display: none" data-bind="visible: validSession">
                    <a href="/my-messages/" target="_top">
                        <img src="/_images/common/mobile-menu-icons-svg/mail-icon.svg">
                    </a>
                    <span class="mail-value" data-bind="visible: MailInbox.unreadMessages() > 0, text: MailInbox.unreadMessages()"></span>
                </div>
            <?php endif; ?>
            <!-- /Message Inbox-->
			<!-- account Icon -->
			<button class="account-icon" style="display: none" data-bind="visible: validSession">
                <img src="/_images/common/mobile-menu-icons-svg/acc-icon.svg" alt="account">
            </button>
			<!-- Search Icon -->
			<button class="search-icon">
                <img src="/_images/common/mobile-menu-icons-svg/search-icon.svg" alt="search">
            </button>
		</span>
    </div>
    <!-- balance display -->
    <div class="balance-display" style="display: none" data-bind="visible: validSession"><span data-bind="text: balance">...</span></div>

    <!-- logged out -->
    <div class="nav-for-mobile__ctas">
        <!--		<a style="display: none" data-bind="visible: validSession()" data-reveal-id="login-modal-deposit" class="login-button--nav-for-mobile menu-link">INFO</a>-->
        <a style="display: none" data-bind="visible: !validSession()" data-reveal-id="login-modal" class="login-button--nav-for-mobile menu-link ui-scroll-top">LOGIN</a>
        <a style="display: none" data-bind="visible: !validSession()" class="join-button--nav-for-mobile menu-link" href="/register/">JOIN</a>
    </div>
    <!-- /logged out -->



</div>
<!-- / Mobile Top Bar -->

<!-- Menu Mobile -->
<?php  include'_partials/nav-for-mobile/menu-mobile.php'; ?>
<!-- Account Menu Mobile -->
<?php include'_partials/nav-for-mobile/account-menu-mobile.php'; ?>
<!-- Balance Display Mobile -->
<?php include'_partials/nav-for-mobile/balance-display-mobile.php'; ?>
<!-- My Messages Mobile -->
<?php //include'_partials/my-messages-mobile/single-message-mobile.php'; ?>
