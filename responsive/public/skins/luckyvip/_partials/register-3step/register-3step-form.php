
<!-- 3 STEP REGISTRATION FORM -->
<div class="registration-3step-form">

  <form id="registration-3step-form">

      <p class="registration-3step__mandatory-fields">Mandatory fields*</p>

      <!-- 1 STEP 
      ////////////////////////////////////////////////////////////////////////////// -->
<fieldset class="step" data-bind="visible: Register.currentStep() == 1">
      <!-- TITLE -->
      <ul class="registration__title" data-bind="value: Register.title">
        <label for="title">Title</label>
        <li data-bind="css: {ticked: Register.title() == 'Ms'}, click: function() { Register.title('Ms');}" class="ticked"><button>Ms</button></li>
        <li data-bind="css: {ticked: Register.title() == 'Miss'}, click: function() { Register.title('Miss');}"><button>Miss</button></li>
        <li data-bind="css: {ticked: Register.title() == 'Mrs'}, click: function() { Register.title('Mrs');}"><button>Mrs</button></li>
        <li data-bind="css: {ticked: Register.title() == 'Mr'}, click: function() { Register.title('Mr');}"><button>Mr</button></li>
      </ul>
      
      <!-- FIRST NAME -->
      <input data-bind="value: Register.firstName" class="textField" type="text" placeholder="First Name*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      
      <!-- LAST NAME -->
      <input data-bind="value: Register.lastName" class="textField" type="text" placeholder="Last Name*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      
      <!-- DATE OF BIRTH 
      http://zurb.com/university/lessons/date-night-with-zurb-foundation-how-to-set-up-a-date-picker
      -->

      <div class="date">
        <label for="startDate">Date of birth</label>
        <input type="number" id="dayOfBirth" name="dayOfBirth" class="fdatepicker" placeholder="DD" maxlength="2" data-bind="value: Register.dayOfBirth, valueUpdate: 'afterkeydown'">
        <span>/</span>
        <input type="number" id="monthOfBirth" name="monthOfBirth" class="fdatepicker" placeholder="MM" maxlength="2" data-bind="value: Register.monthOfBirth, valueUpdate: 'afterkeydown'">
        <span>/</span>
        <input type="number" id="yearOfBirth" name="selectedYearOfBirth" class="fdatepicker" placeholder="YYYY" maxlength="4" pattern="\d{4}" data-bind="value: Register.selectedYearOfBirth, valueUpdate: 'afterkeydown'">
        <span class="error" data-bind="visible: Register.DOBOver18">18+ only. Include leading 0s</span>
      </div>


    <!-- COUNTRY -->
    <select data-bind="options: Register.countries, optionsCaption: 'Country', optionsValue: 'Code', optionsText: 'Desc', value: Register.selectedCountry" class="textField"></select>
    
    <!-- POST CODE -->
    <div>
      <input class="post-code" data-bind="value: Register.postcode, valueUpdate: ['input', 'afterkeydown']" type="text" placeholder="Post Code*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <a href="#" data-nohijack="true" data-bind="visible: Register.showFindAddress, click: Register.postcodeLookup, css: {active: Register.postcodeIsValid}" class="button postfix findaddress" >Find Address</a>
      <div style="display: none" data-bind="visible: Register.postcodeIsValid">
        <select data-bind="visible:  Register.addresses().length > 0,
          options: Register.addresses,
          optionsCaption: 'Please select',
          value: Register.selectedAddress1" class="textField border-important">
        </select>
        <input style="display: none" data-bind="visible: Register.postcodeIsValid && Register.addresses().length === 0, value: Register.address1" class="textField" type="text" placeholder="Address 1*"
        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <input data-bind="value: Register.address2" class="textField" type="text" placeholder="Address 2" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <input data-bind="value: Register.city" class="textField" type="text" placeholder="City*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <input data-bind="value: Register.state" class="textField" type="text" placeholder="County / State*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      </div>
    </div>
<!-- end step 1 -->
</fieldset>

    <!-- 2 STEP  
    ////////////////////////////////////////////////////////////////////////////// -->
<fieldset class="step" style="display: none" data-bind="visible: Register.currentStep() == 2">
    <!-- EMAIL -->
    <div>
      <!-- <label class="error">Error</label> -->
      <input data-bind="value: Register.email" class="textField" type="email" placeholder="E-Mail*" autofocus>
      <!-- <small class="error">Invalid entry</small> -->
      <span style="display: none" data-bind="text: Register.emailNotAvailable, visible: Register.emailNotAvailable() != ''" class="error"></span>
    </div>

    <!-- USERNAME -->
    <div>
      <input data-bind="value: Register.username" class="textField" type="text" placeholder="Username*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
      <span style="display: none" data-bind="text: Register.usernameNotAvailable, visible: Register.usernameNotAvailable() != ''" class="error"></span>
    </div>

    <!-- PASSWORD -->
    <div class="registration__password">
        <input class="textField registration__password" data-bind="value: Register.password, attr: { type: LoginBox.passwordShowText() == 'Hide' ? 'text' : 'password' }" type="password" placeholder="Enter password*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
        <div class="show-password">
          <a class="right" data-bind="text: LoginBox.passwordShowText, click: LoginBox.togglePassword">Show</a>
        </div>
        <!-- <label data-tooltip title="Your password should be 5 - 15 characters long, and contain at least one number, and have no spaces">Password Help</label>-->
    </div>

    <!-- CURRENCY -->
    <select data-bind="options: Register.currencies, optionsCaption: 'Currency', optionsValue: 'Code', optionsText: 'Desc', value: Register.selectedCurrency" class="textField"></select>

    <!-- end step 2 -->
  </fieldset>
    
    


    <!-- 3 STEP  
    ////////////////////////////////////////////////////////////////////////////// -->
<fieldset class="step" style="display: none" data-bind="visible: Register.currentStep() == 3">
    <!-- MOBILE PHONE -->
    <input data-bind="value: Register.mobilePhone" class="textField" type="tel" placeholder="Mobile phone*" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">

    <!-- PROMO CODE -->
    <input data-bind="value: Register.promoCode" class="textField" type="text" placeholder="Promo Code" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">

    <!-- MAILING OPT -->
    <ul class="mailing-opt">
      <p>Yes please keep me up to date with promotions, new games and other information:</p>

      <!-- EMAIL -->
      <li>
        <label data-bind="css: {ticked: Register.wantsEmail}" for="cbEmail">
          <input data-bind="checked: Register.wantsEmail" type="checkbox" checked id="cbEmail">
          <span class="custom checkbox"></span>
          Email
        </label>
      </li>

      <!-- SMS -->
      <li>
        <label data-bind="css: {ticked: Register.wantsSMS}" for="cbSms">
          <input data-bind="checked: Register.wantsSMS" type="checkbox" checked id="cbSms">
          <span class="custom checkbox"></span>
          SMS
        </label>
      </li>

      <!-- OVER 18 -->
      <li>
        <label data-bind="css: {ticked: Register.over18}" for="cbOver18">
        <input data-bind="checked: Register.over18, validationOptions: { insertMessages: false}" type="checkbox" id="cbOver18" name="cbOver18">
          <span class="custom checkbox"></span>
          I am 18 or over and agree to the <a data-nohijack="true" class="register-3step-tc" href="/terms-and-conditions/" target="terms">T&amp;Cs</a>
        </label>
      </li>
      <p data-bind="validationMessage: Register.over18"></p>

    </ul>
    <!-- /MAILING OPT -->
<!-- end step 3 -->
</fieldset>
    <!-- CTA -->
     
    <a class="button register-3step-cta next" data-nohijack="true" data-bind="click: Register.doRegistration, visible: Register.totalSteps() == Register.currentStep()">Join Now</a>
   <a class="registerNextCta" data-bind="click: Register.nextStep, visible:  Register.totalSteps() > 0 && Register.totalSteps() > Register.currentStep() ">Next</a>
</form>

</div>
<!-- /3 STEP REGISTRATION FORM -->
