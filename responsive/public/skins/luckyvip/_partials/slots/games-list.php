<script type="application/javascript">
	skinModules.push({
		id: "GamesDisplay",
		options: {
			defaultMode: "icons"
		}
	});
</script>


<?php
do {
	if(count($this->games) > 0) {
?>


	<section class="<?php echo @$this->category["Class"];?> games-list middle-content__box">
		<div class="section__header hideme">
	            <h1 class="section__title">
					<?php echo $this->category['Description'] ?><span class="section__header__viewall"><?php echo $this->getCategoryHref('View All');?></span>
					<span>
						<a class="games-text-switcher games-sort-icon-list" data-bind="click: GamesDisplay.toggleMode" data-mode="list">

						</a>
					</span>
					<span>
						<a class="games-text-switcher games-sort-icon-tiled active  " data-bind="click: GamesDisplay.toggleMode" data-mode="grid">

						</a>
					</span>
				</h1>
	        </div>

		<!-- ICON VIEW -->

		<div data-bind="visible: GamesDisplay.displayMode() === 'icons'" class="games-list__grid">
	        <?php $i = 1; foreach($this->games as $row){ ?>
				<!-- item -->
				<div class="games-list__grid__child ui-parent-game-icon" data-position="<?php echo $i ?>" data-containerkey="<?php echo $this->category['Key']; ?>">
					<?php include '_global-library/_editor-partials/game-icon.php'; ?>
				</div>
				<!-- /item -->
			<?php $i++; }?>
		</div>

		<!-- TEXT VIEW-->
		<div style="display: none" data-bind="visible: GamesDisplay.displayMode() === 'text'" class="games-list__grid">
			<?php include '_global-library/partials/games/games-text-display.php' ?>
		</div>

	</section>

 <?php }

} while ($this->GetNextCategory());?>


