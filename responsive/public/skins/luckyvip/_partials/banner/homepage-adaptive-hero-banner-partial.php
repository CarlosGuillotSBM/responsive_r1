<?php
     $content = array(
          'line-1' =>  $row['Text1'],
          'line-2' =>  $row['Text2'],
          'line-3' =>  $row['Text3'],
          'cta-text' =>  $row['Text4'],
          'cta-url' =>  $row['Text5'],
          't&c-text' =>  $row['Text6'],
          't&c-url' =>  $row['Text7'],
          'extra-copy' =>  $row['Text8'],
          'image-desktop' =>  $row['Text9'],
          'image-desktop-retina' =>  $row['Text10'],
          'image-tablet' =>  $row['Text11'],
          'image-tablet-retina' =>  $row['Text12'],
          'image-mobile' =>  $row['Text13'],
          'image-mobile-retina' =>  $row['Text14'],
          'restriction' =>  $row['Text15'],
     );
    // echo "<h1 style='color:white'>".$content['restriction']."</h1>";
?>

<style media="screen">
    .hero ui-adaptive-hero-banner {
        background: url(<?=$filtered_content['image-desktop']; ?>);
        background-image: -webkit-image-set(url(<?=$filtered_content['image-desktop']; ?>) 1x,
                                            url(<?=$filtered_content['image-desktop-retina']; ?>) 2x);
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
    }
    @media only screen and (max-width: 64em) and (min-width: 40.0625em) {
        .hero ui-adaptive-hero-banner {
                background: url(<?=$filtered_content['image-tablet']; ?>);
                background-image: -webkit-image-set(url(<?=$filtered_content['image-tablet']; ?>) 1x,
                                                url(<?=$filtered_content['image-tablet-retina']; ?>) 2x);
        }
    }
    @media only screen and (max-width: 40em) {
        .hero ui-adaptive-hero-banner {
                background: url(<?=$filtered_content['image-mobile']; ?>);
                background-image: -webkit-image-set(url(<?=$filtered_content['image-mobile']; ?>) 1x,
                                                url(<?=$filtered_content['image-mobile-retina']; ?>) 2x);
        background-size: 100% auto;
        }
    }
</style>
<a href="<?=$content['cta-url']?>" width="100%" class="hero ui-adaptive-hero-banner hero--cta" data-device='<?=config("Device")?>' data-adaptive-name='<?=$content['element-name']?>' style="background-image: url(<?= $content['image-desktop'] ?>);">
    <span class="hero--html">
        <h2 class="hero--firstline ui-adaptive-hero__first-line">
            <?php echo $content['line-1'] ?>
        </h2>
        <h2 class="hero--secondline">
            <?php echo $content['line-2'] ?>
        </h2>     
        <h3 class="hero--fourthline">
            <?php echo $content['line-3'] ?>
        </h3>
        <span class="slider-text-link">
        <object class="hero--terms ">
            <a class="hero--terms__link" href="<?php echo $content['t&c-url'] ?>">
                <?php echo $content['t&c-text'] ?>
            </a>
            <br> <span class="hero--terms__extra slider-text-link-copyright"><?php echo $content['extra-copy'] ?></span>
        </object>
        </span>
    </span>
</a>




