<?php
/**
 * Created by PhpStorm.
 * User: josem
 * Date: 11/08/2016
 * Time: 10:36
 */

$_SERVER["SERVER_NAME"] = "responsive.spinandwin.dev";
set_include_path("c:/web/responsive/trunk/responsive/application/;c:/web/responsive/trunk/responsive/public/skins/;.");

include 'config/config.php';
include 'libs/controller.php';
include 'controller/ajax-request/ajax.php';

abstract class AbstractAjaxRequestTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var Ajax
     */
    protected $request;

    /**
     * Test account
     *
     * Username: unitTest
     * Pwd:      qqq111
     *
     * @inherit
     */
    protected function setUp()
    {
        // some default parameters
        $_COOKIE["PlayerID"] = "13009";
        $_COOKIE["SessionID"] = "3AE9A53B-091E-43AC-BA28-F95D70F5F660";
        $_REQUEST["debug"] = 0;
        $_COOKIE["LanguageCode"] = "en";
        $_SERVER["HTTP_USER_AGENT"] = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36";
        $_SERVER["REMOTE_ADDR"] = "127.0.0.1";
        $_REQUEST["l"] = 0;

        global $dbConnection;

        //TODO Remove this local dependency in other environments
        $dbConnection["GAME"] = "192.168.206.20";
        $dbConnection["UID"] = "DaGaCube";
        $dbConnection["PWD"] = "DaGaCube";
    }

    /**
     * @param $method
     *
     * @return ReflectionMethod
     */
    public function setMethodAccessible($method)
    {
        $class = new \ReflectionClass(get_class($this->request));
        $method = $class->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * @param $property
     *
     * @return ReflectionMethod
     */
    public function setPropertyAccessible($property)
    {
        $class = new \ReflectionClass(get_class($this->request));
        $method = $class->getProperty($property);
        $method->setAccessible(true);

        return $method;
    }

    abstract public function testBuildParameters();

    abstract public function testGetContent($parameters);

    abstract public function testPrepareResponse();

    /**
     * @inherit
     */
    protected function tearDown(){
        unset($this->request);
    }
}
