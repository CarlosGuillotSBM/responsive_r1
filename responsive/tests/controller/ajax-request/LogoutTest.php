<?php
/**
 * Created by PhpStorm.
 * User: josem
 * Date: 11/08/2016
 * Time: 10:36
 */

$_SERVER["SERVER_NAME"] = "responsive.spinandwin.dev";
set_include_path("c:/web/responsive/trunk/responsive/application/;c:/web/responsive/trunk/responsive/public/skins/;.");

include 'controller/ajax-request/logout.php';

class LogoutTest extends AbstractAjaxRequestTest
{

    protected function setUp()
    {
        parent::setUp();
        $_REQUEST['f'] = "logout";
        $this->request = new Logout();
    }

    public function testBuildParameters()
    {
        $method = $this->setMethodAccessible('buildParameters');
        $method->invokeArgs($this->request, array());

        $this->assertEquals(
            array(
                "PlayerID"  => array("13009", "int", 0),
                "SessionID" => array("3AE9A53B-091E-43AC-BA28-F95D70F5F660", "UI", 0),
                "SkinID" 	=> array(1, "int", 0),
                "Lang" 	    => array("en", "str", 2),
                "IP"  	    => array("127.0.0.1", "str", 50)
                ),
            $this->request->getParameters()
        );

        return $this->request->getParameters();
    }

    /**
     * Test expecting error message with the session not found
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContentWithCode1(array $parameters)
    {
        $this->request->setParameters($parameters);
        $method = $this->setMethodAccessible('getContents');
        $method->invokeArgs($this->request, array());

        //ddbb opened correctly
        $this->assertTrue(isset($this->request->db));

        $ddbbResponse = $this->request->getRawContent();
        $this->assertEquals(1, $ddbbResponse["Code"]);
        $this->assertEquals("No Session found", $ddbbResponse["Msg"]);

        //queryDatabaseWithSQL($this->request->db, "SELECT TOP 1000 * FROM Main.dbo.LoginSession", true);
    }

    /**
     * TODO after loginTest
     *
     * Test expecting a good logout
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContent($parameters)
    {
        $this->markTestSkipped("TODO once LoginTest is done");

        //queryDatabaseWithSQL($this->request->db, "SELECT TOP 1000 * FROM Main.dbo.LoginSession", true);
    }

    public function testPrepareResponse()
    {
        $this->assertTrue(true);
        // TODO: Implement testPrepareResponse() method.
    }

    protected function tearDown(){
        unset($this->logoutRequest);
    }
}
