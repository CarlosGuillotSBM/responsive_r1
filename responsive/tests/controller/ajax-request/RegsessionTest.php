<?php
/**
 * Created by PhpStorm.
 * User: josem
 * Date: 11/08/2016
 * Time: 10:36
 */

include 'controller/ajax-request/regsession.php';

class RegsessionTest extends AbstractAjaxRequestTest
{

    protected function setUp()
    {
        parent::setUp();
        $this->request = new Regsession();
    }

    public function testBuildParameters()
    {
        $method = $this->setMethodAccessible('buildParameters');
        $method->invokeArgs($this->request, array());

        $this->assertEquals(
            array(
                "SkinID"           => array(1, "int", 0),
                "IP"      		   => array("127.0.0.1", "str", 15),
                "Lang" 			   => array("en", "str", 2),
                "CountryIPID"      => array(2130706433, "int", 0),
                "DeviceCategoryID" => array("1", "int", 0),
                "UserAgent"		   => array("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36", "str", 500)
            ),
            $this->request->getParameters()
        );

        return $this->request->getParameters();
    }

    /**
     * Test
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContent($parameters)
    {

    }

    public function testPrepareResponse()
    {

    }

    /**
     * @runInSeparateProcess
     */
    public function testPrepareResponseWithCodeNot0()
    {

    }

    protected function tearDown(){
        unset($this->request);
    }
}
