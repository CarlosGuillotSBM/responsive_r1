<?php
/**
 * Created by PhpStorm.
 * User: josem
 * Date: 11/08/2016
 * Time: 10:36
 */

include 'controller/ajax-request/lobbyfeed.php';

class LobbyfeedTest extends AbstractAjaxRequestTest
{

    protected function setUp()
    {
        parent::setUp();
        $this->request = new Lobbyfeed();
    }

    public function testBuildParameters()
    {
        $method = $this->setMethodAccessible('buildParameters');
        $method->invokeArgs($this->request, array());

        $this->assertEmpty(
            $this->request->getParameters()
        );

        return $this->request->getParameters();
    }

    /**
     * Test expecting error message with the login details not valid 10
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContent($parameters)
    {
        $method = $this->setMethodAccessible('getContents');
        $method->invokeArgs($this->request, array());

        $this->assertEmpty(
            $this->request->getRawContent()
        );
    }

    public function testPrepareResponse()
    {
        $this->assertEquals("default", $this->callMethodPrepareResponseBySkin());
    }

    public function testPrepareResponseForKitty()
    {
        $this->assertEquals(
            $this->apiResponses("Kitty"),
            $this->callMethodPrepareResponseBySkin("Kitty")
        );
    }

    public function testPrepareResponseForKittyAndMobile()
    {
        $this->assertEquals(
            $this->apiResponses("Kitty", 1),
            $this->callMethodPrepareResponseBySkin("Kitty", 1)
        );
    }

    public function testPrepareResponseForPants()
    {
        $this->assertEquals(
            $this->apiResponses("Pants"),
            $this->callMethodPrepareResponseBySkin("Pants")
        );
    }

    public function testPrepareResponseForPantsAndMobile()
    {
        $this->assertEquals(
            $this->apiResponses("Pants", 1),
            $this->callMethodPrepareResponseBySkin("Pants", 1)
        );
    }

    public function testPrepareResponseForExtra()
    {
        $this->assertEquals(
            $this->apiResponses("Extra"),
            $this->callMethodPrepareResponseBySkin("Extra")
        );
    }

    public function testPrepareResponseForExtraAndMobile()
    {
        $this->assertEquals(
            $this->apiResponses("Extra", 1),
            $this->callMethodPrepareResponseBySkin("Extra", 1)
        );
    }

    public function testPrepareResponseForBubbly()
    {
        $this->assertEquals(
            $this->apiResponses("Bubbly"),
            $this->callMethodPrepareResponseBySkin("Bubbly")
        );
    }

    public function testPrepareResponseForBubblyAndMobile()
    {
        $this->assertEquals(
            $this->apiResponses("Bubbly", 1),
            $this->callMethodPrepareResponseBySkin("Bubbly", 1)
        );
    }

    protected function tearDown(){
        unset($this->request);
    }

    private function callMethodPrepareResponseBySkin($skin = null, $device = 0)
    {
        $_REQUEST["skin"] = $skin;
        $_REQUEST["device"] = $device;
        $method = $this->setMethodAccessible('prepareResponse');
        ob_clean();
        ob_start();
        $method->invokeArgs($this->request, array());
        $apiResult = ob_get_contents();
        ob_end_clean();

        return preg_replace('/\s+/', '', $apiResult);
    }

    private function apiResponses($skin, $device = 0)
    {
        $response = "";
        switch($skin) {
            case "Kitty":
                $response = '{
			    "groups": [
					{
						"name": "Club", "linkage": "Club", "type": "server",
						"rooms":
						[
							{"description": "Free Bingo", "id": "133", "css": "room_club"},
							{"description": "Free 50", "id": "104", "css": "room_club"},
							{"description": "Silver £100 a Day", "id": "135", "css": "room_club"},
							{"description": "Gold £500 a week", "id": "136", "css": "room_club"},
							{"description": "Ruby £1000 a month", "id": "137", "css": "room_club"},
							{"description": "Bonus Booster", "id": "115", "css": "room_club"}
						]
					},
					{
						"name": "Bingo", "linkage": "Bingo", "type": "server",
						"rooms":
						[
							{"description": "Kitty Cat", "id": "106", "css": "room_bingo"},
							{"description": "After Dark", "id": "156", "css": "room_bingo"},
							{"description": "Splish Splash", "id": "101", "css": "room_bingo"},
							{"description": "Fat Cat", "id": "110", "css": "room_bingo"},
							{"description": "Penny Heaven", "id": "112", "css": "room_bingo"},
							{"description": "Turbo", "id": "218", "css": "room_bingo"},
							{"description": "Furry 5", "id": "122", "css": "room_bingo"},
							{"description": "Newbie Room", "id": "207", "css": "room_bingo"},
							{"description": "LobbyTest", "id": "0", "css": "room_bingo"}
						]
					},
					{
						"name": "Prebuy", "linkage": "Prebuy", "type": "server",
						"rooms":
						[
							{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
							{"description": "Fiver Frenzy", "id": "246", "css": "room_newbie"},
							{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
							{"description": "Win-Win Wednesday", "id": "208", "css": "room_newbie"},
							{"description": "Pandora\'s Mystery Box", "id": "113", "css": "room_newbie"},
							{"description": "The Big Dabber", "id": "117", "css": "room_newbie"}
						]
					},
					{
						"name": "No Limits", "linkage": "NoLimits", "type": "server",
						"rooms":
						[
							{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
							{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
							{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
						]
					}';

                if ($device == 1) {
                    $response .= '
					,{
						"name": "Slots", "linkage": "Slots", "type": "slots", "instants": "lobbyFeatured",
						"rooms":
						[
						]
					}
					,{
						"name": "Free Spins", "linkage": "FreeSpins", "type": "slots", "instants": "freeSpins", "userData": "BONUSSPINS",
						"rooms":
						[
						]
					}
					';
                }
                $response .= ']
		}';
                break;
            case "Pants":
                $response = '{
			"groups": [
				{
					"name": "Lucky Club", "linkage": "LuckyClub", "type": "server",
					"rooms":
					[
						{"description": "Free Bingo", "id": "133", "css": "room_club"},
						{"description": "Free 50", "id": "104", "css": "room_club"},
						{"description": "Silver £100 a Day", "id": "135", "css": "room_club"},
						{"description": "Gold £500 a week", "id": "136", "css": "room_club"},
						{"description": "Ruby £500 a month", "id": "137", "css": "room_club"},
						{"description": "Welcome £40", "id": "115", "css": "room_club"}
					]
				},
				{
					"name": "Bingo", "linkage": "Bingo", "type": "server",
					"rooms":
					[
						{"description": "Lucky Stripes", "id": "201", "css": "room_bingo"},
						{"description": "After Dark", "id": "156", "css": "room_bingo"},
						{"description": "Red Hearts", "id": "101", "css": "room_bingo"},
						{"description": "Fat Cat", "id": "110", "css": "room_bingo"},
						{"description": "Penny Heaven", "id": "112", "css": "room_bingo"},
						{"description": "Droopy Drawers", "id": "122", "css": "room_bingo"},
						{"description": "Newbie", "id": "207", "css": "room_bingo"},
						{"description": "Turbo", "id": "218", "css": "room_bingo"}
					]
				},
				{
					"name": "Prebuys", "linkage": "Prebuys", "type": "server",
					"rooms":
					[
						{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
						{"description": "Fiver Frenzy", "id": "246", "css": "room_newbie"},
						{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
						{"description": "Win-Win Wednesday", "id": "208", "css": "room_newbie"},
						{"description": "Pandora\'s Mystery Box", "id": "113", "css": "room_newbie"},
						{"description": "The Big Dabber", "id": "117", "css": "room_newbie"}
					]
				},
				{
					"name": "No Limits", "linkage": "NoLimit", "type": "server",
					"rooms":
					[
						{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
						{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
						{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
					]
				}';

                if ($device == 1) {
                    $response .= '
					,{
						"name": "Slots", "linkage": "Slots", "type": "slots", "instants": "lobbyFeatured",
						"rooms":
						[
						]
					}
					,{
						"name": "Freebies", "linkage": "Freebies", "type": "slots", "instants": "freeSpins", "userData": "BONUSSPINS",
						"rooms":
						[
						]
					}
					';
                }
                $response .= ']
		}';
                break;
            case "Extra":
                $response = '{
			"groups": [
				{
					"name": "Bingo", "linkage": "Bingo", "type": "server",
					"rooms":
					[
						{"description": "The Extra Room", "id": "245", "css": "room_bingo"},
						{"description": "The Diner", "id": "110", "css": "room_bingo"},
						{"description": "The Candy Store", "id": "101", "css": "room_bingo"},
						{"description": "The Drive-In", "id": "156", "css": "room_bingo"},
						{"description": "Penny Arcade", "id": "112", "css": "room_bingo"},
						{"description": "Turbo", "id": "218", "css": "room_bingo"},
						{"description": "Lucky Lanes", "id": "122", "css": "room_bingo"},
						{"description": "Newbie Room", "id": "207", "css": "room_bingo"}
					]
				},
				{
					"name": "Prebuy", "linkage": "Prebuy", "type": "server",
					"rooms":
					[
									{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
									{"description": "Win-Win Wednesday", "id": "208", "css": "room_newbie"},
									{"description": "Extra Surprise", "id": "113", "css": "room_newbie"},
									{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
									{"description": "The Jukebox", "id": "117", "css": "room_newbie"},
									{"description": "The Playground", "id": "246", "css": "room_newbie"}
					]

				},
				{
					"name": "No Limits", "linkage": "NoLimits", "type": "server",
					"rooms":
					[
						{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
						{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
						{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
					]
				}';

                if ($device == 1) {
                    $response .= '
					,{
						"name": "Slots", "linkage": "Slots", "type": "slots", "instants": "lobbyFeatured",
						"rooms":
						[
						]
					}
					,{
						"name": "Free Spins", "linkage": "FreeSpins", "type": "slots", "instants": "freeSpins", "userData": "BONUSSPINS",
						"rooms":
						[
						]
					}
					';
                }
                $response .= ']
		}';
                break;
            case "Bubbly":
                $response = '{
			"groups": [
				{
					"name": "Bingo", "linkage": "Bingo", "type": "server",
					"rooms":
					[
						{"description": "Bubble Trouble", "id": "245", "css": "room_bingo"},
						{"description": "The Fizz Room", "id": "110", "css": "room_bingo"},
						{"description": "Frothy Fun", "id": "101", "css": "room_bingo"},
						{"description": "Watershed", "id": "156", "css": "room_bingo"},
						{"description": "Popping Pennies", "id": "112", "css": "room_bingo"},
						{"description": "Turbo", "id": "218", "css": "room_bingo"},
						{"description": "The Whirlpool", "id": "122", "css": "room_bingo"},
						{"description": "Newbie Room", "id": "207", "css": "room_bingo"}
					]
				},
				{
					"name": "Prebuy", "linkage": "Prebuy", "type": "server",
					"rooms":
					[
						{"description": "Fiver Frenzy", "id": "246", "css": "room_newbie"},
						{"description": "Weekly Shop", "id": "210", "css": "room_newbie"},
						{"description": "Win-Win Wednesday", "id": "208", "css": "room_newbie"},
						{"description": "Treasure Chest", "id": "113", "css": "room_newbie"},
						{"description": "£10k Slider", "id": "118", "css": "room_newbie"},
						{"description": "Soap \'n\' Suds", "id": "117", "css": "room_newbie"}
					]
				},
				{
					"name": "No Limits", "linkage": "NoLimits", "type": "server",
					"rooms":
					[
						{"description": "No Limits Daily", "id": "213", "css": "room_prebuy"},
						{"description": "No Limits Weekly", "id": "214", "css": "room_prebuy"},
						{"description": "No Limits Monthly", "id": "215", "css": "room_prebuy"}
					]
				}
			]
		}
		';
                break;
        }

        return preg_replace('/\s+/', '', $response);
    }
}
