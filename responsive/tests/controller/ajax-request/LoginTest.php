<?php
/**
 * Created by PhpStorm.
 * User: josem
 * Date: 11/08/2016
 * Time: 10:36
 */

include 'controller/ajax-request/login.php';

class LoginTest extends AbstractAjaxRequestTest
{

    protected function setUp()
    {
        parent::setUp();
        $_REQUEST['f'] = "login";
        $_REQUEST['u'] = "unitTest";
        $_REQUEST['p'] = "qqq111";
        $_REQUEST['s'] = "1024x800";
        $_REQUEST['r'] = "";
        $this->request = new Login();
    }

    public function testBuildParameters()
    {
        $method = $this->setMethodAccessible('buildParameters');
        $method->invokeArgs($this->request, array());

        $this->assertEquals(
            array(
                "Username" 		   => array("unitTest", "str", 15),
                "Password" 		   => array("qqq111", "str", 15),
                "DeviceCategoryID" => array(config("RealDevice"), "int", 0),
                "UserAgent"		   => array("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36", "str", 500),
                "SkinID" 	       => array(1, "int", 0),
                "Lang" 	           => array("en", "str", 2),
                "IP"  	           => array("127.0.0.1", "str", 50),
                "ScreenSize"	   => array("1024x800", "str", 15),
                "RedirectURL"	   => array("", "str", 15)
                ),
            $this->request->getParameters()
        );

        return $this->request->getParameters();
    }

    /**
     * Test expecting error message with the login details not valid 10
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContentWithCode1(array $parameters)
    {
        $this->request->setParameters($parameters);
        $method = $this->setMethodAccessible('getContents');
        $method->invokeArgs($this->request, array());

        //ddbb opened correctly
        $this->assertTrue(isset($this->request->db));

        $ddbbResponse = $this->request->getRawContent();
        $this->assertEquals(1, $ddbbResponse["Code"]);
        $this->assertEquals("Sorry, your login details are not valid", $ddbbResponse["Msg"]);
    }

    /**
     * Test expecting error message with the login details not valid
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContentWithCodeX(array $parameters)
    {
        $this->markTestSkipped("TODO");
    }

    /**
     * TODO after loginTest
     *
     * Test expecting a good logout
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContent($parameters)
    {
        $this->markTestSkipped("TODO");

        //queryDatabaseWithSQL($this->request->db, "SELECT TOP 1000 * FROM Main.dbo.LoginSession", true);
    }

    public function testPrepareResponse()
    {
        $this->assertTrue(true);
        // TODO: Implement testPrepareResponse() method.
    }

    protected function tearDown(){
        unset($this->logoutRequest);
    }
}
