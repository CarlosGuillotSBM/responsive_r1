<?php

include 'controller/ajax-request/leaderboardbycompetition.php';

class LeaderboardByCompetitionTest extends AbstractAjaxRequestTest
{

    protected function setUp()
    {
        parent::setUp();
        $_REQUEST["cId"] = 315;
        $this->request = new Leaderboardbycompetition();
    }

    public function testBuildParameters()
    {
        $method = $this->setMethodAccessible('buildParameters');
        $method->invokeArgs($this->request, array());

        $this->assertEquals(
            315,
            $this->request->getParameters()
        );

        return $this->request->getParameters();
    }

    /**
     * Test
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContent($parameters)
    {
        $this->request->setParameters($parameters);
        $method = $this->setMethodAccessible('getContents');
        $method->invokeArgs($this->request, array());

        $ddbbResponse = $this->request->getRawContent();
        $this->assertEquals(
            array("CompetitionID", "PlayerID", "Points", "ScorePoints", "PromoName", "Username", "Position" ),
            array_keys($ddbbResponse[0])
        );
    }

    public function testPrepareResponse()
    {
        $this->markTestSkipped();
        /*
        $this->request->setRawContent(array(
            "Code" => 0,
            "Balance" => "0.00",
            //...
        ));
        $method = $this->setMethodAccessible('prepareResponse');
        $method->invokeArgs($this->request, array());
        $property = $this->setPropertyAccessible('response');
        $this->assertEquals(array(
            "Code" => 0,
            "Balance" => "0.00"),
            $property->getValue($this->request)
        );
        */
    }

    public function testPrepareLeaderBoard()
    {
        $method = $this->setMethodAccessible('prepareLeaderBoard');
        $rows = array(
            array(
                "PlayerID" => 101,
                "Position" => "1",
            ),
            array(
                "PlayerID" => 102,
                "Position" => "2",
            ),
            array(
                "PlayerID" => 103,
                "Position" => "3",
            ),
            array(
                "PlayerID" => 104,
                "Position" => "4",
            ),
            array(
                "PlayerID" => 105,
                "Position" => "5",
            ),
            array(
                "PlayerID" => 106,
                "Position" => "6",
            ),
            array(
                "PlayerID" => 107,
                "Position" => "7",
            )
        );

        $playerID = 104;
        $result = $method->invokeArgs($this->request, array($rows, $playerID));

        $this->assertEquals(5, count($result));
        $this->assertEquals($result[4], $rows[4]);

        $playerID = 107;
        $result = $method->invokeArgs($this->request, array($rows, $playerID));

        $this->assertEquals(5, count($result));
        $this->assertEquals($result[4], $rows[6]);
    }

    protected function tearDown(){
        unset($this->request);
    }
}
