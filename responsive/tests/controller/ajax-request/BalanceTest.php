<?php
/**
 * Created by PhpStorm.
 * User: josem
 * Date: 11/08/2016
 * Time: 10:36
 */

include 'controller/ajax-request/balance.php';

class BalanceTest extends AbstractAjaxRequestTest
{

    protected function setUp()
    {
        parent::setUp();
        $this->request = new Balance();
    }

    public function testBuildParameters()
    {
        $method = $this->setMethodAccessible('buildParameters');
        $method->invokeArgs($this->request, array());

        $this->assertEquals(
            array(
                "PlayerID"  => array("13009", "int", 0),
                "SessionID" => array("3AE9A53B-091E-43AC-BA28-F95D70F5F660", "UI", 0),
                "Lang" 	    => array("en", "str", 2),
                "IP"        => array(getPlayerIP(), "str", 50)
            ),
            $this->request->getParameters()
        );

        return $this->request->getParameters();
    }

    /**
     * Test expecting error message with the login details not valid 10
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContentWithCodeMinus1(array $parameters)
    {
        $parameters["PlayerID"] = -1;
        $this->request->setParameters($parameters);
        $method = $this->setMethodAccessible('getContents');
        $method->invokeArgs($this->request, array());

        $ddbbResponse = $this->request->getRawContent();
        $this->assertEquals(-1, $ddbbResponse["Code"]);
        $this->assertEquals("Your session has expired. Please log in again.", $ddbbResponse["Msg"]);
    }

    /**
     * Test expecting error message with the login details not valid 10
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContentWithSessionNull(array $parameters)
    {
        $parameters["SessionID"] = null;
        $this->request->setParameters($parameters);
        $method = $this->setMethodAccessible('getContents');
        $method->invokeArgs($this->request, array());

        $ddbbResponse = $this->request->getRawContent();

        $this->assertEquals(null, $ddbbResponse);
    }

    /**
     * Test
     *
     * @depends testBuildParameters
     *
     * @param array $parameters
     */
    public function testGetContent($parameters)
    {
        $date = new DateTime();
        $dateFormat = $date->format("Y-m-d H:i:s.u");
        $valuesForInsertion = array(
            "PlayerID" => "13009",
            "SessionID" => "3AE9A53B-091E-43AC-BA28-F95D70F5F660",
            "GroupID" => "3",
            "Username" => "unitTest",
            "SkinID" => "3",
            "IP" => "127.0.0.1",
            "SessionStart" => $dateFormat,
            "LastActivity" => $dateFormat,
            "SelfExclude" => "",
            "DeviceID" => "1123",
            "CurrentGameID" => "0",
            "Status" => "1",
            "ReadyToTruncate" => "0",
            "ScreenSize" => "1920x1080",
        );

        $this->request->openDB("Responsive");
        $query = prepareRowForInsertion("Main.dbo.LoginSession", $valuesForInsertion);
        queryDatabaseWithSQL($this->request->db, $query, true);

        $this->request->setParameters($parameters);
        $method = $this->setMethodAccessible('getContents');
        $method->invokeArgs($this->request, array());

        $ddbbResponse = $this->request->getRawContent();

        $this->assertEquals(0, $ddbbResponse["Code"]);
        $this->assertEquals(
            array("Code", "Balance", "Real", "Bonus", "BonusWins", "Points", "BonusSpins", "Tickets", "PlayerClass", "FundedStatus", "DaysFromRegistration" ),
            array_keys($ddbbResponse)
        );

        $queryDelete = prepareRowForDeletion("Main.dbo.LoginSession", array(
            "PlayerID" => "13009"
        ));
        queryDatabaseWithSQL($this->request->db, $queryDelete, true);
    }

    public function testPrepareResponse()
    {
        $this->request->setRawContent(array(
            "Code" => 0,
            "Balance" => "0.00",
            //...
        ));
        $method = $this->setMethodAccessible('prepareResponse');
        $method->invokeArgs($this->request, array());
        $property = $this->setPropertyAccessible('response');
        $this->assertEquals(array(
            "Code" => 0,
            "Balance" => "0.00"),
            $property->getValue($this->request)
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function testPrepareResponseWithCodeNot0()
    {
        $this->request->setRawContent(array(
            "Code" => 1,
            "Msg" => "Error in the login",
            //...
        ));
        $method = $this->setMethodAccessible('prepareResponse');
        $method->invokeArgs($this->request, array());
        $property = $this->setPropertyAccessible('response');

        $this->assertEquals(null, $_COOKIE["PlayerID"]);
        $this->assertEquals(null, $_COOKIE["SessionID"]);
        $this->assertEquals(array(
            "Code" => 1,
            "Msg" => "Error in the login"),
            $property->getValue($this->request)
        );
    }

    protected function tearDown(){
        unset($this->logoutRequest);
    }
}
