var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var compass = require('compass-importer');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');


browserSync.init({
        injectChanges: true,
        proxy: "responsive.luckyvip.dev"
    });

gulp.task('styles', function() {
    gulp.src('scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        //.pipe(sourcemaps.write())
        // .pipe(autoprefixer('last 2 version', 'ie 10'))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../../public/skins/luckyvip/_css/'))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

//Watch task
gulp.task('default',function() {
    gulp.watch('scss/**/*.scss',['styles']);
    // Watching markup changes, reloading browser
    gulp.watch("../../public/skins/luckyvip/**/*.php").on('change', browserSync.reload);
    gulp.watch("../../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
});
