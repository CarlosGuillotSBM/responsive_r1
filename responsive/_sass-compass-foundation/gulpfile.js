var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var compass = require('compass-importer');
var sourcemaps = require('gulp-sourcemaps');

var proxykittybingo = browserSync.create("kittybingo");
var proxyluckypantsbingo = browserSync.create("luckypantsbingo");
var proxyspinandwin = browserSync.create("spinandwin");
var proxymagicalvegas = browserSync.create("magicalvegas");
var proxybingoextra = browserSync.create("bingoextra");
var proxygivebackbingo = browserSync.create("givebackbingo");
var proxyluckyvip = browserSync.create("luckyvip");
var proxyregalwins = browserSync.create("regalwins");
var proxykingjack = browserSync.create("kingjack");
var proxyaspers = browserSync.create("aspers");

var proxykittybingo_started = false;
var proxyluckypantsbingo_started = false;
var proxyspinandwin_started = false;
var proxymagicalvegas_started = false;
var proxybingoextra_started = false;
var proxygivebackbingo_started = false;
var proxyluckyvip_started = false;
var proxyregalwins_started = false;
var proxykingjack_started = false;
var proxyaspers_started = false;

gulp.task('styles-kittybingo', function() {

    gulp.src('_responsive.kittybingo/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/kittybingo/_css/'))
        .pipe(proxykittybingo.stream({match: '**/*.css'}));
        if(!proxykittybingo_started) {
            proxykittybingo.init({
            proxy: "http://responsive.kittybingo.webdev",
            host: 'responsive.kittybingo.webdev',
            open: 'external'
            });
            proxykittybingo_started = true;
        }
});

gulp.task('styles-luckypantsbingo', function() {

    gulp.src('_responsive.luckypantsbingo/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/luckypantsbingo/_css/'))
        .pipe(proxyluckypantsbingo.stream({match: '**/*.css'}));
        if(!proxyluckypantsbingo_started) {
            proxyluckypantsbingo.init({
            proxy: "http://responsive.luckypantsbingo.webdev",
            host: 'responsive.luckypantsbingo.webdev',
            open: 'external'
            });
            proxyluckypantsbingo_started = true;
        }

});

gulp.task('styles-spinandwin', function() {
    gulp.src('_responsive.spinandwin/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/spinandwin/_css/'))
        .pipe(proxyspinandwin.stream({match: '**/*.css'}));
        if(!proxyspinandwin_started) {
            proxyspinandwin.init({
            proxy: "http://responsive.spinandwin.webdev",
            host: 'responsive.spinandwin.webdev',
            open: 'external'
            });
            proxyspinandwin_started = true;
        }
});

gulp.task('styles-magicalvegas', function() {
    gulp.src('_responsive.magicalvegas/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/magicalvegas/_css/'))
        .pipe(proxymagicalvegas.stream({match: '**/*.css'}));
        if(!proxymagicalvegas_started) {
            proxymagicalvegas.init({
            proxy: "http://responsive.magicalvegas.webdev",
            host: 'responsive.magicalvegas.webdev',
            open: 'external'
            });
            proxymagicalvegas_started = true;
        }
});

gulp.task('styles-kingjack', function() {
    gulp.src('_responsive.kingjack/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/kingjack/_css/'))
        .pipe(proxykingjack.stream({match: '**/*.css'}));
        if(!proxykingjack_started) {
            proxykingjack.init({
            proxy: "http://responsive.kingjack.webdev",
            host: 'responsive.kingjack.webdev',
            open: 'external'
            });
            proxykingjack_started = true;
        }
});

gulp.task('styles-givebackbingo', function() {
    gulp.src('_responsive.givebackbingo/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/givebackbingo/_css/'))
        .pipe(proxygivebackbingo.stream({match: '**/*.css'}));
        if(!proxygivebackbingo_started) {
            proxygivebackbingo.init({
            proxy: "http://responsive.givebackbingo.webdev",
            host: 'responsive.givebackbingo.webdev',
            open: 'external'
            });
            proxygivebackbingo_started = true;
        }
});

gulp.task('styles-bingoextra', function() {
    gulp.src('_responsive.bingoextra/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/bingoextra/_css/'))
        .pipe(proxybingoextra.stream({match: '**/*.css'}));
        if(!proxybingoextra_started) {
            proxybingoextra.init({
            proxy: "http://responsive.bingoextra.webdev",
            host: 'responsive.bingoextra.webdev',
            open: 'external'
            });
            proxybingoextra_started = true;
        }
});

gulp.task('styles-luckyvip', function() {
    gulp.src('_responsive.luckyvip/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/luckyvip/_css/'))
        .pipe(proxyluckyvip.stream({match: '**/*.css'}));
        if(!proxyluckyvip_started) {
            proxyluckyvip.init({
            proxy: "http://responsive.luckyvip.webdev",
            host: 'responsive.luckyvip.webdev',
            open: 'external'
            });
            proxyluckyvip_started = true;
        }
});

gulp.task('styles-regalwins', function() {
    gulp.src('_responsive.regalwins/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/regalwins/_css/'))
        .pipe(proxyregalwins.stream({match: '**/*.css'}));
        if(!proxyregalwins_started) {
            proxyregalwins.init({
            proxy: "http://responsive.regalwins.webdev",
            host: 'responsive.regalwins.webdev',
            open: 'external'
            });
            proxyregalwins_started = true;
        }
});

gulp.task('styles-aspers', function() {
    gulp.src('_responsive.aspers/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('../public/skins/aspers/_css/'))
        .pipe(proxyaspers.stream({match: '**/*.css'}));
        if(!proxyaspers_started) {
            proxyaspers.init({
            proxy: "http://responsive.aspers.webdev",
            host: 'responsive.aspers.webdev',
            open: 'external'
            });
            proxyregalwins_started = true;
        }
});


//Watch task
exports.default = function(done) {
    gulp.watch('_responsive.spinandwin/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-spinandwin'));
    gulp.watch('_responsive.magicalvegas/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-magicalvegas'));
    gulp.watch('_responsive.kingjack/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-kingjack'));
    gulp.watch('_responsive.luckypantsbingo/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-luckypantsbingo'));
    gulp.watch('_responsive.givebackbingo/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-givebackbingo'));
    gulp.watch('_responsive.bingoextra/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-bingoextra'));
    gulp.watch('_responsive.kittybingo/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-kittybingo'));
    gulp.watch('_responsive.luckyvip/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-luckyvip'));
    gulp.watch('_responsive.regalwins/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-regalwins'));
    gulp.watch('_responsive.aspers/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-aspers'));
    // Watching markup changes, reloading browser
    gulp.watch("../public/skins/spinandwin/**/*.php").on('change', proxyspinandwin.reload);
    gulp.watch("../public/skins/magicalvegas/**/*.php").on('change', proxymagicalvegas.reload);
    gulp.watch("../public/skins/kingjack/**/*.php").on('change', proxykingjack.reload);
    gulp.watch("../public/skins/luckypantsbingo/**/*.php").on('change', proxyluckypantsbingo.reload);
    gulp.watch("../public/skins/givebackbingo/**/*.php").on('change', proxygivebackbingo.reload);
    gulp.watch("../public/skins/bingoextra/**/*.php").on('change', proxybingoextra.reload);
    gulp.watch("../public/skins/kittybingo/**/*.php").on('change', proxykittybingo.reload);
    gulp.watch("../public/skins/luckyvip/**/*.php").on('change', proxyluckyvip.reload);
    gulp.watch("../public/skins/regalwins/**/*.php").on('change', proxyregalwins.reload);
    gulp.watch("../public/skins/aspers/**/*.php").on('change', proxyregalwins.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.aspers = function(done) {
    gulp.watch('_responsive.aspers/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-aspers'));
    gulp.watch("../public/skins/aspers/**/*.php").on('change', proxyregalwins.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.bingoextra = function(done) {
    gulp.watch('_responsive.bingoextra/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-bingoextra'));
    gulp.watch("../public/skins/bingoextra/**/*.php").on('change', proxybingoextra.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.givebackbingo = function(done) {
    gulp.watch('_responsive.givebackbingo/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-givebackbingo'));
    gulp.watch("../public/skins/givebackbingo/**/*.php").on('change', proxygivebackbingo.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.kingjack = function(done) {
    gulp.watch('_responsive.kingjack/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-kingjack'));
    gulp.watch("../public/skins/kingjack/**/*.php").on('change', proxykingjack.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.kittybingo = function(done) {
    gulp.watch('_responsive.kittybingo/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-kittybingo'));
    gulp.watch("../public/skins/kittybingo/**/*.php").on('change', proxykittybingo.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.luckypants = function(done) {
    gulp.watch('_responsive.luckypantsbingo/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-luckypantsbingo'));
    gulp.watch("../public/skins/luckypantsbingo/**/*.php").on('change', proxyluckypantsbingo.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.luckyvip = function(done) {
    gulp.watch('_responsive.luckyvip/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-luckyvip'));
    gulp.watch("../public/skins/luckyvip/**/*.php").on('change', proxyluckyvip.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.magicalvegas = function(done) {
    gulp.watch('_responsive.magicalvegas/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-magicalvegas'));
    gulp.watch("../public/skins/magicalvegas/**/*.php").on('change', proxymagicalvegas.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.regalwins = function(done) {
    gulp.watch('_responsive.regalwins/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-regalwins'));
    gulp.watch("../public/skins/regalwins/**/*.php").on('change', proxyregalwins.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

exports.spinwin = function(done) {
    gulp.watch('_responsive.spinandwin/scss/**/*.scss', { interval: 1000 }, gulp.series('styles-spinandwin'));
    gulp.watch("../public/skins/spinandwin/**/*.php").on('change', proxyspinandwin.reload);
    gulp.watch("../public/skins/_global-library/**/*.php").on('change', browserSync.reload);
    done()
};

//  Build tasks for deployment

exports.compile = function(done) {
        gulp.src('_responsive.kittybingo/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/kittybingo/_css/'))

        gulp.src('_responsive.luckyvip/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/luckyvip/_css/'))

        gulp.src('_responsive.regalwins/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/regalwins/_css/'))

        gulp.src('_responsive.bingoextra/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/bingoextra/_css/'))

        gulp.src('_responsive.givebackbingo/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/givebackbingo/_css/'))

        gulp.src('_responsive.magicalvegas/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/magicalvegas/_css/'))

        gulp.src('_responsive.kingjack/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/kingjack/_css/'))

        gulp.src('_responsive.spinandwin/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/spinandwin/_css/'))

        gulp.src('_responsive.luckypantsbingo/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/luckypantsbingo/_css/'))

        gulp.src('_responsive.aspers/scss/main.scss')
        .pipe(sass({ importer: compass }).on('error', sass.logError))
        .pipe(gulp.dest('../public/skins/aspers/_css/'))
    done()
};

gulp.task("compile",exports.compile);
