
/*

	COPY ALL THE GAMES AND GAME CATEGORIES FROM 1 SKIN TO ANOTHER

*/
declare @fromSkinID int = 5
declare @toSkinID int = 6


-- GET GAMES FROM THE DEFAULT SKIN OF 0  
insert  [Responsive].[dbo].[GameCategoryDesc]
select ID,
		@toSkinID 
      ,[ParentID]
      ,[key]
      ,[Description]
      ,[Class]
      ,[SortOrder]
  FROM [Responsive].[dbo].[GameCategoryDesc]
  where skinid=0


insert [Responsive].[dbo].[DeviceGameCategoryDesc]
SELECT [GameCategoryDescID]
      ,[DeviceCategoryID]
      ,[GameID]
      ,@toSkinID
      ,[Position]
      ,[Live]
  FROM [Responsive].[dbo].[DeviceGameCategoryDesc]
  where Live = 0 
  and SkinID = @fromSkinID


/*



update main..skinGame 
set gameicon = sg2.gameicon,
    Thumb = sg2.thumb
from main..SkinGame, main..SkinGame sg2
where main..skinGame.gameid = sg2.gameid
and main..skinGame.SkinID = 6
and sg2.SkinID = 5


--delete  from main..skingame where skinid=6
--delete  from rep..skingame where skinid=6


insert main..skingame
SELECT 6
      ,[GameID]
      ,[WagerWithBonusFunds]
      ,[AllWinsAreReal]
      ,[FundedPlayersOnly]
      ,[PlayerClassIDs]
      ,[GameURL]
      ,[SiteID]
      ,[Status]
      ,[ProgressiveJackpotGroupID]
      ,[DoubleUpBaseWinLimit]
      ,[DoubleUpNonBaseWinLimit]
      ,[Restricted]
      ,[defaultStake]
      ,[defaultLines]
      ,[Icon]
      ,[Thumb]
      ,[GameIcon]
      ,[PayLinesText]
      ,[Feature]
      ,[Blurb]
      ,[Volatility]
      ,[WagerPercent]
      ,[DemoPlay]
  FROM [Main].[dbo].[SkinGame]
where SkinID=5


*/