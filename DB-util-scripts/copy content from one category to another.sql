
/*
	COPY CONTENT FROM ONE staging CATEGORY TO ANOTHER staging category
	
*/
declare @sourceCategoryID int = 1551
declare @destCategoryID int = 135

declare @c table(id int)
declare @cID int
declare @lastID int
declare @destCategoryLiveID int


insert @c
select ID from Content where ContentCategoryID = @sourceCategoryID


select @destCategoryLiveID = liveID from ContentCategory where ID = @destCategoryID


while (select count(*) from @c) > 0
begin

	SELECT TOP 1 @cID = ID from @c  	
	delete from @c where id=@cID	
	
	select @lastID = Max(id)+1 from Content  
	INSERT Content(ID,ContentCategoryID,Device,ContentCategoryTypeID,Name,Title,Image,ImageAlt,Image1,ImageAlt1,DetailsURL,Intro,Body,Terms,Text1,Text2,Text3,Text4,Text5,Text6,Text7,Text8,Text9,DateFrom_DST,DateTo_DST,SortOrder,StateID,StatusID,StagingID,userID)
	SELECT @lastID,@destCategoryID,Device,ContentCategoryTypeID,Name,Title,Image,ImageAlt,Image1,ImageAlt1,DetailsURL,Intro,Body,Terms,Text1,Text2,Text3,Text4,Text5,Text6,Text7,Text8,Text9,DateFrom_DST,DateTo_DST,SortOrder,1,StatusID,@destCategoryLiveID,userID
	FROM Content 
	where ID = @cid		
	
	-- might be double record so lets check
	if not exists(SELECT @lastID,contentDay from Content c (nolock) 
				join contentDay cd (nolock) on cd.contentID = c.ID
				where c.ID = @lastid 
				and c.StatusID = 1)
	begin						
		INSERT ContentDay
		SELECT distinct @lastID,contentDay from Content c (nolock) 
		join contentDay cd (nolock) on cd.contentID = c.ID
		where c.ID = @cid 
		and c.StatusID = 1
	end			
end	