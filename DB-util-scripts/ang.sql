USE [Responsive]
GO
/****** Object:  StoredProcedure [dbo].[DataCheckPromoCode]    Script Date: 06/26/2015 09:48:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
Author:      CM
Date       : 26may15
Mod date(s): 
Description: Called by MyAccount to validate player based promo codes.
Called From: 

Result =   

[DataCheckPlayerPromoCode] 422,'hamish','en',''

=============================================*/
CREATE PROCEDURE [dbo].[DataCheckPlayerPromoCode]
  @PlayerID INT,
  @PromoCode VARCHAR(20),
  @lang  varchar(2),
  @IP varchar(16)
  
AS
BEGIN
SET NOCOUNT ON;
--#--
declare @promoTable udt.PromoTable
declare @msg varchar(200)
declare @today datetime = cast(getdate() as date)

-- check how many times attempted ( max 3 per day )
if (select COUNT(*) from Main.dbo.EventLog with (nolock) where entityID = @playerID and EventLogTypeID in (424,425) and cast([DATE] as DATE) = @today ) >= 3
begin

	set @msg = 'Player validated ' + @PromoCode + ' promocode with failure - too many attempts today'
	EXEC main.dbo.svrEventLog @EventLogTypeID = 425, @EntityID = @PlayerID, @IP = @IP, @Details = @msg
	
	select 'Code' = 2,
		   'Msg'  =	fn.GetEventMessage(@lang,'PROMO_CODE_CHECK_FAIL_TO_MANY_TRIES')
	return
end


insert @promoTable 
select * from main.[fn].[GetPromo](@PlayerID,2,0,1,@PromoCode)





if @@ROWCOUNT > 0
begin

	set @msg = 'Player validated ' + @PromoCode + ' promocode with success'
	EXEC main.dbo.svrEventLog @EventLogTypeID = 423, @EntityID = @PlayerID, @IP = @IP, @Details = @msg

	select 'Code' = 0
	
end	
else
begin

	set @msg = 'Player validated ' + @PromoCode + ' promocode with failure'
	EXEC main.dbo.svrEventLog @EventLogTypeID = 424, @EntityID = @PlayerID, @IP = @IP, @Details = @msg

	select 'Code' = 1,
			'Msg' = fn.GetEventMessage(@lang,'PROMO_CODE_CHECK_FAIL')	
end	

RETURN
--#--
END

