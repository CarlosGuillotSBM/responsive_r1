		--DWH Code for Table Modifications
			DECLARE @PlayerIDs udt.CogsIntTableType
		

INSERT INTO @PlayerIDs SELECT ID FROM (SELECT p.ID 
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '7%' and LEN(RTRIM(p.MobilePhone)) = 10 and p.CountryCode = 'GBR'
UNION
select p.ID   
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '07%' and LEN(RTRIM(p.MobilePhone)) = 11 and p.CountryCode = 'GBR'
UNION
select p.ID  
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '007%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
UNION
select p.ID   
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '447%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
UNION
select p.ID  
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '0447%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select p.ID   
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '4407%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select p.ID  
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '00447%' and LEN(RTRIM(p.MobilePhone)) = 14 and p.CountryCode = 'GBR') aux



			EXEC Main..dwhModsBulk @DBName = 'Main', @TableName = 'Player', @ID =0 ,@IDs = @PlayerIDs  