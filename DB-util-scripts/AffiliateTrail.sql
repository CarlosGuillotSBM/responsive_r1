
		SELECT  

				at.PlayerID,
				at.[Date] AS FCDate,
				at.Referrer,
				at.AffiliateID AS SubCampaignID,
				p.SkinID,
				c.AffiliateID as AffiliateID,
				at.Status

				
		FROM Rep.dbo.AffiliateTrail at (NOLOCK)
		JOIN Rep.dbo.Player p (NOLOCK) ON p.ID = at.PlayerID
		JOIN stb_data.dbo.SubCampaigns sc (NOLOCK) ON sc.ID = at.AffiliateID
		JOIN stb_data.dbo.Campaigns c (NOLOCK) ON c.ID = sc.CampaignID
		WHERE at.[Date] < '2016-04-21'
		ORDER BY PlayerID ASC , FCDate ASC
