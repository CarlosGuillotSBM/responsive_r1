USE [Main]
GO
/****** Object:  StoredProcedure [dbo].[xTxGame]    Script Date: 03/24/2016 16:36:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=====================================================================================================
Author:      AF
Date       : 21Feb11
Description: Game transaction recorder based on Promo and Bonus Model 'F' rules
             Checks and updates bet limits, player balances
             Records a game transaction pro-rating the wagers/wins with real/bonus funds based on the Promo and Bonus Model 'F' rules
             Records game voids (refunds)
             Updates any bonus win applications
             Updates any Bonus on deposits wagering applications
             Uses TxGameRound to summarise the game round assuming a game round may consist of many TxGame records
             for various multi state game plays

Called From: Our servers games provider API's (eyecon.php, igt.php, sheriff.php ...)
EventLogTypeID used in sproc:
104 Invalid login session - PlayerID/SessionID not found
105 Invalid login session - session expired
300 Game - Failed to write transaction
301 Game - Game not found
302 Game - Game disabled
303 Game - Player invalid
304 Game - Funds available for game less than wager
305 Game - Wager < 0
306 Game - Win < 0
307 Game - duplicate game provider Ref and sub ref (the transaction is already written)
308 Game - PlayerLimits exceeded
309 Game - fail due to player balance difference at time of update
313 Game - Void transaction failure because there were no wagers for the game
315 Game - Failed to update TxBonus transaction(s)
316 Game - Game transaction for 'Recon'/'EndGameRound' not found
317 Game - fail due to unmatched application of Wager to player balances
319 Game - fail due to unmatched application of Win to player balances
Returns:
  Result =   0 : Success
           104 : Invalid login session - PlayerID/SessionID not found
           105 : Invalid login session - session expired
           300 : Failed to write transaction (general database error)
           301 : Game not found
           302 : Game disabled
           303 : Player not found
           304 : Funds available for game less than wager
           305 : Wager < 0
           306 : Win < 0
           307 : duplicate game provider Ref and SubRef (the transaction is already written)
           308 : PlayerLimits exceeded
           309 : fail due to player balance difference at time of update
           313 : Void transaction failure because there were no wagers for the game
           315 : Failed to update TxBonus transaction(s)
           316 : Game - Game transaction for 'Recon' not found
           317 : Game - fail due to unmatched application of Wager to player balances
           319 : Game - fail due to unmatched application of Win to player balances
  Ref  (our reference - TxGame.ID)
  Balance
  Real
  Bonus
  BonusWins
  WagerReal
  WagerBonus
  WagerBonusWins

02Nov11 AF - Added MGS specific requirements to CheckSession
           - Added Action RefreshSession for MGS
           - made BIGINTS where required
03Nov11 AF - Adjusted the check on existing TxGameRound to check MGS specifically for a complete game (Status=1)
17Nov11 AF - Added ProgressiveJackpotWagerPercent awareness
29Nov11 AF - Set TxGameRound.Status=1 when an IGT game and SubRef = 999
02Dec11 AF - @Loss can be negative
30Jan12 AF - @PromoWin is converted to money when comparing its amount in the while clause
15Mar12 HI - @ProgressiveJackpotWagerPercentData fixed rounding bug and increased precision
26Feb12 AF - Only evaluate TxPromo cancellations is @TxGameRoundStatus = 1
             Done to address the problem where a wager precedes a win in a seperate tx, and the bonuses are cancelled because balances are zero, but the win tx has not come yet
27Feb12 AF - Fixed @WinReal setting bug where @WinReal was set for a special rule#2 bonus and aded to thereafter
29Feb12 AF - Added MaxBonus to evaluation of promos
             For a promo where ConvertableToReal is restricted to MaxBonus, remove excess from BonusWins
05Mar12 AF - Made MGS EndGameRound independent of TxGameRound status
             ConvertableToReal and MaxConvertableToReal changes
06Mar12 AF - Added update of @TxGameRoundStatus in UPDATE TxGameRound
07Mar12 AF - Line 1054 modified RealBal limit for special rule #2
             BonusBalTransferredToReal and BonusWinsBalTransferredToReal in TxPromo cannot be < 0
08Mar12 AF - Deals with 317 : Game - fail due to unmatched application of Wager to player balances - usually where a wager is placed with bonus and there are no active promos
             Altered BonusVoidBalanceThreshold to operate per promo, not playerclass
09Mar12 AF - Changed block dealing with 'UPDATE dbo.TxPromo wagering requirements based on @Wager on FIFO basis' to ignore IsCashbackBonus if already chacked as the amount to apply could be zero - this was causing deadlocks and timeouts
13Mar12 AF - Added WagerWithBonusFunds Checks
15Mar12 AF - For MGS GetBalance use Game.ID via proxt GameProviderGameID
             Removed the promo rule that does not count cashback wagers on cashback bonuses
             Special rule #1 no longer applies
             @ProgressiveJackpotWagerPercentData=smallint
19Mar12 AF - Added recording to TxPromoDelta to track TxPromo movements to assist debugs (will be dropped when assessed as correct)
20Mar12 AF - for MGS EndGame calls, if SessionID not found, look on Rep.LoginSession
21Mar12 AF - Recon, Void, VoidCogs or EndGame calls, Player.Status can be anything AND if SessionID not found, look on Rep.LoginSession
22Mar12 AF - Added var to decide if we are going to track promo movements in TxPromoDelta
23Mar12 AF - Removed WITH (NOLOCK) from selects from TxGameRound, TxGame, TxPromo as dirty reads were occurring
28Mar12 AF - Included CompetitionTriggers
           - Added code to write to TxGame.TxGameRoundStatus
29Mar12 AF - Recoded how the wager/wins are applied
             Added pre balance columns to TxPromoDelta
             Added eventcode = 319 where total @Win is not applied
30Mar12 HI - Turned on the tournament triggers
02Apr12 AF - Removed update of TxGameRoundStatus in UPDATE TxGame - not needed for CDC
03Apr12 HI - updated comp triggers to pass @txGameRoundWager and @TxGameRoundWin instead of @Wager and @win
04apr12 AF - Removed insert of TxGameRoundStatus on TxGame
20Apr12 AF - Added insert to TxGameRoundReplicate table
30Apr12 AF - TxPromoReplicateUpdate
09may12 HI - Seprated wager/win and bingo wager / win
11May12 AF - Streamlined 09May12 HI changes
             Allow MGS Checksession to probe rep (where they come in for a playassist check)
23May12 AF - Removed WagerBonusCashBack and IsCashBack details which are not applicable and may be adversly affecting the sproc logic
15Jun12 AF - Conditional EndGameRound based ON GameProvider FOR competition triggers - MGS specifically
29Jun12 AF - When Promo.ConvertableToReal = 0, do Not record TxPromo.BonusBalTransferredToReal as this is zero
02Jul12 AF - Coded Sheriff 2.1 API changes
05Jul12 AF - fully qualified object NAMES
           - Added set deadlock
19Jul12 AF - Corrected TxGameRoundReplicate INSERT for Wagers
24Jul12 AF - Corrected TxGameRoundReplicate INSERT for all Wagers
30Jul12 HI - Added TxWagerTypeID, TxWinTypeID parameters for Specific win and wager TxTypes
31Jul12 AF - Deal with new TxWagerTypeID, TxWinTypeID parameters. IF they are zero, SET to DEFAULT (10=wager, 11=win)
02Aug12 AF - IF AllWinsAreReal=1 wins GO TO Real
11Aug12 AF - TxEndGameRound @Action added
13Aug12 AF - Grease stuff added
15Aug12 AF - Grease jackpot added
             Sheriff games RETURN realwin/bonuswin SPLIT
16Aug12 AF - Grease TxGreaseJackpotReplicate
17Aug12 AF - Baddamedia - IF same ref/subref - regard AS a 'recon'
22Aug12 AF - Baddamedia - added Id AND DateStamp fields TO txgamegreasejackpot
30Aug12 AF - Balance available FOR a game involving Bonus funds depends ON IF the promo allows the bonus funds TO be USED ON the specific game
             MaxConvertableToReal is a percentage IF Promo.MaxConvertableToReal BETWEEN 0.01 AND 1.00 (1% and 100%)
10Sep12 AF - Win applied TO promos checks FOR restricted game promos FIRST
             Removed BalanceControl checks
12Sep12 AF - Added GreaseJackpot TO @Win
21Sep12 AF - Where real funds are used for wagers, and deposit promos to not apply to the game, check that the realbal of the active deposit promo(s) do not exceed the Player.realBal
24Sep12 HI - Added return of CoinSizeString, MaxWin, MaxDoubleUp
27Sep12 HI - removed return of CoinSizeString, MaxWin, MaxDoubleUp
11oct12 HI - Record 0 wager transactions
16Oct12 AF - Added Ash Gaming
             Added Refund TO RETURN
19Oct12 AF - Record 0 wager transactions ONLY FOR Bingo
05Nov12 AF - Added jackpotwinner RETURN data FOR Baddamedia responses
09Nov12 AF - New @TxWinTypeID = 63 FOR games that must ONLY payout AS a bonus win - regardless OF the wagers, Allwinsarereal AND ANY other settings.
INSERT promotype SELECT 12,'Promo from Bingo Win as Bonus Funds','10:51',1
14Nov12 HI - add @win=0 line 2171, to stop double tx's on paying a bingo win.
26Nov12 AF - Added GreaseMini (7002) conditional processing
28Nov12 AF - Only consider Grease progressive jackpot if @WagerReal > 0
           - Grease jackpot calculations are not rounded
01Dec12 AF - IGT does not use SubRef=-1 or 999 - it uses the IGT actionId (the subref being the number after the '-' + 1 as we dont have subref's OF 0)
07Dec12 AF - Bally - Different RETURN fields, uses TxGameRefBally mapping TABLE TO generate UNIQUE TxGameRef bigint's
16Jan13 AF - Removed Ash conditional processing as Ash will be on ReelRunner
04mar13 UB - updated gamprovidergameid length
14Mar13 AF - Bingo wins are NOT real if the player is unfunded
08Apr13 HI - Send a mail when a grease PJ is won - edited put gameroundID not GameID
30Apr13 AF - Removed PJGrease and TxGameGrease* stuff as the PJ's have ALL moved TO the SLOT DATABASE
06May13 AF - Promos ARE got WITH Main.fn.GetPromo
07May13 AF - Write to PromoLowDepositOffer IF there IS a LOW balance deposit offer AND the players balance goes below the threshold AS hord coded AS @LowBalancePromoThreshold
20May13 AF - If Bally send a gameCycleId, get it or insert it if it does not exist and set @Ref - selects TOP 1 TxGameRef... TO avoud SQL ERROR * Error: SQLSTATE: 21000 code: 512 message: [Microsoft][SQL Server Native Client 11.0][SQL Server]Subquery returned more than 1 value. This is not permitted when the subquery follows =, !=, <, <= , >, >= or when the subquery is used as an expression
21May13 AF - WITH (NOLOCK) USED ON TxGameRound AND TxGame selections (see {AF 21May13})
21May13 HI - added ERROR CATCH WITH log around txPromo Problems
23May13 (22:52) AF - removed ERROR CATCH WITH log around txPromo Problems
                     removed WITH (NOLOCK) --{AF 21May13 as we got duplicates! ***NEVER NOLOCK THIS - NEVER EVER***
24May13 AF - Look FOR Bally TxGameCycleID WITH a LOCK
24May13 AF - If Eyecon ref/subref is found treat as recon
24May13 AF - Dont write to TxPromoReplicateUpdate - this gets written to when the TxPromo is finished (i.e. Status is not 2 or 4)
           - added with (ROWLOCK) to selects from TxPromo
28May13 AF - WITH (ROWLOCK) --{AF 28May13 - added WITH (ROWLOCK)} for selects from TxGame
29May13 AF - Eyecon Voids - if @Refund=0 set @Refund to the @TxGameRoundWager
06Jun13 AF - IF a Bally game, check the TxPlayerBally table TO ensure the STATUS=0. SET Status=1 WHEN this PROC starts AND =0 WHEN exiting the PROC OR logging IN.
             IF attempting TO EXEC this sproc AND STATUS=1, WRITE a LOG TO TxPromoDelta about it.
07Jun13 AF - IF a Eyecon void is received AND we already have the TRANSACTION send a success MESSAGE rather than an error
27Jun13 AF - If a Player.PlayerClassID is a lower class (PlayerClass.SubClass < 2) then override the AllWinsAreReal to = 0
             Between the local hours of 01:30 and 07:00 override the AllWinsAreReal to = 0
28Jun13 AF - Removed 01:30am thing
06Aug13 AF - @ProgressiveJackpotWagerPercent = decimal (7,4) was decimal (4,3)
20Aug13 AF - GameProviderGameID FOR IGT is actually our Main.Game.ID so we can distinguish BETWEEN their html5 AND flash games
22Aug13 AF - FOR IGT Recon's we need to search PlayerID+TxGameRef for the same GameProviderID to establish the correct Game.ID
30Aug13 AF - PlaynGo
04Sep13 AF - More PlaynGo
09Sep13 AF - Some More PlaynGo
16Sep13 AF - If TxGameRound.Status changes and date <> today, write a daga.mods record
24sep13 HI - PlaynGo moved to gameproviderID 14, references updated
25Sep13 AF - Introduced @LastActivePromoID in the block that cancels Promos if players total balance < 1 - dont cancel the most recent promo if its a bingo game and @Win=0
26Sep13 AF - Removed @LastActivePromoID in the block that cancels Promos if players total balance < 1 - dont cancel any promos if its a bingo game and @Win=0
28Sep13 AF - Introduced @FirstActivePromoID in the block that cancels Promos if players total balance < 1 - dont cancel the oldest active promo if its a bingo game and @Win=0
06Oct13 AF - Use PlaynGoGameSession record to get our players current session because PlaynGo have such a rubbish integration
             NOTE - we assume a player can only ever have one active playngo game going at one time
07Oct13 AF - Fixed PlaynGo Subref for voids
15Oct13 AF - Assume PlaynGo TransactionId is a bigint and use this for Subref - remove mapping table and remove @PlaynGotransactionId input
22Nov13 AF - Final statement of sproc - IF @GameProviderID NOT IN (3,9,14) --others --- was IF @GameProviderID NOT IN (3,9,12)
11Feb14 AF - Commented out svrCheckPlayerLoginSession block with the PlaynGo if statement as we assume a valid session is required IF --(@GameProviderID = 14) OR 
12mar14 HI - line 529: get the default params for bally games getbalance from game/skingame so players can use bonus funds when configured to instead of always being denied
04Apr14 AF - SET @ThisGameHasRestrictedPromos = 1 IF there ARE any ACTIVE promos WITH RestrictToGames=1 AND @Win > 0
12may14 HI - fixed int conversion bug for IGT voids.
13may14 HI - fixed IGT voids.
13May14 AF - Altered the (stupid) promo SYSTEM rules IN that IF a wager is placed WITH real money FOR bingo (regardless of whether linked to a deposit promo or not), and there is a win from such real money wager it gets applied to Real directly
03Jun14 AF - Altered the (stupid) promo SYSTEM rules IN that IF a wager is placed WITH real money FOR ANY GAME (regardless of whether linked to a deposit promo or not), and there is a win from such real money wager it gets applied to Real directly
09Jun14 HI - Get WagerPercent from SkinGame instead of game.
12Jun14 HI - reject GetBalance for eyecon games if player unfunded and game + skingame.FundedPlayersOnly  = 2
01Jul14 RS - Added GameID TO PlayNGo query TO GET SESSION so players could have more than one game OPEN at a TIME.
27Jul14 RS - Added lose count trigger for competitions engine
04Aug14 RS - Turned lose count to lose amount
03Sep14 AF - For Bingo wagers, TxGameRound.Status=2. Only on a Bingo Win do we Add the Bingo TxGameRound.TxGameRef to TxBingoGameRef so we can mark related TxGameRounds at readytotruncate time. This is to ensure Main..TxGameRound record exists for prebuy games
14Oct14 AF - Added specifics for NetEnt (GameProviderID = 17)
15Oct14 AF - Removed TxGameRound and TxGame Rep replication
23Oct14 RS - Added GameID TO PlayNGo query TO GET SESSION so players could have more than one game OPEN at a TIME.
24Oct14 AF - Get @SessionID from LoginSession if NetEnt dont send the gameId.
25Oct14 AF - Get @SessionID from Main..Login Session and then Rep..LoginSession if not found in PlaynGoGameSession for PlaynGo and NetEnt (in case its not written properly or cleared etc). Netent dont always send the gameid
29Nov14 AF - Added line for IGT games recon or void where we now have the channel ('MOB' or 'INT') suffixed onto end of GameProviderGameID
             see line ... Game.ID >= CASE RIGHT(@GameProviderGameID,3) WHEN 'MOB' THEN 4500 ELSE 4000 END
02Dec14 AF - Added SkinGame.Status condition check to IGT game lookup for Recons and Voids
04Dec14 AF - in the case of eyecon, the win must be identical, otherwise its a new transaction that we have not recorded - typically a 'feature win' for recons
20Jan15 AF - Same NetEnt Ref and SubRef is a recon
23Jan15 AF - NetEnt allow calls to inactive players accounts
           - NetEnt has NetEntGameSession table in main with one record per player
           - If there is no NetEnt SessionID found, the Player.Status MUST be inactive
04Mar15 RS - Added logic so, for Geco games, if a Tx call is duplicated but using same win and wager, we don'T throw ERROR AND SKIP processing the TRANSACTION. IF either wager OR win is different FROM the recorded txn, we throw ERROR.
13Mar15 RS - Added logic so, for Realistic games, if a Tx call is duplicated but using same win and wager, we don'T throw ERROR AND SKIP processing the TRANSACTION. IF either wager OR win is different FROM the recorded txn, we throw ERROR.
16apr15 HI - DEV-1925. Make it so PlayerLimits of 0 = unlimited.
30Apr15 RS - Bug fix. Changed conditions for when a transsaction is already FOUND FOR specific game providers TO ONLY consider the Void TYPE FOR that PROVIDER (Added a parenthesis block)
15may15 RS - Added idempotency IN Voids FROM Realistic, as per their request
08Jun15 AF - Do not allow NetEnt calls if a player status is disabled.
17jun15 RS - Added out of session request for Realistic, so they can end rounds after session has died OUT
23Jul15 AF - CHECK GameProvider.Status
03sep15 HI - return country, language and currency on getbalance for scientific games
03Sep15 AF - BonusWinsBal on TxPromos are assumed zero for special rule#2 promos statuses (TxPromo.Status = 4)
             udt.TxPromoUpdateTable now includes BonusBal and BonusWinsBal to ensure Rep is updated properly
08oct15 RS - DEV-3265 Handling sessions for Scientific Games
12oct15 RS - DEV-3265 Stop handling sessions for Scientific Games as it's not needed
30oct15 HI - fix bally get balance - it was picking up the status of always the last game found     
02feb16 RS - Included mgs-ainsworth in mgs special conditions        
05feb16 HI - return success for Scientific Games Voids if we can't find the record. 
05feb16 HI - to stop truncation problems for Scientific games pass the ref in RefundNotes
29feb16 RS - DEV-4809 Included mgs-ainsworth in TxGameRound creation
11mar16 CM - DEV-4946 Stopping players to play games if they have the given gameID blocked by the Reality Check process (look for DEV-4946 in the codes)
=====================================================================================================*/
ALTER PROCEDURE [dbo].[xTxGame]
@PlayerID INT,
@SessionID UNIQUEIDENTIFIER,      -- comes in from PlaynGo as 00000000-0000-0000-0000-000000000000 as we have to get it from PlaynGoGameSession
@IP VARCHAR(15),                  -- this is game providers server IP address
@GameProviderID INT,              -- as set at in the php API script
@GameProviderGameID VARCHAR(100), -- the game providers game id (Or Game.ID for MGS GetBalance calls or Game.ID for all IGT and PlaynGo calls)
@GameLaunchSourceID TINYINT,      -- where the game was launched from in the website (0=unknown)
@Action VARCHAR(15),              -- Tx | TxEndGameRound | GetBalance | CheckSession | RefreshSession | Recon | Void (from a game provider) | VoidCogs (from cogs) | EndGameRound
@Wager MONEY,
@Win MONEY,

@Ref BIGINT,                     -- the game providers reference for the transaction. A single game ROUND must have the SAME Ref
                                 -- Note that MGS may sometimes send the same @Ref for a NEW GAME ROUND. (The SubRef is guaranteed unique) - in this case, we check the TxGameRound.Status of the existing @Ref - if the Status=1 (complete), we start a new game round

@SubRef BIGINT,                  -- The game providers sub reference for the transaction. GameID+Ref+SubRef must be unique
                                 -- Passing a SubRef of -1 will flag this sproc to auto generate a SubRef - this is currently used
                                 -- by Bingo and IGT games where the IGT txnId already exists, and the RGSGame "finished" attribute is "false"

@Refund FLOAT,                   -- Refers to Wager-Win to refund where @Action=Void. And could be negative in rare circumstances.
                                 -- Where negative, do not remove a win from players balance, but record the tx.

@RefundRef VARCHAR(50),          -- Only applicable where @Action = "Void" | "VoidCogs"
@RefundNotes VARCHAR(4000),      -- Only applicable where @Action = "Void" | "VoidCogs" - also used for TransactionID for SG because the ID is being truncated in PHP
@TxWagerTypeID INT = 0,          -- The TxTypeID of the Wager if a specific ID required else default (10)
@TxWinTypeID INT = 0,            -- The TxTypeID of the Win if a specific ID required else default (11)
@BallygameCycleId UNIQUEIDENTIFIER = NULL --only for bally games where their 'Ref' is a uniqueid and we convert to a bigint using a mapping table TxGameRefBally

AS
BEGIN
SET NOCOUNT ON;
SET DEADLOCK_PRIORITY 10 --Highest priority - this sproc will never be a victim
--#--
DECLARE @TrackPromoDelta BIT = 0 -- Debug table TxPromoDelta to record TxPromo movements. Set to zero unless problems reported.

DECLARE @Result INT = 0          -- 0 means all good, <> 0 indicates an error
DECLARE @Data VARCHAR(1000) = ''
DECLARE @DebugData VARCHAR(1000) = '' --Holds additional debug data on errors
DECLARE @EventLogTypeID INT = 0 --Main.EventLogType
DECLARE @EntityID BIGINT = 0
DECLARE @Today DATE = GETDATE()

-- for scientific games get the ID from @RefundNotes because the ID needs to be passed as a string because of truncation in PHP
/*
if @GameProviderID = 19 -- scientific games
begin
	if @RefundNotes <> ''
		set @Ref = @RefundNotes
end
*/

/*
Get @SessionID and @GameLaunchSourceID from PlaynGoGameSession or NetEntGameSession for PlaynGo games AND NetEnt games
*/
IF @GameProviderID IN (14,17)--PlaynGo, NetEnt, SG
BEGIN
  SET @SessionID = NULL

  IF @GameProviderID=14
    SELECT @SessionID=SessionID,
           @GameLaunchSourceID=GameLaunchSourceID
    FROM PlaynGoGameSession WITH (NOLOCK)
    WHERE PlayerID = @PlayerID AND GameProviderGameID = @GameProviderGameID

  ELSE IF @GameProviderID=17
    SELECT @SessionID=SessionID,
           @GameLaunchSourceID=GameLaunchSourceID,
           @GameProviderGameID = CASE WHEN @GameProviderGameID='' THEN GameProviderGameID ELSE @GameProviderGameID END
    FROM NetEntGameSession WITH (NOLOCK)
    WHERE PlayerID = @PlayerID
    
  IF @SessionID IS NULL
  BEGIN
    --Check Main..LoginSession
    SELECT TOP 1
           @SessionID = SessionID,
           @GameLaunchSourceID=0
    FROM Main..LoginSession WITH (NOLOCK)
    WHERE PlayerID = @PlayerID AND Status = 1
    ORDER BY ID DESC

    --Check Rep..LoginSession
    IF @SessionID IS NULL
      SELECT TOP 1
             @SessionID = ISNULL(SessionID,'00000000-0000-0000-0000-000000000000'),
             @GameLaunchSourceID=0
      FROM Rep..LoginSession WITH (NOLOCK)
      WHERE PlayerID = @PlayerID
      ORDER BY ID DESC
  END
END

/*
Check LoginSession (unless its a Recon call (from IGT or Sheriff or Bally or Eyecon)
or a VoidCogs in which case the cogs guid is checked)
*/
IF Main.fn.isCogsGuid(@PlayerID,@SessionID) = 1
  SET @Result = 0
ELSE
  BEGIN
    EXEC @Result = dbo.svrCheckPlayerLoginSession @PlayerID, @SessionID, @IP

    IF @Result <> 0 --Session invalid (-10 or -11)
      BEGIN
        IF (@GameProviderID = 14) OR
           (@GameProviderID IN(6,21) AND @Action = 'CheckSession') OR        
           (@GameProviderID =  16 AND @Action = 'TxEndGameRound') OR
           (@GameProviderID =  19 AND @Action = 'TxEndGameRound') OR
           (@Action IN ('EndGameRound','Void','Recon')) --the players session may not exist so check Rep.LoginSession to find a valid session so we can close the game
            BEGIN
              IF EXISTS (SELECT 1 FROM [Rep].[dbo].[LoginSession] WITH (NOLOCK) WHERE PlayerID = @PlayerID AND SessionID = @SessionID)
                SET @Result = 0
            END

        --If a NetEnt call and there is no playersession, allow it IF the player status is inactive as NetEnt sometimes makes calls to inactive players
        --***NO - changed on 08Jun15 as players can still play netent games if they are disabled from cogs while online.
        --IF @GameProviderID = 17 AND (SELECT [Status] FROM Player WITH (NOLOCK) WHERE ID = @PlayerID) <> 1
        --  SET @Result = 0

        IF @Result <> 0 --Session invalid (-10 or -11)
          BEGIN
            SELECT 'Result'  = CASE @Result WHEN -10 THEN 104 ELSE 105 END,
                   'agsRef'  = 0,
                   'Balance' = NULL,
                   'Real'    = NULL,
                   'Bonus'   = NULL,
                   'BonusWins' = NULL,
                   'WagerReal' = NULL,
                   'WagerBonus' = NULL,
                   'WagerBonusWins' = NULL,
                   'Refund' = NULL,
                   'GetLowBalancePromoID' = 0
            RETURN
          END
      END
  END

--Bally Player Check
IF @GameProviderID = 9
BEGIN
  DECLARE @TxPlayerBallyStatus BIT = (SELECT Status FROM TxPlayerBally WITH (ROWLOCK) WHERE PlayerID = @PlayerID)
  IF @TxPlayerBallyStatus IS NULL
    INSERT TxPlayerBally (PlayerID,Status) VALUES (@PlayerID,1)
  IF @TxPlayerBallyStatus = 0
    UPDATE TxPlayerBally WITH (ROWLOCK) SET Status=1 WHERE PlayerID = @PlayerID
  IF @TxPlayerBallyStatus = 1
    BEGIN
      INSERT TxPromoDelta SELECT 0,@PlayerID,GETDATE(),0,0,0,0,0,0,'Action:'+@Action+' Ref:'+CAST(@Ref AS VARCHAR(10))+' SubRef:'+CAST(@SubRef AS VARCHAR(10))+' BallygameCycleId:'+CAST(@BallygameCycleId AS NVARCHAR(50))
      SET @EntityID = @PlayerID
      SET @EventLogTypeID = 300
      GOTO ReturnData
    END
END

--Voids cannot have wagers/wins - the data is taken from the recorded TxGame record(s)
IF SUBSTRING(@Action,1,4) = 'Void'
  BEGIN
    SET @Wager = 0
    SET @Win = 0
  END

--Session is good, return if CheckSession or RefreshSession
IF @Action IN ('CheckSession','RefreshSession')
  BEGIN
    IF (@GameProviderID IN (6,21,17)) AND @Action = 'CheckSession' --MGS, mgs-ainsworth and NetEnt need particular return data
      BEGIN
        DECLARE @pUsername NVARCHAR(15)
        DECLARE @pCurrencyCode CHAR(3)
        DECLARE @pCountryCode CHAR(2)
        DECLARE @pCity NVARCHAR(50)
        --DECLARE @pDOB VARCHAR(10)
        --DECLARE @pRegDate VARCHAR(10)
        --DECLARE @pGender CHAR(1)
        --DECLARE @pBalance MONEY

        SELECT @pUsername = Player.Username,
               @pCurrencyCode = Player.CurrencyCode,
               @pCountryCode  = Country.Code2,
               @pCity = Player.City
               ----@pDOB = '1980-04-19T00:00:00.000',--convert(varchar(50),Player.DOB,127)
               --@pDOB = '1980-04-19',--convert(varchar(50),Player.DOB,120) select convert(varchar(10),getdate(),120)
               --@pRegDate = convert(varchar(10),Player.Date,120),
               --@pGender = case Player.Gender when 0 then 'f' else 'm' end,
               --@pBalance=daga.fn.formatMoney(Player.Real,0)
          FROM  dbo.Player WITH (NOLOCK) JOIN dbo.Country WITH (NOLOCK) ON Country.Code = Player.CountryCode
          WHERE Player.ID = @PlayerID AND
                Player.Status = CASE @GameProviderID WHEN 17 THEN Player.Status ELSE 1 END

        IF @@ROWCOUNT=0 OR @pUsername = NULL
          SET @pUsername = NULL

        --Player invalid - does not exist or status <> 1
        IF @pUsername IS NULL
          BEGIN
            SET @EntityID = @PlayerID
            SET @EventLogTypeID = 303
            GOTO ReturnData
          END

        SELECT 'Result'       = @Result,
               'Username'     = @pUsername,
               'CurrencyCode' = @pCurrencyCode,
               'CountryCode'  = @pCountryCode,
               'City'         = @pCity
               --'DOB'          = @pDOB,
               --'RegDate'      = @pRegDate,
               --'Gender'       = @pGender,
               --'Balance'      = @pBalance
        RETURN
      END
    ELSE
      BEGIN
        IF @GameProviderID = 9
           UPDATE TxPlayerBally WITH (ROWLOCK) SET Status=0 WHERE PlayerID = @PlayerID
        SELECT 'Result'    = @Result
        RETURN
      END
  END

--Game and Player details
DECLARE @SkinID INT = NULL
DECLARE @PlayerCurrencyCode VARCHAR(3)
DECLARE @PlayerCountryCode VARCHAR(2)
DECLARE @PlayerLanguageCode VARCHAR(2)
DECLARE @PlayerDOB VARCHAR(10)
DECLARE @PlayerRegDate VARCHAR(10)
DECLARE @PlayerGender CHAR(1)
DECLARE @PlayerFunded BIT
DECLARE @UKDate DATETIME = daga.fn.toDST(GETDATE()) --dst adjusted uk date
DECLARE @AllWinsAreNotRealTimeFrom DATETIME = CAST(CONVERT(CHAR(11),@UKDate,106)+ ' 01:30:00' AS DATETIME)
DECLARE @AllWinsAreNotRealTimeTo   DATETIME = CAST(CONVERT(CHAR(11),@UKDate,106)+ ' 06:30:00' AS DATETIME)
DECLARE @PlayerLowerClass BIT
DECLARE @Username NVARCHAR(15)
DECLARE @BonusModel CHAR(1)
DECLARE @BonusVoidBalanceThreshold MONEY
DECLARE @GameID INT
DECLARE @GameCategoryID INT
DECLARE @GameStatus TINYINT
DECLARE @WagerWithBonusFunds BIT
DECLARE @GameWagerPercent DECIMAL(4,3)  --The percentage that any wagers on this transaction contribute to active TxPromos (eg 0.50 = 50%)
DECLARE @ProgressiveJackpotWagerPercent DECIMAL(7,4)  --The percentage that any wagers on this transaction contribute to progressive jackpots (eg 0.06 = 6%)

DECLARE @BalanceReal MONEY        --Real balance immediately before tx
DECLARE @BalanceBonus MONEY       --Bonus balance immediately before tx
DECLARE @BalanceBonusWins MONEY   --BonusWins balance immediately before tx
DECLARE @ThisGameBalanceBonus MONEY = 0       --Bonus balance immediately before tx from bonuses that are available from promos for the game
DECLARE @ThisGameBalanceBonusWins MONEY = 0   --BonusWins balance immediately before tx from bonus wins that are available from promos for the game
DECLARE @ThisGameHasRestrictedPromos BIT = 0  --if there are any promos that are restricted to this game
DECLARE @BalanceControl MONEY     --Total balances for control at time of writing new balances

DECLARE @WagerReal MONEY = 0      --Portion of wager placed with Real funds not linked to any promo
DECLARE @WagerRealPromo MONEY = 0 --Portion of wager placed with Real funds from promos
DECLARE @WagerBonus MONEY = 0     --Portion of wager placed with Bonus funds. TxPromo.Status=2
DECLARE @WagerBonusWins MONEY = 0 --Portion of wager placed with BonusWins funds. TxPromo.Status=2
DECLARE @WagerBonusSpecialRule2 MONEY = 0 --Total portion of wagers placed with special rule #2 for model 'F' (where TxPromo.Status=4)

DECLARE @WinRealTotal MONEY = 0   --Total real wins (@WinReal+@WinRealPromo+@WinRealSpecialrule2)
DECLARE @WinReal MONEY = 0        --Portion of win as Real funds
DECLARE @WinRealPromo MONEY = 0   --Portion of win as Real funds from wagers made with TxPromo balances
DECLARE @WinRealSpecialrule2 MONEY = 0   --Portion of win as Real funds from wagers made with TxPromo balances
DECLARE @WinBonus MONEY = 0       --Portion of win as Bonus funds from wagers made with TxPromo balances
DECLARE @WinBonusWins MONEY = 0   --Portion of win as BonusWins funds from wagers made with TxPromo balances

DECLARE @AllWinsAreReal BIT = 0       --If a SkinGame.AllWinsAreReal=1 all wins go to real regardless of wagers (except if @TxWinTypeID = 63 meaning all wins should go to bonus or if PlayerClass.SubClass < 2)
DECLARE @GetLowBalancePromoID INT = 0
DECLARE @GameFundedOnly TINYINT = 0


IF @TxWagerTypeID = 0
  SET @TxWagerTypeID = 10--default
IF @TxWinTypeID = 0
  SET @TxWinTypeID = 11--default

--Variables used to pro-rata the types of balance applied to the wager and win
DECLARE @WagerToApply MONEY = 0
DECLARE @WinToApply MONEY = 0

/*
TxGame records individual transactions. These transactions can be wagers, wagers and wins, or just wins.
One or more TxGame transactions will be grouped to a single TxGameRound record.
TxGameRound.GameID + TxGameRound.TxGameRef + TxGameRound.PlayerID must be unique and records a single game round (composed of 1 or more wagers/win transactions for a game)
*/
DECLARE @TxGameID BIGINT = 0
DECLARE @TxGameRoundID BIGINT = 0
DECLARE @TxGameRoundDate DATE--to compare with @Today - if TxGameRound.Status changes and TxGameround.Date <> today write a daga.Mods rec
DECLARE @TxGameRoundSubRef BIGINT = 0 --To hold value of the last recorded TxGame.SubRef
DECLARE @TxGameRoundStatus TINYINT = 1 --default to complete

--Get player details
SELECT @SkinID = Player.SkinID,
       @Username = Player.Username,
       @PlayerCurrencyCode = Player.CurrencyCode,
       @PlayerCountryCode = Country.Code2,
       @PlayerLanguageCode = Player.LanguageCode,
       @PlayerDOB = '1980-04-19',--convert(varchar(50),Player.DOB,120) select convert(varchar(10),getdate(),120)
       @PlayerRegDate = CONVERT(VARCHAR(10),Player.Date,120),
       @PlayerGender = CASE Player.Gender WHEN 0 THEN 'f' ELSE 'm' END,
       @BonusModel = Player.BonusModel,
       @BalanceReal  = Player.Real,
       @BalanceBonus = Player.Bonus,
       @BalanceBonusWins = Player.BonusWins,
       @PlayerFunded = CASE WHEN Player.PlayerStage >= 50 THEN 1 ELSE 0 END,
       @PlayerLowerClass = CASE WHEN PlayerClass.SubClass < 2 THEN 1 ELSE 0 END
       FROM Player WITH (ROWLOCK) --{AF 28May13 - added WITH (ROWLOCK)}
            JOIN Country WITH (NOLOCK) ON Country.Code = Player.CountryCode
            JOIN PlayerClass WITH (NOLOCK) ON PlayerClass.ID = Player.PlayerClassID
       WHERE Player.ID = @PlayerID AND
             Player.Status = CASE WHEN @Action IN ('EndGameRound','Void','Recon','GetBalance') OR @GameProviderID=17 THEN Player.Status ELSE 1 END

--Player invalid - does not exist or status <> 1
IF @SkinID IS NULL
  BEGIN
    SET @EntityID = @PlayerID
    SET @EventLogTypeID = 303
    GOTO ReturnData
  END

--Hard coded @LowBalancePromoThreshold where if the players applicable balances fall below this a record will be written to PromoLowDepositOffer
DECLARE @LowBalancePromoThreshold MONEY = 2

--Balance control total - if this changes immediately before writing the game transaction, the entire transaction is voided
SET @BalanceControl = @BalanceReal + @BalanceBonus + @BalanceBonusWins

IF @GameProviderID = 9 AND @Action = 'Recon'--Bally recon - must exist in Main and will get GameProviderGameID
  BEGIN
    --Get details
    SET @Ref = ISNULL((SELECT TxGameRef FROM Main.dbo.TxGameRefBally WITH (NOLOCK) WHERE gameCycleId = @BallygameCycleId),0)
    SET @GameProviderGameID = NULL

    IF @SubRef = 0--Bally are looking for all TxGame records
      SELECT TOP 1 @GameProviderGameID = Game.GameProviderGameID
        FROM dbo.TxGameRound WITH (NOLOCK)
             JOIN Game WITH (NOLOCK) ON Game.ID = TxGameRound.GameID
        WHERE TxGameRound.TxGameRef= @Ref AND
              TxGameRound.PlayerID = @PlayerID AND
              Game.GameProviderID  = 9
    ELSE--Bally are looking for specific TxGame records where TxGame.SubRef >= @SubRef and TxGame.SubRef <= @SubRef + 99
      SELECT TOP 1 @GameProviderGameID = Game.GameProviderGameID
        FROM dbo.TxGameRound WITH (NOLOCK)
             JOIN dbo.TxGame WITH (NOLOCK) ON TxGame.TxGameRoundID = TxGameRound.ID
             JOIN Game WITH (NOLOCK) ON Game.ID = TxGameRound.GameID
        WHERE TxGameRound.TxGameRef= @Ref AND
              TxGameRound.PlayerID = @PlayerID AND
              (TxGame.SubRef BETWEEN @SubRef AND @SubRef+99) AND
              Game.GameProviderID  = 9

    --transaction does not exist (on Main) - we dont expect a recon more than a day after (as the tx will have been switched out of main)
    IF @GameProviderGameID IS NULL
    BEGIN
      SET @EventLogTypeID = 316
      GOTO ReturnData
    END
  END

IF @GameProviderID = 17 AND @Action = 'Void' AND @Ref=0 AND @SubRef=0 --NetEnt Void - the RefundRef is the TxGame.SubRef - unique for NetEnt
  BEGIN
    --Get details
    SET @Ref = NULL
    SET @SubRef = @RefundRef
    SELECT @Ref = TxGame.Ref
      FROM TxGame WITH (NOLOCK) JOIN Game WITH (NOLOCK) ON Game.ID = TxGame.GameID
      WHERE TxGame.PlayerID = @PlayerID AND TxGame.SubRef = @RefundRef AND Game.GameProviderID = @GameProviderID AND Game.GameProviderGameID = @GameProviderGameID

    --transaction does not exist (on Main) - assume no void needed
    IF @Ref IS NULL
    BEGIN
      SET @EventLogTypeID = 0
      GOTO ReturnData
    END
  END

--Get game details (including SkinGame data which overides Game data)
IF @Action = 'GetBalance' AND @GameProviderID IN(6,21)--MGS and mgs-ainsworth
  SELECT @GameID = Game.ID,
         @GameStatus = GameProvider.Status + Game.Status + SkinGame.Status, --These must all be 1, so the total must be 3
         @GameWagerPercent = SkinGame.WagerPercent,
         @ProgressiveJackpotWagerPercent = Game.ProgressiveJackpotWagerPercent,
         @WagerWithBonusFunds = SkinGame.WagerWithBonusFunds,
         @AllWinsAreReal = SkinGame.AllWinsAreReal,
         @GameCategoryID = Game.GameCategoryID
    FROM Game WITH (NOLOCK)
         JOIN GameProvider WITH (NOLOCK) ON GameProvider.ID = Game.GameProviderID
         JOIN SkinGame WITH (NOLOCK) ON SkinGame.SkinID = @SkinID AND SkinGame.GameID = Game.ID
    WHERE Game.GameProviderID = @GameProviderID AND
          Game.ID = @GameProviderGameID
ELSE
  BEGIN
    IF (@Action='GetBalance' AND @GameProviderID = 9)
			 SELECT @GameID = Game.ID,
              @GameStatus = GameProvider.Status + Game.Status + SkinGame.Status, --These must all be 1, so the total must be 3
	            @GameWagerPercent = SkinGame.WagerPercent,
	            @ProgressiveJackpotWagerPercent = Game.ProgressiveJackpotWagerPercent,
	            @WagerWithBonusFunds = SkinGame.WagerWithBonusFunds,
	            @AllWinsAreReal = SkinGame.AllWinsAreReal,
	            @GameCategoryID = Game.GameCategoryID
			   FROM Game WITH (NOLOCK)
              JOIN GameProvider WITH (NOLOCK) ON GameProvider.ID = Game.GameProviderID
		          JOIN SkinGame WITH (NOLOCK) ON SkinGame.SkinID = @SkinID AND SkinGame.GameID = Game.ID
			   WHERE Game.GameProviderID = @GameProviderID
			   and game.Status=1 
			   and skingame.Status=1
    ELSE

      BEGIN
        IF (@Action='Recon' OR @Action='void') AND @GameProviderID = 4
          BEGIN
		        SELECT @GameID = Game.ID,
                   @GameStatus = GameProvider.Status + Game.Status + SkinGame.Status, --These must all be 1, so the total must be 3
					         @GameWagerPercent = SkinGame.WagerPercent,
					         @ProgressiveJackpotWagerPercent = Game.ProgressiveJackpotWagerPercent,
					         @WagerWithBonusFunds = SkinGame.WagerWithBonusFunds,
					         @AllWinsAreReal = SkinGame.AllWinsAreReal,
					         @GameCategoryID = Game.GameCategoryID
			        FROM TxGameRound WITH (NOLOCK) 
			             JOIN Game WITH (NOLOCK) ON Game.ID = TxGameRound.GameID
                   JOIN GameProvider WITH (NOLOCK) ON GameProvider.ID = Game.GameProviderID
					         JOIN SkinGame WITH (NOLOCK) ON SkinGame.SkinID = @SkinID AND SkinGame.GameID = Game.ID
			        WHERE TxGameRound.PlayerID = @PlayerID AND
			              TxGameRound.TxGameRef = @Ref AND
			              Game.GameProviderID = @GameProviderID

			        IF @GameID IS NULL--the recon is not found - its a new transaction that needs to be inserted
		            SELECT TOP 1
		                   @GameID = Game.ID,
                       @GameStatus = ISNULL(GameProvider.Status,0) + Game.Status + ISNULL(SkinGame.Status,0), --These must all be 1, so the total must be 3
					             @GameWagerPercent = SkinGame.WagerPercent,
					             @ProgressiveJackpotWagerPercent = Game.ProgressiveJackpotWagerPercent,
					             @WagerWithBonusFunds = SkinGame.WagerWithBonusFunds,
					             @AllWinsAreReal = SkinGame.AllWinsAreReal,
					             @GameCategoryID = Game.GameCategoryID
			            FROM Game WITH (NOLOCK)
                       JOIN GameProvider WITH (NOLOCK) ON GameProvider.ID = Game.GameProviderID
					             JOIN SkinGame WITH (NOLOCK) ON SkinGame.SkinID = @SkinID AND SkinGame.GameID = Game.ID
			            WHERE Game.GameProviderID = @GameProviderID AND
						            Game.GameProviderGameID = SUBSTRING(@GameProviderGameID,1,CHARINDEX(right(@GameProviderGameID,3),@GameProviderGameID)-1) AND
						            Game.ID >= CASE RIGHT(@GameProviderGameID,3) WHEN 'MOB' THEN 4500 ELSE 4000 END
						            --AND SkinGame.Status = 1
						      ORDER BY Game.ID
			    END
        ELSE
		      SELECT @GameID = Game.ID,
                 @GameStatus = GameProvider.Status + Game.Status + SkinGame.Status, --These must all be 1, so the total must be 3
					       @GameWagerPercent = SkinGame.WagerPercent,
					       @ProgressiveJackpotWagerPercent = Game.ProgressiveJackpotWagerPercent,
					       @WagerWithBonusFunds = SkinGame.WagerWithBonusFunds,
					       @AllWinsAreReal = SkinGame.AllWinsAreReal,
					       @GameCategoryID = Game.GameCategoryID,
					       @GameFundedOnly = CAST(Game.FundedPlayersOnly AS TINYINT) + CAST(SkinGame.FundedPlayersOnly AS TINYINT) 

			      FROM Game WITH (NOLOCK)
                 JOIN GameProvider WITH (NOLOCK) ON GameProvider.ID = Game.GameProviderID
					       JOIN SkinGame WITH (NOLOCK) ON SkinGame.SkinID = @SkinID AND SkinGame.GameID = Game.ID
			      WHERE Game.GameProviderID = @GameProviderID AND
				Game.GameProviderGameID = CASE WHEN @GameProviderID = 4 THEN Game.GameProviderGameID ELSE @GameProviderGameID END AND 
				Game.ID = CASE WHEN @GameProviderID = 4 THEN @GameProviderGameID ELSE Game.ID END
	    END
  END

/*
AllWinsAreReal overides:
- If Player is not funded
- If Player is in a lower or red class
- If its between 01:30 and 06:30
*/

IF @PlayerFunded = 0
   OR @PlayerLowerClass = 1
   --OR (@UKDate between @AllWinsAreNotRealTimeFrom and @AllWinsAreNotRealTimeTo)
  SET @AllWinsAreReal = 0

--Game not found
IF ISNULL(@GameID,0) = 0
  BEGIN
    SET @EntityID = @PlayerID
    SET @EventLogTypeID = 301
    GOTO ReturnData
  END

--Game disabled
IF @GameStatus <> 3 --GameProvider.Status + Game.Status + SkinGame.Status must all be 1
  BEGIN
    SET @EntityID = @GameID
    SET @EventLogTypeID = 302
    GOTO ReturnData
  END

--Wager < 0
IF @Wager < 0
  BEGIN
    SET @EntityID = @GameID
    SET @EventLogTypeID = 305
    GOTO ReturnData
  END

--Win < 0
IF @Win < 0
  BEGIN
    SET @EntityID = @GameID
    SET @EventLogTypeID = 306
    GOTO ReturnData
  END


/***************************************************/
/*           START  REALITY CHECK    DEV-4946      */
/***************************************************/
IF @Action IN ('Tx', 'TxEndGameRound','TxEndGameRound') 
BEGIN
	-- We check whether the player has a given game blocked already
	IF 0 < (SELECT COUNT(PlayerID) FROM Main..RealityCheckPlayerGame rcp (NOLOCK) WHERE rcp.GameID = @GameID and rcp.PlayerID = @PlayerID)
	BEGIN
	
    SET @EntityID = @GameID
    SET @EventLogTypeID = 300 -- At the moment, the reality check failure is included as part of the 300 error type
    GOTO ReturnData
	END
	

END
/***************************************************/
/*            END  REALITY CHECK                   */
/***************************************************/

IF @Action = 'GetBalance'
  GOTO ReturnData

--Calculate the @ThisGameBalanceBonus and @ThisGameBalanceBonusWins that are specifically available for the game
IF @WagerWithBonusFunds = 1
BEGIN
  --Get a total of the bonus balances that the player can use on this game
  SET @ThisGameBalanceBonus = NULL
  SET @ThisGameHasRestrictedPromos = 0
  SELECT
    @ThisGameBalanceBonus     = SUM(TxPromo.BonusBal),
    @ThisGameBalanceBonusWins = SUM(CASE TxPromo.Status WHEN 4 THEN 0 ELSE TxPromo.BonusWinsBal END)
  FROM dbo.TxPromo WITH (NOLOCK)
       JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
  WHERE TxPromo.PlayerID = @PlayerID AND
        TxPromo.RestrictToGames = 1 AND
        TxPromo.Status IN (2,4)

  IF @ThisGameBalanceBonus IS NULL
    BEGIN
      SET @ThisGameBalanceBonus = 0
      SET @ThisGameBalanceBonusWins = 0
      SET @ThisGameHasRestrictedPromos = 0
    END
  ELSE
    BEGIN
      IF @Win > 0
        SET @ThisGameHasRestrictedPromos = 1
      ELSE
        BEGIN
          IF (@ThisGameBalanceBonus + @ThisGameBalanceBonusWins) > 0
            SET @ThisGameHasRestrictedPromos = 1
        END
    END

  --Add that to the bonus balances that the player can use without restriction
  SELECT
    @ThisGameBalanceBonus     += ISNULL(SUM(TxPromo.BonusBal),0),
    @ThisGameBalanceBonusWins += ISNULL(SUM(CASE TxPromo.Status WHEN 4 THEN 0 ELSE TxPromo.BonusWinsBal END),0)
  FROM dbo.TxPromo WITH (NOLOCK)
  WHERE TxPromo.PlayerID = @PlayerID AND
        TxPromo.RestrictToGames = 0 AND
        TxPromo.Status IN (2,4)

  --Ensure the total >=0 and <= Player bonus balances
  IF @ThisGameBalanceBonus < 0
    SET @ThisGameBalanceBonus = 0

  IF @ThisGameBalanceBonus > @BalanceBonus
    SET @ThisGameBalanceBonus = @BalanceBonus

  IF @ThisGameBalanceBonusWins < 0
    SET @ThisGameBalanceBonusWins = 0

  IF @ThisGameBalanceBonusWins > @BalanceBonusWins
    SET @ThisGameBalanceBonusWins = @BalanceBonusWins
END

/*
Get the total of TxGameRound Wager transactions at this point in time
This is needed where @Win>0 and we need to know the prior wagers (if any)
in order to determine the portion of the win relating to Real, Promos and Special Rule #2 Promos.
Also used to apply game void refunds back to applicable balances.

In multi state games there may be multiple wagers and/or multiple win transactions for a single game round.
Some game providers make seperate transaction calls for wagers and wins for a single game round.
*/
DECLARE @TxGameRoundWagerReal FLOAT
DECLARE @TxGameRoundWagerRealPromo FLOAT
DECLARE @TxGameRoundWagerBonus FLOAT
DECLARE @TxGameRoundWagerBonusSpecialRule2 FLOAT
DECLARE @TxGameRoundWagerBonusWins FLOAT
DECLARE @TxGameRoundWager FLOAT --total wagers at this point in time for the game round

DECLARE @UpdateTxGameRoundTransactions INT = 0
DECLARE @UpdateTxGameRoundWin MONEY = 0
DECLARE @UpdateTxGameRoundWinReal MONEY = 0
DECLARE @UpdateTxGameRoundWinBonus MONEY = 0
DECLARE @UpdateTxGameRoundWinBonusWins MONEY = 0
DECLARE @UpdateTxGameRoundDate DATETIME2(7) = GETDATE()
DECLARE @UpdateTxGameRoundTxGameRef BIGINT = 0
DECLARE @UpdateTxGameRoundGameLaunchSourceID TINYINT = 0

--If Bally send a gameCycleId, get it or insert it if it does not exist and set @Ref
IF @Action <> 'Recon' AND @GameProviderID = 9 AND @BallygameCycleId IS NOT NULL --bally
BEGIN
  --SET @Ref = ISNULL((SELECT TOP 1 TxGameRef FROM Main.dbo.TxGameRefBally WITH (NOLOCK) WHERE gameCycleId = @BallygameCycleId),0)
  --SET @Ref = ISNULL((SELECT TOP 1 TxGameRef FROM Main.dbo.TxGameRefBally WHERE gameCycleId = @BallygameCycleId),0)--NEED TO LOCK!!! - the index already says dont use pagelocks so we dont need a with (ROWLOCK)!!!
  SELECT @Ref = TxGameRef FROM TxGameRefBally WITH (ROWLOCK) WHERE gameCycleId = @BallygameCycleId--NEED TO LOCK!!!
  IF ISNULL(@Ref,0) = 0
    BEGIN
      INSERT TxGameRefBally (gameCycleId) VALUES (@BallygameCycleId)
      IF @@ERROR=0
        SET @Ref = SCOPE_IDENTITY()
      ELSE--a dupe is found - abort
        BEGIN
          SET @EntityID = @PlayerID
          SET @EventLogTypeID = 307--dupe
          GOTO ReturnData
        END
    END
END

/*
New Game round (to be inserted) or existing game round (to be added to)
If call is from MGS, they may send the same @Ref for a NEW GameRound, for a Tx action.
Therefore we need to check the TxGameRound.Status for MGS. If Status=1 (complete), we assume a new game round
*/
SELECT @TxGameRoundID             = TxGameRound.ID,
       @TxGameRoundDate           = TxGameRound.Date,
       @TxGameRoundSubRef         = TxGameRound.TxGameSubRef,
       @TxGameRoundWagerReal      = CAST(TxGameRound.WagerReal AS FLOAT),
       @TxGameRoundWagerRealPromo = CAST(TxGameRound.WagerRealPromo AS FLOAT),
       @TxGameRoundWagerBonus     = CAST(TxGameRound.WagerBonus AS FLOAT),
       @TxGameRoundWagerBonusSpecialRule2 = CAST(TxGameRound.WagerBonusSpecialRule2 AS FLOAT),
       @TxGameRoundWagerBonusWins = CAST(TxGameRound.WagerBonusWins AS FLOAT),
       @UpdateTxGameRoundTransactions = TxGameRound.Transactions,
       @UpdateTxGameRoundWin = @Win + TxGameRound.Win,
       @UpdateTxGameRoundWinReal = TxGameRound.WinReal,
       @UpdateTxGameRoundWinBonus = TxGameRound.WinBonus,
       @UpdateTxGameRoundWinBonusWins = TxGameRound.WinBonusWins,
       @UpdateTxGameRoundDate = TxGameRound.Date,
       @UpdateTxGameRoundTxGameRef = TxGameRound.TxGameRef,
       @UpdateTxGameRoundGameLaunchSourceID = TxGameRound.GameLaunchSourceID
FROM dbo.TxGameRound WITH (ROWLOCK) --{AF 28May13 - added WITH (ROWLOCK)}
WHERE TxGameRound.GameID    = @GameID AND
      TxGameRound.TxGameRef = @Ref AND
      TxGameRound.PlayerID  = @PlayerID AND
      TxGameRound.Status = CASE WHEN (@GameProviderID IN (6,21) AND @Action = 'Tx') THEN 2 ELSE TxGameRound.Status END

SET @TxGameRoundID             = ISNULL(@TxGameRoundID,0)
SET @TxGameRoundSubRef         = ISNULL(@TxGameRoundSubRef,0)
SET @TxGameRoundWagerReal      = ISNULL(@TxGameRoundWagerReal,0)
SET @TxGameRoundWagerRealPromo = ISNULL(@TxGameRoundWagerRealPromo,0)
SET @TxGameRoundWagerBonus     = ISNULL(@TxGameRoundWagerBonus,0)
SET @TxGameRoundWagerBonusSpecialRule2     = ISNULL(@TxGameRoundWagerBonusSpecialRule2,0)
SET @TxGameRoundWagerBonusWins = ISNULL(@TxGameRoundWagerBonusWins,0)

IF (@Action <> 'TxEndGameRound' AND @GameProviderID in (4,19))--igt and scientific games
   SET @TxGameRoundStatus = 2


IF (@GameCategoryID = 1 AND @GameProviderID = 1)--Daubs Bingo games - only set TxGameRound.Status=1 where @Win > 0 (i.e. on completion)
BEGIN
  IF @Win = 0
    SET @TxGameRoundStatus = 2
  ELSE
    BEGIN
      IF (SELECT TxGameRef FROM TxBingoGameRef WITH (NOLOCK) WHERE TxGameRef = @Ref) IS NULL
        INSERT TxBingoGameRef (TxGameRef) VALUES (@Ref)
    END
END

/*
EndGameRound - update TxGameRound.Status
*/
IF @Action = 'TxEndGameRound'
BEGIN
  SET @TxGameRoundStatus = 1
  SET @Action='Tx'
  UPDATE TxGameRound SET Status = @TxGameRoundStatus WHERE ID = @TxGameRoundID
  --dont need the replication insert - this is done when the TxGame is written

  IF @Today<>@TxGameRoundDate
    EXEC daga..dwhMods 'Main', 'TxGameRound', @TxGameRoundID
END


IF @Action = 'EndGameRound'
BEGIN
  IF @TxGameRoundID = 0
    BEGIN
      SET @EntityID = @TxGameRoundID
      SET @EventLogTypeID = 316
      GOTO ReturnData
    END
  SET @TxGameRoundStatus = 1
  UPDATE TxGameRound SET Status = @TxGameRoundStatus WHERE ID = @TxGameRoundID

  IF @Today <> @TxGameRoundDate
    EXEC daga..dwhMods 'Main', 'TxGameRound', @TxGameRoundID

  GOTO ReturnData
END

/*
@TxGameRoundWager = total wager to date (including the @Wager of this transaction if any).
This amount must be used for any @Win pro ratas where the win is split for the purpose of applying to promos and real based on wagers thereof
*/
SET @TxGameRoundWager = @Wager + @TxGameRoundWagerReal + @TxGameRoundWagerRealPromo + @TxGameRoundWagerBonus + @TxGameRoundWagerBonusSpecialRule2 + @TxGameRoundWagerBonusWins

/*
Check the game providers game reference and sub reference is unique (ie we have not already processed the transaction).
Exception is where a game passes in a @SubRef of "-1".
In this circumstance this sproc will auto generate a sub reference.
This situation is used:
  - by Bingo where a player can purchase many times for a single game and the bingo @Ref is the Bingo gameID.

!!!We can never have a SubRef of zero!!!

Additionally, a TxGameRound record is added when the 1st game round transaction is initiated.
Subsequent transactions for the same Game round are added to the TxGameRound record,
which is assumed to exist if there are any existing TxGame records where GameID=@GameID and Ref=@Ref and PlayerID=@PlayerID
*/
IF @SubRef = -1  --specific request to auto generate a SubRef - this will guarantee a unique transaction, but its *not* the preferred practice
  SET @SubRef = @TxGameRoundSubRef + 1
ELSE --The sub ref is specified by the calling game (which it should be and is the preferred practice) - now we check its unique
  BEGIN
    DECLARE @RecordedWin MONEY = 0
    DECLARE @RecordedWager MONEY = 0
    SET @TxGameID = NULL

    IF @Action = 'Recon' AND @GameProviderID IN (2,9)--Eyecon and Bally recons need to find the first txn with the same Ref
      BEGIN
        SELECT TOP 1
               @TxGameID = ID,
               @WagerReal = WagerReal,
               @WagerRealPromo = WagerRealPromo,
               @WagerBonus = WagerBonus,
               @WagerBonusSpecialRule2 = WagerBonusSpecialRule2,
               @WagerBonusWins = WagerBonusWins,
               @RecordedWin = Win,
               @WinRealTotal = WinReal
          FROM dbo.TxGame WITH (ROWLOCK) --{AF 28May13 - added WITH (ROWLOCK)}
          WHERE GameID = @GameID AND
                TxGame.PlayerID = @PlayerID AND
                TxGame.Ref = @Ref

        --in the case of eyecon, the win must be identical, otherwise its a new transaction that we have not recorded - typically a 'feature win'
        IF @TxGameID IS NOT NULL AND @GameProviderID = 2 AND @RecordedWin <> @Win
          SET @TxGameID = NULL
      END
    ELSE
      BEGIN
        SELECT @TxGameID = ID,
				@RecordedWager = Wager,
               @WagerReal = WagerReal,
               @WagerRealPromo = WagerRealPromo,
               @WagerBonus = WagerBonus,
               @WagerBonusSpecialRule2 = WagerBonusSpecialRule2,
               @WagerBonusWins = WagerBonusWins,
               @RecordedWin = Win,
               @WinRealTotal = WinReal
          FROM dbo.TxGame WITH (ROWLOCK) --{AF 28May13 - added WITH (ROWLOCK)}
          WHERE GameID = @GameID AND
                TxGame.PlayerID = @PlayerID AND
                TxGame.Ref = @Ref AND
                TxGame.SubRef = @SubRef
      END

    --IF @@ROWCOUNT<>0 AND @TxGameID IS NOT NULL AND @TxGameID <> 0
    IF @TxGameID IS NOT NULL
      --meaning the transaction is duplicated (SubRef can never be zero), (or in the case of a 'Recon' the transaction exists which is the desired outcome)
      --Note that for a Recon, the system calling for the Recon is only checking we have the record, they do not require wagers/wins.
      BEGIN
        IF @Action = 'Void' AND @GameProviderID IN (6,21,14,17) --MGS, mgs-ainsworth and PlaynGo and NetEnt checks the bet being refunded has been placed - all good if it has - the refund will proceed. Need to generate a new subref
           SET @SubRef = @TxGameRoundSubRef + 1
        ELSE
          BEGIN
            SET @EntityID = @TxGameRoundID

            SET @EventLogTypeID = 307

            --If @Action = Recon, assume a success return - i.e. we have the transaction they are looking for
            IF @Action = 'Recon'
              SET @EventLogTypeID = 0
            
            --For Eyecon and PlaynGo and NetEnt, if they send the same Ref and SubRef, it indicates a recon
            --Eyecon
            IF (@GameProviderID = 2 AND 
				((SUBSTRING(@Action,1,2) = 'Tx' AND
                  ((@RecordedWin = @Win AND @Win > 0) OR --win recon
                   (@RecordedWin = 0 AND @Win = 0)))
                  OR
                  (@Action = 'Void') )
               )
              SET @EventLogTypeID = 0

            --PlaynGo

            IF (@GameProviderID = 14 AND
                ((SUBSTRING(@Action,1,2) = 'Tx')
                 OR
                 (@Action = 'Void'))
               )
              SET @EventLogTypeID = 0

            --NetEnt
            IF (@GameProviderID = 17 AND
                ((SUBSTRING(@Action,1,2) = 'Tx')
                 OR
                 (@Action = 'Void'))
               )
              SET @EventLogTypeID = 0

			--Realistic
            IF (@GameProviderID = 16 AND ( ((SUBSTRING(@Action,1,2) = 'Tx') AND @RecordedWager = @Wager AND @RecordedWin = @Win ) OR (@Action = 'Void' AND @RecordedWager = @Refund * -1 ) ) )
				SET @EventLogTypeID = 0
				
            --Geco
            IF (@GameProviderID = 18 AND (SUBSTRING(@Action,1,2) = 'Tx') AND (@RecordedWager = @Wager) AND (@RecordedWin = @Win) )
				SET @EventLogTypeID = 0
			
			--SG
            IF (@GameProviderID = 19 AND (SUBSTRING(@Action,1,2) = 'Tx' OR SUBSTRING(@Action,1,2) = 'TxEndGameRound') AND (@RecordedWager = @Wager) AND (@RecordedWin = @Win) )
				SET @EventLogTypeID = 0
			
            GOTO ReturnData
          END
      END
    ELSE --the transaction does not exist and if this is a MGS or mgs-ainsworth void, or PlaynGo or NetEnt Void respond with an AlreadyProcessed = true
      BEGIN
        IF @Action = 'Void' AND @GameProviderID in (6,21)
          BEGIN
            SET @EntityID = @TxGameRoundID
            SET @EventLogTypeID = 307
            GOTO ReturnData
          END

        IF @Action = 'Void' AND @GameProviderID IN (14,17)
          BEGIN
            SET @TxGameID = 0
            SET @EventLogTypeID = 0
            GOTO ReturnData
          END
      END
  END

--If a 'Recon', and the transaction has not been found above, it needs to be treated as a Tx request as the transaction has not been recorded by ags
IF @Action = 'Recon'
  BEGIN
    /*
    If we have a TxGameRoundID we assume the wager portion of the transaction has been recorded.
    */
    IF @TxGameRoundID <> 0
      BEGIN
        SET @TxGameRoundWager -= @Wager
        IF @TxGameRoundWager < 0
          SET @TxGameRoundWager = 0

        SET @Wager = 0
      END

    SET @Action = 'Tx'
  END

/*
Deal with voids and refunds.
Refunds will only be performed where there were wagers in the game round, and are limited to the wagers placed on the game.
It is the policy of DaGaCube to not remove any recorded wins when a game is voided.
This can be done through a cogs adjustment if required.
*/
DECLARE @RefundReal FLOAT = 0
DECLARE @RefundRealPromo FLOAT = 0
DECLARE @RefundBonus FLOAT = 0
DECLARE @RefundBonusSpecialRule2 FLOAT = 0
DECLARE @RefundBonusWins FLOAT = 0
DECLARE @RefundRequest FLOAT = @Refund

--Action=VoidCogs does not have the option of specifying the Refund amount - this will be the value of wagers as calculated here.
IF @Action = 'VoidCogs'
  BEGIN
    SET @RefundReal      = @TxGameRoundWagerReal
    SET @RefundRealPromo = @TxGameRoundWagerRealPromo
    SET @RefundBonus     = @TxGameRoundWagerBonus
    SET @RefundBonusSpecialRule2 = @TxGameRoundWagerBonusSpecialRule2
    SET @RefundBonusWins = @TxGameRoundWagerBonusWins
    SET @Refund = @RefundReal + @RefundRealPromo + @RefundBonus + @RefundBonusSpecialRule2 + @RefundBonusWins

    IF @Refund = 0
      BEGIN
        SET @EntityID = @TxGameRoundID
        SET @EventLogTypeID = 313
        GOTO ReturnData
      END
  END

--Action=Void - the Refund amount is sent from the game provider, and is apportioned according to the original wagers and up to a maximum of the original wagers
IF @Action = 'Void'
  BEGIN
    IF @Refund=0 AND @GameProviderID IN (2,17) --eyecon refunds sometimes send as zero so refund the whole wager. NetEnt must refund the entire wager
      SET @Refund = @TxGameRoundWager

    IF @Refund > 0 AND @TxGameRoundWager > 0
      BEGIN
        IF @Refund > @TxGameRoundWager
          SET @Refund = @TxGameRoundWager
        SET @RefundReal      = (@TxGameRoundWagerReal / @TxGameRoundWager) * @Refund
        SET @RefundRealPromo = (@TxGameRoundWagerRealPromo / @TxGameRoundWager) * @Refund
        SET @RefundBonus     = (@TxGameRoundWagerBonus / @TxGameRoundWager) * @Refund
        SET @RefundBonusSpecialRule2 = (@TxGameRoundWagerBonusSpecialRule2 / @TxGameRoundWager) * @Refund
        SET @RefundBonusWins = (@TxGameRoundWagerBonusWins / @TxGameRoundWager) * @Refund
      END
    ELSE --Cannot void without wagers
      BEGIN
        SET @EntityID = @TxGameRoundID
        SET @EventLogTypeID = CASE WHEN @GameProviderID in (17,19) THEN 0 ELSE 313 END
        GOTO ReturnData
      END
  END

/*---------------------------------------------------------------------
Process the game transaction.
- Check Player Balance covers Wager
- Check Player Wager and Loss limits
- Apply the @Wager to be drawn from any applicable TxPromo balances and Player balances
----------------------------------------------------------------------*/
IF @Action = 'Tx'
BEGIN

/*
Check the player has enough funds for the wager
*/
IF (@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins) < @Wager
  BEGIN
    SET @EntityID = @PlayerID
    SET @EventLogTypeID = 304
    GOTO ReturnData
  END

--Check players wager and loss limits
DECLARE @WagerLimitSession MONEY
DECLARE @WagerThisSession MONEY
DECLARE @WagerLimitDay MONEY
DECLARE @WagerThisDay MONEY
DECLARE @WagerLimitWeek MONEY
DECLARE @WagerThisWeek MONEY
DECLARE @WagerLimitMonth MONEY
DECLARE @WagerThisMonth MONEY
DECLARE @LossLimitSession MONEY
DECLARE @LossThisSession MONEY
DECLARE @LossLimitDay MONEY
DECLARE @LossThisDay MONEY
DECLARE @LossLimitWeek MONEY
DECLARE @LossThisWeek MONEY
DECLARE @LossLimitMonth MONEY
DECLARE @LossThisMonth MONEY

DECLARE @Loss MONEY = @Wager-@Win
--IF @Loss < 0
--  SET @Loss = 0

SELECT @WagerLimitSession = WagerLimitSession,
       @WagerThisSession  = WagerThisSession,
       @WagerLimitDay     = WagerLimitDay,
       @WagerThisDay      = WagerThisDay,
       @WagerLimitWeek    = WagerLimitWeek,
       @WagerThisWeek     = WagerThisWeek,
       @WagerLimitMonth   = WagerLimitMonth,
       @WagerThisMonth    = WagerThisMonth,
       @LossLimitSession  = LossLimitSession,
       @LossThisSession   = LossThisSession,
       @LossLimitDay      = LossLimitDay,
       @LossThisDay       = LossThisDay,
       @LossLimitWeek     = LossLimitWeek,
       @LossThisWeek      = LossThisWeek,
       @LossLimitMonth    = LossLimitMonth,
       @LossThisMonth     = LossThisMonth
FROM dbo.PlayerLimit WITH (NOLOCK)
WHERE PlayerLimit.PlayerID = @PlayerID

--Player limits may not have been set up for player (in which case @WagerLimitSession = null and we ignore limits)
IF @@ROWCOUNT<>0 AND @WagerLimitSession IS NOT NULL
  BEGIN
    IF (@Wager+@WagerThisSession > @WagerLimitSession and @WagerLimitSession > 0) OR 
       (@Wager+@WagerThisDay     > @WagerLimitDay and @WagerLimitDay > 0) OR
       (@Wager+@WagerThisWeek    > @WagerLimitWeek and @WagerLimitWeek > 0) OR
       (@Wager+@WagerThisMonth   > @WagerLimitMonth and @WagerLimitMonth > 0) OR
       (@Loss+@LossThisSession   > @LossLimitSession and @LossLimitSession > 0) OR
       (@Loss+@LossThisDay       > @LossLimitDay and @LossLimitDay > 0) OR
       (@Loss+@LossThisWeek      > @LossLimitWeek and @LossLimitWeek > 0) OR
       (@Loss+@LossThisMonth     > @LossLimitMonth and @LossLimitMonth > 0)

       BEGIN
        SET @EntityID = @PlayerID
        SET @EventLogTypeID = 308
        GOTO ReturnData
       END
  END

/*
Write the new TxGame transaction, stamping the balances prior to the transaction
Wrap this in a BEGIN TRAN...COMMIT in case player balances have changed
Note that if there is a rollback, TxGame, TxGameRound will be missing an ID number (as its rolled back)
{25Oct11 - REMOVED TRANSACTION DUE TO EFFICIENCY PROBLEMS}
*/
---------------------------------------------------------------
--BEGIN TRAN
---------------------------------------------------------------

/*
Draw wagers from the balances of the TxPromo records
Wagers are taken from balance pools of TxPromo records on a FIFO basis as follows (assuming game allows WagerWithBonus, else only use items 2 and 3):
1.1 BonusWins (including Cash Back types) that are restricted to the game being played
1.2 BonusWins (including Cash Back types) unrestricted game promos
2.1 Real from Promos that are restricted to the game being played (non bingo)
2.2 Real from unrestricted game Promos (non bingo)
3 Real not from Promos
4.1 Bonus FROM dbo.TxPromo.BonusBal for pending promos that are Deposit Promos - Restricted
4.2 Bonus FROM dbo.TxPromo.BonusBal for pending promos that are Deposit Promos
5.1 Bonus FROM dbo.TxPromo.BonusBal for pending promos (including Cash Back types) - Restricted
5.2 Bonus FROM dbo.TxPromo.BonusBal for pending promos (including Cash Back types)
6.1 Bonus from Special Rule #2 promos (TxPromo.Status=4) (including Cash Back types) - Restricted
6.2 Bonus from Special Rule #2 promos (TxPromo.Status=4) (including Cash Back types)
*/
SET @WagerToApply = @Wager

--Use these Variables to store totals of where wagers were placed from
SET @WagerReal = 0
SET @WagerRealPromo = 0
SET @WagerBonus = 0
SET @WagerBonusSpecialRule2 = 0
SET @WagerBonusWins = 0

DECLARE @ThisTxPromoID INT
DECLARE @ThisAmount MONEY

DECLARE @TxPromoDeltaRealBal MONEY
DECLARE @TxPromoDeltaBonusBal MONEY
DECLARE @TxPromoDeltaBonusWinsBal MONEY

SET @WagerToApply = @Wager

/*
1.1 BonusWins from Promos that are restricted to this game
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonusWins > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusWinsBal THEN TxPromo.BonusWinsBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.BonusWinsBal > 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 1
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonusWins += @ThisAmount
        SET @WagerToApply -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusWinsBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,0,-@ThisAmount,
          '1.1 Wager with BonusWins - Restricted to Game'
      END
  END

/*
1.2 BonusWins from TxPromos that are not restricted
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonusWins > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusWinsBal THEN TxPromo.BonusWinsBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.BonusWinsBal > 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 0
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonusWins += @ThisAmount
        SET @WagerToApply -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusWinsBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,0,-@ThisAmount,
          '1.2 Wager with BonusWins - Not Restricted to Game'
      END
  END

/*
2.1 Real from Promos that are restricted to this game (non bingo)
This pulls all available active (Status=2) deposit promos RealBal's for wagering use.
*** 03 June 14 - ANY WAGER PLACED WITH REAL FUNDS WILL GIVE REAL WINS ***
*/
/*
WHILE (@WagerToApply > 0 AND @BalanceReal > 0 AND @GameCategoryID <> 1)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.RealBal THEN TxPromo.RealBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.IsDepositBonus = 1 AND
          TxPromo.RealBal > 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 1
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerRealPromo += @ThisAmount
        SET @WagerToApply   -= @ThisAmount
        UPDATE dbo.TxPromo SET RealBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,-@ThisAmount,0,0,
          '2.1 Wager with Real from Promos - Restricted to Game'
      END
  END

/*
2.2 Real from unrestricted Promos (non bingo)
This pulls all available active (Status=2) deposit promos RealBal's for wagering use.
*/
WHILE (@WagerToApply > 0 AND @BalanceReal > 0 AND @GameCategoryID <> 1)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.RealBal THEN TxPromo.RealBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.IsDepositBonus = 1 AND
          TxPromo.RealBal > 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 0
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerRealPromo += @ThisAmount
        SET @WagerToApply   -= @ThisAmount
        UPDATE dbo.TxPromo SET RealBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,-@ThisAmount,0,0,
          '2.2 Wager with Real from Promos - Not Restricted to Game'
      END
  END
*/

/*
3. Real not from Promos
*/
SET @WagerReal = CASE WHEN (@WagerToApply > (@BalanceReal-@WagerRealPromo)) THEN (@BalanceReal-@WagerRealPromo) ELSE @WagerToApply END
SET @WagerToApply -= @WagerReal

--At this point, is @WagerToApply > 0 players real balance must be zero


/*
4.1 Bonus from Deposit Promos (where TxPromo.Status=2) - Restricted to games
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonus > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusBal THEN TxPromo.BonusBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.IsDepositBonus = 1 AND
          TxPromo.BonusBal > 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 1
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonus   += @ThisAmount
        SET @WagerToApply -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,-@ThisAmount,0,
          '4.1 Wager with Bonus from Deposit Promos - Restricted to Game'
      END
  END

/*
4.2 Bonus from Deposit Promos (where TxPromo.Status=2) - Not Restricted to games
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonus > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusBal THEN TxPromo.BonusBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.IsDepositBonus = 1 AND
          TxPromo.BonusBal > 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 0
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonus   += @ThisAmount
        SET @WagerToApply -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,-@ThisAmount,0,
          '4.2 Wager with Bonus from Deposit Promos - Not Restricted to Game'
      END
  END


/*
5.1 Bonus from non-Deposit Promos (where TxPromo.Status=2) - restricted to games
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonus > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusBal THEN TxPromo.BonusBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.BonusBal > 0 AND
          TxPromo.IsDepositBonus = 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 1
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonus   += @ThisAmount
        SET @WagerToApply -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,-@ThisAmount,0,
          '5.1 Wager with Bonus from non-Deposit Promos - Restricted to Game'
      END
  END

/*
5.2 Bonus from non-Deposit Promos (where TxPromo.Status=2) - not restricted to games
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonus > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusBal THEN TxPromo.BonusBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.BonusBal > 0 AND
          TxPromo.IsDepositBonus = 0 AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 0
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonus   += @ThisAmount
        SET @WagerToApply -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,-@ThisAmount,0,
          '5.2 Wager with Bonus from non-Deposit Promos - Not Restricted to Game'
      END
  END


/*
6.1 Bonus (where TxPromo.Status=4 - special rule #2) - restricted to games
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonus > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusBal THEN TxPromo.BonusBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.BonusBal > 0 AND
          TxPromo.Status = 4 AND
          TxPromo.RestrictToGames = 1
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonusSpecialRule2 += @ThisAmount
        SET @WagerToApply           -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,-@ThisAmount,0,
          '6.1 Wager with Bonus (where TxPromo.Status=4 - special rule #2) - Restricted to Game'
      END
  END

/*
6.2 Bonus (where TxPromo.Status=4 - special rule #2) - not restricted to games
*/
WHILE (@WagerToApply > 0 AND @ThisGameBalanceBonus > 0)
  BEGIN
    SET @ThisAmount = NULL
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
      @ThisTxPromoID            = TxPromo.ID,
      @ThisAmount               = CASE WHEN @WagerToApply > TxPromo.BonusBal THEN TxPromo.BonusBal ELSE @WagerToApply END,
      @TxPromoDeltaRealBal      = RealBal,
      @TxPromoDeltaBonusBal     = BonusBal,
      @TxPromoDeltaBonusWinsBal = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.BonusBal > 0 AND
          TxPromo.Status = 4 AND
          TxPromo.RestrictToGames = 0
    ORDER BY ID

    IF @ThisTxPromoID IS NULL
      BREAK
    ELSE
      BEGIN
        SET @WagerBonusSpecialRule2 += @ThisAmount
        SET @WagerToApply           -= @ThisAmount
        UPDATE dbo.TxPromo SET BonusBal -= @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,-@ThisAmount,0,
          '6.2 Wager with Bonus (where TxPromo.Status=4 - special rule #2) - Not Restricted to Game'
      END
  END

/*
Entire wager should be applied
If not, a serious balance match error has occurred
IF @WagerToApply <> 0 BEGIN ROLLBACK TRAN SET @EntityID = @GameID SET @EventLogTypeID = 315 GOTO ReturnData END
*/
IF @WagerToApply <> 0
  BEGIN
    SET @EntityID = @PlayerID
    SET @EventLogTypeID = 317

    --Check Players Bonus and BonusWins balances vs active promos - if there are no active promos, but there is a bonus balance on player account then remove those balances
    IF (((@BalanceBonus + @BalanceBonusWins) > 0) AND
        NOT EXISTS (SELECT 1 FROM dbo.TxPromo WITH (ROWLOCK)
                      WHERE TxPromo.PlayerID = @PlayerID AND TxPromo.Status IN (2,4)))
      EXEC dbo.svrUpdatePlayerBalance
           @PlayerID=@PlayerID,
           @TxTypeID=18,
           @TxID=@TxGameRoundID,
           @AmountReal=0,
           @AmountBonus=@BalanceBonus,
           @AmountBonusWins=@BalanceBonusWins,
           @AmountPoints=0,
           @BalanceControl=-1

    IF @TrackPromoDelta = 1
      INSERT TxPromoDelta
      SELECT 0,@PlayerID,GETDATE(),@WagerToApply,@WagerToApply,@WagerToApply,@WagerToApply,@WagerToApply,@WagerToApply,
      '!!! @WagerToApply <> 0 !!!'

    GOTO ReturnData
  END

--Update the GameRoundWager metrics so we can properly pro-rata the wins to promowins and real wins (TxGameRoundWager already has the full total wager)
SET @TxGameRoundWagerReal              += @WagerReal
SET @TxGameRoundWagerRealPromo         += @WagerRealPromo
SET @TxGameRoundWagerBonus             += @WagerBonus
SET @TxGameRoundWagerBonusSpecialRule2 += @WagerBonusSpecialRule2
SET @TxGameRoundWagerBonusWins         += @WagerBonusWins


/*
Apply any Wins based on Promo Model F rules.
Model E: Uses topup to RealBalanceAtActivation for deposit promos
Model F: Uses topup to Real for deposit promos

Both model E and F apply to multiple pending TxPromos on a LIFO basis as follows:
The total @Win is pro-rated to PromoWins and Real
PromoWins = the wagers drawn from promos (including Real from Deposit bonuses) / Total Wager for Game Round
RealWins = the balance of the @Win (being wagers drawn from the Player.Real balance - not linked to any promo)

Apply PromoWins:
-RESTRICTED GAMES-
1.1 Topup all special rule #2 BonusBal TxPromos (Status=4) on a LIFO basis
2.1. Topup TxPromo.BonusBal to TxPromo.Bonus of non special rule #2 pending promos (Status=2) on a LIFO basis
3.1. Topup TxPromo.RealBal to either TxPromo.RealBalanceAtActivation (model 'E') or TxPromo.Real (model 'F') of pending Deposit TxPromos (Status=2) on a LIFO basis
4.1. Remainder of PromoWin is added to either the TxPromo.BonusWinsBal of the ACTIVE promo (if not a special rule #2 promo),
   or to Player.Real if the active promo is a special rule #2 promo on a LIFO basis.
   If no active promo exists, activate 1st available (FIFO BASIS) and use that for the BonusWins.

-UNRESTRICTED GAMES-
1.2 Topup all special rule #2 BonusBal TxPromos (Status=4) on a LIFO basis
2.2. Topup TxPromo.BonusBal to TxPromo.Bonus of non special rule #2 pending promos (Status=2) on a LIFO basis
3.2. Topup TxPromo.RealBal to either TxPromo.RealBalanceAtActivation (model 'E') or TxPromo.Real (model 'F') of pending Deposit TxPromos (Status=2) on a LIFO basis
4.2. Remainder of PromoWin is added to either the TxPromo.BonusWinsBal of the ACTIVE promo (if not a special rule #2 promo),
   or to Player.Real if the active promo is a special rule #2 promo on a LIFO basis.
   If no active promo exists, activate 1st available (FIFO BASIS) and use that for the BonusWins.

-ALL GAMES-
5. Apply any remaining win thats not a promo win to Player.Real

The date on which the promo first had a win applied to it or a wager applied to its wagering requirements gets an activation date
*/
DECLARE @RealBalanceAtActivation MONEY = 0
DECLARE @PromoWinCalc FLOAT = 0                --Win portion applicable to all TxPromos
DECLARE @PromoWin MONEY = 0

IF @Win > 0
BEGIN
  IF @AllWinsAreReal = 1 OR @TxWinTypeID = 63
    SET @PromoWinCalc = 0
  ELSE
    SET @PromoWinCalc =
      CASE @TxGameRoundWager
        WHEN 0 THEN 0
        ELSE ROUND(((@TxGameRoundWagerRealPromo + @TxGameRoundWagerBonus + @TxGameRoundWagerBonusWins + @TxGameRoundWagerBonusSpecialRule2) / @TxGameRoundWager) * CAST(@Win AS FLOAT),2)
      END

  IF @TxWinTypeID = 63
    SET @WinBonus = @Win

  SET @PromoWin=CAST(@PromoWinCalc AS MONEY)

  IF @TrackPromoDelta = 1
    INSERT TxPromoDelta
    SELECT 0,@PlayerID,GETDATE(),0,0,0,0,0,0,
    '@PromoWin='+cast(@PromoWin as varchar(50))+' @TxWintypeID='+cast(@TxWinTypeID as varchar(10))+' @ThisGameHasRestrictedPromos='+cast(@ThisGameHasRestrictedPromos as varchar(10))

  --- RESTRICTED GAMES ---
  IF @ThisGameHasRestrictedPromos = 1
  BEGIN
  /*
  1.1 TopUp BonusBal (where TxPromo.Status=4 - special rule #2) for restricted games
  */
  WHILE @PromoWin > 0
    BEGIN
      SET @ThisAmount = NULL
      SET @ThisTxPromoID = NULL

      SELECT TOP 1
        @ThisTxPromoID            = TxPromo.ID,
        @ThisAmount               = CASE WHEN @PromoWin > (TxPromo.Bonus - TxPromo.BonusBal) THEN (TxPromo.Bonus - TxPromo.BonusBal) ELSE @PromoWin END,
        @TxPromoDeltaRealBal      = RealBal,
        @TxPromoDeltaBonusBal     = BonusBal,
        @TxPromoDeltaBonusWinsBal = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
      WHERE TxPromo.PlayerID = @PlayerID AND
            (TxPromo.Bonus - TxPromo.BonusBal) > 0 AND
            TxPromo.Status = 4 AND
            TxPromo.RestrictToGames = 1
      ORDER BY ID DESC

      IF @ThisTxPromoID IS NULL
        BREAK
      ELSE
        BEGIN
          SET @WinBonus += @ThisAmount
          SET @PromoWin -= @ThisAmount
          UPDATE dbo.TxPromo SET BonusBal += @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

          IF @TrackPromoDelta = 1
            INSERT TxPromoDelta
            SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,@ThisAmount,0,
            '1.1   Win TopUp BonusBal (where TxPromo.Status=4 - special rule #2) - Restricted to Game'
        END
    END

  /*
  2.1 TopUp BonusBal (where TxPromo.Status=2) - restricted games
  */
  WHILE @PromoWin > 0
    BEGIN
      SET @ThisAmount = NULL
      SET @ThisTxPromoID = NULL

      SELECT TOP 1
        @ThisTxPromoID            = TxPromo.ID,
        @ThisAmount               = CASE WHEN @PromoWin > (TxPromo.Bonus - TxPromo.BonusBal) THEN (TxPromo.Bonus - TxPromo.BonusBal) ELSE @PromoWin END,
        @TxPromoDeltaRealBal      = RealBal,
        @TxPromoDeltaBonusBal     = BonusBal,
        @TxPromoDeltaBonusWinsBal = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
      WHERE TxPromo.PlayerID = @PlayerID AND
            (TxPromo.Bonus - TxPromo.BonusBal) > 0 AND
            TxPromo.Status = 2 AND
            TxPromo.RestrictToGames = 1
      ORDER BY ID DESC

      IF @ThisTxPromoID IS NULL
        BREAK
      ELSE
        BEGIN
          SET @WinBonus += @ThisAmount
          SET @PromoWin -= @ThisAmount
          UPDATE dbo.TxPromo
            SET BonusBal += @ThisAmount,
                DateActivated  = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
                RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
            WHERE TxPromo.ID=@ThisTxPromoID

          IF @TrackPromoDelta = 1
            INSERT TxPromoDelta
            SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,@ThisAmount,0,
            '2.1.   Win TopUp BonusBal (where TxPromo.Status=2) - Restricted to Game'
        END
    END

  /*
  3.1. TopUp RealBal (where TxPromo.Status=2 and IsDepositBonus=1) - restricted games
  03 Jun 14 - n/a
  */
  /*
  WHILE @PromoWin > 0
    BEGIN
      SET @ThisAmount = NULL
      SET @ThisTxPromoID = NULL

      SELECT TOP 1
        @ThisTxPromoID = TxPromo.ID,
        @ThisAmount = CASE
                        WHEN @PromoWin > (CASE @BonusModel
                                            WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                            WHEN 'F' THEN TxPromo.Real
                                          END - TxPromo.RealBal)
                                          THEN
                                          CASE @BonusModel
                                            WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                            WHEN 'F' THEN TxPromo.Real
                                          END - TxPromo.RealBal
                        ELSE @PromoWin
                      END,
        @TxPromoDeltaRealBal      = RealBal,
        @TxPromoDeltaBonusBal     = BonusBal,
        @TxPromoDeltaBonusWinsBal = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
           JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
      WHERE TxPromo.PlayerID = @PlayerID AND
            TxPromo.IsDepositBonus = 1 AND
            ((CASE @BonusModel
                WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                WHEN 'F' THEN TxPromo.Real
              END) - TxPromo.RealBal) > 0 AND
            TxPromo.Status = 2 AND
            TxPromo.RestrictToGames = 1
      ORDER BY ID DESC

      IF @ThisTxPromoID IS NULL
        BREAK
      ELSE
        BEGIN
          SET @WinRealPromo += @ThisAmount
          SET @PromoWin     -= @ThisAmount
          UPDATE dbo.TxPromo
            SET RealBal += @ThisAmount,
                DateActivated  = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
                RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
            WHERE TxPromo.ID=@ThisTxPromoID

          IF @TrackPromoDelta = 1
            INSERT TxPromoDelta
              SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,@ThisAmount,0,0,
              '3.1   Win TopUp RealBal (where TxPromo.Status=2 and IsDepositBonus=1) - Restricted to Game'
        END
    END
  */

  /*
  4.1. Remainder of PromoWins to BonusWins (non special rule #2) - if exists - Restricted games
  */
  IF @PromoWin > 0
    BEGIN
      --Get 1st pending (non special rule #2) FIFO promo - otherwise known as the active promo - restricted games
      SET @ThisTxPromoID = NULL

      SELECT TOP 1
          @ThisTxPromoID            = TxPromo.ID,
          @TxPromoDeltaRealBal      = RealBal,
          @TxPromoDeltaBonusBal     = BonusBal,
          @TxPromoDeltaBonusWinsBal = BonusWinsBal
        FROM dbo.TxPromo WITH (ROWLOCK)
           JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
        WHERE TxPromo.PlayerID = @PlayerID AND
              TxPromo.Status = 2 AND
              TxPromo.RestrictToGames = 1
        ORDER BY ID

      --Apply win to this promo
      IF @ThisTxPromoID IS NOT NULL
        BEGIN
          SET @WinBonusWins = @PromoWin
          SET @PromoWin = 0--its now fully applied

          UPDATE dbo.TxPromo
            SET --Remainder to BonusWins
                BonusWinsBal += @WinBonusWins,
                DateActivated = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
                RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
            WHERE TxPromo.ID=@ThisTxPromoID

          IF @TrackPromoDelta = 1
            INSERT TxPromoDelta
            SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,0,@WinBonusWins,
            '4.1   Win Remainder of PromoWins to BonusWins (non special rule #2) - if exists - Restricted to Game'
        END
      ELSE--there are no Status=2 promos, try a special rule#2 with a REAL bal that needs topping up - restricted games
        BEGIN
          /*
          4.1.2 Remainder (or portion of) of PromoWins to topup any Real Bal (for special rule #2) of first active pending promo on FIFO basis - restricted games
          */
          --Get 1st pending FIFO promo - otherwise known as the active promo - restricted games
          SET @ThisTxPromoID = NULL
          SET @ThisAmount = NULL

          SELECT TOP 1 @ThisTxPromoID = TxPromo.ID,
                       @ThisAmount =
                               CASE TxPromo.Real
                                 WHEN 0 THEN 0
                                 ELSE
                                   CASE
                                      WHEN @PromoWin > (CASE @BonusModel
                                                          WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                                          WHEN 'F' THEN TxPromo.Real
                                                        END - TxPromo.RealBal)
                                                        THEN
                                                        CASE @BonusModel
                                                          WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                                          WHEN 'F' THEN TxPromo.Real
                                                        END - TxPromo.RealBal
                                      ELSE @PromoWin
                                    END
                               END,
              @TxPromoDeltaRealBal      = RealBal,
              @TxPromoDeltaBonusBal     = BonusBal,
              @TxPromoDeltaBonusWinsBal = BonusWinsBal
            FROM dbo.TxPromo WITH (ROWLOCK)
               JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
            WHERE TxPromo.PlayerID = @PlayerID AND
                  TxPromo.Status = 4 AND
                  TxPromo.RestrictToGames = 1
            ORDER BY ID

          --Apply win to this promo
          IF @ThisTxPromoID IS NOT NULL AND @ThisAmount > 0
            BEGIN
              SET @WinRealSpecialrule2 = @ThisAmount
              SET @PromoWin -= @ThisAmount

              UPDATE dbo.TxPromo
                SET --special rule #2 to top up real
                    RealBal += @ThisAmount,
                    DateActivated = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
                    RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
                WHERE TxPromo.ID=@ThisTxPromoID

              IF @TrackPromoDelta = 1
                INSERT TxPromoDelta
                SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,@ThisAmount,0,0,
                '4.1.2   Win Remainder of PromoWins to TopUp Real (for special rule #2) of first active pending promo on FIFO basis - Restricted to Game'
            END
        END
    END
  END --IF @ThisGameHasRestrictedPromos = 1

  --Unrestricted games
  /*
  1.2 TopUp BonusBal (where TxPromo.Status=4 - special rule #2) for unrestricted games
  */
  WHILE @PromoWin > 0
    BEGIN
      SET @ThisAmount = NULL
      SET @ThisTxPromoID = NULL

      SELECT TOP 1
        @ThisTxPromoID            = TxPromo.ID,
        @ThisAmount               = CASE WHEN @PromoWin > (TxPromo.Bonus - TxPromo.BonusBal) THEN (TxPromo.Bonus - TxPromo.BonusBal) ELSE @PromoWin END,
        @TxPromoDeltaRealBal      = RealBal,
        @TxPromoDeltaBonusBal     = BonusBal,
        @TxPromoDeltaBonusWinsBal = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
      WHERE TxPromo.PlayerID = @PlayerID AND
            (TxPromo.Bonus - TxPromo.BonusBal) > 0 AND
            TxPromo.Status = 4 AND
            TxPromo.RestrictToGames = 0
      ORDER BY ID DESC

      IF @ThisTxPromoID IS NULL
        BREAK
      ELSE
        BEGIN
          SET @WinBonus += @ThisAmount
          SET @PromoWin -= @ThisAmount
          UPDATE dbo.TxPromo SET BonusBal += @ThisAmount WHERE TxPromo.ID=@ThisTxPromoID

          IF @TrackPromoDelta = 1
            INSERT TxPromoDelta
            SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,@ThisAmount,0,
            '1.2   Win TopUp BonusBal (where TxPromo.Status=4 - special rule #2) - Not Restricted to Game'
        END
    END

  /*
  2.2 TopUp BonusBal (where TxPromo.Status=2) - unrestricted games
  */
  WHILE @PromoWin > 0
    BEGIN
      SET @ThisAmount = NULL
      SET @ThisTxPromoID = NULL

      SELECT TOP 1
        @ThisTxPromoID            = TxPromo.ID,
        @ThisAmount               = CASE WHEN @PromoWin > (TxPromo.Bonus - TxPromo.BonusBal) THEN (TxPromo.Bonus - TxPromo.BonusBal) ELSE @PromoWin END,
        @TxPromoDeltaRealBal      = RealBal,
        @TxPromoDeltaBonusBal     = BonusBal,
        @TxPromoDeltaBonusWinsBal = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
      WHERE TxPromo.PlayerID = @PlayerID AND
            (TxPromo.Bonus - TxPromo.BonusBal) > 0 AND
            TxPromo.Status = 2 AND
            TxPromo.RestrictToGames = 0
      ORDER BY ID DESC

      IF @ThisTxPromoID IS NULL
        BREAK
      ELSE
        BEGIN
          SET @WinBonus += @ThisAmount
          SET @PromoWin -= @ThisAmount
          UPDATE dbo.TxPromo
            SET BonusBal += @ThisAmount,
                DateActivated  = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
                RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
            WHERE TxPromo.ID=@ThisTxPromoID

          IF @TrackPromoDelta = 1
            INSERT TxPromoDelta
            SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,@ThisAmount,0,
            '2.2   Win TopUp BonusBal (where TxPromo.Status=2) - Not Restricted to Game'
        END
    END


  /*
  3.2. TopUp RealBal (where TxPromo.Status=2 and IsDepositBonus=1) - Not Restricted to Game
  03 Jun 14 - n/a
  */
  /*
  WHILE @PromoWin > 0
    BEGIN
      SET @ThisAmount = NULL
      SET @ThisTxPromoID = NULL

      SELECT TOP 1
        @ThisTxPromoID = TxPromo.ID,
        @ThisAmount = CASE
                        WHEN @PromoWin > (CASE @BonusModel
                                            WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                            WHEN 'F' THEN TxPromo.Real
                                          END - TxPromo.RealBal)
                                          THEN
                                          CASE @BonusModel
                                            WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                            WHEN 'F' THEN TxPromo.Real
                                          END - TxPromo.RealBal
                        ELSE @PromoWin
                      END,
        @TxPromoDeltaRealBal      = RealBal,
        @TxPromoDeltaBonusBal     = BonusBal,
        @TxPromoDeltaBonusWinsBal = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
      WHERE TxPromo.PlayerID = @PlayerID AND
            TxPromo.IsDepositBonus = 1 AND
            ((CASE @BonusModel
                WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                WHEN 'F' THEN TxPromo.Real
              END) - TxPromo.RealBal) > 0 AND
            TxPromo.Status = 2 AND
            TxPromo.RestrictToGames = 0
      ORDER BY ID DESC

      IF @ThisTxPromoID IS NULL
        BREAK
      ELSE
        BEGIN
          SET @WinRealPromo += @ThisAmount
          SET @PromoWin     -= @ThisAmount
          UPDATE dbo.TxPromo
            SET RealBal += @ThisAmount,
                DateActivated  = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
                RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
            WHERE TxPromo.ID=@ThisTxPromoID

          IF @TrackPromoDelta = 1
            INSERT TxPromoDelta
              SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,@ThisAmount,0,0,
              '3.2   Win TopUp RealBal (where TxPromo.Status=2 and IsDepositBonus=1) - Not Restricted to Game'
        END
    END
  */

  /*
  4.2. Remainder of PromoWins to BonusWins (non special rule #2) - if exists - Not Restricted to Game
  */
  IF @PromoWin > 0
  BEGIN
    --Get 1st pending (non special rule #2) FIFO promo - otherwise known as the active promo
    SET @ThisTxPromoID = NULL

    SELECT TOP 1
        @ThisTxPromoID            = TxPromo.ID,
        @TxPromoDeltaRealBal      = RealBal,
        @TxPromoDeltaBonusBal     = BonusBal,
        @TxPromoDeltaBonusWinsBal = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
      WHERE TxPromo.PlayerID = @PlayerID AND
            TxPromo.Status = 2 AND
            TxPromo.RestrictToGames = 0
      ORDER BY ID

    --Apply win to this promo
    IF @ThisTxPromoID IS NOT NULL
      BEGIN
        SET @WinBonusWins = @PromoWin
        SET @PromoWin = 0--its now fully applied

        UPDATE dbo.TxPromo
          SET --Remainder to BonusWins
              BonusWinsBal += @WinBonusWins,
              DateActivated = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
              RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
          WHERE TxPromo.ID=@ThisTxPromoID

        IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,0,0,@WinBonusWins,
          '4.2   Win Remainder of PromoWins to BonusWins (non special rule #2) - if exists - Not Restricted to Game'
      END
    ELSE--there are no Status=2 promos, try a special rule#2 with a REAL bal that needs topping up
      BEGIN
        /*
        4.2.1. Remainder (or portion of) of PromoWins to topup any Real Bal (for special rule #2) of first active pending promo on FIFO basis
        */
        --Get 1st pending FIFO promo - otherwise known as the active promo
        SET @ThisTxPromoID = NULL
        SET @ThisAmount = NULL

        SELECT TOP 1 @ThisTxPromoID = TxPromo.ID,
                     @ThisAmount =
                             CASE TxPromo.Real
                               WHEN 0 THEN 0
                               ELSE
                                 CASE
                                    WHEN @PromoWin > (CASE @BonusModel
                                                        WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                                        WHEN 'F' THEN TxPromo.Real
                                                      END - TxPromo.RealBal)
                                                      THEN
                                                      CASE @BonusModel
                                                        WHEN 'E' THEN (CASE WHEN TxPromo.DateActivated IS NULL THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END)
                                                        WHEN 'F' THEN TxPromo.Real
                                                      END - TxPromo.RealBal
                                    ELSE @PromoWin
                                  END
                             END,
            @TxPromoDeltaRealBal      = RealBal,
            @TxPromoDeltaBonusBal     = BonusBal,
            @TxPromoDeltaBonusWinsBal = BonusWinsBal
          FROM dbo.TxPromo WITH (ROWLOCK)
          WHERE TxPromo.PlayerID = @PlayerID AND
                TxPromo.Status = 4 AND
                TxPromo.RestrictToGames = 0
          ORDER BY ID

        --Apply win to this promo
        IF @ThisTxPromoID IS NOT NULL AND @ThisAmount > 0
          BEGIN
            SET @WinRealSpecialrule2 = @ThisAmount
            SET @PromoWin -= @ThisAmount

            UPDATE dbo.TxPromo
              SET --special rule #2 to top up real
                  RealBal += @ThisAmount,
                  DateActivated = CASE WHEN DateActivated IS NULL THEN GETDATE() ELSE DateActivated END,
                  RealBalanceAtActivation = CASE WHEN DateActivated IS NULL THEN @BalanceReal ELSE RealBalanceAtActivation END
              WHERE TxPromo.ID=@ThisTxPromoID

            IF @TrackPromoDelta = 1
              INSERT TxPromoDelta
              SELECT @ThisTxPromoID,@PlayerID,GETDATE(),@TxPromoDeltaRealBal,@TxPromoDeltaBonusBal,@TxPromoDeltaBonusWinsBal,@ThisAmount,0,0,
              '4.2.1   Win Remainder of PromoWins to TopUp Real (for special rule #2) of first active pending promo on FIFO basis - Not Restricted to Game'
          END
      END
  END

  /*
  Balance is Real
  We combine all the Real Wins to @WinRealTotal, and only record the break up in TxPromoWager to display the calculations
  */
  SET @WinReal = (@Win-@WinBonus-@WinBonusWins-@WinRealPromo-@WinRealSpecialrule2)--this amount will include the balance portion of @PromoWin

  IF @WinReal < 0--just to ensure...
    SET @WinReal = 0

  SET @WinRealTotal = @WinReal+@WinRealPromo+@WinRealSpecialrule2

  --Double check
  IF @Win <> @WinRealTotal+@WinBonus+@WinBonusWins
    BEGIN
      SET @EntityID = @PlayerID
      SET @EventLogTypeID = 319

      IF @TrackPromoDelta = 1
        INSERT TxPromoDelta
          SELECT 0,@PlayerID,GETDATE(),@WinRealTotal,@WinBonus,@WinBonusWins,0,0,0,
            '@Win='+daga.fn.formatMoney(@Win,0)+
            ' @WinReal='+daga.fn.formatMoney(@WinReal,0)+
            ' @WinRealPromo='+daga.fn.formatMoney(@WinRealPromo,0)+
            ' @WinRealSpecialrule2='+daga.fn.formatMoney(@WinRealSpecialrule2,0)+
            ' @WinBonus='+daga.fn.formatMoney(@WinBonus,0)+
            ' @WinBonusWins='+daga.fn.formatMoney(@WinBonusWins,0)

      GOTO ReturnData
    END

END--IF @Win > 0


END--IF @Action = 'Tx'


/*
Process the game transaction
*/
--Add TxGameRound record if its a new game round
IF @TxGameRoundID = 0
  BEGIN
    /*
    Insert new game round record. Status can be 1:Complete or 2:In Progress
    MGS: Status=2. They send specific EndGame method to close a game round
    Sheriff: Status=1. We do not receive any endgame data from sheriff
    */
    IF @GameProviderID in (6,21)--mgs, ainsworth
      SET @TxGameRoundStatus = 2

    IF @GameProviderID = 7 --Baddamedia
      SET @TxGameRoundStatus = 2

    INSERT TxGameRound (GameID, GameLaunchSourceID, TxGameRef, PlayerID, TxGameSubRef, Status)
                        VALUES
                       (@GameID, @GameLaunchSourceID, @Ref, @PlayerID, @SubRef, @TxGameRoundStatus)
    SET @TxGameRoundID = SCOPE_IDENTITY()
    SET @TxGameRoundDate = @Today
    /*IF @TxGameRoundID is null or @@ERROR<>0 BEGIN ROLLBACK TRAN SET @EntityID = @GameID SET @DebugData = 'Cannot insert new TxGameRound Record' SET @EventLogTypeID = 300 GOTO ReturnData END*/
  END

--Add the TxGame transaction record
INSERT TxGame
       (GameID, PlayerID, TxGameRoundID,
        BalanceReal, BalanceBonus, BalanceBonusWins,
        Wager,Win,
        WagerReal, WagerRealPromo, WagerBonus, WagerBonusSpecialRule2, WagerBonusWins,
        WinReal, WinBonus, WinBonusWins,
        Ref, SubRef, Status)
        VALUES
        (@GameID, @PlayerID, @TxGameRoundID,
        @BalanceReal, @BalanceBonus, @BalanceBonusWins,
        CASE SUBSTRING(@Action,1,4)
             WHEN 'Void' THEN (0-@Refund)
             ELSE (@WagerReal+@WagerRealPromo+@WagerBonus+@WagerBonusSpecialRule2+@WagerBonusWins)
        END,
        CASE SUBSTRING(@Action,1,4)
             WHEN 'Void' THEN 0
             ELSE (@WinRealTotal+@WinBonus+@WinBonusWins)
        END,
        CASE SUBSTRING(@Action,1,4)
             WHEN 'Void' THEN (0-@RefundReal)
             ELSE @WagerReal
        END,
        CASE SUBSTRING(@Action,1,4)
             WHEN 'Void' THEN (0-@RefundRealPromo)
             ELSE @WagerRealPromo
        END,
        CASE SUBSTRING(@Action,1,4)
             WHEN 'Void' THEN (0-@RefundBonus)
             ELSE @WagerBonus
        END,
        CASE SUBSTRING(@Action,1,4)
             WHEN 'Void' THEN (0-@RefundBonusSpecialRule2)
             ELSE @WagerBonusSpecialRule2
        END,
        CASE SUBSTRING(@Action,1,4)
             WHEN 'Void' THEN (0-@RefundBonusWins)
             ELSE @WagerBonusWins
        END,
        @WinRealTotal, @WinBonus, @WinBonusWins, @Ref, @SubRef, 1)

SET @TxGameID = SCOPE_IDENTITY()
/*IF @TxGameID is null or @@ERROR<>0 BEGIN ROLLBACK TRAN SET @EntityID = @GameID SET @DebugData = 'Cannot insert new TxGame Record' SET @EventLogTypeID = 300 GOTO ReturnData END*/

--Update TxGameRound
DECLARE @UpdateTxGameRoundWager MONEY = 0
DECLARE @UpdateTxGameRoundWagerReal MONEY = 0
DECLARE @UpdateTxGameRoundWagerRealPromo MONEY = 0
DECLARE @UpdateTxGameRoundWagerBonus MONEY = 0
DECLARE @UpdateTxGameRoundWagerBonusSpecialRule2 MONEY = 0
DECLARE @UpdateTxGameRoundWagerBonusWins MONEY = 0

UPDATE TxGameRound
  SET TxGameSubRef = @SubRef,
      @UpdateTxGameRoundTransactions = Transactions += 1,
      @UpdateTxGameRoundWager = Wager =
        CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (Wager-@Refund) ELSE @TxGameRoundWager END,
      @UpdateTxGameRoundWin = Win += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN 0 ELSE @Win END,
      @UpdateTxGameRoundWagerReal = WagerReal += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@RefundReal) ELSE @WagerReal END,
      @UpdateTxGameRoundWagerRealPromo = WagerRealPromo += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@RefundRealPromo) ELSE @WagerRealPromo END,
      @UpdateTxGameRoundWagerBonus = WagerBonus     += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@RefundBonus) ELSE @WagerBonus END,
      @UpdateTxGameRoundWagerBonusSpecialRule2 = WagerBonusSpecialRule2 += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@RefundBonusSpecialRule2) ELSE @WagerBonusSpecialRule2 END,
      @UpdateTxGameRoundWagerBonusWins = WagerBonusWins += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@RefundBonusWins) ELSE @WagerBonusWins END,
      @UpdateTxGameRoundWinReal = WinReal += @WinRealTotal,
      @UpdateTxGameRoundWinBonus = WinBonus += @WinBonus,
      @UpdateTxGameRoundWinBonusWins = WinBonusWins += @WinBonusWins,
      @UpdateTxGameRoundDate = TxGameRound.Date,
      @UpdateTxGameRoundGameLaunchSourceID = TxGameRound.GameLaunchSourceID,
      @UpdateTxGameRoundTxGameRef = TxGameRound.TxGameRef,
      @TxGameRoundStatus = [Status] =
      CASE SUBSTRING(@Action,1,4)
           WHEN 'Void' THEN 0
           ELSE CASE WHEN @GameProviderID = 4 AND @SubRef = 999 THEN 1
                     ELSE [Status]
                END
      END
WHERE TxGameRound.ID = @TxGameRoundID

/*
Update player balance - seperate transactions for
 Wager (10)
 Win (11)
 BingoWager (30)
 BingoWin (31)
 Refund (14)
NOTE: @BalanceControl parameter is passed to svrUpdatePlayerBalance to ensure balances are identical to start of sproc
      If not, the players balance is not updated and an errorlog is written
      *** Changed to not applicable on 10Sep2012
Bingo (@GameCategoryID = 1) records zero wager transactions
*/

IF @Refund = 0 AND ((@Wager > 0 AND @GameCategoryID <> 1) OR (@GameCategoryID = 1 AND @Win=0))
  BEGIN
    DECLARE @TotalWagerReal MONEY = (@WagerReal+@WagerRealPromo)
    DECLARE @TotalWagerBonus MONEY = (@WagerBonus+@WagerBonusSpecialRule2)
    DECLARE @ProgressiveJackpotWagerPercentData AS SMALLINT = CAST(@ProgressiveJackpotWagerPercent*10000 AS SMALLINT)
    EXEC @Result = dbo.svrUpdatePlayerBalance
         @PlayerID=@PlayerID,
         @TxTypeID=@TxWagerTypeID,
         @TxID=@TxGameID,
         @AmountReal=@TotalWagerReal,
         @AmountBonus=@TotalWagerBonus,
         @AmountBonusWins=@WagerBonusWins,
         @AmountPoints=0,
         --@BalanceControl=@BalanceControl,
         @BalanceControl=-1,
         @Data=@ProgressiveJackpotWagerPercentData

    IF @Result <> 0 OR @@ERROR <> 0
      BEGIN
        SET @EntityID = @GameID
        SET @EventLogTypeID = 309 --specific fail
        GOTO ReturnData
      END

  END
---------------------------------------------------------------
--COMMIT TRAN
---------------------------------------------------------------
DECLARE @PromoID INT = 0
DECLARE @PromoTable udt.PromoTable
DECLARE @PromoPercent MONEY = 0 --Percent of Bingo Win
DECLARE @PromoValue MONEY

--Update player with any win
IF @Win > 0
  BEGIN
    --If a Bingo win that must be sent to promo, issue a promo for the win
    IF @TxWinTypeID = 63
      BEGIN
        INSERT @PromoTable SELECT * FROM
          Main.fn.GetPromo(
            @PlayerID,
            12,--PromoTypeID--12 is a bingo win that must go to bonus funds
            0,--@RequireCard
            0,--@PromoCodeRequired
            ''
          )

        --Get the 1st Promo that matched the entered @PromoCode
        SELECT TOP 1
          @PromoID = PromoID,
          @PromoPercent=Pct
        FROM @PromoTable
        ORDER BY ID

        SET @PromoID = ISNULL(@PromoID,0)

        IF @PromoID <> 0
          BEGIN
            SET @PromoValue = (@Win * (@PromoPercent/100))
            EXEC dbo.svrTxPromo @PlayerID, @PromoID, @Win, 0, @TxGameID
          END
      END
    ELSE
      EXEC dbo.svrUpdatePlayerBalance @PlayerID, @TxWinTypeID, @TxGameID, @WinRealTotal, @WinBonus, @WinBonusWins, 0, -1
  END
/*
Update the TxPromo records OR Process the Void
*/

DECLARE @RealBal MONEY
DECLARE @BonusBal MONEY
DECLARE @BonusWinsBal MONEY

IF @Action = 'Tx'
BEGIN
  /*
  UPDATE dbo.TxPromo wagering requirements based on @Wager on FIFO basis
  Add the ExpiryDate to TxPromo if there is a wager applied against it if not already set and if there is a TimeLimit on the Promo
  These are done on an individual basis so we can get a seperate TxPlayerBalance record for each change
  Where the wager completes a TxPromo, and there is still a remainder of the wager, it is used against the next TxPromo which then becomes the active TxPromo and so forth
  */
  SET @WagerToApply = ROUND((CAST(@Wager AS FLOAT)*CAST(@GameWagerPercent AS FLOAT)),2)
  DECLARE @ThisStatus VARCHAR(8) = NULL --Complete (Status will be 3 | 4) or Null (no status change)
  DECLARE @ConvertableToReal TINYINT = NULL
  DECLARE @PromoMaxConvertableToReal MONEY = NULL
  DECLARE @MaxConvertableToReal MONEY = 0
  DECLARE @ExpiryDate DATETIME2(3) = NULL
  DECLARE @SpecialRule2 BIT = NULL

  --Adjustments to Player balances
  DECLARE @RealBalAdjustment MONEY      --Where Bonus+BonusWins are transferred to real (the Bonus and BonusWins will balance the transaction)
  DECLARE @BonusBalAdjustment MONEY     --Bonus to be reduced due to completion
  DECLARE @BonusWinsBalAdjustment MONEY --BonusWins to be reduced due to completion
  DECLARE @BonusBalTransferredToReal MONEY
  DECLARE @BonusWinsBalTransferredToReal MONEY

  DECLARE @ThisAmount1 MONEY = 0 --to calculate without confusion
  DECLARE @ThisAmount2 MONEY = 0

  DECLARE @TxPromoWagersRequired MONEY = 0
  DECLARE @TxPromoWagersApplied MONEY = 0
  DECLARE @TxPromoExpiryDate DATETIME2(3)
  DECLARE @TxPromoTimeLimit INT = 0

  DECLARE @TxPromoUpdateTable udt.TxPromoUpdateTable --for replications

  WHILE @WagerToApply > 0
  BEGIN
    SET @ThisTxPromoID = NULL
    SET @ThisAmount1 = 0           --to hold the wager applied to the TxPromo
    SET @ThisAmount2 = 0           --to hold the wager applied to the TxPromo
    SET @ThisAmount = 0            --to hold the wager applied to the TxPromo
    SET @ThisStatus = NULL         --3,4 or NULL
    SET @ConvertableToReal = NULL  --will be 0:BonusBal removed, Bonuswins are convertable, 1:BonusBal and BonusWins convertable
    SET @PromoMaxConvertableToReal = NULL  --0:no limit, 0.01-1.00 = a % of BonusWins, any other amount > 1.00=the actual limit
    SET @ExpiryDate = NULL
    SET @RealBal = NULL
    SET @BonusBal = NULL
    SET @BonusWinsBal = NULL
    SET @SpecialRule2 = NULL
    SET @RealBalAdjustment = 0
    SET @BonusBalAdjustment = 0
    SET @BonusWinsBalAdjustment = 0
    SET @BonusBalTransferredToReal = 0
    SET @BonusWinsBalTransferredToReal = 0
    SET @TxPromoWagersRequired = 0
    SET @TxPromoWagersApplied = 0
    SET @TxPromoExpiryDate = NULL
    SET @TxPromoTimeLimit = 0

    --1st: Only promos restricted to this game
    SELECT TOP 1
      @ThisTxPromoID = ID,
      @TxPromoWagersRequired  = WagersRequired,
      @TxPromoWagersApplied   = WagersApplied,
      @TxPromoExpiryDate      = ExpiryDate,
      @TxPromoTimeLimit       = TimeLimit,
      @ConvertableToReal      = ConvertableToReal,
      @PromoMaxConvertableToReal = MaxConvertableToReal,
      @RealBal                = RealBal,
      @BonusBal               = BonusBal,
      @BonusWinsBal           = BonusWinsBal
    FROM dbo.TxPromo WITH (ROWLOCK)
         JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
    WHERE TxPromo.PlayerID = @PlayerID AND
          TxPromo.Status = 2 AND
          TxPromo.RestrictToGames = 1
    ORDER BY ID

    --2nd: Unrestricted promos
    IF @ThisTxPromoID IS NULL
      SELECT TOP 1
        @ThisTxPromoID = ID,
        @TxPromoWagersRequired  = WagersRequired,
        @TxPromoWagersApplied   = WagersApplied,
        @TxPromoExpiryDate      = ExpiryDate,
        @TxPromoTimeLimit       = TimeLimit,
        @ConvertableToReal      = ConvertableToReal,
        @PromoMaxConvertableToReal   = MaxConvertableToReal,
        @RealBal                = RealBal,
        @BonusBal               = BonusBal,
        @BonusWinsBal           = BonusWinsBal
      FROM dbo.TxPromo WITH (ROWLOCK)
      WHERE TxPromo.PlayerID = @PlayerID AND
            TxPromo.Status = 2 AND
            TxPromo.RestrictToGames = 0
      ORDER BY ID

    IF ISNULL(@ThisTxPromoID,0)=0 --no more records to process
      BREAK

    SET @ThisAmount1 = @WagerToApply

    SET @ThisAmount2 =
            CASE
              WHEN @ThisAmount1 <= (@TxPromoWagersRequired-@TxPromoWagersApplied)
              THEN @ThisAmount1
              ELSE (@TxPromoWagersRequired-@TxPromoWagersApplied)
            END

    SET @ThisAmount = CASE WHEN @ThisAmount2 < 0 THEN 0 ELSE @ThisAmount2 END
    SET @WagerToApply -= @ThisAmount

    SET @TxPromoExpiryDate = CASE WHEN @ThisAmount > 0 AND @TxPromoExpiryDate IS NULL
                                 THEN CASE @TxPromoTimeLimit
                                             WHEN 0 THEN NULL
                                             ELSE DATEADD(HOUR, @TxPromoTimeLimit, GETDATE())
                                           END
                                  ELSE @TxPromoExpiryDate
                             END

    SET @ThisStatus = CASE
                        WHEN @TxPromoWagersRequired=(@TxPromoWagersApplied+@ThisAmount) THEN 'Complete' --complete - special rule #2 will still be evaluated below
                        ELSE NULL --no change
                      END

    --!!! NOTE: Due to blocking and efficiency problems, we do NOT replicate the ongoing changes to TxPromo. The player Promo report which displays the ongoing status of promos is pulled from Main.TxPromo and that is controlled via svrCheckPlayerLoginSession to ensure the player can only call it once a minute
    UPDATE dbo.TxPromo
      SET WagersApplied += @ThisAmount,
          ExpiryDate     = @TxPromoExpiryDate,
          DateActivated  = CASE WHEN (@ThisAmount > 0 AND TxPromo.DateActivated IS NULL) THEN GETDATE() ELSE TxPromo.DateActivated END,
          RealBalanceAtActivation = CASE WHEN (@ThisAmount > 0 AND TxPromo.DateActivated IS NULL) THEN @BalanceReal ELSE TxPromo.RealBalanceAtActivation END,
          DateCompleted = CASE WHEN TxPromo.DateCompleted IS NULL AND @ThisStatus IS NOT NULL THEN GETDATE() ELSE TxPromo.DateCompleted END
      WHERE TxPromo.ID=@ThisTxPromoID

    --If @ThisTxPromoID complete, evaluate distribution of bonuses/bonuswins, and new status based on the Promo record configuration
    IF @ThisStatus = 'Complete'
      BEGIN
        --Drop BonusBalance for TxPromo and transfer BonusWins to Real (subject to special rule #2)
        IF @ConvertableToReal = 0
          BEGIN
            IF @ExpiryDate IS NOT NULL--Special Rule #2
              BEGIN
                SET @RealBalAdjustment      = @BonusWinsBal --add to real
                SET @BonusWinsBalAdjustment = @BonusWinsBal --remove because its transferred to real
                SET @BonusBalAdjustment     = 0             --dont do anything
                SET @SpecialRule2 = 1
              END
            ELSE--Remove bonus balances, only add BonusWins to real
              BEGIN
                SET @RealBalAdjustment      = @BonusWinsBal --add to real
                SET @BonusWinsBalAdjustment = @BonusWinsBal --remove because its transferred to real
                SET @BonusBalAdjustment     = @BonusBal     --remove because its not convertable
              END
          END

        IF @ConvertableToReal = 1--Entire Bonus+BonusWins are transferred
          BEGIN
            SET @RealBalAdjustment      = @BonusBal+@BonusWinsBal
            SET @BonusWinsBalAdjustment = @BonusWinsBal
            SET @BonusBalAdjustment     = @BonusBal
          END

        --Limit of transfer to real
        SET @MaxConvertableToReal = @PromoMaxConvertableToReal
        IF @PromoMaxConvertableToReal BETWEEN 0.01 AND 1.00--apply it as a %
          SET @MaxConvertableToReal = ROUND(@PromoMaxConvertableToReal*@RealBalAdjustment,2)

        IF @PromoMaxConvertableToReal > 0 AND @RealBalAdjustment > @MaxConvertableToReal
          BEGIN
            SET @RealBalAdjustment = @MaxConvertableToReal
            SET @BonusWinsBalTransferredToReal = CASE WHEN @RealBalAdjustment <= @BonusWinsBalAdjustment THEN @RealBalAdjustment ELSE @BonusWinsBalAdjustment END
            SET @BonusBalTransferredToReal = CASE WHEN @RealBalAdjustment-@BonusWinsBalAdjustment <= @BonusBalAdjustment THEN @RealBalAdjustment-@BonusWinsBalAdjustment ELSE @BonusBalAdjustment END
          END
        ELSE
          BEGIN
            SET @BonusWinsBalTransferredToReal = @BonusWinsBalAdjustment
            SET @BonusBalTransferredToReal     = @BonusBalAdjustment
          END

        IF @BonusWinsBalTransferredToReal < 0
           SET @BonusWinsBalTransferredToReal = 0
        IF @BonusBalTransferredToReal < 0
           SET @BonusBalTransferredToReal = 0

        DELETE @TxPromoUpdateTable

        UPDATE dbo.TxPromo
          SET --BonusWinsBal = CASE @SpecialRule2 WHEN 1 THEN 0 ELSE BonusWinsBal END,--This has changed as we want to know what the balance was before the promo was finalesed as a special rule#2
              BonusBalTransferredToReal = CASE @ConvertableToReal WHEN 0 THEN 0 ELSE @BonusBalTransferredToReal END,
              BonusWinsBalTransferredToReal = @BonusWinsBalTransferredToReal,
              [Status] = CASE @SpecialRule2 WHEN 1 THEN 4 ELSE 3 END
          OUTPUT  INSERTED.ID,
                  INSERTED.DateActivated,
                  INSERTED.RealBalanceAtActivation,
                  INSERTED.DateCompleted,
                  INSERTED.ExpiryDate,
                  INSERTED.WagersApplied,
                  INSERTED.BonusBal,
                  INSERTED.BonusWinsBal,
                  INSERTED.BonusBalTransferredToReal,
                  INSERTED.BonusWinsBalTransferredToReal,
                  INSERTED.[Status]
          INTO @TxPromoUpdateTable
        WHERE TxPromo.ID = @ThisTxPromoID

        --Write the balance update (if applicable)
        IF @RealBalAdjustment + @BonusBalAdjustment + @BonusWinsBalAdjustment <> 0
          EXEC @Result = dbo.svrUpdatePlayerBalance
               @PlayerID=@PlayerID,
               @TxTypeID=16,
               @TxID=@ThisTxPromoID,
               @AmountReal=@RealBalAdjustment,
               @AmountBonus=@BonusBalAdjustment,
               @AmountBonusWins=@BonusWinsBalAdjustment,
               @AmountPoints=0,
               @BalanceControl=-1

        --Replicate
        UPDATE Rep.dbo.TxPromo
          SET DateActivated=i.DateActivated,
              RealBalanceAtActivation=i.RealBalanceAtActivation,
              DateCompleted=i.DateCompleted,
              ExpiryDate=i.ExpiryDate,
              WagersApplied=i.WagersApplied,
              BonusBal=i.BonusBal,
              BonusWinsBal=i.BonusWinsBal,
              BonusBalTransferredToReal=i.BonusBalTransferredToReal,
              BonusWinsBalTransferredToReal=i.BonusWinsBalTransferredToReal,
              [Status] = i.[Status]
          FROM Rep.dbo.TxPromo WITH (NOLOCK) JOIN @TxPromoUpdateTable i ON i.ID = TxPromo.ID

      END--IF @ThisStatus='Complete'
  END--WHILE @WagerToApply > 0
END--IF @Action = 'Tx'

IF SUBSTRING(@Action,1,4) = 'Void'
BEGIN
  DECLARE @TxGameVoidID INT
  SET @PromoID = 0
  INSERT TxGameVoid (SourceOfVoid, TxGameRoundID, RefundAmountRequest, RefundAmount, Ref, Notes)
                     VALUES
                    (CASE @Action WHEN 'Void' THEN 'external' ELSE 'cogs' END, @TxGameRoundID, @RefundRequest, @Refund, @RefundRef, @RefundNotes)
  SET @TxGameVoidID = SCOPE_IDENTITY()

  IF @TxGameVoidID IS NOT NULL AND @@ERROR=0
  BEGIN
    /*
    Add a new TxPromo and TxPromoWagers record based on PromoType=8 (Game Void)
    Its too complicated and inefficient and unnecessary to find any pending TxPromos that were used for wagering for the voided game.
    As we are giving bonus funds back, we simply create a new TxPromo record which will be based on any exising Promo record for PromoType = 8
    If there are no Promo records with PromoType=8, then the bonus/bonuswins wagers will be added back to the players balance without a TxPromo record
    */

    IF (@RefundBonus+@RefundBonusSpecialRule2+@RefundBonusWins) > 0
    BEGIN
      SET @WagerToApply = (@RefundBonus+@RefundBonusSpecialRule2+@RefundBonusWins)
      SET @PromoID = NULL
      SET @PromoPercent = 0
      DELETE @PromoTable
      INSERT @PromoTable SELECT * FROM
        Main.fn.GetPromo(
          @PlayerID,
          8,--PromoTypeID--8 is a game void
          0,--@RequireCard
          0,--@PromoCodeRequired
          ''
        )

      --Get the 1st Promo that matched the entered @PromoCode
      SELECT TOP 1
        @PromoID = PromoID,
        @PromoPercent=Pct
      FROM @PromoTable
      ORDER BY ID

      SET @PromoID = ISNULL(@PromoID,0)
      IF @PromoID <> 0
        BEGIN
          SET @PromoValue = (@WagerToApply * (@PromoPercent/100))

          EXEC dbo.svrTxPromo @PlayerID, @PromoID, @PromoValue, 0, @TxGameVoidID
        END
    END

    /*
    Update player Balance with the Real portion of the refund and
    if there was not promo records with Promotype=8 the bonuses also
    Note that the TxPlayerBalance record for TxType=14 (voids) could have zero Real, Bonus and BonusWins if
    all the wagers were applied to the new TxPromo set above. In this case we do not write the TxPlayerBalance record
    */
    SET @RealBal  = (@RefundReal+@RefundRealPromo)
    SET @BonusBal = CASE @PromoID WHEN 0 THEN (@RefundBonus+@RefundBonusSpecialRule2) ELSE 0 END --so we dont double count from the svrTxPromo above
    SET @BonusWinsBal = CASE @PromoID WHEN 0 THEN @RefundBonusWins ELSE 0 END --so we dont double count from the svrTxPromo above

    IF (@RealBal+@BonusBal+@BonusWinsBal) > 0
      EXEC @Result = dbo.svrUpdatePlayerBalance
           @PlayerID=@PlayerID,
           @TxTypeID=14,
           @TxID=@TxGameID,
           @AmountReal=@RealBal,
           @AmountBonus=@BonusBal,
           @AmountBonusWins=@BonusWinsBal,
           @AmountPoints=0,
           @BalanceControl=-1
  END--@TxGameVoidID IS NOT NULL AND @@ERROR=0
END--IF SUBSTRING(@Action,1,4) = 'Void'

--Update PlayerLimits if applicable
IF @WagerLimitSession IS NOT NULL
  BEGIN
    UPDATE dbo.PlayerLimit
    SET WagerThisSession  += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@Refund) ELSE @Wager END,
        WagerThisDay      += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@Refund) ELSE @Wager END,
        WagerThisWeek     += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@Refund) ELSE @Wager END,
        WagerThisMonth    += CASE SUBSTRING(@Action,1,4) WHEN 'Void' THEN (0-@Refund) ELSE @Wager END,
        LossThisSession   += @Loss,
        LossThisDay       += @Loss,
        LossThisWeek      += @Loss,
        LossThisMonth     += @Loss
     WHERE PlayerLimit.PlayerID = @PlayerID
  END

------------------------------------------------------------
ReturnData:
------------------------------------------------------------

/*
Get new player balances
*/
SELECT @BalanceReal      = Player.Real,
       @BalanceBonus     = Player.Bonus,
       @BalanceBonusWins = Player.BonusWins
  FROM dbo.Player
  WHERE Player.ID = @PlayerID


/*
Cancel all Status=2 and Status=4 TxPromos ONLY where @TxGameRoundStatus=1 (completed game) based on the TxPromo.BonusVoidBalanceThreshold
Do not cancel TxPromos that were issued within the 30 seconds (see TxPromoID=1501427 which was cancelled before player balance could be updated)
*/
IF @TxGameRoundStatus=1 AND ((@BalanceReal + @BalanceBonus + @BalanceBonusWins) < 1) --1 is cogs maximum threshold to cancel a promo
BEGIN
  DECLARE @FirstActivePromoID INT = 0
  /*
  If this is a bingo game, and @Win=0 find the first active PromoID and keep it alive
  This is in case 3 minutes later the player wins on the game.
  This is to prevent players dumping their full bonus on 1 bingogame in the hope of getting the win paid as real.
  If a player makes a successful deposit, and at the time of making the deposit their total balance < 1 we will clear any active promo at that point
  */
  IF @Win = 0 AND @GameCategoryID = 1
    BEGIN
      SET @FirstActivePromoID = NULL
      SELECT TOP 1 @FirstActivePromoID = TxPromo.ID
        FROM TxPromo WITH (ROWLOCK)
        WHERE TxPromo.PlayerID = @PlayerID AND
              TxPromo.Status IN (2,4) AND
              DATEDIFF(second,TxPromo.Date,GETDATE()) > 30
      ORDER BY ID

      SET @FirstActivePromoID = ISNULL(@FirstActivePromoID,0)

      IF @TrackPromoDelta = 1
          INSERT TxPromoDelta
          SELECT 0,@PlayerID,GETDATE(),0,0,0,0,0,0,
          '@FirstActivePromoID='+cast(@FirstActivePromoID as varchar(50))
    END

  SET @ThisTxPromoID = 0
  WHILE @ThisTxPromoID IS NOT NULL
  BEGIN
    SET @ThisTxPromoID = NULL
    SET @BonusBal = 0
    SET @BonusWinsBal = 0
    SET @ConvertableToReal = NULL --will be 0:BonusBal removed, Bonuswins are convertable, 1:BonusBal and BonusWins convertable
    SET @MaxConvertableToReal = NULL
    SET @RealBalAdjustment = 0
    SET @BonusBalAdjustment = 0
    SET @BonusWinsBalAdjustment = 0

    SELECT TOP 1
      @ThisTxPromoID        = TxPromo.ID,
      @ConvertableToReal    = TxPromo.ConvertableToReal,
      @MaxConvertableToReal = TxPromo.MaxConvertableToReal,
      @BonusBal             = TxPromo.BonusBal,
      @BonusWinsBal         = CASE TxPromo.Status WHEN 4 THEN 0 ELSE TxPromo.BonusWinsBal END--Special rule#2 promos assume no bonus wins bal available
    FROM dbo.TxPromo WITH (ROWLOCK)
    WHERE TxPromo.ID <> @FirstActivePromoID AND
          TxPromo.PlayerID = @PlayerID AND
          TxPromo.Status IN (2,4) AND
          DATEDIFF(second,TxPromo.Date,GETDATE()) > 30 AND
          ((@BalanceReal + @BalanceBonus + @BalanceBonusWins) <= TxPromo.BonusVoidBalanceThreshold)
    ORDER BY ID

    IF @ThisTxPromoID IS NULL --no more records to process
      BREAK

    UPDATE dbo.TxPromo
      SET DateCompleted = CASE WHEN TxPromo.DateCompleted IS NULL THEN GETDATE() ELSE TxPromo.DateCompleted END,
          [Status] = 5
    WHERE TxPromo.ID=@ThisTxPromoID

    IF @TrackPromoDelta = 1
        INSERT TxPromoDelta
        SELECT 0,@PlayerID,GETDATE(),0,0,0,0,0,0,
        'Set TxPromo.Status=5 @TxPromoID='+cast(@ThisTxPromoID as varchar(50))

    --distribute remaining bonus and bonus wins to real based on @ConvertableToReal
    --Drop BonusBalance for TxPromo and transfer BonusWins to Real
    IF @ConvertableToReal = 0
      BEGIN
        SET @RealBalAdjustment      = @BonusWinsBal --add to real
        SET @BonusWinsBalAdjustment = @BonusWinsBal --remove because its transferred to real
        SET @BonusBalAdjustment     = @BonusBal     --remove because its not convertable
      END

    IF @ConvertableToReal = 1--Entire Bonus+BonusWins are transferred
      BEGIN
        SET @RealBalAdjustment      = @BonusBal+@BonusWinsBal
        SET @BonusWinsBalAdjustment = @BonusWinsBal
        SET @BonusBalAdjustment     = @BonusBal
      END

    DELETE @TxPromoUpdateTable

    UPDATE dbo.TxPromo
      SET BonusBalTransferredToReal = CASE WHEN @ConvertableToReal=0 THEN 0 ELSE @BonusBalAdjustment END,
          BonusWinsBalTransferredToReal = @BonusWinsBalAdjustment
      OUTPUT  INSERTED.ID,
              INSERTED.DateActivated,
              INSERTED.RealBalanceAtActivation,
              INSERTED.DateCompleted,
              INSERTED.ExpiryDate,
              INSERTED.WagersApplied,
              INSERTED.BonusBal,
              INSERTED.BonusWinsBal,
              INSERTED.BonusBalTransferredToReal,
              INSERTED.BonusWinsBalTransferredToReal,
              INSERTED.[Status]
      INTO @TxPromoUpdateTable
      WHERE TxPromo.ID = @ThisTxPromoID

    --Write the balance update (if applicable)
    IF @RealBalAdjustment + @BonusBalAdjustment + @BonusWinsBalAdjustment <> 0
      EXEC @Result = dbo.svrUpdatePlayerBalance
           @PlayerID=@PlayerID,
           @TxTypeID=16,
           @TxID=@ThisTxPromoID,
           @AmountReal=@RealBalAdjustment,
           @AmountBonus=@BonusBalAdjustment,
           @AmountBonusWins=@BonusWinsBalAdjustment,
           @AmountPoints=0,
           @BalanceControl=-1

    --Replicate
    UPDATE Rep.dbo.TxPromo
      SET DateActivated=i.DateActivated,
          RealBalanceAtActivation=i.RealBalanceAtActivation,
          DateCompleted=i.DateCompleted,
          ExpiryDate=i.ExpiryDate,
          WagersApplied=i.WagersApplied,
          BonusBal=i.BonusBal,
          BonusWinsBal=i.BonusWinsBal,
          BonusBalTransferredToReal=i.BonusBalTransferredToReal,
          BonusWinsBalTransferredToReal=i.BonusWinsBalTransferredToReal,
          [Status] = i.[Status]
      FROM Rep.dbo.TxPromo WITH (NOLOCK) JOIN @TxPromoUpdateTable i ON i.ID = TxPromo.ID
  END--WHILE @ThisTxPromoID IS NOT NULL

  /*
  Get new player balances in case there was a change in the above
  */
  SELECT @BalanceReal      = Player.Real,
         @BalanceBonus     = Player.Bonus,
         @BalanceBonusWins = Player.BonusWins
    FROM dbo.Player
    WHERE Player.ID = @PlayerID
END--IF @TxGameRoundStatus=1 AND ((@BalanceReal + @BalanceBonus + @BalanceBonusWins) < 1)


/*
Check that any active promo's real Balances do not exceed the Player balances (in case the promos did not apply)
03Jun14 - n/a
*/
/*
IF @Action IN ('Tx','EndGameRound')
BEGIN
  DECLARE @TotalRealOnActivePromos MONEY = 0
  SELECT @TotalRealOnActivePromos = SUM(RealBal)
  FROM dbo.TxPromo WITH (NOLOCK)
  WHERE TxPromo.PlayerID = @PlayerID AND
        TxPromo.Status IN (2,4) AND
        TxPromo.IsDepositBonus=1

  IF @TotalRealOnActivePromos > @BalanceReal
  BEGIN
    SET @TotalRealOnActivePromos      = @TotalRealOnActivePromos-@BalanceReal

    SET @ThisTxPromoID = 0
    WHILE @ThisTxPromoID IS NOT NULL
    BEGIN
      SET @ThisTxPromoID = NULL
      SET @RealBalAdjustment = 0

      SELECT TOP 1
        @ThisTxPromoID          = TxPromo.ID,
        @RealBalAdjustment      = CASE WHEN TxPromo.RealBal  >= @TotalRealOnActivePromos  THEN @TotalRealOnActivePromos ELSE TxPromo.RealBal END
      FROM dbo.TxPromo WITH (ROWLOCK)
      WHERE TxPromo.PlayerID = @PlayerID AND
            TxPromo.Status IN (2,4) AND
            TxPromo.IsDepositBonus=1 AND
            TxPromo.RealBal > 0
      ORDER BY ID

      IF @ThisTxPromoID IS NULL --no more records to process
        BREAK

      UPDATE dbo.TxPromo
        SET TxPromo.RealBal -= @RealBalAdjustment
        WHERE TxPromo.ID=@ThisTxPromoID

      SET @TotalRealOnActivePromos -= @RealBalAdjustment

      IF @TotalRealOnActivePromos = 0
        BREAK
    END--WHILE @ThisTxPromoID IS NOT NULL
  END--if promos bals are > player balances
END
*/

--Competition Triggers - only based on completed games
IF @TxGameRoundStatus=1 AND @Action IN ('Tx','EndGameRound')
BEGIN
  SET @TxGameRoundWager = 0
  DECLARE @TxGameRoundWin MONEY = 0

  SELECT @TxGameRoundWager=Wager,
         @TxGameRoundWin=Win
  FROM   dbo.TxGameRound
  WHERE  ID = @TxGameRoundID

  IF (@Action = 'Tx' AND @GameProviderID<>6) OR (@Action = 'EndGameRound' AND @GameProviderID=6)
  BEGIN
    --wager amount and count
    IF @TxGameRoundWager > 0
      BEGIN
        BEGIN TRY
        -- wager amount
          EXEC dbo.svrCompetitionTrigger 4,@PlayerID,@GameID,@TxGameRoundWager
        END TRY
        BEGIN CATCH
          --ignores error
        END CATCH

        BEGIN TRY
        -- wager count
          EXEC dbo.svrCompetitionTrigger 5,@PlayerID,@GameID,@TxGameRoundWager
        END TRY
        BEGIN CATCH
          --ignores error
        END CATCH
      END

    --win amount and count and biggest single win
    IF @Win > 0
      BEGIN
        BEGIN TRY
        -- win amount
          EXEC dbo.svrCompetitionTrigger 7,@PlayerID,@GameID,@TxGameRoundWin
        END TRY
        BEGIN CATCH
          --ignores error
        END CATCH

        BEGIN TRY
        -- win count
          EXEC dbo.svrCompetitionTrigger 8,@PlayerID,@GameID,@TxGameRoundWin
        END TRY
        BEGIN CATCH
          --ignores error
        END CATCH

        BEGIN TRY
        -- single win
          EXEC dbo.svrCompetitionTrigger 18,@PlayerID,@GameID,@TxGameRoundWin
        END TRY
        BEGIN CATCH
          --ignores error
        END CATCH
      END
  END

  --lose in a row - losses are based on completed game rounds
  IF @TxGameRoundWin = 0 AND ((@Action = 'Tx' AND @GameProviderID<>6) OR (@Action = 'EndGameRound' AND @GameProviderID=6))
  BEGIN
    BEGIN TRY
      -- loss in a row
      EXEC dbo.svrCompetitionTrigger 10,@PlayerID,@GameID,0
    END TRY
    BEGIN CATCH
      --ignores error
    END CATCH

    BEGIN TRY
      -- lose amount
      EXEC dbo.svrCompetitionTrigger 9,@PlayerID,@GameID,@TxGameRoundWager
    END TRY
	BEGIN CATCH
	  --ignores error
	END CATCH
  END

  /*
  Get new player balances in case there was a change in the above
  */
  SELECT @BalanceReal      = Player.Real,
         @BalanceBonus     = Player.Bonus,
         @BalanceBonusWins = Player.BonusWins
    FROM dbo.Player
    WHERE Player.ID = @PlayerID
END


--Calculate the @ThisGameBalanceBonus and @ThisGameBalanceBonusWins that are specifically available for the game
IF @WagerWithBonusFunds = 1
BEGIN
  --Get a total of the bonus balances that the player can use on this game
  SELECT
    @ThisGameBalanceBonus     = ISNULL(SUM(TxPromo.BonusBal),0),
    @ThisGameBalanceBonusWins = ISNULL(SUM(CASE TxPromo.Status WHEN 4 THEN 0 ELSE TxPromo.BonusWinsBal END),0)--Special rule#2 promos assume zero BonusWins bal
  FROM dbo.TxPromo WITH (NOLOCK)
       JOIN dbo.PromoGame WITH (NOLOCK) ON PromoGame.GameID = @GameID AND PromoGame.PromoID = TxPromo.PromoID
  WHERE TxPromo.PlayerID = @PlayerID AND
        TxPromo.RestrictToGames = 1 AND
        TxPromo.Status IN (2,4)

  --Add that to the bonus balances that the player can use without restriction
  SELECT
    @ThisGameBalanceBonus     += ISNULL(SUM(TxPromo.BonusBal),0),
    @ThisGameBalanceBonusWins += ISNULL(SUM(CASE TxPromo.Status WHEN 4 THEN 0 ELSE TxPromo.BonusWinsBal END),0)--Special rule#2 promos assume zero BonusWins bal

  FROM dbo.TxPromo WITH (NOLOCK)
  WHERE TxPromo.PlayerID = @PlayerID AND
        TxPromo.RestrictToGames = 0 AND
        TxPromo.Status IN (2,4)

  --Ensure the total >=0 and <= Player bonus balances
  IF @ThisGameBalanceBonus < 0
    SET @ThisGameBalanceBonus = 0

  IF @ThisGameBalanceBonus > @BalanceBonus
    SET @ThisGameBalanceBonus = @BalanceBonus

  IF @ThisGameBalanceBonusWins < 0
    SET @ThisGameBalanceBonusWins = 0

  IF @ThisGameBalanceBonusWins > @BalanceBonusWins
    SET @ThisGameBalanceBonusWins = @BalanceBonusWins
END

--Check low balance deposit promo offers to (@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins) and has not been offered in last 5 hours
IF (@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins) < @LowBalancePromoThreshold
BEGIN
  IF NOT EXISTS (SELECT 1 FROM dbo.PromoLowDepositOffer WITH (NOLOCK) WHERE @PlayerID=PlayerID AND [Date] >= DATEADD(HOUR,-5,GETDATE()))
  BEGIN
    SET @GetLowBalancePromoID = NULL
    DELETE @PromoTable

    INSERT @PromoTable SELECT * FROM
      Main.fn.GetPromo(
        @PlayerID,
        2,--PromoTypeID
        0,--@RequireCard
        0,--@PromoCodeRequired
        '{*LowBal*}'--@PromoCode - special code to only get promos that are LowBal deposit ticker promos
      )

    SELECT TOP 1 @GetLowBalancePromoID = PromoID FROM @PromoTable ORDER BY ID

    --If we have a valid promo ID, write it to PromoLowDepositOffer and set @GetLowBalancePromoID to the newly written record
    IF @GetLowBalancePromoID IS NOT NULL
      BEGIN
        UPDATE dbo.PromoLowDepositOffer SET Status=0 WHERE PlayerID=@PlayerID AND Status IN (1,2)
        INSERT dbo.PromoLowDepositOffer (PlayerID,PromoID,Date,Status) VALUES (@PlayerID,@GetLowBalancePromoID,GETDATE(),1)
      END
  END
END


--EventLog
IF @Result <> 0 OR @EventLogTypeID <> 0
  BEGIN
    SET @Data = 'PlayerID: ' + CAST(@PlayerID AS VARCHAR(10)) +
                ' GameProviderID: ' + CAST(@GameProviderID AS VARCHAR(10)) +
                ' GameProviderGameID: ' + ISNULL(@GameProviderGameID,'0') +
                ' GameLaunchSourceID: ' + CAST(@GameProviderID AS VARCHAR(10)) +
                ' Action: ' + @Action +
                ' Wager: ' + daga.fn.formatMoney(@Wager,0) +
                ' Win: ' + daga.fn.formatMoney(@Win,0) +
                ' Ref: ' + CAST(@Ref AS VARCHAR(20)) +
                ' SubRef: ' + CAST(@SubRef AS VARCHAR(20)) +
                ' TxWagerTypeID: ' + CAST(@TxWagerTypeID AS VARCHAR(10)) +
                ' TxWinTypeID: ' + CAST(@TxWinTypeID AS VARCHAR(10))

    IF @EventLogTypeID IN (304,309,317,319)
      SET @Data += ' Real: ' + daga.fn.formatMoney(@BalanceReal,0) +
                   ' Bonus: ' + daga.fn.formatMoney(@BalanceBonus,0) +
                   ' BonusWins: ' + daga.fn.formatMoney(@BalanceBonusWins,0)

    IF @DebugData <> ''
      SET @Data += ' Debug Data: '+@DebugData

    EXEC dbo.svrEventLog @EventLogTypeID, @EntityID, @IP, @Data
  END

IF @GameProviderID = 3 --Sheriff
  BEGIN
    SELECT 'Result'  = @EventLogTypeID,
           'agsRef'  = @TxGameID,
           'Balance' = daga.fn.formatMoney((@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins),0),
           'Real'    = daga.fn.formatMoney(@BalanceReal,0),
           'Bonus'   = daga.fn.formatMoney(@ThisGameBalanceBonus,0),
           'BonusWins' = daga.fn.formatMoney(@ThisGameBalanceBonusWins,0),
           'WagerReal' = daga.fn.formatMoney(@WagerReal+@WagerRealPromo,0),
           'WagerBonus' = daga.fn.formatMoney(@WagerBonus+@WagerBonusSpecialRule2,0),
           'WagerBonusWins' = daga.fn.formatMoney(@WagerBonusWins,0),
           'WinReal'  = daga.fn.formatMoney(@WinRealTotal,0),
           'WinBonus' = daga.fn.formatMoney(@Win-@WinRealTotal,0),
           'Refund' = daga.fn.formatMoney(@Refund,0)
  END

IF @GameProviderID = 9 --Bally
  BEGIN
    IF @EventLogTypeID <> 300
      UPDATE TxPlayerBally WITH (ROWLOCK) SET Status=0 WHERE PlayerID = @PlayerID
    IF @Action = 'Recon'
      --SELECT ALL TX's or Select only the specific @SubRef being looked for
      BEGIN
        IF @EventLogTypeID <> 0
          BEGIN
            SELECT 'Result' = @EventLogTypeID
            RETURN
          END
        IF @SubRef = 0
          SELECT 'Result'     = @EventLogTypeID,
                 'GameProviderGameID'  = @GameProviderGameID,
                 'TxGameRoundComplete' = CASE TxGameRound.Status WHEN 2 THEN 0 ELSE 1 END, --voided still means its complete
                 'TxGameRoundID' = TxGameRound.ID,
                 'TxGameID'   = TxGame.ID,
                 'SubRef'     = TxGame.SubRef,
                 'CurrencyCode' = @PlayerCurrencyCode,
                 'BallyType'         = CASE WHEN TxGame.Wager < 0 THEN 'REFUND'
                                            WHEN TxGame.Wager > 0 THEN 'STAKE'
                                            WHEN (TxGame.Wager=0 AND TxGame.Win >= 0) THEN 'WIN'
                                            ELSE 'STAKE'
                                       END,
                 'Amount'       = CASE WHEN TxGame.Wager < 0 THEN daga.fn.formatMoney(-(TxGame.Wager),0)
                                       WHEN TxGame.Wager > 0 THEN daga.fn.formatMoney(TxGame.Wager,0)
                                       WHEN TxGame.Win > 0 THEN daga.fn.formatMoney(TxGame.Win,0)
                                       ELSE daga.fn.formatMoney(0,0)
                                  END
          FROM dbo.TxGameRound (NOLOCK)
               JOIN dbo.TxGame (NOLOCK) ON TxGame.TxGameRoundID = TxGameRound.ID
          WHERE TxGameRound.ID = @TxGameRoundID
          ORDER BY TxGame.ID
        ELSE
          SELECT 'Result'              = @EventLogTypeID,
                 'GameProviderGameID'  = @GameProviderGameID,
                 'TxGameRoundComplete' = CASE TxGameRound.Status WHEN 2 THEN 0 ELSE 1 END, --voided still means its complete
                 'TxGameRoundID' = TxGameRound.ID,
                 'TxGameID'   = TxGame.ID,
                 'SubRef'     = TxGame.SubRef,
                 'CurrencyCode' = @PlayerCurrencyCode,
                 'BallyType'         = CASE WHEN TxGame.Wager < 0 THEN 'REFUND'
                                            WHEN TxGame.Wager > 0 THEN 'STAKE'
                                            WHEN (TxGame.Wager=0 AND TxGame.Win >= 0) THEN 'WIN'
                                            ELSE 'STAKE'
                                       END,
                 'Amount'       = CASE WHEN TxGame.Wager < 0 THEN daga.fn.formatMoney(-(TxGame.Wager),0)
                                       WHEN TxGame.Wager > 0 THEN daga.fn.formatMoney(TxGame.Wager,0)
                                       WHEN TxGame.Win > 0 THEN daga.fn.formatMoney(TxGame.Win,0)
                                       ELSE daga.fn.formatMoney(0,0)
                                  END
          FROM dbo.TxGameRound (NOLOCK)
               JOIN dbo.TxGame (NOLOCK) ON TxGame.TxGameRoundID = TxGameRound.ID
          WHERE TxGameRound.ID = @TxGameRoundID AND
                TxGame.SubRef BETWEEN @SubRef AND @SubRef+99
          ORDER BY TxGame.ID
      END
    ELSE
      SELECT 'Result'  = @EventLogTypeID,
             'TxGameRoundID' = @TxGameRoundID,
             'TxGameID'  = @TxGameID,
             'Balance' = daga.fn.formatMoney((@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins),0),
             'Real'    = daga.fn.formatMoney(@BalanceReal,0),
             'Bonus'   = daga.fn.formatMoney(@ThisGameBalanceBonus,0),
             'BonusWins' = daga.fn.formatMoney(@ThisGameBalanceBonusWins,0),
             'WagerReal' = daga.fn.formatMoney(@WagerReal+@WagerRealPromo,0),
             'WagerBonus' = daga.fn.formatMoney(@WagerBonus+@WagerBonusSpecialRule2,0),
             'WagerBonusWins' = daga.fn.formatMoney(@WagerBonusWins,0),
             'WinReal'  = daga.fn.formatMoney(@WinRealTotal,0),
             'WinBonus' = daga.fn.formatMoney(@Win-@WinRealTotal,0),
             'Refund' = daga.fn.formatMoney(@Refund,0),
             'CountryCode' = @PlayerCountryCode,
             'CurrencyCode' = @PlayerCurrencyCode,
             'LanguageCode' = @PlayerLanguageCode
  END

IF @GameProviderID = 14 --PlaynGo
  BEGIN
    IF @Action = 'GetBalance'
      BEGIN
        SELECT 'Result'  = @EventLogTypeID,
               'agsRef'  = @TxGameID,
               'Balance' = daga.fn.formatMoney((@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins),0),
               'Real'    = daga.fn.formatMoney(@BalanceReal,0),
               'Bonus'   = daga.fn.formatMoney(@ThisGameBalanceBonus,0),
               'BonusWins' = daga.fn.formatMoney(@ThisGameBalanceBonusWins,0),
               'WagerReal' = daga.fn.formatMoney(@WagerReal+@WagerRealPromo,0),
               'WagerBonus' = daga.fn.formatMoney(@WagerBonus+@WagerBonusSpecialRule2,0),
               'WagerBonusWins' = daga.fn.formatMoney(@WagerBonusWins,0),
               'Refund' = daga.fn.formatMoney(@Refund,0),
               'GetLowBalancePromoID' = @GetLowBalancePromoID,
               'CurrencyCode' = @PlayerCurrencyCode,
               'CountryCode' = @PlayerCountryCode,
               'DOB' = @PlayerDOB,
               'RegDate' = @PlayerRegDate,
               'Gender' = @PlayerGender
      END

    IF @Action <> 'GetBalance'
      BEGIN
        SELECT 'Result'  = @EventLogTypeID,
               'agsRef'  = @TxGameID,
               'Balance' = daga.fn.formatMoney((@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins),0),
               'Real'    = daga.fn.formatMoney(@BalanceReal,0),
               'Bonus'   = daga.fn.formatMoney(@ThisGameBalanceBonus,0),
               'BonusWins' = daga.fn.formatMoney(@ThisGameBalanceBonusWins,0),
               'WagerReal' = daga.fn.formatMoney(@WagerReal+@WagerRealPromo,0),
               'WagerBonus' = daga.fn.formatMoney(@WagerBonus+@WagerBonusSpecialRule2,0),
               'WagerBonusWins' = daga.fn.formatMoney(@WagerBonusWins,0),
               'Refund' = daga.fn.formatMoney(@Refund,0),
               'GetLowBalancePromoID' = @GetLowBalancePromoID,
               'CurrencyCode' = @PlayerCurrencyCode
      END
  END


IF @GameProviderID = 19 -- scientific games
  BEGIN
    SELECT 'Result'  = @EventLogTypeID,
           'agsRef'  = @TxGameID,
           'Balance' = daga.fn.formatMoney((@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins),0),
           'Real'    = daga.fn.formatMoney(@BalanceReal,0),
           'Bonus'   = daga.fn.formatMoney(@ThisGameBalanceBonus,0),
           'BonusWins' = daga.fn.formatMoney(@ThisGameBalanceBonusWins,0),
           'WagerReal' = daga.fn.formatMoney(@WagerReal+@WagerRealPromo,0),
           'WagerBonus' = daga.fn.formatMoney(@WagerBonus+@WagerBonusSpecialRule2,0),
           'WagerBonusWins' = daga.fn.formatMoney(@WagerBonusWins,0),
           'Refund' = daga.fn.formatMoney(@Refund,0),
           'GetLowBalancePromoID' = @GetLowBalancePromoID,
           'CountryCode' = @PlayerCountryCode,
           'CurrencyCode' = @PlayerCurrencyCode,
           'LanguageCode' = @PlayerLanguageCode
		
  END


IF @GameProviderID NOT IN (3,9,14,19) --others
  BEGIN
    SELECT 'Result'  = @EventLogTypeID,
           'agsRef'  = @TxGameID,
           'Balance' = daga.fn.formatMoney((@BalanceReal + @ThisGameBalanceBonus + @ThisGameBalanceBonusWins),0),
           'Real'    = daga.fn.formatMoney(@BalanceReal,0),
           'Bonus'   = daga.fn.formatMoney(@ThisGameBalanceBonus,0),
           'BonusWins' = daga.fn.formatMoney(@ThisGameBalanceBonusWins,0),
           'WagerReal' = daga.fn.formatMoney(@WagerReal+@WagerRealPromo,0),
           'WagerBonus' = daga.fn.formatMoney(@WagerBonus+@WagerBonusSpecialRule2,0),
           'WagerBonusWins' = daga.fn.formatMoney(@WagerBonusWins,0),
           'Refund' = daga.fn.formatMoney(@Refund,0),
           'GetLowBalancePromoID' = @GetLowBalancePromoID
		
  END

RETURN
--#--
END
