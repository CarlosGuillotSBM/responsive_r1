USE [Main]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Player_MobileNumberValidated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Player] DROP CONSTRAINT [DF_Player_MobileNumberValidated]
END

GO
ALTER TABLE Main..Player	
ALTER COLUMN MobileNumberValidated smallint
GO

USE [Main]
GO

ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_MobileNumberValidated]  DEFAULT ((0)) FOR [MobileNumberValidated]
GO


