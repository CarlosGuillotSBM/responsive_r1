
/*

	COPY ALL THE CONTENT AND CONTENT CATEGORIES FROM 1 SKIN TO ANOTHER

IF you want a clean start use

delete c from Responsive..Content c (nolock) join Responsive..ContentCategory cc (nolock) on cc.ID = c.ContentCategoryID 
where cc.SkinID = 12


delete cc from Responsive..ContentCategory cc (nolock) 
where cc.SkinID = 12

If you want just update one page add the key of the page in @FromPage

*/
declare @fromSkinID int = 5
declare @toSkinID int = 12
declare @FromPage nvarchar(100) = ''





declare @t table(id int)
declare @c table(id int)
declare @id int
declare @cid int
declare @dcctid int
declare @page varchar(100)
declare @key varchar(100)
declare @description varchar(100)
declare @nextID int

declare @stagingID int
declare @liveID int


-- get the next contentID
select top 1 @nextID = ID+1 from Content order by ID desc


IF (@FromPage <> '')
BEGIN
insert @t
select id 
FROM [Responsive].[dbo].[ContentCategory]
where SkinID=@fromSkinID
and [TYPE]=2
	  and Page = @FromPage 	
END 
ELSE
BEGIN
insert @t
select id 
FROM [Responsive].[dbo].[ContentCategory]
where SkinID=@fromSkinID
and [TYPE]=2
END

while (select COUNT(*) from @t) > 0
begin

	select top 1 @id = ID from @t 
	
		insert [ContentCategory]
		SELECT 
		   @toskinID
		  ,[DefaultContentCategoryTypeID]
		  ,[Page]
		  ,[Key]
		  ,[Description]
		  ,2
		  ,NULL
	  FROM [Responsive].[dbo].[ContentCategory]
	  where ID = @id			

	  set @liveID = SCOPE_IDENTITY()

		insert [ContentCategory]
		SELECT 
		   @toskinID
		  ,[DefaultContentCategoryTypeID]
		  ,[Page]
		  ,[Key]
		  ,[Description]
		  ,1
		  ,@liveID
	  FROM [Responsive].[dbo].[ContentCategory]
	  where ID = @id 


	set @stagingID = SCOPE_IDENTITY()

	insert @c
	select distinct ID from Content where contentcategoryID = @id
	
	while (select COUNT(*) from @c) > 0
	begin
		select top 1 @cid = ID from @c 
		
		
		insert [Content]
		SELECT @nextID
			  ,[Device]
			  ,@stagingID
			  ,[SortOrder]
			  ,[ContentCategoryTypeID]
			  ,[Name]
			  ,[Title]
			  ,[Image]
			  ,[ImageAlt]
			  ,[Image1]
			  ,[ImageAlt1]
			  ,[DetailsURL]
			  ,[Intro]
			  ,[Body]
			  ,[Terms]
			  ,[Text1]
			  ,[Text2]
			  ,[Text3]
			  ,[Text4]
			  ,[Text5]
			  ,[Text6]
			  ,[Text7]
			  ,[Text8]
			  ,[Text9]
			  ,[DateFrom_DST]
			  ,[DateTo_DST]
			  ,[OfferID]
			  ,[StateID]
			  ,[StatusID]
			  ,[DateUpdated]
			  ,[DateCreated]
			  ,[StagingID]
			  ,[UserID]
			  ,[EditingUserID]
			  ,[Text10]
			  ,[Text11]
			  ,[Text12]
			  ,[Text13]
			  ,[Text14]
			  ,[Text15]
			  ,[Text16]
			  ,[Text17]
			  ,[Text18]
		  FROM [Responsive].[dbo].[Content]
		  where ID = @cid

		insert [Content]
		SELECT @nextID
			  ,[Device]
			  ,@liveID
			  ,[SortOrder]
			  ,[ContentCategoryTypeID]
			  ,[Name]
			  ,[Title]
			  ,[Image]
			  ,[ImageAlt]
			  ,[Image1]
			  ,[ImageAlt1]
			  ,[DetailsURL]
			  ,[Intro]
			  ,[Body]
			  ,[Terms]
			  ,[Text1]
			  ,[Text2]
			  ,[Text3]
			  ,[Text4]
			  ,[Text5]
			  ,[Text6]
			  ,[Text7]
			  ,[Text8]
			  ,[Text9]
			  ,[DateFrom_DST]
			  ,[DateTo_DST]
			  ,[OfferID]
			  ,[StateID]
			  ,[StatusID]
			  ,[DateUpdated]
			  ,[DateCreated]
			  ,@stagingID
			  ,[UserID]
			  ,[EditingUserID]
			  ,[Text10]
			  ,[Text11]
			  ,[Text12]
			  ,[Text13]
			  ,[Text14]
			  ,[Text15]
			  ,[Text16]
			  ,[Text17]
			  ,[Text18]
		  FROM [Responsive].[dbo].[Content]
		  where ID = @cid
		

		delete from @c where id = @cid
		set @nextID += 1
	  end	
		
		
	delete from @t where id = @id
end
