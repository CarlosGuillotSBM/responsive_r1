SELECT tc.CookieGUID,tc.PlayerID, ta.ActionKey, tpn.Label, tc.ip, tu.URL,tpn.DateVisited, d.DeviceString, tca.affiliateID
FROM [Rep].[dbo].[TrackCookie] tc (nolock) 
JOIN [Rep].[dbo].[TrackPlayerNavigation] tpn (nolock) on tpn.CookieGUID = tc.CookieGUID
JOIN [Rep].[dbo].[TrackURLs] tu (nolock) on tu.ID = tpn.URLID
JOIN [Main].[dbo].[Device] d (nolock) on d.ID = tc.DeviceID
JOIN [Rep].[dbo].[TrackAction] ta (nolock) on ta.ActionID = tpn.ActionID
LEFT JOIN [Rep].[dbo].[TrackCookieAffiliate] tca (nolock) on tca.CookieGUID = tc.CookieGUID
order by tpn.DateVisited desc