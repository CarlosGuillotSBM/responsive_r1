  DECLARE @SkinID INT = 12, @sourceSkinID INT = 11
  
  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinCountry] WHERE skinID = @SkinID))
		BEGIN
			INSERT INTO [Main].[dbo].[SkinCountry]
			SELECT @SkinID
					,[CountryCode]
					,[Status]
					,[DisplayDescription]  
			FROM [Main].[dbo].[SkinCountry]
			where SkinID = @sourceSkinID
			
			SELECT 'Success: [SkinCountry] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinCountry] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
  
  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinCurrency] WHERE skinID = @SkinID))
		BEGIN  
			INSERT INTO [Main].[dbo].[SkinCurrency]
			SELECT @SkinID
					,[CurrencyCode]
					,[Status]
					,[DisplayDescription]
			FROM [Main].[dbo].[SkinCurrency]
			where SkinID = @sourceSkinID
			
			SELECT 'Success: [SkinCurrency] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinCurrency] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		
 -- IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinGame] WHERE skinID = @SkinID))
	--	BEGIN  
	--		INSERT INTO [Main].[dbo].[SkinGame]
	--		SELECT @SkinID
	--				,[GameID]
	--				,[WagerWithBonusFunds]
	--				,[AllWinsAreReal]
	--				,[FundedPlayersOnly]
	--				,[PlayerClassIDs]
	--				,[GameURL]
	--				,[SiteID]
	--				,[Status]
	--				,[ProgressiveJackpotGroupID]
	--				,[DoubleUpBaseWinLimit]
	--				,[DoubleUpNonBaseWinLimit]
	--				,[Restricted]
	--				,[defaultStake]
	--				,[defaultLines]
	--				,[Icon]
	--				,[Thumb]
	--				,[GameIcon]
	--				,[PayLinesText]
	--				,[Feature]
	--				,[Blurb]
	--				,[Volatility]
	--				,[WagerPercent]
	--				,[DemoPlay]
	--		FROM [Main].[dbo].[SkinGame]
	--		where SkinID = @sourceSkinID
  			
	--		SELECT 'Success: [SkinGame] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
	--	END
	--ELSE 
	--	SELECT 'There was already [SkinGame] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
	
		
  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinGameCategoryDescription] WHERE skinID = @SkinID))
		BEGIN  		
			INSERT INTO [Main].[dbo].[SkinGameCategoryDescription]
			SELECT	[GameCategoryDescriptionID]
					,@SkinID
					,[DeviceCategoryID]
					,[GameID]
					,[Position]
			FROM [Main].[dbo].[SkinGameCategoryDescription]
			where SkinID = @sourceSkinID
   			
			SELECT 'Success: [SkinGameCategoryDescription] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinGameCategoryDescription] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		 

  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinPlayerBalanceAdjustmentCategory] WHERE skinID = @SkinID))
		BEGIN  				 
			INSERT INTO [Main].[dbo].[SkinPlayerBalanceAdjustmentCategory]
			SELECT @SkinID
					,[PlayerBalanceAdjustmentCategoryID]
					,[DisplayDescription]
			FROM [Main].[dbo].[SkinPlayerBalanceAdjustmentCategory]
			WHERE SkinID = @sourceSkinID
    			
			SELECT 'Success: [SkinPlayerBalanceAdjustmentCategory] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinPlayerBalanceAdjustmentCategory] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 
  

  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinSecurityQuestion] WHERE skinID = @SkinID))
		BEGIN  		  
			INSERT INTO [Main].[dbo].[SkinSecurityQuestion]
			SELECT @SkinID 
					,[SecurityQuestionID]
					,[Status]
					,[DisplayDescription]
			FROM [Main].[dbo].[SkinSecurityQuestion]
			WHERE SkinID = @sourceSkinID
      			
			SELECT 'Success: [SkinSecurityQuestion] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinSecurityQuestion] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 
  
  

  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinStatus] WHERE skinID = @SkinID))
		BEGIN  		  
			INSERT INTO [Main].[dbo].[SkinStatus]
			SELECT @SkinID 
						,[TableName]
						,[Status]
						,[DisplayDescription]
						,[Description]
			FROM [Main].[dbo].[SkinStatus]  
			WHERE SkinID = @sourceSkinID
      			
			SELECT 'Success: [SkinStatus] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinStatus] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 

  

  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinTxType] WHERE skinID = @SkinID))
		BEGIN  		  
			INSERT INTO [Main].[dbo].[SkinTxType]
			SELECT @SkinID 
					,[TxTypeID]
					,[DisplayDescription]
			FROM [Main].[dbo].[SkinTxType]  
			WHERE SkinID = @sourceSkinID
      			
			SELECT 'Success: [SkinTxType] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinTxType] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 


  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinURL] WHERE skinID = @SkinID))
		BEGIN  		  
			INSERT INTO [Main].[dbo].[SkinURL]
			SELECT @SkinID
					,[SkinURL]
					,[Status]
			FROM [Main].[dbo].[SkinURL]   
			WHERE SkinID = @sourceSkinID
      			
			SELECT 'Success: [SkinURL] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinURL] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 


  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[SkinWebFoundUs] WHERE skinID = @SkinID))
		BEGIN  		   
			INSERT INTO [Main].[dbo].[SkinWebFoundUs]
			SELECT @SkinID
					,[WebFoundUsID]
					,[Status]
					,[DisplayDescription]
			FROM [Main].[dbo].[SkinWebFoundUs]  
			WHERE SkinID = @sourceSkinID
     			
			SELECT 'Success: [SkinWebFoundUs] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinWebFoundUs] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 
    
  IF (NOT EXISTS (SELECT TOP 1 * FROM [Rep].[dbo].[SkinStatus] WHERE skinID = @SkinID))
		BEGIN  		
				INSERT INTO [Rep].[dbo].[SkinStatus]
				SELECT @SkinID
						,[TableName]
						,[Status]
						,[DisplayDescription]
						,[Description]
				FROM [Rep].[dbo].[SkinStatus]
				WHERE SkinID = @sourceSkinID
  
  			SELECT 'Success: [SkinStatus] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinStatus] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 
		
  IF (NOT EXISTS (SELECT TOP 1 * FROM [Rep].[dbo].[SkinStatus] WHERE skinID = @SkinID))
		BEGIN  		
				INSERT INTO [Rep].[dbo].[SkinStatus]
				SELECT @SkinID
						,[TableName]
						,[Status]
						,[DisplayDescription]
						,[Description]
				FROM [Rep].[dbo].[SkinStatus]
				WHERE SkinID = @sourceSkinID
  
  			SELECT 'Success: [SkinStatus] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [SkinStatus] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 		

  IF (NOT EXISTS (SELECT TOP 1 * FROM [Main].[dbo].[PlayerClass] WHERE skinID = @SkinID))
		BEGIN  		
				INSERT INTO [Main].[dbo].[PlayerClass]
				SELECT  ClassID, DisplayClassID, SubClass, isDefault, @SkinID, CurrencyCode, Description, GamePointsTimeFrameBasis, GamePointsPerTimeFrameFrom, 
													GamePointsPerTimeFrameTo, DisplayColor, DepositAmount, WagerAmount, HoldAmount, UpgradeClassID, MinWagerAmount, MaxWagerAmount, MinNetCash, 
													MaxNetCash, MinDepositCount
				FROM         Main..PlayerClass
				WHERE     (SkinID = @sourceSkinID)
  
  			SELECT 'Success: [PlayerClass] records where copied  SkinID: ' + CONVERT(NVARCHAR(100),@SkinID)
		END
	ELSE 
		SELECT 'There was already [PlayerClass] related with this SkinID: ' + CONVERT(NVARCHAR(100),@SkinID) 