					
select DISTINCT 'EXEC Responsive..AdminSetContentLive2 '+ CAST( c.ContentCategoryID AS NVARCHAR(12) ) + ', ''' + substring(
        (
            Select ','+CAST(ST1.ID AS NVARCHAR(20)) AS [text()]
            FROM Responsive..Content (nolock) ST1 
						WHERE ST1.ContentCategoryID = cc.ID 
            ORDER BY ST1.ID
            For XML PATH ('')
        ), 2, 1000)   +''''       

FROM Responsive..ContentCategory (nolock) cc 
JOIN Responsive..Content (nolock) c on c.ContentCategoryID = cc.ID   
where cc.SkinID = 2 and Type = 1  -- 1 means staging so it's fine, no worries

