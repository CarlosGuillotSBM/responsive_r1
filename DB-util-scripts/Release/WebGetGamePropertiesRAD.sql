USE [Main]
GO
/****** Object:  StoredProcedure [dbo].[WebGetGamePropertiesRAD]    Script Date: 04/27/2016 08:36:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
Author:      HI
Date       : 18oct12
Description: returns the coinsizes, maxbet, maxdoubleup
       and if a game is only for funded players
Called From: Game.php
22Oct12 AF - returns various game config details
22Oct12 HI - get environment automatically
23Oct12 AF - get environment from function dbo.getEnviro
           - Added support for new field SkinGame.PlayerClassIDs (restricts player classes to games if required)
           - use isThis to determine server
25Oct12 AF - Hard coded MGS GameProviderGameID's until next release
17Dec12 RS - Added segment for NBP games if in DEV to modify GameURL
19dec12 HI - disable game if either game or skingame status = 0
02Jan13 AF - Returns Username
07Feb13 UB - Added 6056 - Max Damage and The Alien Attack
22Feb13 AF - Returns 'FirstLaunch' for sheriff games so we can play intro movie or not
28Feb13 UB - Added MGS HTML5 games
04Mar13 UB - updated GamProviderGameId length
12Mar13 UB - Added remaining MGS HTML5 games
12Mar13 HI - Moved MGS GameName into it's own column in game instead of being hard coded here
14Mar13 AF - Changed GameName to more applicably named GameProviderGameLaunchID which is now used from this sproc instead of GameProviderGameID
             MGS mobile have the GameProviderGameLaunchID as part of the #LanguageCode#
21Mar13 AF - Planned maintenance for MGS
03apr13 HI - return player gender and DOB for IGT games
10May13 HI - use new dev url's
15May13 HI - pass to mobile games URLs to get home etc.
21May13 AF - Addded GetCaller to @Data
04Jul13 AF - Added Bally maintenance
22Jul13 HI - bubbletime in dev point at dev
25jul13 HI - return width and height
14aug13 HI - add staging for bally
20aug13 HI - update dev game paths
30aug13 HI - always return homeurl so we can help players navigate back
06Oct13 AF - Write a PlaynGoGameSession record to hold our players current playngo session because PlaynGo have such a rubbish integration
             NOTE - we assume a player can only ever have one active playngo game going at one time
11oct13 RS - Changed game url for live html5 slots to be converted to the dev url
06dec13 HI - update staging url for bally
14mar14 RS - Updated dev url for scratchcards
02may14 HI - Add replace for IGT mobile games for their staging test
06may14 HI - Add replace for IGT cust02 desktop games for their staging test
07may14 HI - Make sure both fundedPlayersOnly in game and skingame are 0 to mean fundedPlayersOnly = 0
13jun14 RS - Added Daub-TheGamingLab gameIds to url to be changed to Dev and Coreix
01Jul14 RS - Added GameID TO PlayNGo query TO GET SESSION so players could have more than one game OPEN at a TIME.
03sep14 HI - return a guid for demo play to use
09Oct14 AF - Return GameTypeID and SkinCode and GameProviderGameID
14Oct14 AF - Write a PlaynGoGameSession record to hold our players current NetEnt session because NetEnt have such a rubbish integration
             NOTE - we assume a player can only ever have one active NetEnt game going at one time
16Oct14 AF - LoginSession holds CurrentGameID to aid sproc CogsGetOnlinePlayers
22Oct14 AF - Added specific NetEnt_merchantId and NetEnt_merchantPassword per skin
26nov14 HI - update gametypeid
01dec14 HI - fix to make it so we can play HTML5 games on desktop
07jan15 HI - don't screen out minigames
08jan15 HI - accept either the game name or the gameID
23Jan15 AF - NetEntGameSession in seperate table and one record per player REGARDLESS of game being launched - it will overwrite the player record
19feb15 HI - get skinID if missing
25mar15 HI - get the home url before checking for session 
30mar15 HI - check game has demo mode enabled if launched in demo mode
01jun15 HI - Add realistic launch replace for UAT
28jul15 HI - check game provider is active
21sep15 RS - DEV-3467 Remove check for real money only games as they're supposed to launch games now
08oct15 RS - DEV-3265 Handling sessions for Scientific Games
12oct15 RS - DEV-3265 Stop handling sessions for Scientific Games as it's not needed
09dec15 HI - add netent user / pwd for bubbly bingo
01feb16 RS - Added moduleId, clientId and productId for mgs-ainsworth games
10mar16 CM - Reset player reality check restrictions for the given game
07Apr16 RS - DEV-4670 Added ProviderID in final result set

[WebGetGamePropertiesRAD] 11712,'332C9909-9825-4F9F-AFCB-34FC6C0C6E2C','','jazz-cat-minigame',1,100,0,6

select top 1 * from Game (nolock) where DetailURL = 'jazz-cat-minigame-V2' and GameTypeID in (1,2) and GameTypeID >= @DeviceCategoryID order by GameTypeID

=============================================*/
ALTER PROCEDURE [dbo].[WebGetGamePropertiesRAD]

@PlayerID INT,
@SessionID UNIQUEIDENTIFIER,
@IP VARCHAR(15),
@Game VARCHAR(100), -- this can be either the gameID or the detailURL of the game. if detailURL passed then the correct game for the device is launched
@DeviceCategoryID TINYINT,
@GameLaunchSourceID TINYINT,
@FunMode TINYINT = 0,
@SkinID INT = 0

AS
BEGIN
SET NOCOUNT ON;
--#--
DECLARE @GameStatus TINYINT = NULL --0=Disabled, 1:OK, 2=Require funded and Not Funded, 3=Self Excluded, 4=Requires real funds and real bal=0, 5=Player class not allowed to play this game
DECLARE @GameProviderCode VARCHAR(10) = NULL
DECLARE @GameProviderGameLaunchID VARCHAR(100)
DECLARE @GameProviderGameID VARCHAR(100)
DECLARE @GameDescription VARCHAR(100)

DECLARE @CurrencyCode CHAR(3)
DECLARE @LanguageCode CHAR(2)
DECLARE @CountryCode CHAR(3)
DECLARE @CountryCode2 CHAR(2)
DECLARE @WagerWithBonusFunds BIT
DECLARE @RealBal MONEY
DECLARE @SelfExcluded BIT
DECLARE @GameURL VARCHAR(500)
DECLARE @FundedPlayersOnly BIT = NULL
DECLARE @Funded BIT
DECLARE @SiteID VARCHAR(10)
DECLARE @CoinSizeString VARCHAR(500)
DECLARE @MaxDoubleUp MONEY
DECLARE @MaxWin MONEY
DECLARE @PlayerClassID VARCHAR(10)
DECLARE @PlayerClassIDs VARCHAR(500) = ''
DECLARE @Username NVARCHAR(15)
DECLARE @FirstLaunch BIT=1
DECLARE @PlayerDOB VARCHAR(100)
DECLARE @PlayerGender CHAR(1)
DECLARE @LobbyURL VARCHAR(500)
DECLARE @CashierURL VARCHAR(500)
DECLARE @PromoURL VARCHAR(500)
DECLARE @BackgroundURL VARCHAR(500)
DECLARE @width INT
DECLARE @height INT
DECLARE @Result INT = 0

DECLARE @HomeURL VARCHAR(100)
DECLARE @GameTypeID TINYINT
DECLARE @SkinCode CHAR(3)
DECLARE @NetEnt_merchantId varchar(50) = ''
DECLARE @NetEnt_merchantPassword varchar(50) = ''
declare @GameID int
declare @GameHasFunMode tinyint
declare @playertype INT
DECLARE @ModuleID VARCHAR(10)
DECLARE @ClientID VARCHAR(10)
DECLARE @ProductID VARCHAR(10)
DECLARE @GameProviderID INT
DECLARE @GameMasterProviderID INT


if @Game NOT LIKE '%[^0-9]%'
begin
	-- the ID of the game was passed
	set @GameID = cast(@Game as int)	
end
ELSE
BEGIN

	IF @funMode=0 and @SkinID=0
	begin
		select @SkinID = SkinID from Player with (nolock) where ID = @PlayerID	
	end


	-- the name of the game was passed
	-- get the correct gameid for the gamename + device
	IF @GameLaunchSourceID = 100
	begin
		-- for bingo minigames choose 
		select top 1 @GameID = ID from Game (nolock) join skingame sg (nolock) on game.id = sg.gameid and skinid=@skinid where DetailURL = @Game and GameTypeID in (2,3) and game.status=1 and sg.status=1 order by GameTypeID		
	end
	ELSE
	BEGIN
		SELECT TOP 1 @GameID = ID FROM Game (NOLOCK) join skingame sg (nolock) on game.id = sg.gameid and skinid=@skinid  WHERE DetailURL = @Game AND GameTypeID >= @DeviceCategoryID  and game.status=1 and sg.status=1 ORDER BY GameTypeID		
	END	

END



DECLARE @Data VARCHAR(100) = HOST_NAME()+'.'+@@SERVERNAME+'.'+DB_NAME()+'.'+OBJECT_NAME(@@PROCID) + ' Launch GameID='+CAST(@GameID AS VARCHAR(10))



IF @funMode=1
  BEGIN
	  SET @Result = 0
	  SET @LanguageCode = 'en'
  END
ELSE
  BEGIN 
  
	SELECT @HomeURL = homeURL 
	  FROM skin s WITH (NOLOCK)
	  JOIN player p WITH (nolock) ON p.skinID = s.id
	  WHERE p.id = @playerID
  
  
  
	  --Check LoginSession
	  EXEC @Result = svrCheckPlayerLoginSession @PlayerID, @SessionID, @IP, @Data
	  IF @Result <> 0 --Session invalid (-10 or -11)
	    BEGIN
		    SELECT 'Result' = 104,'HomeURL' = @HomeURL --converted FROM -10 OR -11 TO 104=INVALID_PLAYER_SESSION
		    RETURN
	    END

	/*
	Player Properties
	- Get the coinsize, max win and maxdoubleup for the skin / game / playerclass
	*/
	SELECT
	  @SkinID = Player.SkinID,
	  @CurrencyCode   = Player.CurrencyCode,
	  @LanguageCode   = Player.LanguageCode,
	  @CountryCode    = Player.CountryCode,
	  @CountryCode2   = Country.Code2,
	  @SelfExcluded   = ISNULL(PlayerSelfExclude.Status,0),
	  @RealBal        = Player.Real,
	  @Funded         = CASE WHEN Player.PlayerStage > 49 THEN 1 ELSE 0 END,
	  @CoinSizeString = ISNULL(GameCoinSize.CoinSizeString,''),
	  @MaxDoubleUp    = ISNULL(GameCoinSize.MaxDoubleUp,0),
	  @MaxWin         = ISNULL(GameCoinSize.MaxWin,0),
	  @PlayerClassID  = ',' + CAST(Player.PlayerClassID AS VARCHAR(10)) + ',',
	  @Username       = Player.Username,
	  @PlayerDOB      = daga.fn.formatDate(player.dob,'yyyy-mm-dd',''),
	  @PlayerGender   = CASE WHEN player.gender = 1 THEN 'm' ELSE 'f' END,
	  @playertype	  = playertypeID
	FROM Player WITH (NOLOCK)
	JOIN Country WITH (NOLOCK) ON Country.Code = Player.CountryCode
	JOIN PlayerClass WITH (NOLOCK) ON PlayerClass.ID = Player.PlayerClassID
	LEFT JOIN PlayerSelfExclude WITH (NOLOCK) ON PlayerSelfExclude.PlayerID = @PlayerID AND PlayerSelfExclude.Status=1
	LEFT JOIN GameCoinSize WITH (NOLOCK) ON (GameCoinSize.PlayerClassID = Player.PlayerclassID AND
											 GameCoinSize.SkinID = Player.SkinID AND
											 GameCoinSize.GameID = @GameID)
	WHERE Player.ID = @PlayerID
END




/*
Game Properties
*/
SELECT
  @GameStatus          = CASE WHEN SkinGame.[Status] = 0 OR game.[Status] = 0 OR gameprovider.[Status] = 0 THEN 0 ELSE 1 END,
  @GameProviderCode    = GameProvider.Code,
  @GameProviderGameLaunchID  = Game.GameProviderGameLaunchID,
  @GameProviderGameID  = Game.GameProviderGameID,
  @GameDescription     = Game.[Description],
  @FundedPlayersOnly   = case when (SkinGame.FundedPlayersOnly = 0 and Game.FundedPlayersOnly = 0) then 0 else 1 end,
  @WagerWithBonusFunds = SkinGame.WagerWithBonusFunds,
  @GameURL             = SkinGame.GameURL,
  @SiteID              = SkinGame.SiteID,
  @PlayerClassIDs      = SkinGame.PlayerClassIDs,
  @HomeURL             = Skin.HomeURL,
  @LobbyURL            = Skin.LobbyURL,
  @CashierURL          = Skin.CashierURL,
  @PromoURL            = Skin.PromoURL,
  @BackgroundURL       = Skin.BackgroundURL,
  @width = width,
  @height = height,
  @GameTypeID      = Game.GameTypeID,
  @SkinCode = Skin.Code,
  @GameHasFunMode = skingame.DemoPlay,
  @ModuleID = Game.ModuleID,
  @ClientID = Game.ClientID,
  @ProductID = Game.ProductID, 
  @GameProviderID = GameProvider.ID,
  @GameMasterProviderID = GameProvider.MasterProviderID
FROM SkinGame WITH (NOLOCK)
JOIN Skin WITH (NOLOCK) ON Skin.ID = SkinGame.SkinID
JOIN Game WITH (NOLOCK) ON Game.ID = SkinGame.GameID
JOIN GameProvider WITH (NOLOCK) ON GameProvider.ID = Game.GameProviderID
WHERE SkinGame.SkinID = @SkinID AND SkinGame.GameID = @GameID

/*
Insert/Update PlaynGoGameSession record for PlaynGo
*/
IF @GameProviderCode = 'PlaynGo'
BEGIN
  UPDATE PlaynGoGameSession
    SET SessionID = @SessionID,
        GameID = @GameID,
        GameProviderGameID = @GameProviderGameID,
        GameLaunchSourceID = @GameLaunchSourceID
    WHERE PlayerID = @PlayerID AND GameID = @GameID

  IF @@ROWCOUNT = 0
    INSERT PlaynGoGameSession (PlayerID, SessionID, GameID, GameProviderGameID, GameLaunchSourceID)
                               VALUES
                              (@PlayerID, @SessionID, @GameID, @GameProviderGameID, @GameLaunchSourceID)
END
                              
IF @GameProviderCode = 'NetEnt'
BEGIN
  UPDATE NetEntGameSession
    SET SessionID = @SessionID,
        GameID = @GameID,
        GameProviderGameID = @GameProviderGameID,
        GameLaunchSourceID = @GameLaunchSourceID,
        [Date] = GETDATE()
    WHERE PlayerID = @PlayerID 

  IF @@ROWCOUNT = 0
    INSERT NetEntGameSession (PlayerID, SessionID, GameID, GameProviderGameID, GameLaunchSourceID, [Date])
                              VALUES
                             (@PlayerID, @SessionID, @GameID, @GameProviderGameID, @GameLaunchSourceID, GETDATE())


  SET @NetEnt_merchantId = CASE @SkinID
                                WHEN 1 THEN 'SW_merchant'
                                WHEN 2 THEN 'KB_merchant'
                                WHEN 3 THEN 'LP_merchant'
                                WHEN 4 THEN 'DS_merchant'
                                WHEN 5 THEN 'MV_merchant'
                                WHEN 6 THEN 'BX_merchant'
                                WHEN 7 THEN 'BB_merchant'
                           END
    
    
  IF daga.fn.isThis('Server', NULL) = 'LIVE'
    BEGIN
      SET @NetEnt_merchantPassword = CASE @SkinID
                                    WHEN 1 THEN 'chewrEZ2tRed'
                                    WHEN 2 THEN 'Wru3pUWrUSwa'
                                    WHEN 3 THEN 'PHaHatE75asP'
                                    WHEN 4 THEN '2RUceChuk2jA'
                                    WHEN 5 THEN 'Pre5w3tREchA'
                                    WHEN 6 THEN 'baStatuwu2ru'
                                    WHEN 7 THEN 'vestUfr5swan'
                               END
    END
  ELSE
    SET @NetEnt_merchantPassword = 'testing' + CAST(@SkinID AS VARCHAR(2))
END


-- hack for eyecon new environment testing
if @playertype = 10
begin
	if @GameID between 2051 and 2550 and @GameDescription like '%minigame%'
	begin
		set @GameURL = 'https://stg.eyeconalderney.gg/launch/daub/content-holder-daub.swf?initialServlet=https://stg.eyeconalderney.gg/maroon/servlet/MaroonAPI'
	end
	else
	begin
		set @GameURL = 'https://stg.eyeconalderney.gg/maroon/servlet/MaroonAPI'
	end	
end


/*
--Planned maintenance for MGS games
IF @GameProviderCode = 'MGS' AND
   (
   (GETDATE() BETWEEN '07 Nov 2013 11:00:00' AND '07 Nov 2013 12:00:00') 
   --OR (GETDATE() BETWEEN '25 Apr 2013 06:59:00' AND '25 Apr 2013 07:30:00')
   )
  SET @GameStatus=0
*/

/*
--Planned maintenance for Bally games
IF @GameProviderCode = 'Bally' AND
   (GETDATE() BETWEEN '25 Apr 2013 06:59:00' AND '25 Apr 2013 06:59:00') 
  SET @GameStatus=0
*/

--Planned maintenance for IGT games
IF @GameProviderCode = 'IGT' AND
   (GETDATE() BETWEEN '19 Apr 2016 00:59:00' AND '19 Apr 2016 04:01:00')
  SET @GameStatus=0  

/*
--Planned maintenance for Netent games
IF @GameProviderCode = 'NetEnt' AND
   (GETDATE() BETWEEN '15 Sep 2015 01:59:00' AND '15 Sep 2015 07:01:00') 
  SET @GameStatus=0
*/

IF daga.fn.isThis('Server',NULL) = 'DEV'
  BEGIN
    IF @GameID IN (1,999)
      SET @GameURL = 'http://dev.game.dagacube.net/simulate.php'

    IF @GameID BETWEEN 93 AND 149
      SET @GameURL = 'https://flash.dagacubedev.net/scratchcards/'

    IF (@GameID BETWEEN 300 AND 399) OR @GameID = 5000   
      SET @GameURL = REPLACE(@GameURL,'https://gameclient.dagacube.net/slots/','https://flash.dagacubedev.net/slots/')

    IF @GameID BETWEEN 500 AND 599
      SET @GameURL = REPLACE(@GameURL,'https://gameclient.dagacube.net/html5/slots/','https://html5.dagacubedev.net/slots/')

    IF @GameID BETWEEN 10000 AND 10099
      SET @GameURL = REPLACE(@GameURL,'https://gameclient.dagacube.net/slots/','https://flash.dagacubedev.net/slots/')
      
    IF @GameID BETWEEN 13000 AND 13099
      SET @GameURL = REPLACE(@GameURL,'https://gameclient.dagacube.net/slots/','https://flash.dagacubedev.net/slots/')
            
  END

IF daga.fn.isThis('Server',NULL) = 'COREIX'
  BEGIN
    IF @GameID IN (1,999)
      SET @GameURL = 'https://sec.aquagamesys.net/agsGame/simulate.php'

	  IF (@GameID BETWEEN 500 AND 599)
	    SET @GameURL = REPLACE(@GameURL,'https://gameclient.dagacube.net/html5/slots/','http://skin1.aquagamesys.net/html5/slots/')  
      
	  IF (@GameID BETWEEN 300 AND 399)
	    SET @GameURL = REPLACE(@GameURL,'https://gameclient.dagacube.net/slots/','http://skin1.aquagamesys.net/flash/slots/')  
       
      IF @GameID BETWEEN 13000 AND 13099
        SET @GameURL = REPLACE(@GameURL,'https://gameclient.dagacube.net/slots/','http://skin1.aquagamesys.net/flash/slots/')
      
	  IF (@GameID BETWEEN 9000 AND 9999)
	    SET @GameURL = REPLACE(@GameURL,'https://games-alderney.ballyinteractive.com/','https://games-alderney.ballyinteractivedev.com/')  

	  IF (@GameID BETWEEN 4000 AND 4499)
			SET @GameURL = REPLACE(@GameURL,'https://platform.rgsgames.com/game.do','https://rgs-cust02.lab.wagerworks.com/game.do')  


	  IF (@GameID BETWEEN 4500 AND 4599)
			SET @GameURL = REPLACE(@GameURL,'https://m.rgsgames.com/games/','https://ipl-dmzcust02.lab.wagerworks.com/games/')  

	  IF (@GameID BETWEEN 16000 AND 16000)
			SET @GameURL = REPLACE(@GameURL,'https://daub.realisticgames.co.uk/LaunchGame','https://daub.realistic-uat.com/LaunchGame')  


  END



IF @GameID BETWEEN 6500 AND 6999 --MGS mobile games
  SET @GameURL = REPLACE(@GameURL,'#LanguageCode#',@GameProviderGameLaunchID + '/' + @LanguageCode)
ELSE
  SET @GameURL = REPLACE(@GameURL,'#LanguageCode#',@LanguageCode)

IF @GameStatus = 1 AND (@FundedPlayersOnly=1 AND @Funded = 0)
   SET @GameStatus = 2

--Commented out for DEV-3467 as game is now supposed to be launched.
--IF @GameStatus = 1 AND (@WagerWithBonusFunds=0 AND @RealBal < 0.01)
--   SET @GameStatus = 4

IF @GameStatus = 1 AND @PlayerClassIDs <> '' AND (CHARINDEX(@PlayerClassID,',' + @PlayerClassIDs + ',') = 0)
   SET @GameStatus = 5

IF @SelfExcluded = 1
  SET @GameStatus = 3

if @GameHasFunMode = 0 and @FunMode = 1
   SET @GameStatus = 7	


--LoginSession
IF @GameStatus = 1
  UPDATE LoginSession SET CurrentGameID = @GameID WHERE PlayerID = @PlayerID AND Status = 1

	DELETE FROM Main..RealityCheckPlayerGame WHERE PlayerID = @PlayerID and GameID = @GameID

SELECT
  'Result'             = @Result,
  'GameID'			   = @GameID,	
  'GameStatus'         = ISNULL(@GameStatus,0),
  'GameProviderCode'   = ISNULL(@GameProviderCode,''),
  'GameProviderGameID' = @GameProviderGameID,
  'GameProviderGameLaunchID' = @GameProviderGameLaunchID,
  'GameDescription'    = @GameDescription,
  'SkinID'             = @SkinID,
  'CurrencyCode'       = @CurrencyCode,
  'LanguageCode'       = @LanguageCode,
  'CountryCode'        = @CountryCode,
  'CountryCode2'       = @CountryCode2,
  'GameURL'            = @GameURL,
  'SiteID'             = @SiteID,
  'CoinSizeString'     = @CoinSizeString,
  'MaxDoubleUp'        = @MaxDoubleUp,
  'MaxWin'             = @MaxWin,
  'Username'           = @Username,
  'FirstLaunch'        = @FirstLaunch,
  'PlayerDOB'          = @PlayerDOB,
  'PlayerGender'       = @PlayerGender,
  'HomeURL'            = @HomeURL,
  'LobbyURL'           = @LobbyURL,
  'CashierURL'         = @CashierURL,
  'PromoURL'           = @PromoURL,
  'BackgroundURL'      = @BackgroundURL,
  'Width'			         = @width,
  'Height'			       = @height,
  'GUID'			         = NEWID(),
  'GameTypeID'         = @GameTypeID,
  'SkinCode'          = @SkinCode,
  'NetEnt_merchantId' = @NetEnt_merchantId,
  'NetEnt_merchantPassword' = @NetEnt_merchantPassword,
  'ModuleID'			= @ModuleID,
  'ClientID'			= @ClientID,
  'ProductID'			= @ProductID, 
  'ProviderID'			= @GameProviderID,
  'MasterProviderID'	= @GameMasterProviderID

--#--
END
