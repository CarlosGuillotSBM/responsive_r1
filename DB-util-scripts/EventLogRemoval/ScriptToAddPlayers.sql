

DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)
DECLARE @WeekStart DATETIME2(7) = DATEADD(DAY, -7, CAST(@Now AS DATE))

DECLARE @PlayerID INT = 0, @PlayerClassID INT, @PrizeID INT, @PlayerClassPropertyInstanceID INT, @OldPlayerID INT = -1 

 
 
IF OBJECT_ID('tempdb..#PClassProperty') IS NOT NULL DROP TABLE #PClassProperty

 INSERT INTO Main.dbo.PlayerDailyPrizes ([PlayerID],[PlayerClassID],[NumPrizes],[PrizeID],
																					[PlayerClassPropertyID],[StartDate],[EndDate],[PlayerClassPropertyInstanceID])
 SELECT DISTINCT       
      pcpi.[PlayerID],
      pcpi.[PlayerClassID],
      pcpi.[Value],
      pcpi.[ExtraValue],
      pcpi.[PlayerClassPropertyID] - 1000,
      pcpi.[FromDate],
      pcpi.[ToDate],
      pcpi.[ID]
 FROM Main..Player (nolock) p 
 JOIN Rep..PlayerSummary (nolock) ps on p.ID = ps.PlayerID
 JOIN Main..PlayerClassPropertyInstance pcpi (nolock) on p.ID = pcpi.PlayerID
 WHERE p.LastLoginDate > DATEADD(DAY,-8,@Now) AND ((@now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL))
	AND pcpi.PlayerClassPropertyID IN (1010,1011)  AND ps.LastFundedDate BETWEEN @WeekStart AND @now  
 
 
	INSERT INTO Main.dbo.PlayerDailyPrizes ([PlayerID],[PlayerClassID],[NumPrizes],[PrizeID],
																					[PlayerClassPropertyID],[StartDate],[EndDate],[PlayerClassPropertyInstanceID])
 SELECT DISTINCT       
      pcpi.[PlayerID],
      pcpi.[PlayerClassID],
      pcpi.[Value],
      pcpi.[ExtraValue],
      pcpi.[PlayerClassPropertyID],
      pcpi.[FromDate],
      pcpi.[ToDate],
      pcpi.[ID]
 FROM Main..Player (nolock) p 
 JOIN Rep..PlayerSummary (nolock) ps on p.ID = ps.PlayerID
 JOIN Main..PlayerClassPropertyInstance pcpi (nolock) on p.playerClassID = pcpi.PlayerClassID
 WHERE  p.LastLoginDate > DATEADD(DAY,-8,@Now)	
	AND pcpi.PlayerClassPropertyID IN (10,11) 
	AND pcpi.ID NOT IN (SELECT DISTINCT PlayerClassPropertyInstanceID FROM Main.dbo.PlayerDailyPrizes) 
	AND ((@now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL)) 
	AND ps.LastFundedDate BETWEEN @WeekStart AND @now 


SET @PlayerID = -1
SET @PlayerClassID = NULL
SET @PrizeID = NULL
SET @PlayerClassPropertyInstanceID = NULL
SET @OldPlayerID = 0 

IF OBJECT_ID('tempdb..#Players') IS NOT NULL DROP TABLE #Players	

 SELECT DISTINCT p.ID as PlayerID, p.PlayerClassID as PlayerClassID, pcpi.ID AS ID
 INTO #Players
 FROM Main..Player (nolock) p 
 JOIN Main..PlayerClassPropertyInstance pcpi (nolock) on p.ID = pcpi.PlayerID
 WHERE pcpi.PlayerClassPropertyID IN (43,1043) AND p.LastLoginDate > DATEADD(DAY,-15,@Now)
 
 
 INSERT INTO #Players
 SELECT p.ID as PlayerID, p.PlayerClassID as PlayerClassID, pcpi.ID AS ID
 FROM Main..Player (nolock) p 
 JOIN Main..PlayerClassPropertyInstance pcpi (nolock) on p.playerClassID = pcpi.PlayerClassID
 WHERE pcpi.PlayerClassPropertyID IN (43,1043) AND p.ID NOT IN (SELECT PlayerID FROM #Players) AND p.LastLoginDate > DATEADD(DAY,-15,@Now)
 
 
		WHILE (@PlayerID > @OldPlayerID) 
		BEGIN  
 
				SELECT @PlayerClassID = PlayerClassID FROM Main..Player WITH (NOLOCK) WHERE ID = @PlayerID
				DECLARE @PropertyID INT=43, @DailyXDaySpins INT=0, @PrizeTypeID INT=0
				DECLARE @PlayerClassValues TABLE (Value INT, PromoID INT, PlayerClassPropertyID INT)
				DECLARE @PlayerKey INT = (SELECT PlayerKey FROM VM3VL40DB02.DWH.dbo.Dim_Player WITH(NOLOCK) WHERE PlayerID=@PlayerID)

				--MultipleValues=1 for propertyId=43
				--Get the instance records for player class
				INSERT INTO @PlayerClassValues
					SELECT pcpi.Value, pcpi.ExtraValue, pcpi.ID
						FROM Main..PlayerClassPropertyInstance pcpi WITH (NOLOCK)
								 WHERE (pcpi.PlayerClassID = @PlayerClassID AND PlayerClassPropertyID = @PropertyID)
											 AND ((@Now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL))

				--If there are instance records set for Player then merge the records with the records from Player class
				IF (EXISTS(SELECT TOP 1 1 FROM Main..PlayerClassPropertyInstance pcpi WITH (NOLOCK)
									WHERE (PlayerID = @PlayerID AND PlayerClassPropertyID = @PropertyID + 1000)
									AND ((@Now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL))))
				BEGIN
						SELECT
							'Value'=pcpi.Value,
							'PromoID'=pcpi.ExtraValue
						INTO #PlayerSource
						FROM Main..PlayerClassPropertyInstance pcpi WITH (NOLOCK)
									WHERE (PlayerID = @PlayerID AND PlayerClassPropertyID = @PropertyID + 1000)
									AND ((@Now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL))

						MERGE @PlayerClassValues AS TRGT
						USING #PlayerSource AS SRC
						ON
						(
							TRGT.Value = SRC.Value AND
							TRGT.PromoID = SRC.PromoID
						)
						WHEN MATCHED
						THEN UPDATE SET Value=SRC.Value
						WHEN NOT MATCHED THEN
						INSERT (Value, PromoID) VALUES (SRC.Value, SRC.PromoID);
						IF OBJECT_ID('tempdb..#PlayerSource') IS NOT NULL DROP TABLE #PlayerSource;
				END

				DECLARE @DailySpinNoOfDays INT
				DECLARE @DailySpinPromoID INT
				SELECT TOP 1 @DailySpinNoOfDays=Value, @DailySpinPromoID=PromoID FROM @PlayerClassValues
				DECLARE @LastTxPromoDate DATETIME2(7) = NULL
				--If there is already an event for today the player will not be checked for the daily spins in the subsequent logins on the same day
				IF (NOT EXISTS(SELECT TOP 1 1 FROM Main..EventLog WITH (NOLOCK) WHERE Main.$PARTITION.Fn_PartitionByDateTime(date) >= 6 AND EventLogTypeID = 119 AND EntityID = @PlayerID))
				BEGIN
				 WHILE @DailySpinPromoID IS NOT NULL
						BEGIN
							SET @DailySpinNoOfDays=0
							SET @DailySpinPromoID=NULL
							SET @LastTxPromoDate = NULL

							--Get the number of days the free spin has to be given for a specific promo
							SELECT TOP 1 @DailySpinNoOfDays=Value, @DailySpinPromoID=PromoID,  @PlayerClassPropertyInstanceID = PlayerClassPropertyID FROM @PlayerClassValues 
								SELECT @DailySpinNoOfDays
								--If there is a transaction for the above specific promo in the last X days give spins.
								--The check is made both on Main and DWH trans tables to make sure we are not missing any records in the process
								IF ( EXISTS(SELECT TxPromo.Date FROM Main..TxPromo WITH (NOLOCK)
														 WHERE PromoID = @DailySpinPromoID AND TxPromo.PlayerID = @PlayerID
																	 AND TxPromo.Date > (DATEADD(DAY,-ISNULL(@DailySpinNoOfDays,0),@today))
																	 )
									 )
									 
									 BEGIN
									 
				SELECT 'Tino1'
										SELECT @LastTxPromoDate =  TxPromo.Date FROM Main..TxPromo WITH (NOLOCK)
														 WHERE PromoID = @DailySpinPromoID AND TxPromo.PlayerID = @PlayerID
																	 AND TxPromo.Date > (DATEADD(DAY,-ISNULL(@DailySpinNoOfDays,0),@today))
																	 
									 END
								 
									IF(
									 EXISTS( (SELECT t.DateKey FROM VM3VL40DB02.DWH.dbo.Fact_PromotionTransaction t WITH(NOLOCK)
										 JOIN VM3VL40DB02.DWH.dbo.Dim_Promotion p WITH(NOLOCK) ON t.PromoKey=p.PromoKey
										 WHERE t.PlayerKey = @PlayerKey AND p.PromoID = @DailySpinPromoID
													 AND t.DateKey>= CONVERT(VARCHAR(8),(DATEADD(DAY,-ISNULL(@DailySpinNoOfDays,0),@today)),112)
													 )
										 )
									)
									BEGIN
									
				SELECT 'Tino2'
										SELECT @LastTxPromoDate = CONVERT(DATETIME2(7),CONVERT(VARCHAR(8),t.DateKey,112)) FROM VM3VL40DB02.DWH.dbo.Fact_PromotionTransaction t WITH(NOLOCK)
										 JOIN VM3VL40DB02.DWH.dbo.Dim_Promotion p WITH(NOLOCK) ON t.PromoKey=p.PromoKey
										 WHERE t.PlayerKey = @PlayerKey AND p.PromoID = @DailySpinPromoID
													 AND t.DateKey>= CONVERT(VARCHAR(8),(DATEADD(DAY,-ISNULL(@DailySpinNoOfDays,0),@today)),112)
													 
									END
									
									IF (@LastTxPromoDate IS NOT NULL)
									BEGIN
										-- give them spins and write a log
										SELECT @prizeID=PrizeID, @DailyXDaySpins=PrizeAmount, @PrizeTypeID=pr.PrizeTypeID FROM Main..Promo p WITH (NOLOCK)
										JOIN Main..Prize pr WITH (NOLOCK) ON p.PrizeID = pr.ID
										WHERE p.ID = @DailySpinPromoID AND pr.PrizeTypeID IN (15, 21)
																	
																	
										-- here is where we add rows to the PlayerDailyPrizes
										INSERT INTO Main..PlayerDailyPrizes ([PlayerID],[NumPrizes],[PrizeID],[PlayerClassPropertyID],[StartDate],[EndDate],[PlayerClassPropertyInstanceID])							
										VALUES (@PlayerID, @DailyXDaySpins,@PrizeID,43,DATEADD(DAY,1,@LastTxPromoDate),DATEADD(DAY,@DailySpinNoOfDays,@LastTxPromoDate),@PlayerClassPropertyInstanceID)
									END					
															
									DELETE @PlayerClassValues WHERE Value = @DailySpinNoOfDays AND PromoID=@DailySpinPromoID
								END
		          

						END
					
			SET @OldPlayerID = @PlayerID
			-- Get next customerId
			SELECT  @PlayerID = p.PlayerID, @PlayerClassID = p.PlayerClassID
			FROM  #Players p
			WHERE p.PlayerID > @PlayerID 
			ORDER BY p.PlayerID desc
			
				END
				
		