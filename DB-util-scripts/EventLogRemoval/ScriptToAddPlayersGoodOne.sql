select * 
from Main..PlayerClassPropertyInstance pcpi (nolock) 
where ID 
IN(
110764,
110765,
110766,
110767
)

select * from Main..PlayerClass pc (nolock) where pc.ID IN (9,11)

select *
from Main..Promo p (nolock) 
where ID IN (
4062,
8487,
4062,
8487
)

select *
from Main..Prize pr (nolock)
where ID IN (
351,
349
)

DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)
SELECT TxPromo.Date, txPromo.PromoID FROM Main..TxPromo WITH (NOLOCK)
														 WHERE 
--														 PromoID IN (4062,
--8487) 
--AND 
TxPromo.PlayerID = 1173226
AND TxPromo.Date > (DATEADD(DAY,-15,@today))

select *
from Main..Promo p (nolock) 
where ID IN (1828,
10871
)


select * from Main.._Rep_prizePlayer (nolock) where PlayerID = 1206329

select * 
from Main..PlayerClassPropertyInstance pcpi (nolock) 
where ExtraValue 
IN(
10871
) and PlayerClassPropertyID = 1043

-- TRUNCATE TABLE Main..PlayerDailyPrizes

select * from Main..PlayerDailyPrizes


select * 
from Main..PlayerClassPropertyInstance pcpi (nolock) 
where PlayerID = 1206492  

select * from Main..PlayerDailyPrizes where PlayerClassPropertyInstanceID IN 
(
110766,
110767
)



select * 
from Main..PlayerClassPropertyInstance pcpi (nolock) 
where ID IN (
110766,
110767
)


				
DECLARE @Promotions TABLE (PromoID INT, PlayerClassID INT ) 				
INSERT INTO @Promotions 
SELECT DISTINCT ExtraValue
FROM Main..PlayerClassPropertyInstance pcpi (nolock) 
where pcpi.PlayerClassPropertyID = 43

DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)
SELECT COUNT( *) 
			FROM VM3VL40DB02.DWH.dbo.Fact_PromotionTransaction t WITH(NOLOCK)
				JOIN @Promotions p on p.PromoID = t.PromoKey
			WHERE t.DateKey>= CONVERT(VARCHAR(8),(DATEADD(DAY,-ISNULL(15,0),@today)),112) 
													 		

SELECT *
				FROM Main..TxPromo t WITH (NOLOCK) 
				JOIN @Promotions p on p.PromoID = t.PromoID
				
				 WHERE 
							 t.Date > (DATEADD(DAY,-ISNULL(15,0),@today))		
							 




select * from 


seLECT * from Main..PlayerDailyPrizes order by id desc

select * from Main..PlayerClassPropertyInstance where PlayerClassPropertyID = 1043

--TRUNCATE TABLE Main..PlayerDailyPrizes

-- RUN THIS TO ADD Player Daily Prizes 43 								 
							 
DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)

INSERT INTO Main..PlayerDailyPrizes (
[PlayerID]      
      ,[NumPrizes]
      ,[PrizeID]
      ,[PlayerClassPropertyID]
      ,[StartDate]
      ,[EndDate]
      ,[PlayerClassPropertyInstanceID]) 
           
SELECT t.PlayerKey as PlayerID, pr.PrizeAmount as NumPrizes, pr.PrizeID as PrizeID, 43 as PlayerClassPropertyID , 
DATEADD(DAY,1,CONVERT(DATETIME2(7),SUBSTRING(CONVERT(VARCHAR(8),t.DAteKey), 1, 4) + '-' + 
SUBSTRING(CONVERT(VARCHAR(8),t.DAteKey), 5, 2) + '-' + SUBSTRING(CONVERT(VARCHAR(8),t.DAteKey), 7, 2))) as StartDate, 

DATEADD(MICROSECOND,-1,DATEADD(DAY,p.Value+2,CONVERT(DATETIME2(7),SUBSTRING(CONVERT(VARCHAR(8),t.DAteKey), 1, 4) + '-' + 
SUBSTRING(CONVERT(VARCHAR(8),t.DAteKey), 5, 2) + '-' + SUBSTRING(CONVERT(VARCHAR(8),t.DAteKey), 7, 2)))) as EndDate , p.ID as PlayerClassPropertyInstance
			FROM VM3VL40DB02.DWH.dbo.Fact_PromotionTransaction t WITH(NOLOCK)				
			JOIN Main..Player pl (nolock) on pl.ID = t.PlayerKey
			JOIN	Main..PlayerClassPropertyInstance p (nolock) on p.ExtraValue = t.PromoKey AND pl.PlayerClassID = p.PlayerClassID	AND p.PlayerClassPropertyID = 43				
			JOIN Main..Promo pr (nolock) on pr.ID = p.ExtraValue 
			WHERE t.DateKey>= CONVERT(VARCHAR(8),(DATEADD(DAY,-ISNULL(15,0),@today)),112) 
--							 AND Pl.ID not IN (
--select el.EntityID from Main..EventLog el (nolock) where el.EventLogTypeID = 119 and Date > CONVERT(DATE,GETDATE()))
													 		
UNION
SELECT t.PlayerID, pr.PrizeAmount as NumPrizes, pr.PrizeID as PrizeID, 43 as PlayerClassPropertyID ,  
DATEADD(DAY,1,CONVERT(date,t.[Date])) as StartDate, 
DATEADD(MICROSECOND,-1,CONVERT(DATETIME2(7), DATEADD(DAY,p.Value+2,CONVERT(date,t.[Date])))) as EndDate, p.ID as PlayerClassPropertyInstance
				FROM Main..TxPromo t WITH (NOLOCK) 
				JOIN Main..Player pl (nolock) on pl.ID = t.PlayerID
			JOIN	Main..PlayerClassPropertyInstance p (nolock) on p.ExtraValue = t.PromoID AND pl.PlayerClassID = p.PlayerClassID	AND p.PlayerClassPropertyID = 43
			JOIN Main..Promo pr (nolock) on pr.ID = p.ExtraValue 
				
				 WHERE 
							 t.Date > (DATEADD(DAY,-ISNULL(15,0),@today))		
--							 AND Pl.ID not IN (
--select el.EntityID from Main..EventLog el (nolock) where el.EventLogTypeID = 119 and Date > CONVERT(DATE,GETDATE()))



		
		
-- RUN THIS TO ADD GIVEN PRIZEPLAYER 	43 	you need to run first the previous proc  and use PlayerDailyPrizes instead of PlayerClassPropertyInstance and all that
							 
							 
DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)

UPDATE Main..PrizePlayer 
SET PrizePlayer.PlayerDailyPrizeID = p.ID
FROM
Main..PlayerDailyPrizes (nolock) p
JOIN Main..PrizePlayer pp on pp.PlayerID = p.PlayerID and p.PrizeID = pp.PrizeID
where DATEDIFF(DAY,pp.AwardDate,@Now) = 0 and pp.Details like ('%days from %') and p.PlayerClassPropertyID = 43 AND	 p.playerID IN (
select el.EntityID from Main..EventLog el (nolock) where el.EventLogTypeID = 119 and Date > CONVERT(DATE,GETDATE()))




-- RUN THIS TO ADD GIVEN PRIZEPLAYER 	10 You need first to create PlayerDailyPrizes for the 10	
							 
							 
DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)

UPDATE Main..PrizePlayer
 SET PrizePlayer.PlayerDailyPrizeID = pdp.id
				FROM Rep..PlayerSummary ps WITH (NOLOCK) 
				JOIN Main..Player pl (nolock) on pl.ID = ps.PlayerID
				JOIN	Main..PlayerClassPropertyInstance p (nolock) on  pl.PlayerClassID = p.PlayerClassID	AND p.PlayerClassPropertyID = 10
				JOIN Main..PlayerDailyPrizes (nolock) pdp on pdp.PlayerClassPropertyInstanceID = p.id
				JOIN Main..PrizePlayer pp on pp.PlayerID = pl.ID and p.ExtraValue = pp.PrizeID
				 WHERE 
							 ps.LastFundedDate > (DATEADD(DAY,-ISNULL(7,0),@today))		
							 AND Pl.ID IN (
select el.EntityID from Main..EventLog el (nolock) where el.EventLogTypeID = 115 and Date > CONVERT(DATE,GETDATE()))
	
AND DATEDIFF(DAY,pp.AwardDate,@Now) = 0 --and pp.Details not like ('%days from %')	 
--order by aux.PlayerID, aux.PrizeID desc					



-- RUN THIS TO ADD GIVEN PRIZEPLAYER 	11 You need first to create PlayerDailyPrizes for the 10	
							 
							 
DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)

UPDATE Main..PrizePlayer
 SET PrizePlayer.PlayerDailyPrizeID = pdp.id
				FROM Rep..PlayerSummary ps WITH (NOLOCK) 
				JOIN Main..Player pl (nolock) on pl.ID = ps.PlayerID
				JOIN	Main..PlayerClassPropertyInstance p (nolock) on  pl.PlayerClassID = p.PlayerClassID	AND p.PlayerClassPropertyID = 11
				JOIN Main..PlayerDailyPrizes (nolock) pdp on pdp.PlayerClassPropertyInstanceID = p.id
				JOIN Main..PrizePlayer pp on pp.PlayerID = pl.ID and p.ExtraValue = pp.PrizeID
				 WHERE 
							 ps.LastFundedDate > (DATEADD(DAY,-ISNULL(7,0),@today))		
							 AND Pl.ID IN (
select el.EntityID from Main..EventLog el (nolock) where el.EventLogTypeID = 116 and Date > CONVERT(DATE,GETDATE()))
	
AND DATEDIFF(DAY,pp.AwardDate,@Now) = 0 --and pp.Details not like ('%days from %')	 						 									 