USE [Main]
GO
/****** Object:  StoredProcedure [dbo].[CogsPlayerClassPropertyInstanceModify]    Script Date: 12/15/2016 11:39:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================  
Author:    SR  
Date       : 19Apr13  
Mod date(s):  
Description: updates the player class property Instance  
Called From: cogs  
Comments:  
  
HI - 23may14 - removed debugging  
PS - 09Jan15 - Adding New field @ValueInt to Check For Free Spins
CM - 15Dec16 - DEV-7800 Adding values in PlayerDailyPrizes when required (we may need to add more types in the future)
=============================================*/  
  
ALTER PROCEDURE [dbo].[CogsPlayerClassPropertyInstanceModify]  
@CogsUserID UNIQUEIDENTIFIER,  
@SessionID UNIQUEIDENTIFIER,  
@IP VARCHAR(15),  
@SkinIDs VARCHAR(1000),  
@ID INT,  
@PlayerClassID INT=NULL,  
@PlayerID INT=NULL,  
@PlayerClassPropertyID INT,  
@Value INT, 
@ValueInt INT,  
@ExtraValue INT=NULL,  
@FromDate DATETIME2(7) =NULL,  
@ToDate DATETIME=NULL,  
@LockStatus TINYINT  
  
AS  
BEGIN  
SET NOCOUNT ON;  
--#--  
  
SET @ToDate=@ToDate+'23:59:59'  
  
--Session/Skin validation------------------------------  
DECLARE @SkinTable udt.SkinTable --(Result int, SkinID int)  
DECLARE @Caller VARCHAR(500) = HOST_NAME()+'.'+@@SERVERNAME+'.'+DB_NAME()+'.'+OBJECT_NAME(@@PROCID)  
DECLARE @Result INT  
  
INSERT @SkinTable  
  EXECUTE Cogs.dbo.svrValidateCogsRequest  
          @CogsUserID,@SessionID,@SkinIDs,@IP,@Caller  
  
SET @Result = (SELECT TOP 1 Result FROM @SkinTable)  
IF @Result != 0 --A validation error occurred. Cogs will have logged it  
  BEGIN  
    SELECT 'Result' = @Result  
    RETURN @Result  
  END  
------------------------------Session/Skin validation--  
  
DECLARE @MergeOutput TABLE  
(  
  MergeAction NVARCHAR(50),  
  NewInstanceID INT  
)  
  
MERGE dbo.PlayerClassPropertyInstance AS pcpi  
USING ( SELECT  @ID,  
        @PlayerClassPropertyID,  
        @PlayerID,  
        @PlayerClassID,  
        @Value,  
        @ValueInt,
        @ExtraValue,  
        @FromDate,  
        @ToDate,  
        @LockStatus  
        )  
 AS source (  [ID]  
           ,[PlayerClassPropertyID]  
           ,[PlayerID]  
           ,[PlayerClassID]  
           ,[Value] 
           ,[ValueInt] 
           ,[ExtraValue]  
           ,[FromDate]  
           ,[ToDate]  
           ,[LockStatus])  
ON pcpi.ID = source.ID  
WHEN MATCHED THEN  
UPDATE  SET PlayerClassPropertyID=@PlayerClassPropertyID,  
    Value=@Value,  
    ValueInt = @ValueInt,
    ExtraValue=@ExtraValue,  
    PlayerID=@PlayerID,  
    FromDate=@FromDate,  
    ToDate=@ToDate,  
    LockStatus=@LockStatus      
WHEN NOT MATCHED THEN  
    INSERT ([PlayerClassPropertyID]  
           ,[PlayerID]  
           ,[PlayerClassID]  
           ,[Value] 
           ,[ValueInt] 
           ,[ExtraValue]  
           ,[FromDate]  
           ,[ToDate]  
           ,[LockStatus])  
    VALUES (source.[PlayerClassPropertyID]  
           ,source.[PlayerID]  
           ,source.[PlayerClassID]  
           ,source.[Value]  
           ,source.[ValueInt]
           ,source.[ExtraValue]  
           ,source.[FromDate]  
           ,source.[ToDate]  
           ,source.[LockStatus])  
  
OUTPUT $action, INSERTED.ID INTO @MergeOutput;  
  
  
    
  
DECLARE @Action NVARCHAR(50)  
DECLARE @NewID INT  
SELECT  @Action = MergeAction,  
    @NewID = NewInstanceID  
FROM  @MergeOutput  
  
DECLARE @Details VARCHAR(1000)  
IF @Action = 'INSERT'  
	BEGIN
		SET @Details = 'Added Player Class Property Instance: ID - ' + CAST(@NewID AS VARCHAR)  		
		-- DEV-7800 We'll need to tell whether we need to add a row in PlayerDailyPrizes, just in case we add daily spins PlayerClassPropertyID 10 or 1010
		IF (@PlayerClassPropertyID IN (10,11)) -- Class Prize
		BEGIN
			INSERT INTO Main.dbo.PlayerDailyPrizes (PlayerClassID,NumPrizes,PrizeID,PlayerClassPropertyID,PlayerClassPropertyInstanceID, StartDate, EndDate)
			VALUES (@PlayerClassID, @Value, @ExtraValue, 10, @NewID, @FromDate, @ToDate)
		END
		ELSE IF (@PlayerClassPropertyID IN (1010,1011)) -- Player Prize
		BEGIN		
			INSERT INTO Main.dbo.PlayerDailyPrizes (PlayerID,NumPrizes,PrizeID,PlayerClassPropertyID,PlayerClassPropertyInstanceID, StartDate, EndDate)
			VALUES (@PlayerID, @Value, @ExtraValue, @PlayerClassPropertyID-1000, @NewID, @FromDate, @ToDate)
		END
  END
ELSE  
	BEGIN
		SET @Details = 'Updated Player Class Property Instance: ID - ' + CAST(@NewID AS VARCHAR)
		-- DEV-7800 We'll need to tell whether we need to add a row in PlayerDailyPrizes, just in case we add daily spins PlayerClassPropertyID 10 or 1010
		IF (@PlayerClassPropertyID IN (10,11)) -- Class Prize
		BEGIN
			UPDATE Main.dbo.PlayerDailyPrizes
			SET PlayerClassID = @PlayerClassID, NumPrizes = @Value, PrizeID = @ExtraValue, 
					PlayerClassPropertyID = @PlayerClassPropertyID, StartDate = @FromDate, EndDate = @ToDate
			WHERE PlayerClassPropertyInstanceID = @NewID
		END
		ELSE IF (@PlayerClassPropertyID IN (1010,1011)) -- Player Prize
		BEGIN		
			UPDATE Main.dbo.PlayerDailyPrizes
			SET PlayerID = @PlayerID, NumPrizes = @Value, PrizeID = @ExtraValue, 
					PlayerClassPropertyID = @PlayerClassPropertyID-1000, StartDate = @FromDate, EndDate = @ToDate
			WHERE PlayerClassPropertyInstanceID = @NewID
		END		  
  END
--select @Action, @Details  
  
--Write Event--  
IF @@ROWCOUNT = 0  
  BEGIN  
    IF (@Action = 'INSERT')  
      SELECT 'Result' = -99  
    ELSE  
      SELECT 'Result' = -98  
  END  
ELSE  
  BEGIN  
    EXEC Cogs.dbo.svrEventLog 451, @CogsUserID, NULL, @NewID, @IP, @Details  
    SELECT 'Result' = 0, 'Message' = 'Success', 'ID' = @NewID  
  END  
END  