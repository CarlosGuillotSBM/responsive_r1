USE [Main]
GO
/****** Object:  StoredProcedure [dbo].[svrTxDepositExtraProcessing]    Script Date: 12/16/2016 19:10:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
Author: AF
Date  : 01Mar15
Description: Performs the extra calls to various other sprocs resulting from a successful deposit
             The processing was moved from the other sprocs as they were causing Cashier..CoinDeposit problems with timeouts
             This is based on the entries in the table Cashier..TxDepositExtraProcessing where Processed = 0
             Called from job Main_svrTxDepositExtraProcessing every minute
             *** Check the ProcessTime of the rows. Adapt the job runtime to be as frequent as possible based on thr ProcessTime
Modifications:
*** NOTE - SET UP SWITCHING OF Processed rows ***

HI - 21apr15 - make sure the free spins and cards are given with the correct prizeID
MB - 25May16 - DEV-5644: call to svrCompRegisterAction instead of svrCompetitionTrigger
ZP - 04Jul16 - DEV-5952: Added Player Funded restriction to be taken in account.
ZP - 29Jul16 - DEV-5730 Changed the competition action register so it takes now amount real and amount bonus (instead of just amount)
AF - 05Dec16 - Added WHERE $PARTITION.Fn_PartitionByDateTime(date) = 6 (only todays events) for eventlog searches 
CM - 16Dec16 - DEV-7800 Event Log removal
=============================================*/
ALTER PROCEDURE [dbo].[svrTxDepositExtraProcessing]

AS
BEGIN
SET NOCOUNT ON;

DECLARE @ID INT = 1,
        @TxDepositID INT,
        @PlayerID INT,
        @SessionID UNIQUEIDENTIFIER,
        @Amount MONEY

DECLARE @SkinID INT
DECLARE @PlayerClassID INT
DECLARE @PlayerFunded BIT
DECLARE @DepositMsg VARCHAR(1000)
DECLARE @Zone VARCHAR(100)
DECLARE @Now DATETIME2 = GETDATE()
DECLARE @WeekStart DATETIME2 = DATEADD(DAY, -7, @Now)
DECLARE @msg VARCHAR(500)
DECLARE @FreeCards INT
DECLARE @FreeSpins INT
DECLARE @PrizeID INT

DECLARE @TimeStart DATETIME2

DECLARE @T TABLE (ID INT IDENTITY(1,1), TxDepositID INT, PlayerID INT, SessionID UNIQUEIDENTIFIER, Amount MONEY)
INSERT @T SELECT TOP 10 TxDepositID, PlayerID, SessionID, Amount FROM Cashier..TxDepositExtraProcessing WHERE Processed = 0 ORDER BY TxDepositID

WHILE @ID IS NOT NULL
BEGIN
  SET @TimeStart = GETDATE()
  SET @TxDepositID = NULL
  SELECT @TxDepositID = TxDepositID, @PlayerID = PlayerID, @SessionID = SessionID, @Amount = Amount FROM @T WHERE ID = @ID
  IF @TxDepositID IS NULL 
    BREAK

  --send message to the bingo middleware about the deposit
  SELECT  @Zone = z.Name, @SkinID = s.ID, @PlayerClassID = p.PlayerClassID,
			@PlayerFunded = CASE WHEN p.PlayerStage >= 50 THEN 1 ELSE 0 END
  FROM    Bingo.dbo.Zone z WITH ( NOLOCK )
          JOIN Skin s WITH ( NOLOCK ) ON s.BingoZoneID = z.ID
          JOIN Player p WITH ( NOLOCK ) ON p.SkinID = s.ID AND p.ID = @PlayerID

  SET @DepositMsg = 'PLAYERDEPOSIT,' + CAST(@PlayerID AS VARCHAR(20)) + ',' + CAST(@Amount AS VARCHAR(20))
  EXEC Bingo.dbo.sfsMessageSend @Zone, @DepositMsg

  -- hack so we could do timed banners without updating middleware
  SET @DepositMsg = 'PLAYERINFO,' + CAST(@PlayerID AS VARCHAR(20)) + ',FUNDED'
  EXEC Bingo.dbo.sfsMessageSend @Zone, @DepositMsg


	-- DEV-7800 
		-- We check whether we have any ungiven daily prizes for the player	
		
    DECLARE @PrizeTypeIDForDailySpins INT=0
    DECLARE @LastFundDate DATETIME2(0) = '2000-01-01'
    DECLARE @FirstFundDate DATETIME2(0) = '2000-01-01'

    SELECT  @LastFundDate = LastFundedDate
      FROM  Rep..PlayerSummary WITH (NOLOCK)
      WHERE PlayerID = @PlayerID		
		
		DECLARE @PrizesToBeGiven TABLE ([ID] INT,[PlayerID] INT ,[PlayerClassID] INT ,[NumPrizes] INT ,[PrizeID] INT ,[PlayerClassPropertyID] INT ,[StartDate] DATETIME2(7),[EndDate] DATETIME2(7), [PlayerClassPropertyInstanceID] INT)
		
		INSERT INTO @PrizesToBeGiven 
		SELECT [ID]
      ,[PlayerID]
      ,[PlayerClassID]
      ,[NumPrizes]
      ,[PrizeID]
      ,[PlayerClassPropertyID]
      ,[StartDate]
      ,[EndDate]
      ,[PlayerClassPropertyInstanceID]
		FROM Main.dbo.PlayerDailyPrizes pdp (NOLOCK)
		WHERE pdp.PlayerID = @PlayerID 
		AND ((@Now BETWEEN pdp.StartDate AND pdp.EndDate) OR (pdp.StartDate IS NULL AND pdp.EndDate IS NULL))
		AND NOT EXISTS(select top 1 * FROM Main.dbo.PrizePlayer pp (NOLOCK) WHERE pdp.ID = pp.PlayerDailyPrizeID AND pdp.PlayerID = @PlayerID AND DATEDIFF( DAY, pp.AwardDate, @Now) = 0 )
		AND pdp.PlayerClassPropertyID IN (10,11)
		
		INSERT INTO @PrizesToBeGiven
		SELECT [ID]
      ,[PlayerID]
      ,[PlayerClassID]
      ,[NumPrizes]
      ,[PrizeID]
      ,[PlayerClassPropertyID]
      ,[StartDate]
      ,[EndDate]
      ,[PlayerClassPropertyInstanceID]
		FROM Main.dbo.PlayerDailyPrizes pdp (NOLOCK)
		WHERE pdp.PlayerClassID = @PlayerClassID 
		AND ((@Now BETWEEN pdp.StartDate AND pdp.EndDate) OR (pdp.StartDate IS NULL AND pdp.EndDate IS NULL))
		AND NOT EXISTS(SELECT top 1 * FROM @PrizesToBeGiven pg WHERE pg.PlayerClassPropertyInstanceID = pdp.PlayerClassPropertyInstanceID)
		AND NOT EXISTS(select top 1 * FROM Main.dbo.PrizePlayer pp (NOLOCK) WHERE pdp.ID = pp.PlayerDailyPrizeID AND pdp.PlayerID = @PlayerID AND DATEDIFF( DAY, pp.AwardDate, @Now) = 0 )
		AND pdp.PlayerClassPropertyID IN (10,11)
		
		-- here we will iterate through #PrizesToBeGiven and will give execute the proc [svrGivePrizeByID] to give each of the prizes.
		-- this proc will add it to the PrizePlayer table. So it also needs which Id from the PlayerDailyPrize table was the one giving the prize
		
		-- Declare 
		DECLARE @GivenPlayerDailyPrizeID INT = 0, @GivenPrizeID INT, @GivenAmount INT, @GivenPlayerClassPropertyInstanceID INT, @OldID INT = -1 
		
		IF (EXISTS( SELECT TOP 1 * FROM @PrizesToBeGiven) )
		BEGIN
			SELECT  TOP 1 @GivenPlayerDailyPrizeID = ptb.ID, @GivenAmount =  ptb.NumPrizes, 
							@GivenPrizeID = ptb.PrizeID, @GivenPlayerClassPropertyInstanceID = ptb.ID
			FROM @PrizesToBeGiven ptb
			WHERE ID > @GivenPlayerDailyPrizeID 
			ORDER BY ID asc		
		
		-- Iterate over all the prizes for the player
		WHILE (@GivenPlayerDailyPrizeID > @OldID) 
		BEGIN  


			-- call your give prizes
			EXEC Main.dbo.svrGivePrizebyID @PlayerID, @GivenAmount, @GivenPrizeID, @GivenPlayerClassPropertyInstanceID

      SELECT @PrizeTypeIDForDailySpins=Prize.PrizeTypeID FROM Main..Prize WITH (NOLOCK) WHERE ID=@prizeID
      IF (@PrizeTypeIDForDailySpins = 21)
      BEGIN
        DECLARE @ScheduleMessageTableForDailySpins udt.ScheduleMessageTableType
        INSERT @ScheduleMessageTableForDailySpins SELECT @PlayerID ,'','','',''

        IF (@SkinID = 1)
          BEGIN
            EXEC Responsive..CogsScheduleMessage
              @ScheduleMessageTableForDailySpins,
              'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
              'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
              '1.1.1.1',
              @SkinID,
              183,
              @Now,
              60,
              0,
              1
          END
        IF (@SkinID = 3)
          BEGIN
            EXEC Responsive..CogsScheduleMessage
            @ScheduleMessageTableForDailySpins,
            'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
            'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
            '1.1.1.1',
            @SkinID,
            51,
            @Now,
            60,
            0,
            1
          END
      END
      
      
			SET @OldID = @GivenPlayerDailyPrizeID
			-- Get next prize
      SELECT  TOP 1 @GivenPlayerDailyPrizeID = ptb.ID, @GivenAmount =  ptb.NumPrizes, 
							@GivenPrizeID = ptb.PrizeID, @GivenPlayerClassPropertyInstanceID = ptb.ID
			FROM @PrizesToBeGiven ptb
			WHERE ID > @GivenPlayerDailyPrizeID 
			ORDER BY ID asc

		END
	END
		
		
		-- END DEV-7800


  ---- daily free cards / spins
  ---- give daily free spins if there are some available for the class or player.	
  ---- and they have not already received them today  
  --SET @FreeSpins = fn.PlayerClassPropertyValue(@PlayerID, 10) 
  --IF @FreeSpins > 0
  --  AND NOT EXISTS ( SELECT TOP 1 1
  --                   FROM   EventLog WITH (NOLOCK)
  --                   WHERE  $PARTITION.Fn_PartitionByDateTime(date) = 6 AND EventLogTypeID = 115 AND EntityID = @PlayerID )
  --  BEGIN
	  
	 -- SET @PrizeID = fn.PlayerClassPropertyExtraValue(@PlayerID,10)
	  		
  --    -- give them spins and write a log
  --    SET @msg = CAST(@FreeSpins AS VARCHAR(10)) + ' Daily Free Spins Given'  
  --    EXEC svrGiveBonusSpins @PlayerID, '', @FreeSpins, @msg,NULL,NULL,NULL,0,0,@PrizeID
  --    EXEC svrEventLog 115, @PlayerID, '', @msg
  --  END

  ---- daily free crads
  --SET @FreeCards = fn.PlayerClassPropertyValue(@PlayerID, 11) 
  --IF @FreeCards > 0
  --  AND NOT EXISTS ( SELECT TOP 1 1
  --                   FROM   EventLog WITH (NOLOCK)
  --                   WHERE  $PARTITION.Fn_PartitionByDateTime(date) = 6 AND EventLogTypeID = 116 AND EntityID = @PlayerID)
  --  BEGIN
	 -- SET @PrizeID = fn.PlayerClassPropertyExtraValue(@PlayerID,11)	
    
    
  --    -- give them spins and write a log
  --    SET @msg = CAST(@FreeCards AS VARCHAR(10)) + ' Daily Free Cards Given'  
  --    EXEC Bingo.dbo.svrGiveFreeCards @PlayerID, @SessionID, '1.1.1.1', @FreeCards, NULL, 3, 20, 0, NULL,@PrizeID
  --    EXEC svrEventLog 116, @PlayerID, '', @msg
  --  END
  
  --Competition engine triggers related to deposits
  EXEC svrCompRegisterAction @PlayerID, 2, @PlayerClassID, @PlayerFunded, @SkinID, @SessionID, NULL, @Amount, NULL
  
  --EXEC svrCompetitionTrigger 1,@PlayerID,0,@Amount
  --EXEC svrCompetitionTrigger 2,@PlayerID,0,@Amount
  --EXEC svrCompetitionTrigger 34,@PlayerID,0,@Amount
  --EXEC svrCompetitionTrigger 35,@PlayerID,0,@Amount
  --EXEC svrCompetitionTrigger 36,@PlayerID,0,@Amount

  UPDATE Cashier..TxDepositExtraProcessing SET Processed=1, ProcessDate = GETDATE(), ProcessTime = DATEDIFF(ms,@TimeStart,GETDATE()) WHERE TxDepositID = @TxDepositID
  SET @ID += 1
END

RETURN 0
--#--
END

