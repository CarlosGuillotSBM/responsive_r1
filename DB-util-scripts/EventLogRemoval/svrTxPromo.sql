USE [Main]
GO
/****** Object:  StoredProcedure [dbo].[svrTxPromo]    Script Date: 12/15/2016 12:50:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
Author:      AF
Date       : 01Jul09
Mod date(s):
Description: Writes Promo transaction to TxPromo, and updates player balances.
             Which player field to update (real/bonus/points)
             and the value its updated with depends on the Promo.TxTypeID and Promo.PromoValueType
             The Value that the applicable player field is updated with is determined by the PromoValueType and the Promo.Value field
             For example, an update based on a percentage of the source:
               if Promo.Value=0.50 (50%) and the @Value passed into this sproc is 100,
               the player balance will be updated with 50.00 (50% of 100)

             PromoValue determines the rate that the @Value passed in is multiplied by.

             ***
             Note that the @Value is ignored here where the @PromoValueType is 'mon' or 'int'
             meaning that the Promo is set up to be a fixed amount determined by Promo.Value
             ***

The TxType.Asset fields from TxType where TxType.Table = "TxPromo" (as used in this sproc) are important
in that they determine which Balance fields are updated.
The svrUpdatePlayerBalance sproc still uses hard coded TxTypeID's to determine the balance movement.
  +   51   TxPromo               Promo - Bonus Cash (Bonus)
  +   52   TxPromo               Promo - Points (Points)
  -  500   TxPromo               Virtual Gifts - Real Cash  (Real)
  -  501   TxPromo               Virtual Gifts - Bonus Cash (Bonus)
  -  502   TxPromo               Virtual Gifts - Points (Points)

Note that 500,501,502 "virtual gifts" are a special case in that they require the players applicable balance to be sufficient for the @Value
or for the Promo.Value because the balances will be reduced for the item.
As such, they are checked here before the transaction is written

Called From: various other sprocs (and from external servers via xTxPromo - xTxPromo serves to verify credentials)

Return Values:
Any Positive int = success. The return int represents the new TxPromo.ID
 0 = Balance update error
-1 = Promo not found
-2 = Balance insufficient (only used in special cases as described above)
-3 = Error inserting promo
-4 = Promo usage limit reached

17Nov11 AF - Added @Data for svrUpdatePlayerBalance
19Nov11 AF - Adjusted for removal of percent of source - using only rate of source now
22Feb12 AF - Added new types for real and gifts
29Feb12 AF - Added MaxBonus to TxPromo record
05Mar12 AF - ConvertableToReal and MaxConvertableToReal changes
08Mar12 AF - BonusVoidBalanceThreshold change
30Mar12 AF - IF @PromoTypeID = 8--bonus from a game void
               SET @TxPlayerBalanceData = 3
30Apr12 AF - Added TxPromoReplicateInsert
01May12 AF - TxPromoReplicateInsert.ID refers to TxPromo.ID
26Jul12 AF - Added check on number of times promo used, and returns -4 if usage limit is reached
30Aug12 AF - Added TxPromo.RestrictToGames field
16Sep12 AF - Added temporary net cash check in case its not covered in svrGetPromo
26Sep12 HI - Added Email Functionality if an email is associated with the promo
27Sep12 AF - Removed @NC (net cash amount) quick fix to disallow promos where @NC <= -100 - using player classes now
30Sep12 HI - Use Rep.dbo.svrAddMailToQueue to send mail
14fab13 HI - Super HACK!. If luckypants and _SmallReg_, _RemainingReg_, _BigReg_ Promocode
       Then issue bonus spins to the player
08mar13 HI - And the hacks just keep on coming! add free spins to 1st deposit  promos for spin and win
14mar13 - HI - removed bonus spins on small reg promo for LP
22mar13 - HI - added free spins on promoid 832
28mar12 - RS - Added promos to trigger free spins for easter login promos while we reingeneer prizes
10apr13 - HI - added 10 free spins to LP when FTD <= 10 mins from reg, and dep =10 cleared old free spin stuff
11apr13 - HI - added 20 free spins to LP when FTD <= 10 mins from reg and dep >=20 , cleared old free spin stuff
17apr13 - HI - Add promocode free spins to all skins
01May13 - AF - Changes for new promo schema
07May13 - AF - PromoLowDepositOffer status changes
07may13 - HI - Issue the Prize if one is attached to the promo
28May13 - AF - Dropped TxPromoReplicateInsert and insert directly into Rep
13Jun13 - AF - Added time based hacks for spins
24Jun13 - AF - Fixed chain promo lookup bug
25may13 - HI - Remove comments/hacks
26sep13 - RS - Changed promo ids for extra free spins hack to reflect new deposit promos
11oct13 - RS - Changed svrGiveBonusSpins call to svrGivePrizeByID so we could pick what PrizeID to give (temporary fix until we can test time limit promos)
13Oct13 - AF - Removed time based hack for kitty as time based now works
06Mar14 - AF - PlayerLimit Promo table updates 
06Feb15 - AF - If its a points issue, make DateCompleted now
10Jul15 - RS - DEV-2641 we're now able to award real cash from promotions of type 200 (previously only 100). Refactored some code and deleted deprecated hack
16Jul15 - AF - Write TxPromo.ExpiryDate based on Promo.TimeLimit
20Aug15 - AF - Apply correct DateActivated, DateCompleted, Real for promos of TxTypeID IN (100,200) (real money promos)
22Aug15 - AF - Allow promos with a TimeLimit = -1 to be converted to real immediately on issue of the promo
11Jan16 - SR - [Dev-3832] Modified to link Deposit transaction to Cogs Promo by considering deposit amount to wagering requirement
16mar16 - HI - return the prizeplayerID if there is one for 3rd party free spins
28apr16 - HI - [DES-3077] save the promoID in the mailBatch Table
14Jun16 - AF - If the promo is an instant cashback (TimeLimit = -1) then write the @TxPlayerBalanceData = 4
exec svrTxPromo 402,5226,8,0,0,0
06Oct16 - SR - [Dev-6641] If Promo.RateConvertableToReal >0 then MaxConvertableToReal= Bonus Amount * Promo.RateConvertableToReal
16Nov16 - RS - Changed agsRef from INT to BIGINT as we've jumped to those figures already.
15Dec16 - CM - DEV-7800 If there is a condition in PlayerClassPropertyInstance that matches this promo and player we add a new record in PlayerDailyPrize
=============================================*/
ALTER PROCEDURE [dbo].[svrTxPromo]
@PlayerID INT,
@PromoID INT,
@Value MONEY,        --based on a value from a specific range / Promo.Value that was determined by the Main.fn.GetPromo - will be a Fixed Amount - the caller must calculate the rate is applicable so that this value is the full amount to give as the promo
@DepositReal MONEY,  --this must be 0 unless its a deposit bonus promo or a cogs promo linked to a deposit
@ExternalRef BIGINT,    --Reference number from an external system if applicable. Will be zero if called from ags, but will be the TxGameVoid Id if from a game void, Will be the TxCashier.ID for deposit bonuses
@SendEmail BIT = 1,   --1 = Will send a mail if there is one associated with the promo, 0 = don't send mail
@PrizeplayerID int = 0 output -- return the prizeplayerID if there is one

AS
BEGIN
SET NOCOUNT ON;
--#--
DECLARE @PromoTypeID INT      --1:Reg, 2:Deposit, 3:Points to Cash etc
DECLARE @TxTypeID INT         --51:Promo - Bonus Cash, 52:Promo - Points, 100:Competition - Real Cash Win, 101:Competition Bonus Cash Win, 102:Competition Points Win, 103:Competition Prize Win
DECLARE @PromoValueTypeID INT --10:Cash, 11:Cash - Percent/Rate of Source, 12:Cash - Time Limits, 13:Cash - Player Number Limits, 20:Points, 21:Points - Percent/Rate of Source, 31:Real Cash - Percent/Rate of Source (for competitions) 40:Prize
DECLARE @PromoPoolID INT
DECLARE @PromoCode NVARCHAR(20)
DECLARE @ChainPromoID INT
DECLARE @ChainPromoStart BIT
DECLARE @ChainPromoEnd BIT
DECLARE @ChainPromoReset INT  -- -1:No reset 0:Reset immediately, 1+:reset after that number of days
DECLARE @WagerMultiple INT
DECLARE @TimeLimit INT
DECLARE @ConvertableToReal TINYINT
DECLARE @MaxConvertableToReal MONEY
DECLARE @MaxBonus MONEY
DECLARE @BonusVoidBalanceThreshold MONEY
DECLARE @AmountReal MONEY = 0
DECLARE @AmountBonus MONEY = 0
DECLARE @AmountPoints INT = 0
DECLARE @AmountRealToAward MONEY = 0
DECLARE @WagersRequired MONEY = 0
DECLARE @TxPlayerBalanceData SMALLINT = 0
DECLARE @MaxTimesPromoCanBeUsed INT
DECLARE @RestrictToGames BIT
DECLARE @MailTemplateID INT
DECLARE @SkinID INT
DECLARE @PromoLowDepositOfferID INT = NULL
DECLARE @PromoCodeList BIT
DECLARE @PrizeID INT = 0
DECLARE @PrizeAmount MONEY = 0
DECLARE @TotalAmountReal MONEY = 0
DECLARE @Now DATETIME2 = GETDATE()
DECLARE @RateConvertableToReal INT=0

SELECT @PromoTypeID = Promo.PromoTypeID,
       @TxTypeID = Promo.TxTypeID,
       @PromoValueTypeID = Promo.PromoValueTypeID,
       @PromoPoolID = Promo.PromoPoolID,
       @PromoCode = Promo.PromoCode,
       @ChainPromoID = Promo.ChainPromoID,
       @ChainPromoStart = Promo.ChainPromoStart,
       @ChainPromoEnd = Promo.ChainPromoEnd,
       @ChainPromoReset = Promo.ChainPromoReset,
       @WagerMultiple = Promo.WagerMultiple,
       @TimeLimit = Promo.TimeLimit,
       @ConvertableToReal = Promo.ConvertableToReal,
       @MaxConvertableToReal = Promo.MaxConvertableToReal,
       @MaxBonus = Promo.MaxBonus,
       @BonusVoidBalanceThreshold = Promo.BonusVoidBalanceThreshold,
       @MaxTimesPromoCanBeUsed = Promo.MaxTimesPromoCanBeUsed,
       @RestrictToGames = Promo.RestrictToGames,
       @MailTemplateID = Promo.MailTemplateID,
       @SkinID = Promo.SkinID,
       @TxPlayerBalanceData = CASE Promo.TimeLimit WHEN -1 THEN 4 ELSE 
                                CASE WHEN Promo.TxTypeID IN (51,100,101)--Cash types
                                     THEN CASE Promo.IncludeInAffiliateCalculation
                                            WHEN 1 THEN 1
                                            ELSE 2
                                          END
                                     ELSE 0
                                END
                              END,
       @PromoLowDepositOfferID = PromoLowDepositOffer.ID,
       @PromoCodeList = CASE SUBSTRING(Promo.PromoCode,1,1) WHEN '{' THEN 1 ELSE 0 END,
       @PrizeID = PrizeID,
       @PrizeAmount = PrizeAmount,
       @RateConvertableToReal=Promo.RateConvertableToReal
  FROM Promo WITH (NOLOCK)
  LEFT JOIN PromoLowDepositOffer ON PromoLowDepositOffer.PlayerID = @PlayerID AND PromoLowDepositOffer.PromoID = Promo.ID AND PromoLowDepositOffer.STATUS=1
  WHERE Promo.ID = @PromoID


--Cannot find promo
IF @PromoTypeID IS NULL
  RETURN -1

--ReCheck usage
IF @MaxTimesPromoCanBeUsed > 0 AND
   @MaxTimesPromoCanBeUsed = (ISNULL((SELECT Total
                                        FROM TxPlayerPromo WITH (NOLOCK)
                                        WHERE PlayerID = @PlayerID AND PromoID = @PromoID),0))
  RETURN -4

--Bonus from a game void
IF @PromoTypeID = 8
  SET @TxPlayerBalanceData = 3

--Check @Value does not exceed @MaxBonus
IF @Value > @MaxBonus AND @MaxBonus > 0
  SET @Value = @MaxBonus

/*
Special case - TxTypes that require sufficient balance must be checked
*/
IF @TxTypeID  IN (500,501,502)
  BEGIN
    DECLARE @Real MONEY
    DECLARE @Bonus MONEY
    DECLARE @Points INT
    SELECT @Real   = Player.Real,
           @Bonus  = Player.Bonus,
           @Points = Player.Points
    FROM dbo.Player
    WHERE Player.ID = @PlayerID

    IF ((@TxTypeID = 500 AND @Real < @Value)
        OR
       (@TxTypeID = 501 AND @Bonus < @Value)
        OR
       (@TxTypeID = 502 AND @Points < @Value))

       RETURN -2
  END

/*
Determine the AmountReal, AmountBonus and AmountPoints and the TxPromo.Status
*/
DECLARE @TxPromoStatus TINYINT = 1

--51:Promo - Bonus Cash, 52:Promo - Points, 100:Competition - Real Cash Win, 101:Competition Bonus Cash Win, 102:Competition Points Win, 103:Competition Prize Win
--For the real deposit amount
IF (@PromoTypeID = 2 OR (@PromoTypeID=9 AND @DepositReal>0))
  SET @AmountReal = @DepositReal

--Real Cash
IF @TxTypeID IN (100,200)
  BEGIN
    SET @TxPromoStatus = 1--non bonus promo (points or real)
    SET @AmountRealToAward = @Value
  END
  
--Bonus Cash
IF @TxTypeID IN (51,101)
  BEGIN
    SET @TxPromoStatus = 2--pending wager requirement completion
    SET @AmountBonus = @Value
    SET @WagersRequired = @WagerMultiple * (@AmountReal + @AmountBonus)
  END

--Points
IF @TxTypeID IN (52,102)
  BEGIN
    SET @TxPromoStatus = 1--non bonus promo (points or real)
    SET @AmountPoints = @Value
  END

--If its a convert to real immediately, make the Status complete
IF @TimeLimit = -1
  SET @TxPromoStatus = 3--wager requirements met and bonus balances transerred to real

IF (@RateConvertableToReal>0 AND @MaxConvertableToReal=0)
BEGIN
	SET @MaxConvertableToReal=@AmountBonus*@RateConvertableToReal
END
/*
Write the TxPromo record
*/
DECLARE @TxPromoID INT
DECLARE @TxPromoIDRep INT
DECLARE @ExpiryDate DATETIME2 = CASE WHEN @TimeLimit <= 0 THEN NULL ELSE DATEADD(HOUR,@TimeLimit,@Now) END

INSERT TxPromo
  (PlayerID, PromoID, [Date], IsDepositBonus, DateActivated, DateCompleted, ExternalRef, TimeLimit, ExpiryDate, Real, Bonus, Points, WagersRequired, ConvertableToReal, MaxConvertableToReal, MaxBonus, BonusVoidBalanceThreshold, RestrictToGames, RealBal, BonusBal, Status, RealToAward, BonusBalTransferredToReal)
   VALUES
  (@PlayerID, @PromoID, @Now, CASE @PromoTypeID WHEN 2 THEN 1 ELSE 0 END,
   CASE @TxPromoStatus WHEN 2 THEN NULL ELSE @Now END,
   CASE @TxPromoStatus WHEN 2 THEN NULL ELSE @Now END,
   @ExternalRef, @TimeLimit, @ExpiryDate,
   CASE WHEN @TxTypeID IN (100,200) THEN @AmountRealToAward ELSE @AmountReal END,
   @AmountBonus, @AmountPoints, @WagersRequired, @ConvertableToReal, @MaxConvertableToReal, @MaxBonus, @BonusVoidBalanceThreshold, @RestrictToGames, @AmountReal, @AmountBonus, @TxPromoStatus, @AmountRealToAward,
   CASE @TimeLimit WHEN -1 THEN @AmountBonus ELSE 0 END
   )

SET @TxPromoID = SCOPE_IDENTITY()

--Error
IF @TxPromoID IS NULL
  RETURN -3

--Rep.TxPromo insert
INSERT Rep..TxPromo 
  (ID, PlayerID, PromoID, [Date], DateActivated, IsDepositBonus, RealBalanceAtactivation, DateCompleted, ExternalRef, TimeLimit, ExpiryDate,
   [Real], Bonus, Points, WagersRequired, WagersApplied, ConvertableToReal, MaxConvertableToReal, MaxBonus, BonusVoidBalanceThreshold, RestrictToGames,
   RealBal, BonusBal, BonusWinsBal, BonusBalTransferredToReal, BonusWinsBalTransferredToReal, Status, RealToAward)
   VALUES
  (@TxPromoID, @PlayerID, @PromoID, @Now,
   CASE @TxPromoStatus WHEN 2 THEN NULL ELSE @Now END,
   CASE @PromoTypeID WHEN 2 THEN 1 ELSE 0 END,
   0,
   CASE @TxPromoStatus WHEN 2 THEN NULL ELSE @Now END,
   @ExternalRef, @TimeLimit, @ExpiryDate,
   CASE WHEN @TxTypeID IN (100,200) THEN @AmountRealToAward ELSE @AmountReal END,
   @AmountBonus, @AmountPoints, @WagersRequired, 0, @ConvertableToReal, @MaxConvertableToReal, @MaxBonus, @BonusVoidBalanceThreshold, @RestrictToGames,
   @AmountReal, @AmountBonus, 0,
   CASE @TimeLimit WHEN -1 THEN @AmountBonus ELSE 0 END,
   0,
   @TxPromoStatus, @AmountRealToAward
   )

SET @TxPromoIDRep = SCOPE_IDENTITY()

--Only update the real player balance if not a deposit, because CashierDeposit/CoinSkinDeposit deals with the deposit balance update for real
IF (@PromoTypeID = 2 OR (@PromoTypeID=9 AND @DepositReal>0)) --If Cogs promo linked to deposit promo
  SET @AmountReal = 0
  
SET @TotalAmountReal = @AmountReal + @AmountRealToAward

--Update player balance if not a gift
IF (@AmountReal+@AmountBonus+@AmountPoints+@AmountRealToAward) > 0
  EXEC svrUpdatePlayerBalance @PlayerID, @TxTypeID, @TxPromoID, @TotalAmountReal, @AmountBonus, 0, @AmountPoints, -1, @TxPlayerBalanceData

IF NOT EXISTS (SELECT 1 FROM TxPlayerPromo WITH (NOLOCK) WHERE PlayerID = @PlayerID AND PromoID = @PromoID)
  INSERT dbo.TxPlayerPromo (PlayerID, PromoID, Total) VALUES (@PlayerID, @PromoID, 1)
ELSE
  UPDATE dbo.TxPlayerPromo SET Total+=1 WHERE PlayerID = @PlayerID AND PromoID = @PromoID

/*
Convert to real is TimeLimit = -1
*/
IF @TimeLimit = -1
BEGIN
  EXEC svrUpdatePlayerBalance
       @PlayerID,
       16,--TxTypeID=16 - Player balance adjustment from a completed, cancelled or voided bonus promo
       @TxPromoID,
       @AmountBonus,--Amount Real being credited with the bonus amount
       @AmountBonus,--Amount of the bonus being removed
       0,0,-1,@TxPlayerBalanceData
END

/*
PlayerLimit Promo updates
*/
IF @PromoValueTypeID = 13
  BEGIN
    UPDATE Promo SET PlayerLimitCount += 1 WHERE ID = @PromoID
  END

/*
PromoPool updates
*/
IF @PromoPoolID <> 0
  BEGIN
    IF NOT EXISTS (SELECT 1 FROM dbo.PromoPoolPlayer WITH (NOLOCK) WHERE PlayerID = @PlayerID AND PromoPoolID = @PromoPoolID)
      INSERT dbo.PromoPoolPlayer (PromoPoolID, PlayerID, Date, Status) VALUES (@PromoPoolID, @PlayerID, GETDATE(), 1)
    ELSE
      UPDATE dbo.PromoPoolPlayer SET [Date] = GETDATE(), [Status]=1 WHERE PlayerID = @PlayerID AND PromoPoolID = @PromoPoolID
  END

/*
PromoChain updates
@PromoChainStatus=1 IF its IN the chain progression, OR its finished but will NOT RESET immediately
*/
DECLARE @PromoChainStatus TINYINT = 1
IF @ChainPromoEnd = 1 AND @ChainPromoReset = 0
  SET @PromoChainStatus = 0

IF @ChainPromoStart = 1 OR @ChainPromoID <> 0 OR @ChainPromoEnd = 1
  BEGIN
    INSERT dbo.PromoChain (PlayerID, PromoID, Date, Status) VALUES (@PlayerID, @PromoID, GETDATE(), @PromoChainStatus)
  END

IF @PromoChainStatus = 0
  BEGIN
    DECLARE @ThisPromoID INT = @PromoID
    WHILE @ThisPromoID IS NOT NULL AND @ThisPromoID > 0
    BEGIN
      UPDATE dbo.PromoChain SET Status = 0 WHERE PlayerID = @PlayerID AND PromoID = @ThisPromoID
      SELECT @ThisPromoID = Promo.ChainPromoID FROM Promo WITH (NOLOCK) WHERE Promo.ID = @ThisPromoID
    END
  END

/*
PromoLowDepositOffer updates
*/
IF @PromoLowDepositOfferID IS NOT NULL
  UPDATE PromoLowDepositOffer SET Status = 3 WHERE ID = @PromoLowDepositOfferID

/*
Update the PromoCode table if its a PromoCodeList
*/
IF @PromoCodeList = 1
  UPDATE dbo.PromoCode
    SET PlayerID = @PlayerID,
        DateClaimed = GETDATE()
    WHERE PromoID = @PromoID
          AND Code = @PromoCode
          AND PlayerID = 0


-- Issue the Prize is one is attached
IF @PrizeID > 0 AND @PrizeAmount > 0
  EXEC @PrizeplayerID = dbo.svrGivePrizeByID @PlayerID,@PrizeAmount,@PrizeID

-- DEV-7800 If there is a condition in PlayerClassPropertyInstance that matches this promo and player we add a new record in PlayerDailyPrize
DECLARE @PlayerClassID INT, @DaysAmount INT, @PlayerClassPropertyID INT = NULL, @PlayerClassPropertyInstanceID INT 

SELECT @PlayerClassID = p.PlayerClassID FROM Main..Player p (NOLOCK) where p.ID = @PlayerID

SELECT TOP 1 @PrizeID = p.PrizeID, @PrizeAmount = p.PrizeAmount, @DaysAmount = pcp.Value, @PlayerClassPropertyID = pcp.PlayerClassPropertyID, @PlayerClassPropertyInstanceID = pcp.ID
FROM Main.dbo.Promo p (NOLOCK)
JOIN Main.dbo.PlayerClassPropertyInstance pcp (NOLOCK) ON p.ID = pcp.ExtraValue
WHERE (pcp.PlayerClassID = @PlayerClassID OR pcp.PlayerID = @PlayerID) and pcp.PlayerClassPropertyID IN (1043,43) 
AND ((@Now BETWEEN pcp.FromDate AND pcp.ToDate) OR (pcp.FromDate IS NULL AND pcp.ToDate IS NULL)) AND p.ID = @PromoID
ORDER BY pcp.PlayerClassPropertyID desc

IF (@PlayerClassPropertyID IS NOT NULL) 
BEGIN
	INSERT INTO Main.dbo.PlayerDailyPrizes (PlayerID, NumPrizes,PrizeID,PlayerClassPropertyID,PlayerClassPropertyInstanceID, StartDate, EndDate)
	VALUES (@PlayerID, @PrizeAmount, @PrizeID, CASE WHEN @PlayerClassPropertyID > 1000 THEN @PlayerClassPropertyID - 1000 ELSE @PlayerClassPropertyID END, @PlayerClassPropertyInstanceID, DATEADD(DAY,1,CONVERT(date,@Now)), DATEADD(MICROSECOND,-1,CONVERT(DATETIME2(7),DATEADD(DAY,@DaysAmount+2,CONVERT(date,@Now)))))
END
-- END DEV-7800 ----------

/*
add to the mail queue if required
*/
IF @MailTemplateID > 0 AND @SendEmail = 1
  BEGIN
    EXEC Rep.[dbo].[mailAddToQueue]
      @MailTemplateID = @MailTemplateID,
      @MailTemplateTypeID  = 1, -- promo type
      @PlayerID = @PlayerID,
      @Value = @Value,
      @ExpireDate = NULL,
	  @CustomString1 = NULL,
      @Status  = 0,
      @PromoID= @PromoID      
  END


RETURN @TxPromoID
--#--
END
