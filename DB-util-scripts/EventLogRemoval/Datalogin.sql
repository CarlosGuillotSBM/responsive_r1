USE [Responsive]
GO
/****** Object:  StoredProcedure [dbo].[DataLogin]    Script Date: 12/16/2016 19:53:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*==========================================================================================
Author:    AF
Date       : 18May09
Description: Creates a new login session when there is a successful login.
             On a successful login, the GUID created here is used for the entire players session
             Checks players expired promos, expires them and wars of pending expiries
Called From: web login page

EventLogTypeID used in sproc:
100 - Successful Login
101 - Failed Login - Username+Skin not found
102 - Failed Login - Username+Skin good, password bad
103 - Failed Login - Username+Skin good but status disabled
106 - Failed Login 3 times - Account disabled - Username+Skin good, password bad
107 - Failed Login 5+ times in a day - Account disabled - Username+Skin good, password bad
108 - Failed Login - Username/Email does not exist for any group
109 - Failed Login - Skin we are logging into does not belong to Group of the players original Skin they registered with
111 - Failed Login - player does not exist anywhere for the group
112 - Failed Login - Sister skin player is logging into disallowed because the players Status on the original skin or last skin player logged into is disabled
20 - Successful Registration on Sister Skin


--Returns
[Code] =
  0: Successful login
  1: Failed login
  3: If there are already 2+ bad password attempts in last 24 hours, lock the account, add reactivation code or Pending Email activation =player should be sent an email from remote server with notice and a link to reactivate the account.
  4: Account is locked - 0|2|4|5|6 (5/6=Disabled due to country/currency change)
  5: Player self excluded [SelfExclude] = string describing date till it ends, or indefinate
  6: Account locked from 5+ bad attempts
  7: Successful login and registration on sister skin - pending ack of t+c
  8: if the email already exists in the target skin. then tell player to contact support.
  9: needs to accept t+c
 10: IP from a country we don't like very much


 Updates:
 HI - 6aug14 - make sure we always have the language code
 HI - 3nov14 - handle T&C better
 HI - 29jan15 - return code instead of result after 3 wrong pwd attempts
 HI - 11feb15 - return funded flag
 HI - 16feb15 - fix bug with free spins for 4 days from FTD
 AF - 16Apr15 - Hash password
 NO - 29Jun15 - return MobileNumberVerified
 CM - 02jul15 - [DEV-2520] returns LimitConfirmation
 CM - 13jul15 - [DEV-2607] and [DEV-2520] Limit confirmation message
 CM - 13jul15 - [DEV-2520] Limit confirmation message, adding type of limit
 AF - 21Jul15 - Uncomment svrVoidExpiredPromo - whay was this commented out???
 CM - 29jul15 - [DEV-2607] 24 hours instead of 2 minutes
 AF - 04Aug15 - Adjusted for new deposit limits, removed redunant playerlimits
 HI - 17aug15 - [DEV-2960] return playerclass directly instead of via playerstage.
 CM - 24aug15 - [DEV-3095] adds date registered to check the game permission
 AF - 16Sep15 - Fixed @LimitConfirmation return to be 0 or 1
 CM - 28Sep15 - [DEV-3411] Player on a break
 CM - 30Sep15 - [DEV-2882] Adding Cashback
 CM - 01Oct15 - [DEV-3530] Adding dimensions/ScreenSize
 HI - 05oct15 - update device category if it's wrong.
 CM - 12oct15 - [DEV-2882] Removing Cashback from login
 CM - 21oct15 - [DEV-3732] Security email being sent
 RS - 29oct15 - [DEV-3846] Changed max login attempts from 2 to 10 as a temporary measure
 CM - 30oct15 - [DEV-3732] Synchronised from live
 CM - 30oct15 - [DEV-3710] RedirectURL added
 RS - 03nov15 - [DEV-3846] Changed relogin attempts from 5 to 15 as a temporary measure
 PS - 25Jan2016 - [DEV-3813] Modified to Add Data to New Column LastLoginIP
 CM - 11Feb16 - [DEV-3831] Adding 8|9|10|11 new invalid statuses from DEV-3831
 HI - 10mar16 - [DEV-5046] pass gaming data in response ( dummy data at the moment )
 HI - 30MAR16 - return the affiliateID on login
 HI - 13apr16 - return dynamic variable
 MB - 25May16 - DEV-5525: create a record in compAction after login so the CE can calculate points
 CM - 06Jun16 - [DEV-5521] ExistingPlayer and ExpiredTestingPeriod added to let the front end know whether the users are new or old from new LP launch and when we need to stop allowing the swap between old and new
 SR - 13Jun16 - [DEV-5581]- Free spins for X days from deposit opted for a specic promo based on player class property Id 43 starting the day after
 20Jun16 AF - DEV-5459
 ZP - 04Jul16 - DEV-5952: Added Player Funded restriction to be taken in account.
 HI - 05jul16 - add eventlogtypeid = 122 for player self exclusion
 ZP - 29Jul16 - DEV-5730 Changed the competition action register so it takes now amount real and amount bonus (instead of just amount)
 SR - 21Sep16 - [Dev-6413] Added PrizeTypeId=21 for Daily spins for X days
 SR - 14Oct16 - [Dev-7124] Modified to schedule a message when net ent free spin is given
 CM - 01Nov16 - Adding (nolock) to EventLog table wherever it didn't have it, because it was slowing the proc down to a point of making it not functional
 05Dec16 AF - Added WHERE $PARTITION.Fn_PartitionByDateTime(date) = 6 (only todays events) for eventlog searches
 DB - 07Dec16 - [DEV-7821] changed partition check to >= 6 for free spins and cards
 15Dec16 - CM - DEV-7800 If There are daily prizes to give we give them to the player

 select * from main..player where skinid=5
 update main..player set status=1 where id = 43
 DataLogin 'celestinom','qqq111',1,1,'','en',''

 datalogin 'hamish', 'qqq111',1,1,'','en','192.168.0.1'

 exec responsive..datalogin 'grape', 'xcalibur1',5,1,'','en','192.168.0.1'
==========================================================================================*/
ALTER PROCEDURE [dbo].[DataLogin]
@Username NVARCHAR(100), --can be username OR email - var gets changes to username if a success and returned from sproc
@Password NVARCHAR(15),
@SkinID INT,
@DeviceCategoryID INT,
@UserAgent VARCHAR(500),
@lang VARCHAR(2),
@IP VARCHAR(15),
@ScreenSize VARCHAR(100) = 'Unknown',
@RedirectURL NVARCHAR(100) = ''
AS
BEGIN
SET NOCOUNT ON;
--#--
SET DEADLOCK_PRIORITY -9 --Low priority - this sproc be a victim of most deadlocks
-- [DEV-5521] ExistingPlayer and ExpiredTestingPeriod added to let the front end know whether the users are new or old from new LP launch and when we need to stop allowing the swap between old and new
-- this two dates need to be updated on new LP launch
--DECLARE @NewLuckyStartDate DATETIME2(7) = '2016-06-06 11:05:12.8230000'
--DECLARE @NewLuckyEndDate DATETIME2(7) = '2016-07-06 11:05:12.8230000'

DECLARE @Now DATETIME2(7) = GETDATE()
DECLARE @today DATETIME2(7) = CAST(@Now AS DATE)
DECLARE @WeekStart DATETIME2(7) = DATEADD(DAY, -7, CAST(@Now AS DATE))
DECLARE @Data NVARCHAR(4000) = 'Skin:' + CAST(@SkinID AS VARCHAR(10)) + ' U:' + @Username
DECLARE @PlayerID INT = NULL
DECLARE @PlayerIDRelated INT = NULL
DECLARE @ReturnData INT = 0
DECLARE @Status TINYINT = NULL
DECLARE @EventLogTypeID INT
DECLARE @SessionID UNIQUEIDENTIFIER
DECLARE @dbPassword VARBINARY(255) = NULL
DECLARE @SelfExclude VARCHAR(11) = ''
DECLARE @RegSessionID UNIQUEIDENTIFIER
DECLARE @GroupID INT
DECLARE @PlayerClassID INT
DECLARE @PlayerFunded BIT
DECLARE @FreeSpins INT
DECLARE @FreeCards INT
DECLARE @PlayerTypeID INT

DECLARE @msg NVARCHAR(500)
DECLARE @CurrencyCode VARCHAR(3)
DECLARE @CurrencySymbol NVARCHAR(10)
DECLARE @CentsSymbol NVARCHAR(10)
DECLARE @DepositCount INT

DECLARE @BreakDate DATETIME2(7)
DECLARE @AffID INT
DECLARE @SubCampaignID INT
DECLARE @DynamicVariable VARCHAR(100)

DECLARE @RegisterCountry VARCHAR(3)

--Login to the skin
SELECT  @PlayerID = Player.ID ,
        @PlayerIDRelated = Player.ID ,
        @dbPassword = Player.[PasswordHash] ,
        @Status = Player.[Status] ,
        @RegSessionID = Player.[RegSessionID] ,
        @GroupID = Skin.GroupID ,
        @PlayerTypeID = Player.PlayerTypeID ,
        @lang = Skin.LanguageCode ,
        @CurrencySymbol = currency.Symbol ,
        @CentsSymbol = currency.CentsSymbol,
        @BreakDate = Player.TakeABreakEndDate,
        @SubCampaignID = Player.SubCampaignID,
        @RegisterCountry = Player.CountryCode
FROM    Main..Player WITH ( ROWLOCK )
        JOIN Main..Skin WITH ( NOLOCK ) ON [Skin].ID = Player.SkinID
        JOIN Main..Currency WITH ( NOLOCK ) ON Player.CurrencyCode = currency.Code
WHERE   ( [Username] = @Username OR [Email] = @Username )
        AND [SkinID] = @SkinID

DECLARE @CountryCode VARCHAR(3) = Main.fn.getCountryCodeFromIP(@IP)--returns '' if not found

--Check the country is enabled and is available to the Skin
IF @CountryCode <> ''
  BEGIN
    SET @CountryCode = ( SELECT CountryCode
                         FROM   Main..Country WITH ( NOLOCK )
                                JOIN Main..SkinCountry WITH ( NOLOCK ) ON SkinCountry.CountryCode = Country.Code
                         WHERE  Country.Code = @CountryCode
                                AND SkinCountry.SkinID = @SkinID
                                AND Country.Status = 1
                                AND SkinCountry.Status = 1
                       )

    IF @PlayerTypeID = 0 AND @CountryCode IS NULL
      BEGIN
        SELECT  'Code' = 10 ,
                'Msg' = fn.GetEventMessage(@lang, 'LOGIN_FAIL_BAD_COUNTRY')
        RETURN
      END

    -- if the player has made a login from a different country within the last hour then disable the account
    /*
    DECLARE @LastLoginCountry VARCHAR(3) = 
      (SELECT TOP 1 Main.fn.getCountryCodeFromIP(IP) FROM Main..LoginSession WITH ( NOLOCK ) WHERE PlayerID = @PlayerID AND DATEDIFF(HOUR, LastActivity, @Now) < 1 ORDER BY ID DESC)

    IF @LastLoginCountry IS NOT NULL AND (@LastLoginCountry <> @CountryCode OR @CountryCode <> @RegisterCountry)
    BEGIN
      SET @EventLogTypeID = 123
      SET @ReturnData = 10
      SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_ACCOUNT_LOCKED')
      UPDATE Main..Player SET [Status] = 0 WHERE ID = @PlayerID
      GOTO WriteLog
    END
    */
  END

-- check we have the language in case an error occures
IF @lang IS NULL
  BEGIN
    SELECT  @lang = LanguageCode FROM Main..Skin WITH (NOLOCK) WHERE ID = @SkinID
  END

--User not found - check for related skins
IF @PlayerID IS NULL
  OR
  ( @PlayerID IS NOT NULL AND ( PWDCOMPARE(@Password, @dbPassword) = 0 ) )
  BEGIN
    DECLARE @RelatedSkinsString VARCHAR(1000)
    DECLARE @RelatedSkin VARCHAR(100)

    SELECT  @RelatedSkinsString = Skin.RelatedSkins ,
            @GroupID = Skin.GroupID
    FROM    Main..Skin WITH ( NOLOCK )
    WHERE   ID = @SkinID

    
    DECLARE @RelatedSkins TABLE ( SkinID INT )
    INSERT  @RelatedSkins SELECT ID FROM Daga.fn.CSVToNVARCHARTable(@RelatedSkinsString)

    DECLARE @RelatedSkinID INT = ( SELECT TOP 1 SkinID FROM @RelatedSkins )
    DECLARE @RelatedSkinUsername NVARCHAR(15) --in case player logged in with email
    DECLARE @RelatedEmail VARCHAR(100)

    WHILE @RelatedSkinID IS NOT NULL
      BEGIN
        SET @RelatedSkinID = ( SELECT TOP 1 SkinID FROM @RelatedSkins )
        SET @PlayerIDRelated = NULL

        SELECT  @PlayerIDRelated = Player.ID,
                @RelatedSkin = Skin.Name,
                @RelatedSkinUsername = Player.Username,
                @RelatedEmail = Player.Email
        FROM    Main..Player WITH ( ROWLOCK )
                JOIN Main..Skin WITH ( NOLOCK ) ON Skin.ID = @RelatedSkinID
        WHERE   ( Player.Username = @Username OR Player.Email = @Username )
                AND Player.SkinID = @RelatedSkinID
                AND ( PWDCOMPARE(@Password, Player.PasswordHash) = 1 )
                AND Player.STATUS = 1 --active
                AND Player.PlayerStage >= 50 --funded

        IF @PlayerIDRelated IS NOT NULL
          BEGIN
            IF EXISTS ( SELECT TOP 1 1 FROM Main..Player ( NOLOCK ) WHERE SkinID = @SkinID AND Email = @RelatedEmail )
              BEGIN
                -- if the email already exists in the target skin. then tell player to contact support.
                SELECT  'Code' = 8 ,
                        'Msg' = fn.GetEventMessage(@lang, 'LOGIN_X_LOGIN_TARGET_EMAIL_EXISTS')
                RETURN
              END
            BREAK
          END

        DELETE @RelatedSkins WHERE SkinID = @RelatedSkinID
      END

    IF @PlayerIDRelated IS NOT NULL AND @RelatedSkin IS NOT NULL
      --return parent skins details to caller
      BEGIN
        DECLARE @UsernameAvailable BIT = 1
        IF EXISTS ( SELECT TOP 1 1
                    FROM    Main..Player WITH ( NOLOCK )
                    WHERE   SkinID = @SkinID AND Username = @RelatedSkinUsername )
          SET @UsernameAvailable = 0
    
        SELECT  'Code' = 7 ,
                'Msg' = fn.GetEventMessage(@lang, 'LOGIN_X_LOGIN_SUCCESS') ,
                'PlayerID' = @PlayerIDRelated ,
                'Skin' = @RelatedSkin ,
                'SkinID' = @RelatedSkinID ,
                'UsernameAvailable' = @UsernameAvailable
        RETURN
      END
    ELSE
      BEGIN
        SET @EventLogTypeID = 101
        SET @ReturnData = 1        
        SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_USER_OR_PWD_NOT_FOUND')        
      END
  END

--Username+Skin ok, check Password and status
IF ( (PWDCOMPARE(@Password, @dbPassword) = 1) ) AND ( @Status IN ( 1, 3 ) ) --Good login (3=pending email validation)
  BEGIN
    SET @EventLogTypeID = 100
    SET @ReturnData = 0
  END

--Account is locked - 0|2|4|5|6 (5/6=Disabled due to country/currency change) plus 8|9|10|11 new invalid statuses from DEV-3831
IF @Status IN ( 0, 2, 4, 5, 6, 8, 9, 10, 11 )
  BEGIN
    IF (@Status = 0 AND @BreakDate > '2000-01-01 00:00:00.0000000' )
      BEGIN
        SET @EventLogTypeID = 121
        SET @ReturnData = 11
        SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_TAKE_BREAK')
      END
    ELSE
      BEGIN
        SET @EventLogTypeID = 103
        SET @ReturnData = 4
        SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_ACCOUNT_LOCKED')
        END
      END

--Incorrect password
IF ( PWDCOMPARE(@Password, @dbPassword) = 0 ) AND @Status IN ( 1, 3, 7 )
  BEGIN
    SET @EventLogTypeID = 102
    SET @ReturnData = 1

    --If there are already 10+ bad password attempts in last 24 hours, lock the account, add reactivation code
    IF ( ( NOT EXISTS ( SELECT TOP 1 1
                        FROM    Main..PlayerReactivate WITH (NOLOCK)
                        WHERE   PlayerReactivate.PlayerID = @PlayerID
                                AND ( ( PlayerReactivate.Status IN ( 1, 3, 5, 6 ) )
                                      OR ( PlayerReactivate.Status = 2 AND PlayerReactivate.Date >= DATEADD(HOUR, -24, @Now) )
                                    )
                      )
         )
         AND ( ( SELECT COUNT(*)
                 FROM   Main..EventLog WITH (NOLOCK)
                 WHERE  Main.$PARTITION.Fn_PartitionByDateTime(Date) = 6 AND EventLogTypeID = @EventLogTypeID AND EntityID = @PlayerID ) >= 10 )
       )
      BEGIN
        --Result 3 will indicate to remote server to email the player - the code will be supplied
        --Note its ok to move status from "3" to "4" because the player requires email validation anyway here, and the WebPlayerReactivate sproc will deal with any reg validation records
        SET @ReturnData = 3
        SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_ACCOUNT_EMAIL_ACTIVATION_REQ')

        --Lock Player
        UPDATE Main..Player WITH ( ROWLOCK ) SET [Status] = 4 WHERE ID = @PlayerID

        --DWH Code for Table Modifications
        EXEC Daga..dwhMods 'Main', 'Player', @PlayerID

        UPDATE Rep..Player WITH ( ROWLOCK ) SET [Status] = 4 WHERE ID = @PlayerID

        --Write reactivation record
        SET @SessionID = NEWID()
        INSERT  Main..PlayerReactivate ( ID, PlayerID ) VALUES ( @SessionID, @PlayerID )

        --Change event log id to track account status change
        SET @EventLogTypeID = 106
      END


    --OK if they have really screwed up and
    --If there are already 15+ bad password attempts in last 24 hours
    --we've locked the account, it's been re-opened and they have still screwed up
    IF ( ( NOT EXISTS ( SELECT TOP 1 1
                        FROM    Main..PlayerReactivate WITH (NOLOCK)
                        WHERE   PlayerReactivate.PlayerID = @PlayerID
                                AND ( PlayerReactivate.Status = 2 AND PlayerReactivate.Date >= DATEADD(HOUR, -24, @Now) ) )
         )
         AND ( ( SELECT COUNT(*)
                 FROM   Main..EventLog WITH (NOLOCK)
                 WHERE  Main.$PARTITION.Fn_PartitionByDateTime(date) = 6 AND EventLogTypeID IN ( 106, 102 ) AND EntityID = @PlayerID ) >= 15 )
       )
      BEGIN
        --Result 6 will indicate the player needs to call the call center
        SET @ReturnData = 6
        SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_ACCOUNT_LOCKED')

        --Lock Player
        UPDATE Main..Player WITH ( ROWLOCK ) SET [Status] = 0 WHERE ID = @PlayerID

        --DWH Code for Table Modifications
        EXEC Daga..dwhMods 'Main', 'Player', @PlayerID

        UPDATE Rep..Player WITH ( ROWLOCK ) SET [Status] = 0 WHERE  ID = @PlayerID

        --Change event log type id for player status changed to 0
        SET @EventLogTypeID = 107
      END
  END

--EventLog and self exclusion
IF @ReturnData = 0
  BEGIN
    --Check self exclusion
    IF @Status IN (12,13,14)--Player.Status
    BEGIN
      IF @Status = 13
        SET @SelfExclude = 'PendingLift'
      ELSE
        SET @SelfExclude = ISNULL(( SELECT TOP 1
                                           Daga.fn.formatDate(CASE @Status WHEN 14 THEN PlayerSelfExclude.DateRemoveExclusion ELSE PlayerSelfExclude.DateTo END, '', '')
                                    FROM   Main..PlayerSelfExclude WITH (NOLOCK)
                                    WHERE  PlayerSelfExclude.PlayerID = @PlayerID AND
                                           PlayerSelfExclude.Status IN (1,2,3)
                                    ORDER BY PlayerSelfExclude.ID DESC), '')

      IF @SelfExclude = '01 Jan 2050'
        SET @SelfExclude = 'indefinite'

      IF @SelfExclude <> ''
        BEGIN
          SET @Data = 'Skin:' + CAST(@SkinID AS VARCHAR(10)) + ' U:' + @Username + ' Self Exclusion:' + @SelfExclude
          SET @ReturnData = 5
          SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_SELF_EXCLUSION')
          SET @EventLogTypeID = 122
        END
    END

    --check for pending promo
    IF @Status = 1
      AND EXISTS ( SELECT TOP 1 1
                   FROM   Main..PlayerRegEmailValidation (NOLOCK)
                   WHERE  PlayerID = @PlayerID AND PromoID <> 0 AND Status = 1 )
      SET @Status = 4

    IF @Status = 1
      SET @Data = ''
    IF @Status = 3
      SET @Data = 'Registration is pending email validation'
    IF @Status = 4
      SET @Data = 'Promo(s) are pending email validation'
  END

DECLARE @CustomerID INT = (SELECT ID FROM Cashier..Customer WITH (NOLOCK) WHERE SkinCustomerID = @PlayerID)
UPDATE Main..TxPlayerBally WITH (ROWLOCK) SET Status = 0 WHERE PlayerID = @PlayerID
UPDATE Cashier..WithdrawReverseSession SET Status = 0 WHERE CustomerID = @CustomerID

--Make new session if a valid login and NOT pending email validation
IF @ReturnData = 0 AND @Status IN ( 1, 4 )
  BEGIN
    -- find the ID of the device used
    DECLARE @DeviceID INT
    DECLARE @DeviceCategoryInTableID INT

    SELECT  @DeviceID = ID, @DeviceCategoryInTableID = DeviceCategoryID FROM Main..Device WITH ( NOLOCK ) WHERE DeviceString = @UserAgent

    IF @DeviceID IS NULL
      BEGIN
        INSERT  Main..Device ( DeviceCategoryID, DeviceString ) VALUES ( @DeviceCategoryID, @UserAgent )
        SET @DeviceID = SCOPE_IDENTITY()
      END

    IF @DeviceCategoryInTableID <> @DeviceCategoryID
      BEGIN
        UPDATE Main..Device SET DeviceCategoryID = @DeviceCategoryID WHERE DeviceString = @UserAgent
      END

    --Update LoginSession with Status=0 and make new loginsession
    DECLARE @LastLoginDate DATETIME2(0) = NULL --as will be reported on player web page
    DECLARE @LastActivity DATETIME2
    DECLARE @LoginSessionID BIGINT = NULL
    SELECT TOP 1
            @LoginSessionID = ID,
            @LastLoginDate = SessionStart,
            @LastActivity = LastActivity
    FROM    Main..LoginSession WITH ( NOLOCK )
    WHERE   PlayerID = @PlayerID AND Status = 1
    ORDER BY ID DESC

    --Update Rep.LoginSession with last login data
    IF @LoginSessionID IS NOT NULL
      BEGIN
        UPDATE  Main..LoginSession SET [Status] = 0 WHERE ID = @LoginSessionID
        UPDATE  Rep..LoginSession WITH ( ROWLOCK ) SET LastActivity = @LastActivity WHERE ID = @LoginSessionID
      END

    IF @LoginSessionID IS NULL
      BEGIN
        SELECT TOP 1
                @LoginSessionID = ID,
                @LastLoginDate = SessionStart,
                @LastActivity = LastActivity
        FROM    Rep..LoginSession WITH ( NOLOCK )
        WHERE   PlayerID = @PlayerID
        ORDER BY ID DESC
      END

    IF @LoginSessionID IS NULL
      BEGIN
        SET @LastLoginDate = @Now
        SET @LastActivity = @Now
      END

    --Update Players last login date, and get the username (in case the username passed in is an email)
    UPDATE  Main..Player WITH ( ROWLOCK )
    SET     LastLoginDate = @Now,
            LoginCount += 1,
            LastLoginIP=@IP,
            @Username = Username,
            @PlayerClassID = PlayerClassID,
            @PlayerFunded = CASE WHEN Player.PlayerStage >= 50 THEN 1 ELSE 0 END
    WHERE   Player.ID = @PlayerID

    --DWH Code for Table Modifications
    EXEC Daga..dwhMods 'Main', 'Player', @PlayerID

    UPDATE  Rep..Player WITH ( ROWLOCK )
    SET     LastLoginDate = @Now,
            LastLoginIP=@IP,
            LoginCount += 1
    WHERE   Player.ID = @PlayerID

    SET @SessionID = NEWID()

    INSERT  Main..LoginSession
            ( PlayerID,
              SessionID,
              GroupID,
              Username,
              SkinID,
              IP,
              SessionStart,
              LastActivity,
              SelfExclude,
              DeviceID,
              ScreenSize
            )
    VALUES  ( @PlayerID,
              @SessionID,
              @GroupID,
              @Username,
              @SkinID,
              @IP,
              @Now,
              @Now,
              @SelfExclude,
              @DeviceID,
              @ScreenSize
            )
    SET @LoginSessionID = SCOPE_IDENTITY()

/*
Record the new login to Rep
Updates to Rep..LoginSession associated rec with the LastActivity and SelfExclude will occur from
- svrLoginSessionExpire
- svrCheckPlayerLoginSession
- WebLogout
- WebPlayerModify
- CogsFraudAlertModify
- CogsPlayerSelfExclude
*/
    INSERT  Rep..LoginSession
            ( ID,
              PlayerID,
              SessionID,
              GroupID,
              Username,
              SkinID,
              IP,
              SessionStart,
              LastActivity,
              SelfExclude,
              DeviceID
            )
    VALUES  ( @LoginSessionID,
              @PlayerID,
              @SessionID,
              @GroupID,
              @Username,
              @SkinID,
              @IP,
              @Now,
              @Now,
              @SelfExclude,
              @DeviceID
            )
  END

IF @ReturnData = 0 --good login
  BEGIN
    --Refresh PlayerLimits for the session
    UPDATE  Main..PlayerLimit WITH ( ROWLOCK )
      SET   WagerThisSession = 0,
            LossThisSession = 0
    WHERE   PlayerID = @PlayerID

    --Expire promos/warn of expiring promos
    DECLARE @TotalBonusLost MONEY
    DECLARE @TotalExpiring MONEY

    EXEC Main..svrVoidExpiredPromo @PlayerID, @TotalBonusLost OUTPUT, @TotalExpiring OUTPUT

    --Competition engine triggers related to log ins
    --EXEC Main..svrCompetitionTrigger 3, @PlayerID, 0
    EXEC Main..svrCompRegisterAction @PlayerID, 1, @PlayerClassID, @PlayerFunded, @SkinID, @SessionID, NULL, NULL, NULL

    DECLARE @Free5DaysSpins INT
  END

GOTO WriteLog

WriteLog:
IF @EventLogTypeID = 106
  SET @Data = @Data + ' Status changed to Multiple Invalid Login attempts'
IF @EventLogTypeID = 107
  SET @Data = @Data
    + ' Status changed to Disabled as there were 5+ invalid login attempts in a day'

SET @PlayerID = ISNULL(@PlayerID, 0)

EXEC Main..svrEventLog @EventLogTypeID, @PlayerID, @IP, @Data

ReturnData:

IF @ReturnData = 0
  BEGIN
    DECLARE @prizeID INT = 0,@PrizeTypeIDForDailySpins INT=0
    DECLARE @LastFundDate DATETIME2(0) = '2000-01-01'
    DECLARE @FirstFundDate DATETIME2(0) = '2000-01-01'

    SELECT  @LastFundDate = LastFundedDate,
            @FirstFundDate = FirstFundedDate,
            @DepositCount = DepositCount
      FROM  Rep..PlayerSummary WITH (NOLOCK)
      WHERE PlayerID = @PlayerID

    DECLARE @msg2 VARCHAR(500)


		-- DEV-7800 
		-- We check whether we have any ungiven daily prizes for the player	
		
				SET  @LastFundDate = '2016-12-16 09:45:40'
		DECLARE @PrizesToBeGiven TABLE ([ID] INT,[PlayerID] INT ,[PlayerClassID] INT ,[NumPrizes] INT ,[PrizeID] INT ,[PlayerClassPropertyID] INT ,[StartDate] DATETIME2(7),[EndDate] DATETIME2(7), [PlayerClassPropertyInstanceID] INT)
		
		INSERT INTO @PrizesToBeGiven 
		SELECT [ID]
      ,[PlayerID]
      ,[PlayerClassID]
      ,[NumPrizes]
      ,[PrizeID]
      ,[PlayerClassPropertyID]
      ,[StartDate]
      ,[EndDate]
      ,[PlayerClassPropertyInstanceID]
		FROM Main.dbo.PlayerDailyPrizes pdp (NOLOCK)
		WHERE pdp.PlayerID = @PlayerID 
		AND ((@Now BETWEEN pdp.StartDate AND pdp.EndDate) OR (pdp.StartDate IS NULL AND pdp.EndDate IS NULL))
		AND NOT EXISTS(select top 1 * FROM Main.dbo.PrizePlayer pp (NOLOCK) WHERE pdp.ID = pp.PlayerDailyPrizeID AND pdp.PlayerID = @PlayerID AND DATEDIFF( DAY, pp.AwardDate, @Now) = 0 )
		AND (pdp.PlayerClassPropertyID = 43 OR (pdp.PlayerClassPropertyID IN (10,11) AND @LastFundDate BETWEEN @WeekStart AND @now ))
		
		INSERT INTO @PrizesToBeGiven
		SELECT [ID]
      ,[PlayerID]
      ,[PlayerClassID]
      ,[NumPrizes]
      ,[PrizeID]
      ,[PlayerClassPropertyID]
      ,[StartDate]
      ,[EndDate]
      ,[PlayerClassPropertyInstanceID]
		FROM Main.dbo.PlayerDailyPrizes pdp (NOLOCK)
		WHERE pdp.PlayerClassID = @PlayerClassID 
		AND ((@Now BETWEEN pdp.StartDate AND pdp.EndDate) OR (pdp.StartDate IS NULL AND pdp.EndDate IS NULL))
		AND NOT EXISTS(SELECT top 1 * FROM @PrizesToBeGiven pg WHERE pg.PlayerClassPropertyInstanceID = pdp.PlayerClassPropertyInstanceID)
		AND NOT EXISTS(select top 1 * FROM Main.dbo.PrizePlayer pp (NOLOCK) WHERE pdp.ID = pp.PlayerDailyPrizeID AND pdp.PlayerID = @PlayerID AND DATEDIFF( DAY, pp.AwardDate, @Now) = 0 )
		AND (pdp.PlayerClassPropertyID = 43 OR (pdp.PlayerClassPropertyID IN (10,11) AND @LastFundDate BETWEEN @WeekStart AND @now ))
		
		-- here we will iterate through #PrizesToBeGiven and will give execute the proc [svrGivePrizeByID] to give each of the prizes.
		-- this proc will add it to the PrizePlayer table. So it also needs which Id from the PlayerDailyPrize table was the one giving the prize
		
		-- Declare 
		DECLARE @GivenPlayerDailyPrizeID INT = 0, @GivenPrizeID INT, @GivenAmount INT, @GivenPlayerClassPropertyInstanceID INT, @OldID INT = -1 
	
	
		
		IF (EXISTS( SELECT TOP 1 * FROM @PrizesToBeGiven) )
		BEGIN
				SELECT  TOP 1 @GivenPlayerDailyPrizeID = ptb.ID, @GivenAmount =  ptb.NumPrizes, 
								@GivenPrizeID = ptb.PrizeID, @GivenPlayerClassPropertyInstanceID = ptb.ID
				FROM @PrizesToBeGiven ptb
				WHERE ID > @GivenPlayerDailyPrizeID 
				ORDER BY ID asc
						
		-- Iterate over all players
			WHILE (@GivenPlayerDailyPrizeID > @OldID) 
			BEGIN  

				-- call your give prizes
				EXEC Main.dbo.svrGivePrizebyID @PlayerID, @GivenAmount, @GivenPrizeID, @GivenPlayerClassPropertyInstanceID

				SELECT @PrizeTypeIDForDailySpins=Prize.PrizeTypeID FROM Main..Prize WITH (NOLOCK) WHERE ID=@prizeID
				IF (@PrizeTypeIDForDailySpins = 21)
				BEGIN
					DECLARE @ScheduleMessageTableForDailySpins udt.ScheduleMessageTableType
					INSERT @ScheduleMessageTableForDailySpins SELECT @PlayerID ,'','','',''

					IF (@SkinID = 1)
						BEGIN
							EXEC Responsive..CogsScheduleMessage
								@ScheduleMessageTableForDailySpins,
								'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
								'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
								'1.1.1.1',
								@SkinID,
								183,
								@Now,
								60,
								0,
								1
						END
					IF (@SkinID = 3)
						BEGIN
							EXEC Responsive..CogsScheduleMessage
							@ScheduleMessageTableForDailySpins,
							'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
							'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
							'1.1.1.1',
							@SkinID,
							51,
							@Now,
							60,
							0,
							1
						END
				END
	      
	      				
				SET @OldID = @GivenPlayerDailyPrizeID
			-- Get next prize
				SELECT  TOP 1 @GivenPlayerDailyPrizeID = ptb.ID, @GivenAmount =  ptb.NumPrizes, 
								@GivenPrizeID = ptb.PrizeID, @GivenPlayerClassPropertyInstanceID = ptb.ID
				FROM @PrizesToBeGiven ptb
				WHERE ID > @GivenPlayerDailyPrizeID 
				ORDER BY ID asc

			END
			
		END
		-- END DEV-7800
		
		
    ---- give daily free spins if there are some available for the class or player.
    ---- and they have not already received them today
    --SET @FreeSpins = Main.fn.PlayerClassPropertyValue(@PlayerID, 10)
    --IF @FreeSpins > 0
    --  AND NOT EXISTS ( SELECT TOP 1 1
    --                   FROM   Main..EventLog WITH (NOLOCK)
    --                   WHERE  Main.$PARTITION.Fn_PartitionByDateTime(date) >= 6 AND EventLogTypeID = 115 AND EntityID = @PlayerID )
    --  AND @LastFundDate BETWEEN @WeekStart AND @now
    --  BEGIN
    --    -- give them spins and write a log
    --    SET @prizeID = Main.fn.PlayerClassPropertyExtraValue(@PlayerID, 10)
    --    SELECT @PrizeTypeIDForDailySpins=Prize.PrizeTypeID FROM Main..Prize WITH (NOLOCK) WHERE ID=@prizeID

    --    SET @msg2 = CAST(@FreeSpins AS VARCHAR(10)) + ' Daily Free Spins Given'
    --    EXEC Main..svrGiveBonusSpins @PlayerID, '', @FreeSpins, @msg2, NULL, NULL, NULL, 0, 0, @prizeID
    --    EXEC Main..svrEventLog 115, @PlayerID, '', @msg2

    --    IF (@PrizeTypeIDForDailySpins = 21)
    --    BEGIN
    --      DECLARE @ScheduleMessageTableForDailySpins udt.ScheduleMessageTableType
    --      INSERT @ScheduleMessageTableForDailySpins SELECT @PlayerID ,'','','',''

    --      IF (@SkinID = 1)
    --        BEGIN
    --          EXEC Responsive..CogsScheduleMessage
    --            @ScheduleMessageTableForDailySpins,
    --            'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
    --            'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
    --            '1.1.1.1',
    --            @SkinID,
    --            183,
    --            @Now,
    --            60,
    --            0,
    --            1
    --        END
    --      IF (@SkinID = 3)
    --        BEGIN
    --          EXEC Responsive..CogsScheduleMessage
    --          @ScheduleMessageTableForDailySpins,
    --          'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
    --          'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
    --          '1.1.1.1',
    --          @SkinID,
    --          51,
    --          @Now,
    --          60,
    --          0,
    --          1
    --        END
    --    END
    --  END

    ---- daily free crads
    --SET @FreeCards = Main.fn.PlayerClassPropertyValue(@PlayerID, 11)
    --IF @FreeCards > 0
    --  AND NOT EXISTS ( SELECT TOP 1 1
    --                   FROM   Main..EventLog WITH (NOLOCK)
    --                   WHERE  Main.$PARTITION.Fn_PartitionByDateTime(date) >= 6 AND EventLogTypeID = 116 AND EntityID = @PlayerID )
    --  AND @LastFundDate BETWEEN @WeekStart AND @now
    --  BEGIN
    --    -- give them cards and write a log
    --    SET @prizeID = Main.fn.PlayerClassPropertyExtraValue(@PlayerID, 11)
    --    SET @msg2 = CAST(@FreeCards AS VARCHAR(10)) + ' Daily Free Cards Given'
    --    EXEC Bingo..svrGiveFreeCards @PlayerID, @SessionID, '1.1.1.1', @FreeCards, NULL, 3, 20, 0, NULL, @prizeID
    --    EXEC Main..svrEventLog 116, @PlayerID, '', @msg2
    --  END

    ----[DEV-5581]- Free spins for X days from deposit opted for a specic promo based on player class property Id 43 starting the day after
    ----Get the player class ID of the player
    --SELECT @PlayerClassID = PlayerClassID FROM Main..Player WITH (NOLOCK) WHERE ID = @PlayerID
    --DECLARE @PropertyID INT=43, @DailyXDaySpins INT=0, @PrizeTypeID INT=0
    --DECLARE @PlayerClassValues TABLE (Value INT, PromoID INT)
    --DECLARE @PlayerKey INT = (SELECT PlayerKey FROM VM3VL40DB02.DWH.dbo.Dim_Player WITH(NOLOCK) WHERE PlayerID=@PlayerID)

    ----MultipleValues=1 for propertyId=43
    ----Get the instance records for player class
    --INSERT INTO @PlayerClassValues
    --  SELECT pcpi.Value, pcpi.ExtraValue
    --    FROM Main..PlayerClassPropertyInstance pcpi WITH (NOLOCK)
    --         WHERE (pcpi.PlayerClassID = @PlayerClassID AND PlayerClassPropertyID = @PropertyID)
    --               AND ((@Now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL))

    ----If there are instance records set for Player then merge the records with the records from Player class
    --IF (EXISTS(SELECT TOP 1 1 FROM Main..PlayerClassPropertyInstance pcpi WITH (NOLOCK)
    --          WHERE (PlayerID = @PlayerID AND PlayerClassPropertyID = @PropertyID + 1000)
    --          AND ((@Now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL))))
    --BEGIN
    --    SELECT
    --      'Value'=pcpi.Value,
    --      'PromoID'=pcpi.ExtraValue
    --    INTO #PlayerSource
    --    FROM Main..PlayerClassPropertyInstance pcpi WITH (NOLOCK)
    --          WHERE (PlayerID = @PlayerID AND PlayerClassPropertyID = @PropertyID + 1000)
    --          AND ((@Now BETWEEN pcpi.FromDate AND pcpi.ToDate) OR (pcpi.FromDate IS NULL AND pcpi.ToDate IS NULL))

    --    MERGE @PlayerClassValues AS TRGT
    --    USING #PlayerSource AS SRC
    --    ON
    --    (
    --      TRGT.Value = SRC.Value AND
    --      TRGT.PromoID = SRC.PromoID
    --    )
    --    WHEN MATCHED
    --    THEN UPDATE SET Value=SRC.Value
    --    WHEN NOT MATCHED THEN
    --    INSERT (Value, PromoID) VALUES (SRC.Value, SRC.PromoID);
    --    IF OBJECT_ID('tempdb..#PlayerSource') IS NOT NULL DROP TABLE #PlayerSource;
    --END

    --DECLARE @DailySpinNoOfDays INT
    --DECLARE @DailySpinPromoID INT
    --SELECT TOP 1 @DailySpinNoOfDays=Value, @DailySpinPromoID=PromoID FROM @PlayerClassValues

    ----If there is already an event for today the player will not be checked for the daily spins in the subsequent logins on the same day
    --IF (NOT EXISTS(SELECT TOP 1 1 FROM Main..EventLog WITH (NOLOCK) WHERE Main.$PARTITION.Fn_PartitionByDateTime(date) >= 6 AND EventLogTypeID = 119 AND EntityID = @PlayerID))
    --BEGIN
    -- WHILE @DailySpinPromoID IS NOT NULL
    --    BEGIN
    --      SET @DailySpinNoOfDays=0
    --      SET @DailySpinPromoID=NULL

    --      --Get the number of days the free spin has to be given for a specific promo
    --      SELECT TOP 1 @DailySpinNoOfDays=Value, @DailySpinPromoID=PromoID FROM @PlayerClassValues

    --      --If there is a transaction for the above specific promo in the last X days give spins.
    --      --The check is made both on Main and DWH trans tables to make sure we are not missing any records in the process
    --      IF ( EXISTS(SELECT ID FROM Main..TxPromo WITH (NOLOCK)
    --                   WHERE PromoID = @DailySpinPromoID AND TxPromo.PlayerID = @PlayerID
    --                         AND TxPromo.Date > (DATEADD(DAY,-ISNULL(@DailySpinNoOfDays,0),@today))
    --                         AND CAST(TxPromo.Date AS DATE) <> @today)
    --         OR
    --         EXISTS( (SELECT PlayerKey FROM VM3VL40DB02.DWH.dbo.Fact_PromotionTransaction t WITH(NOLOCK)
    --           JOIN VM3VL40DB02.DWH.dbo.Dim_Promotion p WITH(NOLOCK) ON t.PromoKey=p.PromoKey
    --           WHERE t.PlayerKey = @PlayerKey AND p.PromoID = @DailySpinPromoID
    --                 AND t.DateKey>= CONVERT(VARCHAR(8),(DATEADD(DAY,-ISNULL(@DailySpinNoOfDays,0),@today)),112)
    --                 AND t.DateKey <> CONVERT(VARCHAR(8),@today,112))
    --           )
    --        )
    --        BEGIN
    --          SET @DailyXDaySpins=0
    --          SET @prizeID=0

    --          -- give them spins and write a log
    --          SELECT @prizeID=PrizeID, @DailyXDaySpins=PrizeAmount, @PrizeTypeID=pr.PrizeTypeID FROM Main..Promo p WITH (NOLOCK)
    --            JOIN Main..Prize pr WITH (NOLOCK) ON p.PrizeID = pr.ID
    --            WHERE p.ID = @DailySpinPromoID AND pr.PrizeTypeID IN (15, 21)

    --          SET @msg = CAST(@DailyXDaySpins AS VARCHAR(10)) + ' Spins Daily '+ CAST(@DailySpinNoOfDays AS VARCHAR(10)) + ' days from Deposit for the promo ID '+ CAST(@DailySpinPromoID AS VARCHAR(15))

    --          IF(ISNULL(@DailyXDaySpins,0) > 0)
    --          BEGIN
    --            EXEC Main..svrGiveBonusSpins @PlayerID, '', @DailyXDaySpins, @msg, NULL, NULL, NULL, 0, 0, @prizeID
    --            EXEC Main..svrEventLog 119, @PlayerID, '', @msg

    --            IF(@PrizeTypeID=21)
    --            BEGIN
    --              DECLARE @ScheduleMessageTable udt.ScheduleMessageTableType

    --              INSERT @ScheduleMessageTable SELECT @PlayerID ,'','','',''

    --              IF (@SkinID = 1)
    --              BEGIN
    --                EXEC Responsive..CogsScheduleMessage
    --                     @ScheduleMessageTable,
    --                     'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
    --                     'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
    --                     '1.1.1.1',
    --                     @SkinID,
    --                     183,
    --                     @Now,
    --                     60,
    --                     0,
    --                     1
    --              END
                  
    --              IF (@SkinID = 3)
    --              BEGIN
    --                EXEC Responsive..CogsScheduleMessage
    --                     @ScheduleMessageTable,
    --                     'F0EF8B4A-42F5-4DB4-B3F4-348394E0F504',
    --                     'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb',
    --                     '1.1.1.1',
    --                     @SkinID,
    --                     51,
    --                     @Now,
    --                     60,
    --                     0,
    --                     1
    --              END
    --            END
    --          END
    --        END
          
    --      DELETE @PlayerClassValues WHERE Value = @DailySpinNoOfDays AND PromoID=@DailySpinPromoID

    --    END
    --END

    --Check has accepted t+c
    IF @ReturnData = 0
      AND @Status IN ( 1, 4 )
      AND NOT EXISTS ( SELECT TOP 1 1 FROM  Main..PlayerTandC WITH ( NOLOCK ) WHERE  PlayerID = @PlayerID )
      BEGIN
        SET @ReturnData = 9
        SET @msg = fn.GetEventMessage(@lang, 'LOGIN_FAIL_ACCEPT_T_AND_C')
      END

    -- is there an affiliate, we want to pass it back on login
    IF @SubCampaignID > 0
      BEGIN
        -- get the affiliate information
        SELECT  @AffID = c.AffiliateID, @DynamicVariable = sc.Name
          FROM stb_data..SubCampaigns sc ( NOLOCK )
               JOIN stb_data..Campaigns c ( NOLOCK ) ON sc.CampaignID = c.ID
          WHERE sc.deleted = 0
            AND c.Deleted = 0
            AND sc.ID = @SubCampaignID
      END

    --check pending deposit limit confirmations
    DECLARE @LimitConfirmation BIT = 0--If there is a pending increase that requires the customers confirmation to accept this will be 1
    DECLARE @LimitConfirmationMessage VARCHAR(100) = ''
    DECLARE @DepositLimitStatus TINYINT = NULL, @DateRequested DATETIME2 = NULL

    SELECT TOP 1 @DepositLimitStatus = CustomerLimitChange.[Status],
                 @DateRequested = CustomerLimitChange.DateRequested
      FROM Cashier..CustomerLimitChange WITH (NOLOCK)
      WHERE CustomerLimitChange.CustomerID = @CustomerID AND CustomerLimitChange.[Status] IN (2,3)

    IF @DepositLimitStatus = 3
      SET @LimitConfirmationMessage = 'Your requested deposit limit decreases will be applied at midnight'

    IF @DepositLimitStatus = 2
      BEGIN
        DECLARE @SecsToConfirmChange INT = DATEDIFF(SECOND,GETDATE(),DATEADD(DAY,1,@DateRequested))
        IF @SecsToConfirmChange > 0
          BEGIN
            DECLARE @hours INT   = (@SecsToConfirmChange/60/60)
            DECLARE @minutes INT = (@SecsToConfirmChange/60)-(@hours * 60)
            DECLARE @seconds INT =  (@SecsToConfirmChange) - (@hours * 3600) - (@minutes * 60)

            SET @LimitConfirmationMessage = 'Your requested deposit limit changes may be confirmed in '
            IF @hours > 0
              SET @LimitConfirmationMessage += CAST(@hours AS VARCHAR(2)) + ' hours '
            IF @minutes > 0 OR @seconds > 0
              SET @LimitConfirmationMessage += CAST(@minutes AS VARCHAR(2)) + ' minutes '
            IF @seconds > 0
              SET @LimitConfirmationMessage += CAST(@seconds AS VARCHAR(2)) + ' seconds '
          END
        ELSE
          BEGIN
            SET @LimitConfirmation = 1
            SET @LimitConfirmationMessage = 'Your requested deposit limit changes are awaiting your confirmation'
          END
      END

   -- DECLARE @ShowBar TINYINT = 1
   -- DECLARE @Version TINYINT = 1
   -- DECLARE @ShowModal TINYINT = 1


   -- SELECT @ShowModal = 0, @ShowBar = ShowBar, @Version = pt.[Version]
   -- FROM Main..Player (nolock) p
   -- LEFT JOIN Main..PlayersOldToRadTransition (NOLOCK) pt on p.ID = pt.PlayerID
   -- WHERE pt.PlayerID = @PlayerID

   -- IF @ShowModal = 1 AND @RegistrationDate < @NewLuckyStartDate
   -- BEGIN
      ---- IN CASE The player is an old player and they haven't seen the modal window, we record it
      --INSERT INTO Main..PlayersOldToRadTransition (PlayerID, ShowBar, Version) VALUES (@PlayerID, 1, 1)
   -- END
   -- ELSE IF @RegistrationDate > @NewLuckyStartDate
   -- BEGIN
      --SET @ShowModal = 0
      --SET @Version = 1
      --SET @ShowBar = 0
   -- END

    SELECT  'Code' = @ReturnData,
            'Msg' = @msg,
            'PlayerID' = Player.ID,
            'SessionID' = CASE WHEN @ReturnData = 0
                               THEN ISNULL(CAST(@SessionID AS VARCHAR(36)), '')
                               ELSE ''
                          END,
            'Username' = Player.Username,
            'Firstname' = Player.Firstname,
            'CurrencyCode' = Player.CurrencyCode,
            'CountryCode' = Player.CountryCode,
            'CountryCode2' = Country.Code2,
            'LanguageCode' = Player.LanguageCode,
            'Balance' = Daga.fn.formatMoney(Player.Real + Player.Bonus + Player.BonusWins, 0),
            'Real' = Daga.fn.formatMoney(Player.Real, 0),
            'Bonus' = Daga.fn.formatMoney(Player.Bonus, 0),
            'BonusWins' = Daga.fn.formatMoney(Player.BonusWins, 0),
            'Points' = Player.Points,
            'Funded' = CASE WHEN PlayerStage = 50 THEN 1
                            ELSE 0
                       END,
            'Class'= PlayerClass.DisplayClassID,
            'LastLoginDate' = Daga.fn.formatDate(@LastLoginDate, '', 'hh:mm:ss'),
            'CurrencySymbol' = @CurrencySymbol,
            'CentsSymbol' = @CentsSymbol,
            'DepositCount' = ISNULL(@DepositCount, 0),
            'MobileVerified' = Player.MobileNumberVerified,
            'RegistrationDate' = Player.Date,
            'LimitConfirmation' = @LimitConfirmation,
            'LimitConfirmationMessage' = @LimitConfirmationMessage,
            'RedirectURL' = @RedirectURL,
            'AffID' = @SubCampaignID,
            'AffiliateID' = @AffID,
            'DynamicVariable' = @DynamicVariable
            --,
            --'Version' = CASE WHEN @today > @NewLuckyEndDate THEN 1 ELSE @Version END, -- if the switch deadline has expired we'll also display the new site. If there's no version we display the new site by default
            --'ShowModal' = CASE WHEN [Date] < @NewLuckyStartDate AND @Version = 0 THEN 1 ELSE 0 END,
            --'ShowBar' = @ShowBar

        /*
        'PromosExpired' = daga.fn.formatMoney(@TotalBonusLost, 0),
        'PromosExpiring' = daga.fn.formatMoney(@TotalExpiring, 0),
        'Status' = @Status,
        'Favourites' = ISNULL(@Favourites, ''),
        'PlayerTypeID' = Player.PlayerTypeID,
        'UserData' = Main.fn.GetUserDataJSON(@PlayerID, 'LOGIN'),
        'Email' = Player.Email
        */
    FROM    Main..Player WITH ( NOLOCK )
            JOIN Main..PlayerClass WITH ( NOLOCK ) ON PlayerClass.ID = Player.PlayerClassID
            JOIN Main..Country WITH ( NOLOCK ) ON Country.Code = Player.CountryCode
    WHERE   Player.ID = @PlayerID

    -- send favourites
    SELECT * FROM fn.GetFavouriteGames(@PlayerID, @DeviceCategoryID)

		-- Gaming Data
			IF Daga.fn.isThis('Server',NULL) = 'LIVE'
			  BEGIN
				  SELECT * FROM VM3VL40DB02.GamingData.dbo.Maxymiser_DaubK_players_data WHERE Max_Playerid = @PlayerID			
			  END
			ELSE
			  BEGIN
			    -- dummy data for dev
			    SELECT 1 [Max_skinid], 1234 [Max_SubCampaignId], 1 [Max_Affiliateid], 1 [Max_VipLevel], 5 [Max_DaysSinceLastDeposit], 10.50 [Max_AverageDepPerday], 1056.87 [Max_AverageWagPerday], 1.5 [Max_depositcount], 30.456 [Max_NetCash], 123 [Max_PreferredGame], 167.78 [Max_LastBalance], 22.56 [Max_NGR]
        END
  END

IF @ReturnData IN ( 1, 2, 4, 5, 6, 11 ) -- bad login or account locked or self excluded
  BEGIN
    SELECT  'Code' = @ReturnData,
            'Msg' = @msg,
            'SelfExclude' = CASE @SelfExclude
                              WHEN '' THEN ''
                              WHEN 'indefinite' THEN 'Self Excluded Indefinitely'
                              WHEN 'PendingLift' THEN 'Please call or email support to lift the self exclusion'
                              ELSE 'Self Excluded until ' + @SelfExclude
                            END,
            'RedirectURL' = @RedirectURL
  END

IF @ReturnData = 3 --account locked because of bad password attempts
  BEGIN
    SELECT  'Code' = 3,
            'Msg' = @msg,
            'PlayerID' = Player.ID,
            'Firstname' = Player.Firstname,
            'Email' = Player.Email,
            'Username' = Player.Username,
            'ReactivationGUID' = CAST(@SessionID AS VARCHAR(36)),
            'RedirectURL' = @RedirectURL
    FROM    Main..Player (NOLOCK)
    WHERE   Player.ID = @PlayerID
  END
  
RETURN
--#--
END
