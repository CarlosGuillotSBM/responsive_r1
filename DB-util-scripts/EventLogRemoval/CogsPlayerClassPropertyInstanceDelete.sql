USE [Main]
GO
/****** Object:  StoredProcedure [dbo].[CogsPlayerClassPropertyInstanceDelete]    Script Date: 12/15/2016 11:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
Author:    SR
Date       : 23Apr13
Mod date(s):

Description: deletes the player class property instance
Called From: cogs

UPDATE:
CM - 15dec16 - DEV-7800 We'll delete also the DailyPrizes that will be given due to this ClassProperty 

DROP PROCEDURE [dbo].[CogsPlayerClassPropertyInstanceDelete]
Comments:
=============================================*/

ALTER PROCEDURE [dbo].[CogsPlayerClassPropertyInstanceDelete]
@CogsUserID UNIQUEIDENTIFIER,
@SessionID UNIQUEIDENTIFIER,
@IP VARCHAR(15),
@SkinIDs VARCHAR(1000),
@ID INT
AS

--Session/Skin validation------------------------------
DECLARE @SkinTable udt.SkinTable --(Result int, SkinID int)
DECLARE @Caller VARCHAR(500) = HOST_NAME()+'.'+@@SERVERNAME+'.'+DB_NAME()+'.'+OBJECT_NAME(@@PROCID)
DECLARE @Result INT

INSERT @SkinTable
  EXECUTE Cogs.dbo.svrValidateCogsRequest
          @CogsUserID,@SessionID,@SkinIDs,@IP,@Caller

SET @Result = (SELECT TOP 1 Result FROM @SkinTable)
IF @Result != 0 --A validation error occurred. Cogs will have logged it
  BEGIN
    SELECT 'Result' = @Result
    RETURN @Result
  END
--Session/Skin validation------------------------------

DECLARE @DataDeleted VARCHAR(8000)
DECLARE @PlayerClassPropertyID INT, @PlayerClassID INT,@Value INT, @ExtraValue INT,@FromDate DATETIME, @ToDate DATETIME, @LockStatus TINYINT

SELECT @PlayerClassPropertyID=PlayerClassPropertyID,
     @PlayerClassID=PlayerClassID,
     @Value=Value,
     @ExtraValue=ExtraValue,
     @FromDate=FromDate,
     @ToDate=ToDate,
     @LockStatus =LockStatus
FROM PlayerClassPropertyInstance
WHERE ID=@ID

SET @DataDeleted = 'ID-'+CAST(@ID AS VARCHAR)+
           ', PlayerClassPropertyID-'+CAST(@PlayerClassPropertyID AS VARCHAR)+
           ', PlayerClassID-'+ISNULL(CAST(@PlayerClassID AS VARCHAR),'')+
           ', Value-'+CAST(@Value AS VARCHAR)+
           ', Extra Value-'+ISNULL(CAST(@ExtraValue AS VARCHAR),'')+
           ', From Date-'+ISNULL(CAST((CAST(@FromDate AS DATE)) AS VARCHAR),'')+
           ', To Date-'+ISNULL(CAST((CAST(@ToDate AS DATE)) AS VARCHAR),'')+
           ', Lock Status-'+ISNULL(CAST(@LockStatus AS VARCHAR),'')

DELETE FROM PlayerClassPropertyInstance WHERE ID=@ID;
-- DEV-7800 We'll delete also the DailyPrizes that will be given due to this ClassProperty 
DELETE FROM Main.dbo.PlayerDailyPrizes WHERE PlayerClassPropertyInstanceID = @ID

DECLARE @Details VARCHAR(8000)
SET @Details = 'Deleted Player Class Property Instance: ' + @DataDeleted

EXEC Cogs.dbo.svrEventLog 452, @CogsUserID, NULL, @ID, @IP, @Details
SELECT 'Result' = 0, 'Message' = 'Success'
