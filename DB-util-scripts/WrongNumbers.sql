UPDATE REp..Player
SET MobilePhone = aux.mobilePhone
FROM (select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '7%' and LEN(RTRIM(p.MobilePhone)) = 10 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '07%' and LEN(RTRIM(p.MobilePhone)) = 11 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '007%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID   
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '447%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '0447%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID   
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '4407%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from REp..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '00447%' and LEN(RTRIM(p.MobilePhone)) = 14 and p.CountryCode = 'GBR') aux
where aux.PlayerID = ID


UPDATE Main..Player
SET MobilePhone = aux.mobilePhone
FROM (select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '7%' and LEN(RTRIM(p.MobilePhone)) = 10 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '07%' and LEN(RTRIM(p.MobilePhone)) = 11 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '007%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID   
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '447%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '0447%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID   
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '4407%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select c.CallingCode+RIGHT(RTRIM(p.MobilePhone),10) as mobilePhone, p.ID as playerID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '00447%' and LEN(RTRIM(p.MobilePhone)) = 14 and p.CountryCode = 'GBR') aux
where aux.PlayerID = ID



select p2.ID, p2.Username, p2.SkinID, pc.Description as Class, st.DisplayDescription as Status, P2.SendSMS as 'sendSMS: 1 means yes, 0 no', p2.DoNotCall as 'do not call (1 means no call)', p2.MobilePhone
FROM Main..Player (nolock) p2 
JOIN Main..PlayerClass (nolock) pc on pc.ID = p2.PlayerClassID
JOIN Main..Status (nolock) st on st.Status = p2.Status 
--select p2.ID, p2.Username, p2.SkinID, p2.Status, p2.PlayerClassID, p2.SendSMS, p2.DoNotCall
--FROM Main..Player (nolock) p2 
where  p2.SkinID <> 2 and p2.CountryCode = 'GBR'
and st.TableName IN ('Player', '_default_')
and p2.ID NOT IN (select p.ID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '7%' and LEN(RTRIM(p.MobilePhone)) = 10 and p.CountryCode = 'GBR'
UNION
select p.ID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '07%' and LEN(RTRIM(p.MobilePhone)) = 11 and p.CountryCode = 'GBR'
UNION
select p.ID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
--where skinID <> 2 and p.MobilePhone like '007%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
--UNION
--select p.ID    
--from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
--where skinID <> 2 and p.MobilePhone like '447%' and LEN(RTRIM(p.MobilePhone)) = 12 and p.CountryCode = 'GBR'
UNION
select p.ID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '0447%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select p.ID   
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '4407%' and LEN(RTRIM(p.MobilePhone)) = 13 and p.CountryCode = 'GBR'
UNION
select p.ID  
from Main..Player (nolock) p JOIN Main..Country c (nolock) on c.Code = p.CountryCode
where skinID <> 2 and p.MobilePhone like '00447%' and LEN(RTRIM(p.MobilePhone)) = 14 and p.CountryCode = 'GBR')