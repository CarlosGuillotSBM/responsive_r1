DECLARE @homany TABLE (howmany Smallint)
		INSERT INTO @homany (howmany)
		SELECT [Count] FROM (
		MERGE Main..PlayerBadAttempts AS ba
		USING ( SELECT  5,
						102,
						'2017-01-27'
		) AS source (PlayerID,
					 EventLogType,
					 StartDate2)
		ON ba.PlayerID = source.PlayerID and ba.EventLogType = source.EventLogType
		WHEN MATCHED THEN
				UPDATE SET  
						ba.StartDate = source.StartDate2,
						ba.[Count] = CASE WHEN (source.StartDate2 = ba.StartDate) THEN ba.[Count] + 1 ELSE 1 END
		WHEN NOT MATCHED THEN
				INSERT
				VALUES (source.PlayerID,
					source.EventLogType,
					1,
					source.StartDate2)
		OUTPUT inserted.[Count]) as m ([Count]);
		
		SELECT * from @homany
		
		DECLARE @today DATETIME2(7) = CAST(GETDATE() AS DATE)
		
		select * from Main..playerBadAttempts where StartDate = @today