DECLARE @SkinID INT = 9


DECLARE @CC Table (ContentCategoryID INT, Contents NVARCHAR(200))

INSERT INTO @CC
select DISTINCT cc.ID,    substring(
        (
            Select ','+CAST(CT.ID AS NVARCHAR)  AS [text()]
            From Responsive..Content CT
            Where CT.ContentCategoryID = cc.ID
            For XML PATH ('')
        ), 2, 1000) [Content] 
from Responsive..Content (nolock) c
Join Responsive..ContentCategory (nolock) cc on c.ContentCategoryID = cc.ID 
where cc.SkinID = @SkinID and cc.LiveID IS NOT NULL 

AND 0 = (
select COUNT(*)
from Responsive..Content (nolock) c2
Join Responsive..ContentCategory (nolock) cc2 on c2.ContentCategoryID = cc2.ID 
where cc2.LiveID IS NULL and cc.LiveID = cc2.ID
)
select 'EXEC Responsive.[dbo].[AdminSetContentLive] ''' + CAST(ContentCategoryID AS NVARCHAR(200)) + ''', '''',321,''f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb'', ''0.0.0.0'',''' + Contents+''''

FROM @CC