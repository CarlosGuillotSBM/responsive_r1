OIFS="$IFS"
IFS=$'\n'
PASS=true

for f in $(find . -name "$1" -type f -not -path "*/node_modules/*" -print)
do if [ ! -s $f ]; then
 echo "$f is empty"
 PASS=false
fi; done

if $PASS; then
 exit 0
else
 exit 1
fi;
