#! Groovy
def timestamp = ''
def deployPath = ''
def copyPath = ''
def DOMAIN = ''
def SERVER = ''
def SFTP_SERVER='192.168.50.50'
//prod sftp server 192.168.50.50
//dev sftp server 172.16.210.172

def deploySite(def USERNAME, def PASSWORD, def DOMAIN, def SFTP_SERVER, def SERVER, def deployPath, def copyPath){

    sh "sshpass -p $PASSWORD scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null responsive.zip ${DOMAIN}${USERNAME}@${SFTP_SERVER}:/${copyPath}/"
    sh """
       sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DOMAIN}${USERNAME}@${SFTP_SERVER} '"C:\\Program Files\\7-Zip\\7z.exe"' x "\\\\\\\\${SERVER}\\\\${deployPath}\\\\responsive.zip" "-o\\\\\\\\${SERVER}\\\\${deployPath} -y"
       """
    sh """
       sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DOMAIN}${USERNAME}@${SFTP_SERVER} del \\\\\\\\${SERVER}\\\\${deployPath}\\\\responsive.zip
       """
    sh "sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DOMAIN}${USERNAME}@${SFTP_SERVER} robocopy /e /is /it /move /NFL /NDL /NJH /nc /ns /np \\\\\\\\${SERVER}\\\\${deployPath}\\\\responsive\\\\application \\\\\\\\${SERVER}\\\\${deployPath}\\\\application || exit 0"
    sh "sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DOMAIN}${USERNAME}@${SFTP_SERVER} robocopy /e /is /it /move /NFL /NDL /NJH /nc /ns /np \\\\\\\\${SERVER}\\\\${deployPath}\\\\responsive\\\\public \\\\\\\\${SERVER}\\\\${deployPath}\\\\public || exit 0"
    sh "sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DOMAIN}${USERNAME}@${SFTP_SERVER} robocopy /e /is /it /move /NFL /NDL /NJH /nc /ns /np \\\\\\\\${SERVER}\\\\${deployPath}\\\\responsive\\\\vendor \\\\\\\\${SERVER}\\\\${deployPath}\\\\vendor || exit 0"
    sh "sshpass -p $PASSWORD ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${DOMAIN}${USERNAME}@${SFTP_SERVER} rmdir /s /q \\\\\\\\${SERVER}\\\\${deployPath}\\\\responsive"
}

node('nodejs'){
  properties ([
    buildDiscarder(logRotator(artifactNumToKeepStr: '3',daysToKeepStr: '14')),
    disableConcurrentBuilds()
  ])

  stage('SCM checkout') {
    deleteDir()
    checkout([$class: 'GitSCM', branches: [[name: "*/$BRANCH_NAME"]],
                extensions: [[$class: 'CloneOption', timeout: 60],[$class: 'SparseCheckoutPaths',  sparseCheckoutPaths:[[$class:'SparseCheckoutPath', path:'!responsive/landing-pages'],[$class:'SparseCheckoutPath', path:'/*']]]], gitTool: 'Default',
                userRemoteConfigs: [[credentialsId: 'bitbucket-key', url: 'git@bitbucket.org:spacebarmediadev/responsive.git']]
            ])
  }
      stage('Build') {
        parallel sass:{
              retry(3) {
              dir('responsive/_sass-compass-foundation/') {

                  sh 'npm install'
                  sh 'bower install'
                  sh 'npm link gulp'
                  sh 'gulp compile'

              }
              sh 'chmod 777 empty_files_test.sh'
              sh './empty_files_test.sh main.css'
              }
          }, javascipt:{
            retry(3) {
              dir('responsive/public/skins/_global-library/js/') {
                sh 'npm set strict-ssl false'
                sh 'npm install'
                script {
                  def buildTimestamp = env.BUILD_TIMESTAMP
                  timestamp = buildTimestamp.replace(" ", "-")
                }
                sh "grunt javascript css replacestr --pv=${timestamp} -v"
              }
              sh 'chmod 777 empty_files_test.sh'
              sh './empty_files_test.sh main.css'
              }

          }

      }
      stage('Package') {

          sh 'zip --symlinks -r responsive.zip responsive/application responsive/vendor responsive/public -x *node_modules*'

      }
      stage('Deploy') {

            if(env.BRANCH_NAME == 'develop'){
              deployPath = 'StagingResponsive'
              env.RELEASE_SCOPE = input message: 'User input required', ok: 'Deploy!',
                            parameters: [choice(name: 'Deployment Server', choices: 'WEB05\nWEB03\nBoth', description: 'Where to deploy?')]
              if (env.RELEASE_SCOPE == 'WEB03') {
                withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                  deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}",'192.168.10.50',"${deployPath}",'StagingResponsive')
                }
              } else if (env.RELEASE_SCOPE == 'WEB05') {
                withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                  deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}",'192.168.10.40',"${deployPath}",'StagingResponsive_VM6VL10WEB05')
                }
              } else if (env.RELEASE_SCOPE == 'Both') {
                parallel server1: {
                  withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}",'192.168.10.50',"${deployPath}",'StagingResponsive')
                  }
                }, server2: {
                  withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}",'192.168.10.40',"${deployPath}",'StagingResponsive_VM6VL10WEB05')
                  }
                }
              }
            } else if (env.BRANCH_NAME == 'master'){
              deployPath = 'LiveResponsive'
              parallel server1: {
                withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                  deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}",'192.168.10.50',"${deployPath}",'LiveResponsive')
                }
              }, server2: {
                withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                  deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}",'192.168.10.40',"${deployPath}",'LiveResponsive_VM6VL10WEB05')
                }
              }
            } else if (env.BRANCH_NAME == 'testing'){
              env.RELEASE_SCOPE = input message: 'User input required', ok: 'Deploy!',
                            parameters: [choice(name: 'Deploy Path', choices: 'Testing\nStaging', description: 'Where to deploy?')]
              if (env.RELEASE_SCOPE == 'Testing') {
                deployPath = 'TestingResponsive'
                copyPath = 'TestingResponsive'
                SFTP_SERVER = '172.16.210.172'
                SERVER = '192.168.205.40'
                DOMAIN = "DGCDEV\\\\"
                withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                  deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}","${SERVER}","${deployPath}","${copyPath}")
                }
              } else {
                SERVER = '192.168.60.50'
                parallel server1: {
                  withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}","${SERVER}",'StagingResponsive','RADStaging_StagingResponsive')
                  }
                }, server2: {
                  withCredentials([usernamePassword(credentialsId: 'jenkins-sftp', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    deploySite("${USERNAME}","${PASSWORD}","${DOMAIN}","${SFTP_SERVER}","${SERVER}",'LiveResponsive','RADStaging_LiveResponsive')
                  }
                }
              }
            }

      }
      stage('Post actions') {
        archiveArtifacts artifacts: 'responsive.zip'
        deleteDir()
      }
}