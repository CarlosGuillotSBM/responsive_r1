<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$fileSource ='D:\ftp-update\files\out.xml';
$newLine = "\r\n";
echo ("starting" .$newLine);

$url = $argv[1];

$trunkDir = 'D:\WEB2\responsive\trunk';
$copyDir = 'D:\ftp-update\trunk';
$ftp_user_name = $argv[2];//DGCDEV\svc_bamboo_sftp
$ftp_user_pass = $argv[3];//'Uy2M<8290po3mjBWQ73';
$ftp_server = $argv[4];//'172.16.210.172';
$ftp_dir= $argv[5];//'/RAD';

$gruntFolder = $trunkDir.'\responsive\public\skins\_global-library\js';

date_default_timezone_set('UTC');

$svnUsername = $argv[6]; 
$svnPassword = $argv[7];
$svnauth = "--username " . $svnUsername . " --password " . $svnPassword; 

$configRoute = $copyDir . '\responsive\application\config';
$dbFile = $configRoute."db.php";
$dbPwd = $argv[8];
$configFile = $configRoute."config.php";
$version = $argv[9];


$svnTrunk = $url.$argv[10];//'/trunk/';
$goToLive = $svnTrunk;//$url.'/branches/goToLive/';

$nextTagDir = '/tags/'. $argv[11] . '/'; // live or debug
$nextTagName = $nextTagDir. $argv[11] .'-v_' . $version . date ( '-Y_m_d-H.i');
$bugFixingsBranch = '/branches/bugFixing';

$createTag = $argv[12]; // 0 = false, 1 = true

	$sftp = null;
try {

	chdir($trunkDir); 	
	echo ('svn switch '. $svnTrunk . " $svnauth" .$newLine);
	shell_exec ('svn switch '. $svnTrunk . " $svnauth");

	// find the last tag
	echo ("svn log -v --xml ". $url . $nextTagDir .  " --limit 4" . " $svnauth  > ".$fileSource .$newLine);
	shell_exec ("svn log -v --xml ". $url . $nextTagDir .  " --limit 10" . " $svnauth  > ".$fileSource);

    $xml = simplexml_load_file($fileSource);
	
    $files = $xml->xpath('//path');
	$currrentlyLive = $url . $files[4];
	
	
	
	if (file_exists ($copyDir)){
		echo ('rmdir /S /Q '.$copyDir);
		shell_exec ('rmdir /S /Q '.$copyDir);
	}
	
	//echo 'svn diff --summarize --xml --old='.$currrentlyLive.' --new='.$goToLive;
	echo ('svn diff --summarize --xml --old='.$currrentlyLive.' --new='.$goToLive. " $svnauth" .' > '.$fileSource .$newLine);
	shell_exec ('svn diff --summarize --xml --old='.$currrentlyLive.' --new='.$goToLive. " $svnauth" .' > '.$fileSource);


    $sftp = connect($ftp_server,$ftp_user_name, $ftp_user_pass);

	if (file_exists($fileSource)) {
		$xml = simplexml_load_file($fileSource);

		$files = $xml->xpath('//path[@kind="file" and @item!="deleted"]');
		foreach ($files as $file) {
			// exporting the files from svn
			if (!file_exists ($copyDir.dirname(str_replace($currrentlyLive,'',$file)))){
					shell_exec ('mkdir "'.$copyDir.dirname(str_replace($currrentlyLive,'',$file)).'"');
				}
			echo ('svn export '.str_replace($currrentlyLive,$goToLive,$file).' '.$copyDir."\\".str_replace($currrentlyLive,'',$file).$newLine);
			shell_exec ('svn export '.str_replace($currrentlyLive,$goToLive,$file).' '.$copyDir."\\".str_replace($currrentlyLive,'',$file));
		}
		
		// substitutions of the version number and the db password
		if (file_exists($dbFile)) {
			//read the entire string
			$str=file_get_contents($dbFile);

			//replace the db Password
			$str=str_replace('@@DbPwd', $dbPwd,$str);

			//write the entire string
			file_put_contents($dbFile, $str);
		}
			
		if (file_exists($configFile)) {
			//read the entire string
			$str=file_get_contents($configFile);

			//replace the version
			$str=str_replace('@@Version', $version, $str);

			//write the entire string
			file_put_contents($configFile, $str);
		}	
		
		foreach ($files as $file) {			
			// uploading the new and updated files via sftp
			if(!file_exists('ssh2.sftp://' . $sftp . $ftp_dir . dirname(str_replace($currrentlyLive,'',$file)))){	
				echo ('Creating folder: ' . dirname(str_replace($currrentlyLive,'',$file))."\r\n");
				mkdir('ssh2.sftp://' . $sftp . $ftp_dir . dirname(str_replace($currrentlyLive,'',$file)),0777,true);
			} 
			echo ('Adding file: from:' . $copyDir.str_replace($currrentlyLive,'',$file)." to: " . $ftp_dir .str_replace($currrentlyLive,'',$file)."\r\n");
			copy($copyDir.str_replace($currrentlyLive,'',$file),'ssh2.sftp://' . $sftp . $ftp_dir .str_replace($currrentlyLive,'',$file));
		}
		
	
		// adding empty files added
		$files = $xml->xpath('//path[@kind="dir" and @item="added"]');
		foreach ($files as $file) {
			// creating the new empty folder via sftp	
			mkdir('ssh2.sftp://' . $sftp . $ftp_dir .str_replace($currrentlyLive,'',$file),0777,true);
			
		}
		
		$files = $xml->xpath('//path[@kind="file" and @item="deleted"]'); 
		foreach ($files as $file) {
			echo "delete file ". $sftp . $ftp_dir .str_replace($currrentlyLive,'',$file)."\r\n";
			ssh2_sftp_unlink($sftp, $ftp_dir .str_replace($currrentlyLive,'',$file));
		}	
		
		// deleting empty folders via sftp	
		$files = $xml->xpath('//path[@kind="dir" and @item="deleted"]');
		foreach ($files as $file) {
			echo "delete folder ". $sftp . $ftp_dir .str_replace($currrentlyLive,'',$file);
			ssh2_sftp_rmdir($sftp, $ftp_dir .str_replace($currrentlyLive,'',$file));
			
		}	
		if ($createTag == 1) {
			chdir($trunkDir); 
			// we create a tag and a branch of goToLive 
			exec ('svn copy ' . $goToLive . ' ' . $url .  $nextTagName . ' --message "New release" '. " $svnauth"); 
			
			if ($argv[11] == 'live') {
				//we delete the previous bug Fixing branch so it's ready to be recreated
				exec ('svn del '. $url . $bugFixingsBranch . ' --message "deleting bug fixing branch to recreate it"'. " $svnauth");
				
				// we create the bug fixing branch so it's easier for developers and designers to bug fix any live errors
				exec ('svn copy ' . $goToLive . ' ' . $url . $bugFixingsBranch . ' --message "New bug fixing branch" '. " $svnauth"); 
			}
		}
	
	} else {
		exit('Failed to open test.xml.');
	}
	ssh2_exec($sftp, 'exit');
	unset($sftp);
} catch (Exception $e) {
	var_dump($e);
    echo 'Caught exception: ', $e->getMessage(), "\n";
	ssh2_exec($sftp, 'exit');
	unset($sftp);
	exit(255); // error
}

	/**
	* Returns a sftp connection on the given server using the given user/pass
	*/
	function connect($ftp_server,$ftp_user_name, $ftp_user_pass) {

		$conn = ssh2_connect($ftp_server, 22);
		if (false === $conn)
			throw new Exception('Can\'t connect to remote server');
		$result = ssh2_auth_password($conn, $ftp_user_name, $ftp_user_pass);
		echo ("Connection stablished".$newLine);
		if ($result === false) {
			throw new Exception('Authentication failed!');
		}
		
		return ssh2_sftp($conn);
	}

?>