<?php
/*
This file is called from Scientific Games after a player has launched a game in a new browser window.
25Jan13 AF - DELETE method returns 204
*/


//echo "hello";
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "SG";
$GameProviderID   = 19;//Scientific Games.
$COGSGuid         = "B31E14B1-BB11-4A37-A154-A8DC8B07570E"; //this is agsMain.Skin.COGSGuid for SkinID=1 (spin and win). Used when Ash sends a Recon request with no session id. The db will validate this

WriteLog('SG', 0,'RECV:'. file_get_contents("php://input"), 310);
exit;


function ErrorResponse ($errCode, $data) {
  global $GameProviderCode, $PlayerID, $glo, $RequestMethod, $Verb;
  $statusCode = 401;//unauthorised - the user is not logged in
  if ($RequestMethod == "gamecycles") {
    if ($errCode == E_INVALID_GAMEID ||
        $errCode == E_PLAYER_NOT_FOUND ||
        $errCode == E_RECON_TXGAME_NOT_FOUND
       )
      $statusCode = 404;//not found
	  
    if ($errCode == E_PLAYER_LIMITS_EXCEEDED)
      $statusCode = 530;//player limits exceeded	  	  	  
	  
    if ($statusCode == 401 && $Verb == "PUT") {
      if ($errCode == E_INSUFFICIENT_FUNDS)
        $statusCode = 402;//payment required
    }
  }
  $data = "SEND: Status=".$statusCode.(($data == "") ? "" : "   ").$data;
  if ($errCode > 0) {
    http_response_code($statusCode);
  }

  WriteLog($GameProviderCode, $errCode, $data, $PlayerID);
  exit;
}





/*
$_SERVER["REQUEST_URI"] always starts as /bally/rest/gp/....
We rewrite all calls to go to bally.php and evaluate the call methods based on the $_SERVER["REQUEST_URI"] from position 15
*/
if (!isset($_SERVER["HTTP_AUTHORIZATION"]))
  $_SERVER["HTTP_AUTHORIZATION"] = "";

$Verb          = $_SERVER["REQUEST_METHOD"]; //GET | PUT | DELETE
$RequestArray  = explode("/",strtolower(substr($_SERVER["REQUEST_URI"],15)));
$RequestMethod = $RequestArray[0]; //system | players | gamesessions | gamecycles
$SessionID     = substr($_SERVER["HTTP_AUTHORIZATION"],7,36); //Authorisation header is sent as "Bearer <sessionid><GameLaunchSourceID><playerid>"
$GameLaunchSourceID = intval(substr($_SERVER["HTTP_AUTHORIZATION"],43,3));
$PlayerID      = intval(substr($_SERVER["HTTP_AUTHORIZATION"],46));
$Response = "";

$phpInput      = file_get_contents("php://input");
$RequestData   = "Call:".$Verb." ".$_SERVER["REQUEST_URI"]." Auth:".$_SERVER["HTTP_AUTHORIZATION"]." Params:".json_encode($_REQUEST)." Content:".$phpInput." RequestArray:".json_encode($RequestArray);
WriteLog($GameProviderCode, 0, "RECV: ".$RequestData, $PlayerID);

exit;

/*
//Authorisation header is sent as "Bearer <sessionid><GameLaunchSourceID><playerid>


$Verb          = $_SERVER["REQUEST_METHOD"]; //GET | PUT | DELETE
$RequestArray  = explode("/",strtolower(substr($_SERVER["REQUEST_URI"],15)));
$RequestMethod = $RequestArray[0]; //system | players | gamesessions | gamecycles
$SessionID     = substr($_SERVER["HTTP_AUTHORIZATION"],7,36); //Authorisation header is sent as "Bearer <sessionid><GameLaunchSourceID><playerid>"
$GameLaunchSourceID = intval(substr($_SERVER["HTTP_AUTHORIZATION"],43,3));
$PlayerID      = intval(substr($_SERVER["HTTP_AUTHORIZATION"],46));
$Response = "";

$phpInput      = file_get_contents("php://input");
$RequestData   = "Call:".$Verb." ".$_SERVER["REQUEST_URI"]." Auth:".$_SERVER["HTTP_AUTHORIZATION"]." Params:".json_encode($_REQUEST)." Content:".$phpInput." RequestArray:".json_encode($RequestArray);




//system
$Verb = "GET"; $RequestMethod = "system"; $SessionID = "8D5E14B2-CB5A-403B-AC37-65E3BC3FAE62"; $GameLaunchSourceID = 0; $PlayerID = 1; $RequestArray[1] = $PlayerID;

//players
$Verb = "GET"; $RequestMethod = "players"; $SessionID = "8D5E14B2-CB5A-403B-AC37-65E3BC3FAE62"; $GameLaunchSourceID = 0; $PlayerID = 1; $RequestArray[1] = $PlayerID;

//gamesessions - PUT
$Verb = "PUT"; $RequestMethod = "gamesessions"; $SessionID = "8D5E14B2-CB5A-403B-AC37-65E3BC3FAE62"; $GameLaunchSourceID = 0; $PlayerID = 1; $RequestArray[1] = $PlayerID;
$phpInput = json_encode(array("gameSession" => array("gameSessionId" => "", "playerId" => "1", "status" => "NEW", "currencyCode" => null, "countryCode" => null, "softwareId" => "BallyTest", "languageCode" => null, "customParameters" => null)),true);

//gamecycles - PUT WAGER and WIN
$Verb = "PUT"; $RequestMethod = "gamecycles"; $SessionID = "8D5E14B2-CB5A-403B-AC37-65E3BC3FAE62"; $GameLaunchSourceID = 0; $PlayerID = 1; 
$phpInput =
json_encode(array(
  "gameCycle" => array("gameCycleId" => "1c018f26-3e54-409d-bf02-9ed561e011ba",
                       "playerId" => "1",
                       "gameSessionId" => "",
                       "softwareId" => "BallyTest",
                       "complete" => "True",
                       "transactions" => array(
                                              array(
                                                     "transactionId" => "1",
                                                     "currencyCode" => "GBP",
                                                     "actions" => array(
                                                                        array(
                                                                              "actionId" => "1",
                                                                              "type" => "STAKE",
                                                                              "amount" => "100",
                                                                              "jackpotAmount" => "0"
                                                                             ),
                                                                        array("actionId" => "2",
                                                                              "type" => "WIN",
                                                                              "amount" => "100",
                                                                              "jackpotAmount" => "0"
                                                                             )
                                                                       )
                                                   )
                                              )
                      )
),true);

//gamecycles - PUT REFUND
$Verb = "PUT"; $RequestMethod = "gamecycles"; $SessionID = "8D5E14B2-CB5A-403B-AC37-65E3BC3FAE62"; $GameLaunchSourceID = 0; $PlayerID = 1; 
$phpInput =
json_encode(array(
  "gameCycle" => array("gameCycleId" => "1c018f26-3e54-409d-bf02-9ed561e011ba",
                       "playerId" => "1",
                       "gameSessionId" => "",
                       "softwareId" => "BallyTest",
                       "complete" => "True",
                       "transactions" => array(
                                              array(
                                                     "transactionId" => "77",
                                                     "currencyCode" => "GBP",
                                                     "actions" => array(
                                                                        array(
                                                                              "actionReference" => "29",
                                                                              "actionId" => "1",
                                                                              "type" => "REFUND",
                                                                              "amount" => "100",
                                                                              "jackpotAmount" => "0"
                                                                             )
                                                                       )
                                                   )
                                              )
                      )
),true);

//gamecycles - GET (no subref)
$Verb = "GET"; $RequestMethod = "gamecycles"; $SessionID = "8D5E14B2-CB5A-403B-AC37-65E3BC3FAE62"; $GameLaunchSourceID = 0; $PlayerID = 1; $RequestArray[1] = "1C018F26-3E54-409D-BF02-9ED561E011BA"; //$RequestArray[2] = 1;

//gamecycles - GET (with subref)
$Verb = "GET"; $RequestMethod = "gamecycles"; $SessionID = "8D5E14B2-CB5A-403B-AC37-65E3BC3FAE62"; $GameLaunchSourceID = 0; $PlayerID = 1; $RequestArray[1] = "1C018F26-3E54-409D-BF02-9ED561E011BA"; $RequestArray[2] = 1;
*/

if ($RequestMethod != "system" &&
    $RequestMethod != "players" &&
    $RequestMethod != "gamesessions" &&
    $RequestMethod != "gamecycles") {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $RequestData [RequestMethod param missing]");
}

if ($RequestMethod == "system") {
  echo json_encode(array("system" => array("operator"=>"Daub", "version"=>"1.0.0", "status"=>"Alive")),JSON_UNESCAPED_SLASHES);
  exit;
}

if ($RequestMethod == "gamesessions" && $Verb == "DELETE") {
  http_response_code(204);
  exit;
}

//Check authentication
if (strlen($SessionID) != 36) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $RequestData [SessionID invalid]");
}

//Container
if ($RequestMethod == "gamesessions" || ($RequestMethod == "gamecycles" && $Verb == "PUT")) {
  //Make arrays of the JSON content
  $GameRequest = json_decode($phpInput,true);
  if ($RequestMethod == "gamesessions")
    $GameRequest = $GameRequest["gameSession"];
  if ($RequestMethod == "gamecycles")
    $GameRequest = $GameRequest["gameCycle"];
}

//Check playerID match
if (($RequestMethod == "players" && ($PlayerID == 0 || ($PlayerID != $RequestArray[1]))) ||
    (($RequestMethod == "gamesessions" || ($RequestMethod == "gamecycles" && $Verb == "PUT")) && ($PlayerID == 0 || ($PlayerID != @$GameRequest["playerId"])))
   )
{
  ErrorResponse(E_INVALID_PARAMS, "RECV: $RequestData [PlayerID invalid]");
}

if (!isset($GameRequest["softwareId"])) {
  $GameRequest["softwareId"] = "";
}

if (!isset($GameRequest["gameCycleId"])) {
  if ($RequestMethod == "gamecycles" && $Verb == "GET") {
    $GameRequest["gameCycleId"] = $RequestArray[1];
  } else {
    $GameRequest["gameCycleId"] = "f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb";
  }
}

//open connection to db
require_once("D:/WEB/inc/db.php");
$dbErr = "";
$db = "";
$dbSproc = "xTxGame";
$dbParams = array(
  "PlayerID"           => array($PlayerID, "int", 0),
  "SessionID"          => array($SessionID, "str", 36),
  "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),//this is game server ip - but use anyway
  "GameProviderID"     => array($GameProviderID, "int", 0),
  "GameProviderGameID" => array($GameRequest["softwareId"], "str", 30),
  "GameLaunchSourceID" => array($GameLaunchSourceID, "int", 0),
  "Action"             => array("", "str", 15),
  "Wager"              => array(0, "int", 0),
  "Win"                => array(0, "int", 0),
  "Ref"                => array(0, "int", 0),
  "SubRef"             => array(0, "int", 0),
  "Refund"             => array(0, "int", 0),
  "RefundRef"          => array("", "str", 50),
  "RefundNotes"        => array("", "str", 4000),
  "TxWagerTypeID"      => array(0, "int", 0),//will be default
  "TxWinTypeID"        => array(0, "int", 0),//will be default
  "BallygameCycleId"   => array($GameRequest["gameCycleId"], "str", 36)
);




if(!dbInit("","agsMain",1)) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
  ErrorResponse($errCode, $dbErr);//exits
}

if ($RequestMethod == "gamecycles") {
  /*
  execute xTxGame for each transaction and build up the response
  Bally transactionId + actionId will be combined to create a unique SubRef as (transactionId*100) + actionId
  When GETTING, the SubRef/100 will give an integer being the transactionId
  */
  $credits = array();
  $transactions = array();
  $actions = array();

  if ($Verb == "PUT") {
    $itransactions=0;
    $transactionCount=count($GameRequest["transactions"])-1;
    
    while ($itransactions<=$transactionCount) {
      $iactions=0;
      $actionCount = count($GameRequest["transactions"][$itransactions]["actions"])-1;
  
      $CurrencyCode = "";
      $Balance = 0;

      while ($iactions<=$actionCount) {
        $dbParams["Wager"][0]       = 0;
        $dbParams["Win"][0]         = 0;
        $dbParams["Refund"][0]      = 0;
        $dbParams["RefundRef"][0]   = "";
        $dbParams["RefundNotes"][0] = "";

        $dbParams["SubRef"][0] = ($GameRequest["transactions"][$itransactions]["transactionId"]*100) + ($iactions+1);

        //set the xTxGame properties
        $type = $GameRequest["transactions"][$itransactions]["actions"][$iactions]["type"];
        if ($type == "STAKE" || $type == "WIN") {
          if ($iactions == $actionCount && $itransactions == $transactionCount && $GameRequest["complete"] == "True") {//last action and complete - TxEndGameRound
            $dbParams["Action"][0] = "TxEndGameRound";
          } else {
            $dbParams["Action"][0] = "Tx";
          }
        }
        
        if ($type == "STAKE") 
          //$dbParams["Wager"][0] = number_format($GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"]/100,2,".","");
          $dbParams["Wager"][0] = number_format($GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"],2,".","");
        if ($type == "WIN")
          //$dbParams["Win"][0] = number_format($GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"]/100,2,".","");
          $dbParams["Win"][0] = number_format($GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"],2,".","");
        if ($type == "REFUND") {
            $dbParams["Action"][0]    = "Void";
            //$dbParams["Refund"][0]    = number_format($GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"]/100,2,".","");
            $dbParams["Refund"][0]    = number_format($GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"],2,".","");
            $dbParams["RefundRef"][0] = $GameRequest["transactions"][$itransactions]["actions"][$iactions]["actionReference"];
        }
  
        /*
        db calls xTxGame
        Returns:
          Result = 0 : Success
                <> 0 : Various fail codes
        */
        $rs = dbExecSproc($dbSproc,$dbParams,1);
        if ($rs === false) {
          $errCode = E_DB_SELECT;
          WriteLog("db", $errCode, $dbErr);
        } else {
          $row = sqlsrv_fetch_object($rs);
          if (!isset($row) || $row==null) {
            $errCode = E_DB_RETURN;
            WriteLog("db", $errCode, $dbErr);
          }
        }
  
        if ($dbErr != "") {
          ErrorResponse($errCode, $dbErr);//exits
        }
        
        dbFree($rs);
        
        //db access was successful, other error
        if ($row->Result != 0) {
          if ($row->Result == 104 || $row->Result == 105)
            $row->Result = 104;
        
          ErrorResponse($row->Result, "");
        }

        $CurrencyCode = $row->CurrencyCode;
        $Balance = floatval($row->Balance);
        
        $jackpotAmount = 0;
        if (isset($GameRequest["transactions"][$itransactions]["actions"][$iactions]["jackpotAmount"])) {
          $jackpotAmount = $GameRequest["transactions"][$itransactions]["actions"][$iactions]["jackpotAmount"];
        }
        
        //Build 'actions' return string
        $actions[] = array("actionReference" => $row->TxGameID, //TxGame.ID (same as transactionReference as we do not store specific action ref's)
                           "actionId"        => $iactions+1,
                           "type"            => $GameRequest["transactions"][$itransactions]["actions"][$iactions]["type"],
                           //"amount"          => intval($GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"]),
                           "amount"          => $GameRequest["transactions"][$itransactions]["actions"][$iactions]["amount"],
                           //"jackpotAmount"   => intval($GameRequest["transactions"][$itransactions]["actions"][$iactions]["jackpotAmount"]),
                           "jackpotAmount"   => $jackpotAmount,
                           "actionStatus"    => "SUCCESS"
                          );
        
        $iactions++;
      }
  
      $transactions[] = array("transactionReference" => strval(($GameRequest["transactions"][$itransactions]["transactionId"]*100)),
                              "transactionId"        => intval($GameRequest["transactions"][$itransactions]["transactionId"]),
                              "currencyCode"         => $CurrencyCode,
                              "actions"              => $actions,
                              "transactionStatus"    => "SUCCESS"
                             );
  
      $itransactions++;
    }

    //$credits[] = array("fundtype" => array("type" => "CASH","cashable" => true), "amount" => intval(number_format($Balance*100,0,"","")));
    $credits[] = array("fundtype" => array("type" => "CASH","cashable" => true), "amount" => $Balance);

    $responseParams = array(
      "gameCycleResponse" => array(
        "playerId" => "$PlayerID",
        "playerBalance" => array(
          "currencyCode" => $CurrencyCode,
          "credits" => $credits
         ),
        "transactions" => $transactions,
      )
    );
  } elseif ($Verb == "GET") {//return everything for the TxGameRoundId (gameCycleId)
    //BallygameCycleId is the Ref which maps to the TxGame.Ref in xTxGame
    $dbParams["Action"][0] = "Recon";
    if (isset($RequestArray[2]) && is_int(intval(@$RequestArray[2])) && intval(@$RequestArray[2]) != 0) {
      $dbParams["SubRef"][0] = ($RequestArray[2]*100);
    } else {
      $dbParams["SubRef"][0] = 0;
    }

    /*
    db calls xTxGame
    Returns:
      Result = 0 : Success
            <> 0 : Various fail codes
    */
    $rs = dbExecSproc($dbSproc,$dbParams,1);
    if ($rs === false) {
      $errCode = E_DB_SELECT;
      WriteLog("db", $errCode, $dbErr);
    }

    if ($dbErr != "") {
      ErrorResponse($errCode, $dbErr);//exits
    }

    $iactions = 1;
    $complete = "";
    $thisTransactionId = 0;
    $thisSubRef = 0;

    while($row = sqlsrv_fetch_object($rs)) {
      if (!isset($row) || $row==null) {
        $errCode = E_DB_RETURN;
        WriteLog("db", $errCode, $dbErr);
      }

      //db access was successful, other error
      if ($row->Result != 0) {
        if ($row->Result == 104 || $row->Result == 105)
          $row->Result = 104;
      
        ErrorResponse($row->Result, "");
      }
      
      if ($thisTransactionId == 0) {
        $thisSubRef = $row->SubRef;
        $thisTransactionId = intval($thisSubRef/100);
      }

      $GameRequest["softwareId"] = $row->GameProviderGameID;
      $complete = (boolean) ($row->TxGameRoundComplete);

      if ($thisTransactionId != intval($row->SubRef/100)) {
        $transactions[] = array("transactionReference" => strval($thisSubRef),
                                "transactionId"        => intval($thisTransactionId),
                                //"currencyCode"         => $row->CurrencyCode,
                                "actions"              => $actions,
                                "transactionStatus"    => "SUCCESS"
                               );
        $thisSubRef = $row->SubRef;
        $thisTransactionId = intval($thisSubRef/100);
        $actions = array();
        $iactions = 1;
      }

      if ($dbParams["SubRef"][0] == 0) {
        $actions[] = array("actionReference" => $row->TxGameID,
                           "actionId"        => $iactions,
                           "type"            => $row->BallyType,
                           //"amount"          => intval($row->Amount*100),
                           "amount"          => floatval($row->Amount),
                           //"jackpotAmount"   => ???,
                           "actionStatus"    => "SUCCESS"
                          );
      } else {
        $actions[] = array("actionReference" => $row->TxGameID,
                           "actionId"        => $iactions,
                           "actionStatus"    => "SUCCESS"
                          );
      }
      $iactions++;
    }

    $transactions[] = array("transactionReference" => strval($thisSubRef),
                            "transactionId"        => intval($thisTransactionId),
                            //"currencyCode"         => $row->CurrencyCode,
                            "actions"              => $actions,
                            "transactionStatus"    => "SUCCESS"
                           );

    if ($dbParams["SubRef"][0] == 0) {//getting all transactions
      $responseParams = array(
        "gameCycle" => array(
          "gameCycleId" => $GameRequest["gameCycleId"],
          "playerId"    => strval($PlayerID),
          //"gameSessionId" => "",
          "softwareId" => $GameRequest["softwareId"],
          "complete"   => $complete,
          "transactions" => $transactions
          )
      );
    } else {//getting a specific transactionid
      $responseParams = array("transaction" => $transactions);
    }
  }

  dbFree($rs);
  dbClose($db);
} else {
  $dbParams["Action"][0] = "GetBalance";

  /*
  db calls xTxGame
  Returns:
    Result = 0 : Success
          <> 0 : Various fail codes
  */
  
  $rs = dbExecSproc($dbSproc,$dbParams,1);
  if ($rs === false) {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  } else {
    $row = sqlsrv_fetch_object($rs);
    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }
  
  if ($dbErr != "") {
    ErrorResponse($errCode, $dbErr);//exits
  }
  
  dbFree($rs);
  dbClose($db);
  
  //db access was successful, other error
  if ($row->Result != 0) {
    if ($row->Result == 104 || $row->Result == 105)
      $row->Result = 104;
  
    ErrorResponse($row->Result, "");
  }

  $credits = array();
  //$credits[] = array("fundtype" => array("type" => "CASH","cashable" => "true"), "amount" => intval(number_format($row->Balance*100,0,"","")));
  $credits[] = array("fundtype" => array("type" => "CASH","cashable" => "true"), "amount" => floatval($row->Balance));
  $responseParams = array(
	    "player"    => array(
	    //"uri" => "/$RequestMethod/$PlayerID",
	    "playerId" => "$PlayerID",
	    "firstName"   => "",
	    "lastName"    => "",
	    "countryCode" => $row->CountryCode,
	    "currencyCode" => $row->CurrencyCode,
	    "languageCode" => $row->LanguageCode,
	    "playerBalance" => array(
	    "currencyCode" => $row->CurrencyCode,
	    "credits" => $credits,
      )
    )
  );
}

//$Response = json_encode($responseParams,JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
$Response = json_encode($responseParams,JSON_UNESCAPED_SLASHES);

WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);
header("Content-type: application/json");
echo $Response;
exit;
?>