<?php
/*
This file is called from igt after a player has launched a game in a new browser window.
It can also be called by igt with a HeartBeat request to determine if the system is active

AF 04Mar12 - Added PlayerID to WriteLog params for game logs
AF 05Mar12 - Added checks that $PlayerID and $SessionID != "null"
AF 06Mar12 - Added GameLaunchSourceID
AF 07Mar12 - Modified VOID to return REJECTED if failed
AF 09Mar12 - Deals with ReturnCode=304 (Funds available less than wager)
AF 25Mar12 - Deals with ReturnCode=308 (E_PLAYER_LIMITS_EXCEEDED) properly
AF 02Nov12 - Removed conversion of XML to Array - using only xml structure due to problems with php 5.4.8
AF 30Nov12 - Changed is do that actionId (after dash) will be the SubRef
AF 24Feb13 - IGT sometimes send a double request for a void, and we would ordinarily return a 'error - duplicate' which they dont seem to want to accept, so the change is
             if (!($row->Result == E_DUPLICATE_REF && $RequestMethod == "Void"))
AF 20Aug13 - Last 4 characters of IGT securetoken is Main.Game.ID and is used in xTxGame to lookup the correct game because their html5 and flash games use the same igt code
AF 29Aug13 - Added GetPlayerBalance as a RequestMethod
AF 06Sep13 - Fixed GetPlayerBalance response
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "IGT";
$GameProviderID   = 4;//IGT=4
$COGSGuid         = "B31E14B1-BB11-4A37-A154-A8DC8B07570E"; //this is agsMain.Skin.COGSGuid for SkinID=1 (spin and win). Used when rgs sends a Recon request with no session id. The db will validate this

function ErrorResponse ($errCode, $data, $RequestMethod, $Response) {
	global $GameProviderCode, $PlayerID;
	$Response = str_replace("#balance#","<Balance amount=\"0.00\" type=\"CASH\"/>",$Response);

	if ($RequestMethod == "Init") {
	$Response.="<Init status=\"FAILURE\" msg=\"".$errCode."\"/></GameResponse>";
	}

  if ($RequestMethod == "Play" || $RequestMethod == "Recon" || $RequestMethod == "Void" || $RequestMethod == "GetPlayerBalance") {
	$status = "FAILURE";
	if ($errCode == E_INVALID_WAGER ||
		$errCode == E_INVALID_WIN ||
		$errCode == E_DUPLICATE_REF ||
		$errCode == E_PLAYER_LIMITS_EXCEEDED ||
		$errCode == E_BALANCE_DIFFERENCE ||
		$errCode == E_VOID_FAIL_NO_WAGERS ||
		$errCode == E_INVALID_TXID) {

	  $status = "REJECTED";
	}

	$Response.="<".$RequestMethod." status=\"".$status."\" id=\"\" action=\"\" msg=\"".$errCode."\"/></GameResponse>";
	}

	if ($RequestMethod == "EndSession") {
	$Response.="<EndSession status=\"FAILURE\"/></GameResponse>";
	}

	$data = (($Response == "") ? "" : "SEND: ".$Response."   ").$data;
	WriteLog($GameProviderCode, $errCode, $data, $PlayerID);

	echo $Response;
	exit;
}

$RequestMethod = "";
$Response = "";

$rawRequest = file_get_contents("php://input");
$GameRequest = simplexml_load_string($rawRequest);

if ($GameRequest === false) {
	$errCode = E_INVALID_PARAMS;
  ErrorResponse($errCode, "RECV: $rawRequest [IGT Failed to load xml]", $RequestMethod, $Response);
}

if (isset($GameRequest->HeartBeat)) {
  $RequestMethod = "HeartBeat";
  $Response = "<GameResponse><HeartBeat status=\"SUCCESS\"/>";
}

if ($RequestMethod != "HeartBeat") {
	if (isset($GameRequest->Init)) {
	  $RequestMethod = "Init";
	}

	if (isset($GameRequest->GetPlayerBalance)) {
	  $RequestMethod = "GetPlayerBalance";
	}

	if (isset($GameRequest->Play)) {
	  $RequestMethod = "Play";
	}

	if (isset($GameRequest->Recon)) {
	  $RequestMethod = "Recon";
	}

	if (isset($GameRequest->Void)) {
	  $RequestMethod = "Void";
	}

	if (isset($GameRequest->EndSession)) {
	  $RequestMethod = "EndSession";
	}

	if ($RequestMethod == "") {
		$errCode = E_INVALID_PARAMS;
    WriteLog($GameProviderCode, $errCode, "RECV: ".$rawRequest, 0);
	  ErrorResponse($errCode, "No RequestMethod", $RequestMethod, $Response);
	}

  $PlayerID = 0;
  $sessionid = "";
  $GameLaunchSourceID = 0;

  if (isset($GameRequest->Header->Customer["userId"]))
  	$PlayerID = $GameRequest->Header->Customer["userId"];

  WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, $PlayerID);

	//Generate common response header (echo of received XML)
	$Response = "<GameResponse>";
	$Response.= "<Header>";

	if ($RequestMethod == "Init" || $RequestMethod == "Play" || $RequestMethod == "GetPlayerBalance") {
	  $Response.="<GameDetails gameId=\"".$GameRequest->Header->GameDetails["gameId"].
							 "\" name=\"".$GameRequest->Header->GameDetails["name"].
							 "\" class=\"".$GameRequest->Header->GameDetails["class"].
					     "\"/>";
	  $Response.="<Customer ccyCode=\"".$GameRequest->Header->Customer["ccyCode"].
						   "\" secureToken=\"".$GameRequest->Header->Customer["secureToken"].
						   "\" userId=\"".$GameRequest->Header->Customer["userId"].
					     "\">#balance#</Customer>";
						 
					//WriteLog($GameProviderCode, 0, "TEST: Tino", $PlayerID);	 

						 
//5FC8F296-DBB5-4C37-B555-5C824B414DE00004504						 
//5FC8F296-DBB5-4C37-B555-5C824B414DE00004504
						 
    $sessionid = substr($GameRequest->Header->Customer["secureToken"],0,36);
    $GameLaunchSourceID = intval(substr($GameRequest->Header->Customer["secureToken"],36,3));
    $GameProviderGameID = substr($GameRequest->Header->Customer["secureToken"],39,4);//this is actually our internal Game.ID - xTxGame will use this for IGT games
	}

	if ($RequestMethod == "Play") {	
		  
		$Response.= "%%MESSAGES%%";
	}
	
	//WriteLog($GameProviderCode, 0, "source: ".$GameLaunchSourceID, $PlayerID);
	
	
	if ($RequestMethod == "Recon" || $RequestMethod == "Void") {
	  $Response.="<GameDetails gameId=\"".$GameRequest->Header->GameDetails["gameId"].
							 "\" name=\"".$GameRequest->Header->GameDetails["name"].
							 "\" class=\"".$GameRequest->Header->GameDetails["class"].
					     "\"/>";
	  $Response.="<Customer ccyCode=\"".$GameRequest->Header->Customer["ccyCode"].
						   "\" userId=\"".$GameRequest->Header->Customer["userId"].
					     "\">#balance#</Customer>";

    $sessionid = $COGSGuid;
	  $GameProviderGameID = $GameRequest->Header->GameDetails["gameId"];
	}

	if ($RequestMethod == "EndSession") {
	  $Response.="<Customer userId=\"".$GameRequest->Header->Customer["userId"]."\"></Customer>";
      $GameProviderGameID = substr($GameRequest->Header->Customer["secureToken"],39,4);//this is actually our internal Game.ID - xTxGame will use this for IGT games
	}

	
	$Response.="</Header>";

	//Check authentication
	if ($GameRequest->Header->Auth["username"] != "ags" && $GameRequest->Header->Auth["password"] != "pLd(4Re@1.Wq") {
	  ErrorResponse(E_INVALID_AUTH, "username:".$GameRequest->Header->Auth["username"]." password:".$GameRequest->Header->Auth["password"], $RequestMethod, $Response);
	}

	if($RequestMethod != "EndSession")
	{
		//Check PlayerID and SessionID
		if ($PlayerID == "null" || $PlayerID == 0 || $sessionid == "null" || $sessionid == "") {
		  ErrorResponse(E_INVALID_PARAMS, "PlayerID or sessionid invalid", $RequestMethod, $Response);
		}
	}
	
  if ($RequestMethod != "EndSession") {
    $wager=0;
    $win=0;
    $ref=0;
    $subref = 999;
    $refund = 0;
    $refundref="";
    $refundnotes="";

  	if ($RequestMethod == "Play" || $RequestMethod == "Recon" || $RequestMethod == "Void") {

      if (count($GameRequest->$RequestMethod->RGSGame->RGSAction) == 1) {
      	$RGSAction = $GameRequest->$RequestMethod->RGSGame->RGSAction;
        if ($GameRequest->$RequestMethod->RGSGame->RGSAction["action"] == "STAKE") {
  	      $wager = $GameRequest->$RequestMethod->RGSGame->RGSAction["amount"];
        }

        if ($GameRequest->$RequestMethod->RGSGame->RGSAction["action"] == "WIN") {
  	      $win = $GameRequest->$RequestMethod->RGSGame->RGSAction["amount"];
        }

        if ($GameRequest->$RequestMethod->RGSGame->RGSAction["action"] == "REFUND") {
  	      $refund = $GameRequest->$RequestMethod->RGSGame->RGSAction["amount"];
        }
        $subref = (substr($RGSAction["actionId"],strpos($RGSAction["actionId"],"-")+1)+1);//subref is the IGT actionId number after the '-' and 1 is added to it because we dont allow a subref = 0
      } else {
        foreach ($GameRequest->$RequestMethod->RGSGame->RGSAction as $RGSAction) {
          if ($RGSAction["action"] == "STAKE") {
    	      $wager = $RGSAction["amount"];
    	    }

          if ($RGSAction["action"] == "WIN") {
    	      $win = $RGSAction["amount"];
    	    }

          if ($RGSAction["action"] == "REFUND") {
    	      $refund = $RGSAction["amount"];
    	    }
          $subref = (substr($RGSAction["actionId"],strpos($RGSAction["actionId"],"-")+1)+1);//subref is the IGT actionId number after the '-' and 1 is added to it because we dont allow a subref = 0
    	  }
      }

  	  $ref = $GameRequest->$RequestMethod->RGSGame["txnId"];

  	  if ($RequestMethod == "Void") {
  	    $refundref   = $GameRequest->Void["ref"];
  	    $refundnotes = $GameRequest->Void["reason"];
  	  }
  	}

  	$dbSproc = "xTxGame";

  	$dbParams = array(
  					  "PlayerID"           => array($PlayerID, "int", 0),
  					  "SessionID"          => array($sessionid, "str", 36),
  					  "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),//this is game server ip - but use anyway
  					  "GameProviderID"     => array($GameProviderID, "int", 0),
  					  //"GameProviderGameID" => array($GameRequest->Header->GameDetails["gameId"], "str", 30),
  					  "GameProviderGameID" => array($GameProviderGameID, "str", 30),
					  "GameLaunchSourceID" => array($GameLaunchSourceID, "int", 0),
  					  "Action"             => array("", "str", 15),
  					  "Wager"              => array($wager, "int", 0),
  					  "Win"                => array($win, "int", 0),
  					  "Ref"                => array($ref, "int", 0),
  					  "SubRef"             => array($subref, "int", 0),
  					  "Refund"             => array($refund, "int", 0),
  					  "RefundRef"          => array($refundref, "str", 50),
  					  "RefundNotes"        => array($refundnotes, "str", 4000)
  					 );

  	if ($RequestMethod == "Init" || $RequestMethod == "GetPlayerBalance") {
  	  $dbParams["Action"][0] = "GetBalance";
  	}

  	if ($RequestMethod == "Play") {
  	  $dbParams["Action"][0] = "Tx";
  	  if ($GameRequest->$RequestMethod->RGSGame["finished"] == "Y")
  	    $dbParams["Action"][0] = "TxEndGameRound";
  	}

  	if ($RequestMethod == "Recon") {
  	  $dbParams["Action"][0] = "Recon";
  	}

  	if ($RequestMethod == "Void") {
  	  $dbParams["Action"][0] = "Void";
  	}

		/*
		db calls xTxGame
		Returns:
			Result = 0 : Success
			      <> 0 : Various fail codes
		*/
	  require_once("D:/WEB/inc/db.php");

  	$dbErr = "";
  	$db = "";

	  if(!dbInit("","agsMain",1)) {
			$errCode = E_DB_CONNECT;
		  WriteLog("db", $errCode, $dbErr);
	  } else {
	    $rs = dbExecSproc($dbSproc,$dbParams,1);
	    if ($rs === false) {
				$errCode = E_DB_SELECT;
			  WriteLog("db", $errCode, $dbErr);
	    } else {
	      $row = sqlsrv_fetch_object($rs);
	      if (!isset($row) || $row==null) {
					$errCode = E_DB_RETURN;
				  WriteLog("db", $errCode, $dbErr);
	      }
	    }
	  }

    if ($dbErr != "") {
		  ErrorResponse($errCode, $dbErr, $RequestMethod, $Response);//exits
    }

  	dbFree($rs);
  	dbClose($db);

	  //db access was successful, other error
	  if ($row->Result != 0) {
      if ($row->Result == 104 || $row->Result == 105)
        $row->Result = 104;

      if ($RequestMethod == "Init") {
		  	ErrorResponse($row->Result, "", $RequestMethod, $Response);
		  } else {
	      if ($row->Result != E_PLAYER_NOT_FOUND &&
	          $row->Result != E_INSUFFICIENT_FUNDS &&
	          $row->Result != E_VOID_FAIL_NO_WAGERS &&
	          $row->Result != E_PLAYER_LIMITS_EXCEEDED)
	      {
          if (!($row->Result == E_DUPLICATE_REF && $RequestMethod == "Void"))
			  	ErrorResponse($row->Result, "", $RequestMethod, $Response);
	      }
	    }
	  }
  }
}

/*
Success - make response
*/
if ($RequestMethod == "Init" || $RequestMethod == "Play" || $RequestMethod == "Recon" || $RequestMethod == "Void"  || $RequestMethod == "GetPlayerBalance") {
  $Response = str_replace("#balance#","<Balance amount=\"".$row->Balance."\" type=\"CASH\"/>",$Response);
}

if ($RequestMethod == "Init") {
  $Response.="<Init status=\"SUCCESS\"/>";
}

if ($RequestMethod == "GetPlayerBalance") {
  $Response.="<GetPlayerBalance status=\"SUCCESS\"/>";
}

  //WriteLog($GameProviderCode, 33, "TEST: Always".$RequestMethod, $PlayerID);
if ($RequestMethod == "Play") {
	if ($row->Result == E_PLAYER_NOT_FOUND) {
  	$Response.="<$RequestMethod id=\"".$row->agsRef."\" status=\"NOT_FOUND\">";
	} elseif ($row->Result == E_INSUFFICIENT_FUNDS || $row->Result == E_PLAYER_LIMITS_EXCEEDED) {
  	$Response.="<$RequestMethod id=\"".$row->agsRef."\" status=\"INSUFFICIENT_FUNDS\">";
	} else {
  	$Response.="<$RequestMethod id=\"".$row->agsRef."\" status=\"SETTLED\">";
	}
	$messages = "";
	
  //WriteLog($GameProviderCode, 0, "TEST: Just play", $PlayerID);
	if(isset($row->RealityCheck) && $row->RealityCheck == 1){
		$messages = "<Messages>".
				"<Message> <Type>REALITY_CHECK</Type>".
				"<Message>You've been playing for ".$row->Minutes." minutes.".
				"</Message>".
				"<MessageActions>".
				"<MessageAction> <Name>CONTINUE</Name> </MessageAction>".
				"<MessageAction><Name>CLOSE</Name></MessageAction>".
				"<MessageAction>".
				"<Name>VIEW_ACCOUNT_HISTORY</Name>".
				"<ActionParameters>".
				"<ActionParameter> <Name>url</Name> <Value>https://www.luckypantsbingo.com/</Value> </ActionParameter>".
				"<ActionParameter><Name>closeGame</Name><Value>YES</Value></ActionParameter>".
				"</ActionParameters>".
				"</MessageAction>".
				"</MessageActions>".
				"</Message>".
				"</Messages>";	
	
	//WriteLog($GameProviderCode, 0, "TEST: After messages", $PlayerID);

	}	
	
	$Response = str_replace("%%MESSAGES%%",$messages,$Response);
}

if ($RequestMethod == "Recon") {
	if ($row->Result == E_PLAYER_NOT_FOUND || $row->Result == E_INSUFFICIENT_FUNDS || $row->Result == E_PLAYER_LIMITS_EXCEEDED) {
		$Response.="<Recon status=\"SETTLED\" id=\"\" action=\"CANCEL\" msg=\"".$row->Result."\">";
	} else {
		$Response.="<Recon action=\"COMPLETE\" id=\"".$row->agsRef."\" status=\"SETTLED\">";
	}
}

if ($RequestMethod == "Void") {
	if ($row->Result == E_VOID_FAIL_NO_WAGERS) {
		$Response.="<Void status=\"SETTLED\" id=\"\" msg=\"".$row->Result."\">";
	} else {
		$Response.="<Void status=\"SETTLED\" id=\"".$row->agsRef."\">";
	}
}

if ($RequestMethod != "HeartBeat") {
	if (@$row->Result != 0) {
		$wager=0;
		$win=0;
	}
}

if ($RequestMethod == "Play" || $RequestMethod == "Recon" || $RequestMethod == "Void") {
  $Response.="<RGSGameState finished=\"".$GameRequest->$RequestMethod->RGSGame["finished"]."\" txnId=\"".$GameRequest->$RequestMethod->RGSGame["txnId"]."\">";

  if (count($GameRequest->$RequestMethod->RGSGame->RGSAction) == 1) {
    if ($GameRequest->$RequestMethod->RGSGame->RGSAction["action"] == "STAKE") {
      $Response.="<RGSAction action=\"STAKE\" actionId=\"\" actionRef=\"\" amount=\"".$wager."\">";
      $Response.="<FundType amount=\"".$wager."\" type=\"CASH\"/>";
      $Response.="</RGSAction>";
    }

    if ($GameRequest->$RequestMethod->RGSGame->RGSAction["action"] == "WIN") {
      $Response.="<RGSAction action=\"WIN\" actionId=\"\" actionRef=\"\" amount=\"".$win."\">";
      $Response.="<FundType amount=\"".$win."\" type=\"CASH\"/>";
      $Response.="</RGSAction>";
    }

    if ($GameRequest->$RequestMethod->RGSGame->RGSAction["action"] == "REFUND") {
      $Response.="<RGSAction action=\"REFUND\" actionId=\"\" actionRef=\"\" amount=\"".$refund."\"/>";
    }
  } else {
    foreach ($GameRequest->$RequestMethod->RGSGame->RGSAction as $RGSAction) {
      if ($RGSAction["action"] == "STAKE") {
        $Response.="<RGSAction action=\"STAKE\" actionId=\"\" actionRef=\"\" amount=\"".$wager."\">";
        $Response.="<FundType amount=\"".$wager."\" type=\"CASH\"/>";
        $Response.="</RGSAction>";
	    }
      if ($RGSAction["action"] == "WIN") {
        $Response.="<RGSAction action=\"WIN\" actionId=\"\" actionRef=\"\" amount=\"".$win."\">";
        $Response.="<FundType amount=\"".$win."\" type=\"CASH\"/>";
        $Response.="</RGSAction>";
	    }
      if ($RGSAction["action"] == "REFUND") {
        $Response.="<RGSAction action=\"REFUND\" actionId=\"\" actionRef=\"\" amount=\"".$refund."\"/>";
	    }
	  }
  }

  $Response.="</RGSGameState>";
  $Response.="</$RequestMethod>";
}

if ($RequestMethod == "EndSession") {
  $Response.="<EndSession status=\"SUCCESS\"/>";
}

$Response.="</GameResponse>";

if ($RequestMethod != "HeartBeat") {
  WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);
}
echo $Response;
exit;
?>