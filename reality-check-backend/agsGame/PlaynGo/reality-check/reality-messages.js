var sbm = sbm || {};

sbm.ExternalCommunicator = { 
	source: window.parent,//Set at init message 
	targetOrigin: "*",//Set at init message 
	init: function () { 
		window.addEventListener("message", 
			this.processReceivedMessage.bind(this), false); 
		}, 
	//Post message to parent host (PNGHostInterface component) 
	request: function (req) { 
		this.source.postMessage(req, this.targetOrigin); 
	}, 
	//Events coming from parent host (PNGHostInterface component) 
	processReceivedMessage: function (e) { 
		console.log("RC Component received: ", e, e.data.type); 
		switch (e.data.type) { 
			case "init": 
				//Setup communication with PNGHostInterface 
				this.targetOrigin = e.origin; 
				this.source = e.source; 
				break; 
			//When game is disabled, show message 
			case "gameDisabled": 
				sbm.RCComponent.show(); 
				break; 
			//Game is enabled again 
			case "gameEnabled":
				break; 
		} 
	} 
};

sbm.RCComponent = {
	$messagebox: $("#ui-reality-check-container"), 
	init: function () { 
		this.$messagebox.hide(); 
	}, 
	show: function () { 
		this.$messagebox.show(); 
	}, 
	//Listener for button. 
	//Close message and send enable request to game. 
	onContinuePlaying: function () { 
		this.$messagebox.hide(); 
		sbm.ExternalCommunicator.request({req: "gameEnable"}); 
	}, 
	//Listener for button. 
	//Close message and send enable game end request to game. 
	onStopPlaying: function (url) { 
		this.$messagebox.hide(); 
		sbm.ExternalCommunicator.request({req: "gameEnd", data: {redirectUrl: url}}); 
	}, 
	onRealityCheck: function (action) { 
		switch (action) { 
		//Reality check is needed, 
		// send request that the game should disable user interaction 
		case "activate": 
			sbm.ExternalCommunicator.request({req: "gameDisable"}); 
			break; 
		}
	} 
};
		
		
		
		
		
		
		
		
		
		
		
		
		
		