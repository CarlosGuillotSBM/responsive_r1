<?php
/*
This file is called from PlaynGo Server after a player has launched a game in a new browser window.
PlaynGo do not have a Session based API where they return our session.
We have to only use PlayerID and map it in xTxGame to our internal session and lock the ip to 92.43.213.66 (their server)

Here are their calls
Flash:
<authenticate><username>7</username><password></password><extra></extra><productId>121</productId><client /><CID /><clientIP>121.210.130.62</clientIP><contextId>0</contextId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><language>en_GB</language><gameId>225</gameId></authenticate>
<balance><externalId>7</externalId><productId>121</productId><currency>GBP</currency><gameId>225</gameId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken></balance>
<reserve><externalId>7</externalId><productId>121</productId><transactionId>382972280</transactionId><real>0.75</real><currency>GBP</currency><gameId>225</gameId><gameSessionId>2522686</gameSessionId><contextId>0</contextId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><roundId>188719926</roundId></reserve>
<release><externalId>7</externalId><productId>121</productId><transactionId>382972520</transactionId><real>0.50</real><currency>GBP</currency><gameSessionId>2522686</gameSessionId><contextId>0</contextId><state>0</state><type>0</type><gameId>225</gameId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><roundId>188719926</roundId><jackpotGain>0.00</jackpotGain><jackpotLoss>0.00</jackpotLoss><freegameExternalId /><turnover>0</turnover><freegameFinished>0</freegameFinished></release>

Mobile:
<authenticate><username>7</username><password></password><extra></extra><productId>121</productId><client /><CID /><clientIP>121.210.130.62</clientIP><contextId>0</contextId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><language>en_GB</language><gameId>100243</gameId></authenticate>
<balance><externalId>7</externalId><productId>121</productId><currency>GBP</currency><gameId>100243</gameId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken></balance>
<reserve><externalId>7</externalId><productId>121</productId><transactionId>383126865</transactionId><real>0.75</real><currency>GBP</currency><gameId>100243</gameId><gameSessionId>2523864</gameSessionId><contextId>0</contextId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><roundId>188796273</roundId></reserve>
<release><externalId>7</externalId><productId>121</productId><transactionId>383127222</transactionId><real>1.35</real><currency>GBP</currency><gameSessionId>2523864</gameSessionId><contextId>0</contextId><state>0</state><type>0</type><gameId>100243</gameId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><roundId>188796273</roundId><jackpotGain>0.00</jackpotGain><jackpotLoss>0.00</jackpotLoss><freegameExternalId /><turnover>0</turnover><freegameFinished>0</freegameFinished></release>

Response status codes to return back to PlaynGo
Status Code         Value   Description
OK                    0     Request successful.
NOUSER                1     User was not logged in.
INTERNAL              2     Internal server error.
INVALIDCURRENCY       3     An unsupported currency was specified.
WRONGUSERNAMEPASSWORD 4     Wrong username or password.
ACCOUNTLOCKED         5     Account is locked.
ACCOUNTDISABLED       6     Account is disabled.
NOTENOUGHMONEY        7     The requested amount is too high or too low.
UNAVAILABLE           8     The system is unavailable for this request. Try again later.

Modifications:
08Oct13 AF - If its a void and we did record a wager for the game (code 313), return a void success anyway with agsRef = ""
15Oct13 AF - $subref is their transactionId - ASSUME BIGINT!
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "PlaynGo";
$GameProviderID   = 14;
function ErrorResponse ($errCode, $data, $Response) {
  global $glo, $GameProviderCode, $PlayerID, $RequestMethod, $CurrencyCode;
  if ($Response != "") {
    switch ($errCode) {
      case E_INVALID_PLAYER_SESSION:
      case E_PLAYER_NOT_FOUND:
        $Response = str_replace("#statusCode#", "1", $Response);
        $Response = str_replace("#statusMessage#", "Your session has expired. Please log in again", $Response);
        $Response.= "<real>0</real>";
        $Response.= "<currency>".$CurrencyCode."</currency>";
        break;
      case E_INVALID_AUTH:
        $Response = str_replace("#statusCode#", "4", $Response);
        $Response = str_replace("#statusMessage#", "Authentication Error", $Response);
        $Response.= "<real>0</real>";
        $Response.= "<currency>".$CurrencyCode."</currency>";
        break;
      case E_INVALID_GAMEID:
      case E_GAME_DISABLED:
        $Response = str_replace("#statusCode#", "8", $Response);
        $Response = str_replace("#statusMessage#", "This game is currently unavailable", $Response);
        $Response.= "<externalId>".$PlayerID."</externalId>";
        $Response.= "<real>0</real>";
        break;
      case E_INSUFFICIENT_FUNDS:
        $Response = str_replace("#statusCode#", "2", $Response);
        $Response = str_replace("#statusMessage#", "You do not have enough funds for this wager. Please visit the cashier.", $Response);
        $Response.= "<externalTransactionId></externalTransactionId>";
        $Response.= "<real>0</real>";
        $Response.= "<currency>".$CurrencyCode."</currency>";
        break;
      default:
        $Response = str_replace("#statusCode#", "2", $Response);
        $Response = str_replace("#statusMessage#", "An error has occurred. Please re-launch the game.", $Response);
        break;
    }
  }

  if ($Response == "") {
    $Response="err";
  } else {
    $Response.="</$RequestMethod>";
  }
  $data = (($Response == "err") ? "" : "SEND: ".$Response."   ").$data;
  if ($PlayerID != 0)
    WriteLog($GameProviderCode, $errCode, $data, $PlayerID);

  echo $Response;
  exit;
}
$PlayerID = 0;
$RequestMethod = "";
$Response = "";

//Need to wrap their requests in something - PlaynGo
$rawRequest  = "<PlaynGo>".file_get_contents("php://input")."</PlaynGo>";
//$rafa = "<authenticate><username>226</username><password></password><extra></extra><productId>121</productId><client /><CID /><clientIP>121.210.130.62</clientIP><contextId>0</contextId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><language>en_GB</language><gameId>100250</gameId></authenticate>";
//$rafa = "<balance><externalId>226</externalId><productId>121</productId><currency>GBP</currency><gameId>100250</gameId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken></balance>";
//$rafa = "<reserve><externalId>226</externalId><productId>121</productId><transactionId>383126865</transactionId><real>0.75</real><currency>GBP</currency><gameId>100250</gameId><gameSessionId>2523864</gameSessionId><contextId>0</contextId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><roundId>188796273</roundId></reserve>";
//$rafa = "<release><externalId>226</externalId><productId>121</productId><transactionId>383127222</transactionId><real>1.35</real><currency>GBP</currency><gameSessionId>2523864</gameSessionId><contextId>0</contextId><state>0</state><type>0</type><gameId>100250</gameId><accessToken>Afgf23cfKaJJ23k15kdN1</accessToken><roundId>188796273</roundId><jackpotGain>0.00</jackpotGain><jackpotLoss>0.00</jackpotLoss><freegameExternalId /><turnover>0</turnover><freegameFinished>0</freegameFinished></release>";
//$rawRequest  = "<PlaynGo>".$rafa."</PlaynGo>";
$GameRequest = simplexml_load_string($rawRequest);
//WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, 1);
//exit;
if ($GameRequest === false) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $rawRequest [PlaynGo Failed to load xml]", $Response, $PlayerID);
}


WriteLog($GameProviderCode, 0, "LOG: " . print_r($GameRequest,true), 460);


if (isset($GameRequest->authenticate)) {
  $RequestMethod = "authenticate";
  $GameRequest = $GameRequest->authenticate;
}

if (isset($GameRequest->balance)) {
  $RequestMethod = "balance";
  $GameRequest = $GameRequest->balance;
}

if (isset($GameRequest->reserve)) {
  $RequestMethod = "reserve";
  $GameRequest = $GameRequest->reserve;
}

if (isset($GameRequest->release)) {
  $RequestMethod = "release";
  $GameRequest = $GameRequest->release;
}

if (isset($GameRequest->cancelReserve)) {
  $RequestMethod = "cancelReserve";
  $GameRequest = $GameRequest->cancelReserve;
}

if ($RequestMethod == "") {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $rawRequest [No RequestMethod]", $Response, $PlayerID);
}





//Generate common response header
$Response = "<$RequestMethod><statusCode>#statusCode#</statusCode><statusMessage>#statusMessage#</statusMessage>";

/*
Since PaynGo have a rubbish integration methodology, we have to get our session id from our db in xTxGame based solely on the PlayerID
*/
if ($RequestMethod == "authenticate") {
	$PlayerID = intval ($GameRequest->username);
} else {
	$PlayerID = intval ($GameRequest->externalId);
}
$SessionID          = "00000000-0000-0000-0000-000000000000";//the idiots dont have a way to send our sessionid back - we have to get it from our db
$GameLaunchSourceID = 0;//the idiots dont have a way to send our GameLaunchSourceID back - we have to get it from our db
$GameProviderGameID = $GameRequest->gameId;//etc etc...
$CurrencyCode = "";

if (isset($GameRequest->currency))
  $CurrencyCode = $GameRequest->currency;

if (empty($PlayerID) || $PlayerID == 0)
  ErrorResponse(E_PLAYER_NOT_FOUND, "PlayerID is empty", $Response, 0);

WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, $PlayerID);

//Check authentication (exits from funtion WriteLog if auth fails)
/*
if ($RequestMethod == "authenticate") {
	
  if (!($GameRequest->accessToken == "Afgf23cfKaJJ23k15kdN1" && $GameRequest->password == ""))
	ErrorResponse(E_INVALID_AUTH, "accessToken:".$GameRequest->accessToken." Password:".$GameRequest->password, $Response);
} else {
  if (!($GameRequest->accessToken == "Afgf23cfKaJJ23k15kdN1"))
	ErrorResponse(E_INVALID_AUTH, "accessToken:".$GameRequest->accessToken, $Response);	
}
*/
$wager  = 0;
$win    = 0;
$ref    = 0;
$subref = 0;
$refund = 0;
$refundref   = "";
$refundnotes = "";

$Action = "";

switch ($RequestMethod) {
  case "authenticate":
  case "balance":
    $Action = "GetBalance";
    break;

  case "reserve":
    $Action = "Tx";
    $wager  = $GameRequest->real;
    $ref    = $GameRequest->roundId;
    $subref = $GameRequest->transactionId;
    break;
  case "release":
    if ($GameRequest->state == 0) {
      $Action = "Tx";
    } else {
      $Action = "TxEndGameRound";
    }
    $win = $GameRequest->real;
    $ref = $GameRequest->roundId;
    $subref = $GameRequest->transactionId;
    if ($ref == 0)
      $Action = "GetBalance";
    break;
  case "cancelReserve":
    $Action = "Void";
    $ref = $GameRequest->roundId;
    $subref = $GameRequest->transactionId;
    $refund = $GameRequest->real;
    $refundref=$ref;
    $refundnotes="PlaynGo Cancel Reserve Ref:$ref SubRef:$subref";
    break;
  default:
    break;
}

$dbSproc = "xTxGame";

$dbParams = array(
          "PlayerID"           => array($PlayerID, "int", 0),
          "SessionID"          => array($SessionID, "str", 36),
          "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),//this is their game server ip - but use anyway
          "GameProviderID"     => array($GameProviderID, "int", 0),
          "GameProviderGameID" => array($GameProviderGameID, "str", 30),
          "GameLaunchSourceID" => array($GameLaunchSourceID, "int", 0),
          "Action"             => array($Action, "str", 15),
          "Wager"              => array($wager, "int", 0),
          "Win"                => array($win, "int", 0),
          "Ref"                => array($ref, "int", 0),
          "SubRef"             => array($subref, "int", 0),
          "Refund"             => array($refund, "int", 0),
          "RefundRef"          => array($refundref, "str", 50),
          "RefundNotes"        => array($refundnotes, "str", 4000),
          "TxWagerTypeID"      => array(0, "int", 0),//will be default
          "TxWinTypeID"        => array(0, "int", 0),//will be default
          "BallygameCycleId"   => array("00000000-0000-0000-0000-000000000000", "str", 36)
         );


/*
db calls xTxGame
Returns:
  Result = 0 : Success
        <> 0 : Various fail codes
*/
require_once("D:/WEB/inc/db.php");

$dbErr = "";
$db = "";

if(!dbInit("","agsMain",1)) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams,1);
  if ($rs === false) {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  } else {
    $row = sqlsrv_fetch_object($rs);
    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }
}

if ($dbErr != "") {
  ErrorResponse($errCode, $dbErr, $Response);
}

dbFree($rs);
dbClose($db);

if (isset($row->CurrencyCode))
  $CurrencyCode = $row->CurrencyCode;

//If its a void and we did record a wager for the game, return a void success anyway
if ($row->Result == 313) {
	$row->agsRef = 0;
  $row->Result = 0;
}

//db access was successful, other error (or already processed)
if ($row->Result != 0) {
  if ($row->Result == 104 || $row->Result == 105)
    $row->Result = 104;

  if ($row->Result == 307) {
    ErrorResponse(E_DUPLICATE_REF, "", $Response);
  } else {
    ErrorResponse($row->Result, "", $Response);
  }
}

/*
Success - make response
*/
$Response = str_replace("#statusCode#", "0", $Response);
$Response = str_replace("#statusMessage#", "", $Response);

if ($RequestMethod == "authenticate") {
  //$Response.= "<externalId>".$PlayerID."</externalId>";
  $Response.= "<externalId>".$PlayerID."</externalId>";
  $Response.= "<userCurrency>".$row->CurrencyCode."</userCurrency>";
  $Response.= "<country>".$row->CountryCode."</country>";
  $Response.= "<nickname>".$PlayerID."</nickname>";
  $Response.= "<birthdate>".$row->DOB."</birthdate>";
  $Response.= "<registration>".$row->RegDate."</registration>";
  $Response.= "<language>en</language>";
  //$Response.= "<affiliateId>$PlayerID</affiliateId>";
  $Response.= "<real>".$row->Balance."</real>";
  $Response.= "<gender>".$row->Gender."</gender>";
}

if ($RequestMethod == "balance") {
  $Response.= "<real>".$row->Balance."</real>";
  $Response.= "<currency>".$row->CurrencyCode."</currency>";
}

if ($RequestMethod == "reserve") {
  $Response.= "<externalTransactionId>".$row->agsRef."</externalTransactionId>";
  $Response.= "<real>".$row->Balance."</real>";
  $Response.= "<currency>".$row->CurrencyCode."</currency>";
}

if ($RequestMethod == "release") {
  $Response.= "<externalTransactionId>".$row->agsRef."</externalTransactionId>";
  $Response.= "<real>".$row->Balance."</real>";
  $Response.= "<currency>".$row->CurrencyCode."</currency>";
}

if ($RequestMethod == "cancelReserve") {
  $Response.= "<externalTransactionId>".(($row->agsRef == 0) ? "" : $row->agsRef)."</externalTransactionId>";
}

$Response.="</$RequestMethod>";

WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);

echo $Response;
exit;
?>