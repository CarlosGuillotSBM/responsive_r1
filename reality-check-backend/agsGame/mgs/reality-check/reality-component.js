var sbm = sbm || {};

/**************************************************************************************************
											UTILS
***************************************************************************************************/
sbm.utils = {
	
	getUrlParameterByName: function (name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search.replace(/&amp;/g, '&'));
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	},
	
	minutesToHours: function (time) {

		if (time < 1) {
			return "few seconds";
		} else if (time >= 1 && time < 60) {
			return time + " minutes";
		} else {

			var hours = Math.floor(time / 60);
			var minutes = (time % 60);
			
			if (minutes === 0) {
				if (hours === 1) return hours + " hour";
				return hours + " hours";
			}
			return hours + "h" + minutes + "m";
		}
	},
	
	output: function (msg) {
		console && console.log("%c SBM RC: " + msg, "background: #222; color: #bada55");
	}
};

/**************************************************************************************************
										DATASERVICE
***************************************************************************************************/
sbm.dataservice = {

	ajax: function (data, callback) {
		$.ajax(
			{
				"url": "/realityCheck.php",
				"dataType": "json",
				"timeout" : 10000,
				"data": data
			}
		)
		.done(function (data) {
			callback && callback(data);
		})
		.fail(function (jqXHR, textStatus) {
			callback && console.log(jqXHR, textStatus);
		});
	}	

};

/**************************************************************************************************
									REALITY CHECK
***************************************************************************************************/

sbm.reality = (function (sbm, $) {

	// player's settings

	var interval = 0;
	var timeElapsed = 0;
	var homeUrl = "";
	var historyUrl = "";
	
	// stop game
	var stopGame  = function () {
		mgs.inGameInterface.setMode(mgs.inGameInterface.modes.fullscreen);
		mgs.inGameInterface.preventGameplay();
		sbm.utils.output("Sent request to resume the game");
	};
	
	// resume game
	var resumeGame = function  () {
		mgs.inGameInterface.setMode(mgs.inGameInterface.modes.hidden);
		mgs.inGameInterface.allowGameplay();
		sbm.utils.output("Sent request to resume the game");
	};
	
	// Start reality check timer
	var startTimer = function (interval) {

		setTimeout(function() {

			stopGame();
			
			// set display
			
			timeElapsed += interval;
			var elapsedFormated = sbm.utils.minutesToHours(timeElapsed);
			var intervalFormated = sbm.utils.minutesToHours(interval);
			
			$(".ui-interval").text(intervalFormated);
			$(".ui-elapsed").text(elapsedFormated);
		
		}, interval * 60000);

	};	
	
	// initialization
	var init = function (mgs, player) {

		if (!mgs)  {
			throw "Could not find MGS interface";
		}
		
		mgs.inGameInterface.setMode(mgs.inGameInterface.modes.hidden);

		/*
		 	Attach DOM events
		*/

		$(".continue-cta").on("click", function () {
			resumeGame();
			// start timer
			startTimer(interval);
		});
		
		$(".stop-cta").on("click", function () {
			resumeGame();
			// stop reminders
			var data = {
				"Function": "no-reminders",
				"SessionID": player.sessionId,
				"PlayerID": player.playerId
			}
			sbm.dataservice.ajax(data);
		});
		
		$(".home-cta").on("click", function () {
			// end game and go home
			mgs.inGameInterface.toLobby();
		});

		$(".activity-cta").on("click", function () {
			// end game and go to history
			top.location.href = historyUrl;
		});

		// callback
		var gotSettings = function (data) {
	
			if (data.Code == 0) {
				
				homeUrl = data.HomeURL;
				historyUrl = data.HistoryURL;
				interval = data.Interval;
				
				sbm.utils.output("Interval = " + interval);

				data.Interval > 0 && startTimer(interval);
			}
		
		};
		
		// get player settings
		var data = {
			"Function": "get-settings",
			"SessionID": player.sessionId,
			"PlayerID": player.playerId
		};
		sbm.dataservice.ajax(data, gotSettings);
	}

	return {
		init: init	
	}

}(sbm, jQuery));

/**************************************************************************************************
									STARTING POINT
***************************************************************************************************/
(function (sbm, $) {

	var sessionId = $.cookie("SessionID"),
		playerId = $.cookie("PlayerID");
	
	var p = sbm.utils.getUrlParameterByName("p").replace("http", "https");
	if (playerId === "396186") {
		
		var onInterfaceLoaded = function () {
			sbm.reality.init(window.mgs, { sessionId: sessionId, playerId: playerId});
		};
		
		$.ajax({
		  url: p + "/MobileWebGames/js/InterfaceApi/InterfaceApi.js",
		  dataType: "script",
		  success: function () {
			window.mgs.inGameInterface.init(onInterfaceLoaded);
		  }
		});
		
	}

}(sbm, jQuery));