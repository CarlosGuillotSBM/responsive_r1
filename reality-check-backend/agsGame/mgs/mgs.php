<?php
/*
This file is called from MGS Vanguard Server after a player has launched a game in a new browser window.
It can also be called by MGS Vanguard with a [Ping] request to determine if the system is active

AF 04Mar12 - Added PlayerID to WriteLog params for game logs
AF 06Mar12 - Added GameLaunchSourceID
AF 19Mar12 - RefundBet will return a success if the transaction was found or not
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "MGS";
$GameProviderID   = 6;//MGS=6

function ErrorResponse ($errCode, $data, $Response) {
	global $glo, $GameProviderCode, $PlayerID;
	if ($Response != "") {
	  $Response = str_replace("#Success#","0",$Response);
	  $Response.="<Error Type=\"string\" Value=\"".@$glo["LogCodes"][$errCode]."\" />";
	  $Response.="<ErrorCode Type=\"int\" Value=\"".$errCode."\" />";
	  $Response.="</Returnset></Result></PKT>";
	}

  $data = (($Response == "") ? "" : "SEND: ".$Response."   ").$data;
  WriteLog($GameProviderCode, $errCode, $data, $PlayerID);

  echo $Response;
  exit;
}

$PlayerID = 0;
$RequestMethod = "";
$Response = "";

$rawRequest = file_get_contents("php://input");
$xml = simplexml_load_string($rawRequest);

if ($xml === false) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $rawRequest [MGS Failed to load xml]", $Response, $PlayerID);
}

//Ping call
if (isset($xml->Method["Name"]) && @$xml->Method["Name"] == "Ping") {
  $RequestMethod = "Ping";
  //WriteLog($GameProviderCode,0,"RECV: ".$rawRequest, $PlayerID);//remove after testing
  //$Response = "<?xml version=\"1.0\" encoding=\"UTF-8\"\?\>";
  $Response = "<PKT><Result Name=\"Ping\" Success=\"1\"><Returnset><ClientIp Type=\"string\" Value=\"".$_SERVER["REMOTE_ADDR"]."\" /></Returnset></Result></PKT>";
  echo $Response;
  exit;
}

if (isset($xml->Method["Name"])) {
  $RequestMethod = $xml->Method["Name"];

  if (!( $RequestMethod == "GetAccountDetails" ||
         $RequestMethod == "RefreshToken" ||
         $RequestMethod == "GetBalance" ||
         $RequestMethod == "PlaceBet" ||
         $RequestMethod == "AwardWinnings" ||
         $RequestMethod == "RefundBet" ||
         $RequestMethod == "EndGame")) {
    $RequestMethod = "";
  }
}

if ($RequestMethod == "") {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $rawRequest [No RequestMethod]", $Response, $PlayerID);
}

//Make arrays of the xml request
$GameRequest = XMLtoArray($xml->Method);
//echo print_r($GameRequest);//use charles to see this data

/*
Token is $_POST["SessionID"].str_pad($GameLaunchSourceID, 3, "0", STR_PAD_LEFT).str_pad($GameID, 4, "0", STR_PAD_LEFT).$_POST["PlayerID"];
eg 2FF221F6-E9AD-420B-A298-2495C4E218A5LLLGGGGPPPPPPPP
where
  1st 36 chars are sessionid (0,36)
  LLL=GameLaunchSourceID     (36,3)
  GGGG=Game.ID               (39,4)
  PPPPPPPP=Player.ID         (43...)
*/

$GameLaunchSourceID = intval (substr($GameRequest["Params"]["Token"]["@attributes"]["Value"],36,3));
//$PlayerID           = substr($GameRequest["Params"]["Token"]["@attributes"]["Value"],39);
$PlayerID           = substr($GameRequest["Params"]["Token"]["@attributes"]["Value"],43);
if (empty($PlayerID)) {
  ErrorResponse(E_PLAYER_NOT_FOUND, "PlayerID is empty", $Response, 0);
}
WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, $PlayerID);

//Generate common response header
//$Response = "<?xml version=\"1.0\" encoding=\"UTF-8\"\?\>";
$Response = "<PKT><Result Name=\"".$RequestMethod."\" Success=\"#Success#\"><Returnset>";

//Check authentication (exits from funtion WriteLog if auth fails)
if (!(substr($GameRequest["Auth"]["@attributes"]["Login"],0,3) == "mgs" && $GameRequest["Auth"]["@attributes"]["Password"] == "wF63yhh9Jm")) {
  ErrorResponse(E_INVALID_AUTH, "Login:".$GameRequest["Auth"]["@attributes"]["Login"]." Password:".$GameRequest["Auth"]["@attributes"]["Password"], $Response);
}

$wager=0;
$win=0;
$ref=0;
$subref = -1;
$refund = 0;
$refundref="";
$refundnotes="";

/*
MGS only sends a single [Token] to identify the player.
We send this [Token] when launching the game.
The [Token] we send is a single string being the <SessionID><GameLaunchSourceID><GameID><PlayerID>
When we receive the Token from a Vanguard call, need to split it (see comments above).
*/
$SessionID = substr($GameRequest["Params"]["Token"]["@attributes"]["Value"],0,36);

/*
This code refers to MGS preferred 'dynamic' token system which we will use if we get problems with the static token
$SessionID = $GameRequest["Params"]["Token"]["@attributes"]["Value"];
$PlayerID  = 0;//We dont know the player id - the ags db will look it up from MGSSession table
*/
//$GameProviderGameID = "";//test game id
$GameProviderGameID = substr($GameRequest["Params"]["Token"]["@attributes"]["Value"],39,4);//this is actually our internal Game.ID - xTxGame will use this for MGS games for a GetBalance call
$AlreadyProcessed   = "false";

$Action = "";
switch ($RequestMethod) {
  case "GetAccountDetails":
	  $Action = "CheckSession";
		break;
  case "RefreshToken":
	  $Action = "RefreshSession";
		break;
  case "GetBalance":
	  $Action = "GetBalance";
		break;
  case "PlaceBet":
	  $Action = "Tx";
	  $wager  = number_format(($GameRequest["Params"]["BetAmount"]["@attributes"]["Value"]/100),2,".","");
	  $ref    = $GameRequest["Params"]["TransactionID"]["@attributes"]["Value"];
	  $subref = $GameRequest["Params"]["BetReferenceNum"]["@attributes"]["Value"];
    $GameProviderGameID = $GameRequest["Params"]["GameReference"]["@attributes"]["Value"];
		break;
  case "AwardWinnings":
	  $Action = "Tx";
	  $win  = number_format(($GameRequest["Params"]["WinAmount"]["@attributes"]["Value"]/100),2,".","");
	  $ref    = $GameRequest["Params"]["TransactionID"]["@attributes"]["Value"];
	  $subref = $GameRequest["Params"]["WinReferenceNum"]["@attributes"]["Value"];
    $GameProviderGameID = $GameRequest["Params"]["GameReference"]["@attributes"]["Value"];
		break;
  case "RefundBet":
	  $Action = "Void";
	  $ref    = $GameRequest["Params"]["TransactionID"]["@attributes"]["Value"];
	  $subref = $GameRequest["Params"]["BetReferenceNum"]["@attributes"]["Value"];
	  $refund = number_format(($GameRequest["Params"]["RefundAmount"]["@attributes"]["Value"]/100),2,".","");;
    $GameProviderGameID = $GameRequest["Params"]["GameReference"]["@attributes"]["Value"];
    $refundref=$ref;
    $refundnotes="MGS RefundBet Ref:$ref SubRef:$subref";
		break;
  case "EndGame":
	  $Action = "EndGameRound";
	  $ref    = $GameRequest["Params"]["TransactionID"]["@attributes"]["Value"];
    $GameProviderGameID = $GameRequest["Params"]["GameReference"]["@attributes"]["Value"];
		break;
  default:
		break;
}

$dbSproc = "xTxGame";

$dbParams = array(
				  "PlayerID"           => array($PlayerID, "int", 0),
				  "SessionID"          => array($SessionID, "str", 36),
				  "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),//this is game server ip - but use anyway
				  "GameProviderID"     => array($GameProviderID, "int", 0),
				  "GameProviderGameID" => array($GameProviderGameID, "str", 30),
          "GameLaunchSourceID" => array($GameLaunchSourceID, "int", 0),
				  "Action"             => array($Action, "str", 15),
				  "Wager"              => array($wager, "int", 0),
				  "Win"                => array($win, "int", 0),
				  "Ref"                => array($ref, "int", 0),
				  "SubRef"             => array($subref, "int", 0),
				  "Refund"             => array($refund, "int", 0),
				  "RefundRef"          => array($refundref, "str", 50),
				  "RefundNotes"        => array($refundnotes, "str", 4000)
				 );


/*
db calls xTxGame
Returns:
	Result = 0 : Success
	      <> 0 : Various fail codes
*/
require_once("D:/WEB/inc/db.php");

$dbErr = "";
$db = "";

if(!dbInit("","agsMain",1)) {
	$errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams,1);
  if ($rs === false) {
		$errCode = E_DB_SELECT;
	  WriteLog("db", $errCode, $dbErr);
  } else {
    $row = sqlsrv_fetch_object($rs);
    if (!isset($row) || $row==null) {
			$errCode = E_DB_RETURN;
		  WriteLog("db", $errCode, $dbErr);
    }
  }
}

if ($dbErr != "") {
  ErrorResponse($errCode, $dbErr, $Response);
}

dbFree($rs);
dbClose($db);

//db access was successful, other error (or already processed)
if ($row->Result != 0) {
  if ($row->Result == 104 || $row->Result == 105)
    $row->Result = 104;

  if ($row->Result == 307) {
    if ($RequestMethod == "PlaceBet" || $RequestMethod == "AwardWinnings" || $RequestMethod == "RefundBet") {
      $AlreadyProcessed = "true";
    } else {
  	  ErrorResponse(E_DUPLICATE_REF, "", $Response);
    }
  } else {
    if ($RequestMethod != "RefundBet") {
	    ErrorResponse($row->Result, "", $Response);
	  }
  }
}

/*
Success - make response
*/
//send token back
$Response = str_replace("#Success#","1",$Response);
//$Response.= "<Token Type=\"string\" Value=\"".$row->SessionID."\" />";
$Response.= "<Token Type=\"string\" Value=\"".$GameRequest["Params"]["Token"]["@attributes"]["Value"]."\" />";

if ($RequestMethod == "GetAccountDetails") {
  //mgs uses GB for UK
  if ($row->CountryCode == "UK")
    $row->CountryCode = "GB";
  $Response.="<LoginName Type=\"string\" Value=\"".$PlayerID."\" /><Currency Type=\"string\" Value=\"".$row->CurrencyCode."\" /><Country Type=\"string\" Value=\"".$row->CountryCode."\" /><City Type=\"string\" Value=\"".$row->City."\" />";
}

if ($RequestMethod == "GetBalance" || $RequestMethod == "EndGame") {
  $Response.="<Balance Type=\"string\" Value=\"".number_format(($row->Balance*100),0,"","")."\" />";
}

if ($RequestMethod == "PlaceBet" || $RequestMethod == "AwardWinnings" || $RequestMethod == "RefundBet") {
  $Response.="<Balance Type=\"string\" Value=\"".number_format(($row->Balance*100),0,"","")."\" />";
  if ($RequestMethod == "RefundBet" && $row->Result != 0) {
    $Response.="<ExtTransactionID Type=\"string\" Value=\"".$ref."\" />";
  } else {
    $Response.="<ExtTransactionID Type=\"string\" Value=\"".$row->agsRef."\" />";
  }
  $Response.="<AlreadyProcessed Type=\"bool\" Value=\"".$AlreadyProcessed."\" />";
}

$Response.="</Returnset></Result></PKT>";

WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);

echo $Response;
exit;
?>