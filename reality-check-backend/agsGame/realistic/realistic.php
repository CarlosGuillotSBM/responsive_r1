<?php
/*
This file is called from Realistic after a player has launched a realistic game in a new browser window.
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "Realistic";
$GameProviderID   = 16;//Realistic=16
$COGSGuid         = "B31E14B1-BB11-4A37-A154-A8DC8B07570E"; //this is agsMain.Skin.COGSGuid for SkinID=1 (spin and win). Used when Realistic sends a ??? request with no session id. The db will validate this
$accessid         = "zH6tibcztU";//Realistic accessid for them to access our server

function ErrorResponse ($errCode, $data) {
	global $GameProviderCode, $PlayerID;
	
	switch ($errCode) {
		case E_INVALID_AUTH:
		  $RealisticErrCode = 1;
		  break;
		case E_INVALID_PLAYER_SESSION:
		  $RealisticErrCode = 2;
		  break;
		case E_INVALID_TXID:
		  $RealisticErrCode = 3;
		  break;
		case E_INSUFFICIENT_FUNDS:
		  $RealisticErrCode = 4;
		  break;
		case E_DUPLICATE_REF:
		  $RealisticErrCode = 5;
		  break;
		case E_GAME_DISABLED:
		  $RealisticErrCOde = 6;
		  break;
		case E_SELF_EXCLUSION:
		  $RealisticErrCode = 7;
		  break;
		case E_VOID_FAIL_NO_WAGERS:
			$RealisticErrCode = 8;
		  break;
		case E_PLAYER_LIMITS_EXCEEDED:
			$RealisticErrCode = 9;
		  break;
		default:
		  $RealisticErrCode = 999;
		  break;
	}
	
  $Response = "status=invalid&error=$RealisticErrCode";
  $data = "SEND: ".$Response.(($data == "") ? "" : "   ").$data;
  WriteLog($GameProviderCode, $errCode, $data, $PlayerID);

  echo $Response;
  exit;
}
$rawRequest = "";

foreach ($_REQUEST as $var => $value) {
  $_REQUEST[$var] = trim($_REQUEST[$var]);
  $rawRequest.="$var=$value ";
}
$Response = "";

$PlayerID = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] : 0;
WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, $PlayerID);

$errCode = 0;

if (!(@$_REQUEST["accessid"] == $accessid)) {
  ErrorResponse(E_INVALID_AUTH, "");
}

$gameid    = isset($_REQUEST["gameid"]) ? $_REQUEST["gameid"] : 0; //the gameid as used from Realistic games
$SessionID = isset($_REQUEST["guid"])   ? substr($_REQUEST["guid"],0,36) : "";
$Wager     = isset($_REQUEST["wager"])  ? $_REQUEST["wager"]  : -1;
$Win       = isset($_REQUEST["win"])    ? $_REQUEST["win"]    : -1;
$ref       = isset($_REQUEST["round"])  ? $_REQUEST["round"]  : 0;
$subref    = isset($_REQUEST["subRound"])  	? $_REQUEST["subRound"]  : 0;//all their refs should be globally unique
//$subref    = -1;//all realistic subrefs will be 1 as they dont have a way of recordiing a game round - they only record an entire session - so each ref is unique
$type      = isset($_REQUEST["type"])   ? $_REQUEST["type"]   : "";
$refund		= isset($_REQUEST["refAmount"])  ? $_REQUEST["refAmount"]  : 0;
$refundref	= isset($_REQUEST["refRef"])   ? $_REQUEST["refRef"]   : "";
$refundnotes= isset($_REQUEST["refReason"])   ? $_REQUEST["refReason"]   : "";

//$jpwin   = isset($_REQUEST["jpwin"])   ? $_REQUEST["jpwin"]   : 0;
$GameLaunchSourceID = intval(substr($_REQUEST["guid"],36,3));

//Hack for reconciliation purposes. Don't process the request 25% of the times and delay response 5 seconds 20% of the times
// $randNumber = rand ( 1, 100 );
// if($randNumber >= 1 && $randNumber <= 25)
// {
	// sleep ( 5 );
	// ErrorResponse(0, "");
// }
// else if ($randNumber >= 26 && $randNumber <= 50)
	// sleep ( 5 );

if ($Wager < 0 && ($type != "CANCELWAGER" && $type != "BC" ))       ErrorResponse(E_INVALID_WAGER, "");
if ($Win < 0 && ($type != "CANCELWAGER" && $type != "BC"))         ErrorResponse(E_INVALID_WIN, "");
if ($PlayerID == 0)   ErrorResponse(E_INVALID_PLAYER_SESSION, "$PlayerID");
if ($SessionID == "") ErrorResponse(E_INVALID_PLAYER_SESSION, "$SessionID");
if ($ref <= 0 && $type != "BC") ErrorResponse(E_INVALID_TXID, "");
if ($subref <= 0 && $type != "BC")     ErrorResponse(E_INVALID_TXID, "");
if ($type == "")     ErrorResponse(E_INVALID_PARAMS, "");

$RequestMethod = "TxEndGameRound";

if ($type== 'ENDROUND' || ($type == "WAGER" && $Wager == 0 && $Win == 0)) {
  $RequestMethod = "TxEndGameRound";
}
else if ($type == "WAGER") {
  $RequestMethod = "Tx";
}
else if ($type == "BC") {
  $Wager = 0;
	$Win = 0;
  $RequestMethod = "GetBalance";
}
else if ($type == "CANCELWAGER") {
	$Wager = 0;
	$Win = 0;
  $RequestMethod = "Void";
}

$dbErr = "";
$dbSproc = "xTxGame";
$db = "";

$dbParams = array(
    "PlayerID"           => array($PlayerID, "int", 0),
    "SessionID"          => array($SessionID, "str", 36),
    "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),//this is the ip
    "GameProviderID"     => array($GameProviderID, "int", 0),
    "GameProviderGameID" => array($gameid, "str", 30),
    "GameLaunchSourceID" => array($GameLaunchSourceID, "int", 0),
    "Action"             => array($RequestMethod, "str", 15),
    "Wager"              => array($Wager, "int", 0),
    "Win"                => array($Win, "int", 0),
    "Ref"                => array($ref, "int", 0),
    "SubRef"             => array($subref, "int", 0),
    "Refund"             => array($refund, "int", 0),
    "RefundRef"          => array($refundref, "str", 50),
    "RefundNotes"        => array($refundnotes, "str", 4000)
);

/*
db calls xTxGame
Returns:
	Result = 0 : Success
	      <> 0 : Various fail codes
*/
require_once("D:/WEB/inc/db.php");

if(!dbInit("","agsMain",1)) {
	$errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams,1);
  if ($rs === false) {
		$errCode = E_DB_SELECT;
	  WriteLog("db", $errCode, $dbErr);
  } else {
    $row = sqlsrv_fetch_object($rs);
    if (!isset($row) || $row==null) {
			$errCode = E_DB_RETURN;
		  WriteLog("db", $errCode, $dbErr);
    }
  }
}

if ($dbErr != "" || !isSet($row->Result) ) {
  ErrorResponse($errCode, $dbErr, $Response);
}

dbFree($rs);
dbClose($db);

//db access was successful, other error
if ($row->Result != 0) {
  if ($row->Result == 104 || $row->Result == 105)
    $row->Result = 104;

  ErrorResponse($row->Result, "");
}

$Response = "status=ok&bal=".@$row->Balance."&uid=$PlayerID&guid=".$SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT)."&txid=".@$row->agsRef;
WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);
echo $Response;
exit;
?>