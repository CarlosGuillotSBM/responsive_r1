var sbm = sbm || {};

/**************************************************************************************************
											UTILS
***************************************************************************************************/
sbm.utils = {
	
	getUrlParameterByName: function (name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search.replace(/&amp;/g, '&'));
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	},
	
	minutesToHours: function (time) {

		if (time < 1) {
			return "few seconds";
		} else if (time >= 1 && time < 60) {
			return time + " minutes";
		} else {

			var hours = Math.floor(time / 60);
			var minutes = (time % 60);
			
			if (minutes === 0) {
				if (hours === 1) return hours + " hour";
				return hours + " hours";
			}
			return hours + "h" + minutes + "m";
		}
	}
};

/**************************************************************************************************
										DATASERVICE
***************************************************************************************************/
sbm.dataservice = {

	ajax: function (data, callback) {
		$.ajax(
			{
				"url": "https://sec.aquagamesys.net/agsGame/realityCheck.php",
				"dataType": "json",
				"timeout" : 10000,
				"data": data
			}
		)
		.done(function (data) {
			callback && callback(data);
		})
		.fail(function (jqXHR, textStatus) {
			callback && console.log(jqXHR, textStatus);
		});
	}	

};

/**************************************************************************************************
									REALITY CHECK
***************************************************************************************************/

sbm.reality = (function (sbm, communicator, rccomponent, $) {

	// player's settings

	var interval = 0;
	var timeElapsed = 0;
	var homeUrl = "";
	var historyUrl = "";
	
	// Start reality check timer
	var startTimer = function (interval) {

		setTimeout(function() {

			// stop game
			rccomponent.onRealityCheck("activate");
			
			// set display
			
			timeElapsed += interval;
			var elapsedFormated = sbm.utils.minutesToHours(timeElapsed);
			var intervalFormated = sbm.utils.minutesToHours(interval);
			
			$(".ui-interval").text(intervalFormated);
			$(".ui-elapsed").text(elapsedFormated);
		
		}, interval * 60000);

	};
	
	// initialization
	var init = function () {

		communicator.init();
		rccomponent.init();

		/*
		 	Attach DOM events
		*/

		$(".continue-cta").on("click", function () {
			// resume game
			rccomponent.onContinuePlaying();
			// start timer
			startTimer(interval);
		});
		
		$(".stop-cta").on("click", function () {
			// resume game
			rccomponent.onContinuePlaying();
			// stop reminders
			var data = {
				"Function": "no-reminders",
				"SessionID": sbm.utils.getUrlParameterByName("s"),
				"PlayerID": sbm.utils.getUrlParameterByName("p")
			}
			sbm.dataservice.ajax(data);
		});
		
		$(".home-cta").on("click", function () {
			// end game and go home
			rccomponent.onStopPlaying(homeUrl);
		});

		$(".activity-cta").on("click", function () {
			// end game and go to history
			rccomponent.onStopPlaying(historyUrl);
		});

		// callback
		var gotSettings = function (data) {
	
			if (data.Code == 0) {
				
				historyUrl = data.HomeURL;
				homeUrl = data.HistoryURL;
				interval = data.Interval;

				data.Interval > 0 && startTimer(interval);
			}
		
		};

		// get player settings
		var data = {
			"Function": "get-settings",
			"SessionID": sbm.utils.getUrlParameterByName("s"),
			"PlayerID": sbm.utils.getUrlParameterByName("p")
		}
		sbm.dataservice.ajax(data, gotSettings);
	}

	return {
		init: init	
	}

}(sbm, ExternalCommunicator, RCComponent, jQuery));

/**************************************************************************************************
									STARTING POINT
***************************************************************************************************/
(function(sbm) {
	sbm.reality.init();
}(sbm));