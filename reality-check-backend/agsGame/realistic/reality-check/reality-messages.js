ExternalCommunicator = { 
	source: window.parent,//Set at init message 
	targetOrigin: "*",//Set at init message 
	init: function () { 
		window.addEventListener("message", 
			this.processReceivedMessage.bind(this), false); 
		}, 
	//Post message to parent host
	request: function (req) { 
		this.source.postMessage(req, this.targetOrigin); 
	},
	//Events coming from parent host (PNGHostInterface component) 
	processReceivedMessage: function (e) { 
		console.log("RC Component received: ", e, e.data.type); 
		switch (e.data.type) { 
			case "init": 
				//Setup communication with PNGHostInterface 
				this.targetOrigin = e.origin; 
				this.source = e.source; 
				break; 
			//When game is disabled, show message 
			case "gameDisabled": 
				RCComponent.show(); 
				break; 
			//Game is enabled again 
			case "gameEnabled": 63
				break; 
		} 
	}
};

RCComponent = {
	$messagebox: $("#ui-reality-check-container"), 
	init: function () {
		// RC component is ready - inform the game to allow player to place a stake
		ExternalCommunicator.request("REALITY_CHECK_READY"); 
		this.$messagebox.hide();
	}, 
	show: function () { 
		this.$messagebox.show(); 
	}, 
	//Listener for button. 
	//Close message and send enable request to game. 
	onContinuePlaying: function () { 
		this.$messagebox.hide(); 
		ExternalCommunicator.request("GAME_ENABLED"); 
	}, 
	//Listener for button. 
	//Close message and send enable game end request to game. 
	onStopPlaying: function (url) { 
		this.$messagebox.hide(); 
		ExternalCommunicator.request({req: "GAME_END", data: {redirectUrl: url}}); 
	}, 
	onRealityCheck: function (action) { 
		switch (action) { 
		//Reality check is needed, 
		//Send request that the game should disable user interaction 
		case "activate": 
			ExternalCommunicator.request({req: "GAME_DISABLED"}); 
			break; 
		}
	} 
};
		
		
		
		
		
		
		
		
		
		
		
		
		
		