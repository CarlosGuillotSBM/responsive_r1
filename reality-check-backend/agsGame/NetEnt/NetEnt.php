<?php
/*
This is the soap server for NetEnt calls
Use http://dev.dagacube.net/netent/WalletServer3_0.wsdl on DEV
Use http://skin1.aquagamesys.net/netent/WalletServer3_0.wsdl on COREIX
Use https://game.dagacube.net/NetEnt/WalletServer3_0.wsdl on LIVE
*/

require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
require_once("D:/WEB/inc/db.php");

$GameProviderCode = "NetEnt";
$GameProviderID   = 17;
$COGSGuid         = "B31E14B1-BB11-4A37-A154-A8DC8B07570E"; //this is agsMain.Skin.COGSGuid for SkinID=1 (spin and win). Used when Eyecon sends a ??? request with no session id. The db will validate this
$callerId         = "NetEnt";
$callerPassword   = "NetEntTest1";

/*
$rawRequest = file_get_contents("php://input");
$theFileName="d:\agsLogs\NetEnt.log";
$theFileData = date("H:i:s")."\r\nREQUEST: ".str_replace("\n","",$rawRequest);
$fp = fopen($theFileName,"a");
fwrite($fp,$theFileData."\r\n");
fclose($fp);
*/

function ErrorResponse ($PlayerID, $errCode, $data, $Balance=0) {
	global $GameProviderCode;

	switch ($errCode) {
		case E_INVALID_AUTH:
		  $NetEntErrCode = 5;
		  $message = "Your account could not be accessed now. Please try again later.";
		  break;
		case E_INSUFFICIENT_FUNDS:
		  $NetEntErrCode = 1;
		  $message = "You do not have sufficient funds for this wager. Please visit the cashier.";
		  break;
		case E_INVALID_SIGNATURE://meaning illegal currency
		  $NetEntErrCode = 2;
		  $message = "A technical error has occurred. Please try again later.";
		  break;
		case E_INVALID_WIN:
		  $NetEntErrCode = 3;
		  $message = "A technical error has occurred. Please try again later.";
		  break;
		case E_INVALID_WAGER:
		  $NetEntErrCode = 4;
		  $message = "A technical error has occurred. Please try again later.";
		  break;
		case E_PLAYER_LIMITS_EXCEEDED:
		  $NetEntErrCode = 6;
		  $message = "You have reached your wager limit. Please contact our support team.";
		  break;
		default:
		  $NetEntErrCode = 0;
		  $message = "A technical error has occurred. Please try again later.";
		  break;
	}

  $Soapdetail = new SoapVar(array("errorCode" => "$NetEntErrCode", "message" => $message, "balance" => "$Balance"), SOAP_ENC_OBJECT);
  $Response = new SoapFault("$NetEntErrCode", $message, null, $Soapdetail);

  $data = "SEND: ".json_encode($Response).(($data == "") ? "" : "   ").$data;
  WriteLog($GameProviderCode, $errCode, $data, $PlayerID);

  /*
  NOTE: If we require NetEnt to rollback the transaction then send them an http 500, not a soapfault
  */
  return $Response;
}

function DBxTxGame ($RequestMethod, $PlayerID, $GameID="", $Wager=0, $Win=0, $Ref=0, $SubRef=0, $Refund=0, $RefundRef="", $RefundNotes="") {
	global $GameProviderID;

	$dbErr = "";
	$dbSproc = "xTxGame";
	$db = "";
	
	$dbParams = array(
	    "PlayerID"           => array($PlayerID, "int", 0),
	    "SessionID"          => array("00000000-0000-0000-0000-000000000000", "str", 36),//we will need to look it up (sigh)
	    "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),
	    "GameProviderID"     => array($GameProviderID, "int", 0),
	    "GameProviderGameID" => array($GameID, "str", 30),
	    "GameLaunchSourceID" => array(0, "int", 0),
	    "Action"             => array($RequestMethod, "str", 15),
	    "Wager"              => array($Wager, "int", 0),
	    "Win"                => array($Win, "int", 0),
	    "Ref"                => array($Ref, "int", 0),
	    "SubRef"             => array($SubRef, "int", 0),
	    "Refund"             => array($Refund, "int", 0),
	    "RefundRef"          => array($RefundRef, "str", 50),
	    "RefundNotes"        => array($RefundNotes, "str", 4000)
	);

	/*
	db calls xTxGame
	Returns:
		Result = 0 : Success
		      <> 0 : Various fail codes
	*/

	if(!dbInit("","Main",1)) {
		$errCode = E_DB_CONNECT;
	  WriteLog("db", $errCode, $dbErr);
	} else {
	  $rs = dbExecSproc($dbSproc,$dbParams,1);
	  if ($rs === false) {
			$errCode = E_DB_SELECT;
		  WriteLog("db", $errCode, $dbErr);
	  } else {
	    $row = sqlsrv_fetch_object($rs);
	    if (!isset($row) || $row==null) {
				$errCode = E_DB_RETURN;
			  WriteLog("db", $errCode, $dbErr);
	    }
	  }
	}

	if ($dbErr != "")
	  return ErrorResponse($PlayerID, $dbErr, "");

	dbFree($rs);
	dbClose($db);

	//db access was successful, other error
	if ($row->Result != 0) {
	  if ($row->Result == 104 || $row->Result == 105)
	    $row->Result = 104;
	
	  return ErrorResponse($PlayerID, $row->Result, "", $row->Balance);
	}

	return $row;
}

class WebService {

  //getPlayerCurrency-----------------------------------------------------------------------------------------------------------
  function getPlayerCurrency($params) {
  	global $GameProviderCode, $callerId, $callerPassword;
  	/*
		String callerId
		String callerPassword
		String playerName
		String sessionId (optional)
  	*/
  	$PlayerID = substr(@$params->playerName,3);//they prefix the playerid with XX_ where XX is the brand - SW, KB, LP etc
    WriteLog($GameProviderCode, 0, "RECV: getPlayerCurrency ".json_encode($params), $PlayerID);

  	if (@$params->callerId != $callerId || @$params->callerPassword != $callerPassword)
  	  return ErrorResponse($PlayerID, E_INVALID_AUTH, "Auth invalid");

  	if (!is_numeric($PlayerID))
  	  return ErrorResponse($PlayerID, E_INVALID_PARAMS, "NetEnt playerName invalid (playerName=".@$params->playerName.")");

		//$response = array();
		//$response[] = new SoapVar("NetEnt",XSD_STRING,NULL,NULL,"callerId");
		//$request = new SoapVar($params, SOAP_ENC_OBJECT, NULL, NULL,"getPlayerCurrency");

    $row = DBxTxGame($RequestMethod="CheckSession", $PlayerID, $GameID="", $Wager=0, $Win=0, $Ref=0, $SubRef=0, $Refund=0, $RefundRef="", $RefundNotes="");

    if (isset($row->Result)) {//a successful return from the database call. If not, its a soap fault oblect
	    $Response = new SoapVar(array(
	      "currencyIsoCode" => "$row->CurrencyCode"
	    ), SOAP_ENC_OBJECT);
    } else {
    	$Response = $row;
    }
    
    WriteLog($GameProviderCode, 0, "SEND: ".json_encode($Response), $PlayerID);

    return $Response;
  }

  //getBalance-----------------------------------------------------------------------------------------------------------
  function getBalance($params) {
  	global $GameProviderCode, $callerId, $callerPassword;
  	/*
		String callerId
		String callerPassword
		String playerName
		String currency
		String gameId (optional)
		String sessionId (optional)
  	*/

  	$PlayerID = substr(@$params->playerName,3);
    WriteLog($GameProviderCode, 0, "RECV: getBalance ".json_encode($params), $PlayerID);

  	if (@$params->callerId != $callerId || @$params->callerPassword != $callerPassword)
  	  return ErrorResponse($PlayerID, E_INVALID_AUTH, "Auth invalid");

  	if (isset($params->currency) && (@$params->currency != "GBP" && @$params->currency != "EUR" && @$params->currency != "CAD" && @$params->currency != "AUD"))
  	  return ErrorResponse($PlayerID, E_INVALID_SIGNATURE, "Currency invalid");

  	if (!is_numeric($PlayerID))
  	  return ErrorResponse($PlayerID, E_INVALID_PARAMS, "NetEnt playerName invalid (playerName=".@$params->playerName.")");

    $gameId = (isset($params->gameId) ? $params->gameId : "");

    $row = DBxTxGame($RequestMethod="GetBalance", $PlayerID, $GameID=$gameId, $Wager=0, $Win=0, $Ref=0, $SubRef=0, $Refund=0, $RefundRef="", $RefundNotes="");

    if (isset($row->Result)) {//a successful return from the database call. If not, its a soap fault oblect
    	//if ($row->Balance < 0.01) {
  	  //  return ErrorResponse($PlayerID, E_INSUFFICIENT_FUNDS, "NetEnt zero balance (playerName=".@$params->playerName.")");
    	//} else {
			  $Response = new SoapVar(array(
			    "balance" => "$row->Balance"
			  ), SOAP_ENC_OBJECT);
			//}
    } else {
    	$Response = $row;
    }

    WriteLog($GameProviderCode, 0, "SEND: ".json_encode($Response), $PlayerID);

    return $Response;
  }

  //withdrawAndDeposit-----------------------------------------------------------------------------------------------------------
  function withdrawAndDeposit($params) {
  	global $GameProviderCode, $callerId, $callerPassword;
  	/*
		String callerId,
		String callerPassword,
		String playerName,
		double withdraw,
		double deposit,
		boolean bigWin,
		double jackpotAmount,
		double bonusWin,
		double bonusBet,
		BonusPrograms bonusPrograms,
		Tournaments tournaments,
		JackpotContributions jackpotContributions,
		String currency,
		String transactionRef,
		String gameRoundRef,
		String gameId,
		String reason,
		String source,
		String startDate,
		String sessionId

    RECV: withdrawAndDeposit{
      "callerId":"NetEnt",
      "callerPassword":"NetEntTest1",
      "playerName":"KIT402",
      "withdraw":1,
      "deposit":0,
      "bonusWin":0,
      "currency":"GBP",
      "transactionRef":5,
      "gameRoundRef":5,
      "gameId":"starburst_sw",
      "reason":"GAME_PLAY_FINAL",
      "sessionId":"1413351208966-53-J53D34S3I2EU8"
		*/

  	$PlayerID = substr(@$params->playerName,3);
    WriteLog($GameProviderCode, 0, "RECV: withdrawAndDeposit ".json_encode($params), $PlayerID);

  	if (@$params->callerId != $callerId || @$params->callerPassword != $callerPassword)
  	  return ErrorResponse($PlayerID, E_INVALID_AUTH, "Auth invalid");

  	if (isset($params->currency) && (@$params->currency != "GBP" && @$params->currency != "EUR" && @$params->currency != "CAD" && @$params->currency != "AUD"))
  	  return ErrorResponse($PlayerID, E_INVALID_SIGNATURE, "Currency invalid");

  	if (!is_numeric($PlayerID))
  	  return ErrorResponse($PlayerID, E_INVALID_PARAMS, "NetEnt playerName invalid (playerName=".@$params->playerName.")");

    //transactionRef is unique
    if (isset($params->gameRoundRef)) {
    	$Ref = $params->gameRoundRef;
    } else {
    	$Ref = $params->transactionRef;
    }
  	$SubRef = $params->transactionRef;

    $Refund = 0;
    $RefundRef = "";
    $RefundNotes = "";

  	//CLEAR_HANGED_GAME_STATE is effectively a refund
    if (@$params->reason == "CLEAR_HANGED_GAME_STATE") {
      $RequestMethod = "Void";
    	$Refund = $params->deposit;
    	$RefundRef = $SubRef;
    	$RefundNotes = "NetEnt Clear Hanged Game State Refund Bet";
    }

    if (@$params->reason == "GAME_PLAY_FINAL") {
    	$RequestMethod = "TxEndGameRound";
    } else {
    	$RequestMethod = "Tx";
    }

    $row = DBxTxGame($RequestMethod, $PlayerID, $GameID=$params->gameId, $Wager=$params->withdraw, $Win=$params->deposit, $Ref, $SubRef, $Refund, $RefundRef, $RefundNotes);

    if (isset($row->Result)) {//a successful return from the database call. If not, its a soap fault oblect
		  $Response = new SoapVar(array(
		    "newBalance"    => "$row->Balance",
		    "transactionId" => "$row->agsRef"
		  ), SOAP_ENC_OBJECT);
    } else {
    	$Response = $row;
    }

    WriteLog($GameProviderCode, 0, "SEND: ".json_encode($Response), $PlayerID);

    return $Response;
  }

  //deposit-----------------------------------------------------------------------------------------------------------
  function deposit($params) {
  	global $GameProviderCode, $callerId, $callerPassword;
  	/*
		String callerId,
		String callerPassword,
		String playerName,
		double amount,
		BonusProgram bonusPrograms,
		Tournaments tournaments,
		Boolean bigWin,
		Double jackpotAmount,
		Double bonusWin,
		String currency,
		String transactionRef,
		String gameRoundRef,
		String gameId,
		String reason,
		String source,
		String startDate,
		String sessionId
    
    RECV: deposit{"callerId":"NetEnt","callerPassword":"NetEntTest1","playerName":"KB_960","amount":100,"bonusPrograms":{"bonus":{"depositionId":1}},"currency":"GBP","transactionRef":705,"reason":"WAGERED_BONUS"}
		*/

  	$PlayerID = substr(@$params->playerName,3);
    $gameId = (isset($params->gameId) ? $params->gameId : "");

  	if (@$params->callerId != $callerId || @$params->callerPassword != $callerPassword)
  	  return ErrorResponse($PlayerID, E_INVALID_AUTH, "Auth invalid");

  	if (isset($params->currency) && (@$params->currency != "GBP" && @$params->currency != "EUR" && @$params->currency != "CAD" && @$params->currency != "AUD"))
  	  return ErrorResponse($PlayerID, E_INVALID_SIGNATURE, "Currency invalid");

    if (@$params->reason == "WAGERED_BONUS")//use pseudo game id 17998 as the release to real from a WAGERED_BONUS is not assigned to a specific game
      $gameId = "NetEntBonusWin";

    if (@$params->reason == "AWARD_TOURNAMENT_WIN")//use pseudo game id 17999 as the real payout from a AWARD_TOURNAMENT_WIN
      $gameId = "NetEntTournamentWin";

    WriteLog($GameProviderCode, 0, "RECV: deposit ".json_encode($params), $PlayerID);

  	if (!is_numeric($PlayerID))
  	  return ErrorResponse($PlayerID, E_INVALID_PARAMS, "NetEnt playerName invalid (playerName=".@$params->playerName.")");

    //transactionRef is unique
    if (isset($params->gameRoundRef)) {
    	$Ref = $params->gameRoundRef;
    } else {
    	$Ref = $params->transactionRef;
    }
  	$SubRef = $params->transactionRef;
  	
    $Refund = 0;
    $RefundRef = "";
    $RefundNotes = "";

  	//CLEAR_HANGED_GAME_STATE is effectively a refund
    if (@$params->reason == "CLEAR_HANGED_GAME_STATE") {
      $RequestMethod = "Void";
    	$Refund = $params->amount;
    	$RefundRef = $SubRef;
    	$RefundNotes = "NetEnt Clear Hanged Game State Refund Wager";
    }

    
    if (@$params->reason == "GAME_PLAY_FINAL" || @$params->reason == "AWARD_TOURNAMENT_WIN" || @$params->reason == "WAGERED_BONUS") {
    	$RequestMethod = "TxEndGameRound";
    } else {
    	$RequestMethod = "Tx";
    }

    $row = DBxTxGame($RequestMethod, $PlayerID, $GameID=$gameId, $Wager=0, $Win=$params->amount, $Ref, $SubRef, $Refund, $RefundRef, $RefundNotes);

    if (isset($row->Result)) {//a successful return from the database call. If not, its a soap fault oblect
		  $Response = new SoapVar(array(
		    "balance"    => "$row->Balance",
		    "transactionId" => "$row->agsRef"
		  ), SOAP_ENC_OBJECT);
    } else {
    	$Response = $row;
    }

    WriteLog($GameProviderCode, 0, "SEND: ".json_encode($Response), $PlayerID);

    return $Response;
  }

  //withdraw-----------------------------------------------------------------------------------------------------------
  function withdraw($params) {
  	global $GameProviderCode, $callerId, $callerPassword;
  	/*
		String callerId,
		String callerPassword,
		String playerName,
		double amount,
		double bonusBet,
		JackpotContributions jackpotContributions,
		String currency,
		String transactionRef,
		String gameRoundRef,
		String gameId,
		String reason,
		String sessionId		
		*/

  	$PlayerID = substr(@$params->playerName,3);
    WriteLog($GameProviderCode, 0, "RECV: withdraw ".json_encode($params), $PlayerID);

  	if (@$params->callerId != $callerId || @$params->callerPassword != $callerPassword)
  	  return ErrorResponse($PlayerID, E_INVALID_AUTH, "Auth invalid");

  	if (isset($params->currency) && (@$params->currency != "GBP" && @$params->currency != "EUR" && @$params->currency != "CAD" && @$params->currency != "AUD"))
  	  return ErrorResponse($PlayerID, E_INVALID_SIGNATURE, "Currency invalid");

  	if (!is_numeric($PlayerID))
  	  return ErrorResponse($PlayerID, E_INVALID_PARAMS, "NetEnt playerName invalid (playerName=".@$params->playerName.")");

    //transactionRef is unique
    if (isset($params->gameRoundRef)) {
    	$Ref = $params->gameRoundRef;
    } else {
    	$Ref = $params->transactionRef;
    }
  	$SubRef = $params->transactionRef;

    $Refund = 0;
    $RefundRef = "";
    $RefundNotes = "";

    if (@$params->reason == "GAME_PLAY_FINAL") {
    	$RequestMethod = "TxEndGameRound";
    } else {
    	$RequestMethod = "Tx";
    }

    $row = DBxTxGame($RequestMethod, $PlayerID, $GameID=$params->gameId, $Wager=$params->amount, $Win=0, $Ref, $SubRef, $Refund, $RefundRef, $RefundNotes);

    if (isset($row->Result)) {//a successful return from the database call. If not, its a soap fault oblect
		  $Response = new SoapVar(array(
		    "balance" => "$row->Balance",
		    "transactionId" => "$row->agsRef"
		  ), SOAP_ENC_OBJECT);
    } else {
    	$Response = $row;
    }

    WriteLog($GameProviderCode, 0, "SEND: ".json_encode($Response), $PlayerID);

    return $Response;
  }

  //rollbackTransaction-----------------------------------------------------------------------------------------------------------
  function rollbackTransaction($params) {
  	global $GameProviderCode, $callerId, $callerPassword;
  	/*
		String callerId
		String callerPassword
		long transactionRef
		String playerName
		String gameId,
		String sessionId
  	*/

  	$PlayerID = substr(@$params->playerName,3);
    WriteLog($GameProviderCode, 0, "RECV: rollbackTransaction ".json_encode($params), $PlayerID);

  	if (@$params->callerId != $callerId || @$params->callerPassword != $callerPassword)
  	  return ErrorResponse($PlayerID, E_INVALID_AUTH, "Auth invalid");

  	if (!is_numeric($PlayerID))
  	  return ErrorResponse($PlayerID, E_INVALID_PARAMS, "NetEnt playerName invalid (playerName=".@$params->playerName.")");

    //transactionRef is unique for NetEnt. Whatever TxGame record holds this SubRef will be the amount that is voided
    $RequestMethod="Void";
    $Refund = 0;
    $RefundRef = $params->transactionRef;
   	$RefundNotes = "NetEnt rollbackTransaction Refund Wager";

    $gameId = (isset($params->gameId) ? $params->gameId : "");
    $row = DBxTxGame($RequestMethod, $PlayerID, $GameID=$gameId, $Wager=0, $Win=0, $Ref=0, $SubRef=0, $Refund, $RefundRef, $RefundNotes);

    if (isset($row->Result)) {//a successful return from the database call. If not, its a soap fault oblect
		  $Response = new SoapVar("rollbackTransactionResponse", SOAP_ENC_OBJECT);
    } else {
    	$Response = $row;
    }

    WriteLog($GameProviderCode, 0, "SEND: ".json_encode($Response), $PlayerID);

    return $Response;
  }

}

//ini_set("soap.wsdl_cache_enabled", "0");
if ($glo["Computer"] == "server10") {
  $uri = "skin1.aquagamesys.net";
} else {
  $uri = "dev.dagacube.net";
}

$server = new SoapServer("http://$uri/netent/WalletServer3_0.wsdl");
$server->setClass("WebService");
$server->handle();
?>