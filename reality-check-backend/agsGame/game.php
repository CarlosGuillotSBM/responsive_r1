<?php
/*
This file is called from anywhere any game is launched from.
It can be located inline or in a new window etc - the caller deals with that.
POST vars sent here are:
  PlayerID  int
  SessionID guid
  GameID (agsMain.Game.ID to launch)  int
  GameLaunchSourceID tinyint (see agsMain.GameLaunchSource - defaults to zero)
AF 22Oct112 - New code to retreive all relevant config details from database
AF 23Oct12 - removed $Environment as the DB deals with it
           - Added new GameStatus=5 - cannot play this game - wrong player class
AF 26Oct12 - write Launch log line before writing ant error
AF 14Dec12 - Added Bally Games
AF 14Jan13 - Added Eyecon
AF 18Jan13 - Force IGT skins from 2 and above to use D002 skin to avoid palava with IGT assigning new skin.
AF 22Feb13 - If FirstLaunch for Sheriff games show intro movie else dont
AF 14Mar13 - Changed GameProviderGameID to GameProviderGameLaunchID
             Removed references to ASH Gaming
             MGS Games from 6001-6499 = traditional web games
             MGS games from 6500-6999 = html5 games
HI 03apr13 - added gender and dob to IGT games launch
HI 08apr13 - hack for IGT mobile roulette denom
AF 14May13 - Eyecon games launch via a redirect with a query string to avoid security warnings as they dont use SSL
HI 15May13 - Eyecon testing IP restrictions
AF 23May13 - Remove Eyecon testing IP restrictions
HI 25jul13 - return width, height for games
AF 20Aug13 - Last 4 characters of IGT securetoken is Main.Game.ID and is used in xTxGame to lookup the correct game because their html5 and flash games use the same igt code
AF 29Aug13 - Added PlaynGo
AF 09Sep13 - LuckyPants skin 3 to use D003.
AF 13Sep13 - Modified PlaynGo
AF 15Sep13 - Modified PlaynGo - use ticket for mobile game launch
RS 18Sep13 - Added code at bottom of this file to avoid opening home page in new tab from ios devices in standalone mode
AF 06Oct13 - Modified PlaynGo - launch with PlayerID only
CM 18mar14 - Closing the window button when there was an error.
NO 18Jun14 - Added support for i18n
HI 17Jul14 - Don't send username to eyecon
AF 09Oct14 - Added NetEnt games
AF 10Oct14 - Proper checks for $ErrMsg
AF 24Oct14 - Fixed PlaynGo games as we now get their gameId we can associate it with the session so players can have more than 1 PnG game open at any one time
AF 18Dec14 - NetEnt with inclusion.js
NO 22Mar16 - Eyecon adding reality check
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions for agsGame

$PlayerID = ((isset($_REQUEST["PlayerID"]) ? $_REQUEST["PlayerID"] : 0));
if (!(is_numeric($PlayerID)))
  $PlayerID = 0;

$SessionID = ((isset($_REQUEST["SessionID"]) ? $_REQUEST["SessionID"] : ""));
if (strlen($SessionID) != 36)
  $SessionID = "";

$GameID = ((isset($_REQUEST["GameID"]) ? $_REQUEST["GameID"] : 0));
if (!(is_numeric($GameID)))
  $GameID = 0;

$GameLaunchSourceID = ((isset($_REQUEST["GameLaunchSourceID"]) ? $_REQUEST["GameLaunchSourceID"] : 0));
if (!(is_numeric($GameLaunchSourceID)) || $GameLaunchSourceID < 0 || $GameLaunchSourceID > 255)
  $GameLaunchSourceID = 0;

if ($PlayerID == 0 || $SessionID == "" || $GameID == 0) {
  WriteLog("Application", E_INVALID_GAME_LAUNCH_PARAMS, "Player IP=".$_SERVER["REMOTE_ADDR"]." SessionID=".$SessionID." game=".$GameID, $PlayerID);//310: Game - invalid post variables to launch game
  exit;
}

$GameProviderCode = "";

require_once("D:/WEB/inc/db.php");

$dbSproc = "WebGetGameProperties";

$dbParams = array(
    "PlayerID"           => array($PlayerID, "int", 0),
    "SessionID"          => array($SessionID, "str", 36),
    "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),//players IP
    "GameID"             => array($GameID, "int", 0),
    "GameLaunchSourceID" => array($GameLaunchSourceID, "int", 0),
	"DeviceID"			 => array(2, "int", 0)
);

$dbErr = "";
$db = "";

if(!dbInit("","agsMain",1)) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {

WriteLog("db", 'SQL', SdbParams($dbParams), $PlayerID);

  $rs = dbExecSproc($dbSproc,$dbParams,1);
  if ($rs === false) {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  } else {
    $row = sqlsrv_fetch_object($rs);
    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }
}

dbFree($rs);
dbClose($db);

if ($dbErr != "") {
  WriteLog("Application", $dbErr, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
  exit;
}




$GameDescription = $GameID;
$Width  = 800;
$Height = 650;

if($row->Result == 104) {
	$HomeURL = $row->HomeURL;
	$GameStatus = 6; // bad session
} else {
	//db access was successful, other error
	if ($row->Result != 0) {
	  WriteLog("Application", $row->Result, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
	  exit;
	}
	
	$GameStatus         = $row->GameStatus;//0=disabled (or not available for the skin), 1=enabled, 2=Cannot Play - Funded only, 3=cannot play - Self Excluded, 4=Cannot play - real funds only
	$GameProviderCode   = $row->GameProviderCode;
  $GameProviderGameID = $row->GameProviderGameID;
	$GameProviderGameLaunchID = $row->GameProviderGameLaunchID;
	$GameDescription    = $row->GameDescription;
	$SkinID             = $row->SkinID;
	$CurrencyCode       = $row->CurrencyCode;
	$LanguageCode       = $row->LanguageCode;
	$CountryCode        = $row->CountryCode;
	$CountryCode2       = $row->CountryCode2;
	$GameURL            = $row->GameURL;
	$SiteID             = $row->SiteID;
	$CoinSizeString     = $row->CoinSizeString;
	$MaxDoubleUp        = $row->MaxDoubleUp;
	$MaxWin             = $row->MaxWin;
	$Username           = $row->Username;
	$FirstLaunch        = $row->FirstLaunch;
	$PlayerDOB          = $row->PlayerDOB;
	$PlayerGender       = $row->PlayerGender;
	$HomeURL            = $row->HomeURL;
	$LobbyURL           = $row->LobbyURL;
	$CashierURL         = $row->CashierURL;
	$PromoURL           = $row->PromoURL;
	$BackgroundURL      = $row->BackgroundURL;
	$Width            	= $row->Width;
	$Height      		    = $row->Height;
  $GUID               = $row->GUID;
  $GameTypeID         = $row->GameTypeID;
  $SkinCode           = $row->SkinCode;
  $NetEnt_merchantId  = $row->NetEnt_merchantId;
  $NetEnt_merchantPassword = $row->NetEnt_merchantPassword;
  $ErrMsg             = "";
}

$NetEnt_merchantPassword = "testing7";

// Eyecon testing IP restrictions
/*
$ip = getPlayerIP();
if($GameID > 2000 && $GameID < 3000)
{
  $a = Array();
  $a = array_merge($a,getIPRange('180.214.67.98',27));
  $a = array_merge($a,getIPRange('180.214.71.96',27));
  $a = array_merge($a,getIPRange('203.33.60.192',28));
  $a[] = '62.232.79.254';
  $a[] = '196.192.15.123';
  $a[] = '58.172.242.216';
  $a[] = '120.151.3.227';
  $a[] = '203.45.176.36';

  if(!in_array($ip,$a)) $GameStatus = 0;

}
*/

// i18n
if (isset($SkinID) && $SkinID == 4) {
  require_once("translations/game-i18n-gr.php");
} else {
  require_once("translations/game-i18n-en.php");
}


WriteLog($GameProviderCode, -999, "Launch Game [$GameID] $GameDescription. GameLaunchSourceID=$GameLaunchSourceID", $PlayerID); //-999 is special code referring to game launch for logs



switch($GameStatus) {
  case 0:	
    WriteLog($GameProviderCode, E_GAME_DISABLED, "", $PlayerID);
    $ErrMsg=$i18n["gameUnavailable"];
    break;

  case 2://Funded Only
    WriteLog($GameProviderCode, E_PLAYER_NOT_FUNDED, "", $PlayerID);
    $ErrMsg=$i18n["onlyForFunded"];
    break;

  case 3://Self Excluded
    WriteLog($GameProviderCode, E_SELF_EXCLUSION, "", $PlayerID);
    $ErrMsg=$i18n["accountSelfExcluded"];
    break;

  case 4://Real only
    WriteLog($GameProviderCode, E_REQUIRE_REAL_FUNDS, "", $PlayerID);
    $ErrMsg=$i18n["onlyWithRealFunds"];
    break;

  case 5://Player class restriction
    WriteLog($GameProviderCode, E_PLAYER_CLASS_RESTRICTED, "", $PlayerID);
    $ErrMsg=$i18n["onlyVIPplayers"];
    break;

  case 6://Bad Session    
    $ErrMsg=$i18n["sessionExpired"];
    break;
}


if ($ErrMsg == "") {
	if ($GameProviderCode == "Eyecon") {
	  if(stripos($GameURL, '?') == FALSE)
		$gameloc = $GameURL."?";
	  else
	    $gameloc = $GameURL."&"; 
	
	  $gameloc = $gameloc.
	               "uid=".$PlayerID.
	               "&alias=".$PlayerID. //.$Username.
	               "&gameid=".$GameProviderGameLaunchID.
	               "&guid=".$SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).
	               "&brand=".$SiteID.
	               "&nid=".$SkinID.
	               "&lang=".$LanguageCode.
	               "&cur=".$CurrencyCode.
	               "&RealityCheckRemaining=30".
	               "&RealityCheckFrequency=30".
				   "&SessionDuration=0";
	}

	if ($GameProviderCode == "IGT") {
		if($SkinID == 5)
		{
			$skincode = "D001";
		}
		else
		{
			$skincode = "D".str_pad($SkinID, 3, '0', STR_PAD_LEFT);
		}
	}

	if ($GameProviderCode == "PlaynGo") {
		if ($GameID < 14500) {//Flash games
		  $gameloc = $GameURL.
		               "?div=PlaynGo".
		               "&gid=".$GameProviderGameLaunchID.
		               "&lang=".$LanguageCode."_".$CountryCode2.
		               "&pid=121".
		               "&username=".$PlayerID.
		               "&practice=0".
		               //"&password=".
		               //"&background=0xFFFFFF".
		               "&sound=1".
		               //"&ctx=".
		               "&width=".($Width*.96)."px".
		               "&height=".($Height*0.87)."px".
		               //"&demo=".
		               //"&callback="
		               "";
		} else {//mobile games
		  $gameloc = $GameURL.
		               "?pid=121".
		               "&GID=".$GameProviderGameLaunchID.
		               "&lang=".$LanguageCode."_".$CountryCode2.
		               //"&practice=0".
		               "&ctx=default".
		               //"&user=".
		               "&ticket=".$PlayerID.
		               "&lobby=".$LobbyURL.
		               "&iframeoverlay=http://skin1.aquagamesys.net/testskinex/reality-check-pngo/".
		               //"&scale=".
		               //"&urlmode=".
	                 "";
		}
	}

  /*
  NetEnt requires the player to login to the game server 1st to get a NetEnt sessionId
  */
  if ($GameProviderCode == "NetEnt") {

	$NetEnt_sessionId = "";
    try {
			$client = new SoapClient ("CasinoMC-10.0.wsdl");//change to correct http location for production!
			$realityURL = 'https://skin1.aquagamesys.net/netent/inclusion.html';
			
			
			$params = array();
			$params[] = new SoapVar($PlayerID, XSD_STRING, null, null, 'userName' );
			$params[] = new SoapVar('Channel', XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar( (($GameID >= 17500) ? 'mobg' : 'bbg'), XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar('Country', XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar($CountryCode2, XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar('Sex', XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar(strtoupper($PlayerGender), XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar('Birthdate', XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar(str_replace("-","",$PlayerDOB), XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar('DisplayName', XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar($Username, XSD_STRING, null, null, 'extra' );
			$params[] = new SoapVar($NetEnt_merchantId, XSD_STRING, null, null, 'merchantId' );
			$params[] = new SoapVar($NetEnt_merchantPassword, XSD_STRING, null, null, 'merchantPassword' );
			$params[] = new SoapVar($CurrencyCode, XSD_STRING, null, null, 'currencyISOCode' );
			//$params[] = new SoapVar($realityURL, XSD_STRING, null, null, 'pluginUrl' );
  
			try {
			
			
			
			//print_r($params);
			
			
  		  $response = $client->loginUserDetailed(new SoapVar($params, SOAP_ENC_OBJECT));
			} catch (SoapFault $fault) {
		    WriteLog($GameProviderCode, E_INVALID_PARAMS, "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", $PlayerID);
		    $ErrMsg=$i18n["gameUnavailable"];
			} 

      if ($ErrMsg == "") {
	      $NetEnt_sessionId = $response->loginUserDetailedReturn;
	      if ($GameID >= 17500) {//mobile
	      	$NetEnt_gameName = $GameProviderGameLaunchID.".mobile";
	      } else {
	      	$NetEnt_gameName = $GameProviderGameLaunchID.".desktop";
	      }
	    }
    } catch(SoapFault $fault) {
      WriteLog($GameProviderCode, E_INVALID_PARAMS, "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})" , $PlayerID);
      $ErrMsg = $i18n["gameUnavailable"];
    }

  }
}

/*
DaGaCube      1-1999 (GameID=1 and GameID=999 - simulator)
Eyecon     2001-2999
Sheriff    3001-3999
IGT        4001-4999
NextGen    5001-5999
MGS        6001-6999
Baddamedia 7001-7999
Bally      9001-9999
Daub-Naked Penguin Boy 10001-19999
*/

?>
<html>
<head>
<title><?php echo $GameDescription;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<?php if ($GameProviderCode == "NetEnt"):?>
<?php if ($GameID >= 17450 && $GameID <= 17499)://Refers to Live Casino games only?>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>

<?php else:?>

<script type="text/javascript" src="<?php echo $GameURL;?>gameinclusion/library/gameinclusion.js"></script>
<script type="text/javascript">
	var startGame = function () {

		var flashparams = {
			allowscriptaccess: "always",
			wmode: "window"
	  };
	
		var mobileparams = {
			lobbyUrl: "<?php echo $LobbyURL;?>",
	  };
	
	  <?php if ($GameID >= 17500):?>
		var config = {
			  lobbyURL: "<?php echo $LobbyURL;?>",
				gameId: "<?php echo $GameProviderGameID;?>",
				//gameName: "<?php echo $NetEnt_gameName;?>",
				staticServer: "<?php echo $GameURL;?>",
				gameServer: "https://daub-game-test.casinomodule.com/", //change for production!
				sessionId : "<?php echo $NetEnt_sessionId;?>",
				targetElement: "netEntGame",
				walletMode: "seamlesswallet",
				language: "en",
				customParameter: "<?php echo $SessionID;?>",
				mobileParams: mobileparams,
				pluginUrl: "<?php echo $realityURL;?>"
								
		};
	  <?php else:?>
		var config = {
			  gameId: "<?php echo $GameProviderGameID;?>",
				//gameName: "<?php echo $NetEnt_gameName;?>,
				staticServer: "<?php echo $GameURL;?>",
				gameServer: "https://daub-game-test.casinomodule.com/", //change for production!
				sessionId : "<?php echo $NetEnt_sessionId;?>",
				targetElement: "netEntGame",
				walletMode: "seamlesswallet",
				language: "en",
				customParameter: "<?php echo $SessionID;?>",
				flashParams: flashparams
		};
	  <?php endif;?>
	    
	
		var success = function (netEntExtend) {
			//Game launch successful
			netEntExtend.resize(<?php echo $Width*.96;?>, <?php echo $Height*.87;?>);
			netEntExtend.addEventListener("gameReady", function () {netEntExtend.get("volumeLevel", function (volumeLevel) {});
			netEntExtend.set("volumeLevel", 50);
			});
		};
		
		var error = function (e) {
			alert("error\n" + e.message);
			//Error handling here
		};
		netent.launch(config, success, error);
	};
	window.onload = startGame;
</script>
<?php endif;?>
<?php endif;?>

<?php if ($GameStatus == 1 && $ErrMsg == ""):?>
	<?php if ($GameProviderCode == "PlaynGo" && $GameID < 14500):?>
		<script>window.resizeTo(<?php echo $Width;?>,<?php echo $Height;?>);</script>
		<script src="<?php echo $gameloc;?>" type="text/javascript"></script>
		<script type="text/javascript">
			function ShowCashier() {
			  window.location = '';
			}
			function PlayForReal() {
			  window.location = '';
			}
			function Logout() {
			  window.close();
			}
		</script>
	<?php else:?>
    <?php if ($GameProviderCode != "NetEnt"):?>
			<script language="JavaScript">
			  function onloadH(e) {
					window.resizeTo(<?php echo $Width;?>,<?php echo $Height;?>);
			    <?php if ($GameProviderCode == "Eyecon" || $GameProviderCode == "PlaynGo"):?>
			      document.location.href = "<?php echo $gameloc;?>";
			    <?php else:?>
				
			      document.theForm.submit();
			    <?php endif;?>
			    return true;
			  }
			</script>
	  <?php endif;?>
	<?php endif;?>
<?php endif;?>
<style type="text/css">
body{
  font-family: Arial, Tahoma, Geneva, sans-serif;
  color: #0a375e;
  font-size:#fff;
  font-weight: bold;
  font-size: 20px;
}
#err{
  margin: 120px auto;
  text-align:center;
  background: url('images/error-modal.png') no-repeat top left;
  width:600px;
  height: 334px;
  padding-top: 50px;
}
#gamename{
  font-size: 30px;
  color: #fff;
  display: block;
}
#loading{
	text-align:center;
	width:100%;
}
.button {
	font-size: 12px;
	font-weight: bold;
	color: #ffffff;
	text-decoration:none;
	padding: 20px 20px;
	margin-top:10px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	border: 1px solid #1f5500;
	-moz-box-shadow:
		0px 1px 2px rgba(000,000,000,0.8),
		inset 0px 1px 1px rgba(255,255,255,0.7);
	-webkit-box-shadow:
		0px 1px 2px rgba(000,000,000,0.8),
		inset 0px 1px 1px rgba(255,255,255,0.7);
	-o-box-shadow:
		0px 1px 2px rgba(000,000,000,0.8),
		inset 0px 1px 1px rgba(255,255,255,0.7);
	box-shadow:
		0px 1px 2px rgba(000,000,000,0.8),
		inset 0px 1px 1px rgba(255,255,255,0.7);
	text-shadow:
		1px 1px 1px rgba(000,000,000,1),
		0px 1px 0px rgba(255,255,255,0.3);
		max-width: 200px;
}
.button-orange {
	font-size: 13px;
	margin-top:0px;
	background: #fc9700; /* Old browsers */
	background: -moz-linear-gradient(top,  #fc9700 0%, #eb2a0c 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fc9700), color-stop(100%,#eb2a0c)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  #fc9700 0%,#eb2a0c 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  #fc9700 0%,#eb2a0c 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  #fc9700 0%,#eb2a0c 100%); /* IE10+ */
	background: linear-gradient(to bottom,  #fc9700 0%,#eb2a0c 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fc9700', endColorstr='#eb2a0c',GradientType=0 ); /* IE6-9 */

	border: 1px solid #c10000;
	text-shadow:
	1px 2px 1px rgba(166,37,8,1),
	0px 1px 0px rgba(255,255,255,0.3);
}
<?php if($SkinID == 5) echo 'BODY{ background-color: #000000 }'; ?>

</style>
</head>
<?php if ($GameStatus == 1 && $ErrMsg == ""):?>
	<?php if ($GameProviderCode == "PlaynGo" && $GameID < 14500):?>
		<div id="PlaynGo">
			You either have JavaScript turned off or an old version of <a target="_blank" href="http://get.adobe.com/flashplayer/">Adobes Flash Player</a>
		</div>
  <?php elseif ($GameProviderCode == "NetEnt"):?>
	  <?php if($GameID >= 17450 && $GameID <= 17499):?>
			<body scrolling="no" style="margin: 0pt; padding: 0pt;overflow: hidden">
			<div style="position:absolute;z-pos:10;">
			</div>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
				<tr>
					<td width = "1024" align="center">
						<table id="gameTable" border="0" cellpadding="0" cellspacing="0" width="1024">
							<tr height="768">
								<td>
									<!-- game is loaded here -->
									<object id="flashcontent" type="application/x-shockwave-flash" width="100%" height="100%"></object>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
	
			<script>
			  function loadNetEntGame() {
	  		  var serviceHost = "https://daub-livegame-test.casinomodule.com"; //change for production!
		  	  var staticUrl = "<?php echo $GameURL;?>"; // Alternatively add altBrand at the end. Don't forget slash at the end!

					var flashvars = {
						accountType: "SEAMLESS_WALLET",
						brandingLocale: "<?php echo $LanguageCode;?>",
						casinoId: "daub",
						customLeaveTable: "false",
						gameHostURL: staticUrl,
						integrationTest: "true",
						lang: "<?php echo $LanguageCode;?>",
						lobbyURL: escape("<?php echo $LobbyURL;?>"),
						jsonRequestURL: serviceHost,
						sessionid: "<?php echo $NetEnt_sessionId;?>",
						showMiniLobby: "true",
						wmode: "direct"
				  };
				
					var flashparams = {
						allowFullScreen: "true",
						allowFullScreenInteractive: "true",
						allowscriptaccess: "always",
						base: "https://daub-livegame-test.casinomodule.com/", //change for production!,
						FlashVars: flashvars,
						loop: "true",
						quality: "high",
						scale: "exactfit",
						wmode: "direct"
				  };
					swfobject.embedSWF(staticUrl + "livecasinoloader/livecasinoloader-application.swf", "flashcontent", "100%", "100%", "10.0.42.34", "swfobject/expressInstall.swf", flashvars, flashparams);
				}
	
				function openLCHistory(url) {
				  var features = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no,width=440,height=420";
				  var remote = window.open(url, "history", features);
				}
	
				function rules(url) {
				  var features = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no,width=440,height=420";
				  var remote = window.open(url, "rules", features);
				}
	
				function netent_onLeaveTableClicked() {
					// Available in Live Casino 1.4 and up.
					// OPTIONAL: Replace this script with a custom script for leaving the game. This will only run if the flashvar 'customLeaveTable' has been set to true.
					alert("You clicked on leave table and this is your custom javascript running!");
				}
			</script>
	
		<?php else:?>

	    <body>
		  <div id="netEntGame">
	      <?php echo "Launching $GameDescription...";?>
	      <br><br>
		    <div id="loading">
		      <img src="images/skin<?php echo $SkinID;?>.png" />
		    </div>
		  </div>
    <?php endif;?>
  <?php else:?>
  	<body onLoad="onloadH();">
  	<?php echo "Launching $GameDescription...";?>
  	<br><br>
  	<div id="loading">
			<img src="images/skin<?php echo $SkinID;?>.png" />
  	</div>	
	<?php endif;?>
<?php else:?>
  <body>
  <div id=err>
    <?php
      echo "<span id=gamename>".$GameDescription."</span>";
      echo $ErrMsg;
    ?>
	<br/><br/><br/><br/>	
	<a id="erra" href="javascript:goHome('<?php echo $HomeURL;?>')" class="button button-orange "><?php echo $i18n["backToLobby"] ?></a>	    
  </div>
  <br>
<?php endif;?>

<?php if ($GameStatus == 1 && $ErrMsg == "" && $GameProviderCode != "Eyecon" && $GameProviderCode != "PlaynGo" && $GameProviderCode != "NetEnt"):?>
<form name="theForm" target="_self" method="<?php echo (($GameProviderCode == "Sheriff" ||
                                                         $GameProviderCode == "IGT" ||
                                                         $GameProviderCode == "MGS" ||
                                                         $GameProviderCode == "Bally" ||
														 $GameProviderCode == "Realistic" ||
														 $GameProviderCode == "Geco" ||
														$GameProviderCode == "SG") ? "GET" : "POST");?>" action="<?php echo $GameURL;?>">

  <?php if ($GameProviderCode == "DaGaCube" && ($GameID == 1 || $GameID == 999)):?>
    <input type="hidden" name="PlayerID"    value="<?php echo $PlayerID;?>">
    <input type="hidden" name="SessionID"   value="<?php echo $SessionID;?>">
    <input type="hidden" name="SkinID"      value="<?php echo $SkinID;?>">
    <input type="hidden" name="url"         value="<?php echo $GameURL;?>">
    <input type="hidden" name="GameID"      value="<?php echo $GameProviderGameLaunchID;?>">
  <?php endif;?>

  <?php if ($GameProviderCode == "DaGaCube" && $GameID != 1 && $GameID != 999):?>
    <input type="hidden" name="uid"       value="<?php echo $PlayerID;?>">
    <input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="guid"      value="<?php echo $SessionID;?>">
    <input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="url"       value="<?php echo $GameURL;?>">
    <input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="operator"  value="1">
    <input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
    <input type="hidden" name="HomeURL"    value="<?php echo $HomeURL;?>">
    <input type="hidden" name="LobbyURL"    value="<?php echo $LobbyURL;?>">
    <input type="hidden" name="CashierURL"    value="<?php echo $CashierURL;?>">
    <input type="hidden" name="PromoURL"    value="<?php echo $PromoURL;?>">
    <input type="hidden" name="BackgroundURL"    value="<?php echo $BackgroundURL;?>">
  <?php endif;?>

  <?php if ($GameProviderCode == "Sheriff"):?>
    <input type="hidden" name="site_id"          value="<?php echo $SiteID;?>">
    <input type="hidden" name="player_reference" value="<?php echo $PlayerID;?>">
    <input type="hidden" name="game_id"          value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="session_id"       value="<?php echo $SessionID;?>">
    <input type="hidden" name="mode"             value="real">
    <input type="hidden" name="locale"           value="en_US">
    <input type="hidden" name="currency"         value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="x_site_id"        value="<?php echo $SiteID;?>">
    <input type="hidden" name="x_GameLaunchSourceID" value="<?php echo $GameLaunchSourceID;?>">
    <input type="hidden" name="show_intro"       value="<?php echo $FirstLaunch;?>">
  <?php endif;?>

  <?php // Flash Games
  if ($GameProviderCode == "IGT" && $GameID < 4500):?>
    <input type="hidden" name="uniqueid"     value="<?php echo $PlayerID;?>">
    <input type="hidden" name="currencycode" value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="countrycode"  value="<?php echo $CountryCode2;?>">
    <input type="hidden" name="presenttype"  value="FLSH">
    <input type="hidden" name="softwareid"   value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="language"     value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="nscode"       value="DAUB">
    <input type="hidden" name="skincode"     value="<?php echo $skincode;?>">
    <input type="hidden" name="securetoken"  value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 4, '0', STR_PAD_LEFT);?>">
    <input type="hidden" name="dateofbirth"  value="<?php echo $PlayerDOB;?>">
    <input type="hidden" name="gender"  value="<?php echo $PlayerGender;?>">
  <?php endif;?>

  <?php // HTML 5 games
  if ($GameProviderCode == "IGT" && $GameID >= 4500):?>
    <input type="hidden" name="uniqueid"     value="<?php echo $PlayerID;?>">
    <input type="hidden" name="currencycode" value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="countrycode"  value="<?php echo $CountryCode2;?>">
    <input type="hidden" name="presenttype"  value="HTML">
    <input type="hidden" name="softwareid"   value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="language"     value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="nscode"       value="DAUB">
    <input type="hidden" name="skincode"     value="<?php echo $skincode;?>">
    <input type="hidden" name="securetoken"  value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 4, '0', STR_PAD_LEFT);?>">
    <input type="hidden" name="minbet"       value="0.01">
    <input type="hidden" name="denomamount"  value="0.01">
    <input type="hidden" name="dateofbirth"  value="<?php echo $PlayerDOB;?>">
    <input type="hidden" name="gender"  value="<?php echo $PlayerGender;?>">
		<?php if ($skincode == "D999"):?>
			<input type="hidden" name="<?php echo $skincode."_homeURL";?>"  value="<?php echo $HomeURL;?>">
			<input type="hidden" name="<?php echo $skincode."_lobbyURL";?>"  value="<?php echo $LobbyURL;?>">
			<input type="hidden" name="<?php echo $skincode."_cashierURL";?>"  value="<?php echo $CashierURL;?>">
		<?php endif;?>
  <?php endif;?>


  
   <?php if ($GameProviderCode == "MGS" && $GameID <= 6499):?>
    <input type="hidden" name="authToken" value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 4, '0', STR_PAD_LEFT).str_pad($PlayerID, 7, '0', STR_PAD_LEFT);?>">
	<input type="hidden" name="applicationid" value="1023">
	<!--input type="hidden" name="serverid" value="<?php echo $SiteID;?>"-->	
	<input type="hidden" name="theme" value="quickfiressl">
	<input type="hidden" name="variant" value="Alderney">
	<input type="hidden" name="usertype" value="0">	
    <input type="hidden" name="sext1"     value="genauth">
    <input type="hidden" name="sext2"     value="genauth">
    <input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="csid"      value="<?php echo $SiteID;?>">
    <!--input type="hidden" name="ul"        value="<?php echo 'config-quickfiressl--'.$LanguageCode.'--MIT';?>"-->
	<input type="hidden" name="ul"        value="<?php echo $LanguageCode;?>">
  <?php endif;?>

  
  <?php if ($GameProviderCode == "MGS" && $GameID >= 6500):?>
    <input type="hidden" name="casinoID"   value="<?php echo $SiteID;?>">
    <input type="hidden" name="lobbyURL"   value="<?php echo $LobbyURL;?>">
    <input type="hidden" name="bankingURL" value="<?php echo $CashierURL;?>">
    <input type="hidden" name="loginType"  value="VanguardSessionToken">
    <input type="hidden" name="authToken"  value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 4, '0', STR_PAD_LEFT).str_pad($PlayerID, 7, '0', STR_PAD_LEFT);?>">
    <input type="hidden" name="isRGI"      value="true">
  <?php endif;?>

  <?php if ($GameProviderCode == "Baddamedia"):?>
    <input type="hidden" name="PlayerID"           value="<?php echo $PlayerID;?>">
    <input type="hidden" name="SessionID"          value="<?php echo $SessionID;?>">
    <input type="hidden" name="CurrencyCode"       value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="CountryCode"        value="<?php echo $CountryCode;?>">
    <input type="hidden" name="LanguageCode"       value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="GameLaunchSourceID" value="<?php echo $GameLaunchSourceID;?>">
    <input type="hidden" name="Game"               value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="Launcher"           value="DGC">
  <?php endif;?>

  <?php if ($GameProviderCode == "Bally"):?>
    <input type="hidden" name="playerid"           value="<?php echo $PlayerID;?>">
    <input type="hidden" name="currencycode"       value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="countrycode"        value="<?php echo $CountryCode2;?>">
    <input type="hidden" name="softwareId"         value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="language"           value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="operatorid"         value="DAUB">
    <input type="hidden" name="skinid"             value="<?php echo $SiteID;?>">
    <input type="hidden" name="affiliateid"        value="1">
    <input type="hidden" name="authtype"           value="Token">
    <input type="hidden" name="authtoken"          value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).$PlayerID;?>">
    <input type="hidden" name="DemoMode"           value="False">
    <input type="hidden" name="RealPlay"           value="1">
		<input type="hidden" name="homeURL"            value="<?php echo $LobbyURL;?>">		  
  <?php endif;?>

  <?php if ($GameProviderCode == "NPB"):?>
    <input type="hidden" name="uid"       value="<?php echo $PlayerID;?>">
    <input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="guid"      value="<?php echo $SessionID;?>">
    <input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="url"       value="<?php echo $GameURL;?>">
    <input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="operator"  value="1">
    <input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
  <?php endif;?>

  <?php if ($GameProviderCode == "BetFoundry"):?>
    <input type="hidden" name="uid"       value="<?php echo $PlayerID;?>">
    <input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="guid"      value="<?php echo $SessionID;?>">
    <input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="url"       value="<?php echo $GameURL;?>">
    <input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="operator"  value="1">
    <input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
    <input type="hidden" name="HomeURL"    value="<?php echo $HomeURL;?>">
    <input type="hidden" name="LobbyURL"    value="<?php echo $LobbyURL;?>">
    <input type="hidden" name="CashierURL"    value="<?php echo $CashierURL;?>">
    <input type="hidden" name="PromoURL"    value="<?php echo $PromoURL;?>">
    <input type="hidden" name="BackgroundURL"    value="<?php echo $BackgroundURL;?>">
  <?php endif;?>

  <?php 
  
  if ($GameProviderCode == "Realistic"):?>
    <input type="hidden" name="freePlay"         value="false">
    <input type="hidden" name="externalGameId"  value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="languageCode"     value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="sessionToken"     value="<?php echo $SessionID;?>"> <!-- If freePlay true, sessionID should be false -->
    <input type="hidden" name="currencyCode"     value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="userId"          value="<?php echo $PlayerID;?>">
	<input type="hidden" name="operatorId" 			value="<?php echo $SkinID;?>">
		
  <?php endif;?>
  
  <?php   
  
  //nuno if ($GameProviderCode == "Geco" && $GameID <= 18499)    	lightracers mobile is 18502
  if ($GameProviderCode == "Geco" && $GameID >= 18499):?>    	
	<input type="hidden" name="launcher" 			value="spacebar"> <!--in coreix I reversed the order-->
    <input type="hidden" name="GameID"  			value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="LanguageCode"    value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="SessionID"    	value="<?php echo $SessionID;?>"> <!-- If freePlay true, sessionID should be false -->
    <input type="hidden" name="CurrencyCode"	value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="PlayerID"         value="<?php echo $PlayerID;?>">	
	<input type="hidden" name="CountryCode"		value="<?php echo $CountryCode;?>">	
	<input type="hidden" name="PlayMode"      	value="real"> <!-- demo or real as values -->
	<input type="hidden" name="SkinID"    		value="<?php echo $SkinID;?>">
  <?php endif;?>
  
  <?php   
  // nuno if ($GameProviderCode == "Geco" && $GameID >= 18500):    	
  if ($GameProviderCode == "Geco" && $GameID <= 18500):?>    	
	<input type="hidden" name="launcher" 			value="spacebar-flash"><!--in coreix I reversed the order-->
    <input type="hidden" name="GameID"  			value="<?php echo $GameProviderGameLaunchID;?>">
    <input type="hidden" name="LanguageCode"    value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="SessionID"    	value="<?php echo $SessionID;?>"> <!-- If freePlay true, sessionID should be false -->
    <input type="hidden" name="CurrencyCode"	value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="PlayerID"         value="<?php echo $PlayerID;?>">	
	<input type="hidden" name="CountryCode"		value="<?php echo $CountryCode;?>">	
	<input type="hidden" name="PlayMode"      	value="real"> <!-- demo or real as values -->
	<input type="hidden" name="SkinID"    		value="<?php echo $SkinID;?>">
  <?php endif;?>

  
  <?php
  if ($GameProviderCode == "SG" && $GameID <= 19500):?>    	
	<input type="hidden" name="context" 			value="default">
	<input type="hidden" name="realMoney"      	value="true"> <!-- demo or real as values -->
	<input type="hidden" name="gameCode"  			value="<?php echo $GameProviderGameLaunchID;?>">    
	<input type="hidden" name="ticket"    	value="<?php echo $SessionID;?>"> <!-- If freePlay true, sessionID should be false -->
	<input type="hidden" name="accountId"         value="<?php echo $PlayerID;?>">	
	<input type="hidden" name="partnerCode"         value="daub_bingo">		
	<input type="hidden" name="currency"	value="<?php echo $CurrencyCode;?>">
	<input type="hidden" name="languageCode"    value="<?php echo $LanguageCode;?>">
	<input type="hidden" name="realityCheck" value="0">
	<input type="hidden" name="realityWaitForGameEnd" value="true">
	<input type="hidden" name="realityLink" value="https://www.luckypantsbingo.com">
	<input type="hidden" name="realityButtons" value="QUITCONT">
	<!--input type="hidden" name="realitySessionTime" value="1"-->

  <?php endif;?>  

  <?php   
  if ($GameProviderCode == "SG" && $GameID >= 19500):?>    	
    <input type="hidden" name="game"  				value="<?php echo $GameProviderGameLaunchID;?>">    
    <input type="hidden" name="partnercode"          value="daub_bingo">
	<input type="hidden" name="lobbyurl"    			value="<?php echo $LobbyURL;?>">
	<input type="hidden" name="partnerticket"    	value="<?php echo $SessionID;?>">
	<input type="hidden" name="partneraccountid"    value="<?php echo $PlayerID;?>">	
	<input type="hidden" name="realmoney" 			value="true">
	<input type="hidden" name="realityCheck" value="0">
	<input type="hidden" name="realityWaitForGameEnd" value="true">
	<input type="hidden" name="realityLink" value="https://www.luckypantsbingo.com">
	<input type="hidden" name="realityButtons" value="QUITCONT">
	<!--input type="hidden" name="realitySessionTime" value="1"-->

  <?php endif;?>    
  
  
</form>
<?php endif;?>

<!--RS 18-09-2013. Added to avoid opening home page in new tab from ios devices in standalone mode-->
<script type="text/javascript">
	function fixLinksAfterAddToHome() {
		var a = document.getElementById("erra");
		a.onclick=function()
		{
			window.location=this.getAttribute("href");
			return false;
		}		
	}
	var isIOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/gi);
	if(isIOS){
      if (window.navigator.standalone == true){        
        // solve links opening in new tab issue for web app on iOS after add to home
        fixLinksAfterAddToHome();
      }      
    }
	
function goHome(url)
{
	parent.postMessage('0E33EB2B-E0D1-4495-8E83-15E2D9BBD4B7','https://html5.dagacubedev.net');
	parent.postMessage('0E33EB2B-E0D1-4495-8E83-15E2D9BBD4B7','https://bingoclient.dagacube.net');	
	
  <?php if($GameLaunchSourceID == 200):?>
		window.location.href = url;
	<?php else:?>	
		window.close();
	<?php endif;?>	
}

<?php if ($GameProviderCode == "NetEnt" && $GameID >= 17450 && $GameID <= 17499)://Refers to Live Casino games only?>
loadNetEntGame();
<?php endif;?>	

</script>
</body>
</html>