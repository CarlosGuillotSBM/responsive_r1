<?php
/*
This file is called from MGS Vanguard Server after a player has launched a game in a new browser window.
It can also be called by MGS Vanguard with a [Ping] request to determine if the system is active

AF 04Mar12 - Added PlayerID to WriteLog params for game logs
AF 06Mar12 - Added GameLaunchSourceID
AF 19Mar12 - RefundBet will return a success if the transaction was found or not
AF 02Nov12 - Removed conversion of XML to Array - using only xml structure due to problems with php 5.4.8
AF 25Oct12 - Fixed $Token to use a fixed length string
AF 05Jul15 - db56.php for php5.6.10
RS 02Feb16 - Changed gameproviderid for mgs-ainsworth games.
*/

require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "MGS";
$GameProviderID   = 6;//MGS=6  RS. This might be overwritten for subproviders of mgs, like ainsworth.

function ErrorResponse ($errCode, $data, $Response) {
  global $glo, $GameProviderCode, $PlayerID;
  if ($Response != "") {
    $Response = str_replace("#Success#","0",$Response);
    $Response.="<Error Type=\"string\" Value=\"".@$glo["LogCodes"][$errCode]."\" />";
    $Response.="<ErrorCode Type=\"int\" Value=\"".$errCode."\" />";
    $Response.="</Returnset></Result></PKT>";
  }

  if ($Response == "") $Response="err";
  $data = (($Response == "") ? "" : "SEND: ".$Response."   ").$data;
  if ($PlayerID != 0)
    WriteLog($GameProviderCode, $errCode, $data, $PlayerID);

  echo $Response;
  exit;
}

$PlayerID = 0;
$RequestMethod = "";
$Response = "";

$rawRequest = file_get_contents("php://input");
$GameRequest = simplexml_load_string($rawRequest);

if ($GameRequest === false) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $rawRequest [MGS Failed to load xml]", $Response);
}

//Ping call
if (isset($GameRequest->Method["Name"]) && @$GameRequest->Method["Name"] == "Ping") {
  $RequestMethod = "Ping";
  //WriteLog($GameProviderCode,0,"RECV: ".$rawRequest, $PlayerID);//remove after testing

  $Response = "<PKT><Result Name=\"Ping\" Success=\"1\"><Returnset><ClientIp Type=\"string\" Value=\"".$_SERVER["REMOTE_ADDR"]."\" /></Returnset></Result></PKT>";
  echo $Response;
  exit;
}

if (isset($GameRequest->Method["Name"])) {
  $RequestMethod = $GameRequest->Method["Name"];

  if (!( $RequestMethod == "GetAccountDetails" ||
         $RequestMethod == "RefreshToken" ||
         $RequestMethod == "GetBalance" ||
         $RequestMethod == "PlaceBet" ||
         $RequestMethod == "AwardWinnings" ||
         $RequestMethod == "RefundBet" ||
         $RequestMethod == "EndGame")) {
    $RequestMethod = "";
  }
}

if ($RequestMethod == "") {

  ErrorResponse(E_INVALID_PARAMS, "RECV: $rawRequest [No RequestMethod]", $Response);
}

/*
Token is $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 5, '0', STR_PAD_LEFT).str_pad($PlayerID, 7, '0', STR_PAD_LEFT)
eg 2FF221F6-E9AD-420B-A298-2495C4E218A5LLLGGGGPPPPPPP
where
  1st 36 chars are sessionid (0,36)
  LLL=GameLaunchSourceID     (36,3)
  GGGGG=Game.ID              (39,5)
  PPPPPPP=Player.ID          (44,7)
*/
$Token = $GameRequest->Method->Params->Token["Value"];
if (strlen($Token) != 51) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $rawRequest [Invalid token length]", $Response);
}

$GameLaunchSourceID = intval (substr($Token,36,3));
$PlayerID           = intval(substr($Token,44));
if (empty($PlayerID)) {
	$PlayerID = 0;
  ErrorResponse(E_PLAYER_NOT_FOUND, "PlayerID is empty", $Response);
}
WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, $PlayerID);

//Generate common response header
//$Response = "<?xml version=\"1.0\" encoding=\"UTF-8\"\?\>";
$Response = "<PKT><Result Name=\"".$RequestMethod."\" Success=\"#Success#\"><Returnset>";

//Check authentication (exits from funtion WriteLog if auth fails)
if (!(substr($GameRequest->Method->Auth["Login"],0,3) == "mgs" && $GameRequest->Method->Auth["Password"] == "wF63yhh9Jm")) {
  ErrorResponse(E_INVALID_AUTH, "Login:".$GameRequest->Method->Auth["Login"]." Password:".$GameRequest->Method->Auth["Password"], $Response);
}

$wager=0;
$win=0;
$ref=0;
$subref = -1;
$refund = 0;
$refundref="";
$refundnotes="";

/*
MGS only sends a single [Token] to identify the player.
We send this [Token] when launching the game.
The [Token] we send is a single string being the <SessionID><GameLaunchSourceID><GameID><PlayerID>
When we receive the Token from a Vanguard call, need to split it (see comments above).
*/
$SessionID = substr($Token,0,36);

/*
This code refers to MGS preferred 'dynamic' token system which we will use if we get problems with the static token
$SessionID = $GameRequest["Params"]["Token"]["@attributes"]["Value"];
$PlayerID  = 0;//We dont know the player id - the ags db will look it up from MGSSession table
*/
$GameProviderGameID = substr($Token,39,5);//this is actually our internal Game.ID - xTxGame will use this for MGS games for a GetBalance call

//Modify provider code and if for ainsworth games
if($GameProviderGameID >= 21000 && $GameProviderGameID< 22000){
	$GameProviderID   = 21;
}

$AlreadyProcessed   = "false";

$Action = "";
switch ($RequestMethod) {
  case "GetAccountDetails":
    $Action = "CheckSession";
    break;
  case "RefreshToken":
    $Action = "RefreshSession";
    break;
  case "GetBalance":
    $Action = "GetBalance";
    break;
  case "PlaceBet":
    $Action = "Tx";
    $wager  = number_format(($GameRequest->Method->Params->BetAmount["Value"]/100),2,".","");
    $ref    = $GameRequest->Method->Params->TransactionID["Value"];
    $subref = $GameRequest->Method->Params->BetReferenceNum["Value"];
    $GameProviderGameID = $GameRequest->Method->Params->GameReference["Value"];
    break;
  case "AwardWinnings":
    $Action = "Tx";
    $win  = number_format(($GameRequest->Method->Params->WinAmount["Value"]/100),2,".","");
    $ref    = $GameRequest->Method->Params->TransactionID["Value"];
    $subref = $GameRequest->Method->Params->WinReferenceNum["Value"];
    $GameProviderGameID = $GameRequest->Method->Params->GameReference["Value"];
    break;
  case "RefundBet":
    $Action = "Void";
    $ref    = $GameRequest->Method->Params->TransactionID["Value"];
    $subref = $GameRequest->Method->Params->BetReferenceNum["Value"];
    $win  = number_format(($GameRequest->Method->Params->RefundAmount["Value"]/100),2,".","");
    $GameProviderGameID = $GameRequest->Method->Params->GameReference["Value"];
    $refundref=$ref;
    $refundnotes="MGS RefundBet Ref:$ref SubRef:$subref";
    break;
  case "EndGame":
    $Action = "EndGameRound";
    $ref = $GameRequest->Method->Params->TransactionID["Value"];
    $GameProviderGameID = $GameRequest->Method->Params->GameReference["Value"];
    break;
  default:
    break;
}

/*
*** DO NOT RUN THIS CHECK - IT ALWAYS FAILS ***
if (is_numeric($ref) === false || is_numeric($subref) === false) {
  ErrorResponse(E_INVALID_TXID, "ref and or subref not numeric. ref=$ref subref=$subref", $Response);
}
*/

$dbSproc = "xTxGame";

$dbParams = array(
  "PlayerID"           => array(intval($PlayerID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "SessionID"          => array($SessionID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(36)),
  "IP"                 => array($_SERVER["REMOTE_ADDR"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
  "GameProviderID"     => array(intval($GameProviderID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "GameProviderGameID" => array(strval($GameProviderGameID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(100)),
  "GameLaunchSourceID" => array($GameLaunchSourceID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "Action"             => array($Action, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
  "Wager"              => array(floatval($wager), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
  "Win"                => array(floatval($win), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
	"Ref"                => array($ref, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_BIGINT),
	"SubRef"             => array($subref, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_BIGINT),
  "Refund"             => array(floatval($refund), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_FLOAT),
  "RefundRef"          => array(strval($refundref), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(50)),
  "RefundNotes"        => array($refundnotes, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(4000))
);

/*
db calls xTxGame
Returns:
  Result = 0 : Success
        <> 0 : Various fail codes
*/
require_once("D:/WEB/inc/db56.php");

$dbErr = $SdbParams = "";
$db = "";

if(dbInit("", "Main", __FILE__."[".__LINE__."]", 1) == false) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams);
  if ($rs === false) {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  } else {
    $row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }
}

if ($dbErr != "") {
  ErrorResponse($errCode, $dbErr, $Response);
}

dbFree($rs);
dbClose($db);

//db access was successful, other error (or already processed)
if ($row->Result != 0) {
  if ($row->Result == 104 || $row->Result == 105)
    $row->Result = 104;

  if ($row->Result == 307) {
    if ($RequestMethod == "PlaceBet" || $RequestMethod == "AwardWinnings" || $RequestMethod == "RefundBet") {
      $AlreadyProcessed = "true";
    } else {
      ErrorResponse(E_DUPLICATE_REF, "", $Response);
    }
  } else {
    if ($RequestMethod != "RefundBet") {
      ErrorResponse($row->Result, "", $Response);
    }
  }
}

/*
Success - make response
*/
//send token back
$Response = str_replace("#Success#","1",$Response);
$Response.= "<Token Type=\"string\" Value=\"".$Token."\" />";

if ($RequestMethod == "GetAccountDetails") {
  //mgs uses GB for UK
  if ($row->CountryCode == "UK")
    $row->CountryCode = "GB";
  $Response.="<LoginName Type=\"string\" Value=\"".$PlayerID."\" /><Currency Type=\"string\" Value=\"".$row->CurrencyCode."\" /><Country Type=\"string\" Value=\"".$row->CountryCode."\" /><City Type=\"string\" Value=\"".$row->City."\" />";
}

if ($RequestMethod == "GetBalance" || $RequestMethod == "EndGame") {
  $Response.="<Balance Type=\"string\" Value=\"".number_format(($row->Balance*100),0,"","")."\" />";
}

if ($RequestMethod == "PlaceBet" || $RequestMethod == "AwardWinnings" || $RequestMethod == "RefundBet") {
  $Response.="<Balance Type=\"string\" Value=\"".number_format(($row->Balance*100),0,"","")."\" />";
  if ($RequestMethod == "RefundBet" && $row->Result != 0) {
    $Response.="<ExtTransactionID Type=\"string\" Value=\"".$ref."\" />";
  } else {
    $Response.="<ExtTransactionID Type=\"string\" Value=\"".$row->agsRef."\" />";
  }
  $Response.="<AlreadyProcessed Type=\"bool\" Value=\"".$AlreadyProcessed."\" />";
}

$Response.="</Returnset></Result></PKT>";

WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);

echo $Response;
exit;
?>