<?php
/*
This file is called from affiliate pages anywhere any game is launched from.
It can be located inline or in a new window etc - the caller deals with that.
POST vars sent here are:
  PlayerID  int
  GameID (agsMain.Game.ID to launch)  int
  SkinID (agsMain.Skin.ID to use for the game) int
  SessionType (Slot.Spin.SessionType) tinyint
  CompID (agsMain.Competition.ID the game will participate in) int
  GameLaunchSourceID int
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions for agsGame

$AID = ((isset($_REQUEST["a"]) ? $_REQUEST["a"] : 0));
if (!(is_numeric($AID)))
  $AID = 0;

$PlayerID = ((isset($_REQUEST["PlayerID"]) ? $_REQUEST["PlayerID"] : 0));
if (!(is_numeric($PlayerID)))
  $PlayerID = 0;

$SkinID = ((isset($_REQUEST["SkinID"]) ? $_REQUEST["SkinID"] : 0));
if (!(is_numeric($SkinID)))
  $SkinID = 0;

$SessionType = ((isset($_REQUEST["SessionType"]) ? $_REQUEST["SessionType"] : 0));
if (!(is_numeric($SessionType)))
  $SessionType = 0;

$GameID = ((isset($_REQUEST["GameID"]) ? $_REQUEST["GameID"] : 0));
if (!(is_numeric($GameID)))
  $GameID = 0;

$CompID = ((isset($_REQUEST["CompID"]) ? $_REQUEST["CompID"] : 0));
if (!(is_numeric($CompID)))
  $CompID = 0;

$GameLaunchSourceID = ((isset($_POST["GameLaunchSourceID"]) ? $_POST["GameLaunchSourceID"] : 0));
if (!(is_numeric($GameLaunchSourceID)) || $GameLaunchSourceID < 0 || $GameLaunchSourceID > 255)
  $GameLaunchSourceID = 0;

if ($SessionType == 0 || $GameID == 0 || $AID == 0) {
  WriteLog("Application", E_INVALID_GAME_LAUNCH_PARAMS, "AID=".$AID.", Player IP=".$_SERVER["REMOTE_ADDR"].", GameID=".$GameID.", SessionType=".$SessionType.", CompID=".$CompID, $PlayerID);//310: Game - invalid post variables to launch game
  exit;
}

$GameProviderCode = "DaGaCube";

require_once("D:/WEB/inc/db56.php");

$dbSproc = "spaceBetGetPreRegistrationPlayerData";

$dbParams = array(
  "GameID"      => array(intval($GameID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "SkinID"      => array(intval($SkinID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "PlayerID"    => array(intval($PlayerID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "CompID"      => array(intval($CompID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "SessionType" => array(intval($SessionType), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT)
);

$dbErr = $SdbParams = "";
$db = "";

if(dbInit("", "Slot", __FILE__."[".__LINE__."]", 1) == false) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams);
  if ($rs === false) {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  } else {
    $row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }
}

dbFree($rs);
dbClose($db);

if ($dbErr != "") {
  echo ":".$dbErr;
  WriteLog("Application", $dbErr, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
  exit;
}

//db access was successful, other error
if ($row->Result != 0) {
  WriteLog("Application", $row->Result, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
  exit;
}

$DemoPlayerID     = $row->DemoPlayerID;
$DemoSessionID    = $row->DemoSessionID;
$CurrencyCode       = $row->CurrencyCode;
$LanguageCode       = $row->LanguageCode;
$GameURL            = $row->GameURL;
$HomeURL            = $row->HomeURL;
$BackgroundURL      = $row->BackgroundURL;
$GameDescription  = $row->GameDescription;
$GameStatus     = $row->GameStatus;
$RegisterURL    = $HomeURL.'/register/';

$ErrMsg = "";

WriteLog($GameProviderCode, -999, "Launch Demo Game [$GameID] $GameDescription. GameLaunchSourceID=$GameLaunchSourceID", $PlayerID); //-999 is special code referring to game launch for logs

?>
<html>
<head>
<title><?php echo $GameDescription;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include_once("D:/WEB/inc/jsDisableMouseDown.js");?>

<script language="JavaScript">

  function onloadH(e) {
      document.theForm.submit();
      return true;
  }
</script>
<style type="text/css">
body{
  font-family: Arial, Tahoma, Geneva, sans-serif;
  color: #0a375e;
  font-size:#fff;
  font-weight: bold;
  font-size: 20px;
  background: url('images/error-bkg.jpg');
}
#err{
  margin: 120px auto;
  text-align:center;
  background: url('images/error-modal.png') no-repeat top left;
  width:600px;
  height: 334px;
  padding-top: 50px;
}
#gamename{
  font-size: 30px;
  color: #fff;
  display: block;
}
</style>
</head>
<?php if ($GameStatus == 1):?>
  <body onLoad="onloadH();">
  <?php echo "Launching $GameDescription...";?>
<?php else:?>
  <body>
  <div id=err>
    <?php
      echo "<span id=gamename>".$row->GameDescription."</span>";
      echo "<br><br>";
      echo $ErrMsg;
    ?><br/><br/>
    <!--<a href="#"><img src="images/error-cashier-button.png"></a>-->
  </div>
  <br>
<?php endif;?>

<?php if ($GameStatus == 1):?>
<form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>">


  <?php if ($GameProviderCode == "DaGaCube" && $GameID != 1 && $GameID != 999):?>
    <input type="hidden" name="uid"       value="<?php echo $DemoPlayerID;?>">
  <input type="hidden" name="sessionType"value="<?php if($SessionType == 1) echo "FUN"; else if ($SessionType == 3) echo "AFF"; else echo "REAL";?>">
    <input type="hidden" name="gameid"    value="<?php echo $GameID;?>">
    <input type="hidden" name="guid"      value="<?php echo $DemoSessionID;?>">
    <input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">
    <input type="hidden" name="url"       value="<?php echo $GameURL;?>">
    <input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
    <input type="hidden" name="operator"  value="1">
    <input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
    <input type="hidden" name="HomeURL"    value="<?php echo $HomeURL;?>">
    <input type="hidden" name="RegisterURL"    value="<?php echo $RegisterURL;?>">
    <input type="hidden" name="BackgroundURL"    value="<?php echo $BackgroundURL;?>">
  <input type="hidden" name="a"        value="<?php echo $AID;?>">
  <?php endif;?>
</form>
<?php endif;?>
</body>
</html>