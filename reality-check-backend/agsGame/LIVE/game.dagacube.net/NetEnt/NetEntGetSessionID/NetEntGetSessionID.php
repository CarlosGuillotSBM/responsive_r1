﻿<?php
/*
This file is called from game.php via curl to get the NetEnt SessionID through a soap call.
It is isolated from game.php so it can be run in an earlier version of php as we experienced some issues where the 
  $client->loginUserDetailed(new SoapVar($params, SOAP_ENC_OBJECT));
would time out then the system got busy.

Returns: string NetEnt_sessionId
         string Error
*/

$Return = array("NetEnt_sessionId" => "", "Error" => "");

try {
  $client = new SoapClient ("CasinoMC-10.0.wsdl");

  $params = array();
  $params[] = new SoapVar($_POST["PlayerID"], XSD_STRING, null, null, 'userName' );
  $params[] = new SoapVar('Channel', XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar( (($_POST["GameID"] >= 17500) ? 'mobg' : 'bbg'), XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar('Country', XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar($_POST["CountryCode2"], XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar('Sex', XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar($_POST["PlayerGender"], XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar('Birthdate', XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar($_POST["PlayerDOB"], XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar('DisplayName', XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar($_POST["Username"], XSD_STRING, null, null, 'extra' );
  $params[] = new SoapVar($_POST["NetEnt_merchantId"], XSD_STRING, null, null, 'merchantId' );
  $params[] = new SoapVar($_POST["NetEnt_merchantPassword"], XSD_STRING, null, null, 'merchantPassword' );
  $params[] = new SoapVar($_POST["CurrencyCode"], XSD_STRING, null, null, 'currencyISOCode' );

  try {
    $response = $client->loginUserDetailed(new SoapVar($params, SOAP_ENC_OBJECT));
  } catch (SoapFault $fault) {
    $Return["NetEnt_sessionId"] = "";
    $Return["Error"] = "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})";
  }

  if ($Return["Error"] == "") {
    $Return["NetEnt_sessionId"] = $response->loginUserDetailedReturn;
  }
} catch(SoapFault $fault) {
  $Return["NetEnt_sessionId"] = "";
  $Return["Error"] = "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})";
}
$Return = base64_encode(serialize($Return));
echo $Return;
exit;
?>