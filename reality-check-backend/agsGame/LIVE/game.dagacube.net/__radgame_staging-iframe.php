<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style type="text/css">

          /* http://meyerweb.com/eric/tools/css/reset/ 
             v2.0 | 20110126
             License: none (public domain)
          */

          html, body, div, span, applet, object, iframe,
          h1, h2, h3, h4, h5, h6, p, blockquote, pre,
          a, abbr, acronym, address, big, cite, code,
          del, dfn, em, img, ins, kbd, q, s, samp,
          small, strike, strong, sub, sup, tt, var,
          b, u, i, center,
          dl, dt, dd, ol, ul, li,
          fieldset, form, label, legend,
          table, caption, tbody, tfoot, thead, tr, th, td,
          article, aside, canvas, details, embed, 
          figure, figcaption, footer, header, hgroup, 
          menu, nav, output, ruby, section, summary,
          time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
          }
          /* HTML5 display-role reset for older browsers */
          article, aside, details, figcaption, figure, 
          footer, header, hgroup, menu, nav, section {
            display: block;
          }
          body {
            line-height: 1;
			background-color: #000000;
          }
          ol, ul {
            list-style: none;
          }
          blockquote, q {
            quotes: none;
          }
          blockquote:before, blockquote:after,
          q:before, q:after {
            content: '';
            content: none;
          }
          table {
            border-collapse: collapse;
            border-spacing: 0;
          }

        </style>

    </head>
    <body>
      
      <?php 

        $skinId = @$_GET["skinID"];
        $playerId = @$_GET["PlayerID"];
        $sessionId = @$_GET["SessionID"];
        $gameId = @$_GET["GameID"];
        $fun = @$_GET["fun"];
        $source = @$_GET["GameLaunchSourceID"];
     
        $path = "https://game.dagacube.net/radgame_staging.php?&skinID=$skinId&PlayerID=$playerId&SessionID=$sessionId&GameID=$gameId&fun=$fun&GameLaunchSourceID=$source";

      ?>
         
            <iframe id="ui-game-frame" src="<?php echo $path ?>" style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:0px; height:0px; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
              Your browser doesn't support iframes
            </iframe>
			<!--
			<iframe id="ui-game-frame" src="<?php echo $path ?>" style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
              Your browser doesn't support iframes
            </iframe>
			-->
            <script type="text/javascript">
                
				var frame = {

					getScreenDimensions: function () {
						var w = window.top ? window.top.window : window,
						d = window.top ? window.top.document : document,
						e = d.documentElement,
						g = d.getElementsByTagName('body')[0],
						w = w.innerWidth || e.clientWidth || g.clientWidth,
						h = w.innerHeight|| e.clientHeight|| g.clientHeight;
						
						return { w: w, h: h};
					},
					
					resize: function () {
						var d = frame.getScreenDimensions();
						document.getElementById("ui-game-frame").style.width = d.w + "px";
						document.getElementById("ui-game-frame").style.height = d.h + "px";
						
						alert(d.w + "x" + d.h);
					},
					
					setup: function () {
						
						frame.resize();
						
						var resized = function () {
							frame.resize();
						};
						
						window.addEventListener("resize", resized , false);
					}
				
				};
				
				window.onload = function () {
					setTimeout(function () {
					  frame.setup();
					}, 2000);
				};
				
                

            </script>

    </body>
</html>