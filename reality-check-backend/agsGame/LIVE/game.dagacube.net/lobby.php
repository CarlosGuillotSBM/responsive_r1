<html manifest="magicalvegas.manifest">

<head>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" /> 	
<title>Prototype</title>
</head>
<body>

<script>
	if (window.navigator.standalone == true) {
		document.write('<p>Welcome Home</p>');

		window.applicationCache.addEventListener('updateready',updateSite, false); 
		window.applicationCache.addEventListener('checking',logEvent,false);
		window.applicationCache.addEventListener('noupdate',logEvent,false);
		window.applicationCache.addEventListener('downloading',logEvent,false);
		window.applicationCache.addEventListener('cached',logEvent,false);  
		window.applicationCache.addEventListener('obsolete',logEvent,false);
		window.applicationCache.addEventListener('error',logEvent,false); 

	}
	else
	{
		document.write('<p>Tap the + button and choose "Add to Home Screen"</p>');
		document.write('<link rel="apple-touch-icon-precomposed" href="icon@2x.png"/>');
	}

	
	function updateSite(event) 
	{
		logEvent(event)
		window.applicationCache.swapCache();
		window.location.reload(false);	
	}

	
	// log cache
	function logEvent(event) 
	{      	
		elem = document.createElement("div")
		elem.innerHTML = event.type;
		document.body.insertBefore(elem,document.body.childNodes[0]);
	}
   
    function launchGame(name)
	{
		var url = "https://game.dagacube.net/radgame2.php?skinID=5&PlayerID=508749&SessionID=f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb&GameID=" + name + "&fun=0&GameLaunchSourceID=1";		
		document.location.href=url;
	}

</script>
	<table align="center">
		<tr>
			<td><img class="image" src="https://www.magicalvegas.com/_global-library/_upload-images/games/list-icons-122/cricket-star-slots-game.jpg" width="122" height="92" onclick="launchGame('cricket-star')"></td>			
			<td><img class="image" src="https://www.magicalvegas.com/_global-library/_upload-images/games/list-icons-122/cute-and-cuddly-slots-game.jpg" width="122" height="92" onclick="launchGame('cute-and-cuddly')"></td>
		</tr>
		<tr>		
			<td><img class="image" src="https://www.magicalvegas.com/_global-library/_upload-images/games/list-icons-122/gemix-slots-game.jpg" width="122" height="92" onclick="launchGame('gemix')"></td>
			<td><img class="image" src="https://www.magicalvegas.com/_global-library/_upload-images/games/list-icons-122/grease-danny-and-sandy-slots-game.jpg" width="122" height="92" onclick="launchGame('grease-danny-and-sandy')"></td>
		</tr>
		<tr>		
			<td><img class="image" src="https://www.magicalvegas.com/_global-library/_upload-images/games/list-icons-122/flowers-slots-game.jpg" width="122" height="92" onclick="launchGame('flowers')"></td>
			<td><img class="image" src="https://www.magicalvegas.com/_global-library/_upload-images/games/list-icons-122/fluffy-favourites-slots-game.jpg" width="122" height="92" onclick="launchGame('fluffy-favourites')"></td>
		</tr>		
	</table>	
</body>
</html>