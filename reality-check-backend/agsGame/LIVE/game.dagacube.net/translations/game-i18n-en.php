<?php

$i18n = array(
    "gameUnavailable"        => "This game is currently unavailable.",
    "onlyForFunded"          => "This game is only available for funded players.",
    "accountSelfExcluded"    => "Your account has been self excluded. Game play is not available.",
    "onlyWithRealFunds"      => "This game can only be played with real funds.",
    "onlyVIPplayers"         => "This game is restricted to VIP class players.<br>Please contact support to request details on how you can move to a higher class.",
    "sessionExpired"         => "Your session has expired.<br>Please go back to the lobby and log in again.",
    "backToLobby"			 => "Back to Lobby",
	"noDemoPlay"			 => "This game is not available in Fun Mode<br>Please log in to play."
);