<?php
/*
This file simulates a game.
It is for GameProvider=DaGaCube, GameID=1 or GameID=999.
!!!This game must be disabled for non test skins at production!!!
AF 05Jul15 - db56.php for php5.6.10
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode   = "DaGaCube";
$GameProviderID     = 1;
$GameProviderGameID = "simulate".$_POST["GameID"]; //1 or 999
$msg="";

if (!isset($_POST["Action"]))
  $_POST["Action"]="";

if (isset($_POST["Ref"])) {
  $Ref = $_POST["Ref"] + 1;
} else {
  $Ref = 1;
}
//$Ref   = rand(1,100000);
$RandomWager = rand(1,10);
$RandomWin   = rand(0,100);
if ($RandomWin >= 50)
  $RandomWin=0;

/*
$Ref   = 1;
$RandomWager = 1;
$RandomWin   = 0;
*/

if ($_POST["Action"] != "") {

	/*
	db calls xTxGame
	Returns:
		Result = 0 : Success
		      <> 0 : Various fail codes
	*/
  require_once("D:/WEB/inc/db56.php");

  $dbErr = $SdbParams = "";
  $dbSproc = "xTxGame";
  
	$dbParams = array(
	  "PlayerID"           => array(intval($_POST["PlayerID"]), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
	  "SessionID"          => array($_POST["SessionID"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(36)),
	  "IP"                 => array($_SERVER["REMOTE_ADDR"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
	  "GameProviderID"     => array(intval($GameProviderID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
	  "GameProviderGameID" => array($GameProviderGameID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(100)),
	  "GameLaunchSourceID" => array(1, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
	  "Action"             => array($_POST["Action"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
	  "Wager"              => array(floatval($_POST["Wager"]), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
	  "Win"                => array(floatval($_POST["Win"]), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
	  "Ref"                => array(intval($_POST["Ref"]), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
	  "SubRef"             => array(intval($_POST["SubRef"]), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
	  "Refund"             => array(floatval($_POST["Refund"]), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
	  "RefundRef"          => array("", SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(50)),
	  "RefundNotes"        => array("", SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(4000))
	);

  $db = "";

if(dbInit("", "Main", __FILE__."[".__LINE__."]", 1) == false) {
    WriteLog("db",E_DB_CONNECT,$dbErr);
    WriteLog($GameProviderCode,E_DB_CONNECT,$dbErr,$_POST["PlayerID"]);
		$msg = "*** Error [".E_DB_CONNECT."] ".$glo["LogCodes"][E_DB_CONNECT];
  } else {
    $rs = dbExecSproc($dbSproc,$dbParams);
    if ($rs === false) {
      WriteLog("db",E_DB_SELECT,$dbErr);
      WriteLog($GameProviderCode,E_DB_SELECT,$dbErr,$_POST["PlayerID"]);
		  $msg = "*** Error [".E_DB_SELECT."] ".$glo["LogCodes"][E_DB_SELECT];
    } else {
    	$row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
      if (!isset($row) || $row==null) {
        WriteLog("db",E_DB_RETURN,$dbErr);
        WriteLog($GameProviderCode,E_DB_RETURN,$dbErr,$_POST["PlayerID"]);
		    $msg = "*** Error [".E_DB_RETURN."] ".$glo["LogCodes"][E_DB_RETURN];
      }
    }
  }

  if ($msg != "") {
		$_POST["Action"] = "";
  }

  dbFree(@$rs);
  dbClose(@$db);

  if ($msg == "") {
    //db access was successful, other error
    if ($row->Result != 0) {
      if ($row->Result == 104 || $row->Result == 105)
        $row->Result = 104;

			WriteLog($GameProviderCode, $row->Result, "",$_POST["PlayerID"]);
      $msg = "*** Error [".$row->Result."] ".$glo["LogCodes"][$row->Result];
    }
  }

  if ($msg == "") {
    $msg ="Action:$_POST[Action] Result:".$row->Result."<br>";
    if ($_POST["Action"] != "CheckSession") {
      $msg.="Wager:".($row->WagerReal+$row->WagerBonus+$row->WagerBonusWins)." (Real:".$row->WagerReal." Bonus:".$row->WagerBonus." BonusWins:".$row->WagerBonusWins.")<br>";
      if ($_POST["Action"] != "GetBalance") {
        $msg.="Win:".$_POST["Win"]."<br>";
        $msg.="Ref:$_POST[Ref] SubRef:$_POST[SubRef] Refund:$_POST[Refund]<br>";
      }
      $msg.="Balance:".$row->Balance."   Real:".$row->Real." Bonus:".$row->Bonus." BonusWins:".$row->BonusWins."<br>";
      $msg.="TxGameID: ".$row->agsRef;
      WriteLog($GameProviderCode,0,str_replace("<br>"," ",$msg),$_POST["PlayerID"]);
    }
  }
}
?>
<html>
<head>
<title>DagaCube Simulate Game</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="simulate.css" />

<script type="text/javascript">
  function submit() {
    if (document.theForm.SubRef.value == 0) {
      alert("SubRef cannot be zero");
      return false;
    }
    document.theForm.submit();
  }
</script>
</head>
<body class="fixed">
GameID:<?php echo $_POST["GameID"];?><br>
<?php
  echo $msg;
  $msg="";
?>
<br><br>
<form name="theForm" target="_self" method="post" action="<?php echo $_POST["url"];?>">
  <input type="hidden" name="PlayerID"  value="<?php echo $_POST["PlayerID"];?>">
  <input type="hidden" name="SessionID" value="<?php echo $_POST["SessionID"];?>">
  <input type="hidden" name="url"       value="<?php echo $_POST["url"];?>">
  <input type="hidden" name="GameID"    value="<?php echo $_POST["GameID"];?>">

  <table border="1" cellpadding="1" cellspacing="0" bordercolor="#999999">
    <tr>
      <td><label for="Title">Action</label></td>
      <td>
      <select name="Action" id="Action" class="fixed">
        <OPTION value="Tx" selected>Tx</OPTION>
        <OPTION value="GetBalance">GetBalance</OPTION>
        <OPTION value="CheckSession">CheckSession</OPTION>
        <OPTION value="Recon">Recon</OPTION>
        <OPTION value="Void">Void</OPTION>
        <OPTION value="VoidCogs">VoidCogs</OPTION>
      </select>
    </td>
    </tr>

    <tr>
      <td><label for="Wager">Wager</label></td>
      <td><input type="text" id="Wager" name="Wager" value="<?php echo $RandomWager;?>" /></td>
    </tr>

    <tr>
      <td><label for="Win">Win</label></td>
      <td><input type="text" id="Win" name="Win" value="<?php echo $RandomWin;?>" /></td>
    </tr>

    <tr>
      <td><label for="Ref">Ref</label></td>
      <td><input type="text" id="Ref" name="Ref" value="<?php echo $Ref;?>" /></td>
    </tr>

    <tr>
      <td><label for="SubRef">SubRef</label></td>
      <td><input type="text" id="SubRef" name="SubRef" value="-1" /></td>
    </tr>

    <tr>
      <td><label for="Refund">Refund</label></td>
      <td><input type="text" id="Refund" name="Refund" value="0" /></td>
    </tr>
  </table>
</form>
<input type="button" value="Submit" onClick="javascript:void (submit());">
</body>
</html>