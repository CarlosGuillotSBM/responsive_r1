<?php
/*
This file is called from anywhere any game is launched from.
It can be located inline or in a new window etc - the caller deals with that.
POST vars sent here are:
  PlayerID  int
  SessionID guid
  GameID (agsMain.Game.ID to launch)  int
  GameLaunchSourceID tinyint (see agsMain.GameLaunchSource - defaults to zero)
AF 22Oct112 - New code to retreive all relevant config details from database
AF 23Oct12 - removed $Environment as the DB deals with it
           - Added new GameStatus=5 - cannot play this game - wrong player class
AF 26Oct12 - write Launch log line before writing ant error
AF 14Dec12 - Added Bally Games
AF 14Jan13 - Added Eyecon
AF 18Jan13 - Force IGT skins from 2 and above to use D002 skin to avoid palava with IGT assigning new skin.
AF 22Feb13 - If FirstLaunch for Sheriff games show intro movie else dont
AF 14Mar13 - Changed GameProviderGameID to GameProviderGameLaunchID
             Removed references to ASH Gaming
             MGS Games from 6001-6499 = traditional web games
             MGS games from 6500-6999 = html5 games
HI 03apr13 - added gender and dob to IGT games launch
HI 08apr13 - hack for IGT mobile roulette denom
AF 14May13 - Eyecon games launch via a redirect with a query string to avoid security warnings as they dont use SSL
HI 15May13 - Eyecon testing IP restrictions
AF 23May13 - Remove Eyecon testing IP restrictions
HI 25jul13 - return width, height for games
AF 20Aug13 - Last 4 characters of IGT securetoken is Main.Game.ID and is used in xTxGame to lookup the correct game because their html5 and flash games use the same igt code
AF 29Aug13 - Added PlaynGo
AF 09Sep13 - LuckyPants skin 3 to use D003.
AF 13Sep13 - Modified PlaynGo
AF 15Sep13 - Modified PlaynGo - use ticket for mobile game launch
RS 18Sep13 - Added code at bottom of this file to avoid opening home page in new tab from ios devices in standalone mode
AF 06Oct13 - Modified PlaynGo - launch with PlayerID only
CM 18mar14 - Closing the window button when there was an error.
NO 18Jun14 - Added support for i18n
HI 17Jul14 - Don't send username to eyecon
AF 09Oct14 - Added NetEnt games
AF 10Oct14 - Proper checks for $ErrMsg
AF 24Oct14 - Fixed PlaynGo games as we now get their gameId we can associate it with the session so players can have more than 1 PnG game open at any one time
AF 18Dec14 - NetEnt with inclusion.js
AF 19Feb15 - NetEnt play for fun
           - made all $skinID $SkinID
HI 09mar15 - NetEnt Scaling
AF 05Jul15 - db56.php for php5.6.10
HI 12nov15 - add scientific games
NO 07Jan16 - DEV-3204 changed function goHome to check how was the game opened and close it properly
RS 07Apr16 - Appended ProviderID to SessionID for Geco games
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions for agsGame
require_once("D:/WEB/inc/Mobile_Detect.php"); //global variables and functions for agsGame


$PlayerID = ((isset($_REQUEST["PlayerID"]) ? $_REQUEST["PlayerID"] : 0));
if (!(is_numeric($PlayerID)))
  $PlayerID = 0;
  
setcookie("PlayerID", $PlayerID);
  
$SessionID = ((isset($_REQUEST["SessionID"]) ? $_REQUEST["SessionID"] : ""));
if (strlen($SessionID) != 36)
  $SessionID = "";

setcookie("SessionID", $SessionID);
  
// gameID is actually a string
$Game = ((isset($_REQUEST["GameID"]) ? $_REQUEST["GameID"] : ''));
  
$DeviceID = ((isset($_REQUEST["DeviceID"]) ? $_REQUEST["DeviceID"] : 1));
if (!(is_numeric($DeviceID)))
  $DeviceID = 1;

$GameLaunchSourceID = ((isset($_REQUEST["GameLaunchSourceID"]) ? $_REQUEST["GameLaunchSourceID"] : 0));
if (!(is_numeric($GameLaunchSourceID)) || $GameLaunchSourceID < 0 || $GameLaunchSourceID > 255)
  $GameLaunchSourceID = 0;

$funMode =  ((isset($_REQUEST["fun"]) ? $_REQUEST["fun"] : 0));
if ($funMode == 1 || $funMode == true || $funMode === 'true') {
  $funMode = 1;
  $SkinID = ((isset($_REQUEST["skinID"]) ? $_REQUEST["skinID"] : 0));
  $SessionID = 'C601EA5C-9F19-4697-8FBC-CF2A867CD76B';
} else {
  $SkinID = 0;
  $funMode = 0;
  if ($PlayerID == 0 || $SessionID == "" || $Game == '') {    
    WriteLog("Application", E_INVALID_GAME_LAUNCH_PARAMS, "Player IP=".$_SERVER["REMOTE_ADDR"]." SessionID=".$SessionID." game=".$Game, $PlayerID);//310: Game - invalid post variables to launch game
    exit;
  }
}

$GameProviderCode = "";

require_once("D:/WEB/inc/db56.php");

$dbSproc = "WebGetGamePropertiesRAD";

$dbParams = array(
  "PlayerID"           => array(intval($PlayerID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "SessionID"          => array($SessionID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(36)),
  "IP"                 => array($_SERVER["REMOTE_ADDR"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),//players IP
  "Game"               => array($Game, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(100)),
  "DeviceCategoryID"   => array(intval(getDevice()), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "GameLaunchSourceID" => array(intval($GameLaunchSourceID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "FunMode"            => array(intval($funMode), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "SkinID"             => array(intval($SkinID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "DeviceID"           => array(intval($DeviceID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT)
);

$dbErr = $SdbParams = "";
$db = "";

if(dbInit("", "Main", __FILE__."[".__LINE__."]", 1) == false) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams);
  if ($rs === false) {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  } else {
    $row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }
}

dbFree($rs);
dbClose($db);

if ($dbErr != "") {
  WriteLog("Application", $dbErr, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
  exit;
}
/*
$GameDescription = $GameID;
$Width  = 800;
$Height = 650;
*/

if($row->Result == 104) {
  $HomeURL = $row->HomeURL;
  $GameStatus = 6; // bad session
} else {
  //db access was successful, other error
  if ($row->Result != 0) {
    WriteLog("Application", $row->Result, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
    exit;
  }
  $GameID			  = $row->GameID;	
  $GameStatus         = $row->GameStatus;//0=disabled (or not available for the skin), 1=enabled, 2=Cannot Play - Funded only, 3=cannot play - Self Excluded, 4=Cannot play - real funds only
  $GameProviderCode   = $row->GameProviderCode;
  $GameProviderGameID = $row->GameProviderGameID;
  $GameProviderGameLaunchID = $row->GameProviderGameLaunchID;
  $GameDescription    = $row->GameDescription;
  $SkinID             = $row->SkinID;
  $CurrencyCode       = $row->CurrencyCode;
  $LanguageCode       = $row->LanguageCode;
  $CountryCode        = $row->CountryCode;
  $CountryCode2       = $row->CountryCode2;
  $GameURL            = $row->GameURL;
  $SiteID             = $row->SiteID;
  $CoinSizeString     = $row->CoinSizeString;
  $MaxDoubleUp        = $row->MaxDoubleUp;
  $MaxWin             = $row->MaxWin;
  $Username           = $row->Username;
  $FirstLaunch        = $row->FirstLaunch;
  $PlayerDOB          = $row->PlayerDOB;
  $PlayerGender       = $row->PlayerGender;
  $HomeURL            = $row->HomeURL;
  $LobbyURL           = $row->LobbyURL;
  $CashierURL         = $row->CashierURL;
  $PromoURL           = $row->PromoURL;
  $BackgroundURL      = $row->BackgroundURL;
  $Width              = $row->Width;
  $Height             = $row->Height;
  $GUID               = $row->GUID;
  $GameTypeID         = $row->GameTypeID;
  $SkinCode           = $row->SkinCode;
  $NetEnt_merchantId  = $row->NetEnt_merchantId;
  $NetEnt_merchantPassword = $row->NetEnt_merchantPassword;
  $ModuleID           = $row->ModuleID;
  $ClientID           = $row->ClientID;
  $ProductID          = $row->ProductID;
  $ProviderID		  = $row->ProviderID;
  $MasterProviderID	  = $row->MasterProviderID;
  $HistoryURL		  = $row->HistoryURL;
  $Interval			  = $row->Interval;
  $ErrMsg             = "";
  $ShiftID            = str_replace(' ', '', strtolower($GameDescription));
}

setcookie("HistoryURL", @$HistoryURL);

// Eyecon testing IP restrictions
/*
$ip = getPlayerIP();
if($GameID > 2000 && $GameID < 3000)
{
  $a = Array();
  $a = array_merge($a,getIPRange('180.214.67.98',27));
  $a = array_merge($a,getIPRange('180.214.71.96',27));
  $a = array_merge($a,getIPRange('203.33.60.192',28));
  $a[] = '62.232.79.254';
  $a[] = '196.192.15.123';
  $a[] = '58.172.242.216';
  $a[] = '120.151.3.227';
  $a[] = '203.45.176.36';

  if(!in_array($ip,$a)) $GameStatus = 0;

}
*/
//print_r($dbParams);
//print_r($row);


// i18n
if (isset($SkinID) && $SkinID == 4) {
  require_once("translations/game-i18n-gr.php");
} else {
  require_once("translations/game-i18n-en.php");
}

@WriteLog($GameProviderCode, -999, "Launch Game [$GameID] $GameDescription. GameLaunchSourceID=$GameLaunchSourceID", $PlayerID); //-999 is special code referring to game launch for logs

/*
if ($PlayerID == 7 && $GameProviderCode == "NetEnt") {
  $NetEnt_merchantId  = "BB_merchant";
  $NetEnt_merchantPassword = "vestUfr5swan";
}
*/

switch($GameStatus) {
  case 0:
    if ($funMode == 0)
      WriteLog($GameProviderCode, E_GAME_DISABLED, "", $PlayerID);
      
    $ErrMsg=$i18n["gameUnavailable"];
    break;

  case 2://Funded Only
    WriteLog($GameProviderCode, E_PLAYER_NOT_FUNDED, "", $PlayerID);
    $ErrMsg=$i18n["onlyForFunded"];
    break;

  case 3://Self Excluded
    WriteLog($GameProviderCode, E_SELF_EXCLUSION, "", $PlayerID);
    $ErrMsg=$i18n["accountSelfExcluded"];
    break;

  case 4://Real only
    WriteLog($GameProviderCode, E_REQUIRE_REAL_FUNDS, "", $PlayerID);
    $ErrMsg=$i18n["onlyWithRealFunds"];
    break;

  case 5://Player class restriction
    WriteLog($GameProviderCode, E_PLAYER_CLASS_RESTRICTED, "", $PlayerID);
    $ErrMsg=$i18n["onlyVIPplayers"];
    break;

  case 6://Bad Session
    $ErrMsg=$i18n["sessionExpired"];
    break;
	
  case 7://Game launched in fun mode, but game does not support it.
    WriteLog($GameProviderCode, E_NO_DEMO_PLAY, "", $PlayerID);
    $ErrMsg=$i18n["noDemoPlay"];
    break;	
	
}

if ($ErrMsg == "") {  

  if ($GameProviderCode == "Eyecon") {
    if(stripos($GameURL, '?') == FALSE)
      $gameloc = $GameURL."?";
    else
      $gameloc = $GameURL."&";

	$intervalInSecs = (intval($Interval) * 60);
	
    $gameloc = $gameloc.
                 "uid=".$PlayerID.
                 "&alias=".$PlayerID. //.$Username.
                 "&gameid=".$GameProviderGameLaunchID.
                 "&guid=".$SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).
                 "&brand=".$SiteID.
                 "&nid=".$SkinID.
                 "&lang=".$LanguageCode.
                 "&cur=".$CurrencyCode;
				 if ($DeviceID > 1 && $intervalInSecs > 0) {
					 $gameloc .= "&RealityCheckRemaining=".$intervalInSecs.
					 "&RealityCheckFrequency=".$intervalInSecs.
					 "&SessionDuration=0";
				 } else {
					$gameloc .= "&RealityCheckFrequency=-1";
				 }
  }

  if ($GameProviderCode == "IGT") {
    if($SkinID == 5 || $SkinID == 6 || $SkinID == 7)
    {
      $SkinCode = "D999";
    }
    else
    {
      $SkinCode = "D".str_pad($SkinID, 3, '0', STR_PAD_LEFT);
    }
  }

  if ($GameProviderCode == "PlaynGo") {
  
	// hack!
	//$GameURL = 'http://custstage.playngonetwork.com/Casino/js';
  
  
    if ($GameID < 14500) {//Flash games
      $gameloc = $GameURL.
                 "?div=PlaynGo".
                 "&gid=".$GameProviderGameLaunchID.
                 "&lang=".$LanguageCode."_".$CountryCode2.
                 "&pid=121".
                 "&username=".$PlayerID.
                 "&practice=".$funMode.
                 //"&password=".
                 "&background=0x000000".
                 "&sound=1".
                 //"&ctx=".
				 "&width=100%".
                 "&height=100%".
				 
                 //"&width=".($Width*.96)."px".
                 //"&height=".($Height*0.87)."px".
                 "&demo=2".
                 //"&callback="
                 "";
    } else {//mobile games
      $gameloc = $GameURL.
                 "?pid=121".
                 "&GID=".$GameProviderGameLaunchID.
                 "&lang=".$LanguageCode."_".$CountryCode2.
                 "&practice=".$funMode.
                 "&ctx=default".
                 //"&user=".
                 "&ticket=".$PlayerID.
                 "&lobby=".$LobbyURL;
				 if ($DeviceID > 1) {
					 $gameloc .= "&iframeoverlay=https://game.dagacube.net/playngo/reality-check/?s=" . $SessionID . "%26p=".$PlayerID;
				}
                 //"&scale=Yes".
                 //"&urlmode=".
    }
  }	

  /*
  NetEnt requires the player to login to the game server 1st to get a NetEnt sessionId
  */
  if ($GameProviderCode == "NetEnt") {
  
	$realityURL = "";
	
	if ($DeviceID > 1) {
		$realityURL = 'https://game.dagacube.net/netent/inclusion.html?i=' . $Interval . '%26history=' . $HistoryURL . '%26home=' . $HomeURL;
	}
  
    $NetEnt_sessionId = "";
    if ($CountryCode2 == "GG")//Guernsey must be considered UK
      $CountryCode2 = "GB";

    if ($funMode == 1) {
      $NetEnt_sessionId = "DEMO-".chr( mt_rand( 97 ,122 ) ) .substr( md5( time( ) ) ,1 )."-GBP";
    } else {
    	/*
    	Due to an eventual error that occurs with php 5.6.x in line:
    	  $response = $client->loginUserDetailed(new SoapVar($params, SOAP_ENC_OBJECT));
    	we have had to curl out to game.dagacube.net\NetEntGetSessionID which uses php 5.4.43 to get the $NetEnt_sessionId
    	*/
      $curlURL  = "https://game.dagacube.net/NetEntGetSessionID/";

		  $curlData = "PlayerID=".$PlayerID.
                  "&GameID=".$GameID.
                  "&CountryCode2=".$CountryCode2.
                  "&PlayerGender=".strtoupper($PlayerGender).
                  "&PlayerDOB=".str_replace("-","",$PlayerDOB).
                  "&Username=".$Username.
                  "&NetEnt_merchantId=".$NetEnt_merchantId.
                  "&NetEnt_merchantPassword=".$NetEnt_merchantPassword.
                  "&CurrencyCode=".$CurrencyCode;

		  $curlHeader ="POST /".substr($curlURL,8)." HTTP/1.1\r\n".
		               "Content-type: application/x-www-form-urlencoded\r\n".
		               "Content-length: %d\r\n\r\n";
		  $curlHeader = sprintf($curlHeader,strlen($curlData));

		  //Process
		  $ch = curl_init();
		  if (!$ch) {
        WriteLog($GameProviderCode, E_INVALID_PARAMS, "Could not init curl.", $PlayerID);
        $ErrMsg=$i18n["gameUnavailable"];
		  } else {
		    curl_setopt($ch, CURLOPT_URL,$curlURL);
		    curl_setopt($ch, CURLOPT_POST, 9);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlData);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    //curl_setopt($ch, CURLOPT_SSLVERSION, 3);
		    //curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'SSLv3');
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10); //allow 10 secs
		
		    $curlResponse = curl_exec ($ch);
		    curl_close ($ch);
		
		    if (!$curlResponse) {
          WriteLog($GameProviderCode, E_INVALID_PARAMS, "No response from NetEnt loginUserDetailed - possible timeout", $PlayerID);
          $ErrMsg=$i18n["gameUnavailable"];
		    } else {
		    	$response = unserialize(base64_decode($curlResponse));
          $NetEnt_sessionId = $response["NetEnt_sessionId"];
		    	if ($response["Error"] != "") {
	          WriteLog($GameProviderCode, E_INVALID_PARAMS, $response["Error"], $PlayerID);
	          $ErrMsg=$i18n["gameUnavailable"];
		    	}
		    }
		  }
    }//end funMode

    if ($GameID >= 17500) {//mobile
      $NetEnt_gameName = $GameProviderGameLaunchID.".mobile";
    } else {
      $NetEnt_gameName = $GameProviderGameLaunchID.".desktop";
    }

  }//end NetEnt
}

// Find out the device type
function getDevice()
{
	$detect = new Mobile_Detect;
	
	if($detect->isMobile() || $detect->isTablet()) {return 2;}	
	return 1;
}


/*
DaGaCube      1-1999 (GameID=1 and GameID=999 - simulator)
Eyecon     2001-2999
Sheriff    3001-3999
IGT        4001-4999
NextGen    5001-5999
MGS        6001-6999
Baddamedia 7001-7999
Bally      9001-9999
Daub-Naked Penguin Boy 10001-19999
*/

?>
<html>
<head>
<title><?php echo @$GameDescription;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<?php if ($ErrMsg == ""):?>
<script type="text/javascript">
	// pass game dimensions to parent window - sending as string IE9 does not accept objects
	top.postMessage('{ "width": <?php echo $Width;?>, "height": <?php echo $Height;?>}', '*');
</script>
<?php endif;?>

<?php if ($GameProviderCode == "NetEnt" && $ErrMsg == ""):?>
<?php if ($GameID >= 17450 && $GameID <= 17499)://Refers to Live Casino games only?>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>

<?php else:?>

<script type="text/javascript" src="<?php echo $GameURL;?>gameinclusion/library/gameinclusion.js"></script>
<script type="text/javascript">
	var startGame = function () {

		var flashparams = {
			allowscriptaccess: "always",
			wmode: "window"
	  };
	
		var mobileparams = {
			lobbyUrl: "<?php echo $LobbyURL;?>",
	  };
	
	  <?php if ($GameID >= 17500):?>
		var config = {
			  lobbyURL: "<?php echo $LobbyURL;?>",
			  gameId: "<?php echo $GameProviderGameID;?>",
				staticServer: "<?php echo $GameURL;?>",
				gameServer: "https://daub-game.casinomodule.com/",
				sessionId : "<?php echo $NetEnt_sessionId;?>",
				targetElement: "netEntGame",
				walletMode: "seamlesswallet",
				language: "en",
				customParameter: "<?php echo $SessionID;?>",
				mobileParams: mobileparams,
				pluginUrl: "<?php echo @$realityURL;?>"
		};
	  <?php else:?>
		var config = {
			  gameId: "<?php echo $GameProviderGameID;?>",
				staticServer: "<?php echo $GameURL;?>",
				gameServer: "https://daub-game.casinomodule.com/",
				sessionId : "<?php echo $NetEnt_sessionId;?>",
				targetElement: "netEntGame",
				walletMode: "seamlesswallet",
				language: "en",
				customParameter: "<?php echo $SessionID;?>",
				flashParams: flashparams
				
		};
		config.width = '100%';
		config.height = '99%';
		config.enforceRatio = false;
	  <?php endif;?>
	    
	
		var success = function (netEntExtend) {
			//Game launch successful
			//netEntExtend.resize(<?php echo $Width*.96;?>, <?php echo $Height*.87;?>);			
			netEntExtend.addEventListener("gameReady", function () {netEntExtend.get("volumeLevel", function (volumeLevel) {});
			netEntExtend.set("volumeLevel", 50);
			});
		};
		
		var error = function (e) {
			alert("error\n" + e.message);
			//Error handling here
		};
		netent.launch(config, success, error);
	};
	window.onload = startGame;
</script>
<?php endif;?>
<?php endif;?>

<?php if ($GameStatus == 1 && $ErrMsg == ""):?>
	<?php if ($GameProviderCode == "PlaynGo" && $GameID < 14500):?>
	<!--
	  <script>window.resizeTo(<?php echo $Width;?>,<?php echo $Height;?>);</script>
	  -->
	  <script src="<?php echo $gameloc;?>" type="text/javascript"></script>
	  <script type="text/javascript">
	    function ShowCashier() {
	      window.location = '';
	    }
	    function PlayForReal() {
	      window.location = '';
	    }
	    function Logout() {
	      window.close();
	    }
	  </script>
	<?php else:?>
    <?php if ($GameProviderCode != "NetEnt"):?>
		  <script language="JavaScript">
		    function onloadH(e) {
		      <?php if ($GameProviderCode == "Eyecon" || $GameProviderCode == "PlaynGo"):?>
		        document.location.href = "<?php echo $gameloc;?>";
		      <?php else:?>
		        document.theForm.submit();
		      <?php endif;?>
		      return true;
		    }
		  </script>
	  <?php endif;?>
	<?php endif;?>
<?php endif;?>

<style type="text/css">
body{
  font-family: Arial, Tahoma, Geneva, sans-serif;
  color: #0a375e;
  font-size:#fff;
  font-weight: bold;
  font-size: 20px;
  background-color:#000000;
}
#err{
  margin: 120px auto;
  text-align:center;
  background: url('images/error-modal.png') no-repeat top left;
  width:600px;
  height: 334px;
  padding-top: 50px;
}
#gamename{
  font-size: 30px;
  color: #fff;
  display: block;
}
#loading{
  text-align:center;
  width:100%;
}
.button {
  font-size: 12px;
  font-weight: bold;
  color: #ffffff;
  text-decoration:none;
  padding: 20px 20px;
  margin-top:10px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  border: 1px solid #1f5500;
  -moz-box-shadow:
    0px 1px 2px rgba(000,000,000,0.8),
    inset 0px 1px 1px rgba(255,255,255,0.7);
  -webkit-box-shadow:
    0px 1px 2px rgba(000,000,000,0.8),
    inset 0px 1px 1px rgba(255,255,255,0.7);
  -o-box-shadow:
    0px 1px 2px rgba(000,000,000,0.8),
    inset 0px 1px 1px rgba(255,255,255,0.7);
  box-shadow:
    0px 1px 2px rgba(000,000,000,0.8),
    inset 0px 1px 1px rgba(255,255,255,0.7);
  text-shadow:
    1px 1px 1px rgba(000,000,000,1),
    0px 1px 0px rgba(255,255,255,0.3);
    max-width: 200px;
}
.button-orange {
  font-size: 13px;
  margin-top:0px;
  background: #fc9700; /* Old browsers */
  background: -moz-linear-gradient(top,  #fc9700 0%, #eb2a0c 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fc9700), color-stop(100%,#eb2a0c)); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  #fc9700 0%,#eb2a0c 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  #fc9700 0%,#eb2a0c 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  #fc9700 0%,#eb2a0c 100%); /* IE10+ */
  background: linear-gradient(to bottom,  #fc9700 0%,#eb2a0c 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fc9700', endColorstr='#eb2a0c',GradientType=0 ); /* IE6-9 */

  border: 1px solid #c10000;
  text-shadow:
  1px 2px 1px rgba(166,37,8,1),
  0px 1px 0px rgba(255,255,255,0.3);
}


</style>
</head>
<?php if ($GameStatus == 1 && $ErrMsg == ""):?>
  <?php if ($GameProviderCode == "PlaynGo" && $GameID < 14500):?>
    <div id="PlaynGo">
    You either have JavaScript turned off or an old version of <a target="_blank" href="http://get.adobe.com/flashplayer/">Adobes Flash Player</a>
    </div>
  <?php elseif ($GameProviderCode == "NetEnt"):?>
	  <?php if($GameID >= 17450 && $GameID <= 17499):?>
			<body scrolling="no" style="margin: 0pt; padding: 0pt;overflow: hidden">
			<div style="position:absolute;z-pos:10;">
			</div>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
				<tr>
					<td width = "1024" align="center">
						<table id="gameTable" border="0" cellpadding="0" cellspacing="0" width="1024">
							<tr height="768">
								<td>
									<!-- game is loaded here -->
									<object id="flashcontent" type="application/x-shockwave-flash" width="100%" height="100%"></object>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
	
			<script>
			  function loadNetEntGame() {
	  		  var serviceHost = "https://daub-livegame.casinomodule.com";
		  	  var staticUrl = "<?php echo $GameURL;?>"; // Alternatively add altBrand at the end. Don't forget slash at the end!

					var flashvars = {
						accountType: "SEAMLESS_WALLET",
						brandingLocale: "<?php echo $LanguageCode;?>",
						casinoId: "daub",
						customLeaveTable: "false",
						gameHostURL: staticUrl,
						lang: "<?php echo $LanguageCode;?>",
						lobbyURL: escape("<?php echo $LobbyURL;?>"),
						jsonRequestURL: serviceHost,
						sessionid: "<?php echo $NetEnt_sessionId;?>",
						showMiniLobby: "true",
						wmode: "direct"
				  };
				
					var flashparams = {
						allowFullScreen: "true",
						allowFullScreenInteractive: "true",
						allowscriptaccess: "always",
						base: "https://daub-livegame.casinomodule.com/",
						FlashVars: flashvars,
						loop: "true",
						quality: "high",
						scale: "exactfit",
						wmode: "direct"
				  };
					swfobject.embedSWF(staticUrl + "livecasinoloader/livecasinoloader-application.swf", "flashcontent", "100%", "100%", "10.0.42.34", "swfobject/expressInstall.swf", flashvars, flashparams);
				}
	
				function openLCHistory(url) {
				  var features = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no,width=440,height=420";
				  var remote = window.open(url, "history", features);
				}
	
				function rules(url) {
				  var features = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no,width=440,height=420";
				  var remote = window.open(url, "rules", features);
				}
	
				function netent_onLeaveTableClicked() {
					// Available in Live Casino 1.4 and up.
					// OPTIONAL: Replace this script with a custom script for leaving the game. This will only run if the flashvar 'customLeaveTable' has been set to true.
					//alert("You clicked on leave table and this is your custom javascript running!");
          alert("You left the table");
				}
			</script>
	
		<?php else:?>

	    <body>
		  <div id="netEntGame">
	      <?php echo "Launching $GameDescription...";?>
	      <br><br>
		    <div id="loading">
		      <img src="images/skin<?php echo $SkinID;?>.png" />
		    </div>
		  </div>
    <?php endif;?>
  <?php else:?>
    <body onLoad="onloadH();">    
    <br><br>
    <div id="loading" style='width:100%'>
      <img src="images/skin<?php echo $SkinID;?>.png" width='150px' height='150px'/>
    </div>
  <?php endif;?>
<?php else:?>
  <body>
  <div id=err>
    <?php
      echo "<span id=gamename>".@$GameDescription."</span>";
      echo $ErrMsg;
    ?>
    <br/><br/><br/><br/>
    <a id="erra" href="javascript:goHome('<?php echo $HomeURL;?>')" class="button button-orange "><?php echo $i18n["backToLobby"] ?></a>
  </div>
  <br>
<?php endif;?>

<?php if ($GameStatus == 1 && $ErrMsg == "" && $GameProviderCode != "NetEnt" && $GameProviderCode != "PlaynGo"):?>

  <?php if ($GameProviderCode == "DaGaCube" && ($GameID == 1 || $GameID == 999)):?>
	<form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>">
		<input type="hidden" name="PlayerID"    value="<?php echo $PlayerID;?>">
		<input type="hidden" name="SessionID"   value="<?php echo $SessionID;?>">
		<input type="hidden" name="SkinID"      value="<?php echo $SkinID;?>">
		<input type="hidden" name="url"         value="<?php echo $GameURL;?>">
		<input type="hidden" name="GameID"      value="<?php echo $GameProviderGameLaunchID;?>">
	</form>	
  <?php endif;?>

  <?php if ($GameProviderCode == "DaGaCube" && $GameID != 1 && $GameID != 999 && $funMode == 0):?>
	<?php if (($GameID >= 300 && $GameID < 400) || ($GameID >= 500 && $GameID < 600) ){
		$GameURL = str_replace("gameclient.dagacube.net","gameclient.dagacube.net:8433",$GameURL);
		}
	?>
	
    <form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>">
		<input type="hidden" name="uid"       value="<?php echo $PlayerID;?>">
		<input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="guid"      value="<?php echo $SessionID;?>">
		<input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">		
		<input type="hidden" name="url"       value="<?php echo $GameURL;?>">
		<input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="operator"  value="1">
		<input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
		<input type="hidden" name="HomeURL"    value="<?php echo $HomeURL;?>">
		<input type="hidden" name="LobbyURL"    value="<?php echo $LobbyURL;?>">
		<input type="hidden" name="CashierURL"    value="<?php echo $CashierURL;?>">
		<input type="hidden" name="PromoURL"    value="<?php echo $PromoURL;?>">
		<input type="hidden" name="BackgroundURL"    value="<?php echo $BackgroundURL;?>">
	</form>	
  <?php endif;?>

    <?php if ($GameProviderCode == "DaGaCube" && $GameID != 1 && $GameID != 999 && $funMode == 1):?>
	<form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>">
		<input type="hidden" name="sessionType"    value="FUN">
		<input type="hidden" name="uid"       value="<?php echo $PlayerID;?>">
		<input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="url"       value="">
		<input type="hidden" name="guid"      value="<?php echo $GUID;?>">
		<input type="hidden" name="url"       value="<?php echo $GameURL;?>">
		<input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="operator"  value="1">
		<input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
		<input type="hidden" name="HomeURL"    value="<?php echo $HomeURL;?>">
		<input type="hidden" name="LobbyURL"    value="<?php echo $LobbyURL;?>">
		<input type="hidden" name="CashierURL"    value="<?php echo $CashierURL;?>">
		<input type="hidden" name="PromoURL"    value="<?php echo $PromoURL;?>">
		<input type="hidden" name="BackgroundURL"    value="<?php echo $BackgroundURL;?>">
	</form>	
  <?php endif;?>

  <?php if ($GameProviderCode == "Shifttech"):?>
  <form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
    <input type="hidden" name="uid"    value="<?php echo $PlayerID;?>">
    <input type="hidden" name="guid"   value="<?php echo $SessionID;?>">
	<input type="hidden" name="gameId"   value="<?php echo $ShiftID;?>">
    <?php if ($DeviceID > 1) { ?>
      <input type="hidden" name="interval" value="<?php echo $Interval;?>">
      <input type="hidden" name="historyURL" value="<?php echo $HistoryURL;?>">
      <input type="hidden" name="homeURL" value="<?php echo $HomeURL;?>">
    <?php } ?>
  </form>
  <?php endif;?>

  <?php if ($GameProviderCode == "Sheriff"):?>
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
		<input type="hidden" name="site_id"          value="<?php echo $SiteID;?>">
		<input type="hidden" name="player_reference" value="<?php echo $PlayerID;?>">
		<input type="hidden" name="game_id"          value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="session_id"       value="<?php echo $SessionID;?>">
		<input type="hidden" name="mode"             value="real">
		<input type="hidden" name="locale"           value="en_US">
		<input type="hidden" name="currency"         value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="x_site_id"        value="<?php echo $SiteID;?>">
		<input type="hidden" name="x_GameLaunchSourceID" value="<?php echo $GameLaunchSourceID;?>">
		<input type="hidden" name="show_intro"       value="<?php echo $FirstLaunch;?>">
	</form>	
  <?php endif;?>

  <?php // Flash Games
  if ($GameProviderCode == "IGT" && $GameID < 4500):?>
    <?php if($funMode == 1):?>
	
    <form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">		
		<input type="hidden" name="currencycode" value="FPY">
		<input type="hidden" name="nscode"       value="DAUB">
		<input type="hidden" name="skincode"     value="D001">
		<input type="hidden" name="softwareid"   value="<?php echo $GameProviderGameLaunchID;?>">
	</form>	  
	<?php else:?>
    <form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
		<input type="hidden" name="uniqueid"     value="<?php echo $PlayerID;?>">
		<input type="hidden" name="currencycode" value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="countrycode"  value="<?php echo $CountryCode2;?>">
		<input type="hidden" name="presenttype"  value="FLSH">
		<input type="hidden" name="softwareid"   value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="language"     value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="nscode"       value="DAUB">
		<input type="hidden" name="skincode"     value="<?php echo $SkinCode;?>">
		<input type="hidden" name="securetoken"  value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 4, '0', STR_PAD_LEFT);?>">
		<input type="hidden" name="dateofbirth"  value="<?php echo $PlayerDOB;?>">
		<input type="hidden" name="gender"  value="<?php echo $PlayerGender;?>">
	</form>	
	<?php endif;?>
  <?php endif;?>

  <?php // HTML 5 games
  if ($GameProviderCode == "IGT" && $GameID >= 4500):?>
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
		<input type="hidden" name="uniqueid"     value="<?php echo $PlayerID;?>">
		<input type="hidden" name="currencycode" value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="countrycode"  value="<?php echo $CountryCode2;?>">
		<input type="hidden" name="presenttype"  value="HTML">
		<input type="hidden" name="softwareid"   value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="language"     value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="nscode"       value="DAUB">
		<input type="hidden" name="skincode"     value="<?php echo $SkinCode;?>">
		<input type="hidden" name="securetoken"  value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 4, '0', STR_PAD_LEFT);?>">
		<input type="hidden" name="minbet"       value="0.01">
		<input type="hidden" name="denomamount"  value="0.01">
		<input type="hidden" name="dateofbirth"  value="<?php echo $PlayerDOB;?>">
		<input type="hidden" name="gender"  value="<?php echo $PlayerGender;?>">
		
		<input type="hidden" name="D999_homeURL"    value="<?php echo $HomeURL;?>">
		<input type="hidden" name="D999_lobbyURL"    value="<?php echo $LobbyURL;?>">
		<input type="hidden" name="D999_cashierURL"    value="<?php echo $CashierURL;?>">
		
	</form>	
  <?php endif;?>



    <?php if ($GameProviderCode == "MGS" && $GameID <= 6499):?>     
		<?php if($funMode == 1):?>
		<form name="theForm" target="_self" method="GET" action="https://quickfire3.gameassists.co.uk/quickfiresslen/">
			<input type="hidden" name="bc"        value="config-quickfiressl--<?php echo $LanguageCode;?>--MIT-Demo">
			<input type="hidden" name="csid"      value="1866">  
			<input type="hidden" name="sext1"     value="demo">
			<input type="hidden" name="sext2"     value="demo">		
			<input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
		</form>	
		<?php else:?>
		<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
			<input type="hidden" name="authToken" value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 5, '0', STR_PAD_LEFT).str_pad($PlayerID, 7, '0', STR_PAD_LEFT);?>">
			<!--<input type="hidden" name="applicationid" value="1023">-->
			<input type="hidden" name="applicationid" value="2023">
			<input type="hidden" name="serverid" value="<?php echo $SiteID;?>">
			<!--<input type="hidden" name="theme" value="quickfiressl">-->
			<input type="hidden" name="theme" value="quickfiressl-rmuk">
			<input type="hidden" name="variant" value="MIT">
			<input type="hidden" name="usertype" value="0">
			<input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
			<input type="hidden" name="csid"      value="<?php echo $SiteID;?>">    
			<input type="hidden" name="ul"        value="<?php echo $LanguageCode;?>">
			<input type="hidden" name="sext1"     value="genauth">
			<input type="hidden" name="sext2"     value="genauth">	
		</form>	
		<?php endif;?>
    <?php endif;?>


	<?php if ($GameProviderCode == "MGS" && $GameID >= 6500):?>
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
		<input type="hidden" name="casinoID"   value="<?php echo $SiteID;?>">
		<input type="hidden" name="lobbyURL"   value="<?php echo $LobbyURL;?>">
		<input type="hidden" name="bankingURL" value="<?php echo $CashierURL;?>">
		<input type="hidden" name="loginType"  value="VanguardSessionToken">
		<input type="hidden" name="authToken"  value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 5, '0', STR_PAD_LEFT).str_pad($PlayerID, 7, '0', STR_PAD_LEFT);?>">
		<input type="hidden" name="isRGI"      value="true">
	</form>	
  <?php endif;?>
  
  <?php if ($GameProviderCode == "ANW" && $DeviceID == 1):?> 
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
		<input type="hidden" name="authToken"  value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 5, '0', STR_PAD_LEFT).str_pad($PlayerID, 7, '0', STR_PAD_LEFT);?>">
		<input type="hidden" name="serverid" value="2186">
		<input type="hidden" name="applicationID" value="7217">
		<input type="hidden" name="ModuleID" 	value="<?php echo $ModuleID;?>">
		<input type="hidden" name="ClientID" 	value="<?php echo $ClientID;?>">
		<input type="hidden" name="ProductID" 	value="<?php echo $ProductID;?>">
		<input type="hidden" name="ul"        value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="gameID"    value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="siteID"   value="MIT">
		<?php if ($funMode == 1){?>
		<input type="hidden" name="playmode"	value="demo">		
		<?php }?>
		
		<input type="hidden" name="lobbyURL"   value="<?php echo $LobbyURL;?>">
		<input type="hidden" name="bankingURL" value="<?php echo $CashierURL;?>">
	</form>	
  <?php endif;?>

  <?php 
  
	// MOBILE ANW games
	if ($GameProviderCode == "ANW" && $DeviceID > 1) {
		
		$redirect = "/anw-frame/?" 
			. "GameURL=" . $GameURL 
			. "&authToken=" . $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).str_pad($GameID, 5, '0', STR_PAD_LEFT).str_pad($PlayerID, 7, '0', STR_PAD_LEFT)
			. "&serverid=2186"
			. "&applicationID=7217"
			. "&ModuleID=" . $ModuleID
			. "&ClientID=" . $ClientID
			. "&ProductID=" . $ProductID
			. "&ul=" . $LanguageCode
			. "&providerGameID=" . $GameProviderGameLaunchID
			. "&gameID=" . $GameID
			. "&siteID=MIT"
			. "&lobbyURL=" . $LobbyURL
			. "&bankingURL=" . $CashierURL;
			
		if ($funMode == 1) {
			$redirect .=  "&playmode=demo";
		}
		
		header('Location: '.$redirect);
	}
	
  ?>  
  
  
  <?php if ($GameProviderCode == "Baddamedia"):?>
    <form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>">
		<input type="hidden" name="PlayerID"           value="<?php echo $PlayerID;?>">
		<input type="hidden" name="SessionID"          value="<?php echo $SessionID;?>">
		<input type="hidden" name="CurrencyCode"       value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="CountryCode"        value="<?php echo $CountryCode;?>">
		<input type="hidden" name="LanguageCode"       value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="GameLaunchSourceID" value="<?php echo $GameLaunchSourceID;?>">
		<input type="hidden" name="Game"               value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="Launcher"           value="DGC">
	</form>	
  <?php endif;?>

  
  
	<?php if ($GameProviderCode == "Bally"):?>
		<?php if($funMode == 1):?>
		<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
			<input type="hidden" name="softwareId"         value="<?php echo $GameProviderGameLaunchID;?>">
			<input type="hidden" name="language"           value="<?php echo $LanguageCode;?>">
			<input type="hidden" name="operatorid"         value="DAUB">
			<input type="hidden" name="skinid"             value="<?php echo $SiteID;?>">			
			<input type="hidden" name="RealPlay"           value="0">			
		</form>  
  
		<?php else:?>
		<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>">
			<input type="hidden" name="playerid"           value="<?php echo $PlayerID;?>">
			<input type="hidden" name="currencycode"       value="<?php echo $CurrencyCode;?>">
			<input type="hidden" name="countrycode"        value="<?php echo $CountryCode2;?>">
			<input type="hidden" name="softwareId"         value="<?php echo $GameProviderGameLaunchID;?>">
			<input type="hidden" name="language"           value="<?php echo $LanguageCode;?>">
			<input type="hidden" name="operatorid"         value="DAUB">
			<input type="hidden" name="skinid"             value="<?php echo $SiteID;?>">
			<input type="hidden" name="affiliateid"        value="1">
			<input type="hidden" name="authtype"           value="Token">
			<input type="hidden" name="authtoken"          value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT).$PlayerID;?>">
			<input type="hidden" name="DemoMode"           value="False">
			<input type="hidden" name="RealPlay"           value="1">
			<input type="hidden" name="homeURL"            value="<?php echo $LobbyURL;?>">
		</form>
		<?php endif;?>
    <?php endif;?>

  

  
  
  <?php if ($GameProviderCode == "NPB"):?>
    <form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>">
		<input type="hidden" name="uid"       value="<?php echo $PlayerID;?>">
		<input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="guid"      value="<?php echo $SessionID;?>">
		<input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="url"       value="<?php echo $GameURL;?>">
		<input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="operator"  value="1">
		<input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
	</form>	
  <?php endif;?>

  <?php if ($GameProviderCode == "BetFoundry"):?>
    <form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>"> 
		<input type="hidden" name="uid"       value="<?php echo $PlayerID;?>">
		<input type="hidden" name="gameid"    value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="guid"      value="<?php echo $SessionID;?>">
		<input type="hidden" name="cur"       value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="url"       value="<?php echo $GameURL;?>">
		<input type="hidden" name="lang"      value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="operator"  value="1">
		<input type="hidden" name="SkinID"    value="<?php echo $SkinID;?>">
		<input type="hidden" name="HomeURL"    value="<?php echo $HomeURL;?>">
		<input type="hidden" name="LobbyURL"    value="<?php echo $LobbyURL;?>">
		<input type="hidden" name="CashierURL"    value="<?php echo $CashierURL;?>">
		<input type="hidden" name="PromoURL"    value="<?php echo $PromoURL;?>">
		<input type="hidden" name="BackgroundURL"    value="<?php echo $BackgroundURL;?>">
	</form>	
    <?php endif;?>

	<?php if ($GameProviderCode == "Realistic"):?>
	<form name="theForm" target="_self" method="POST" action="<?php echo $GameURL;?>"> 
		<input type="hidden" name="freePlay"         value="<?php echo ($funMode == 0 ? 'false' : 'true');?>">
		<input type="hidden" name="externalGameId"  value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="languageCode"     value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="sessionToken"     value="<?php echo $SessionID;?>"> <!-- If freePlay true, sessionID should be false -->
		<input type="hidden" name="currencyCode"     value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="userId"          value="<?php echo $PlayerID;?>">
		<input type="hidden" name="operatorId" 		value="<?php echo $SkinID;?>">	
	</form>	
	<?php endif;?>


  
  

  
  	<?php if ($GameProviderCode == "Eyecon" && $GameID >= 14500):?>
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>"> 
		<input type="hidden" name="uid"         	 value="<?php echo $PlayerID;?>">
		<input type="hidden" name="alias"         	 value="<?php echo $PlayerID;?>">
		<input type="hidden" name="gameid"  		 value="<?php echo $GameProviderGameLaunchID;?>">
		<input type="hidden" name="guid"		     value="<?php echo $SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT);?>">
		<input type="hidden" name="brand"        	 value="<?php echo $SiteID;?>"> 
		<input type="hidden" name="nid"     	 	 value="<?php echo $SkinID;?>">
		<input type="hidden" name="lang"         	 value="<?php echo $LanguageCode;?>">
		<input type="hidden" name="cur"         	 value="<?php echo $CurrencyCode;?>">
	</form>	
  <?php endif;?>  
  
  <?php   
  if (($GameProviderCode == "Geco" || $MasterProviderID == 18) && ($GameID - $ProviderID*1000) <= 499):?>  	
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>"> 
		<input type="hidden" name="launcher" 		value="spacebar-flash">
	    <input type="hidden" name="GameID"  		value="<?php echo $GameProviderGameLaunchID;?>">
	    <input type="hidden" name="LanguageCode"    value="<?php echo $LanguageCode;?>">	    
	    <?php if ($funMode == 0){?>
		<input type="hidden" name="SessionID"    	value="<?php echo $SessionID.str_pad($ProviderID, 3, '0', STR_PAD_LEFT);?>">
		<input type="hidden" name="CurrencyCode"	value="<?php echo $CurrencyCode;?>">
		<?php }?>
	    <input type="hidden" name="PlayerID"        value="<?php echo $PlayerID;?>">	
		<input type="hidden" name="CountryCode"		value="<?php echo $CountryCode;?>">	
		<input type="hidden" name="PlayMode"      	value="<?php echo ($funMode == 0 ? 'real' : 'free');?>"> <!-- demo or real as values -->
		<input type="hidden" name="SkinID"    		value="<?php echo $SkinID;?>">
	</form>
  <?php endif;?>
  
  <?php   
  if (($GameProviderCode == "Geco" || $MasterProviderID == 18) && ($GameID - $ProviderID*1000) >= 500) :?>  	
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>"> 
		<input type="hidden" name="launcher" 		value="spacebar">
	    <input type="hidden" name="GameID"  		value="<?php echo $GameProviderGameLaunchID;?>">
	    <input type="hidden" name="LanguageCode"    value="<?php echo $LanguageCode;?>">	    
	    <?php if ($funMode == 0){?>
		<input type="hidden" name="SessionID"    	value="<?php echo $SessionID.str_pad($ProviderID, 3, '0', STR_PAD_LEFT);?>">
		<input type="hidden" name="CurrencyCode"	value="<?php echo $CurrencyCode;?>">
		<?php } ?>
	    <input type="hidden" name="PlayerID"        value="<?php echo $PlayerID;?>">	
		<input type="hidden" name="CountryCode"		value="<?php echo $CountryCode;?>">	
		<input type="hidden" name="PlayMode"      	value="<?php echo ($funMode == 0 ? 'real' : 'free');?>"> <!-- demo or real as values -->
		<input type="hidden" name="SkinID"    		value="<?php echo $SkinID;?>">
	</form>
  <?php endif;?>

  <?php   
  if ($GameProviderCode == "SG" && $GameID <= 19500):?>    	
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>"> 
		<input type="hidden" name="context" 			value="default">
		<input type="hidden" name="realMoney"      	value="<?php echo ($funMode == 0 ? 'true' : 'false');?>"> <!-- demo or real as values -->
		<input type="hidden" name="gameCode"  			value="<?php echo $GameProviderGameLaunchID;?>">    
		<input type="hidden" name="ticket"    	value="<?php echo $SessionID;?>"> <!-- If freePlay true, sessionID should be false -->
		<input type="hidden" name="accountId"         value="<?php echo $PlayerID;?>">	
		<input type="hidden" name="partnerCode"         value="daub_bingo">		
		<input type="hidden" name="currency"	value="<?php echo $CurrencyCode;?>">
		<input type="hidden" name="languageCode"    value="<?php echo $LanguageCode;?>">
		<?php if ($DeviceID > 1) { ?>
			<input type="hidden" name="realityCheck" value="<?php echo $Interval;?>">
			<input type="hidden" name="realityLink" value="<?php echo $HistoryURL;?>">
			<input type="hidden" name="realityWaitForGameEnd" value="true">
			<input type="hidden" name="realityButtons" value="QUITCONT">
		<?php } ?>
	</form>
  <?php endif;?>  

  <?php   
  if ($GameProviderCode == "SG" && $GameID >= 19500):?>    	
	<form name="theForm" target="_self" method="GET" action="<?php echo $GameURL;?>"> 
	<!--
		<input type="hidden" name="context" 			value="mobile">
	-->
	    <input type="hidden" name="game"  				value="<?php echo $GameProviderGameLaunchID;?>">    
	    <input type="hidden" name="partnercode"          value="daub_bingo">
		<input type="hidden" name="lobbyurl"    			value="<?php echo $LobbyURL;?>">
		<input type="hidden" name="partnerticket"    	value="<?php echo $SessionID;?>">
		<input type="hidden" name="partneraccountid"    value="<?php echo $PlayerID;?>">	
		<input type="hidden" name="realmoney" 			value="<?php echo ($funMode == 0 ? 'true' : 'false');?>">
		<?php if ($DeviceID > 1) { ?>
			<input type="hidden" name="realityCheck" value="<?php echo $Interval;?>">
			<input type="hidden" name="realityLink" value="<?php echo $HistoryURL;?>">
			<input type="hidden" name="realityWaitForGameEnd" value="true">
			<input type="hidden" name="realityButtons" value="QUITCONT">
		<?php } ?>
	</form>
  <?php endif;?>
  
   
<?php endif;?>

<!--RS 18-09-2013. Added to avoid opening home page in new tab from ios devices in standalone mode-->
<script type="text/javascript">
  function fixLinksAfterAddToHome() {
    var a = document.getElementById("erra");
    a.onclick=function()
    {
      window.location=this.getAttribute("href");
      return false;
    }
  }
  var isIOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/gi);
  if(isIOS){
      if (window.navigator.standalone == true){
        // solve links opening in new tab issue for web app on iOS after add to home
        fixLinksAfterAddToHome();
      }
    }

function goHome(url)
{
	var isMobile = <?php echo getDevice() ?>;

	// if mobile redirect to home
	if (isMobile > 1) {
	window.location = url;
	} else {
    
		// if game was opened in a new window
		if (window.self === window.top) {
			// close window
			window.close();
		} else {
			// close iframe
			top.postMessage('close-game', '*');
		}  

	}

}

<?php if ($GameProviderCode == "NetEnt" && $GameID >= 17450 && $GameID <= 17499)://Refers to Live Casino games only?>
loadNetEntGame();
<?php endif;?>	

</script>
</body>
</html>