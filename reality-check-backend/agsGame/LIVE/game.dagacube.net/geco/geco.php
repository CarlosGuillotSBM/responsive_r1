<?php
/*
This file is called from Geco after a player has launched a realistic game in a new browser window.
*/

header('Content-Type: application/x-www-form-urlencoded');

require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "Geco";
$GameProviderID   = 18;//Geco=18
$accessid         = "gHcBmuZ2";//Geco accessid for them to access our server




function ErrorResponse ($errCode, $data) {
	global $GameProviderCode, $PlayerID;
	$text = 'Bad Request';
	$httpStatus = 400;
	$statusDescription = 'error%20processing%20request';
	switch ($errCode) {
		case E_INVALID_AUTH:		
			$text = 'Unauthorized';
			$httpStatus = 401;
		  $GecoErrCode = 1;						
			$statusDescription = 'invalid%20proxy-authorization';
		  break;
		case E_INVALID_PLAYER_SESSION:
			$text = 'Unauthorized';
			$httpStatus = 401;
		  $GecoErrCode = 2;
			$statusDescription = 'invalid%20session';
		  break;
		case E_INVALID_TXID:			
		  $GecoErrCode = 3;
			$statusDescription = 'invalid%20txn';
		  break;
		case E_INSUFFICIENT_FUNDS:			
		  $GecoErrCode = 4;
			$statusDescription = 'insufficient%20funds';
		  break;
		case E_DUPLICATE_REF:
		  $GecoErrCode = 5;
			$statusDescription = 'duplicate%20reference';
		  break;
		case E_GAME_DISABLED:
			$statusDescription = 'game%20disabled';
		  $GecoErrCode = 6;
		  break;
		case E_SELF_EXCLUSION:
			$text = 'Unauthorized';
			$statusDescription = 'self%20exclusion';
			$httpStatus = 401;
		  $GecoErrCode = 7;
		  break;
		case E_VOID_FAIL_NO_WAGERS:
			$statusDescription = 'no%20wagers';
			$GecoErrCode = 8;
		  break;
		default:			
		  $GecoErrCode = 999;
		  break;
	}
	header('HTTP/1.0 '. $httpStatus . ' ' . $text,1,$httpStatus);
	
  $Response = "error=$GecoErrCode&status=$statusDescription";
  $data = "SEND: ".$Response.(($data == "") ? "" : "   ").$data;
  WriteLog($GameProviderCode, $errCode, $data, $PlayerID);
  
  
  

  echo $Response;	
  exit;	
}
$rawRequest = "";
$requestBody = @file_get_contents('php://input');


WriteLog($GameProviderCode, 0, 'RAW'. print_r($_REQUEST,true) , $_REQUEST["playerid"]);

foreach ($_REQUEST as $var => $value) {
  $_REQUEST[$var] = trim($_REQUEST[$var]);
  $rawRequest.="$var=$value ";
}

$requestBodyExploded = explode("&", $requestBody);

if (sizeof($requestBodyExploded) > 1) {
	foreach ($requestBodyExploded as $var => $value) {
		
		$varValuesArray = explode("=", $value);
	
		$requestBodyArray[$varValuesArray[0]] = trim($varValuesArray[1]);
		$rawRequest.="$varValuesArray[0]=$varValuesArray[1] ";
	}
}

$Response = "";

$PlayerID = isset($_REQUEST["playerid"]) ? $_REQUEST["playerid"] : 0;
WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, $PlayerID);






$errCode = 0;

if (!(@$requestBodyArray["proxy-authorization"] == $accessid)) {  
	ErrorResponse(E_INVALID_AUTH, "");
}

$gameid    = isset($_REQUEST["gameid"]) ? $_REQUEST["gameid"] : 0; //the gameid as used from Geco games
$SessionID = isset($requestBodyArray["authorization"])   ? substr($requestBodyArray["authorization"],0,36) : "";
$GameProviderID = isset($requestBodyArray["authorization"])   ? substr($requestBodyArray["authorization"],36,3) : 18;
$Wager     = isset($requestBodyArray["wager"])  ? $requestBodyArray["wager"]  : -1;
$Win       = isset($requestBodyArray["win"])    ? $requestBodyArray["win"]    : -1;
$ref       = isset($_REQUEST["gameroundref"])  	? $_REQUEST["gameroundref"]  : 0;//all their refs should be globally unique
$subref    = isset($_REQUEST["gameref"])  ? $_REQUEST["gameref"]  : 0;
//$subref    = -1;//all geco subrefs will be 1 as they dont have a way of recordiing a game round - they only record an entire session - so each ref is unique
$type      = isset($requestBodyArray["method"])   ? $requestBodyArray["method"]   : "BC";
$refund		= isset($requestBodyArray["voidWager"])  ? $requestBodyArray["voidWager"]  : 0;
$refundref	= isset($_REQUEST["refRef"])   ? $_REQUEST["refRef"]   : "";
$refundnotes= isset($_REQUEST["refReason"])   ? $_REQUEST["refReason"]   : "";

$GameLaunchSourceID = intval(substr($SessionID,39,3));

//Hack for reconciliation purposes. Don't process the request 25% of the times and delay response 5 seconds 20% of the times
// $randNumber = rand ( 1, 100 );
// if($randNumber >= 1 && $randNumber <= 25)
// {
	// sleep ( 5 );
	// ErrorResponse(0, "");
// }
// else if ($randNumber >= 26 && $randNumber <= 50)
	// sleep ( 5 );

if ($Wager < 0 && ($type != "Void" && $type != "BC" ))       ErrorResponse(E_INVALID_WAGER, "");
if ($Win < 0 && ($type != "Void" && $type != "BC"))         ErrorResponse(E_INVALID_WIN, "");
if ($PlayerID == 0)   ErrorResponse(E_INVALID_PLAYER_SESSION, "$PlayerID");
if ($SessionID == "") ErrorResponse(E_INVALID_PLAYER_SESSION, "$SessionID");
if ($ref <= 0 && $type != "BC") ErrorResponse(E_INVALID_TXID, "");
if ($subref <= 0 && $type != "BC")     ErrorResponse(E_INVALID_TXID, "");
if ($refund < 0 && $type == "Void")       ErrorResponse(0, "");
if ($type == "")     ErrorResponse(E_INVALID_PARAMS, "");

$RequestMethod = "TxEndGameRound";

if ($_SERVER['REQUEST_METHOD'] === 'GET' && $type != "BC") {
	ErrorResponse(0, "");
}


//if ($type== 'ENDROUND' || ($type == "Tx" && $Wager == 0 && $Win == 0)) {
if ($type== 'ENDROUND' ) {
  $RequestMethod = "TxEndGameRound";
}
else if ($type == "Tx") {
  $RequestMethod = "Tx";
}
else if ($type == "BC") {
  $Wager = 0;
	$Win = 0;
  $RequestMethod = "GetBalance";
}
else if ($type == "Void") {
  $RequestMethod = "Void";
}

$dbErr = "";
$dbSproc = "xTxGame";
$db = "";

$dbParams = array(
  "PlayerID"           => array(intval($PlayerID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "SessionID"          => array($SessionID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(36)),
  "IP"                 => array($_SERVER["REMOTE_ADDR"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
  "GameProviderID"     => array(intval($GameProviderID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "GameProviderGameID" => array(strval($gameid), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(100)),
  "GameLaunchSourceID" => array($GameLaunchSourceID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "Action"             => array($RequestMethod, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
  "Wager"              => array(floatval($Wager), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
  "Win"                => array(floatval($Win), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
  "Ref"                => array($ref, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_BIGINT),
  "SubRef"             => array($subref, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_BIGINT),
  "Refund"             => array(floatval($refund), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_FLOAT),
  "RefundRef"          => array(strval($refundref), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(50)),
  "RefundNotes"        => array($refundnotes, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(4000))
);

/*
db calls xTxGame
Returns:
  Result = 0 : Success
        <> 0 : Various fail codes
*/
require_once("D:/WEB/inc/db56.php");

if(dbInit("", "Main", __FILE__."[".__LINE__."]", 1) == false) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams);
  if ($rs === false) {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  } else {
    $row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }
}

if ($dbErr != "" || !isSet($row->Result) ) {
  ErrorResponse($errCode, $dbErr, $Response);
}

dbFree($rs);
dbClose($db);

//db access was successful, other error
if ($row->Result != 0) {
  if ($row->Result == 104 || $row->Result == 105)
    $row->Result = 104;

  ErrorResponse($row->Result, "");
}

if ($type == "BC")
	$Response = "playerid=$PlayerID&balance=".$row->Balance;
else
	$Response = "playerid=$PlayerID&balance=".$row->Balance."&txid=".$row->agsRef;
WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);
echo $Response;
exit;
?>