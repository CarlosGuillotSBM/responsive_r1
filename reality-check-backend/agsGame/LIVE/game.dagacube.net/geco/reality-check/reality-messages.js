var sbm = sbm || {};

sbm.ExternalCommunicator = { 
	source: window.parent,//Set at init message 
	targetOrigin: "*",//Set at init message 
	init: function () { 
		window.addEventListener("message", 
			this.processReceivedMessage.bind(this), false); 
		}, 
	//Post message to parent host
	request: function (req) { 
		this.source.postMessage(req, this.targetOrigin); 
	},
	//Events coming from parent host (PNGHostInterface component) 
	processReceivedMessage: function (e) { 
		console.log("RC Component received: ", e, e.data.type);
	}
};

sbm.RCComponent = {
	init: function () {}, 
	show: function () {}, 
	//Listener for button. 
	//Close message and send enable request to game. 
	onContinuePlaying: function () { 
		sbm.ExternalCommunicator.request({req: "GAME_ENABLED"}); 
		sbm.utils.output("Sent GAME_ENABLED");
	}, 
	//Listener for button. 
	//Close message and send enable game end request to game. 
	onStopPlaying: function (url) { 
		sbm.ExternalCommunicator.request({req: "GAME_END", data: {redirectUrl: url}});
		sbm.utils.output("Sent GAME_END");
	}, 
	onRealityCheck: function (action) { 
		switch (action) { 
		//Reality check is needed, 
		//Send request that the game should disable user interaction 
		case "activate": 
			sbm.ExternalCommunicator.request({req: "GAME_DISABLED"}); 
			sbm.utils.output("Sent GAME_DISABLED");
			break; 
		}
	} 
};
		
		
		
		
		
		
		
		
		
		
		
		
		
		