<?php

/*
This file is called to interact with the reality check functions
*/
/*
global $glo;
$file = substr(__FILE__, 7);
$glo = array("Application" => "agsGame");
$module = substr($file, strrpos($file, "\\") + 1); //this php file
require_once("D:/WEB/inc/vars.php");
require_once("D:/WEB/inc/genFuncs.php"); //general functions
require_once("D:/WEB/inc/fnAGS.php");
require_once("D:/WEB/inc/db.php");
*/
require_once("D:/WEB/inc/agsGameInc.php"); 
require_once("D:/WEB/inc/db.php");

$function = isset($_GET["Function"]) ? $_GET["Function"]:"" ;
$PlayerID = $_GET["PlayerID"];
$SessionID = $_GET["SessionID"];
$GameID = isset($_GET["GameID"]) ? $_GET["GameID"]:0;

switch ($function) {

	case "no-reminders":

		$dbSproc = "WebRealityCheckSetPeriod";

		$dbParams = array(
			"SessionID"          => array($SessionID, "str", 36),
			"PlayerID"           => array($PlayerID, "int", 0)
		);

		$dbErr = "";
		$db = "";

		if(!dbInit("","agsMain",1)) {
		  $errCode = E_DB_CONNECT;
		  WriteLog("db", $errCode, $dbErr);
		} else {

		WriteLog("db", 'SQL', SdbParams($dbParams), $PlayerID);

		  $rs = dbExecSproc($dbSproc,$dbParams,1);
		  if ($rs === false) {
			$errCode = E_DB_SELECT;
			WriteLog("db", $errCode, $dbErr);
		  } else {
			$row = sqlsrv_fetch_object($rs);
			if (!isset($row) || $row==null) {
			  $errCode = E_DB_RETURN;
			  WriteLog("db", $errCode, $dbErr);
			}
		  }
		}

		dbFree($rs);
		dbClose($db);
	
		if ($dbErr != "") {
		  WriteLog("Application", $dbErr, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
		  exit;
		}
		
		$response['Code'] = 0;
		
		# If a "callback" GET parameter was passed, use its value, otherwise false. Note
		# that "callback" is a defacto standard for the JSONP function name, but it may
		# be called something else, or not implemented at all. For example, Flickr uses
		# "jsonpcallback" for their API.
		$jsonp_callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
		
		if ($jsonp_callback)
		{
			echo $jsonp_callback . '(' . json_encode($response) . ')';
		}
		else
		{
			echo json_encode($response);
		}		
		break;
		
	case "get-settings":

		$dbSproc = "WebRealityCheckGetSettings";

		$dbParams = array(
			"SessionID"          => array($SessionID, "str", 36),
			"PlayerID"           => array($PlayerID, "int", 0)
		);

		$dbErr = "";
		$db = "";

		if(!dbInit("","agsMain",1)) {
		  $errCode = E_DB_CONNECT;
		  WriteLog("db", $errCode, $dbErr);
		} else {

			WriteLog("db", 'SQL', SdbParams($dbParams), $PlayerID);

			$rs = dbExecSproc($dbSproc,$dbParams,1);
			if ($rs === false) {
				$errCode = E_DB_SELECT;
				WriteLog("db", $errCode, $dbErr);
			} else {
				$row = sqlsrv_fetch_object($rs);
				if (!isset($row) || $row==null) {
				  $errCode = E_DB_RETURN;
				  WriteLog("db", $errCode, $dbErr);
				}
			}
		}

		dbFree($rs);
		dbClose($db);
	
		if ($dbErr != "") {
		  WriteLog("Application", $dbErr, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
		  exit;
		}
		
		$response = $row;	
		
		# If a "callback" GET parameter was passed, use its value, otherwise false. Note
		# that "callback" is a defacto standard for the JSONP function name, but it may
		# be called something else, or not implemented at all. For example, Flickr uses
		# "jsonpcallback" for their API.
		$jsonp_callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
		
		if ($jsonp_callback)
		{
			echo $jsonp_callback . '(' . json_encode($response) . ')';
		}
		else
		{
			echo json_encode($response);
		}		
		break;	

	case "realityStop":

		$dbSproc = "WebRealityCheckStopGame";

		$dbParams = array(
			"GameID"			 => array($GameID, "int", 0),
			"PlayerID"           => array($PlayerID, "int", 0),
			"SessionID"          => array($SessionID, "str", 36)
		);

		$dbErr = "";
		$db = "";

		if(!dbInit("","agsMain",1)) {
		  $errCode = E_DB_CONNECT;
		  WriteLog("db", $errCode, $dbErr);
		} else {

			WriteLog("db", 'SQL', SdbParams($dbParams), $PlayerID);

			$rs = dbExecSproc($dbSproc,$dbParams,1);
			if ($rs === false) {
				$errCode = E_DB_SELECT;
				WriteLog("db", $errCode, $dbErr);
			} else {
				$row = sqlsrv_fetch_object($rs);
				if (!isset($row) || $row==null) {
				  $errCode = E_DB_RETURN;
				  WriteLog("db", $errCode, $dbErr);
				}
			}
		}

		dbFree($rs);
		dbClose($db);
	
		if ($dbErr != "") {
		  WriteLog("Application", $dbErr, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
		  exit;
		}
		
		$response = $row;	
		
		# If a "callback" GET parameter was passed, use its value, otherwise false. Note
		# that "callback" is a defacto standard for the JSONP function name, but it may
		# be called something else, or not implemented at all. For example, Flickr uses
		# "jsonpcallback" for their API.
		$jsonp_callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
		
		if ($jsonp_callback)
		{
			echo $jsonp_callback . '(' . json_encode($response) . ')';
		}
		else
		{
			echo json_encode($response);
		}		
		break;		


	case "realityStart":

		$dbSproc = "WebRealityCheckStartGame";

		$dbParams = array(
			"GameID"			 => array($GameID, "int", 0),
			"PlayerID"           => array($PlayerID, "int", 0),
			"SessionID"          => array($SessionID, "str", 36)
		);

		$dbErr = "";
		$db = "";

		if(!dbInit("","agsMain",1)) {
		  $errCode = E_DB_CONNECT;
		  WriteLog("db", $errCode, $dbErr);
		} else {

			WriteLog("db", 'SQL', SdbParams($dbParams), $PlayerID);

			$rs = dbExecSproc($dbSproc,$dbParams,1);
			if ($rs === false) {
				$errCode = E_DB_SELECT;
				WriteLog("db", $errCode, $dbErr);
			} else {
				$row = sqlsrv_fetch_object($rs);
				if (!isset($row) || $row==null) {
				  $errCode = E_DB_RETURN;
				  WriteLog("db", $errCode, $dbErr);
				}
			}
		}

		dbFree($rs);
		dbClose($db);
	
		if ($dbErr != "") {
		  WriteLog("Application", $dbErr, "Player IP=".$_SERVER["REMOTE_ADDR"], $PlayerID);
		  exit;
		}
		
		$response = $row;	
		
		# If a "callback" GET parameter was passed, use its value, otherwise false. Note
		# that "callback" is a defacto standard for the JSONP function name, but it may
		# be called something else, or not implemented at all. For example, Flickr uses
		# "jsonpcallback" for their API.
		$jsonp_callback = isset($_GET['callback']) ? preg_replace('/[^a-z0-9$_]/si', '', $_GET['callback']) : false;
		
		if ($jsonp_callback)
		{
			echo $jsonp_callback . '(' . json_encode($response) . ')';
		}
		else
		{
			echo json_encode($response);
		}		
		break;		
		
	default: 
	
		$response = array(
			"Code" => -3,
			"Msg" => "Function not recognized" 
		);
		
		echo json_encode($response);
		break;	
	
	}


?>


