var sbm = sbm || {};

sbm.ExternalCommunicator = { 
	source: window.parent,
	targetOrigin: "*",
	gameActive: false, // flag will tell if the game is active or idle
	stopGame: false, // flag will tell if it should stop the game
	init: function () { 
		window.addEventListener("message", 
			this.processReceivedMessage.bind(this), false); 
		}, 
	//Post message to parent host
	request: function (req) {
		if (req === "GAME_DISABLED" && gameActive) {
			// can't stop game - will stop when it becomes idle
			this.stopGame = true;
		} else {
			this.source.postMessage(req, this.targetOrigin); 
		}
	},
	//Events coming from parent host (PNGHostInterface component) 
	processReceivedMessage: function (e) { 
		sbm.utils.output("RC Component received: " + e.data); 
		switch (e.data) { 
			case "GAME_READY":
				sbm.utils.output("Game initialized");
				break;
			case "GAME_ACTIVE":
				this.gameActive = true;
				break;
			case "GAME_IDLE":
				if (this.stopGame) {
					sbm.RCComponent.show();
					this.request("REALITY_CHECK_SHOWN");
				}
				break;
		} 
	}
};

sbm.RCComponent = {
	$messagebox: $("#ui-reality-check-container"), 
	init: function () {
		sbm.ExternalCommunicator.request("REALITY_CHECK_READY");
	}, 
	show: function () { 
		this.$messagebox.show(); 
	}, 
	//Listener for button. 
	//Close message and send enable request to game. 
	onContinuePlaying: function () { 
		sbm.ExternalCommunicator.request("REALITY_CHECK_ACKNOWLEDGED"); 
		sbm.utils.output("Requested to resume game");
	}, 
	//Listener for button. 
	//Close message and send enable game end request to game. 
	onStopPlaying: function (url) { 
		top.location.href = url; 
	}, 
	onRealityCheck: function (action) { 
		switch (action) { 
		//Reality check is needed, 
		//Send request that the game should disable user interaction 
		case "activate": 
			sbm.ExternalCommunicator.request("REALITY_CHECK_SHOWN"); 
			sbm.utils.output("Requested to stop game");
			break; 
		}
	} 
};
		
		
		
		
		
		
		
		
		
		
		
		
		
		