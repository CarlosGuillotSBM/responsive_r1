var sbm = sbm || {};

/**************************************************************************************************
											UTILS
***************************************************************************************************/
sbm.utils = {
	
	getUrlParameterByName: function (name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search.replace(/&amp;/g, '&'));
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	},
	
	minutesToHours: function (time) {

		if (time < 1) {
			return "few seconds";
		} else if (time >= 1 && time < 60) {
			return time + " minutes";
		} else {

			var hours = Math.floor(time / 60);
			var minutes = (time % 60);
			
			if (minutes === 0) {
				if (hours === 1) return hours + " hour";
				return hours + " hours";
			}
			return hours + "h" + minutes + "m";
		}
	},
	
	output: function (msg) {
		console.log("%c SBM RC: " + msg, 'background: #013766; color: #75FFFF');
	}
};

/**************************************************************************************************
										DATASERVICE
***************************************************************************************************/
sbm.dataservice = {

	ajax: function (data, callback) {
		$.ajax(
			{
				"url": "/realityCheck.php",
				"dataType": "json",
				"timeout" : 10000,
				"data": data
			}
		)
		.done(function (data) {
			callback && callback(data);
		})
		.fail(function (jqXHR, textStatus) {
			callback && console.log(jqXHR, textStatus);
		});
	}	

};

/**************************************************************************************************
									REALITY CHECK
***************************************************************************************************/

sbm.reality = (function (sbm, $) {

	// player's settings

	var interval = 0,
		timeElapsed = 0,
		homeUrl = "",
		historyUrl = "",
		gameId = sbm.utils.getUrlParameterByName("gameID"),
		sessionId = $.cookie("SessionID"),
		playerId = $.cookie("PlayerID");
	
	// DOM
	
	var $frame = $("#ui-game-frame"),
		$rcWarning = $("#ui-reality-check-container");
	
	// Start reality check timer
	var startTimer = function (interval) {

		setTimeout(function() {

			var stopData = {
				"Function": "realityStop",
				"GameID": gameId,
				"SessionID": sessionId,
				"PlayerID": playerId
			};
		
			sbm.dataservice.ajax(stopData);
			
			// set display
			
			timeElapsed += interval;
			var elapsedFormated = sbm.utils.minutesToHours(timeElapsed);
			var intervalFormated = sbm.utils.minutesToHours(interval);
			
			$(".ui-interval").text(intervalFormated);
			$(".ui-elapsed").text(elapsedFormated);
			
			// stop game
			$rcWarning.show();
		
		}, interval * 60000);

	};
	
	// initialization
	var init = function () {
	
		/*
			Set the iframe src of the game
		*/
		
		var gameURL = sbm.utils.getUrlParameterByName("GameURL") + "?" +
			"authToken=" + sbm.utils.getUrlParameterByName("authToken") +
			"&serverid=" + sbm.utils.getUrlParameterByName("serverid") +
			"&applicationID=" + sbm.utils.getUrlParameterByName("applicationID") +
			"&ModuleID=" + sbm.utils.getUrlParameterByName("ModuleID") +
			"&ClientID=" + sbm.utils.getUrlParameterByName("ClientID") +
			"&ProductID=" + sbm.utils.getUrlParameterByName("ProductID") +
			"&ul=" + sbm.utils.getUrlParameterByName("ul") +
			"&gameID=" + sbm.utils.getUrlParameterByName("providerGameID") +
			"&siteID=" + sbm.utils.getUrlParameterByName("siteID") +
			"&lobbyURL=" + sbm.utils.getUrlParameterByName("lobbyURL") +
			"&bankingURL=" + sbm.utils.getUrlParameterByName("bankingURL");
		
		$frame.attr("src", gameURL);
	
		/*
		 	Attach DOM events
		*/

		$(".continue-cta").on("click", function () {
			// resume game
			$rcWarning.hide();
			
			var startData = {
				"Function": "realityStart",
				"GameID": gameId,
				"SessionID": sessionId,
				"PlayerID": playerId
			};
		
			sbm.dataservice.ajax(startData);
			
			// start timer
			startTimer(interval);
		});
		
		$(".stop-cta").on("click", function () {
			
			// resume game
			$rcWarning.hide();
			
			var startData = {
				"Function": "realityStart",
				"GameID": gameId,
				"SessionID": sessionId,
				"PlayerID": playerId
			};
		
			sbm.dataservice.ajax(startData);
			
			// stop reminders
			var data = {
				"Function": "no-reminders",
				"SessionID": sessionId,
				"PlayerID": playerId
			};
			sbm.dataservice.ajax(data);
		});
		
		$(".home-cta").on("click", function () {
			// end game and go home
			window.location = homeUrl;
		});

		$(".activity-cta").on("click", function () {
			// end game and go to history
			window.location =  historyUrl;
		});

		// callback
		var gotSettings = function (data) {
	
			if (data.Code == 0) {
				
				homeUrl = data.HomeURL;
				historyUrl = data.HistoryURL;
				interval = data.Interval;

				sbm.utils.output("interval=" + interval);
				
				data.Interval > 0 && startTimer(interval);
			}
		
		};

		// get player settings
		var data = {
			"Function": "get-settings",
			"SessionID": sessionId,
			"PlayerID": playerId
		}
		sbm.dataservice.ajax(data, gotSettings);
	}

	return {
		init: init	
	}

}(sbm, jQuery));

/**************************************************************************************************
									STARTING POINT
***************************************************************************************************/
(function(sbm) {
	sbm.reality.init();
}(sbm));