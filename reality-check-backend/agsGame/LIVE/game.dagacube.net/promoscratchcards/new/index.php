<?php
  require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
  require_once("D:/WEB/inc/db56.php");
  $GameProvider   = "ScratchCard";

  function ErrorResponse ($errCode, $data) {
    global $GameProvider;
    $Response = "status=invalid&error=$errCode";

    $data = "SEND: ".$Response.(($data == "") ? "" : "   ").$data;
    //WriteLog($GameProvider, $errCode, $data);
    //echo $Response;
    //exit;
  }

  $errCode = "";

  if(count($_GET) == 0)
  {
    echo "error!!";
    exit;
  }
  else
  {

    $gameSessionId = $_REQUEST['sessionId'];
    $playerId = $_REQUEST['playerId'];

    $gameModeCodes = array("REAL"  => 1, "FUN" => 3, "PROMO" => 2);

    $gameMode = $gameModeCodes[(string) $_GET['sessionType']];
    $gameBrandGameId = $_GET['gameId'];
    $gameScratchCardPrizeGUID = $_GET['prizesecurityid'];

  }

  if($_GET['action'] == "init")
  {
    //Check we have all we need, if we don't. Send an error message

    if( !isset($gameSessionId) || $gameSessionId == "" ||
      !isset($playerId) || $playerId == "" ||
      !isset($gameBrandGameId) || $gameBrandGameId == "")
    {
      //Send a blank response so the client can trigger the bad xml message
      $response = "";
      return;
    }

    $dbErr = $SdbParams = "";
    $dbSproc = "WebScratchCardGetGameConfig";
		$dbParams = array(
		  "PlayerID"           => array(intval($playerId), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
		  "SessionID"          => array($gameSessionId, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(36)),
		  "IP"                 => array($_SERVER["REMOTE_ADDR"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
		  "ScratchCardID"      => array(intval($gameBrandGameId), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT)
		  "ScratchCardType"    => array(intval($gameMode), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT)//Promo type for now
		);
    
    $db = "";

    if(dbInit("", "Main", __FILE__."[".__LINE__."]", 1) == false) {
      $errCode = E_DB_CONNECT;
      //WriteLog("db", $errCode, $dbErr);
    } else {
      $rs = dbExecSproc($dbSproc,$dbParams);
      if ($rs === false) {
        $errCode = E_DB_SELECT;
        //WriteLog("db", $errCode, $dbErr);
      }
      else
      {
      $row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
      }
    }

    if ($errCode != "") {
      $response = '<auth status="' .$errCode.'"></auth>';
      ErrorResponse($errCode, $dbErr);
      echo $response;
      return;
    }
    //print_r($row);


    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
      //exit;
    }

    if ($errCode != "") {
      $response = '<auth status="' .$errCode. '"></auth>';
      ErrorResponse($errCode, $dbErr);
      echo $response;
      return;
    }

    //Check for possible errors in the
    if(isset($row->Result))
    {
      switch($row->Result)
      {
        case -10:
        case -11:
          $response = '<auth status="' .$row->Result. '"></auth>';
          break;

      }
      echo $response;
      return;
    }

    //print_r($row);

    $playerCurrencySymbol = $row->symbol;//htmlentities($row->symbol);
    $playerDecSymbol = $row->centsSymbol;
    $response = '<auth status="0">
            <config >
              <stakes>1 2 5 7 10 25 50 100</stakes>
              <autoplay>1 2 5 7 10 25 50 100</autoplay>
            </config>
            <balance balance="'.$row->real.'" />
          </auth>';
    dbFree($rs);
    dbClose($db);

    echo $response;
    return;

  }

  if($_GET['action'] == "getcard")
  {
    $gameBetStake = $_GET['stake'];
    //print_r($game);
    //Check we have all we need, if we don't. Send an error message

    if( !isset($gameSessionId) || $gameSessionId == "" ||
      !isset($playerId) || $playerId == "" ||
      //!isset($gameMode) || $gameMode == "" ||
      !isset($gameBetStake) || $gameBetStake == "" ||
      !isset($gameScratchCardPrizeGUID) || $gameScratchCardPrizeGUID == "" ||
      !isset($gameBrandGameId) || $gameBrandGameId == "")
    {
      //Send a blank response so the client can trigger the bad xml message
      $response = "";
      return;
    }


    $dbErr = $SdbParams = "";
    $dbSproc = "WebScratchCardPlay";
		$dbParams = array(
		  "PlayerID"           => array(intval($playerId), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
		  "SessionID"          => array($gameSessionId, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(36)),
		  "IP"                 => array($_SERVER["REMOTE_ADDR"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
		  "ScratchCardSecurityGUID" => array($gameScratchCardPrizeGUID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(36)),
		  "ScratchCardType"    => array(intval($gameMode), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT)//Promo type for now
		);

    $db = "";

    if(dbInit("", "Main", __FILE__."[".__LINE__."]", 1) == false) {
      $errCode = E_DB_CONNECT;
      //WriteLog("db", $errCode, $dbErr);
    } else {
      $rs = dbExecSproc($dbSproc,$dbParams);
      if ($rs === false) {
        $errCode = E_DB_SELECT;
        //WriteLog("db", $errCode, $dbErr);
      }
      else
      {
      $row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
      }
    }

    //$languageCode = array("en" => "en_GB");
    //Get the player and game general info

    if (!isset($row) || $row==null) {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);

      //exit;
    }

    if ($dbErr != "") {
      $response = '<card><result status="' .$errCode. '"/></card>';
      ErrorResponse($errCode, $dbErr);
      echo $response;
      return;
    }

    //Check for possible errors in the
    if(isset($row->Result))
    {
      switch($row->Result)
      {
        case -1:
        case -2:
        case -10:
        case -11:
        case -100:
        case -101:
        case -102:
          $response = '<card>
                  <result status="' .$row->Result. '" />
                </card>';
          break;
      }
      echo $response;
      return;
    }

    // HANDLE THIS ERROR
    if(!isset($row->numRows))
    {
      $response = '<card><result status="' .$errCode. '"/></card>';
      echo $response;
      return;
    }

    $gameRows = $row->numRows;
    $gameColumns = $row->numColumns;
    $gameAllowedScratches = $row->allowedScratches;

    $balance = $row->real;

    //Now get the payouts and game values
    if(sqlsrv_next_result($rs))
    {
      $totalPayout = 0;
      $payoutSection = "";

      //Payouts

      while($row = sqlsrv_fetch_array($rs, SQLSRV_FETCH_ASSOC))
      {
        $isWin = $row["isWin"]==1?'true':'false';
        if($row["isWin"] == 1)
        {
          $totalPayout += $row["amount"];
        }
        $payoutSection .= '<payout amount="'.$row["amount"].'" iswin="'.$isWin.'" />';
      }

      $response = '<card>
              <result status="0" balance="'.$balance.'" totalWin="'.$totalPayout.'"/>
              <scratchcardstate>
              <payouts>'.$payoutSection.'</payouts>';

      //Game values
      if(sqlsrv_next_result($rs))
      {
        $response .= "<game columnsize=\"".$gameColumns."\" rowsize=\"".$gameRows."\" totalallowedscratches=\"".$gameAllowedScratches."\" winindex=\"0\">
                  <symbols>";
        while($row = sqlsrv_fetch_object($rs))
        {
          $isWin = $row->isWin==1?'true':'false';
          $response .=    "<symbol name=\"".$row->name."\" keyname=\"".$row->keyname."\" value=\"".$row->value."\" iswin=\"".$isWin."\" />";
        }
        $response .= "    </symbols>
                </game>
              </scratchcardstate>
            </card>";
      }

    }

    dbFree($rs);
    dbClose($db);
    echo $response;
  }
?>