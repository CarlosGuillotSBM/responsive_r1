<?php 

	require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
	$GameProvider   = "ScratchCard";

	function ErrorResponse ($errCode, $data) {
		global $GameProvider;
		$Response = "status=invalid&error=$errCode";

		$data = "SEND: ".$Response.(($data == "") ? "" : "   ").$data;
		//WriteLog($GameProvider, $errCode, $data);
		//echo $Response;
		//exit;
	}
		
	$errCode = "";
		
	$xml_request = file_get_contents('php://input');
	
	if(!$xml_request)
	{
		/*$playerSSOToken = 'f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb';
		$playerBrandId = "1";
		$gameBrandGameId = '4CFD5708-A1FE-DE11-8766-001CC4E296F0';
		$gameSessionId = '00';		
		$gameBetStake = 0;*/		
		echo "error!!";
		exit;
	}
	else
	{	
		$game = new SimpleXMLElement($xml_request);		
		$playerCurrencySymbol = $game->header->player["currencysymbol"];
		$playerDecSymbol = $game->header->player["decsymbol"];
		$playerCurrencyCode = $game->header->player["currencycode"];
		$playerLanguageCode = $game->header->player["languagecode"];
		$playerSSOToken = $game->header->player["ssotoken"];
		$playerBrandId = $game->header->player["playerId"];
		
		$gameModeCodes = array(	"REAL" 	=> 1,
								"FUN"	=> 3,
								"PROMO"	=> 2);
		
		$gameMode = $gameModeCodes[(string)$game->header->player["funmode"]];		
		$gameBrandGameId = $game->game["brandgameid"];
		$gameScratchCardPrizeGUID = $game->game["scratchcardprizesecurityid"];
		$gameSessionId = $game->game["sessionid"];		
		
	}

	if($game->game["action"] == "init")
	{
		//Check we have all we need, if we don't. Send an error message
		
		if(	!isset($playerCurrencySymbol) || $playerCurrencySymbol == "" ||
			!isset($playerDecSymbol) || $playerDecSymbol == "" ||
			!isset($playerCurrencyCode) || $playerCurrencyCode == "" ||
			!isset($playerLanguageCode) || $playerLanguageCode == "" ||
			!isset($playerSSOToken) || $playerSSOToken == "" ||
			!isset($playerBrandId) || $playerBrandId == "" ||
			!isset($gameMode) || $gameMode == "" ||
			!isset($gameBrandGameId) || $gameBrandGameId == "")
		{
			//Send a blank response so the client can trigger the bad xml message
			$response = "";
			return;
		}
		
		require_once("D:/WEB/inc/db.php");

		$dbErr = "";
		$dbSproc = "WebScratchCardGetGameConfig";
		$dbParams = array("PlayerID"           	=> array($playerBrandId, "int", 0),
						  "SessionID"          	=> array($playerSSOToken, "str", 36),
						  "IP"                 	=> array($_SERVER["REMOTE_ADDR"], "str", 15),
						  "ScratchCardID"	   	=> array($gameBrandGameId, "str", 36),
						  "ScratchCardType"		=> array($gameMode, "int", 0)//Promo type for now						  
						 );

		$db = "";
		
		//"BINGOX1\GAME"
		if(!dbInit("","agsMain",1)) {		    
			$errCode = E_DB_CONNECT;
			//WriteLog("db", $errCode, $dbErr);		  
		} else {
		  $rs = dbExecSproc($dbSproc,$dbParams,1);
		  if ($rs === false) {		
				$errCode = E_DB_SELECT;
				//WriteLog("db", $errCode, $dbErr);			  
		  } 
		  else
		  {
			$row = sqlsrv_fetch_object($rs);
		  }
		}		
		
		if (!isset($row) || $row==null) {
			$errCode = E_DB_RETURN;			
			WriteLog("db", $errCode, $dbErr);			
			//exit;
		}
		
		if ($errCode != "") {		
			$response = '<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
							<error code=\"DB_CONNECTION_ERROR '.$errCode.'\" message=\"Error to connect to DB\"  />
						</gameresponse>';
			ErrorResponse($errCode, $dbErr);
			echo $response;
			return;
		}
		
		//Check for possible errors in the 
		if(isset($row->Result))
		{
			switch($row->Result)
			{
				case -10:
				case -11:
					$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
									<error code=\"GENERAL_SESSIONEXPIRED\" message=\"Session invalid\" />
								</gameresponse>";						
					break;
				
			}
			echo $response;
			return;
		}
		
		//print_r($row);
		
		$playerCurrencySymbol = $row->symbol;//htmlentities($row->symbol);
		$playerDecSymbol = $row->centsSymbol;
		
		$response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
					<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
						<header>
							<player brandid=\"".$playerBrandId."\" currencysymbol=\"".$playerCurrencySymbol."\" 
							decsymbol=\"".$playerDecSymbol."\" currencycode=\"".$playerCurrencyCode."\" 
							displaybalance=\"".$row->displayBalance."\" realbalance=\"".$row->real."\" 
							bonusbalance=\"".$row->bonus."\" ssotoken=\"".$playerSSOToken."\" 
							languagecode=\"".$playerLanguageCode."\" guest=\"false\" />
						</header>
						<game action=\"init\" gameid=\"0\" sessionid=\"$gameSessionId\" brandgameid=\"$gameBrandGameId\" friendlyid=\"0\">
							<init min_inside=\"0\" max_inside=\"0\" min_outside=\"0\" max_outside=\"0\" 
							minstake=\"".$row->minStake."\" maxstake=\"".$row->maxStake."\" stakeincrement=\"".$row->stakeValues."\"
							defaultstake=\"".$row->defaultStake."\" maxcoins=\"0\" configid=\"6bfea121-dd9b-df11-aaa8-78e7d156c70c\"
							maxwinmultiplier=\"10000\" />
						</game>
					</gameresponse>";
					
		dbFree($rs);
		dbClose($db);
		
		echo $response;
		return;
	}
	if($game->game["action"] == "getcard")
	{				
		$gameBetStake = $game->game->play->betstate->bet["stake"];	
		//print_r($game);
		//Check we have all we need, if we don't. Send an error message
		
		if(	!isset($playerCurrencySymbol) || $playerCurrencySymbol == "" ||
			!isset($playerDecSymbol) || $playerDecSymbol == "" ||
			!isset($playerCurrencyCode) || $playerCurrencyCode == "" ||
			!isset($playerLanguageCode) || $playerLanguageCode == "" ||
			!isset($playerSSOToken) || $playerSSOToken == "" ||
			!isset($playerBrandId) || $playerBrandId == "" ||
			!isset($gameMode) || $gameMode == "" ||
			!isset($gameBrandGameId) || $gameBrandGameId == "" ||			
			!isset($gameScratchCardPrizeGUID) || $gameScratchCardPrizeGUID == "" ||
			!isset($gameBetStake) || $gameBetStake == "" )
		{
			//Send a blank response so the client can trigger the bad xml message
			$response = "";
			return;
		}
				
		require_once("D:/WEB/inc/db.php");

		$dbErr = "";
		$dbSproc = "WebScratchCardPlay";
		$dbParams = array("PlayerID"        		=> array($playerBrandId, "int", 0),
						  "SessionID"      		    => array($playerSSOToken, "str", 36),
						  "IP"             	    	=> array($_SERVER["REMOTE_ADDR"], "str", 15),
						  "ScratchCardSecurityGUID"	=> array($gameScratchCardPrizeGUID, "str", 20),						  
						  "ScratchCardType"			=> array($gameMode, "int", 0)//Promo type for now							  
						 );

		$db = "";
		
		//"BINGOX1\GAME"
		if(!dbInit("","agsMain",1)) {
			$errCode = E_DB_CONNECT;
			//WriteLog("db", $errCode, $dbErr);		  
		} else {
			$rs = dbExecSproc($dbSproc,$dbParams,1);
			if ($rs === false) {
				$errCode = E_DB_SELECT;
				//WriteLog("db", $errCode, $dbErr);			  
			} 
			else
			{
				$row = sqlsrv_fetch_object($rs);
			}
		}
						
		$languageCode = array("en" => "en_GB");
		//Get the player and game general info
		
		if (!isset($row) || $row==null) {
			$errCode = E_DB_RETURN;			
			//WriteLog("db", $errCode, $dbErr);			
			//exit;
		}
		
		if ($dbErr != "") {			
			$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
							<error code=\"DB_CONNECTION_ERROR\" message=\"Error to connect to DB\" />
						</gameresponse>";
			ErrorResponse($errCode, $dbErr);
			echo $response;
			return;
		}
				
		//Check for possible errors in the 
		if(isset($row->Result))
		{
			switch($row->Result)
			{
				case -1:
				case -2:
					$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
									<error code=\"TXERROR_THIRDPARTY\" message=\"Error trying to process promotion / Transaction\" />
								</gameresponse>";
					break;
				case -10:
				case -11:
					$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
									<error code=\"GENERAL_SESSIONEXPIRED\" message=\"Session invalid\" />
								</gameresponse>";
					break;
				case -100:
					$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
									<error code=\"SCRATCHCARDGAMEID_ALREADYPLAYED\" message=\"Scratch Card or Prize Set not found\" />
								</gameresponse>";
					break;
				case -101:
					$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
									<error code=\"SCRATCHCARDGAMEID_ALREADYPLAYED\" message=\"Scratch Card already played\" />
								</gameresponse>";
					break;
				case -102:
					$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
									<error code=\"SCRATCHCARDGAMEID_EXPIRED\" message=\"Scratch Card expired\" />
								</gameresponse>";
					break;
			}
			echo $response;
			return;
		}
		
		if(!isset($row->numRows))
		{	
			$response = "<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
					<error code=\"TXERROR_THIRDPARTY\" message=\"Error trying to process promotion - no rows returned/ Transaction\" />
				</gameresponse>";
			echo $response;
			return;			
		}
		
		$gameRows = $row->numRows;
		$gameColumns = $row->numColumns;
		$gameAllowedScratches = $row->allowedScratches;
		
		$response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
					<gameresponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">
						<header>
							<player brandid=\"".$playerBrandId."\" currencysymbol=\"".$playerCurrencySymbol."\" 
							decsymbol=\"".$playerDecSymbol."\" currencycode=\"".$playerCurrencyCode."\" 
							displaybalance=\"".$row->displayBalance."\" realbalance=\"".$row->real."\" 
							bonusbalance=\"".$row->bonus."\" ssotoken=\"".$playerSSOToken."\" 
							languagecode=\"".$playerLanguageCode."\" guest=\"false\" />
						</header>
						<game action=\"getcard\" gameid=\"0\" sessionid=\"$gameSessionId\" brandgameid=\"$gameBrandGameId\" friendlyid=\"0\">";
										
		//Now get the payouts and game values
		if(sqlsrv_next_result($rs))
		{
			$totalPayout = 0;
			$payoutSection = "";
			//Payouts
			while($row = sqlsrv_fetch_object($rs))
			{
				$isWin = $row->isWin==1?'true':'false';
				if($row->isWin == 1)
				{
					$totalPayout += $row->amount;
				}				
				$payoutSection .= "<payout amount=\"".$row->amount."\" iswin=\"".$isWin."\" />";
			}
			
			$response .= "	<play balldrop=\"0\" totalstake=\"".$gameBetStake."\" totalpayout=\"".$totalPayout."\" stakeincrement=\"0\" coins=\"0\">
								<scratchcardstate>
									<payouts>
										".$payoutSection."
									</payouts>";
									
			//Game values
			if(sqlsrv_next_result($rs))
			{
				$response .= "<game columnsize=\"".$gameColumns."\" rowsize=\"".$gameRows."\" totalallowedscratches=\"".
							$gameAllowedScratches."\" winindex=\"0\">
									<symbols>";
				while($row = sqlsrv_fetch_object($rs))
				{
					$isWin = $row->isWin==1?'true':'false';
					$response .= 	"<symbol name=\"".$row->name."\" keyname=\"".$row->keyname."\" value=\"".$row->value."\" iswin=\"".$isWin."\" />";
				}				
				$response .= "			</symbols>
									</game>
								</scratchcardstate>
							</play>
						</game>
					</gameresponse>";
			}
			
		}
		
		dbFree($rs);
		dbClose($db);		
		
		echo $response;
	}
?>