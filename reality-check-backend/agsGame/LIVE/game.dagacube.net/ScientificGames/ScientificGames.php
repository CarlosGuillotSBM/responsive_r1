<?php
/*
This file is called from scientific games
15Apr16 AF - Send Ref and SubRef as varchars from php to store them as bigint in db
*/
header('Content-type: application/xml');
ini_set('display_errors', 'On');
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "SG";
$GameProviderID   = 19;//Scientificgames=19
$COGSGuid         = "B31E14B1-BB11-4A37-A154-A8DC8B07570E"; //this is agsMain.Skin.COGSGuid for SkinID=1 (spin and win). Used when Ash sends a Recon request with no session id. The db will validate this

/*
We rewrite all calls to go to scientificgames.php and evaluate the call methods based on the $_SERVER["REQUEST_URI"]
*/

/*

$Verb          = $_SERVER["REQUEST_METHOD"]; //GET | PUT | DELETE
$RequestArray  = explode("/",strtolower(substr($_SERVER["REQUEST_URI"],15)));
$RequestMethod = $RequestArray[0]; //system | players | gamesessions | gamecycles
$SessionID     = substr($_SERVER["HTTP_AUTHORIZATION"],7,36); //Authorisation header is sent as "Bearer <sessionid><GameLaunchSourceID><playerid>"
$GameLaunchSourceID = intval(substr($_SERVER["HTTP_AUTHORIZATION"],43,3));
$PlayerID      = intval(substr($_SERVER["HTTP_AUTHORIZATION"],46));
$Response = "";

$phpInput      = file_get_contents("php://input");
$RequestData   = "Call:".$Verb." ".$_SERVER["REQUEST_URI"]." Auth:".$_SERVER["HTTP_AUTHORIZATION"]." Params:".json_encode($_REQUEST)." Content:".$phpInput." RequestArray:".json_encode($RequestArray);


AUTHENTICATE ***********************************************************************

<?xml version="1.0" encoding="UTF-8"?>
<ns3:playerAuthentication xmlns:ns3="http://williamsinteractive.com/integration/vanilla/api/player" xmlns:ns2="http://williamsinteractive.com/integration/vanilla/api/common">
   <accountRef>310</accountRef>
   <context>DESKTOP</context>
   <gameCode>zeus</gameCode>
   <ticket>SUCCESS</ticket>
</ns3:playerAuthentication>



<?xml version="1.0" encoding="UTF-8"?>
<ns3:playerAuthenticationResponse xmlns:ns3="http://williamsinteractive.com/integration/vanilla/api/player" xmlns:ns2="http://williamsinteractive.com/integration/vanilla/api/common" xmlns:ns4="http://williamsinteractive.com/integration/vanilla/api/transaction">
   <accountRef>310</accountRef>
   <balance>1000</balance>
   <currency>EUR</currency>
   <language>en_US</language>
   <replacementTicket>SUCCESS_REPLACED</replacementTicket>
   <result>SUCCESS</result>
</ns3:playerAuthenticationResponse>



*/
$PlayerID = 0;
$SessionID = null;

$RawRequest  = file_get_contents("php://input");

$RequestMethod = @explode("/",strtolower($_SERVER["REQUEST_URI"]))[3]; //authenticate
$Request = simplexml_load_string($RawRequest);
$PlayerID = @$Request->accountRef;

WriteLog($GameProviderCode, 0, "RECV: ".$RequestMethod.' '.$RawRequest, $PlayerID);



//First section to be removed after testing
if($Request->ticket == 'SUCCESS' || $Request->ticket == 'SUCCESS_REPLACED')
{
  $SessionID = "f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb";
  $GameLaunchSourceID = 1;
}
else
{
  $SessionID = @substr($Request->ticket,0,36);
  $GameLaunchSourceID = @intval(substr($Request->ticket,36,4));
}

if ($PlayerID == 0) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $RawRequest [PlayerID invalid]");
}

if ( ($RequestMethod == "authenticate" || $RequestMethod == "transfertogame" || $RequestMethod == "getbalance") && strlen($SessionID) != 36 ) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $RawRequest [SessionID invalid]");
}

if ( ($RequestMethod == "transfertogame" || $RequestMethod == "transferfromgame" ) && (!isset($Request->gameRoundId) || !isset($Request->transactionId))) {
  ErrorResponse(E_INVALID_PARAMS, "RECV: $RawRequest [gameRound invalid]");
}

if(empty($SessionID)){
  $SessionID = "00000000-0000-0000-0000-000000000000";
}

require_once("D:/WEB/inc/db56.php");

$dbErr = $SdbParams = "";
$db = "";
$dbSproc = "xTxGame";

// setup the param array to call xTxGame.
$dbParams = array(
  "PlayerID"           => array(intval($PlayerID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "SessionID"          => array($SessionID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_NVARCHAR(36)),
  "IP"                 => array($_SERVER["REMOTE_ADDR"], SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
  "GameProviderID"     => array(intval($GameProviderID), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "GameProviderGameID" => array(strval($Request->gameCode), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(100)),
  "GameLaunchSourceID" => array($GameLaunchSourceID, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_INT, SQLSRV_SQLTYPE_INT),
  "Action"             => array("", SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(15)),
  "Wager"              => array(floatval(0), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
  "Win"                => array(floatval(0), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_MONEY),
  "Ref"                => array("0", SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_BIGINT),
  "SubRef"             => array("0", SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_BIGINT),
  "Refund"             => array(floatval(0), SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_FLOAT, SQLSRV_SQLTYPE_FLOAT),
  "RefundRef"          => array("", SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(50)),
  "RefundNotes"        => array("", SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_STRING("UTF-8"), SQLSRV_SQLTYPE_VARCHAR(4000))
);

if(dbInit("", "Main", __FILE__."[".__LINE__."]", 1) == false) {
  $errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
  ErrorResponse($errCode, $dbErr);//exits
}

if(strtolower($Request->context) == 'mobile')
{
  $dbParams["GameProviderGameID"][0] .= "-mobile";
}

switch($RequestMethod)
{
  //Check authentication session
  case "authenticate":
    $dbParams["Action"][0] = "GetBalance";
    $row = runQuery($dbSproc,$dbParams);
    $Response = createResponseHeader("playerAuthenticationResponse","ns3");

    if ($row->Result == 0)
    {
      $Response->addChild('result','SUCCESS','');
      $Response->addChild('accountRef',$PlayerID,'');
      $Response->addChild('balance',floor($row->Balance * 100),'');
      $Response->addChild('currency',$row->CurrencyCode,'');
      $Response->addChild('language',$row->LanguageCode.'_'.$row->CountryCode,'');
    }
    else
    {
      WriteLog($GameProviderCode, 0, "ERROR: ".print_r($row,true), $PlayerID);
    }
    break;


  case "getbalance";
    $dbParams["Action"][0] = "GetBalance";
    $row = runQuery($dbSproc,$dbParams);

    $Response = createResponseHeader("balanceResponse","ns3");

    if ($row->Result == 0)
    {
      $Response->addChild('result','SUCCESS','');
      $Response->addChild('balance',floor($row->Balance * 100),'');
    }
    break;


  case "transfertogame";
    $dbParams["Action"][0] = "Tx";
    $dbParams["Wager"][0] = ($Request->amount == 0) ? 0 : floatval($Request->amount / 100);
    $dbParams["Win"][0] = 0;
    $dbParams["Ref"][0]    = strval($Request->gameRoundId);
    $dbParams["SubRef"][0] = strval($Request->transactionId);

    $row = runQuery($dbSproc,$dbParams);
    $Response = createResponseHeader("transferToGameResponse","ns4");

    if ($row->Result == 0)
    {
      $Response->addChild('balance',floor($row->Balance * 100),'');
      $Response->addChild('result','SUCCESS','');
      $Response->addChild('accountRef',$PlayerID,'');

      $Response->addChild('partnerTransactionRef',$row->agsRef,'');
    }
    else
    {
      $Response->addChild('result','FAILURE','');
      if($row->Result == 304)
      {
        $Response->addChild('message','Game failure','');
      }
      else
      {
        $Response->addChild('message','Not enough balance to place this wager. Please visit the cashier','');
      }
      WriteLog($GameProviderCode, 0, "ERROR: ".print_r($row,true), $PlayerID);

      $Response->addChild('Reconcile','false','');
      //$Response->addChild('requiredUserAction','EXIT','');
    }
    break;



  case "transferfromgame";
    $dbParams["Action"][0] = "TxEndGameRound";
    $dbParams["Wager"][0] = 0;
    $dbParams["Win"][0] = ($Request->amount == 0) ? 0 : floatval($Request->amount / 100);

    $dbParams["Ref"][0] = strval($Request->gameRoundId);
    $dbParams["SubRef"][0] = strval($Request->transactionId);
    $dbParams["SessionID"][0] = "f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb";

    $row = runQuery($dbSproc,$dbParams);
    $Response = createResponseHeader("transferFromGameResponse","ns4");

    if ($row->Result == 0)
    {
      $Response->addChild('result','SUCCESS','');
      $Response->addChild('accountRef',$PlayerID,'');
      $Response->addChild('balance',floor($row->Balance * 100),'');
      $Response->addChild('partnerTransactionRef',$row->agsRef,'');
    }
    else
    {
      $Response->addChild('result','FAILURE','');
      $Response->addChild('message','Game failure','');
      //$Response->addChild('Reconcile','true','');
      //$Response->addChild('requiredUserAction','EXIT','');

      WriteLog($GameProviderCode, 0, "ERROR: ".print_r($row,true), $PlayerID);
    }
    break;


  case "canceltransfertogame";

    $dbParams["Action"][0] = "Void";
    $dbParams["Wager"][0] = 0;
    $dbParams["Win"][0] = 0;
    $dbParams["Ref"][0] = strval($Request->gameRoundId);
    $dbParams["SubRef"][0] = strval($Request->transactionId);
    $dbParams["Refund"][0] = ($Request->amount == 0) ? 0 : floatval($Request->amount / 100);
    $dbParams["RefundRef"][0] = '';
    $dbParams["RefundNotes"][0] = strval($Request->reason);
    $dbParams["SessionID"][0] = "f2dfbeba-1ddb-41ed-9ae8-c218d14d39cb";

    $row = runQuery($dbSproc,$dbParams);

    //echo SdbParams($dbParams);

    $Response = createResponseHeader("cancelTransferToGameResponse","ns4");

    if ($row->Result == 0)
    {
      $Response->addChild('result','SUCCESS','');
      $Response->addChild('balance',floor($row->Balance * 100),'');
      $Response->addChild('partnerTransactionRef',$row->agsRef,'');
    }
    else
    {
      $Response->addChild('result','FAILURE','');
      $Response->addChild('message','Game failure','');
      //$Response->addChild('Reconcile','true','');
      //$Response->addChild('requiredUserAction','EXIT','');
    }
    break;





  default:
    ErrorResponse(E_INVALID_PARAMS, "RECV: $RawRequest [RequestMethod param missing]");

  break;
}
//WriteLog($GameProviderCode, 0, print_r($Response,true), $PlayerID);

SendResponse($Response);

exit;

/*
******************************************************************************************
FUNCTIONS
******************************************************************************************
*/

function runQuery($dbSproc,$dbParams)
{
  global $dbErr;
  $rs = dbExecSproc($dbSproc,$dbParams);
  if ($rs === false)
  {
    $errCode = E_DB_SELECT;
    WriteLog("db", $errCode, $dbErr);
  }
  else
  {
    $row = ((sqlsrv_has_rows($rs)) ? sqlsrv_fetch_object($rs) : null);
    if (!isset($row) || $row==null)
    {
      $errCode = E_DB_RETURN;
      WriteLog("db", $errCode, $dbErr);
    }
  }

  if ($dbErr != "")
  {
    ErrorResponse($errCode, $dbErr);//exits
  }

  dbFree($rs);
  return $row;
}


function ErrorResponse ($errCode, $data) {
  global $GameProviderCode, $PlayerID, $glo, $RequestMethod, $Verb;
  $statusCode = 401;//unauthorised - the user is not logged in
  if ($RequestMethod == "gamecycles") {
    if ($errCode == E_INVALID_GAMEID ||
        $errCode == E_PLAYER_NOT_FOUND ||
        $errCode == E_RECON_TXGAME_NOT_FOUND
       )
      $statusCode = 404;//not found

    if ($errCode == E_PLAYER_LIMITS_EXCEEDED)
      $statusCode = 530;//player limits exceeded

    if ($statusCode == 401 && $Verb == "PUT") {
      if ($errCode == E_INSUFFICIENT_FUNDS)
        $statusCode = 402;//payment required
    }
  }
  $data = "SEND: Status=".$statusCode.(($data == "") ? "" : "   ").$data;
  if ($errCode > 0) {
    http_response_code($statusCode);
  }

  WriteLog($GameProviderCode, $errCode, $data, $PlayerID);
  exit;
}


function SendResponse($Response)
{
  global $GameProviderCode,$PlayerID;
  $r = str_replace(' xmlns=""','',$Response->asXML());
  WriteLog($GameProviderCode, 0, "SEND: ".$r, $PlayerID);
  echo $r; // returns the xml
}


function createResponseHeader($responseName,$nameSpace)
{
  return simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?><'.$nameSpace.':'.$responseName.' xmlns:ns3="http://williamsinteractive.com/integration/vanilla/api/player" xmlns:ns2="http://williamsinteractive.com/integration/vanilla/api/common" xmlns:ns4="http://williamsinteractive.com/integration/vanilla/api/transaction" />');
}

?>