<?php
/*
This file is called from eyecon after a player has launched an eyecon game in a new browser window.
*/
require_once("D:/WEB/inc/agsGameInc.php"); //global variables and functions
$GameProviderCode = "Eyecon";
$GameProviderID   = 2;//Eyecon=2
$COGSGuid         = "B31E14B1-BB11-4A37-A154-A8DC8B07570E"; //this is agsMain.Skin.COGSGuid for SkinID=1 (spin and win). Used when Eyecon sends a ??? request with no session id. The db will validate this
$accessid         = "uy76et522w";//eyecon accessid for them to access our server

function ErrorResponse ($errCode, $data) {
	global $GameProviderCode, $PlayerID;
	
	switch ($errCode) {
		case E_INVALID_AUTH:
		  $EyeconErrCode = 2;
		  break;
		case E_INVALID_PLAYER_SESSION:
		  $EyeconErrCode = 7;
		  break;
		case E_INVALID_TXID:
		  $EyeconErrCode = 8;
		  break;
		case E_INSUFFICIENT_FUNDS:
		  $EyeconErrCode = 13;
		  break;
		case E_DUPLICATE_REF:
		  $EyeconErrCode = 15;
		  break;
		default:
		  $EyeconErrCode = 8;
		  break;
	}
	
  $Response = "status=invalid&error=$EyeconErrCode";
  $data = "SEND: ".$Response.(($data == "") ? "" : "   ").$data;
  WriteLog($GameProviderCode, $errCode, $data, $PlayerID);

  echo $Response;
  exit;
}
$rawRequest = "";

foreach ($_GET as $var => $value) {
  $_GET[$var] = trim($_GET[$var]);
  $rawRequest.="$var=$value ";
}
$Response = "";

$PlayerID = isset($_GET["uid"]) ? $_GET["uid"] : 0;
WriteLog($GameProviderCode, 0, "RECV: ".$rawRequest, $PlayerID);

$errCode = 0;

if (!(@$_GET["accessid"] == $accessid)) {
  ErrorResponse(E_INVALID_AUTH, "");
}

$refund = 0;
$refundref="";
$refundnotes="";

$gameid    = isset($_GET["gameid"]) ? $_GET["gameid"] : 0; //the gameid as used from Eyecon games
$SessionID = isset($_GET["guid"])   ? substr($_GET["guid"],0,36) : "";
$Wager     = isset($_GET["wager"])  ? $_GET["wager"]  : -1;
$Win       = isset($_GET["win"])    ? $_GET["win"]    : -1;
$ref       = isset($_GET["round"])  ? $_GET["round"]  : 0;
$subref    = isset($_GET["ref"])  ? $_GET["ref"]  : 0;//all their refs should be globally unique
//$subref    = 1;//all eyecon subrefs will be 1 as they dont have a way of recordiing a rame round - they only record an entire session - so each ref is unique
$gtype     = isset($_GET["gtype"])   ? $_GET["gtype"]   : "";
$type      = isset($_GET["type"])   ? $_GET["type"]   : "";
//$jpwin   = isset($_GET["jpwin"])   ? $_GET["jpwin"]   : 0;
$GameLaunchSourceID = intval(substr($_GET["guid"],36,3));

if ($Wager < 0)       ErrorResponse(E_INVALID_WAGER, "");
if ($Win < 0)         ErrorResponse(E_INVALID_WIN, "");
if ($PlayerID == 0)   ErrorResponse(E_INVALID_PLAYER_SESSION, "$PlayerID");
if ($SessionID == "") ErrorResponse(E_INVALID_PLAYER_SESSION, "$SessionID");
if ($ref == 0 && $type != "BALANCE_CHECK") ErrorResponse(E_INVALID_TXID, "");
if ($subref == 0 && $type != "BALANCE_CHECK")     ErrorResponse(E_INVALID_TXID, "");
if ($gtype == "")     ErrorResponse(E_INVALID_PARAMS, "");

$RequestMethod = "TxEndGameRound";
$ExtraParam = "";


if ($gtype == "BC") {
  $RequestMethod = "GetBalance";
  $ExtraParam = "&historyURL=http://skin1.aquagamesys.net/testskinex&alias=Activity";
}

if ($type == "RESEND") {
  $RequestMethod = "Recon";
}

if ($type == "ROLLBACK") {
  $RequestMethod = "Void";
}

$dbErr = "";
$dbSproc = "xTxGame";
$db = "";

$dbParams = array(
    "PlayerID"           => array($PlayerID, "int", 0),
    "SessionID"          => array($SessionID, "str", 36),
    "IP"                 => array($_SERVER["REMOTE_ADDR"], "str", 15),//this is sheriffs ip
    "GameProviderID"     => array($GameProviderID, "int", 0),
    "GameProviderGameID" => array($gameid, "str", 30),
    "GameLaunchSourceID" => array($GameLaunchSourceID, "int", 0),
    "Action"             => array($RequestMethod, "str", 15),
    "Wager"              => array($Wager, "int", 0),
    "Win"                => array($Win, "int", 0),
    "Ref"                => array($ref, "int", 0),
    "SubRef"             => array($subref, "int", 0),
    "Refund"             => array($refund, "int", 0),
    "RefundRef"          => array($refundref, "str", 50),
    "RefundNotes"        => array($refundnotes, "str", 4000)
);

/*
db calls xTxGame
Returns:
	Result = 0 : Success
	      <> 0 : Various fail codes
*/
require_once("D:/WEB/inc/db.php");

if(!dbInit("","agsMain",1)) {
	$errCode = E_DB_CONNECT;
  WriteLog("db", $errCode, $dbErr);
} else {
  $rs = dbExecSproc($dbSproc,$dbParams,1);
  if ($rs === false) {
		$errCode = E_DB_SELECT;
	  WriteLog("db", $errCode, $dbErr);
  } else {
    $row = sqlsrv_fetch_object($rs);
    if (!isset($row) || $row==null) {
			$errCode = E_DB_RETURN;
		  WriteLog("db", $errCode, $dbErr);
    }
  }
}

if ($dbErr != "") {
  ErrorResponse($errCode, $dbErr, $Response);
}

dbFree($rs);
dbClose($db);

//db access was successful, other error
if ($row->Result != 0) {
  if ($row->Result == 104 || $row->Result == 105)
    $row->Result = 104;

  ErrorResponse($row->Result, "");
}

$Response = "status=ok&bal=".$row->Balance."&uid=$PlayerID&guid=".$SessionID.str_pad($GameLaunchSourceID, 3, '0', STR_PAD_LEFT)."&txid=".$row->agsRef.$ExtraParam;
WriteLog($GameProviderCode, 0, "SEND: ".$Response, $PlayerID);
echo $Response;
exit;
?>