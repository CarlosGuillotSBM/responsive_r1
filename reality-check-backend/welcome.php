<?php
/*
After a successful login, player goes to the welcome page.
*/
//----- This Script required on every page -----
$file = substr(__FILE__,7);
$glo = array ("Application" => substr($file,0,strpos($file,"\\")));  //agsSkins | agsWEXA | agsCashier | agsCAT
$module = substr($file,strrpos($file,"\\")+1);                       //this php file
require_once("D:/WEB/inc/vars.php");        //global variables
require_once("D:/WEB/inc/genFuncs.php");    //general functions
require_once("D:/WEB/inc/fnAGS.php");       //AGS API function that calls WEXA used for AGS($Function, $Params, $ResponseFormat)
//----------------------------------------------
function DirectToError($e) {
  global $glo;
  header("Location: ".$glo["URLs"]."/error.php?e=$e");
  exit;
}

//get the player type - used to determine the available games/content
/*
#AGSAPI# - iGetPlayerType
*/
$agsParams = array("PlayerID"  => $_POST["PlayerID"],
                   "SessionID" => $_POST["SessionID"],
                   "SkinID"    => $_POST["SkinID"],
                   "PlayerIPAddress" => $glo["IPAddress"]
                 );

$Response = AGS("iGetPlayerType", $agsParams, $glo["AGS"]["ResponseFormat"]);

if ($Response["Code"] == 0 && @$Response["Data"][0]["ReturnData"] == 0) {
  if (@$Response["Data"][0]["PlayerTypeID"] == 0) //general players cannot be here!
    DirectToError("Invalid player type");
} else {
  DirectToError("Cannot get player");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Dagacube Testing</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css\main.css" />

<script type="text/javascript">
  function nav(navTo) {
    document.navForm.action = navTo;
    document.navForm.submit();
  }

  function goGame(gid) {
    loc='<?php echo $glo["AGS"]["GameURL"];?>';
    if ((gid>=14000 && gid<=14999) || (gid>=17000 && gid<=17999)) {
      loc='http://skin1.aquagamesys.net/agsGame/game.php';
    }
	
    document.navForm.action = loc;
    document.navForm.target = 'Game';
    document.navForm.GameID.value = gid;
  if(gid == 313)
  {
    if (navigator.appName.indexOf("Microsoft") != -1) {
      w = window.open("", "Game", "height=768,width=1024,top=100,left=100,status=1");
    } else {
      w = window.open("", "Game", "height=768,width=1024,screenY=100,screenX=100");
    }
  }
  else
  {
    if (navigator.appName.indexOf("Microsoft") != -1) {
      w = window.open("", "Game", "height=720,width=1000,top=100,left=100,status=1");
    } else {
      w = window.open("", "Game", "height=720,width=1000,screenY=100,screenX=100");
    }
  }
    document.navForm.submit();
    document.navForm.target = '';
  }
</script>
</head>
<body>
  <div id="main">
    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 17)://Dagacube or NetEnt?>
      NetEnt
     <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('17001'));">Blackjack</a></td>
          <td><a href="javascript:void (goGame('17002'));">Disco Spins</a></td>
          <td><a href="javascript:void (goGame('17003'));">Flowers</a></td>
          <td><a href="javascript:void (goGame('17004'));">Gonzos Quest</a></td>
          <td><a href="javascript:void (goGame('17005'));">Jack and the Beanstalk</a></td>
          <td><a href="javascript:void (goGame('17006'));">Jackhammer</a></td>
          <td><a href="javascript:void (goGame('17007'));">Jackhammer 2</a></td>
          <td><a href="javascript:void (goGame('17008'));">Lucky Angler</a></td>
          <td><a href="javascript:void (goGame('17009'));">Magic Portals</a></td>
          <td><a href="javascript:void (goGame('17010'));">Mythic Maiden</a></td>
          <td><a href="javascript:void (goGame('17011'));">Starburst</a></td>
          <td><a href="javascript:void (goGame('17012'));">Subtopia</a></td>
          <td><a href="javascript:void (goGame('17013'));">Twin Spin</a></td>
          <td><a href="javascript:void (goGame('17014'));">Victorious</a></td>
        </tr>
        <tr>
		   <td><a href="javascript:void (goGame('17523'));">Neonstaxx Touch</a></td>
          <td><a href="javascript:void (goGame('17501'));">Blackjack Touch</a></td>
          <td><a href="javascript:void (goGame('17502'));">Disco Spins Touch</td>
          <td><a href="javascript:void (goGame('17503'));">Flowers Touch</a></td>
          <td><a href="javascript:void (goGame('17504'));">Gonzos Quest Touch</a></td>
          <td><a href="javascript:void (goGame('17505'));">Jack and the Beanstalk Touch</a></td>
          <td><a href="javascript:void (goGame('17506'));">Jackhammer Touch</a></td>
          <td><a href="javascript:void (goGame('17507'));">Jackhammer 2 Touch</a></td>
          <td><a href="javascript:void (goGame('17508'));">Lucky Angler Touch</a></td>
          <td><a href="javascript:void (goGame('17509'));">Magic Portals Touch</a></td>
          <td><a href="javascript:void (goGame('17510'));">Mythic Maiden Touch</a></td>
          <td><a href="javascript:void (goGame('17511'));">Starburst Touch</a></td>
          <td><a href="javascript:void (goGame('17512'));">Subtopia Touch</a></td>
          <td><a href="javascript:void (goGame('17513'));">Twin Spin Touch</a></td>
          <td><a href="javascript:void (goGame('17514'));">Victorious Touch</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('17450'));">Blackjack Live</a></td>
          <td><a href="javascript:void (goGame('17451'));">Blackjack Live Common Draw</a></td>
          <td><a href="javascript:void (goGame('17452'));">Roulette Live</a></td>
        </tr>
     </table>
    <br><br>
    <?php endif;?>
    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1)://Dagacube?>
      <?php if ($Response["Data"][0]["SkinID"] == 6):?>
      <br><br>
      itechlabs - 1502203 Bingo plaftorm file redistribution
			 <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
					<tr>
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Candy%20Store" target="_new">Bingo Extra Room:  The Candy Store</a></td>
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Diner" target="_new">Bingo Extra Room:  The Diner</a></td>
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Drive-In" target="_new">Bingo Extra Room:  The Drive-In</a></td>						
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Extra%20Room" target="_new">Bingo Extra Room:  The Extra Room</a></td>
						<td><a href="https://www.bingoextra.com/bingo-rules" target="_new">Bingo Rules</a></td>
						<td><a href="https://www.bingoextra.com/promotions/we-got-more-balls" target="_new">Feature: We Got More Balls</a></td>
						<td><a href="https://www.bingoextra.com/promotions/double-your-winnings" target="_new">Feature: DOUBLE YOUR WINNINGS</a></td>
						<td><a href="https://www.bingoextra.com/promotions/progressive-jackpots" target="_new">Feature: Progressive Jackpots</a></td>
						<td><a href="https://www.bingoextra.com/terms-and-conditions" target="_new">Bingo Extra Terms And Conditions</a></td>
					</tr>
			 </table>
			<br><br>
		<?php endif;?>
		<?php if ($Response["Data"][0]["SkinID"] == 2):?>
		<br><br>
		itechlabs - 1504193 Bingo plaftorm Flash and old HTML5 clients
			<table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
					<tr>
						<td><a href="http://skin1.aquagamesys.net/flash/Bingo/Kitty/" target="_new">Kitty bingo: Flash Client</a></td>	
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/kitty/tablet/index.php" target="_new">Kitty bingo: HTML5 Tablet Client</a></td>	
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/kitty/mobile/index.php" target="_new">Kitty bingo: HTML5 Mobile Client</a></td>							
					</tr>
			 </table>
		<?php endif;?>
    <?php endif;?>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 15)://Tablet only?>
      Bingo on a tablet
     <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
      <td><a href="http://skin1.aquagamesys.net/html5/Bingo/kitty/tablet/" target="_new">Kitty-Bingo (HTML5)</a></td>
        </tr>
     </table>
    <?php endif;?>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 5 || $Response["Data"][0]["PlayerTypeID"] == 16 || $Response["Data"][0]["PlayerTypeID"] == 20)://Alderney Tester or NMi or itechlabs?>
		<?php if ($Response["Data"][0]["SkinID"] == 6):?>
      <br><br>
      itechlabs - 1502203 Bingo plaftorm file redistribution
			 <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
					<tr>
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Candy%20Store" target="_new">Bingo Extra Room:  The Candy Store</a></td>
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Diner" target="_new">Bingo Extra Room:  The Diner</a></td>
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Drive-In" target="_new">Bingo Extra Room:  The Drive-In</a></td>						
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/extra/index.php?Room=The%20Extra%20Room" target="_new">Bingo Extra Room:  The Extra Room</a></td>
						<td><a href="https://www.bingoextra.com/bingo-rules" target="_new">Bingo Rules</a></td>
						<td><a href="https://www.bingoextra.com/promotions/we-got-more-balls" target="_new">Feature: We Got More Balls</a></td>
						<td><a href="https://www.bingoextra.com/promotions/double-your-winnings" target="_new">Feature: DOUBLE YOUR WINNINGS</a></td>
						<td><a href="https://www.bingoextra.com/promotions/progressive-jackpots" target="_new">Feature: Progressive Jackpots</a></td>
						<td><a href="https://www.bingoextra.com/terms-and-conditions" target="_new">Bingo Extra Terms And Conditions</a></td>
					</tr>
			 </table>
			<br><br>
		<?php endif;?>
		<?php if ($Response["Data"][0]["SkinID"] == 2):?>
		<br><br>
		itechlabs - 1504193 Bingo plaftorm Flash and old HTML5 clients
			<table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
					<tr>
						<td><a href="http://skin1.aquagamesys.net/flash/Bingo/Kitty/" target="_new">Kitty bingo: Flash Client</a></td>	
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/kitty/tablet/index.php" target="_new">Kitty bingo: HTML5 Tablet Client</a></td>	
						<td><a href="http://skin1.aquagamesys.net/html5/bingo/public/kitty/mobile/index.php" target="_new">Kitty bingo: HTML5 Mobile Client</a></td>							
					</tr>
			 </table>
		<?php endif;?>
    <?php endif;?>
		
		<?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 18 )://Realistic?>
      <br><br>
      Realistic - SessionID: <?php echo @$_POST["SessionID"];?><br>
	  Realistic - PlayerID: <?php echo @$_POST["PlayerID"];?><br>
	  
			 <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
					<tr>							
							<td><a href="javascript:void (goGame('16000'));">6 Appeal</a></td>						
					</tr>
					<tr>							
							<td><a href="javascript:void (goGame('16002'));">Blackjack - Realistic</a></td>						
							<td><a href="javascript:void (goGame('16003'));">Bullseye</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16004'));">Colour of Money</a></td>						
							<td><a href="javascript:void (goGame('16005'));">Double Bubble</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16006'));">European Roulette Realistic</a></td>						
							<td><a href="javascript:void (goGame('16007'));">Hi Lo Blackjack</a></td>	
					</tr>					
					<tr>							
							<td></td>						
							<td><a href="javascript:void (goGame('16009'));">High Rise</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16010'));">Hot Cross Bunnies</a></td>						
							<td><a href="javascript:void (goGame('16011'));">It Came From The Moon</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16012'));">Jackpot Cherries</a></td>						
							<td></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16014'));">Over The Rainbow</a></td>						
							<td><a href="javascript:void (goGame('16015'));">Pentagram</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16016'));">Pirate Radio</a></td>						
							<td><a href="javascript:void (goGame('16017'));">Randall's Riches</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16018'));">Riverboat Gambler</a></td>						
							<td><a href="javascript:void (goGame('16019'));">Snakes and Ladders</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16020'));">Snapshot</a></td>						
							<td><a href="javascript:void (goGame('16021'));">Sunrise Reels</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16022'));">Sunset Reels</a></td>						
							<td><a href="javascript:void (goGame('16023'));">Super Graphics Upside Down</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16024'));">The Great Pyramid</a></td>						
							<td></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16026'));">Tutankhamun</a></td>						
							<td><a href="javascript:void (goGame('16027'));">Wild Safari</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16500'));">6 Appeal - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16502'));">Bullseye - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16503'));">Colour of Money - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16504'));">Diamonds and Rubies - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16505'));">Double Bubble - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16506'));">European Roulette Realistic - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16507'));">Golden Koi- Mobile</a></td>						
							<td><a href="javascript:void (goGame('16508'));">Hi Lo Blackjack - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16509'));">Hi Lo Gambler - Mobile</a></td>						
							<td></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16511'));">Hot Cross Bunnies - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16512'));">It Came From The Moon - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16513'));">Jackpot Cherries- Mobile</a></td>						
							<td><a href="javascript:void (goGame('16514'));">Keno - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16515'));">Over The Rainbow - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16516'));">Pentagram - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16517'));">Pirate Radio - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16518'));">Pot Luck - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16519'));">Randalls Riches - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16520'));">Riverboat Gambler - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16521'));">Snakes and Ladders - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16522'));">Snapshot - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16523'));">Sunken treasure - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16524'));">Sunrise Reels - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16525'));">Sunset Reels - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16526'));">Super Graphics Upside Down - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16527'));">The Great Pyramid - Mobile</a></td>						
							<td><a href="javascript:void (goGame('16528'));">Tutankhamun - Mobile</a></td>	
					</tr>					
					<tr>							
							<td><a href="javascript:void (goGame('16529'));">Wild Safari - Mobile</a></td>						
							<td></td>									
					</tr>		
					<tr>							
							<td><a href="javascript:void (goGame('16530'));">Blackjack - Mobile</a></td>						
							<td></td>									
					</tr>						
			 </table>
			<br><br>
    <?php endif;?>
		
		<?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 19 )://Geco?>
      <br><br>
      Geco - Session: <?php echo @$_POST["SessionID"];?>
			 <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
							<tr><td><a href="javascript:void (goGame('18502'));">Light Racers Mobile</a></td></tr>	
							<!--
							<tr><td><a href="javascript:void (goGame('18000'));">Nauticus mobile</a></td></tr>	
							<tr><td><a href="javascript:void (goGame('18009'));">Mystic Gems</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18011'));">KGB Bears</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18017'));">Light Racers</a></td></tr>							
							<tr><td><a href="javascript:void (goGame('18004'));">88 Coins</a></td></tr>	
							<tr><td><a href="javascript:void (goGame('18005'));">Treasure Compass</a></td></tr>	
							<tr><td><a href="javascript:void (goGame('18500'));">Nauticus flash</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18501'));">Mystic Gems flash</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18503'));">88 Coins Flash</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18502'));">Treasure Compass Flash</a></td></tr>
							
							<tr><td><a href="javascript:void (goGame('18018'));">Reel Force Five html</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18019'));">Scratch for Diamonds HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18020'));">Scratch for Emeralds HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18021'));">Scratch for Gold HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18022'));">Scratch for Rubies HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('18023'));">Core European Roulette</a></td></tr>
							-->

			 </table>
			<br><br>
    <?php endif;?>

	
		<?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 19 )://Geco?>
		<!--
	<br><br>
      Core Gaming - Session: <?php echo @$_POST["SessionID"];?>
			 <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
																		
							<tr><td><a href="javascript:void (goGame('22501'));">Reel Force Five html</a></td></tr>
							<tr><td><a href="javascript:void (goGame('22505'));">Scratch for Diamonds HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('22504'));">Scratch for Emeralds HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('22502'));">Scratch for Gold HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('22503'));">Scratch for Rubies HTML</a></td></tr>
							<tr><td><a href="javascript:void (goGame('22500'));">Core European Roulette</a></td></tr>

			 </table>
			<br><br>
			-->
    <?php endif;?>	
	
	
	
    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 8)://SQS Tester?>
      <br><br>
      SQS - Session: <?php echo @$_POST["SessionID"];?>
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
      <tr>
			</tr>
     </table>
    <?php endif;?>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 12)://Playngo Gaming Tester?>
      <br><br>
      PlaynGo
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('14001'));">Wild Melon</a></td>
          <td><a href="javascript:void (goGame('14501'));">Wild Melon Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14002'));">Ninja Fruits</a></td>
          <td><a href="javascript:void (goGame('14502'));">Ninja Fruits Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14003'));">Blackjack</a></td>
          <td><a href="javascript:void (goGame('14503'));">Texas Holdem Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14004'));">Lucky Diamonds</a></td>
          <td><a href="javascript:void (goGame('14504'));">Lucky Diamonds Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14005'));">Jolly Roger</a></td>
          <td><a href="javascript:void (goGame('14506'));">Jolly Roger Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14006'));">Gold Trophy</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14007'));">Fruit Bonanza</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14008'));">Ace of Spades</a></td>
          <td><a href="javascript:void (goGame('14505'));">Ace of Spades Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('14009'));">Bell of Fortune</a></td>
          <td><a href="javascript:void (goGame('14509'));">Bell of Fortune Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14010'));">Speed Cash</a></td>
          <td><a href="javascript:void (goGame('14514'));">Speed Cash Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14011'));">Gift Shop</a></td>
          <td><a href="javascript:void (goGame('14510'));">Gift Shop Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14012'));">Golden Goal</a></td>
          <td><a href="javascript:void (goGame('14511'));">Golden Goal Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14013'));">Cats And Cash</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14014'));">CopsnRobbers</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14015'));">Tropical Holiday</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14016'));">Fortune Teller</a></td>
          <td><a href="javascript:void (goGame('14513'));">Fortune Teller Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14017'));">Photo Safari</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14018'));">Spacerace</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14019'));">5xMagic</a></td>
          <td><a href="javascript:void (goGame('14507'));">5xMagic Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14020'));">Irish Gold</a></td>
          <td><a href="javascript:void (goGame('14508'));">Irish Gold Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14021'));">Enchanted Meadow</a></td>
          <td><a href="javascript:void (goGame('14515'));">Enchanted Meadow Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14022'));">Riches of Ra</a></td>
          <td><a href="javascript:void (goGame('14514'));">Riches of Ra Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14023'));">Dragonship</a></td>
          <td><a href="javascript:void (goGame('14516'));">Dragonship Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14024'));">Pearl Lagoon</a></td>
          <td><a href="javascript:void (goGame('14517'));">Pearl Lagoon Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14025'));">Jewel Box</a></td>
          <td><a href="javascript:void (goGame('14518'));">Jewel Box Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14026'));">Gunslinger</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14027'));">Myth</a></td>
          <td><a href="javascript:void (goGame('14519'));">Myth Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14028'));">Gold Trophy2</a></td>
          <td><a href="javascript:void (goGame('14520'));">Gold Trophy2 Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14029'));">Aztec Idols</a></td>
          <td><a href="javascript:void (goGame('14521'));">Aztec Idols Mobile</a></td>
        </tr>
       <tr>
          <td><a href="javascript:void (goGame('14030'));">Wild Blood</a></td>
          <td><a href="javascript:void (goGame('14522'));">Wild Blood Mobile</a></td>
        </tr>
    </table>
    <?php endif;?>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 2)://Sheriff Gaming Tester?>
      <br><br>
      Sheriff
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('3072'));">The Amsterdam Master Plan</a></td>
        </tr>
      </table>
    <?php endif;?>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 9)://Bally Tester?>
      <br><br>
      Bally
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('9999'));">Wild Huskies</a></td>
          <td><a href="javascript:void (goGame('9001'));">TigerTreasures</a></td>
          <td><a href="javascript:void (goGame('9002'));">VegasHits</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('9003'));">BigVegas</a></td>
          <td><a href="javascript:void (goGame('9004'));">CupidPsyche</a></td>
          <td><a href="javascript:void (goGame('9005'));">CashWave</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('9006'));">MayanTreasures</a></td>
          <td><a href="javascript:void (goGame('9007'));">PharaohsDream</a></td>
          <td><a href="javascript:void (goGame('9008'));">Morocco</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('9009'));">Quick Hit Platinum</a></td>
          <td><a href="javascript:void (goGame('9010'));">Hot Shot</a></td>
          <td></td>
        </tr>

      </table>
    <br>
    Bally Mobile
          <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('9501'));">Havana Cubana - Mobile</a></td>
          <td><a href="javascript:void (goGame('9502'));">Mayan Treasures - Mobile</a></td>
          <td><a href="javascript:void (goGame('9503'));">Cash Wizard - Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('9504'));">Cash Spin - Mobile</a></td>
          <td><a href="javascript:void (goGame('9505'));">Quick Hit Platinum Triple Blazing 7s - Mobile</a></td>
          <td><a href="javascript:void (goGame('9506'));">Wild Huskies - Mobile</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('9507'));">Cupid and Psyche - Mobile</a></td>
          <td><a href="javascript:void (goGame('9508'));">Big Vegas - Mobile</a></td>
          <td></td>
        </tr>
      </table>
    <?php endif;?>
    <br><br>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 10)://eyecon Tester?>
      <br><br>
      Eyecon
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('2003'));">Aztec Rising</a></td>
          <td><a href="javascript:void (goGame('2032'));">Beez Knees</a></td>
          <td><a href="javascript:void (goGame('2005'));">Cupids Arrow</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('2007'));">Doubleup Ducks</a></td>
          <td><a href="javascript:void (goGame('2009'));">Fireworks Frenzy</a></td>
          <td><a href="javascript:void (goGame('2001'));">Fluffy Favs</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('2014'));">Happy Mushroom</a></td>
          <td><a href="javascript:void (goGame('2033'));">Irish Luck</a></td>
          <td><a href="javascript:void (goGame('2017'));">Jewel Journey</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('2018'));">Lost Island</a></td>
          <td><a href="javascript:void (goGame('2045'));">Make over Magic</a></td>
          <td><a href="javascript:void (goGame('2020'));">Mermaids Pearl</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('2021'));">Pharaohs Luck</a></td>
          <td><a href="javascript:void (goGame('2022'));">Piggy Payout</a></td>
          <td><a href="javascript:void (goGame('2023'));">Pirate Princess</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('2047'));">Secret Garden 2</a></td>
          <td><a href="javascript:void (goGame('2025'));">Secret Garden</a></td>
          <td><a href="javascript:void (goGame('2026'));">Shamanss Dream</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('2027'));">Shopping Spree</a></td>
          <td><a href="javascript:void (goGame('2030'));">Temple of Isis</a></td>
          <td><a href="javascript:void (goGame('2002'));">White Wizard</a></td>
        </tr>
    </table>
    <br><br>

    <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('2500'));">Fluffy Favourites Mini</a></td>
          <td><a href="javascript:void (goGame('2501'));">Fireworks Frenzy Mini</a></td>
          <td><a href="javascript:void (goGame('2502'));">Temple of Isis Mini</a></td>
    </tr>
        <tr>
          <td><a href="javascript:void (goGame('2503'));">Sharman's Dream Mini</a></td>
          <td><a href="javascript:void (goGame('2504'));">Irish Luck Mini</a></td>
          <td><a href="javascript:void (goGame('2505'));">White Wizard Mini</a></td>
    </tr>
        <tr>
          <td><a href="javascript:void (goGame('2506'));">Secret Garden Mini</a></td>
          <td></td>
          <td></td>
    </tr>

    <!--
        <tr>
          <td><a href="javascript:void (goGame('2019'));">Lucky Blossom</a></td>
          <td><a href="javascript:void (goGame('2013'));">Gets the Worm</a></td>
          <td><a href="javascript:void (goGame('2015'));">Horsing About</a></td>
          <td><a href="javascript:void (goGame('2010'));">Fortunes Prophet</a></td>
          <td><a href="javascript:void (goGame('2011'));">Frolicking Frogs</a></td>
          <td><a href="javascript:void (goGame('2012'));">Gamma Girl</a></td>
          <td><a href="javascript:void (goGame('2008'));">Easter Island</a></td>
          <td><a href="javascript:void (goGame('2004'));">Big Top Bucks</a></td>
          <td><a href="javascript:void (goGame('2024'));">Savannah Sunset</a></td>
          <td><a href="javascript:void (goGame('2028'));">Single 0 Roulette</a></td>
          <td><a href="javascript:void (goGame('2029'));">Super Nova</a></td>
          <td><a href="javascript:void (goGame('2031'));">Tin Town</a></td>
          <td><a href="javascript:void (goGame('2034'));">Wonder Wings</a></td>
          <td><a href="javascript:void (goGame('2036'));">Quest for the Grail</a></td>
          <td><a href="javascript:void (goGame('2037'));">Frightening Fortunes</a></td>
          <td><a href="javascript:void (goGame('2038'));">Enchanted Prince</a></td>
          <td><a href="javascript:void (goGame('2042'));">Happy Harvest</a></td>
          <td><a href="javascript:void (goGame('2046'));">Pharaoh`s Luck MultiScratch Jackpot</a></td>
          <td><a href="javascript:void (goGame('2049'));">Frolikin Frogs Multiscratch</a></td>
        </tr>-->
      </table>


        <br><br>

    <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('2601'));">Fluffy Favourites Mobile</a></td>
          <td><a href="javascript:void (goGame('2602'));">Temple of Isis Mobile</a></td>
          <td><a href="javascript:void (goGame('2603'));">Shopping Spree Mobile</a></td>
    </tr>
        <tr>
          <td><a href="javascript:void (goGame('2604'));">Sharmans Dream Mobile</a></td>
          <td><a href="javascript:void (goGame('2605'));">Secret Garden Mobile</a></td>
          <td><a href="javascript:void (goGame('2606'));">Lost Island Mobile</a></td>
    </tr>
        <tr>
          <td><a href="javascript:void (goGame('2607'));">Gets the Worm Mobile</a></td>
          <td><a href="javascript:void (goGame('2608'));">Enchanted Prince Mobile</a></td>
          <td><a href="javascript:void (goGame('2609'));">Double Up Ducks Mobile</a></td>
    </tr>

    </table>

      <br><br>

    <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('2610'));">Xmas Cash Mobile</a></td>
          <td><a href="javascript:void (goGame('2611'));">Sunny Money Mobile</a></td>
          <td><a href="javascript:void (goGame('2612'));">Money Bunny Mobile</a></td>
    </tr>
        <tr>
          <td><a href="javascript:void (goGame('2613'));">Haunted Hallows Mobile</a></td>
          <td><a href="javascript:void (goGame('2614'));">Fiddle Dee Dough Mobile</a></td>
          <td><a href="javascript:void (goGame('2615'));">Autumn Gold Mobile</a></td>
    </tr>

    </table>

    <?php endif;?>
    <br><br>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 11)://igt Tester?>
      <br><br>
      IGT - * means it's a mobile game <!--FYI if you are testing this on the web (not mobile) you need<br>to enable the certificate by clicking this link <a href="https://ipl-dmzcust03-skins.lab.wagerworks.com/">IGT Cert (cust 03)</a>&nbsp;&nbsp;<a href="https://ipl-dmzcust02-skins.lab.wagerworks.com/">IGT Cert (cust 02)</a><br><br> -->
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('4501'));">Da Vinci diamonds*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4500'));">Cats*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4502'));">Kitty Glitter*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4503'));">Cleopatra*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4504'));">Wolf Run*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4505'));">Elvis ALMA*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4506'));">Dungeons & Dragons*</a></td>
        </tr>
     <tr>
          <td><a href="javascript:void (goGame('4015'));">Star Trek</a></td>
        </tr>
     <tr>
          <td><a href="javascript:void (goGame('4068'));">CLUEDO Classic</a></td>
        </tr>
     <tr>
          <td><a href="javascript:void (goGame('4039'));">Pixies of the Forest</a></td>
        </tr>
     <tr>
          <td><a href="javascript:void (goGame('4072'));">Shes a Rich Girl</a></td>
        </tr>
     <tr>
          <td><a href="javascript:void (goGame('4029'));">Siberian Storm</a></td>
        </tr>
      </table>
    <br>
    <?php endif;?>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 3)://igt Tester?>
      <br><br>
      IGT - * means it's a mobile game <!--FYI if you are testing this on the web (not mobile) you need<br>to enable the certificate by clicking this link <a href="https://ipl-dmzcust03-skins.lab.wagerworks.com/">IGT Cert (cust 03)</a>&nbsp;&nbsp;<a href="https://ipl-dmzcust02-skins.lab.wagerworks.com/">IGT Cert (cust 02)</a><br><br> -->
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('4501'));">Da Vinci diamonds*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4500'));">Cats*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4502'));">Kitty Glitter*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4503'));">Cleopatra*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4504'));">Wolf Run*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4505'));">Elvis ALMA*</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('4506'));">Dungeons & Dragons*</a></td>
        </tr>
	</table>
	
    Flash Games
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
<tr><td><a href="javascript:void (goGame('4001'));">3 Wheel Roulette</a></td></tr>
<tr><td><a href="javascript:void (goGame('4006'));">Da Vinci Diamonds</a></td></tr>
<tr><td><a href="javascript:void (goGame('4007'));">Dungeons & Dragons</a></td></tr>
<tr><td><a href="javascript:void (goGame('4505'));">Elvis ALMA</a></td></tr>
<tr><td><a href="javascript:void (goGame('4015'));">Star Trek</a></td></tr>
<tr><td><a href="javascript:void (goGame('4021'));">Treasures of Troy</a></td></tr>
<tr><td><a href="javascript:void (goGame('4024'));">Wolf Run</a></td></tr>
<tr><td><a href="javascript:void (goGame('4025'));">Wild Wolf</a></td></tr>
<tr><td><a href="javascript:void (goGame('4026'));">Monopoly You`re In the Money</a></td></tr>
<tr><td><a href="javascript:void (goGame('4027'));">Double Bonus Spin Roulette</a></td></tr>
<tr><td><a href="javascript:void (goGame('4028'));">Da Vinci Diamonds Dual Play</a></td></tr>
<tr><td><a href="javascript:void (goGame('4029'));">Siberian Storm</a></td></tr>
<tr><td><a href="javascript:void (goGame('4039'));">Pixies of the Forest</a></td></tr>
<tr><td><a href="javascript:void (goGame('4040'));">Monty`s Millions</a></td></tr>
<tr><td><a href="javascript:void (goGame('4041'));">White Orchid</a></td></tr>
<tr><td><a href="javascript:void (goGame('4042'));">Monopoly Multiplier</a></td></tr>
<tr><td><a href="javascript:void (goGame('4043'));">Wipeout</a></td></tr>
<tr><td><a href="javascript:void (goGame('4044'));">Grand Monarch</a></td></tr>
<tr><td><a href="javascript:void (goGame('4045'));">Captain Quid`s Treasure Quest</a></td></tr>
<tr><td><a href="javascript:void (goGame('4047'));">Noah`s Ark</a></td></tr>
<tr><td><a href="javascript:void (goGame('4048'));">Kitty Glitter</a></td></tr>
<tr><td><a href="javascript:void (goGame('4054'));">Fire Opals</a></td></tr>
<tr><td><a href="javascript:void (goGame('4055'));">Coyote Moon</a></td></tr>
<tr><td><a href="javascript:void (goGame('4056'));">Diamond Queen</a></td></tr>
<tr><td><a href="javascript:void (goGame('4057'));">Monopoly PLUS</a></td></tr>
<tr><td><a href="javascript:void (goGame('4058'));">Game King Bonus Poker</a></td></tr>
<tr><td><a href="javascript:void (goGame('4059'));">Game King Double Bonus Poker</a></td></tr>
<tr><td><a href="javascript:void (goGame('4060'));">Game King Double Double Bonus Poker</a></td></tr>
<tr><td><a href="javascript:void (goGame('4061'));">Game King Jacks or Better</a></td></tr>
      </table>

    <?php endif;?>
	
    <br><br>

    <?php if ($Response["Data"][0]["PlayerTypeID"] == 1 || $Response["Data"][0]["PlayerTypeID"] == 21)://Scientific Games Tester?>
      <br><br>
      Scientific Games Flash
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('19001'));">Raging Rhino</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('19002'));">Spartacus (staging)</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('19003'));">Rainbow Riches (staging)</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('19004'));">Cash Spin</a></td>
        </tr>		
      </table>

      <br><br>
      Scientific Games HTML5
      <table border="1" cellpadding="10" cellspacing="1" bordercolor="#999999">
        <tr>
          <td><a href="javascript:void (goGame('19501'));">Raging Rhino</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('19502'));">Spartacus</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('19503'));">Rainbow Riches</a></td>
        </tr>
        <tr>
          <td><a href="javascript:void (goGame('19504'));">Cash Spin</a></td>
        </tr>		
        <tr>
          <td><a href="javascript:void (goGame('19505'));">Rainbow Riches Pick & Mix (staging)</a></td>
        </tr>		
      </table>
    <br>
	
    <?php endif;?>	
	
    <br><br>



  </div>

  <div>
    <form name="navForm" method="POST" action="">
      <input type="hidden" id="PlayerID"  name="PlayerID"  value="<?php echo $_POST["PlayerID"];?>">
      <input type="hidden" id="SessionID" name="SessionID" value="<?php echo $_POST["SessionID"];?>">
      <input type="hidden" id="GameID" name="GameID" value="">
    </form>
  </div>
</body>
</html>